USE [haz_782]
GO

/****** Object:  Table [dbo].[sfr]    Script Date: 3/5/2015 11:45:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sfr](
	[sfr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sfr_type] [int] NULL,
	[date_submitted] [datetime] NULL,
	[username] [varchar](90) NULL,
	[completed_flag] [bit] NULL,
	[ship_id] [bigint] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](5) NULL,
	[part_no] [nvarchar](255) NULL,
	[mfg_city] [nvarchar](255) NULL,
	[mfg_address] [nvarchar](255) NULL,
	[mfg_state] [nvarchar](20) NULL,
	[mfg_zip] [nvarchar](10) NULL,
	[system_equipment_material] [varchar](1024) NULL,
	[method_of_application] [varchar](1024) NULL,
	[proposed_usage] [varchar](1024) NULL,
	[negative_impact] [varchar](1024) NULL,
	[special_training] [varchar](1024) NULL,
	[precautions] [varchar](1024) NULL,
	[properties] [varchar](1024) NULL,
	[comments] [varchar](1024) NULL,
	[advantages] [varchar](1024) NULL,
	[inventory_detail_id] [int] NULL,
	[item_name] [nvarchar](100) NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](10) NULL,
	[specification_no] [nvarchar](10) NULL,
	[ui_id] [int] NULL,
	[mfg_poc] [nvarchar](255) NULL,
	[mfg_phone] [nvarchar](15) NULL,
	[msds_attached] [bit] NULL,
	[date_mailed] [datetime] NULL,
 CONSTRAINT [PK_sfr] PRIMARY KEY CLUSTERED 
(
	[sfr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[sfr]  WITH CHECK ADD  CONSTRAINT [FK_sfr_ui] FOREIGN KEY([ui_id])
REFERENCES [dbo].[ui] ([ui_id])
GO

ALTER TABLE [dbo].[sfr] CHECK CONSTRAINT [FK_sfr_ui]
GO

