USE [haz_782]
GO

/****** Object:  Table [dbo].[user_roles]    Script Date: 2/24/2015 11:47:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[user_roles](
	[user_role_id] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[role] [nvarchar](50) NULL,
	[workcenter_id] [bigint] NULL,
 CONSTRAINT [PK_user_roles] PRIMARY KEY CLUSTERED 
(
	[user_role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[user_roles]  WITH CHECK ADD  CONSTRAINT [FK_user_roles_workcenter] FOREIGN KEY([workcenter_id])
REFERENCES [dbo].[workcenter] ([workcenter_id])
GO

ALTER TABLE [dbo].[user_roles] CHECK CONSTRAINT [FK_user_roles_workcenter]
GO

