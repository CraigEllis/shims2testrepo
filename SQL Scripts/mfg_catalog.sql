USE [haz_782]
GO

/****** Object:  Table [dbo].[mfg_catalog]    Script Date: 3/5/2015 12:26:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mfg_catalog](
	[mfg_catalog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cage] [nvarchar](5) NULL,
	[manufacturer] [nvarchar](255) NULL,
	[msds_no] [nvarchar](20) NULL,
	[niin] [nvarchar](10) NULL,
 CONSTRAINT [PK_mfg_catalog] PRIMARY KEY CLUSTERED 
(
	[mfg_catalog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

