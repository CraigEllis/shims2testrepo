alter table offload_list add slc_id bigint
alter table offload_list add slac_id bigint
alter table offload_list add hcc_id bigint
alter table offload_list add usage_category_id bigint
alter table offload_list add smcc_id bigint

ALTER TABLE offload_list ADD CONSTRAINT fk_slc FOREIGN KEY(slc_id) REFERENCES shelf_life_code(shelf_life_code_id) 
ALTER TABLE offload_list ADD CONSTRAINT fk_slac FOREIGN KEY(slac_id) REFERENCES shelf_life_action_code(shelf_life_action_code_id)
ALTER TABLE offload_list ADD CONSTRAINT fk_hcc FOREIGN KEY(hcc_id) REFERENCES hcc(hcc_id)
ALTER TABLE offload_list ADD CONSTRAINT fk_usage_at FOREIGN KEY(usage_category_id) REFERENCES usage_category(usage_category_id)
ALTER TABLE offload_list ADD CONSTRAINT fk_smcc FOREIGN KEY(smcc_id) REFERENCES smcc(smcc_id)