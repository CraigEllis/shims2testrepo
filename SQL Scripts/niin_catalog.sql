USE [haz_782]
GO

/****** Object:  Table [dbo].[niin_catalog]    Script Date: 3/5/2015 11:45:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[niin_catalog](
	[niin_catalog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](9) NULL,
	[ui] [int] NULL,
	[um] [nvarchar](50) NULL,
	[usage_category_id] [bigint] NULL,
	[description] [nvarchar](1024) NULL,
	[smcc_id] [bigint] NULL,
	[specs] [nvarchar](50) NULL,
	[shelf_life_code_id] [bigint] NULL,
	[shelf_life_action_code_id] [bigint] NULL,
	[remarks] [text] NULL,
	[storage_type_id] [bigint] NULL,
	[cog_id] [bigint] NULL,
	[spmig] [nvarchar](255) NULL,
	[nehc_rpt] [nvarchar](10) NULL,
	[catalog_group_id] [bigint] NULL,
	[catalog_serial_number] [nvarchar](255) NULL,
	[allowance_qty] [int] NULL,
	[created] [datetime] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](255) NULL,
	[msds_num] [nvarchar](255) NULL,
	[NMCPHC] [nvarchar](20) NULL,
	[part_name] [nvarchar](25) NULL,
	[ship_id] [bigint] NULL,
	[manually_entered] [bit] NULL,
 CONSTRAINT [PK_niin_catalog] PRIMARY KEY CLUSTERED 
(
	[niin_catalog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_smclniin] UNIQUE NONCLUSTERED 
(
	[niin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'federal supply class - 4 digit number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'niin_catalog', @level2type=N'COLUMN',@level2name=N'fsc'
GO

