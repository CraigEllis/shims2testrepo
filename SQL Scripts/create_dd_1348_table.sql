USE [haz_782]
GO

/****** Object:  Table [dbo].[dd_1348]    Script Date: 2/5/15 1:05:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[dd_1348](
	[dd_1348_id] [bigint] IDENTITY(1,1) NOT NULL,
	[offload_list_id] [bigint] NOT NULL,
	[archive_id] [bigint] NULL,
	[document_number] [nvarchar](50) NULL,
	[doc_ident] [nvarchar](50) NULL,
	[ri_from] [nvarchar](50) NULL,
	[m_and_s] [nvarchar](50) NULL,
	[ser] [nvarchar](50) NULL,
	[suppaddress] [nvarchar](50) NULL,
	[sig] [nvarchar](50) NULL,
	[fund] [nvarchar](50) NULL,
	[distribution] [nvarchar](50) NULL,
	[project] [nvarchar](50) NULL,
	[pri] [nvarchar](50) NULL,
	[reqd_del_date] [nvarchar](50) NULL,
	[adv] [nvarchar](50) NULL,
	[ri] [nvarchar](50) NULL,
	[op] [nvarchar](50) NULL,
	[mgt] [nvarchar](50) NULL,
	[condition] [nvarchar](50) NULL,
	[unit_price] [nvarchar](50) NULL,
	[total_price] [nvarchar](50) NULL,
	[ship_from] [nvarchar](50) NULL,
	[ship_to] [nvarchar](50) NULL,
	[mark_for] [nvarchar](50) NULL,
	[nmfc] [nvarchar](50) NULL,
	[frt_rate] [nvarchar](50) NULL,
	[cargo_type] [nvarchar](50) NULL,
	[ps] [nvarchar](50) NULL,
	[qty_recd] [nvarchar](50) NULL,
	[up] [nvarchar](50) NULL,
	[unit_weight] [nvarchar](50) NULL,
	[unit_cube] [nvarchar](50) NULL,
	[ufc] [nvarchar](50) NULL,
	[sl] [nvarchar](50) NULL,
	[item_name] [nvarchar](50) NULL,
	[ty_cont] [nvarchar](50) NULL,
	[no_cont] [nvarchar](50) NULL,
	[total_weight] [nvarchar](50) NULL,
	[total_cube] [nvarchar](50) NULL,
	[recd_by] [nvarchar](max) NULL,
	[date_recd] [nvarchar](50) NULL,
	[doc_num] [nvarchar](max) NULL,
	[memo_add_1] [nvarchar](max) NULL,
	[memo_add_2] [nvarchar](max) NULL,
 CONSTRAINT [PK_dd_1348] PRIMARY KEY CLUSTERED 
(
	[dd_1348_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[dd_1348]  WITH CHECK ADD  CONSTRAINT [FK_dd_1348_offload_list] FOREIGN KEY([offload_list_id])
REFERENCES [dbo].[offload_list] ([offload_list_id])
GO

ALTER TABLE [dbo].[dd_1348] CHECK CONSTRAINT [FK_dd_1348_offload_list]
GO

