-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[update_offload_list] ON [dbo].[archived]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Offloadid Bigint
	DECLARE @Archivedid Bigint

	DECLARE archive_cursor CURSOR FOR
		SELECT archived_id, offload_list_id 
		FROM archived;

	OPEN archive_cursor

	FETCH NEXT FROM archive_cursor
	INTO @Archivedid, @Offloadid

	WHILE @@FETCH_STATUS = 0
	BEGIN

		UPDATE offload_list 
		   SET archived_id = @Archivedid
		 WHERE offload_list_id = @Offloadid

		FETCH NEXT FROM archive_cursor
		INTO @Archivedid, @Offloadid
	END
	CLOSE archive_cursor;
	DEALLOCATE archive_cursor;
END
GO
