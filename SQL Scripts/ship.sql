USE [haz_782]
GO

/****** Object:  Table [dbo].[ships]    Script Date: 2/24/2015 11:47:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ships](
	[ship_id] [bigint] IDENTITY(1,1) NOT NULL,
	[current_ship] [bit] NOT NULL,
	[name] [nvarchar](50) NULL,
	[uic] [nvarchar](10) NULL,
	[hull_type] [nvarchar](10) NULL,
	[hull_number] [nvarchar](10) NULL,
	[address] [nvarchar](100) NULL,
	[POC_Name] [nvarchar](50) NULL,
	[POC_Telephone] [nvarchar](50) NULL,
	[POC_Email] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[zip] [nvarchar](10) NULL,
	[act_code] [nvarchar](25) NULL,
	[base] [nvarchar](50) NULL,
	[sndl] [nvarchar](25) NULL,
	[ship_logo_filename] [nvarchar](50) NULL,
	[ship_picture_filename] [nvarchar](50) NULL,
	[POC_Address] [nvarchar](100) NULL,
	[POC_Title] [nvarchar](50) NULL,
 CONSTRAINT [PK_ships] PRIMARY KEY CLUSTERED 
(
	[ship_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

