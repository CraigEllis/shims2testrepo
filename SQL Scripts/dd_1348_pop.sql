USE [haz_782]
GO
/****** Object:  Trigger [dbo].[dd_1348_pop]    Script Date: 2/5/15 12:56:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Craig Ellis
-- Create date: 1-28-15
-- Description:	Adds a new entry into the dd_1348 table for document creation
-- =============================================
ALTER TRIGGER [dbo].[dd_1348_pop] ON [dbo].[offload_list]
   FOR INSERT 
AS 
DECLARE @Offloadid Bigint
DECLARE @mfgCatalogId Bigint

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @Offloadid =(SELECT offload_list_id FROM INSERTED);
	SELECT @MfgCatalogId = (SELECT mfg_catalog_id FROM INSERTED);
    INSERT INTO dd_1348 (offload_list_id, unit_price, total_price)values(@Offloadid,'0','0');

	IF(@MfgCatalogId is null)
	INSERT INTO unique_offload (offload_list_id) values(@Offloadid);

END