alter table offload_list drop column serial_number
alter table offload_list drop column bulk_item
alter table offload_list drop column manufacturer_date
alter table offload_list drop column alternate_ui
alter table offload_list drop column offload_detail_id
alter table offload_list drop constraint fk_offload_reason
alter table offload_list drop column offload_reason_id
alter table offload_list drop column batch_number
alter table offload_list drop column lot_number
alter table offload_list drop column contract_number
alter table offload_list drop column alternate_um
alter table offload_list drop column cosal
alter table offload_list drop column atm

alter table offload_list add fsc nvarchar(50)
alter table offload_list add niin nvarchar(50)
alter table offload_list add cage nvarchar(50)
alter table offload_list add description nvarchar(max)
alter table offload_list add ui nvarchar(50)
alter table offload_list add um nvarchar(50)
alter table offload_list add add_data_4 nvarchar(max)

update offload_list 
set offload_list.fsc = unique_offload.fsc,
	offload_list.niin = unique_offload.niin,
	offload_list.description = unique_offload.description,
	offload_list.ui = unique_offload.ui,
	offload_list.um = unique_offload.um,
	offload_list.add_data_4 = unique_offload.add_data_4
from offload_list
left join unique_offload
on (offload_list.offload_list_id = unique_offload.offload_list_id)

drop table unique_offload

