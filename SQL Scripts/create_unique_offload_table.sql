USE [haz_782]
GO

/****** Object:  Table [dbo].[unique_offload]    Script Date: 2/5/15 1:04:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[unique_offload](
	[unique_offload_id] [bigint] IDENTITY(1,1) NOT NULL,
	[offload_list_id] [bigint] NOT NULL,
	[fsc] [nvarchar](50) NULL,
	[niin] [nvarchar](50) NULL,
	[cage] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[ui] [nvarchar](50) NULL,
	[um] [nvarchar](50) NULL,
	[add_data_4] [nvarchar](max) NULL,
 CONSTRAINT [PK_unique_offload] PRIMARY KEY CLUSTERED 
(
	[unique_offload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[unique_offload]  WITH CHECK ADD  CONSTRAINT [FK_unique_offload_offload_list] FOREIGN KEY([offload_list_id])
REFERENCES [dbo].[offload_list] ([offload_list_id])
GO

ALTER TABLE [dbo].[unique_offload] CHECK CONSTRAINT [FK_unique_offload_offload_list]
GO

