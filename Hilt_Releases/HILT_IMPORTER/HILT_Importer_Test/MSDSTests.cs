﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms;
using System.Data.OleDb;
using HILT_IMPORTER;

namespace HILT_Importer_Test {
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class MSDSTests {
        private TestContext testContextInstance;
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        //public void HMIRSConnectTest() {
        //    int count = 0;

        //    // Connect to the HMIRS DB using ODBC
        //    System.Data.Odbc.OdbcConnection conn =
        //        new System.Data.Odbc.OdbcConnection();
        //    conn.ConnectionString = "DSN=CDROM_CDB";
        //    try {
        //        conn.Open();

        //        // Process data here.
        //        String sql = "SELECT COUNT(*) FROM msdsonline_dbo.Company";
        //        OdbcCommand cmd = new OdbcCommand(sql, conn);
        //        count = (Int32) cmd.ExecuteScalar();
        //    }
        //    catch (Exception ex) {
        //        MessageBox.Show("Failed to connect to data source");
        //    }
        //    finally {
        //        conn.Close();
        //    }

        //    Assert.AreEqual(39930, count);
        //}

        [TestMethod]
        public void formatNumberThousandsTest() {
            int count = 0;

            // Create a BaseImportForm
            BaseImportForm form = new BaseImportForm();

            // Tests
            int num = 4;
            string expected = "4";
            string test = BaseImportForm.formatNumberThousands(num);

            Assert.AreEqual(expected, test);

            num = 44;
            expected = "44";
            test = BaseImportForm.formatNumberThousands(num);

            Assert.AreEqual(expected, test);

            num = 444;
            expected = "444";
            test = BaseImportForm.formatNumberThousands(num);

            Assert.AreEqual(expected, test);

            num = 4444;
            expected = "4,444";
            test = BaseImportForm.formatNumberThousands(num);

            Assert.AreEqual(expected, test);

            num = 44444;
            expected = "44,444";
            test = BaseImportForm.formatNumberThousands(num);

            Assert.AreEqual(expected, test);
        }
    }
}
