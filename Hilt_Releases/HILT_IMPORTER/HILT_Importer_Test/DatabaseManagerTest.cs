﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HILT_IMPORTER;
using HILT_IMPORTER.DataObjects;

namespace HILT_Importer_Test {
    [TestClass]
    public class DatabaseManagerTest {

        [TestMethod]
        public void getTableCountTest() {
            int count = 0;

            DatabaseManager mgr = new DatabaseManager();

            // Process data here.
            count = mgr.getTableCount("HULL_TYPES");

            Assert.IsTrue(count > 0, "Count = " + count.ToString());
        }

        [TestMethod]
        public void getLastMsdsUpdateTest() {
            DatabaseManager mgr = new DatabaseManager();

            DateTime lastUpdate = mgr.getLastHMIRSUpdate();

            Assert.IsTrue(lastUpdate > DateTime.MinValue);
        }

        [TestMethod]
        public void setLastMsdsUpdateTest() {
            DatabaseManager mgr = new DatabaseManager();

            DateTime lastUpdate = new DateTime(2011, 09, 1);
                
            bool result = mgr.setLastHMIRSUpdate(lastUpdate);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void saveMSDSUpdateInfoTest() {
            DatabaseManager mgr = new DatabaseManager();

            MSDSUpdateInfo info = new MSDSUpdateInfo();
            info.table_name = "TEST_TEST";
            info.last_update = new DateTime(2010, 3, 11);

            bool result = mgr.saveMSDSUpdateInfo(info);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void isNewMSDSSerNoTest() {
            DatabaseManager mgr = new DatabaseManager();

            // Known good value - should return false
            bool isNew = mgr.isNewMSDSSerNo("BCWMS");

            Assert.IsFalse(isNew);

            // Known bad value - should return true
            isNew = mgr.isNewMSDSSerNo("-----");

            Assert.IsTrue(isNew);
        }
    }
}
