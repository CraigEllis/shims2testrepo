﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HILT_IMPORTER;
using HILT_IMPORTER.DataObjects;

namespace HILT_Importer_Test {
    [TestClass]
    public class HMIRSImporterTest {
        [TestMethod]
        public void extractHMIRSDocumentsTest() {
            List<HMIRSDocumentRecord> list = null;
            DateTime lastUpdate = new DateTime(2011, 9, 1); // should find just disk 5
            //DateTime lastUpdate = DateTime.MinValue;  // Do the whole enchilada
            bool result = false;

            try {
                HMIRSImporter importer = new HMIRSImporter(null, null);

                result = importer.extractHMIRSDocuments(lastUpdate,
                    "C:\\CD Install\\", "C:\\test\\DiskNumExtract");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                result = false;
            }

            Assert.IsTrue(result);
        }

            [TestMethod]
        public void createMSDSDocumentsTest() {
            List<HMIRSDocumentRecord> list = null;
            //DateTime lastUpdate = new DateTime(2011, 9, 21); // should find 2 MSDSs
            DateTime lastUpdate = DateTime.MinValue;  // Do the whole enchilada
            bool result = false;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                List<string> msdsSerNoList = new List<string>();//mgr.getNewMSDSList(lastUpdate);

                // Add a serial # with no known documents to test how code handles no file found
                //msdsSerNoList.Add("CGMWC");

                HMIRSImporter importer = new HMIRSImporter(null, null);

                result = importer.createMSDSDocuments(msdsSerNoList);
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                result = false;
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void createNewDocZipFileTest() {
            List<HMIRSDocumentRecord> list = null;
            //DateTime lastUpdate = new DateTime(2011, 9, 21); // should find 2 MSDSs
            DateTime lastUpdate = DateTime.MinValue;  // Do the whole enchilada
            bool result = false;

            try {
                // Create a list with 3 files in it.
                List<string> msdsFileList = new List<string>();
                msdsFileList.Add("C:\\test\\Hilt_Hub_Data\\MSDS_Docs\\BCTZL_MSDS_19870101_US_E_R.HTML");
                msdsFileList.Add("C:\\test\\Hilt_Hub_Data\\MSDS_Docs\\CMDBX_MSDS_20010523_US_E_A.RTF");
                msdsFileList.Add("C:\\test\\Hilt_Hub_Data\\MSDS_Docs\\CQNJC_MFR. LABEL_19950901_US_E_A.PDF");

                HMIRSImporter importer = new HMIRSImporter(null, null);

                result = importer.createNewDocZipFile("C:\\test\\", msdsFileList);
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("Failed to process\n" + ex.Message);
                result = false;
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void formatTickCountToHHmmssTest() {
            // Expected results
            string secondsOnly = "00:00:33";
            string secondsMinutes = "00:18:22";
            string secondsMinutesHours = "06:08:01";
            string bigHours = "33:12:12";

            // secondsOnly
            string test = HMIRSImporter.formatTickCountToHHmmss(33300);
            Assert.IsTrue(test.Equals(secondsOnly), "test = " + test);

            // secondsMinutes
            test = HMIRSImporter.formatTickCountToHHmmss(1101700);
            Assert.IsTrue(test.Equals(secondsMinutes), "test = " + test);

            // secondsMinutesHours
            test = HMIRSImporter.formatTickCountToHHmmss(22081400);
            Assert.IsTrue(test.Equals(secondsMinutesHours), "test = " + test);

            // bigHours
            test = HMIRSImporter.formatTickCountToHHmmss(119531890);
            Assert.IsTrue(test.Equals(bigHours), "test = " + test);
        }
    }
}
