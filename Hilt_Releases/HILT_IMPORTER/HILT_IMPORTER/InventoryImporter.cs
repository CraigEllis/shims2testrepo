﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;

namespace HILT_IMPORTER
{
    public class InventoryImporter
    {
        //Initialize variables
        private string serverPath;
        private string username;
        private string password;
        private string uploadFilePath;
        private int port;

        CSVParser parser = new CSVParser();
        List<string[]> itemList = new List<string[]>();

        FileInfo inventoryLog;

        private SqlConnection conn;
        private SqlConnection mappingConn;

       /* Method used to insert records into the inventory table.  These
        * records come from a tab delimited file that is uploaded and
        * parsed using the CSVParser.  The parser returns a List of String 
        * arrays that are then used to create valid data records.
        */
        public void insertInventory()
        {
            #region HILT DB Connection Test
            try {
                conn = createConnection(this.serverPath, this.port, this.username, this.password);
                mappingConn = createConnection(this.serverPath, this.port, this.username, this.password);
                conn.Open();
                conn.Close();
            } catch (Exception) {
                throw new ImportException("Could not connect to the HILT database with the specified host: " + this.ServerPath + " port: " + 
                                            this.Port + " Username: " + this.Username + " Password: " + this.Password);                
            }
            #endregion

            try{
                Stream fileStream = new FileStream(uploadFilePath, FileMode.Open, FileAccess.Read);
                itemList = parser.parseCSV(fileStream);
            } catch (Exception ex) {
                throw new ImportException("Error reading from the specified file.  Returned the following error " + ex.Message);
            }

            if (itemList.Count != 0)
            {
                ////First we need to grab the workcenters and insert them into the table.
                //this.insertWorkCenter(this.itemList);

                ////Make sure in the database there is a workcenter wid 'NONE' description 'UNKNOWN' before running locations code.
                ////Next we need to insert locations, based on the workcenters we just entered.
                //this.insertLocations(this.itemList);

                ////Finally insert the inventory.
                this.insertInventoryFromList(this.itemList);

                ////Lastly insert atmosphere control information
                //this.insertATMC(this.itemList);

            }
            else
            {
                throw new ImportException("Specified file is empty, stopping process.");
            }        
        }

        /* Method used to insert workstations.
         */
        public void insertWorkCenter(List<string[]> items) 
        {        
            //Initialize Variables
            Dictionary<string, int> wcMap;
            string description = "", wid = "", loc_name = "";
            int wc_id = 0;
            
            //Create SQL Commands for use in inserting and updating.
            string insertWorkCenterStmt = "INSERT into workcenter (description, wid) values (@description, @wid); SELECT SCOPE_IDENTITY()";
            string insertLocStmt = "INSERT into locations (name, workcenter_id) values (@name, @workcenter_id)";
            string selectWid = "SELECT wid from workcenter where wid = @wid_value";
            SqlCommand insertWorkCenterCmd = new SqlCommand(insertWorkCenterStmt, conn);
            SqlCommand insertLocationCmd = new SqlCommand(insertLocStmt, conn);
            SqlCommand selectWidCmd = new SqlCommand(selectWid, conn);

            //Opening the connection.
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            insertWorkCenterCmd.Transaction = tran;
            insertLocationCmd.Transaction = tran;
            selectWidCmd.Transaction = tran;

            /* First create a list of column headers
             * since we know the first row is always headers.
             */
            string[] headers = items[0];

            foreach(string[] row in items)  
            {

                //The first row of the list will always be HEADERS.
                if (items[0] != row)  {

                    for (int i = 0; i < row.Length; i++)
                    {
                        if (headers[i].ToUpper().Equals("W/C")) 
                        {
                            Object wid_value = null;
                            description = row[i];
                            wid = row[i];

                            if (wid != null || wid != "")
                            {
                                selectWidCmd.Parameters.AddWithValue("@wid_value", wid);
                                wid_value = selectWidCmd.ExecuteScalar();
                                selectWidCmd.Parameters.Clear();
                            }

                            //Insert workcenter and capture workcenter_id.
                            if(wid_value == null && (wid != null || wid != "")){

                                loc_name = "NEWLY ISSUED";

                            try {
                                    //Insert our newly found workcenter.
                                    insertWorkCenterCmd.Parameters.AddWithValue("@description", description);
                                    insertWorkCenterCmd.Parameters.AddWithValue("@wid", wid);
                                    
                                    Object id = insertWorkCenterCmd.ExecuteScalar();
                                    wc_id = Convert.ToInt32(id);
                                    insertWorkCenterCmd.Parameters.Clear();

                                    //Insert a corresponding location for th enewly created workcenter.
                                    insertLocationCmd.Parameters.AddWithValue("@name", loc_name);
                                    insertLocationCmd.Parameters.AddWithValue("@workcenter_id", wc_id);

                                    insertLocationCmd.ExecuteNonQuery();
                                    insertLocationCmd.Parameters.Clear();
                            }
                            catch (Exception ex) {
                                  //Duplicates
                                     System.Diagnostics.Debug.WriteLine(ex.Message);
                        
                                 }
                            }                                                      
                        }
                    }
                }
            }

            try
            {
                tran.Commit();
                System.Diagnostics.Debug.WriteLine("Transaction Committed");
            }
            catch (Exception ex) {                
                System.Diagnostics.Debug.WriteLine("Transaction Committed with error: " + ex.Message);
                conn.Close();
            }

            conn.Close();
        }

        /*  Method used to insert atmosphere control.
         */
        public void insertATMC(List<string[]> atmList)
        {

            //Initialize Variables
            

            //Create SQL Commands for use in inserting and updating.
            string insertLocStmt = "INSERT into atmosphere_control (atmosphere_control_number, cage, niin, msds_serial_number) values (@atmosphere_control_number, @cage, @niin, @msds_serial_number)";
            string selectLocStmt = "SELECT atmosphere_control_id from atmosphere_control where cage = @cage AND niin=@niin";

            //Opening the connection.
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            SqlCommand insertLocCmd = new SqlCommand(insertLocStmt, conn);
            SqlCommand selectLocCmd = new SqlCommand(selectLocStmt, conn);
            insertLocCmd.Transaction = tran;
            selectLocCmd.Transaction = tran;

            /* First create a list of column headers
             * since we know the first row is always headers.
             */
            string[] headers = atmList[0];

            foreach (string[] row in atmList)
            {
                //The first row of the list will always be HEADERS.
                if (atmList[0] != row)
                {
                    string cage = null;
                    string niin = null;
                    string atmc = null;
                    string msds = null;

                    for (int i = 0; i < row.Length; i++)
                    {
                        if (headers[i].ToUpper().Equals("CAGE"))
                        {
                            cage = this.trimToMax(row[i], 5);
                        }
                        else if (headers[i].ToUpper().Equals("NIIN"))
                        {
                            niin = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("SERIAL #"))
                        {
                            atmc = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("MSDS #"))
                        {
                            msds = row[i];
                        }
                    }

                    if (atmc!=null)
                    {
                        //Verify that this atm control number is not already in the db.
                        selectLocCmd.Parameters.Clear();
                        selectLocCmd.Parameters.AddWithValue("@niin", niin);
                        selectLocCmd.Parameters.AddWithValue("@cage", cage);
                        Object id = selectLocCmd.ExecuteScalar();

                        if (id == null)
                        {
                            //insert location
                            insertLocCmd.Parameters.Clear();
                            insertLocCmd.Parameters.AddWithValue("@niin", niin);
                            insertLocCmd.Parameters.AddWithValue("@cage", cage);
                            insertLocCmd.Parameters.AddWithValue("@atmosphere_control_number", atmc);
                            insertLocCmd.Parameters.AddWithValue("@msds_serial_number", msds);
                            insertLocCmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            try
            {
                tran.Commit();
                System.Diagnostics.Debug.WriteLine("Transaction Committed");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Transaction Committed with error: " + ex.Message);
                conn.Close();
            }

            conn.Close();
        }

        /*  Method used to insert locations, based on our recently inserted workstations.
         */
        public void insertLocations(List<string[]> locList) {
            
            //Initialize Variables
            Dictionary<string, int> wcMap = this.getWorkCenterMap();
            string name = "", workcenter_name = "";
            int workcenter_id = 0;
          
            //Create SQL Commands for use in inserting and updating.
            string insertLocStmt = "INSERT into locations (name, workcenter_id) values (@name, @workcenter_id)";
            string selectLocStmt = "SELECT location_id from locations where name = @loc_name";

            //Opening the connection.
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            SqlCommand insertLocCmd = new SqlCommand(insertLocStmt, conn);
            SqlCommand selectLocCmd = new SqlCommand(selectLocStmt, conn);
            insertLocCmd.Transaction = tran;
            selectLocCmd.Transaction = tran;

            /* First create a list of column headers
             * since we know the first row is always headers.
             */
            string[] headers = locList[0];

            foreach(string[] row in locList)  
            {
                //The first row of the list will always be HEADERS.
                if (locList[0] != row)  {

                    for (int i = 0; i < row.Length; i++)
                    {
                        if (headers[i].ToUpper().Equals("LOC")) {
                            name = row[i];
                        }
                        if (headers[i].ToUpper().Equals("W/C"))
                        {
                            if (wcMap.ContainsKey(row[i]))
                                workcenter_id = wcMap[row[i]];
                            else
                                workcenter_id = 0;
                        }
                    }

                    if (name != "" && workcenter_id!=0)
                    {
                        //Verify that this location is not already in the db.
                        selectLocCmd.Parameters.Clear();
                        selectLocCmd.Parameters.AddWithValue("@loc_name", name);
                        Object loc_id = selectLocCmd.ExecuteScalar();

                        if (loc_id == null)
                        {
                            //insert location
                            insertLocCmd.Parameters.Clear();
                            insertLocCmd.Parameters.AddWithValue("@name", name);
                            insertLocCmd.Parameters.AddWithValue("@workcenter_id", workcenter_id);
                            insertLocCmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            try
            {
                tran.Commit();
                System.Diagnostics.Debug.WriteLine("Transaction Committed");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Transaction Committed with error: " + ex.Message);
                conn.Close();
            }

            conn.Close();
        }

       /* Method used to insert records into the Inventory table.  These
        * records come from a tab delimited file that is uploaded and
        * parsed using the CSVParser.  The parser returns a List of String 
        * arrays that are then used to create valid data records.
        */
        public void insertInventoryFromList(List<string[]> itemList) 
        {
            int count = 0;
            //Initialize Variables
            Dictionary<string, int> locMap = this.getLocationMap();
            Dictionary<string, int> wcMap = this.getWorkCenterMap();
            Dictionary<string, int> mfgMap = this.getCageNiinATCMap();
            Dictionary<string, int> niinAtcMap = this.getNiinMapByATC();

            string test = "";

            //Create SQL Commands for use in inserting and updating.
            string insertInvStmt = "INSERT into inventory (shelf_life_expiration_date, location_id, qty, serial_number, bulk_item," +
                                        "mfg_catalog_id, manufacturer_date, ati, alternate_ui, alternate_um, batch_number, lot_number, contract_number, cosal) " +
                                        "values (@shelf_life_expiration_date, @location_id, @qty, @serial_number, @bulk_item, @mfg_catalog_id, @manufacturer_date, @ati, @alternate_ui, @alternate_um," +
                                        "@batch_number, @lot_number, @contract_number, @cosal)";

            //Opening the connection.
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            SqlCommand insertCmd = new SqlCommand(insertInvStmt, conn);
            insertCmd.Transaction = tran;

            /////
            //Create SQL Commands for use in inserting and updating.
            string insertLocStmt = "INSERT into mfg_catalog (cage, manufacturer, niin_catalog_id, PRODUCT_IDENTITY) values (@cage, @manufacturer, @niin_catalog_id, @PRODUCT_IDENTITY); SELECT SCOPE_IDENTITY()";
            string selectLocStmt = "SELECT mfg_catalog_id from mfg_catalog where cage = @cage AND niin_catalog_id=@niin_catalog_id";

            string selectMsdsCageStmt = "SELECT CAGE from MSDS where MSDSSERNO = @msds AND niin=@niin";
            string selectAnyMsdsStmt = "SELECT TOP 1 CAGE from MSDS where MSDSSERNO = @msds";
            string selectAnyMsdsStmt2 = "SELECT TOP 1 CAGE from MSDS where niin=@niin";

            SqlCommand insertLocCmd = new SqlCommand(insertLocStmt, conn);
            SqlCommand selectLocCmd = new SqlCommand(selectLocStmt, conn);
            insertLocCmd.Transaction = tran;
            selectLocCmd.Transaction = tran;

            SqlCommand selectMsdsCmd = new SqlCommand(selectMsdsCageStmt, conn);
            SqlCommand selectAnyMsdsCmd = new SqlCommand(selectAnyMsdsStmt, conn);
            SqlCommand selectAnyMsdsCmd2 = new SqlCommand(selectAnyMsdsStmt2, conn);
            selectMsdsCmd.Transaction = tran;
            selectAnyMsdsCmd.Transaction = tran;
            selectAnyMsdsCmd2.Transaction = tran;

            /* First create a list of column headers
             * since we know the first row is always headers.
             */
            string[] headers = itemList[0];

            foreach(string[] row in itemList)  
            {
                

                DateTime? shelf_life_expiration_date =null;
                int? location_id = null;
                int qty = 0;
                int? mfg_catalog_id = null;
                DateTime? manufacturer_date = null;
                string batchlot_number = null;
                string contract_number = null;
                int? workcenter_id = null;
                string niin = null;
                string atc = null;
                string cage = null;
                string msds = null;

                string manufacturer = null;
                int? niin_catalog_id = null;
                string cosal = null;

                //The first row of the list will always be HEADERS.
                if (itemList[0] != row)  {

                    for (int i = 0; i < row.Length; i++)
                    {
                        if (headers[i].ToUpper().Equals("ATC")) {
                            /* Use a location map to determine the location id.
                             * Use a NIIN/CAGE Map to capture the niin_catalog_id.
                             * Then using this execute a select statement using the 
                             * new niin_catalog_id to capture the mfg_catalog_id                            * 
                             */
                            atc = row[i];
                            

                            if (atc.Equals("1"))
                                cosal = "HME";
                            else if (atc.Equals("3"))
                                cosal = "MAM";
                            else if (atc.Equals("2"))
                                cosal = "Q";
                            else if (atc.Equals("5"))
                                cosal = "SW";
                            else if (atc.Equals("6"))
                                cosal = "NW";
                            else if (atc.Equals("7"))
                                cosal = "BH";
                            else if (atc.Equals("8"))
                                cosal = "OSI";
                            else if (atc.Equals("4"))
                                cosal = "RSS";
                            else if (atc.Equals("9"))
                                cosal = "LAMPS";

                            atc = "";
                           
                        }
                        else if (headers[i].ToUpper().Equals("COSAL"))
                        {
                            cosal = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("NIIN"))
                        {
                            niin = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("CAGE"))
                        {
                            cage = this.trimToMax(row[i], 5);
                            if (cage.Equals("") || cage.Equals("ZEROS"))
                                cage = "00000";
                        }
                        else if (headers[i].ToUpper().Equals("MFG"))
                        {
                            manufacturer = row[i];
                            if (manufacturer.Equals(""))
                                manufacturer = "UNKNOWN";
                        }
                        else if (headers[i].ToUpper().Equals("MSDS #"))
                        {
                            msds = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("S/L DATE"))
                        {
                            string slDate = row[i];

                            try
                            {
                                shelf_life_expiration_date = DateTime.Parse(slDate);
                            }
                            catch { }

                        }
                        else if (headers[i].ToUpper().Equals("MFD"))
                        {
                            string mfDate = row[i];
                            try
                            {
                                manufacturer_date = DateTime.Parse(mfDate);
                            }
                            catch { }
                        }
                        else if (headers[i].ToUpper().Equals("ACTUAL QTY"))
                        {
                            try
                            {
                                qty = Convert.ToInt32(row[i]);
                            }
                            catch { }
                        }
                        else if (headers[i].ToUpper().Equals("BATCH/LOT#"))
                        {
                            batchlot_number = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("CONTRACT #"))
                        {
                            contract_number = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("LOC"))
                        {
                            if (locMap.ContainsKey(row[i]))
                                location_id = locMap[row[i]];
                            else
                                location_id = null;
                        }
                        else if (headers[i].ToUpper().Equals("W/C"))
                        {
                            if (wcMap.ContainsKey(row[i]))
                                workcenter_id = wcMap[row[i]];
                            else
                            {
                                if (wcMap.ContainsKey("NONE"))
                                    workcenter_id = wcMap["NONE"];
                                else
                                    workcenter_id = null;
                            }
                        }
                    }//end for each header

                    //get mfg_catalog_id
                    string key = niin + cage + atc;
                    if (mfgMap.ContainsKey(key))
                        mfg_catalog_id = mfgMap[key];
                    else
                    {
                        mfg_catalog_id = null;
                        

                        //mfg catalog not found. See if the msds is in the database. If so, get the correct cage.
                        selectMsdsCmd.Parameters.Clear();
                        selectMsdsCmd.Parameters.AddWithValue("@msds", msds);
                        selectMsdsCmd.Parameters.AddWithValue("@niin", niin);
                        string msds_cage = Convert.ToString(selectMsdsCmd.ExecuteScalar());

                        if (msds_cage != null && !msds_cage.Equals(""))
                        {
                            cage = msds_cage;
                            //try the key with the correct cage
                            key = niin + cage + atc;
                            if (mfgMap.ContainsKey(key))
                                mfg_catalog_id = mfgMap[key]; 
                        }

                        if (mfg_catalog_id == null)
                        {

                            //mfg catalog not found. See if the msds or niin match the db. If so, get the correct cage.
                            selectAnyMsdsCmd.Parameters.Clear();
                            selectAnyMsdsCmd.Parameters.AddWithValue("@msds", msds);
                            //selectAnyMsdsCmd.Parameters.AddWithValue("@niin", niin);
                            msds_cage = Convert.ToString(selectAnyMsdsCmd.ExecuteScalar());

                            if (msds_cage != null && !msds_cage.Equals(""))
                            {
                                cage = msds_cage;
                                //try the key with the correct cage
                                key = niin + cage + atc;
                                if (mfgMap.ContainsKey(key))
                                    mfg_catalog_id = mfgMap[key];
                            }

                        }

                        if (mfg_catalog_id == null)
                        {

                           /* //mfg catalog not found. See if the msds or niin match the db. If so, get the correct cage.
                            selectAnyMsdsCmd2.Parameters.Clear();
                            //selectAnyMsdsCmd.Parameters.AddWithValue("@msds", msds);
                            selectAnyMsdsCmd2.Parameters.AddWithValue("@niin", niin);
                            msds_cage = Convert.ToString(selectAnyMsdsCmd2.ExecuteScalar());

                            if (msds_cage != null && !msds_cage.Equals(""))
                            {
                                cage = msds_cage;
                                //try the key with the correct cage
                                key = niin + cage + atc;
                                if (mfgMap.ContainsKey(key))
                                    mfg_catalog_id = mfgMap[key];
                            }*/

                        }

                        //If the msds/niin is not found, insert an mfg_catalog record anyway.
                        if (mfg_catalog_id == null)
                        {
                            count++;
                            test += "       [niin: " + niin + " cage:" + cage + " atc:" + atc + " MSDS#:" + msds + "] ";

                            if (niinAtcMap.ContainsKey(niin + atc))
                                niin_catalog_id = niinAtcMap[niin + atc];
                            else if (niinAtcMap.ContainsKey(niin + 0))
                                niin_catalog_id = niinAtcMap[niin + 0];
                            else
                            {
                                niin_catalog_id = null;                               
                            }



                            //Verify that this mfg_catalog is not already in the db.
                            selectLocCmd.Parameters.Clear();
                            selectLocCmd.Parameters.AddWithValue("@cage", cage);
                            selectLocCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                            Object id = selectLocCmd.ExecuteScalar();

                            if (id == null)
                            {
                                //insert location
                                insertLocCmd.Parameters.Clear();
                                insertLocCmd.Parameters.AddWithValue("@cage", cage);
                                insertLocCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                                insertLocCmd.Parameters.AddWithValue("@manufacturer", manufacturer);
                                insertLocCmd.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(null));
                                insertLocCmd.ExecuteNonQuery();
                                //mfg_catalog_id = Convert.ToInt32(id);
                            }
                            else
                                mfg_catalog_id = Convert.ToInt32(id);

                        }
                    }//end else if mfg_catalog_id is null


                    //check location/workcenter id
                    if (workcenter_id == null)
                    {
                        //SqlCommand wcCmd = new SqlCommand("insert into workcenter(wid, description)VALUES(@wid, @description); SELECT SCOPE_IDENTITY();", conn);
                        //wcCmd.Transaction = tran;
                        //wcCmd.Parameters.AddWithValue("@wid", dbNull("NONE"));
                        //wcCmd.Parameters.AddWithValue("@description", dbNull("UNKNOWN"));
                        //Object o = wcCmd.ExecuteScalar();
                        //wcCmd.Parameters.Clear();
                        //workcenter_id = Convert.ToInt32(o);

                        //SqlCommand locCmd = new SqlCommand("insert into locations(name, workcenter_id)VALUES(@name, @workcenter_id); SELECT SCOPE_IDENTITY();", conn);
                        //locCmd.Transaction = tran;
                        //locCmd.Parameters.AddWithValue("@name", dbNull("NEWLY ISSUED"));
                        //locCmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));
                        //locCmd.ExecuteNonQuery();
                        //locCmd.Parameters.Clear();
                    }
                    if (location_id == null)
                    {
                        SqlCommand locCmd = new SqlCommand("select location_id from locations WHERE name='NEWLY ISSUED' AND workcenter_id=@workcenter_id", conn);
                        locCmd.Transaction = tran;
                        locCmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));
                        Object o = locCmd.ExecuteScalar();
                        location_id = Convert.ToInt32(o);
                    }

                    //insert
                    insertCmd.Parameters.Clear();
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                    insertCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                    insertCmd.Parameters.AddWithValue("@qty", dbNull(qty));
                    insertCmd.Parameters.AddWithValue("@serial_number", dbNull(msds));
                    insertCmd.Parameters.AddWithValue("@bulk_item", dbNull(true));
                    insertCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                    insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                    insertCmd.Parameters.AddWithValue("@ati", dbNull(null));
                    insertCmd.Parameters.AddWithValue("@alternate_ui", dbNull(null));
                    insertCmd.Parameters.AddWithValue("@alternate_um", dbNull(null));
                    insertCmd.Parameters.AddWithValue("@batch_number", dbNull(batchlot_number));
                    insertCmd.Parameters.AddWithValue("@lot_number", dbNull(batchlot_number));
                    insertCmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));
                    insertCmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
                    insertCmd.ExecuteNonQuery();
                    
                }//end if
            }

            try
            {
                tran.Commit();
                System.Diagnostics.Debug.WriteLine("Transaction Committed");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Transaction Committed with error: " + ex.Message);
                conn.Close();
            }

            conn.Close();
        }


        private string trimToMax(string value, int maxLength)
        {

            if (value != null && value.Length > maxLength)
            {
                value = value.Substring(0, maxLength);
            }

            return value;
        }

        /* Methods for connecting to and a db value insertion
         * validation methods.
         */
        #region Database Methods
        protected string getConnectionString(string host, int port, string user, string pass) {
            return "Data Source=" + host + ", " + port + ";Initial Catalog=hazmattest;Integrated Security=False;User Id=" + user + ";PWD= " + pass + ";Min pool size=5;Max pool size=100;connection timeout=15;pooling=yes";
        }

        protected SqlConnection createConnection(string host, int port, string user, string pass)  {
            SqlConnection conn = new SqlConnection(this.getConnectionString(host, port, user, pass));
            return conn;
        }

        /* Method created by Adam Hale to check a value before db insertion.
         * If this object is blank or null it replaces it with a DBNull value.
         */
        private object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            } else {
                return DBNull.Value;
            }
        }
        #endregion

        #region LOGGING
        // Method for storing errors encountered during the File Parsing process.
        private void writeToErrorLog(FileInfo file, String errMsg)
        {
            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine(date + "\t" + errMsg);
            sw.Close();
        }

        private void createLogFile()
        {
            String path = "";
            string currentDate = System.DateTime.Now.ToString("MM-dd-yyyy");

            try
            {
                //Grab the path to the Application Data folder for this user.
                path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path += "\\HILT";

                //If the defined path does not exist, create it.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                //Stamp the folder with the current date.
                path += "\\niin_catalog_import_" + currentDate;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                path += "\\logs";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);                    
                }

                this.inventoryLog = new FileInfo(path + @"\inventoryErrorLog.txt");

            }
            catch (Exception ex)
            {
                throw new ImportException("Error occurred when creating log file:  " + ex.Message);
            }
        }
        #endregion

        // Methods for creating Dictionary Mapping Tables.
        #region Mapping Tables
         public Dictionary<string, int> getMap(String selectStmt)
        {
            Dictionary<string, int> map = new Dictionary<string, int>();
            conn.Open();
            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            SqlDataReader reader = cmdRead.ExecuteReader();

            while (reader.Read())
            {
                if (!map.ContainsKey("" + reader.GetValue(0)))
                    map.Add("" + reader.GetValue(0), Int32.Parse(""+reader.GetValue(1)));
            }

            reader.Dispose();
            reader.Close();
            conn.Close();
            return map;
        }

        public Dictionary<string, int> getMapforDuplicates(String selectStmt)
        {
            //Initialize Variables.
            Dictionary<string, int> map = new Dictionary<string, int>();

            //Open connection for use.
            conn.Open();

            //Initialize some SQL variables for reading data.
            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            SqlDataReader reader = cmdRead.ExecuteReader();

            while (reader.Read()) {
                if (!map.ContainsKey("" + reader.GetValue(0) + reader.GetValue(1)))
                    map.Add("" + reader.GetValue(0) + reader.GetValue(1), Int32.Parse("" + reader.GetValue(2)));
            }

            //Closing variables.
            reader.Dispose();
            reader.Close();
            conn.Close();

            return map;
        }
       
        public Dictionary<string, int> getNiinATCMap()
        {
            string selectStmt = "Select NIIN, COSAL, niin_catalog_id from NIIN_Catalog";
            return getMapforDuplicates(selectStmt);
        }
        public Dictionary<string, int> getNiinMapByATC()
        {
            string selectStmt = "Select NIIN, '', niin_catalog_id from NIIN_Catalog";
            return getMapforDuplicates(selectStmt);
        }
        public Dictionary<string, int> getCageNiinATCMap()
        {
            string selectStmt = "Select NIIN + CAGE, '' , mfg_catalog_id from  Mfg_Catalog m  INNER JOIN NIIN_Catalog n ON (n.niin_catalog_id=m.niin_catalog_id) order by niin, cage";
            return getMapforDuplicates(selectStmt);
        }

        public Dictionary<string, int> getWorkCenterMap(){
            string selectStmt = "Select wid, workcenter_id from workcenter";
            return getMap(selectStmt);
        }

        public Dictionary<string, int> getLocationMap(){
            string selectStmt = "Select name, location_id from locations";
            return getMap(selectStmt);
        }
        #endregion

        //Getters and Setters.
        #region Getters/Setters
        public string UploadFilePath
        {
            get { return uploadFilePath; }
            set { uploadFilePath = value; }
        }

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string ServerPath
        {
            get { return serverPath; }
            set { serverPath = value; }
        }
        #endregion
    }
}










       