﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HILT_IMPORTER
{
    public partial class splash : Form
    {
        public splash()
        {
            InitializeComponent();
            ControlBox = false;
            MaximizeBox = false;
            MinimizeBox = false;
            ShowIcon = false;
            this.CenterToScreen();
            this.FormBorderStyle = FormBorderStyle.None;
            this.MinimumSize = this.Size;
            this.MaximumSize = this.Size;
        }
    }
}
