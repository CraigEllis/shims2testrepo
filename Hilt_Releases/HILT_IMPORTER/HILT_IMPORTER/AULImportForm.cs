﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections;
using System.Threading;
using HILT_IMPORTER.DataObjects;

namespace HILT_IMPORTER {
    public partial class AULImportForm : HILT_IMPORTER.BaseImportForm {
        Boolean m_doPreview = true; 
        AULFile m_FileData = null;
        AULImporter.AUL_TYPE m_aulType;
        String m_vesselType = "";
        AULImporter m_importer = null;
        long m_hull_type_id = 0;

        public AULImportForm() {
            InitializeComponent();

            // Set the label
            Title = "Authorized Use List Import";
            Text = "AUL Import";
            messageRTBox.Text = "";
        }

        long m_ProgressBarMaxValue = 100;
        public long ProgressBarMaxValue {
            get {
                return m_ProgressBarMaxValue;
            }
            set {
                m_ProgressBarMaxValue = value;
            }
        }

        protected override void processCancelButton_Click(object sender, EventArgs e) {
            if (m_processing) {
                // Cancel the background task
                aulBackgroundWorker.CancelAsync();
            } 
            else {
                // Clear the data and reset the status message 
                this.clearData();

                messageRTBox.Text = "";
                m_parentMDI.progressBar.Value = 0;
                m_parentMDI.StatusBarLabel_Value = "Ready";
                setCursors(Cursors.Default);
            }
        }

        protected override void processFileButton_Click(object sender, EventArgs e) {
            // CLear any exsting messages
            if (m_parentMDI == null)
                m_parentMDI = (HiltImportMDIForm)MdiParent;
            m_parentMDI.StatusBarLabel_Value = "Ready";

            // Display the file dialog to select a file to import
            if (aulFileDialog.ShowDialog() == DialogResult.OK) {
                fileTextBox.Text = aulFileDialog.FileName;
                
                // Clear out any existing file data
                m_FileData = null;
                messageRTBox.Text = "";
            }
        }

        protected override void processImportButton_Click(object sender, EventArgs e) {
            m_processing = true;
            DateTime processTime;

            // CLear any exsting messages
            m_parentMDI.StatusBarLabel_Value = "Ready";

            if (m_parentMDI == null)
                m_parentMDI = (HiltImportMDIForm)MdiParent;

            // Make sure we have file data
            if (m_FileData == null) {
                // Run the preview
                previewButton_Click(sender, e);

                if (m_FileData.DataRows.Count == 0)
                    return;
            }

            // Check the preview data to see if we have any warnings
            if (messageRTBox.Text.Contains("ERROR")) {
                return;
            }
            else if (messageRTBox.Text.Contains("INITIAL LOAD")) {
                DialogResult btnResp = MessageBox.Show(
                    "The database does not currently contain records for this unit type.\n" +
                    "Importing this file will create an Authorized Use List for this unit type." +
                    "\n\tClick the Yes button to continue loading this file",
                    "Authorzied Use List Initial Load", MessageBoxButtons.YesNo);

                if (btnResp == System.Windows.Forms.DialogResult.No)
                    return;
            }
            else if (messageRTBox.Text.Contains("WARNING")) {
                DialogResult btnResp = MessageBox.Show(
                    "The import file contains records that may result in an incomplete load " +
                    "or invalid values for this unit type.\n" +
                    "See the message box for details on these warnings." +
                    "\n\tClick the Yes button to continue loading this file",
                    "Warning - Authorzied Use List File Issues", MessageBoxButtons.YesNo);

                if (btnResp == System.Windows.Forms.DialogResult.No)
                    return;
            }

            // Load the file data
            previewButton.Enabled = false;
            setCursors(Cursors.WaitCursor);

            processTime = DateTime.Now;
            m_parentMDI.StatusBarLabel_Value = "Importing File...";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("\n****************************************");
            sb.AppendLine("Importing " + m_FileData.FileType + " for: " + m_vesselType);
            sb.AppendLine("\tFile: " + m_FileData.FileName);
            sb.AppendLine("\tStart Time: " + processTime.ToString());

            messageRTBox.Text += sb.ToString();
            Application.DoEvents();
            // Log a Message
            DatabaseManager.logMessage(DatabaseManager.LOG_LEVELS.Info, "***** AUL Import Started: " +
                processTime.ToString() + " File: " + m_FileData.FileName + " *****");

            //TODO Add backgroundnworker reference to constructor
            m_importer = new AULImporter();

            // Execute importer as a background thread
            m_doPreview = false;
            // Set the progress bar max value
            m_ProgressBarMaxValue = m_FileData.DataRows.Count;
            aulBackgroundWorker.RunWorkerAsync(m_doPreview);

            while (aulBackgroundWorker.IsBusy) {
                Application.DoEvents();
            }
        }

        private void previewButton_Click(object sender, EventArgs e) {
            // CLear any exsting messages
            m_parentMDI.StatusBarLabel_Value = "Ready";
            messageRTBox.Text = "";

            if (m_parentMDI == null)
                m_parentMDI = (HiltImportMDIForm)MdiParent;
            Cursor.Current = Cursors.WaitCursor;
            messageRTBox.Text = "";
            Application.DoEvents();

            // Disable the Import button
            importButton.Enabled = false;
            if (!m_processing)
                clearData();

            long nStartTick = Environment.TickCount;

            if (readFile()) {
                long nEndTick = Environment.TickCount;

                // Execute preview as a background thread
                m_doPreview = true;
                m_parentMDI.StatusBarLabel_Value = "Analyzing File...";
                // Set the progress bar max value
                m_ProgressBarMaxValue = m_FileData.DataRows.Count;

                aulBackgroundWorker.RunWorkerAsync(m_doPreview);

                while (aulBackgroundWorker.IsBusy) {
                    Application.DoEvents();
                }
            }
        }

        private bool readFile() {
            bool validFile = true;
            StringBuilder sb = new StringBuilder();

            if (m_parentMDI == null)
                m_parentMDI = (HiltImportMDIForm)MdiParent; 

            // Check for valid Excel extension
            AULImporter.FILE_TYPE fileType = AULImporter.validateFileExtension(fileTextBox.Text);
            switch (fileType) {
                case AULImporter.FILE_TYPE.INVALID:
                    sb.AppendLine("Not a valid Excel file - Please save the file as an Excel 97-2003 or 2007 Workbook.");
                    validFile = false;
                    break;
                case AULImporter.FILE_TYPE.NO_FILENAME:
                    sb.AppendLine("No file name provided, please select a file to upload.");
                    validFile = false;
                    break;
            }

            if (validFile) {
                // Read the Excel file to gather the file type
                m_parentMDI.StatusBarLabel_Value = "Reading file...";
                Application.DoEvents();
                m_FileData = new ExcelParser().parseExcel(fileTextBox.Text);
                m_parentMDI.StatusBarLabel_Value = "Ready";
                Application.DoEvents();

                // Make sure this is a valid SMCL or SHML
                m_aulType = AULImporter.determineFileType(m_FileData.HeaderRecord);
                m_FileData.FileType = m_aulType;

                if (m_aulType == AULImporter.AUL_TYPE.INVALID) {
                    sb.AppendLine("Not a valid Authorized Use List file.");
                    validFile = false;
                }
                else if (m_aulType == AULImporter.AUL_TYPE.AEL) {
                    // Not supported yet
                    sb.AppendLine("Allowance Equipage List (AEL) files are not supported in this version.");
                    validFile = false;
                }
                else if (m_aulType == AULImporter.AUL_TYPE.SHML) {
                    String patVesselType = "NIINM_(\\w+)";
                    if (Regex.IsMatch(m_FileData.HeaderRecord[AULImporter.SHML_COL_MAP["NIINM_TYPE"]], patVesselType)) {
                        Match match = Regex.Match(m_FileData.HeaderRecord[AULImporter.SHML_COL_MAP["NIINM_TYPE"]], patVesselType);
                        m_vesselType = match.Groups[1].Value;
                    }
                }
                else {
                    // Its a SMCL - set the vesseltype to Submarines
                    m_vesselType = "SUBS";
                }
            }

            if (validFile) {
                generatePreviewHeader(sb);

                sb.Append("\tVessel Type: ");
                sb.Append(m_vesselType);

                DatabaseManager manager = new DatabaseManager();

                // Check the hull Type - Error if it doesn't exist in the database
                if (!manager.checkHullTypeExists(m_vesselType)) {
                    sb.AppendLine("\n\n****************************************");
                    sb.AppendLine("ERROR - The vessel type (" +
                        m_vesselType + ") is not a valid type. ");
                    sb.AppendLine("\tPlease add the vessel type to the HILT Hub database.");
                    sb.AppendLine("\n****************************************");

                    validFile = false;
                    // Disable the Import button
                    importButton.Enabled = false;
                }
                else {
                    // Get the hull_type_id for the file
                    m_hull_type_id = manager.getIDFromValue("hull_types", "hull_type_id",
                        "hull_type", m_vesselType);

                    // Make sure the NIINs are all 9 characters long
                    AULImporter.normalizeNIINs(m_FileData);
                }
            }

            messageRTBox.Text = sb.ToString();

            if (validFile) {
                m_parentMDI.StatusBarLabel_Value = "Ready";
            }
            else {
                // Problems opening file
                m_parentMDI.StatusBarLabel_Visible = true;
                m_parentMDI.StatusBarCount_Visible = false;
                m_parentMDI.ProgressBar_Visible = false;

                m_parentMDI.StatusBarLabel_Value = "Errors Reading File";
            }

            return validFile;
        }

        private void generateSHMLPreview(BackgroundWorker worker) {
            StringBuilder sb = new StringBuilder();

            long nStartTick = Environment.TickCount;

            sb.Append(AULImporter.analyzeAULFile(m_FileData, m_hull_type_id, worker));

            long nEndTick = Environment.TickCount;

            // Run the data validation for the SHML
            processSHMLDataPreview(sb);

            worker.ReportProgress(m_FileData.DataRows.Count, sb.ToString());
        }

        private void generateSMCLPreview(BackgroundWorker worker) {
            StringBuilder sb = new StringBuilder();

            long nStartTick = Environment.TickCount;

            sb.Append(AULImporter.analyzeAULFile(m_FileData, m_hull_type_id, worker));

            long nEndTick = Environment.TickCount;

            // Run the data validation for the SMCL
            processSMCLDataPreview(sb);

            worker.ReportProgress(m_FileData.DataRows.Count, sb.ToString());
        }

        private void processAELDataPreview(StringBuilder sb) {
            StringBuilder sbMsg = new StringBuilder();

            // Hashtables for counts & DB checks
            Hashtable htNiin = new Hashtable();
            Hashtable htAllowed = new Hashtable();
            Hashtable htCosal = new Hashtable();
            Hashtable htUi = new Hashtable();
            Hashtable htCog = new Hashtable();
            Hashtable htFsc = new Hashtable();

            // Check datbase for missing values
            DatabaseManager mgr = new DatabaseManager();

            long nStartTick = Environment.TickCount;

            // Loop through the raw data
            foreach (String[] data in m_FileData.DataRows) {
                htNiin[data[AULImporter.AEL_COL_MAP["NIIN"]]] = null;
                htCosal[data[AULImporter.SHML_COL_MAP["ANC"]]] = null;
                htAllowed[data[AULImporter.SHML_COL_MAP["QTY"]]] = null;
                htUi[data[AULImporter.SHML_COL_MAP["UOI"]]] = null;
                htCog[data[AULImporter.SHML_COL_MAP["COG"]]] = null;
                htFsc[data[AULImporter.SHML_COL_MAP["FSC"]]] = null;
            }

            // TODO - 
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_COG"],
                htCog, mgr));

            if (sbMsg.Length > 0) {
                // Add a warning header then display the details
                sb.Append("\nWARNING - The following keywords are not in the database:");
                sb.AppendLine(sbMsg.ToString());
            }

            long nEndTick = Environment.TickCount;
        }

        private void processSHMLDataPreview(StringBuilder sb) {
            StringBuilder sbMsg = new StringBuilder();

            // Hashtables for counts & DB checks
            Hashtable htNiin = new Hashtable();
            Hashtable htAcc = new Hashtable();
            Hashtable htAllowed = new Hashtable();
            Hashtable htUsageCategory = new Hashtable();  // QUP??
            Hashtable htUi = new Hashtable();
            Hashtable htUm = new Hashtable();
            Hashtable htSlc = new Hashtable();
            Hashtable htSlac = new Hashtable();
            Hashtable htStorageType = new Hashtable();
            Hashtable htSmcc = new Hashtable();
            Hashtable htSpmig = new Hashtable();
            Hashtable htCog = new Hashtable();
            Hashtable htFsc = new Hashtable();
            Hashtable htSmic = new Hashtable();

            // Check datbase for missing values
            DatabaseManager mgr = new DatabaseManager();

            long nStartTick = Environment.TickCount;

            // Loop through the raw data
            foreach (String[] data in m_FileData.DataRows) {
                htNiin[data[AULImporter.SHML_COL_MAP["NIINM_NIIN"]]] = null;
                htUsageCategory[data[AULImporter.SHML_COL_MAP["NIINM_QUP"]]] = null;
                htAcc[data[AULImporter.SHML_COL_MAP["NIINM_AAC"]]] = null;
                htAllowed[data[AULImporter.SHML_COL_MAP["NIINM_ALLOWED_OB"]]] = null;
                htUi[data[AULImporter.SHML_COL_MAP["NIINM_UI"]]] = null;
                htUm[data[AULImporter.SHML_COL_MAP["NIINM_UM"]]] = null;
                htSlc[data[AULImporter.SHML_COL_MAP["NIINM_SHELF_LIFE"]]] = null;
                htSlac[data[AULImporter.SHML_COL_MAP["NIINM_SLAC"]]] = null;
                htStorageType[data[AULImporter.SHML_COL_MAP["NIINM_TYPE_STOR"]]] = null;
                htSpmig[data[AULImporter.SHML_COL_MAP["NIINM_SPMIG"]]] = null;
                htSmcc[data[AULImporter.SHML_COL_MAP["NIINM_SMCC"]]] = null;
                htCog[data[AULImporter.SHML_COL_MAP["NIINM_COG"]]] = null;
                htFsc[data[AULImporter.SHML_COL_MAP["NIINM_FSC"]]] = null;
                htSmic[data[AULImporter.SHML_COL_MAP["NIINM_SMIC"]]] = null;
            }

            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_ALLOWED_OB"],
                htStorageType, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_COG"], 
                htCog, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_SMCC"],
                htSmcc, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_SHELF_LIFE"],
                htSlc, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_SLAC"],
                htSlac, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SHML, AULImporter.SHML_COL_MAP["NIINM_TYPE_STOR"],
                htStorageType, mgr));

            if (sbMsg.Length > 0) {
                // Add a warning header then display the details
                sb.Append("\nWARNING - The following keywords are not in the database:");
                sb.AppendLine(sbMsg.ToString());
            }

            long nEndTick = Environment.TickCount;
        }

        private void processSMCLDataPreview(StringBuilder sb) {
            StringBuilder sbMsg = new StringBuilder();

            // Hashtables for counts & DB checks
            Hashtable htNiin = new Hashtable();
            Hashtable htNiin_Cage = new Hashtable();
            Hashtable htUsageCategory = new Hashtable();
            Hashtable htSmcc = new Hashtable();
            Hashtable htMilspec = new Hashtable();
            Hashtable htUi = new Hashtable();
            Hashtable htUm = new Hashtable();
            Hashtable htSlc = new Hashtable();
            Hashtable htSlac = new Hashtable();
            Hashtable htStorageType = new Hashtable();
            Hashtable htSpmig = new Hashtable();
            Hashtable htCog = new Hashtable();
            Hashtable htFsc = new Hashtable();
            Hashtable htSmic = new Hashtable();

            // Loop through the raw data
            foreach (String[] data in m_FileData.DataRows) {
                htNiin[data[AULImporter.SMCL_COL_MAP["NIIN"]].ToUpper()] = null;
                htNiin_Cage[data[AULImporter.SMCL_COL_MAP["NIIN"]].ToUpper() + "_"
                    + data[AULImporter.SMCL_COL_MAP["CAGE"]].ToUpper()] = null;
                htUsageCategory[data[AULImporter.SMCL_COL_MAP["USAGE_CATEGORY"]].ToUpper()] = null;
                htSmcc[data[AULImporter.SMCL_COL_MAP["SMCC"]].ToUpper()] = null;
                htMilspec[data[AULImporter.SMCL_COL_MAP["MILSPEC"]]] = null;
                htUi[data[AULImporter.SMCL_COL_MAP["UI"]].ToUpper()] = null;
                htUm[data[AULImporter.SMCL_COL_MAP["UM"]].ToUpper()] = null;
                htSlc[data[AULImporter.SMCL_COL_MAP["SLC"]].ToUpper()] = null;
                htSlac[data[AULImporter.SMCL_COL_MAP["SLAC"]].ToUpper()] = null;
                htStorageType[data[AULImporter.SMCL_COL_MAP["TYPE_OF_STORAGE"]].ToUpper()] = null;
                htSpmig[data[AULImporter.SMCL_COL_MAP["SPMIG"]].ToUpper()] = null;
                htCog[data[AULImporter.SMCL_COL_MAP["COG"]].ToUpper()] = null;
                htFsc[data[AULImporter.SMCL_COL_MAP["FSC"]].ToUpper()] = null;
                htSmic[data[AULImporter.SMCL_COL_MAP["SMIC"]].ToUpper()] = null;
            }

            // Check datbase for missing values
            DatabaseManager mgr = new DatabaseManager();

            long nStartTick = Environment.TickCount;

            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SMCL, AULImporter.SMCL_COL_MAP["USAGE_CATEGORY"],
            htUsageCategory, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SMCL, AULImporter.SMCL_COL_MAP["COG"],
                htCog, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SMCL, AULImporter.SMCL_COL_MAP["SMCC"],
                htSmcc, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SMCL, AULImporter.SMCL_COL_MAP["SLC"],
                htSlc, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SMCL, AULImporter.SMCL_COL_MAP["SLAC"],
                htSlac, mgr));
            sbMsg.Append(checkForMissingValues(AULImporter.AUL_TYPE.SMCL, AULImporter.SMCL_COL_MAP["TYPE_OF_STORAGE"],
                htStorageType, mgr));

            if (sbMsg.Length > 0) {
                // Add a warning header then display the details
                sb.Append("\nWARNING - The following keywords are not in the database:");
                sb.AppendLine(sbMsg.ToString());
            }

            long nEndTick = Environment.TickCount;
        }

        private void generatePreviewHeader(StringBuilder sb) {
            sb.AppendLine("****************************************");
            sb.AppendLine("File Preview ");
            sb.Append("\tFile Name: ");
            sb.AppendLine(fileTextBox.Text);
            sb.Append("\t# of Records: ");
            sb.Append(String.Format("{0:G}", m_FileData.DataRows.Count));
            sb.Append("\t\t# of Data columns: ");
            sb.AppendLine(String.Format("{0:G}", m_FileData.HeaderRecord.Length));
            sb.Append("\tFile Type: ");
            sb.Append(m_aulType);
        }

        private String checkForMissingValues(AULImporter.AUL_TYPE aulType, int col,
                Hashtable ht, DatabaseManager mgr) {
            String result = "";
            StringBuilder sb = new StringBuilder();
            String colName = null;
            Hashtable existingValues = null;

            switch (aulType) {
                case AULImporter.AUL_TYPE.SHML:
                    if (col == AULImporter.SHML_COL_MAP["NIINM_ALLOWED_OB"]) {
                        colName = "Usage Category";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "usage_category", "category");
                    }
                    else if (col == AULImporter.SHML_COL_MAP["NIINM_COG"]) {
                        colName = "COG";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "cog_codes", "cog");
                    }
                    else if (col == AULImporter.SHML_COL_MAP["NIINM_SHELF_LIFE"]) {
                        colName = "SLC";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "shelf_life_code", "slc");
                    }
                    else if (col == AULImporter.SHML_COL_MAP["NIINM_SLAC"]) {
                        colName = "SLAC";
                        existingValues = mgr.getKeywordHashtableForTable(
                                "shelf_life_action_code", "slac");
                    }
                    else if (col == AULImporter.SHML_COL_MAP["NIINM_SMCC"]) {
                        colName = "SMCC";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "smcc", "smcc");
                    }
                    else if (col == AULImporter.SHML_COL_MAP["NIINM_TYPE_STOR"]) {
                        colName = "Storage Type";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "storage_type", "type");
                    }
                    break;
                case AULImporter.AUL_TYPE.SMCL:
                    if (col == AULImporter.SMCL_COL_MAP["USAGE_CATEGORY"]) {
                        colName = "Usage Category";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "usage_category", "category");
                    }
                    else if (col == AULImporter.SMCL_COL_MAP["SMCC"]) {
                        colName = "SMCC";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "smcc", "smcc");
                    }
                    else if (col == AULImporter.SMCL_COL_MAP["SLC"]) {
                        colName = "SLC";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "shelf_life_code", "slc");
                    }
                    else if (col == AULImporter.SMCL_COL_MAP["SLAC"]) {
                        colName = "SLAC";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "shelf_life_action_code", "slac");
                    }
                    else if (col == AULImporter.SMCL_COL_MAP["TYPE_OF_STORAGE"]) {
                        colName = "Storage Type";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "storage_type", "type");
                    }
                    else if (col == AULImporter.SMCL_COL_MAP["COG"]) {
                        colName = "COG";
                        existingValues = mgr.getKeywordHashtableForTable(
                            "cog_codes", "cog");
                    }
                    break;
            }

            // Run through the list and find the missing values
            int nCount = 0; 
            foreach (String value in ht.Keys) {
                Application.DoEvents();

                if (!existingValues.ContainsKey(value.ToUpper()) && value.Length > 0) {
                    if (nCount == 0) {
                        sb.Append("\t" + value + ", ");
                    }
                    else if (nCount % 10 == 0) {
                        sb.Append("\n\t" + value.ToUpper() + ", ");
                    }
                    else {
                        sb.Append(value.ToUpper() + ", ");
                    }

                    nCount++;
                }
            }
            if (sb.Length > 0) {
                result = "\n" +colName + ":\n"
                    + sb.ToString();
            }

            return result;
        }

        private void clearData() {
            m_aulType = AULImporter.AUL_TYPE.INVALID;
            m_FileData = null;
            m_hull_type_id = 0;
            m_vesselType = null;
        }

        //**********************************************************************
        // BackgroundWorker Import Stuff
        private void aulBackgroundWorker_DoWork(object sender, DoWorkEventArgs e) {
            if (m_doPreview) {
                // Generate the preview info
                //AULImporter.analyzeAULFile(m_FileData, m_hull_type_id, aulBackgroundWorker);
                switch (m_aulType) {
                    case AULImporter.AUL_TYPE.SHML:
                        generateSHMLPreview(aulBackgroundWorker);
                        break;
                    case AULImporter.AUL_TYPE.SMCL:
                        generateSMCLPreview(aulBackgroundWorker);
                        break;
                }
            }
            else {
                // Run the import
                try {
                    //Cursor.Current = Cursors.AppStarting;
                    m_importer.import(m_FileData, m_vesselType, aulBackgroundWorker);
                }
                //TODO clean this up to use the messageBox
                //catch (ImportException ex) {
                //    MessageBox.Show(ex.Message, "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                catch (Exception ex) {
                    MessageBox.Show(ex.Message, "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void aulBackgroundWorker_ProgressChanged_1(object sender, ProgressChangedEventArgs e) {
            // Update the value of the ProgressBar and the count label to the BackgroundWorker progress.
            m_parentMDI.StatusBarCount_Value = e.ProgressPercentage;

            // change the count to a percentage for the progress bar
            double percent = (e.ProgressPercentage * 100.0) / m_ProgressBarMaxValue;
            m_parentMDI.StatusBarProgress_Value = (int) percent;

            if (e.UserState != null) {
                String msg = e.UserState.ToString();
                // See whether we write this to the message text box, or the status bar
                if (msg.StartsWith("SB_")) {
                    m_parentMDI.StatusBarLabel_Value = msg.Substring(3);
                }
                else {
                    messageRTBox.Text += "\n" + msg;
                }
            }
        }

        private void aulBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (m_doPreview) {
                // Enable the Import button
                importButton.Enabled = true;
            }
            else {
                // Add the end time to the message box
                DateTime processTime = DateTime.Now;
                messageRTBox.Text += "\tEnd Time: " + processTime.ToString();

                // Log a Message
                DatabaseManager.logMessage(DatabaseManager.LOG_LEVELS.Info, "***** AUL Import Finished: " +
                    processTime.ToString() + " *****");
                // Delete the current fileData
                clearData();

                m_processing = false;

                //Scroll the text box to the bottom
                messageRTBox.SelectionStart = messageRTBox.Text.Length;
                messageRTBox.ScrollToCaret();

                previewButton.Enabled = true;
            }

            // Reset things
            Cursor.Current = Cursors.Default;
            setCursors(Cursors.Default);

            m_parentMDI.progressBar.Value = 0;
            m_parentMDI.StatusBarLabel_Value = "Ready";
        }

    }
}
