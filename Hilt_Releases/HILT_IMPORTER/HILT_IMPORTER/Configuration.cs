﻿using System.Configuration;

namespace HILT_IMPORTER {
    public class Configuration {

        public enum UPLOAD_TYPES {
            AUL,
            MSDS,
            MSSL
        };

        public enum DOWNLOAD_TYPES {
            AUL,
            MSDS,
            MSSL
        };

        public enum FILE_CHANGES {
            Initial_Load,
            Threshold_Exceeded,
            Acceptable_Changes
        };

        public static string ConnectionInfo {
            get {
                return Properties.Settings.Default.ConnectionInfo.Replace("\\\\", "\\");
                //return ConfigurationManager.ConnectionStrings["hilt_hubConnectionString"].ConnectionString;
            }
        }

        public static string HILT_HUB_DataFolder {
            get {
                return Properties.Settings.Default.HILT_HUB_DataFolder;
                //return ConfigurationManager.AppSettings["HILT_HUB_DataFolder"];
            }
        }

        public static string CDROM_DataFolder {
            get {
                return Properties.Settings.Default.CDFolderName;
            }
        }

        public static string MSDS_DocumentFolder {
            get {
                return Properties.Settings.Default.HILT_HUB_DataFolder + "\\MSDS_Docs";
            }
        }
    }
}