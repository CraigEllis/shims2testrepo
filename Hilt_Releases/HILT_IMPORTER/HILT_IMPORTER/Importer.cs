﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace HILT_IMPORTER
{
    public class Importer
    {
        #region Variable Declaration
        private string cdOnlinePath;
        private string serverPath;
        private string username;
        private string password;
        private string tempPath = "";
        private string msdsPath = "";
        private string[] filePaths;

        private List<Record> recordsMSDS = new List<Record>();
        
        private FileInfo importErrorFile;
        private FileInfo parserErrorFile;
        private FileInfo exportErrorFile;
        private FileInfo parsedRecordsFile;
        private FileInfo recordsInsertedFile;
        
        private Boolean validRecord;
        private Boolean validInsert;
        private Boolean errorFlag = false;

        private int validRecordCount = 0;
        private int invalidRecordCount = 0;
        private int totalRecords = 0;
        private int recordsInserted = 0;
        private int port;
        private const int batchSize = 30;
        private int exportProgressBar = 0;
        private int importProgessBar = 0;

        private ImportListener listener;

        private SqlGenerator sqlGenerator = new SqlGenerator();
        private SqlConnection conn;
        #endregion

        //Constructor
        #region Constructor
        public Importer(ImportListener listener)
        {
            this.listener = listener;
        }
        #endregion

        public void import()
        {

            listener.updateStatus("Connecting to HILT Database...");
            listener.updateProgress(++this.ExportProgressBar);

            //Test the connection the user has provided for us.
            #region HILT DB Connection Test
            try {
                conn = createConnection(this.serverPath, this.port, this.username, this.password);
                conn.Open();
                conn.Close();
            } catch (Exception) {
                throw new ImportException("Could not connect to the HILT database with the specified host: " + this.ServerPath + " port: " + 
                                            this.Port + " Username: " + this.Username + " Password: " + this.Password);                
            }
            listener.updateProgress(2);
            #endregion

            /* PHASE 1 - Export Process
             * Connect to the HMIRS DB.  Extract all Product Serial Numbers
             * that pertain to MSDS Documents.
             * Create Batch Flat Files from the exported serial numbers.
             */
            #region EXPORTPROCESS
            /*
            // Set up the environment.
            // Create a temporary directory under the users
            // Application Data dir to store our data. 
            listener.updateStatus("Creating temporary directories...");

            // Update variables with the updated path.
            // Create log folder and files.
            this.tempPath = this.createTempDirectory();
            this.MsdsPath = this.tempPath + @"\msds";
            this.createLogDir();
            
            // Update Progress Bar
            this.ExportProgressBar += 4;
            listener.updateProgress(this.ExportProgressBar);

            // Query the HMIRS Databse, Pipe results to a txt file
            // Notify the user.
            // Fire up onlinecd.exe.                 
            listener.updateStatus("Starting CD Online...");
            OnlineCDProcess prcsOnlineCD = this.startOnlineCD();
            
            // Update progress bar.
            this.ExportProgressBar += 5;
            listener.updateProgress(this.ExportProgressBar);

            //Export all Product Serial Numbers contained within the database.
            listener.updateStatus("Exporting Product Serial Numbers from HMIRS database...");
            System.Diagnostics.Process prcsHMIRSExporter = this.exportProdSerialNums(this.tempPath);
            
            //Update progress bar.
            this.ExportProgressBar += 5;
            listener.updateProgress(this.ExportProgressBar);

            //Once the export results have been retrieved, kill the associated processes.
            prcsOnlineCD.MainProcess.Kill();
            prcsOnlineCD.SubProcess.Kill();

            //Move the exported Product Serial Numbers file to a new location.....
            listener.updateStatus("Moving around some files...");
            System.Diagnostics.Process prcsMoveFile = this.fileMover();

            //Update progress bar.
            this.ExportProgressBar += 5;
            listener.updateProgress(this.ExportProgressBar);

            // Count and clean up the text file, notify user
            // Split full count into batch sizes, update progess bar
            // for the user 
            listener.updateStatus("Creating Batch Flat Files...");
            batchFileCount = this.createBatchFlatFiles(tempPath);

            //Update progress bar.
            this.ExportProgressBar += 5;
            listener.updateProgress(this.ExportProgressBar);

            // Using the newly created Batch Flat Files, create
            // all associated MSDS files.
            listener.updateStatus("Exporting MSDS files...");
            DialogResult reply = MessageBox.Show("You are about to begin the MSDS Export Process.  This can take upwards of 12 hours.  " +
                                                 "Please press OK to continue.", "MSDS Export Process", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // Wait for user to accept the notification before beginning.
            if (reply == DialogResult.OK) {
                this.createMSDS(batchFileCount);
            }

            //Update the progress bar to signify end of Export Process.
            listener.updateStatus("The export process has ended, preparing to begin the import process.");

            //Update the progress bar to show we are done.
            if (this.ExportProgressBar < 100) {
                this.ExportProgressBar = 100;
                listener.updateProgress(this.ExportProgressBar);
            }

            //Give it time to catch up and notify the user.
            Thread.Sleep(3000);
           */
            #endregion

            /* #PHASE 2 IMPORT DATA
             * ***********************
             * Open and parse each exported, (value added), file from Online CD. 
             * Get the count, update the progress bar.
             * Insert data into the HILT DB and log all errors 
             * that occur during the process.
             */
            
            #region IMPORT PROCESS
            
            // Set Progress bar back to 0 to show we have begun the import process.
            // Wait a couple seconds so user can see the change in processes.            
            listener.updateStatus("Beginning Import Process...");
            listener.updateProgress(this.ImportProgessBar);
            Thread.Sleep(2000);

            // TEST TEST 
            // This are hardcoded paths for testing purposes only.
            // These paths are only valid for already created temp directories.
            // Thus you would need to point this to an already created msds temporary export directory.
            this.TempPath = "C:\\Importer";
            this.MsdsPath = this.TempPath + @"\msds";
            this.importErrorFile = new FileInfo(this.MsdsPath + "\\logs\\importErrorLog.txt");
            this.parserErrorFile = new FileInfo(this.MsdsPath + "\\logs\\parserErrorLog.txt");
            this.parsedRecordsFile = new FileInfo(this.MsdsPath + "\\logs\\parsedRecordsStats.txt");
            this.recordsInsertedFile = new FileInfo(this.MsdsPath + "\\logs\\recordInsertStats.txt");

            // Grab all of the .txt files in the specified directory.
            // These .txt files are the MSDS records with information. 
            filePaths = Directory.GetFiles(this.MsdsPath, "*.txt");

            //Update progress bar.
            this.ImportProgessBar += 5;
            listener.updateProgress(this.ImportProgessBar);

            // Loop through the files and parse each one, 
            // inserting its records into the HILT DB.
            // Since we can have MSDS files that are .txts
            // we check for underscores, since all non MSDS record files
            // will contain at least one underscore.   
            listener.updateStatus("Parsing Files...");
            foreach (String path in filePaths) {
                if (!Path.GetFileName(path).Contains("_")) {
                    this.parseFile(path);
                }                     
            }

            // Update progress bar.
            this.ImportProgessBar += 15;
            listener.updateProgress(this.ImportProgessBar);


            // Now that we have a List of records, it is time to loop through these records, 
            // and insert values into the HILT DB.
            listener.updateStatus("Inserting Records...");
            this.insertRecords(recordsMSDS);

            //Update the progress bar.
            if (this.ImportProgessBar < 100) {
                this.ImportProgessBar = 100;
                listener.updateProgress(this.ImportProgessBar);
            }
            
            #endregion

            // If connection isn't closed, do so.
            if (this.conn.State == System.Data.ConnectionState.Open) {
                this.conn.Close();
            }

            // Notify the user we are done.
            DialogResult dbFinished = MessageBox.Show("Congratulations, you are finished!!", "HILT Import Process", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // Terminate the program after user presses ok.
            if (dbFinished == DialogResult.OK) {
                Application.Exit();
            }
            
        }


        #region Methods
        #region Export Process Methods.
        /* Create temporary directory within the application in which to store MSDS exports.
         * Return the path used for later use in deleting the tmp folder.
         * */
        #region CreateTempDirectory()
        protected String createTempDirectory()
        {
            string path = "";
            string currentDate = System.DateTime.Now.ToString("MM-dd-yyyy");

            try {
                //Grab the path to the Application Data folder for this user.
                path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path += "\\HILT";

                //If the defined path does not exist, create it.
                if (!Directory.Exists(path)) {
                    Directory.CreateDirectory(path);
                }

                //Stamp the folder with the current date.
                path += "\\import_" + currentDate;

                if (!Directory.Exists(path)) {
                    Directory.CreateDirectory(path);
                }

                return path;
            } catch (Exception) {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("Well this is embarassing.  We were unable to create a temporary folder: " + path +
                                          "Please try again and contact support if this problem persists.");
            }
        }
        #endregion

        /* Create directory within the temporary directory for storing
         * log files.  Also initiate these log files for use.
         */
        #region createLogDir()
        private void createLogDir()
        {
            string logFolderPath = this.MsdsPath + @"\logs";

            try {
                // Check to see if the msds path exists, if it does not
                // create it.
                if (!Directory.Exists(this.MsdsPath)) {
                    Directory.CreateDirectory(MsdsPath);
                }

            } catch (Exception) {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("Well this is embarassing.  We were unable to create a temporary folder: " + this.MsdsPath +
                                          "Please try again and contact support if this problem persists.");
            }

            try {
                // Now do the same for the log directory.
                // Initialize the log files.
                if (!Directory.Exists(logFolderPath)) {
                    Directory.CreateDirectory(logFolderPath);
                    this.ImportErrorFile = new FileInfo(logFolderPath + @"\importErrorLog.txt");
                    this.ParserErrorFile = new FileInfo(logFolderPath + @"\parserErrorLog.txt");
                    this.ExportErrorFile = new FileInfo(logFolderPath + @"\exporterErrorLog.txt");
                    this.ParsedRecordsFile = new FileInfo(logFolderPath + @"\parsedRecordsStats.txt");
                    this.RecordsInsertedFile = new FileInfo(logFolderPath + @"\recordInsertStats.txt");
                }
            } catch (Exception) {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("Well this is embarassing.  We were unable to create a temporary folder: " + logFolderPath +
                                          "Please try again and contact support if this problem persists.");
            }

        }
        #endregion

        //Checks that the cdonline.exe exists and if so executes it.
        #region startOnlineCD()
        protected OnlineCDProcess startOnlineCD()
        {
            //Variable declaration.
            bool found = false;

            //Create a new onlinecd process
            OnlineCDProcess onlineCDProcess = new OnlineCDProcess();
            string cdOnlineExec = this.cdOnlinePath + "\\onlinecd.exe";

            if (File.Exists(cdOnlineExec)) {
                try {
                    /* Create the ProcessStartInfo using "cmd" as the program to be run,
                     * and "/c " as the parameters.
                     * /c tells cmd that we want it to execute the command that follows,
                     * and then exit.  In this case, execute cdonline.exe
                     */
                    System.Diagnostics.ProcessStartInfo cdOnlineInfo =
                        new System.Diagnostics.ProcessStartInfo("cmd", "/c " + "onlinecd.exe");

                    /* The following commands are needed to redirect the standard output.
                     * This means that it will be redirected to the Process.StandardOutput StreamReader.
                     * We also set that we do not want to see a shell prompt during execution.
                     */
                    cdOnlineInfo.RedirectStandardOutput = true;
                    cdOnlineInfo.UseShellExecute = false;

                    // Do not create the black window.
                    cdOnlineInfo.CreateNoWindow = true;

                    //Change the working directory to cdonline.exe's current directory.
                    cdOnlineInfo.WorkingDirectory = this.cdOnlinePath;

                    // Now we create a process, assign its ProcessStartInfo and start it
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = cdOnlineInfo;
                    proc.Start();

                    //Capture the Parent process.
                    onlineCDProcess.MainProcess = proc;

                    /* Check to see if the onlinecd.exe process has started.  This method checks
                     * all running processes GUI title boxes and waits until it sees the Online CDROM box.  This
                     * will signal that the process is ready to proceed.
                     */
                    while (!found) {
                        foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
                        {
                            if (!process.MainWindowTitle.Equals("")) {
                                if (process.MainWindowTitle.StartsWith("Online CDROM")) {
                                    found = true;
                                    onlineCDProcess.SubProcess = process;
                                }
                            }
                        }

                        //Let the process sleep so as to not put to much stress on processor.
                        Thread.Sleep(2000);
                    }

                    return onlineCDProcess;
                }
                catch (Exception) {
                    //Not currently logging these errors.  We want the system to fail if the
                    //default installation fails.
                    throw new ImportException("Oops!  It appears an error occurred when invoking " + cdOnlineExec + ".  Please check your Online CD path and try again.");
                }

            } else {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("The file " + cdOnlineExec + " cannot be found.  Please check your Online CD path and try again.  " +
                                            "This path should point to where the cdonline.exe file lives.");
            }
        }
        #endregion

        /* Accepts the user's specified CDOnline path and finds the DBISQLC.exe file.
         * Then uses said file to export a list of Product Serial Numbers from the 
         * HMIRS database.
         */ 
        #region exportProdSerialNums(String)
        protected System.Diagnostics.Process exportProdSerialNums(String tempDirPath)
        {
            string hmirsDBPath = this.cdOnlinePath + "\\DBISQLC.exe";

            if (File.Exists(hmirsDBPath)) 
            {
                //Create a command to be used.
                //****************************
                //Name of the file that will store exported Serial Numbers.
                string exportFileLoc = "exportResults.txt";
                //SQL command to be executed.
                string sqlCommand = "SELECT DOC_GROUP_NUM FROM msdsonline_dbo.document_index";
                //The name of the file that will store our executed SQL command.
                string exportSQLPath = tempDirPath + "\\hmirExporter.sql";

                //Create the SQL file and store the SQL command in it.
                FileInfo t = new FileInfo(exportSQLPath);
                StreamWriter sw = t.CreateText();
                this.sqlGenerator.generate(sqlCommand, exportFileLoc, sw);
                sw.Close();

                //Create the command line executeble statement to be run.
                string dbCmd = "DBISQLC.exe" + " -q -c \"uid=dba;pwd=sql;\" \"" + exportSQLPath + "\"";

                try {
                    /* Create the ProcessStartInfo using "cmd" as the program to be run,
                     * and "/c " as the parameters.
                     * Incidentally, /c tells cmd that we want it to execute the command that follows,
                     * and then exit.  In this case, execute cdonline.exe and pass it our arguments
                     * specified above.
                     */
                    System.Diagnostics.ProcessStartInfo hmirsDBInfo =
                        new System.Diagnostics.ProcessStartInfo("cmd", "/c " + dbCmd);

                    // Do not create the black window.
                    hmirsDBInfo.CreateNoWindow = true;

                    //Change the working directory to cdonline.exe's current directory.
                    hmirsDBInfo.WorkingDirectory = this.cdOnlinePath;

                    // Now create a process and assign its ProcessStartInfo.  Start it
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = hmirsDBInfo;
                    proc.Start();

                    //Give process time to execute
                    Thread.Sleep(5000);

                    //Return the process for later use.
                    return proc;
                } catch (Exception) {
                    //Not currently logging these errors.  We want the system to fail if the
                    //default installation fails.
                    throw new ImportException("We were unable to probably invoke the file: " + hmirsDBPath + "." +
                                               "  Please try again and contact support if this problem persists.");
                }

            } else {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("We were unable to find the file:  " + hmirsDBPath + ".  Please check your Online CD path and try again.  " +
                                            "This path should point to where the cdonline.exe file lives.");
            }
        }
        #endregion

        /* Move the exportResults.txt file to the temp directory.  The file has to initially be placed in the
         * working directory in order for the DB export process to work properly.
         */
        #region fileMover()
        protected System.Diagnostics.Process fileMover() 
        {
            string currentExportFilePath = this.cdOnlinePath + "\\exportResults.txt";
            string exportFileNewLocation = this.tempPath;

            if (File.Exists(currentExportFilePath))
            {
                //Create the command.
                String mvcmd = "MOVE \"" + currentExportFilePath + "\" \"" + exportFileNewLocation + "\"";

                try {
                    /* Create the ProcessStartInfo using "cmd" as the program to be run,
                     * and "/c " as the parameters.
                     * /c tells cmd that we want it to execute the command that follows,
                     * and then exit.  In this case, execute moving a file.
                     */
                    System.Diagnostics.ProcessStartInfo mvExportInfo =
                        new System.Diagnostics.ProcessStartInfo("cmd", "/c " + mvcmd);

                    // Do not create the black window.
                    mvExportInfo.CreateNoWindow = true;

                    //Change the working directory to cdonline.exe's current directory.
                    mvExportInfo.WorkingDirectory = this.cdOnlinePath;

                    // Now we create a process, assign its ProcessStartInfo and start it
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = mvExportInfo;
                    proc.Start();

                    //Give process time to execute
                    Thread.Sleep(10000);

                    //Return the process for later use.
                    return proc;
                } catch (Exception) {
                    //Not currently logging these errors.  We want the system to fail if the
                    //default installation fails.
                    throw new ImportException("Oops!  It appears we had trouble moving the file: " + currentExportFilePath + " to " + exportFileNewLocation + "." +
                                               "  Please try again and contact support if this problem persists.");
                }
            } else {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("We were unable to find the file:  " + currentExportFilePath + ".  Please check your Online CD path and try again.  " +
                                            "This path should point to where the cdonline.exe file lives.");
            }
        }
        #endregion

        /* Method accepts the exported Prod Serial Numbers file 
         * and breaks it into multiple batch flat files based on the 
         * constant var batchsize.  Returns the number of batch flat
         * files created for later use.
         */
        #region createBatchFlatFiles(String)
        protected int createBatchFlatFiles(string dir)
        {
            //Declare variables.
            int lineCount = 0;
            int currentLine = 0;
            int currentBatch = 0;
            string dirPath = dir + "\\exportResults.txt";
            string line = null;

            //Create a file reader and writer
            StreamReader reader;
            StreamWriter writer = null;

            //Try opening the file.
            try {
                reader = new StreamReader(dirPath);
            } catch (IOException) {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("It appears we cannot find an export file at " + dir + "\\exportResults.txt" +
                                          "Please contact support if this happens again.");
            }

            // First pass, count the number of Product Serial numbers 
            // stored in the export file.
            while (reader.ReadLine() != null)
            { lineCount++; }

            // Close the reader and  then reopen it 
            // for the second pass through.
            reader.Close();
            reader = new StreamReader(dirPath);

            /* Progress through the file.  Whenever we read the number
             * of lines stored in the const int BatchSize, close the writer
             * and open a new file.  In this manner we create multiple batch
             * flat files.  This helps negate the slowdown experienced in exporting
             * thousands of records at once.  We also remove the 's that are seen
             * throughout the Product Serial Export File.
             */
            while ((line = reader.ReadLine()) != null)
            {
                try {
                    /* If the currentLine number is divisible by 
                     * BatchSize, close the reader and create a
                     * new batch flat file.
                     */
                    if (currentLine % this.BatchSize == 0) {
                        /* If the batchSize has been reached, close the writer, and reopen it 
                         * with an incremented batch file number.
                         */
                        if (writer != null) {
                            writer.Close();
                        }

                        writer = new StreamWriter(dir + "\\export_batch" + (currentBatch + 1) + ".txt");
                        currentBatch++;
                    }

                    // For each line, remove the 's that surround
                    // each Serial Number.
                    int idx = -1;
                    while ((idx = line.IndexOf('\'')) != -1) {
                        line = line.Remove(idx, 1);
                    }

                    writer.WriteLine(line);

                    currentLine++;
                }
                catch (Exception) {
                    //Not currently logging these errors.  We want the system to fail if the
                    //default installation fails.
                    throw new ImportException("Well this is embarassing.  It appears an error has occurred during the Batch Flat File " +
                                              "creation process.  Please try again and contact support if this problem persists.");
                }
            }

            //Close the writer
            writer.Close();

            return currentBatch;
        }
        #endregion

        /* Initializes the MSDS file creation process.  Invokes exportMSDS to create
         * MSDS's for the specified Product Serial Numbers in each Batch Flat File.
         */
        #region createMSDS(int)
        protected void createMSDS(int count)
        {
            // Determine when the progress bar should be updated.
            int updateProgessBar = count / (100 - this.ExportProgressBar);

            /* For each batch flat file, create an ini file, an export file, 
             * run cdonline.exe, and update progress
             * Confirm the text file exists and that the contents are what we expect.
             * Notify the user of progress.
             */
            IniGenerator gen = new IniGenerator();
            System.Diagnostics.Process prcsMSDSExport;

            try {
                for (int i = 0; i < count; i++) {
                    //Initialize file paths.
                    string inputFile = this.TempPath + "\\" + "export_batch" + (i + 1) + ".txt";
                    string outputFile = this.MsdsPath + "\\" + "msds" + (i + 1) + ".txt";

                    //Initialize the ini file and open it up for writing/updating.
                    FileInfo iniFile = new FileInfo(this.MsdsPath + @"\hilt.ini");
                    StreamWriter sw = iniFile.CreateText();
                    gen.generate("DBA", "SQL", inputFile, outputFile, "Delimited", "Proprietary", "Serial Number", sw);
                    sw.Close();

                    //Launch the export process for this batch flat file.
                    prcsMSDSExport = this.exportMSDS(this.MsdsPath + @"\hilt.ini");

                    /* TESTING PURPOSES TESTTEST
                     * Use this to create subsets of data.  Tell it to only
                     * grab x number of batch flat files for MSDS creation.
                     */
                     // if (i == 5) {
                     //     break;
                     // }

                    //Update the progress bar every so many records.
                    if (i % updateProgessBar == 0) {
                        listener.updateProgress(++this.ExportProgressBar);
                    }
                }
            } catch (Exception ex) {
                this.writeToExportErrLog(this.ExportErrorFile, "An error occurred while exporting MSDS files.  Error:  " + ex.Message);
            }
        }
        #endregion

        /* Accepts the .ini file for a Batch Flat File and uses that to 
         * execute a batch command that pulls all MSDS information
         * for a specified Product Serial Number from the HMIRS database.
         */        
        #region exportMSDS(String)
        protected System.Diagnostics.Process exportMSDS(String iniFilePath) {
            // Initialize variables.
            System.Diagnostics.Process proc = null;

            // Check that the .ini file exists.
            if (File.Exists(iniFilePath)) {
                //Create command.
                String cmdMSDSExport = "onlinecd.exe" + " -d " + "\"" + iniFilePath + "\"";

                try {
                    /* Create the ProcessStartInfo using "cmd" as the program to be run,
                     * and then exit.  In this case, execute a MSDS export
                     */
                    System.Diagnostics.ProcessStartInfo msdsExportInfo =
                        new System.Diagnostics.ProcessStartInfo("cmd", "/c " + cmdMSDSExport);

                    // Do not create the black window.
                    msdsExportInfo.CreateNoWindow = true;

                    //Change the working directory to cdonline.exe's current directory.
                    msdsExportInfo.WorkingDirectory = this.cdOnlinePath;

                    // Now we create a process, assign its ProcessStartInfo and start it
                    proc = new System.Diagnostics.Process();
                    proc.StartInfo = msdsExportInfo;
                    proc.Start();

                    //Check to see if the process has finished and exited.
                    while (!proc.HasExited) {
                        Thread.Sleep(5000);
                    }

                } catch (Exception ex) {
                    this.writeToExportErrLog(this.ExportErrorFile, "Exception occurred during the MSDS export process.  Error:  " + ex.Message);
                }

            } else {
                this.writeToExportErrLog(this.ExportErrorFile, "The following file could not be located: " + iniFilePath);
            }

            //Return the process for later use.
            return proc;
        }
        #endregion
        #endregion

        #region Import Process Methods

        /* Accepts a filename as an argument.  
         * Parses through the tab delimited file
         * extracting information.
         */
        #region parseFile(String)
        protected void parseFile(String filename)
        {
            // Initialize variables.
            Record record;
            System.IO.StreamReader sr = new System.IO.StreamReader(filename);

            // Array that will contain each records tab delimited values.
            String[] values;

            //TODO
            // Using this line, capture an array of headers for checking
            // index validity.
            string line = sr.ReadLine();
            //this.checkMsdsFileFormat();

            /**********************
             *  PARSE THE FILE  *
            ***********************/
            #region FILEPARSER

            // Grab the first line of data to parse.
            line = sr.ReadLine();

            while (line != null) {
                // Initialize variables.
                record = new Record();

                // Update the total number of records.
                // Set the record to valid for the time being.
                totalRecords++;
                this.ValidRecord = true;

                //Establish the delimiter.
                values = line.Split('\t');

                #region parser if/else
                // For each value, add it to the new record for insertion
                // into the HILT DB.
                for (int i = 0; i < values.Length; i++)
                {
                    try
                    {
                        /* Check to be sure no null records have been inserted.
                         * I have seen HMIRS spit out blank/null/garbage records, we do not 
                         * want this in our DB.  ARTICLE_IND is NEVER empty and if it is
                         * the record is invalid.
                         */
                        if (values[0] == "" || values[0].Length > 1)
                        {
                            this.writeToErrorLog(filename, record.MsdsSerNo, this.parserErrorFile, "Invalid Record. Article IND is invalid:  " + values[0]);
                            this.ValidRecord = false;
                            this.ErrorFlag = true;
                            this.invalidRecordCount++;
                            break;
                        }

                        /* Add values to the record.
                        * Based on the index, we add informtion to the Record object.
                        * Our File Format check above, allows us to be sure that
                        * each value index corresponds to the correct Record field.
                         */
                        if (i == 0) {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "") {
                                record.Article_ind = Convert.ToChar(values[i]);
                            }

                        }
                        else if (i == 1)
                        {
                            record.Rpcage = values[i];
                        }
                        else if (i == 2)
                        {
                            record.Manufacturer = values[i];
                        }
                        else if (i == 3)
                        {
                            record.Description = values[i];
                        }
                        else if (i == 4)
                        {
                            record.Emergency_tel = values[i];
                        }
                        else if (i == 5)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.End_item_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 6)
                        {                                   
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.End_comp_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 7)
                        {
                            record.Fsc = values[i];
                        }
                        else if (i == 8)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Kit_ind = Convert.ToChar(values[i]);
                            }

                        }
                        else if (i == 9)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Kit_part_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 10)
                        {
                            record.Manufacturer_msds_no = values[i];
                        }
                        else if (i == 11)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Mixture_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 12)
                        {
                            record.Niin = values[i];
                        }
                        else if (i == 13)
                        {
                            record.PartNo = values[i];
                        }
                        else if (i == 14)
                        {
                            record.Product_identity = values[i];
                        }
                        else if (i == 15)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Product_ind = Convert.ToChar(values[i]);
                            }

                        }
                        else if (i == 16)
                        {
                            record.Product_language = values[i];
                        }
                        else if (i == 17)
                        {
                            if (values[i] == "" || values[i] == null)
                            {
                                record.Product_load_date = null;
                            }
                            else
                            {
                                record.Product_load_date = Convert.ToDateTime(values[i]);
                            }
                        }
                        else if (i == 18)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Product_record_status = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 19)
                        {
                            record.Product_revision_no = values[i];
                        }
                        else if (i == 20)
                        {
                            record.MsdsSerNo = values[i];
                        }
                        else if (i == 21)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Proprietary_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 22)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Published_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 23)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Purchased_prod_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 24)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Pure_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 25)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Radioactive_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 26)
                        {
                            record.Service_agency_code = values[i];
                        }
                        else if (i == 27)
                        {
                            record.Trade_name = values[i];
                        }
                        else if (i == 28)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Trade_secret_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 29)
                        {
                            record.Percent_vol_volume = values[i];
                        }
                        else if (i == 30)
                        {
                            record.App_odor = values[i];
                        }
                        else if (i == 31)
                        {
                            record.Autoignition_temp = values[i];
                        }
                        else if (i == 32)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Carcinogen_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 33)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Epa_acute = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 34)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Epa_chronic = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 35)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Epa_fire = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 36)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Epa_pressure = Convert.ToChar(values[i]);
                            }

                        }
                        else if (i == 37)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Epa_reactivity = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 38)
                        {
                            record.Evap_rate_ref = values[i];
                        }
                        else if (i == 39)
                        {
                            record.Flash_point_temp = values[i];
                        }
                        else if (i == 40)
                        {
                            record.Neut_agent = values[i];
                        }
                        else if (i == 41)
                        {
                            record.Nfpa_flammability = values[i];
                        }
                        else if (i == 42)
                        {
                            record.Nfpa_health = values[i];
                        }
                        else if (i == 43)
                        {
                            record.Nfpa_reactivity = values[i];
                        }
                        else if (i == 44)
                        {
                            record.Nfpa_special = values[i];
                        }
                        else if (i == 45)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_carcinogens = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 46)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_comb_liquid = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 47)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_comp_gas = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 48)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_corrosive = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 49)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_explosive = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 50)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_flammable = Convert.ToChar(values[i]);
                            }

                        }
                        else if (i == 51)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_high_toxic = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 52)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_irritant = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 53)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_org_perox = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 54)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_otherlongterm = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 55)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_oxidizer = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 56)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_pyro = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 57)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_sensitizer = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 58)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_toxic = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 59)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_unst_react = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 60)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Osha_water_reac = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 61)
                        {
                            record.Other_short_term = values[i];
                        }
                        else if (i == 62)
                        {
                            record.Ph = values[i];
                        }
                        else if (i == 63)
                        {
                            record.Phys_state_code = values[i];
                        }
                        else if (i == 64)
                        {
                            record.Sol_in_water = values[i];
                        }
                        else if (i == 65)
                        {
                            record.Specific_grav = values[i];
                        }
                        else if (i == 66)
                        {
                            record.Vapor_dens = values[i];
                        }
                        else if (i == 67)
                        {
                            record.Vapor_press = values[i];
                        }
                        else if (i == 68)
                        {
                            record.Viscosity = values[i];
                        }
                        else if (i == 69)
                        {
                            record.Voc_grams_liter = values[i];
                        }
                        else if (i == 70)
                        {
                            record.Voc_pounds_gallon = values[i];
                        }
                        else if (i == 71)
                        {
                            record.Vol_org_comp_wt = values[i];
                        }
                        else if (i == 72)
                        {
                            record.Prcnt = values[i];
                        }
                        else if (i == 73)
                        {
                            record.Prcnt_vol_value = values[i];
                        }
                        else if (i == 74)
                        {
                            record.Prcnt_vol_weight = values[i];
                        }
                        else if (i == 75)
                        {
                            record.Acgih_stel = values[i];
                        }
                        else if (i == 76)
                        {
                            record.Acgih_tlv = values[i];
                        }
                        else if (i == 77)
                        {
                            record.Cas = values[i];
                        }
                        else if (i == 78)
                        {
                            record.Chem_mfg_comp_name = values[i];
                        }
                        else if (i == 79)
                        {
                            record.Ingredient_name = values[i];
                        }
                        else if (i == 80)
                        {
                            record.Dot_report_qty = values[i];
                        }
                        else if (i == 81)
                        {
                            record.Epa_report_qty = values[i];
                        }
                        else if (i == 82)
                        {
                            record.Ods_ind = values[i];
                        }
                        else if (i == 83)
                        {
                            record.Osha_pel = values[i];
                        }
                        else if (i == 84)
                        {
                            record.Osha_stel = values[i];
                        }
                        else if (i == 85)
                        {
                            record.Other_rec_limits = values[i];
                        }
                        else if (i == 86)
                        {
                            record.Rtecs_num = values[i];
                        }
                        else if (i == 87)
                        {
                            record.Rtecs_code = values[i];
                        }
                        else if (i == 88)
                        {
                            record.Ct_number = values[i];
                        }
                        else if (i == 89)
                        {
                            record.Ct_cage = values[i];
                        }
                        else if (i == 90)
                        {
                            record.Ct_city = values[i];
                        }
                        else if (i == 91)
                        {
                            record.Ct_company_name = values[i];
                        }
                        else if (i == 92)
                        {
                            record.Ct_country = values[i];
                        }
                        else if (i == 93)
                        {
                            record.Ct_po_box = values[i];
                        }
                        else if (i == 94)
                        {
                            record.Ct_state = values[i];
                        }
                        else if (i == 95)
                        {
                            record.Ct_phone = values[i];
                        }
                        else if (i == 96)
                        {
                            record.Purchase_order_no = values[i];
                        }
                        else if (i == 97)
                        {
                            record.Nrc_lp_num = values[i];
                        }
                        else if (i == 98)
                        {
                            record.TheOperator = values[i];
                        }
                        else if (i == 99)
                        {
                            record.Rad_amount_micro = values[i];
                        }
                        else if (i == 100)
                        {
                            record.Rad_form = values[i];
                        }
                        else if (i == 101)
                        {
                            record.Rad_cas = values[i];
                        }
                        else if (i == 102)
                        {
                            record.Rad_name = values[i];
                        }
                        else if (i == 103)
                        {
                            record.Rad_symbol = values[i];
                        }
                        else if (i == 104)
                        {
                            record.Rep_nsn = values[i];
                        }
                        else if (i == 105)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.IsSealed = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 106)
                        {
                            record.Af_mmac_code = values[i];
                        }
                        else if (i == 107)
                        {
                            record.Certificate_coe = values[i];
                        }
                        else if (i == 108)
                        {
                            record.Competent_caa = values[i];
                        }
                        else if (i == 109)
                        {
                            record.Dod_id_code = values[i];
                        }
                        else if (i == 110)
                        {
                            record.Dot_exemption_no = values[i];
                        }
                        else if (i == 111)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Dot_rq_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 112)
                        {
                            record.Ex_no = values[i];
                        }
                        else if (i == 113)
                        {
                            //Check to see if the field is blank
                            if (values[i] != "")
                            {
                                record.Flash_pt_temp = Convert.ToDouble(values[i]);
                            }
                        }
                        else if (i == 114)
                        {
                            record.Hcc = values[i];
                        }
                        else if (i == 115)
                        {
                            if (values[i] != "")
                            {
                                record.High_explosive_wt = Convert.ToDouble(values[i]);
                            }
                        }
                        else if (i == 116)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Ltd_qty_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 117)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Magnetic_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 118)
                        {
                            record.Magnetism = values[i];
                        }
                        else if (i == 119)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Marine_pollutant_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 120)
                        {
                            if (values[i] != "")
                            {
                                record.Net_exp_qty_dist = Convert.ToDouble(values[i]);
                            }
                        }
                        else if (i == 121)
                        {
                            record.Net_exp_weight = values[i];
                        }
                        else if (i == 122)
                        {
                            record.Net_propellant_wt = values[i];
                        }
                        else if (i == 123)
                        {
                            record.Nos_technical_shipping_name = values[i];
                        }
                        else if (i == 124)
                        {
                            record.Transportation_additional_data = values[i];
                        }
                        else if (i == 125)
                        {
                            record.Dot_hazard_class_div = values[i];
                        }
                        else if (i == 126)
                        {
                            record.Dot_hazard_label = values[i];
                        }
                        else if (i == 127)
                        {
                            record.Dot_max_cargo = values[i];
                        }
                        else if (i == 128)
                        {
                            record.Dot_max_passenger = values[i];
                        }
                        else if (i == 129)
                        {
                            record.Dot_pack_bulk = values[i];
                        }
                        else if (i == 130)
                        {
                            record.Dot_pack_exceptions = values[i];
                        }
                        else if (i == 131)
                        {
                            record.Dot_pack_nonbulk = values[i];
                        }
                        else if (i == 132)
                        {
                            record.Dot_pack_group = values[i];
                        }
                        else if (i == 133)
                        {
                            record.Dot_prop_ship_name = values[i];
                        }
                        else if (i == 134)
                        {
                            record.Dot_prop_ship_modifier = values[i];
                        }
                        else if (i == 135)
                        {
                            record.Dot_psn_code = values[i];
                        }
                        else if (i == 136)
                        {
                            record.Dot_special_provision = values[i];
                        }
                        else if (i == 137)
                        {
                            record.Dot_symbols = values[i];
                        }
                        else if (i == 138)
                        {
                            record.Dot_un_id_number = values[i];
                        }
                        else if (i == 139)
                        {
                            record.Dot_water_other_req = values[i];
                        }
                        else if (i == 140)
                        {
                            record.Dot_water_vessel_stow = values[i];
                        }
                        else if (i == 141)
                        {
                            record.Afjm_hazard_class = values[i];
                        }
                        else if (i == 142)
                        {
                            record.Afjm_pack_paragraph = values[i];
                        }
                        else if (i == 143)
                        {
                            record.Afjm_pack_group = values[i];
                        }
                        else if (i == 144)
                        {
                            record.Afjm_proper_ship_name = values[i];
                        }
                        else if (i == 145)
                        {
                            record.Afjm_proper_ship_modifier = values[i];
                        }
                        else if (i == 146)
                        {
                            record.Afjm_psn_code = values[i];
                        }
                        else if (i == 147)
                        {
                            record.Afjm_special_prov = values[i];
                        }
                        else if (i == 148)
                        {
                            record.Afjm_subsidiary_risk = values[i];
                        }
                        else if (i == 149)
                        {
                            record.Afjm_symbols = values[i];
                        }
                        else if (i == 150)
                        {
                            record.Afjm_un_id_number = values[i];
                        }
                        else if (i == 151)
                        {
                            record.Iata_cargo_packing = values[i];
                        }
                        else if (i == 152)
                        {
                            record.Iata_hazard_class = values[i];
                        }
                        else if (i == 153)
                        {
                            record.Iata_hazard_label = values[i];
                        }
                        else if (i == 154)
                        {
                            record.Iata_packing_group = values[i];
                        }
                        else if (i == 155)
                        {
                            record.Iata_pass_air_pack_lmt_instr = values[i];
                        }
                        else if (i == 156)
                        {
                            record.Iata_pass_air_pack_lmt_per_pkg = values[i];
                        }
                        else if (i == 157)
                        {
                            record.Iata_pass_air_max_qty = values[i];
                        }
                        else if (i == 158)
                        {
                            record.Iata_pass_air_pack_note = values[i];
                        }
                        else if (i == 159)
                        {
                            record.Iata_prop_ship_name = values[i];
                        }
                        else if (i == 160)
                        {
                            record.Iata_prop_ship_modifier = values[i];
                        }
                        else if (i == 161)
                        {
                            record.Iata_pass_cargo_pack_max_qty = values[i];
                        }
                        else if (i == 162)
                        {
                            record.Iata_psn_code = values[i];
                        }
                        else if (i == 163)
                        {
                            record.Iata_special_prov = values[i];
                        }
                        else if (i == 164)
                        {
                            record.Iata_subsidiary_risk = values[i];
                        }
                        else if (i == 165)
                        {
                            record.Iata_un_id_number = values[i];
                        }
                        else if (i == 166)
                        {
                            record.Imo_ems_no = values[i];
                        }
                        else if (i == 167)
                        {
                            record.Imo_hazard_class = values[i];
                        }
                        else if (i == 168)
                        {
                            record.Imo_ibc_instr = values[i];
                        }
                        else if (i == 169)
                        {
                            record.Imo_ibc_provisions = values[i];
                        }
                        else if (i == 170)
                        {
                            record.Imo_limited_qty = values[i];
                        }
                        else if (i == 171)
                        {
                            record.Imo_pack_group = values[i];
                        }
                        else if (i == 172)
                        {
                            record.Imo_pack_instructions = values[i];
                        }
                        else if (i == 173)
                        {
                            record.Imo_pack_provisions = values[i];
                        }
                        else if (i == 174)
                        {
                            record.Imo_prop_ship_name = values[i];
                        }
                        else if (i == 175)
                        {
                            record.Imo_prop_ship_modifier = values[i];
                        }
                        else if (i == 176)
                        {
                            record.Imo_psn_code = values[i];
                        }
                        else if (i == 177)
                        {
                            record.Imo_special_prov = values[i];
                        }
                        else if (i == 178)
                        {
                            record.Imo_stow_segr = values[i];
                        }
                        else if (i == 179)
                        {
                            record.Imo_subsidiary_risk = values[i];
                        }
                        else if (i == 180)
                        {
                            record.Imo_tank_instr_imo = values[i];
                        }
                        else if (i == 181)
                        {
                            record.Imo_tank_instr_provisions = values[i];
                        }
                        else if (i == 182)
                        {
                            record.Imo_tank_instr_un = values[i];
                        }
                        else if (i == 183)
                        {
                            record.Imo_un_number = values[i];
                        }
                        else if (i == 184)
                        {
                            record.Batch_number = values[i];
                        }
                        else if (i == 185)
                        {
                            record.Lot_number = values[i];
                        }
                        else if (i == 186)
                        {
                            /* This is where the HCC goes, we have already captured
                             * this data in the MSDS table and thus do not need
                             * to capture it again.  This is the same field,
                             * simply repeated in 2 different places.
                             */
                        }
                        else if (i == 187)
                        {
                            record.Item_manager = values[i];
                        }
                        else if (i == 188)
                        {
                            record.Item_name = values[i];
                        }
                        else if (i == 189)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Log_flis_niin_ver = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 190)
                        {
                            //Check to see if the field is blank
                            if (values[i] != "")
                            {
                                record.Log_fsc = Convert.ToDouble(values[i]);
                            }
                        }
                        else if (i == 191)
                        {
                            record.Net_unit_weight = values[i];
                        }
                        else if (i == 192)
                        {
                            record.Quantitative_expression = values[i];
                        }
                        else if (i == 193)
                        {
                            record.Shelf_life_code = values[i];
                        }
                        else if (i == 194)
                        {
                            record.Special_emp_code = values[i];
                        }
                        else if (i == 195)
                        {
                            record.Specification_number = values[i];
                        }
                        else if (i == 196)
                        {
                            record.Type_grade_class = values[i];
                        }
                        else if (i == 197)
                        {
                            record.Type_of_container = values[i];
                        }
                        else if (i == 198)
                        {
                            record.Un_na_number = values[i];
                        }
                        else if (i == 199)
                        {
                            record.Unit_of_issue = values[i];
                        }
                        else if (i == 200)
                        {
                            record.Ui_container_qty = values[i];
                        }
                        else if (i == 201)
                        {
                            record.Upc_gtin = values[i];
                        }
                        else if (i == 202)
                        {
                            record.Company_cage_rp = values[i];
                        }
                        else if (i == 203)
                        {
                            record.Company_name_rp = values[i];
                        }
                        else if (i == 204)
                        {
                            record.Label_emerg_phone = values[i];
                        }
                        else if (i == 205)
                        {
                            record.Label_item_name = values[i];
                        }
                        else if (i == 206)
                        {
                            record.Label_proc_year = values[i];
                        }
                        else if (i == 207)
                        {
                            record.Label_prod_ident = values[i];
                        }
                        else if (i == 208)
                        {
                            record.Label_prod_serialno = values[i];
                        }
                        else if (i == 209)
                        {
                            record.Label_signal_word = values[i];
                        }
                        else if (i == 210)
                        {
                            record.Label_stock_no = values[i];
                        }
                        else if (i == 211)
                        {
                            record.Specific_hazards = values[i];
                        }
                        else if (i == 212)
                        {
                            record.Disposal_add_info = values[i];
                        }
                        else if (i == 213)
                        {
                            record.Epa_haz_waste_code = values[i];
                        }
                        else if (i == 214)
                        {
                            //Check to make sure String isnt blank before converting.
                            if (values[i] != "")
                            {
                                record.Epa_haz_waste_ind = Convert.ToChar(values[i]);
                            }
                        }
                        else if (i == 215)
                        {
                            record.Epa_haz_waste_name = values[i];
                        }
                        /* FROM HERE OUT WE ARE DEALING WITH ACTUAL FILES, THUS
                         * NO DATA FOR THESE FIELDS SHOULD BE PRESENT IN THE MSDS EXPORT.
                         * JUST IN CASE, WE CHECK ANYWAY AND IF DATA IS PRESENT, INSERT IT.
                         */
                        else if (i == 216)
                        {
                            if (values[i] != "")
                            {
                                record.Manufacturer_label = values[i];
                            }
                        }
                        else if (i == 217)
                        {
                            /* MSDS GOES HERE, WE KNOW THIS FILE WILL ALWAYS EXIST
                             * OUTSIDE OF THE EXPORT THUS NO DATA WILL EVER LIVE HERE.
                             */
                        }
                        else if (i == 218)
                        {
                            if (values[i] != "")
                            {
                                record.Msds_translated = values[i];
                            }
                        }
                        else if (i == 219)
                        {
                            if (values[i] != "")
                            {
                                record.Neshap_compliance_cert = values[i];
                            }
                        }
                        else if (i == 220)
                        {
                            if (values[i] != "")
                            {
                                record.Other_docs = values[i];
                            }
                        }
                        else if (i == 221)
                        {
                            if (values[i] != "")
                            {
                                record.Product_sheet = values[i];
                            }
                        }
                        else if (i == 222)
                        {
                            if (values[i] != "")
                            {
                                record.Transportation_cert = values[i];
                            }
                        }
                        else if (i > 223)
                        {
                            //Too many fields exist, thus file format must be invalid.
                            this.writeToErrorLog(filename, record.MsdsSerNo, this.parserErrorFile, "MORE THAN THE ALLOTTED NUMBER OF 223 FIELDS.  Total Fields: " + values.Length);
                            this.ValidRecord = false;
                            this.ErrorFlag = true;
                            this.invalidRecordCount++;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        this.writeToErrorLog(filename, record.MsdsSerNo, this.parserErrorFile, "Error recorded in FILE PARSER.  Field: " + i + " Value: " + values[i] + "  Error: " + ex.Message);
                        this.ValidRecord = false;
                        this.ErrorFlag = true;
                        this.invalidRecordCount++;
                        break;
                    }
                }
                #endregion

                // Now get the MSDS files associated with this Record.  
                if (record.MsdsSerNo != null && record.MsdsSerNo != "") {
                    try {
                        //Initialize variables
                        FileInfo[] msdsInfo;
                        DirectoryInfo di = new DirectoryInfo(this.MsdsPath);

                        //Create a search pattern for finding files in the current directory.
                        string searchPattern = Path.GetFileNameWithoutExtension(filename) + "_" + record.MsdsSerNo + "*";

                        //Get all the files in the current directory that begin with the filename.
                        msdsInfo = di.GetFiles(searchPattern, SearchOption.TopDirectoryOnly);

                        // For each MSDS file type found, add each to the record.
                        //TODO Account for multiple MSDS files.
                        foreach (FileInfo fInfo in msdsInfo) {
                            if (fInfo.Name.Contains("MSDS")) {
                                if (record.MsdsFilePath == null || record.MsdsFilePath == "")
                                {
                                    record.MsdsFilePath = this.MsdsPath + "\\" + fInfo.Name;
                                }
                                //If more than one MSDS File exists for this Serial Number,
                                //use the latest one created.
                                else
                                {
                                    //filename
                                    string oldFileName = Path.GetFileNameWithoutExtension(record.MsdsFilePath);
                                    string newFileName = Path.GetFileNameWithoutExtension(fInfo.Name);

                                    //extract the year, month, and day from the string above
                                    string old_strYr = "", old_strMon = "", old_strDay = "";
                                    string new_strYr = "", new_strMon = "", new_strDay = "";
                                    int oldVar = 0, newVar = 0;

                                    for (int i = 0; i <= oldFileName.Length; i++)
                                    {
                                        char c = oldFileName[i];

                                        if (c.ToString().Equals("_"))
                                        {
                                            //we've found the first _
                                            //I'm assuming the length of characters before the second _ is always 5
                                            oldVar = i;

                                            //break from the loop once we find the first _
                                            break;
                                        }
                                    }

                                    //Now do the same for the newFileName
                                    for (int i = 0; i <= newFileName.Length; i++)
                                    {
                                        char c = newFileName[i];

                                        if (c.ToString().Equals("_"))
                                        {
                                            //we've found the first _
                                            //I'm assuming the length of characters before the second _ is always 5
                                            newVar = i;

                                            //break from the loop once we find the first _
                                            break;
                                        }
                                    }

                                    //Extract the Datetime from the original file name.
                                    old_strYr = oldFileName.Substring((oldVar + 7), 4);
                                    old_strMon = oldFileName.Substring((oldVar + 11), 2);
                                    old_strDay = oldFileName.Substring((oldVar + 13), 2);

                                    int old_year = Convert.ToInt32(old_strYr);
                                    int old_month = Convert.ToInt32(old_strMon);
                                    int old_day = Convert.ToInt32(old_strDay);

                                    DateTime old_date = new DateTime(old_year, old_month, old_day);

                                    //Extract the Datetime from the new file name.
                                    new_strYr = newFileName.Substring((newVar + 7), 4);
                                    new_strMon = newFileName.Substring((newVar + 11), 2);
                                    new_strDay = newFileName.Substring((newVar + 13), 2);

                                    int new_year = Convert.ToInt32(new_strYr);
                                    int new_month = Convert.ToInt32(new_strMon);
                                    int new_day = Convert.ToInt32(new_strDay);

                                    DateTime new_date = new DateTime(new_year, new_month, new_day);

                                    //Now compare the dates, whichever one is more recent, use that file.
                                    int result = DateTime.Compare(old_date, new_date);

                                    //If result < 0 then old_date is earlier than new_date
                                    //if result = 0 then old_date is the same as new_date
                                    //if result > 0 then old_date is later than new_date.
                                    if (result < 0)
                                    {
                                        //If new_date is more recent, update the MSDS File to reflect this.
                                        record.MsdsFilePath = this.MsdsPath + "\\" + fInfo.Name;
                                    }
                                }
                            }
                            else if (fInfo.Name.Contains("MFR.")) {
                                record.Manufacturer_label = this.MsdsPath + "\\" + fInfo.Name;
                            }
                            else if (fInfo.Name.Contains("OTHER DOCS")) {
                                record.Other_docs = this.MsdsPath + "\\" + fInfo.Name;
                            }
                            else if (fInfo.Name.Contains("PROD SHEET")) {
                                record.Product_sheet = this.MsdsPath + "\\" + fInfo.Name;
                            }
                            else if (fInfo.Name.Contains("TRANS CERT")) {
                                record.Transportation_cert = this.MsdsPath + "\\" + fInfo.Name;
                            }
                            else if (fInfo.Name.Contains("NESHAP")) {
                                record.Neshap_compliance_cert = this.MsdsPath + "\\" + fInfo.Name;
                            }
                            else {
                                // Catch and log the unknown file type so we can accomadate it at a later time.
                                this.writeToErrorLog(filename, record.MsdsSerNo, this.parserErrorFile, "***NEW FILE TYPE DISCOVERED***  " + fInfo.Name);
                            }
                        }

                    } catch (Exception ex) {
                        this.writeToErrorLog(filename, record.MsdsSerNo, this.parserErrorFile, "Error occurred while locating MSDS file paths for filename: " + 
                                                filename + " and serial number: " + record.MsdsSerNo + ".  Error: " + ex.Message);
                        this.ValidRecord = false;
                        this.ErrorFlag = true;
                        this.invalidRecordCount++;
                        break;
                    }

                    /* Add the newly created record to the
                     * Record list to be inserted into the DB.
                     * Only add valid records.
                     */
                    if (this.ValidRecord == true)
                    {
                        //Store the file name of the just parsed file for later use.
                        record.ParsedFileName = filename;

                        // See if we need to split this MSDS Into multiple records because of 
                        // Multiple PARTNOs
                        List<Record> records = splitRecord(record);
                        foreach (Record newRecord in records) {
                            recordsMSDS.Add(record);
                            this.validRecordCount++;
                        }
                    }

                } //End of if record.msdsSerNo

                //Grab the next line for parsing.
                line = sr.ReadLine();

            } //End of while loop.
            #endregion

            //Close the Reader.
            sr.Close();

            //Output stats to the log so we know what was parsed and what wasnt.
            this.fileParserStats(this.parsedRecordsFile, filename, this.totalRecords, this.RecordsMSDS.Count, this.validRecordCount, this.invalidRecordCount);
        }
        #endregion

        // Returns a list of MSDS File Paths for a record file.
        //TODO NOT EVEN USING I DONT BELIEVE.
        #region getMSDSPaths(String)
        protected List<string> getMSDSPaths(String fName, String tempDir)
        {
            //Initialize variables.
            List<string> msds = new List<string>();

            /* Create an array to store File Information 
            * and a list to store the file path strings for each
            * msds file.
            */
            FileInfo[] msdsInfo;
            DirectoryInfo di = new DirectoryInfo(tempDir);

            try {
                //First get just the filename without the extension.
                String shortName = Path.GetFileNameWithoutExtension(fName);

                //Create a search pattern for finding files in the current directory.
                string searchPattern = shortName + "*";

                //Get all the files in the current directory that begin with the filename.
                msdsInfo = di.GetFiles(searchPattern, SearchOption.TopDirectoryOnly);

                // Loop through and grab just the file name for each of these 
                // file info objects, excluding the record file itself.
                foreach (FileInfo fInfo in msdsInfo)
                {
                    /* We can have MSDS files that are .txt files.
                     * Thus I check to see if an underscore is present.
                     * as all MSDS files will follow the format:
                     * msds#_something_date_something.extension
                     */
                    if (fInfo.Name.Contains("_")) {
                        msds.Add(fInfo.Name);
                    }
                }
            } catch (Exception ex) {
                this.writeToErrorLog(fName, " ", this.parserErrorFile, "Error occurred while grabbing MSDS's for file: " + fName + " Error: " + ex.Message);
                this.ErrorFlag = true;
            }

            // Return a list of filepaths for all 
            // associated MSDS files.
            return msds;
        }

        #endregion


        /* Accepts a list of Records.  Loop through this list, inserting each record into the 
         * HILT DB.  If errors occur, log them and notify user at end of process.
         */
        #region insertRecords(List<Record>)
        protected void insertRecords(List<Record> recordList)
        {
            int progress = 25;
            int counter = 0;

            #region Insert Statements
            string msdsInsertCmd = "INSERT INTO msds_master (MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, NIIN, hcc_id, msds_file, file_name, manually_entered, " +
                                    "ARTICLE_IND, DESCRIPTION, EMERGENCY_TEL, END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, " +
                                    "MIXTURE_IND, PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, PUBLISHED_IND, " +
                                    "PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY_CODE, TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE) " +
                                    "VALUES(@MSDSSERNO, @CAGE, @MANUFACTURER, @PARTNO, @FSC, @NIIN, @hcc_id, @msds_file, @file_name, @manually_entered, " +
                                    "@ARTICLE_IND, @DESCRIPTION, @EMERGENCY_TEL, @END_COMP_IND, @END_ITEM_IND, @KIT_IND, @KIT_PART_IND, @MANUFACTURER_MSDS_NO, " +
                                    "@MIXTURE_IND, @PRODUCT_IDENTITY, @PRODUCT_LOAD_DATE, @PRODUCT_RECORD_STATUS, @PRODUCT_REVISION_NO, @PROPRIETARY_IND, @PUBLISHED_IND, " +
                                    "@PURCHASED_PROD_IND, @PURE_IND, @RADIOACTIVE_IND, @SERVICE_AGENCY_CODE, @TRADE_NAME, @TRADE_SECRET_IND, @PRODUCT_IND, @PRODUCT_LANGUAGE); " +
                                    "SELECT SCOPE_IDENTITY()";

            string msds_AFJM_PSN_InsertCmd = "INSERT INTO msds_afjm_psn (AFJM_HAZARD_CLASS, AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP, AFJM_PROP_SHIP_NAME, AFJM_PROP_SHIP_MODIFIER, " +
                                             "AFJM_PSN_CODE, AFJM_SPECIAL_PROV, AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS, AFJM_UN_ID_NUMBER, msds_id) " +
                                             "VALUES (@AFJM_HAZARD_CLASS, @AFJM_PACK_PARAGRAPH, @AFJM_PACK_GROUP, @AFJM_PROP_SHIP_NAME, @AFJM_PROP_SHIP_MODIFIER, " +
                                             "@AFJM_PSN_CODE, @AFJM_SPECIAL_PROV, @AFJM_SUBSIDIARY_RISK, @AFJM_SYMBOLS, @AFJM_UN_ID_NUMBER, @msds_id); ";

            string msds_Contractor_Info_InsertCmd = "INSERT INTO msds_contractor_info (CT_NUMBER, CT_CAGE, CT_CITY, CT_COMPANY_NAME, CT_COUNTRY, CT_PO_BOX, CT_PHONE, " +
                                                    "PURCHASE_ORDER_NO, CT_STATE, msds_id) " +
                                                    "VALUES (@CT_NUMBER, @CT_CAGE, @CT_CITY, @CT_COMPANY_NAME, @CT_COUNTRY, @CT_PO_BOX, @CT_PHONE, " +
                                                    "@PURCHASE_ORDER_NO, @CT_STATE, @msds_id); ";

            string msds_Disposal_InsertCmd = "INSERT INTO msds_disposal (DISPOSAL_ADD_INFO, EPA_HAZ_WASTE_CODE, EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME, msds_id) " +
                                             "VALUES (@DISPOSAL_ADD_INFO, @EPA_HAZ_WASTE_CODE, @EPA_HAZ_WASTE_IND, @EPA_HAZ_WASTE_NAME, @msds_id); ";

            string msds_Document_Types_InsertCmd = "INSERT INTO msds_document_types (MANUFACTURER_LABEL, MSDS_TRANSLATED, NESHAP_COMP_CERT, OTHER_DOCS, PRODUCT_SHEET, " +
                                                    "TRANSPORTATION_CERT, msds_translated_filename, neshap_comp_filename, other_docs_filename, product_sheet_filename, " +
                                                    "transportation_cert_filename, manufacturer_label_filename, msds_id) " +
                                                    "VALUES (@MANUFACTURER_LABEL, @MSDS_TRANSLATED, @NESHAP_COMP_CERT, @OTHER_DOCS, @PRODUCT_SHEET, " +
                                                    "@TRANSPORTATION_CERT, @msds_translated_filename, @neshap_comp_filename, @other_docs_filename, @product_sheet_filename, " +
                                                    "@transportation_cert_filename, @manufacturer_label_filename, @msds_id); ";

            string msds_DOT_PSN_InsertCmd = "INSERT INTO msds_dot_psn (DOT_HAZARD_CLASS_DIV, DOT_HAZARD_LABEL, DOT_MAX_CARGO, DOT_MAX_PASSENGER, DOT_PACK_BULK, DOT_PACK_EXCEPTIONS, " +
                                            "DOT_PACK_NONBULK, DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER, DOT_PSN_CODE, DOT_SPECIAL_PROVISION, DOT_SYMBOLS, DOT_UN_ID_NUMBER, " +
                                            "DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW, DOT_PACK_GROUP, msds_id) " +
                                            "VALUES (@DOT_HAZARD_CLASS_DIV, @DOT_HAZARD_LABEL, @DOT_MAX_CARGO, @DOT_MAX_PASSENGER, @DOT_PACK_BULK, @DOT_PACK_EXCEPTIONS, " +
                                            "@DOT_PACK_NONBULK, @DOT_PROP_SHIP_NAME, @DOT_PROP_SHIP_MODIFIER, @DOT_PSN_CODE, @DOT_SPECIAL_PROVISION, @DOT_SYMBOLS, @DOT_UN_ID_NUMBER, " +
                                            "@DOT_WATER_OTHER_REQ, @DOT_WATER_VESSEL_STOW, @DOT_PACK_GROUP, @msds_id); ";

            string msds_IATA_PSN_InsertCmd = "INSERT INTO msds_iata_psn (IATA_CARGO_PACKING, IATA_HAZARD_CLASS, IATA_HAZARD_LABEL, IATA_PACK_GROUP, IATA_PASS_AIR_PACK_LMT_INSTR, " +
                                            "IATA_PASS_AIR_PACK_LMT_PER_PKG, IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME, IATA_PROP_SHIP_MODIFIER, IATA_CARGO_PACK_MAX_QTY, " +
                                            "IATA_PSN_CODE, IATA_PASS_AIR_MAX_QTY, IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK, IATA_UN_ID_NUMBER, msds_id) " +
                                            "VALUES (@IATA_CARGO_PACKING, @IATA_HAZARD_CLASS, @IATA_HAZARD_LABEL, @IATA_PACK_GROUP, @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                                            "@IATA_PASS_AIR_PACK_LMT_PER_PKG, @IATA_PASS_AIR_PACK_NOTE, @IATA_PROP_SHIP_NAME, @IATA_PROP_SHIP_MODIFIER, @IATA_CARGO_PACK_MAX_QTY, " +
                                            "@IATA_PSN_CODE, @IATA_PASS_AIR_MAX_QTY, @IATA_SPECIAL_PROV, @IATA_SUBSIDIARY_RISK, @IATA_UN_ID_NUMBER, @msds_id); ";

            string msds_IMO_PSN_InsertCmd = "INSERT INTO msds_imo_psn (IMO_EMS_NO, IMO_HAZARD_CLASS, IMO_IBC_INSTR, IMO_LIMITED_QTY, IMO_PACK_GROUP, IMO_PACK_INSTRUCTIONS, IMO_PACK_PROVISIONS, " +
                                            "IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER, IMO_PSN_CODE, IMO_SPECIAL_PROV, IMO_STOW_SEGR, IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO, IMO_TANK_INSTR_PROV, " +
                                            "IMO_TANK_INSTR_UN, IMO_UN_NUMBER, IMO_IBC_PROVISIONS, msds_id) " +
                                            "VALUES (@IMO_EMS_NO, @IMO_HAZARD_CLASS, @IMO_IBC_INSTR, @IMO_LIMITED_QTY, @IMO_PACK_GROUP, @IMO_PACK_INSTRUCTIONS, @IMO_PACK_PROVISIONS, " +
                                            "@IMO_PROP_SHIP_NAME, @IMO_PROP_SHIP_MODIFIER, @IMO_PSN_CODE, @IMO_SPECIAL_PROV, @IMO_STOW_SEGR, @IMO_SUBSIDIARY_RISK, @IMO_TANK_INSTR_IMO, @IMO_TANK_INSTR_PROV, " +
                                            "@IMO_TANK_INSTR_UN, @IMO_UN_NUMBER, @IMO_IBC_PROVISIONS, @msds_id); ";

            string msds_Ingredients_InsertCmd = "INSERT INTO msds_ingredients (msds_id, CAS, RTECS_NUM, RTECS_CODE, INGREDIENT_NAME, PRCNT, OSHA_PEL, OSHA_STEL, ACGIH_TLV, " +
                                                "ACGIH_STEL, EPA_REPORT_QTY, DOT_REPORT_QTY, PRCNT_VOL_VALUE, PRCNT_VOL_WEIGHT, CHEM_MFG_COMP_NAME, ODS_IND, OTHER_REC_LIMITS) " +
                                                "VALUES (@msds_id, @CAS, @RTECS_NUM, @RTECS_CODE, @INGREDIENT_NAME, @PRCNT, @OSHA_PEL, @OSHA_STEL, @ACGIH_TLV, " +
                                                "@ACGIH_STEL, @EPA_REPORT_QTY, @DOT_REPORT_QTY, @PRCNT_VOL_VALUE, @PRCNT_VOL_WEIGHT, @CHEM_MFG_COMP_NAME, @ODS_IND, @OTHER_REC_LIMITS); ";

            string msds_Item_Description_InsertCmd = "INSERT INTO msds_item_description (msds_id, ITEM_MANAGER, ITEM_NAME, SPECIFICATION_NUMBER, TYPE_GRADE_CLASS, UNIT_OF_ISSUE, " +
                                                     "QUANTITATIVE_EXPRESSION, UI_CONTAINER_QTY, TYPE_OF_CONTAINER, BATCH_NUMBER, LOT_NUMBER, LOG_FLIS_NIIN_VER, LOG_FSC, NET_UNIT_WEIGHT, " +
                                                     "SHELF_LIFE_CODE, SPECIAL_EMP_CODE, UN_NA_NUMBER, UPC_GTIN) " +
                                                     "VALUES (@msds_id, @ITEM_MANAGER, @ITEM_NAME, @SPECIFICATION_NUMBER, @TYPE_GRADE_CLASS, @UNIT_OF_ISSUE, " +
                                                     "@QUANTITATIVE_EXPRESSION, @UI_CONTAINER_QTY, @TYPE_OF_CONTAINER, @BATCH_NUMBER, @LOT_NUMBER, @LOG_FLIS_NIIN_VER, @LOG_FSC, @NET_UNIT_WEIGHT, " +
                                                     "@SHELF_LIFE_CODE, @SPECIAL_EMP_CODE, @UN_NA_NUMBER, @UPC_GTIN); ";

            string msds_Label_Info_InsertCmd = "INSERT INTO msds_label_info (COMPANY_CAGE_RP, COMPANY_NAME_RP, LABEL_EMERG_PHONE, LABEL_ITEM_NAME, LABEL_PROC_YEAR, LABEL_PROD_IDENT, " +
                                               "LABEL_PROD_SERIALNO, LABEL_SIGNAL_WORD, LABEL_STOCK_NO, SPECIFIC_HAZARDS, msds_id) " +
                                               "VALUES (@COMPANY_CAGE_RP, @COMPANY_NAME_RP, @LABEL_EMERG_PHONE, @LABEL_ITEM_NAME, @LABEL_PROC_YEAR, @LABEL_PROD_IDENT, " +
                                               "@LABEL_PROD_SERIALNO, @LABEL_SIGNAL_WORD, @LABEL_STOCK_NO, @SPECIFIC_HAZARDS, @msds_id); ";

            string msds_Phys_Chemical_InsertCmd = "INSERT INTO msds_phys_chemical (msds_id, VAPOR_PRESS, VAPOR_DENS, SPECIFIC_GRAV, VOC_POUNDS_GALLON, VOC_GRAMS_LITER, " +
                                                  "PH, VISCOSITY, EVAP_RATE_REF, SOL_IN_WATER, APP_ODOR, PERCENT_VOL_VOLUME, AUTOIGNITION_TEMP, CARCINOGEN_IND, EPA_ACUTE, " +
                                                  "EPA_CHRONIC, EPA_FIRE, EPA_PRESSURE, EPA_REACTIVITY, FLASH_PT_TEMP, NEUT_AGENT, NFPA_FLAMMABILITY, NFPA_HEALTH, NFPA_REACTIVITY, NFPA_SPECIAL, " +
                                                  "OSHA_CARCINOGENS, OSHA_COMB_LIQUID, OSHA_COMP_GAS, OSHA_CORROSIVE, OSHA_EXPLOSIVE, OSHA_FLAMMABLE, OSHA_HIGH_TOXIC, OSHA_IRRITANT, OSHA_ORG_PEROX, " +
                                                  "OSHA_OTHERLONGTERM, OSHA_OXIDIZER, OSHA_PYRO, OSHA_SENSITIZER, OSHA_TOXIC, OSHA_UNST_REACT, OTHER_SHORT_TERM, PHYS_STATE_CODE, VOL_ORG_COMP_WT, " +
                                                  "OSHA_WATER_REACTIVE) " +
                                                  "VALUES (@msds_id, @VAPOR_PRESS, @VAPOR_DENS, @SPECIFIC_GRAV, @VOC_POUNDS_GALLON, @VOC_GRAMS_LITER, " +
                                                  "@PH, @VISCOSITY, @EVAP_RATE_REF, @SOL_IN_WATER, @APP_ODOR, @PERCENT_VOL_VOLUME, @AUTOIGNITION_TEMP, @CARCINOGEN_IND, @EPA_ACUTE, " +
                                                  "@EPA_CHRONIC, @EPA_FIRE, @EPA_PRESSURE, @EPA_REACTIVITY, @FLASH_PT_TEMP, @NEUT_AGENT, @NFPA_FLAMMABILITY, @NFPA_HEALTH, @NFPA_REACTIVITY, @NFPA_SPECIAL, " +
                                                  "@OSHA_CARCINOGENS, @OSHA_COMB_LIQUID, @OSHA_COMP_GAS, @OSHA_CORROSIVE, @OSHA_EXPLOSIVE, @OSHA_FLAMMABLE, @OSHA_HIGH_TOXIC, @OSHA_IRRITANT, @OSHA_ORG_PEROX, " +
                                                  "@OSHA_OTHERLONGTERM, @OSHA_OXIDIZER, @OSHA_PYRO, @OSHA_SENSITIZER, @OSHA_TOXIC, @OSHA_UNST_REACT, @OTHER_SHORT_TERM, @PHYS_STATE_CODE, @VOL_ORG_COMP_WT, " +
                                                  "@OSHA_WATER_REACTIVE); ";

            string msds_Radiological_Info_InsertCmd = "INSERT INTO msds_radiological_info (NRC_LP_NUM, OPERATOR, RAD_AMOUNT_MICRO, RAD_FORM, RAD_CAS, RAD_NAME, RAD_SYMBOL, REP_NSN, SEALED, msds_id) " +
                                                      "VALUES (@NRC_LP_NUM, @OPERATOR, @RAD_AMOUNT_MICRO, @RAD_FORM, @RAD_CAS, @RAD_NAME, @RAD_SYMBOL, @REP_NSN, @SEALED, @msds_id); ";

            string msds_Transportation_InsertCmd = "INSERT INTO msds_transportation (AF_MMAC_CODE, CERTIFICATE_COE, COMPETENT_CAA, DOD_ID_CODE, DOT_EXEMPTION_NO, DOT_RQ_IND, EX_NO, FLASH_PT_TEMP, " +
                                                   "HIGH_EXPLOSIVE_WT, LTD_QTY_IND, MAGNETIC_IND, MAGNETISM, MARINE_POLLUTANT_IND, NET_EXP_QTY_DIST, NET_EXP_WEIGHT, NET_PROPELLANT_WT, " +
                                                   "NOS_TECHNICAL_SHIPPING_NAME, TRANSPORTATION_ADDITIONAL_DATA, msds_id) " +
                                                   "VALUES (@AF_MMAC_CODE, @CERTIFICATE_COE, @COMPETENT_CAA, @DOD_ID_CODE, @DOT_EXEMPTION_NO, @DOT_RQ_IND, @EX_NO, @FLASH_PT_TEMP, " +
                                                   "@HIGH_EXPLOSIVE_WT, @LTD_QTY_IND, @MAGNETIC_IND, @MAGNETISM, @MARINE_POLLUTANT_IND, @NET_EXP_QTY_DIST, @NET_EXP_WEIGHT, @NET_PROPELLANT_WT, " +
                                                   "@NOS_TECHNICAL_SHIPPING_NAME, @TRANSPORTATION_ADDITIONAL_DATA, @msds_id); ";

            string HCC_ID_SelectCmd = "SELECT h.hcc_id FROM hcc h where h.hcc = @HCC;";

            string NIIN_ID_SelectCmd = "SELECT n.aul_id FROM auth_use_list n where n.niin = @NIIN;";

            string mfg_catalog_InsertCmd = "INSERT INTO mfg_catalog (CAGE, MANUFACTURER, PRODUCT_IDENTITY, AUL_ID, HAZARD_ID) " +
                                           "VALUES (@MFG_CAGE, @MFG, @PRODUCT_IDENTITY, @NIIN_CATALOG_ID, @HAZARD_ID);";

            string find_hazard_id = "SELECT h.hazard_id from hazardous_hcc h where h.hcc = @HCC;";

            string find_hcc = "SELECT h.hcc from hcc h where h.hcc_id = @HCC;";

            #endregion

            //Go ahead and open the connection to do some work.
            try { this.conn.Open(); }
            catch (Exception ex)
            {
                //throw new ImportException("Failed to establish connection to HILT DB. Please check your database connection and try again.");                
                this.writeToErrorLog("No file, DB Connection Error.", "", this.importErrorFile, "Failed to establish connection to HILT DB.  Error:  " + ex.Message);
            }

            #region SqlCommands declared
            SqlCommand cmd_msds;
            SqlCommand cmd_msds_afjm_psn;
            SqlCommand cmd_msds_contractor_info;
            SqlCommand cmd_msds_disposal;
            SqlCommand cmd_msds_document_types;
            SqlCommand cmd_msds_dot_psn;
            SqlCommand cmd_msds_iata_psn;
            SqlCommand cmd_msds_imo_psn;
            SqlCommand cmd_msds_ingredients;
            SqlCommand cmd_msds_item_description;
            SqlCommand cmd_msds_label_info;
            SqlCommand cmd_msds_phys_chemical;
            SqlCommand cmd_msds_radiological_info;
            SqlCommand cmd_msds_transportation;
            SqlCommand cmd_select_hcc;
            SqlCommand cmd_select_niin;
            SqlCommand cmd_mfg_catalog;
            SqlCommand cmd_get_hazard_id;
            SqlCommand cmd_find_hcc;
            #endregion


            //Loop through the list and insert information into the database.
            foreach (Record r in recordList)
            {
                //A transaction for each record, that way if we encounter an error,
                //we can flag it and continue inserting the other records.
                SqlTransaction tran = this.conn.BeginTransaction();
                this.ValidInsert = true;

                #region Declare SqlCommands and Transactions
                cmd_msds = new SqlCommand(msdsInsertCmd, this.conn);
                cmd_msds_afjm_psn = new SqlCommand(msds_AFJM_PSN_InsertCmd, this.conn);
                cmd_msds_contractor_info = new SqlCommand(msds_Contractor_Info_InsertCmd, this.conn);
                cmd_msds_disposal = new SqlCommand(msds_Disposal_InsertCmd, this.conn);
                cmd_msds_document_types = new SqlCommand(msds_Document_Types_InsertCmd, this.conn);
                cmd_msds_dot_psn = new SqlCommand(msds_DOT_PSN_InsertCmd, this.conn);
                cmd_msds_iata_psn = new SqlCommand(msds_IATA_PSN_InsertCmd, this.conn);
                cmd_msds_imo_psn = new SqlCommand(msds_IMO_PSN_InsertCmd, this.conn);
                cmd_msds_item_description = new SqlCommand(msds_Item_Description_InsertCmd, this.conn);
                cmd_msds_label_info = new SqlCommand(msds_Label_Info_InsertCmd, this.conn);
                cmd_msds_phys_chemical = new SqlCommand(msds_Phys_Chemical_InsertCmd, this.conn);
                cmd_msds_radiological_info = new SqlCommand(msds_Radiological_Info_InsertCmd, this.conn);
                cmd_msds_transportation = new SqlCommand(msds_Transportation_InsertCmd, this.conn);
                cmd_select_hcc = new SqlCommand(HCC_ID_SelectCmd, this.conn);
                cmd_select_niin = new SqlCommand(NIIN_ID_SelectCmd, this.conn);
                cmd_mfg_catalog = new SqlCommand(mfg_catalog_InsertCmd, this.conn);
                cmd_msds_ingredients = new SqlCommand(msds_Ingredients_InsertCmd, this.conn);
                cmd_get_hazard_id = new SqlCommand(find_hazard_id, this.conn);
                cmd_find_hcc = new SqlCommand(find_hcc, this.conn);

                cmd_msds.Transaction = tran;
                cmd_msds_afjm_psn.Transaction = tran;
                cmd_msds_contractor_info.Transaction = tran;
                cmd_msds_disposal.Transaction = tran;
                cmd_msds_document_types.Transaction = tran;
                cmd_msds_dot_psn.Transaction = tran;
                cmd_msds_iata_psn.Transaction = tran;
                cmd_msds_imo_psn.Transaction = tran;
                cmd_msds_item_description.Transaction = tran;
                cmd_msds_label_info.Transaction = tran;
                cmd_msds_phys_chemical.Transaction = tran;
                cmd_msds_radiological_info.Transaction = tran;
                cmd_msds_transportation.Transaction = tran;
                cmd_select_hcc.Transaction = tran;
                cmd_select_niin.Transaction = tran;
                cmd_mfg_catalog.Transaction = tran;
                cmd_msds_ingredients.Transaction = tran;
                cmd_get_hazard_id.Transaction = tran;
                cmd_find_hcc.Transaction = tran;
                #endregion

                int msds_id = -1;

                //Updated 02/08/2011  Check to see if this record has a NIIN value.
                //If so, check to see if that NIIN exists in the db.
                //If it does, insert record, if not, skip it.
                if (r.Niin != "" && r.Niin != null)
                {

                    cmd_select_niin.Parameters.AddWithValue("@NIIN", r.Niin);
                    Object n = cmd_select_niin.ExecuteScalar();
                    if (n != null)
                    {
                        #region MSDS Table Insert
                        try
                        {
                            cmd_msds.Parameters.AddWithValue("@MSDSSERNO", dbNull(r.MsdsSerNo));
                            cmd_msds.Parameters.AddWithValue("@CAGE", dbNull(r.Rpcage));
                            cmd_msds.Parameters.AddWithValue("@MANUFACTURER", dbNull(r.Manufacturer));
                            cmd_msds.Parameters.AddWithValue("@PARTNO", dbNull(r.PartNo));
                            cmd_msds.Parameters.AddWithValue("@FSC", dbNull(r.Fsc));
                            cmd_msds.Parameters.AddWithValue("@NIIN", dbNull(r.Niin));

                            //Get the HCC
                            //*******************************
                            if (r.Hcc != "" && r.Hcc != null)
                            {
                                //Check to see if this HCC exists in the table.
                                cmd_select_hcc.Parameters.AddWithValue("@HCC", r.Hcc);
                                Object h = null;

                                try
                                {
                                    h = cmd_select_hcc.ExecuteScalar();
                                }
                                catch (Exception ex)
                                {
                                    this.writeToRecordInsertErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Could Not find the HCC in the table.  HCC: " + r.Hcc + ". Error:  " + ex.Message);
                                }

                                //If it does, update record.
                                if (h != null)
                                {
                                    int hcc_id = Convert.ToInt32(h);
                                    r.Hcc_id = hcc_id;
                                }

                                if (r.Hcc_id == 0)
                                {
                                    r.Hcc_id = null;
                                }
                            }
                            //********************************

                            cmd_msds.Parameters.AddWithValue("@hcc_id", dbNull(r.Hcc_id));

                            //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                            //Get file as a byte array.
                            if (r.MsdsFilePath != null)
                            {
                                byte[] rawFile = File.ReadAllBytes(r.MsdsFilePath);
                                cmd_msds.Parameters.AddWithValue("@msds_file", rawFile);
                            }
                            else
                            {
                                cmd_msds.Parameters.AddWithValue("@msds_file", new byte[0]);
                            }

                            cmd_msds.Parameters.AddWithValue("@file_name", dbNull(r.MsdsFilePath));
                            cmd_msds.Parameters.AddWithValue("@manually_entered", 0);
                            cmd_msds.Parameters.AddWithValue("@ARTICLE_IND", dbNull(r.Article_ind));
                            cmd_msds.Parameters.AddWithValue("@DESCRIPTION", dbNull(r.Description));
                            cmd_msds.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(r.Emergency_tel));
                            cmd_msds.Parameters.AddWithValue("@END_COMP_IND", dbNull(r.End_comp_ind));
                            cmd_msds.Parameters.AddWithValue("@END_ITEM_IND", dbNull(r.End_item_ind));
                            cmd_msds.Parameters.AddWithValue("@KIT_IND", dbNull(r.Kit_ind));
                            cmd_msds.Parameters.AddWithValue("@KIT_PART_IND", dbNull(r.Kit_part_ind));
                            cmd_msds.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(r.Manufacturer_msds_no));
                            cmd_msds.Parameters.AddWithValue("@MIXTURE_IND", dbNull(r.Mixture_ind));
                            cmd_msds.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(r.Product_identity));
                            cmd_msds.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(r.Product_load_date));
                            cmd_msds.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(r.Product_record_status));
                            cmd_msds.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(r.Product_revision_no));
                            cmd_msds.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(r.Proprietary_ind));
                            cmd_msds.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(r.Published_ind));
                            cmd_msds.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(r.Purchased_prod_ind));
                            cmd_msds.Parameters.AddWithValue("@PURE_IND", dbNull(r.Pure_ind));
                            cmd_msds.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(r.Radioactive_ind));
                            cmd_msds.Parameters.AddWithValue("@SERVICE_AGENCY_CODE", dbNull(r.Service_agency_code));
                            cmd_msds.Parameters.AddWithValue("@TRADE_NAME", dbNull(r.Trade_name));
                            cmd_msds.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(r.Trade_secret_ind));
                            cmd_msds.Parameters.AddWithValue("@PRODUCT_IND", dbNull(r.Product_ind));
                            cmd_msds.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(r.Product_language));

                            //Insert the MSDS table records and capture the id returned
                            //for later use.
                            Object id = cmd_msds.ExecuteScalar();
                            msds_id = Convert.ToInt32(id);
                            cmd_msds.Parameters.Clear();

                        }
                        catch (Exception ex)
                        {
                            this.writeToRecordInsertErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS table. Error: " + ex.Message);
                            this.ValidInsert = false;
                        }
                        #endregion

                        //If we have a valid msds insert, continue.
                        if (msds_id != -1)
                        {

                            #region MFG_Catalog Insert
                            //Insert into the manufacturer's table.  Check to be sure the NIIN exists
                            //in the NIIN table prior to doing this.  If it doesn't exist, or the NIIN is 
                            //blank, skip this step.
                            //First check to see if the current NIIN exists in the HILT DB.
                            //*******************************

                            try
                            {
                                //No longer needed now that previous check has been added.
                                //if (r.Niin != "" && r.Niin != null)
                                //{
                                //Check to see if this HCC exists in the table.
                                //cmd_select_niin.Parameters.AddWithValue("@NIIN", r.Niin);
                                //Object n = cmd_select_niin.ExecuteScalar();

                                //If it does, update record.
                                //if (n != null)
                                //{
                                int niin_id = Convert.ToInt32(n);
                                int? hazard_id = null;

                                //Now that we have this data, insert into the mfg_catalog
                                cmd_mfg_catalog.Parameters.AddWithValue("@MFG_CAGE", dbNull(r.Rpcage));
                                cmd_mfg_catalog.Parameters.AddWithValue("@MFG", dbNull(r.Manufacturer));
                                cmd_mfg_catalog.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(r.Product_identity));
                                cmd_mfg_catalog.Parameters.AddWithValue("@NIIN_CATALOG_ID", niin_id);

                                //Updated 1-27-2011 to allow for the hazard_id column to be populated.
                                //Using the known HCC, find the hazard_id.                                
                                if (r.Hcc_id != null)
                                {
                                    //Using the hcc_id, determine the actual hcc value.
                                    cmd_find_hcc.Parameters.AddWithValue("@HCC", r.Hcc_id);
                                    string hcc_value = (String)cmd_find_hcc.ExecuteScalar();

                                    //Now that we know the hcc value, we can determine the hazard_id.
                                    cmd_get_hazard_id.Parameters.AddWithValue("@HCC", hcc_value);
                                    Object hz = cmd_get_hazard_id.ExecuteScalar();

                                    if (hz != null)
                                    {
                                        hazard_id = Convert.ToInt32(hz);

                                        //Performing a sanity check to ensure no invalid data is inserted into the db.
                                        if (hazard_id == 0)
                                        {
                                            hazard_id = null;
                                        }
                                    }
                                }

                                cmd_mfg_catalog.Parameters.AddWithValue("@HAZARD_ID", dbNull(hazard_id));

                                cmd_mfg_catalog.ExecuteNonQuery();
                                cmd_mfg_catalog.Parameters.Clear();
                            }
                            // }
                            // }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MFG_CATALOG table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }

                            #endregion

                            #region MSDS Ingredients Insert
                            try
                            {
                                cmd_msds_ingredients.Parameters.AddWithValue("@msds_id", msds_id);
                                cmd_msds_ingredients.Parameters.AddWithValue("@CAS", dbNull(r.Cas));
                                cmd_msds_ingredients.Parameters.AddWithValue("@RTECS_NUM", dbNull(r.Rtecs_num));
                                cmd_msds_ingredients.Parameters.AddWithValue("@RTECS_CODE", dbNull(r.Rtecs_code));
                                cmd_msds_ingredients.Parameters.AddWithValue("@INGREDIENT_NAME", dbNull(r.Ingredient_name));
                                cmd_msds_ingredients.Parameters.AddWithValue("@PRCNT", dbNull(r.Prcnt));
                                cmd_msds_ingredients.Parameters.AddWithValue("@OSHA_PEL", dbNull(r.Osha_pel));
                                cmd_msds_ingredients.Parameters.AddWithValue("@OSHA_STEL", dbNull(r.Osha_stel));
                                cmd_msds_ingredients.Parameters.AddWithValue("@ACGIH_TLV", dbNull(r.Acgih_tlv));
                                cmd_msds_ingredients.Parameters.AddWithValue("@ACGIH_STEL", dbNull(r.Acgih_stel));
                                cmd_msds_ingredients.Parameters.AddWithValue("@EPA_REPORT_QTY", dbNull(r.Epa_report_qty));
                                cmd_msds_ingredients.Parameters.AddWithValue("@DOT_REPORT_QTY", dbNull(r.Dot_report_qty));
                                cmd_msds_ingredients.Parameters.AddWithValue("@PRCNT_VOL_VALUE", dbNull(r.Prcnt_vol_value));
                                cmd_msds_ingredients.Parameters.AddWithValue("@PRCNT_VOL_WEIGHT", dbNull(r.Prcnt_vol_weight));
                                cmd_msds_ingredients.Parameters.AddWithValue("@CHEM_MFG_COMP_NAME", dbNull(r.Chem_mfg_comp_name));
                                cmd_msds_ingredients.Parameters.AddWithValue("@ODS_IND", dbNull(r.Ods_ind));
                                cmd_msds_ingredients.Parameters.AddWithValue("@OTHER_REC_LIMITS", dbNull(r.Other_rec_limits));

                                //Execute the query.
                                cmd_msds_ingredients.ExecuteNonQuery();
                                cmd_msds_ingredients.Parameters.Clear();
                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_INGREDIENTS table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS Contractor Info Insert
                            try
                            {
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_NUMBER", dbNull(r.Ct_number));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_CAGE", dbNull(r.Ct_cage));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_CITY", dbNull(r.Ct_city));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_COMPANY_NAME", dbNull(r.Ct_company_name));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_COUNTRY", dbNull(r.Ct_country));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_PO_BOX", dbNull(r.Ct_po_box));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_PHONE", dbNull(r.Ct_phone));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@PURCHASE_ORDER_NO", dbNull(r.Purchase_order_no));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@CT_STATE", dbNull(r.Ct_state));
                                cmd_msds_contractor_info.Parameters.AddWithValue("@msds_id", msds_id);

                                //Execute the query.
                                cmd_msds_contractor_info.ExecuteNonQuery();
                                cmd_msds_contractor_info.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_CONTRACTOR. Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS AFJM PSN Insert
                            try
                            {
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(r.Afjm_hazard_class));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(r.Afjm_pack_paragraph));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(r.Afjm_pack_group));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(r.Afjm_proper_ship_name));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(r.Afjm_proper_ship_modifier));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(r.Afjm_psn_code));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(r.Afjm_special_prov));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(r.Afjm_subsidiary_risk));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(r.Afjm_symbols));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(r.Afjm_un_id_number));
                                cmd_msds_afjm_psn.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_afjm_psn.ExecuteNonQuery();
                                cmd_msds_afjm_psn.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_AFJM_PSN table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS DOT PSN Insert
                            try
                            {
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(r.Dot_hazard_class_div));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(r.Dot_hazard_label));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(r.Dot_max_cargo));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(r.Dot_max_passenger));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(r.Dot_pack_bulk));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(r.Dot_pack_exceptions));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(r.Dot_pack_nonbulk));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(r.Dot_prop_ship_name));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(r.Dot_prop_ship_modifier));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(r.Dot_psn_code));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(r.Dot_special_provision));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(r.Dot_symbols));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(r.Dot_un_id_number));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(r.Dot_water_other_req));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(r.Dot_water_vessel_stow));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(r.Dot_pack_group));
                                cmd_msds_dot_psn.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_dot_psn.ExecuteNonQuery();
                                cmd_msds_dot_psn.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_DOT_PSN table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }

                            #endregion

                            #region MSDS IATA PSN Insert
                            try
                            {
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(r.Iata_cargo_packing));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(r.Iata_hazard_class));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(r.Iata_hazard_label));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(r.Iata_packing_group));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(r.Iata_pass_air_pack_lmt_instr));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(r.Iata_pass_air_pack_lmt_per_pkg));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(r.Iata_pass_air_pack_note));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(r.Iata_prop_ship_name));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(r.Iata_prop_ship_modifier));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(r.Iata_pass_cargo_pack_max_qty));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(r.Iata_psn_code));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(r.Iata_pass_air_max_qty));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(r.Iata_special_prov));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(r.Iata_subsidiary_risk));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(r.Iata_un_id_number));
                                cmd_msds_iata_psn.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_iata_psn.ExecuteNonQuery();
                                cmd_msds_iata_psn.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_IATA_PSN table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS IMO PSN Insert
                            try
                            {
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(r.Imo_ems_no));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(r.Imo_hazard_class));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(r.Imo_ibc_instr));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(r.Imo_limited_qty));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(r.Imo_pack_group));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(r.Imo_pack_instructions));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(r.Imo_pack_provisions));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(r.Imo_prop_ship_name));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(r.Imo_prop_ship_modifier));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(r.Imo_psn_code));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(r.Imo_special_prov));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(r.Imo_stow_segr));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(r.Imo_subsidiary_risk));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(r.Imo_tank_instr_imo));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(r.Imo_tank_instr_provisions));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(r.Imo_tank_instr_un));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(r.Imo_un_number));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(r.Imo_ibc_provisions));
                                cmd_msds_imo_psn.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_imo_psn.ExecuteNonQuery();
                                cmd_msds_imo_psn.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_IMO_PSN table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS Item Description Insert
                            try
                            {
                                cmd_msds_item_description.Parameters.AddWithValue("@msds_id", msds_id);
                                cmd_msds_item_description.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(r.Item_manager));
                                cmd_msds_item_description.Parameters.AddWithValue("@ITEM_NAME", dbNull(r.Item_name));
                                cmd_msds_item_description.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(r.Specification_number));
                                cmd_msds_item_description.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(r.Type_grade_class));
                                cmd_msds_item_description.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(r.Unit_of_issue));
                                cmd_msds_item_description.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(r.Quantitative_expression));
                                cmd_msds_item_description.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(r.Ui_container_qty));
                                cmd_msds_item_description.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(r.Type_of_container));
                                cmd_msds_item_description.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(r.Batch_number));
                                cmd_msds_item_description.Parameters.AddWithValue("@LOT_NUMBER", dbNull(r.Lot_number));
                                cmd_msds_item_description.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(r.Log_flis_niin_ver));
                                cmd_msds_item_description.Parameters.AddWithValue("@LOG_FSC", dbNull(r.Log_fsc));
                                cmd_msds_item_description.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(r.Net_unit_weight));
                                cmd_msds_item_description.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(r.Shelf_life_code));
                                cmd_msds_item_description.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(r.Special_emp_code));
                                cmd_msds_item_description.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(r.Un_na_number));
                                cmd_msds_item_description.Parameters.AddWithValue("@UPC_GTIN", dbNull(r.Upc_gtin));

                                cmd_msds_item_description.ExecuteNonQuery();
                                cmd_msds_item_description.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_ITEM_DESC table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }

                            #endregion

                            #region MSDS Phys Chemical Insert
                            try
                            {
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@msds_id", msds_id);
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(r.Vapor_press));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@VAPOR_DENS", dbNull(r.Vapor_dens));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(r.Specific_grav));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(r.Voc_pounds_gallon));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(r.Voc_grams_liter));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@PH", dbNull(r.Ph));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@VISCOSITY", dbNull(r.Viscosity));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(r.Evap_rate_ref));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(r.Sol_in_water));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@APP_ODOR", dbNull(r.App_odor));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(r.Percent_vol_volume));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(r.Autoignition_temp));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(r.Carcinogen_ind));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@EPA_ACUTE", dbNull(r.Epa_acute));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(r.Epa_chronic));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@EPA_FIRE", dbNull(r.Epa_fire));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(r.Epa_pressure));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(r.Epa_reactivity));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(r.Flash_point_temp));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@NEUT_AGENT", dbNull(r.Neut_agent));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(r.Nfpa_flammability));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(r.Nfpa_health));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(r.Nfpa_reactivity));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(r.Nfpa_special));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(r.Osha_carcinogens));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(r.Osha_comb_liquid));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(r.Osha_comp_gas));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(r.Osha_corrosive));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(r.Osha_explosive));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(r.Osha_flammable));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(r.Osha_high_toxic));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(r.Osha_irritant));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(r.Osha_org_perox));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(r.Osha_otherlongterm));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(r.Osha_oxidizer));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_PYRO", dbNull(r.Osha_pyro));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(r.Osha_sensitizer));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(r.Osha_toxic));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(r.Osha_unst_react));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(r.Other_short_term));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(r.Phys_state_code));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(r.Vol_org_comp_wt));
                                cmd_msds_phys_chemical.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(r.Osha_water_reac));

                                cmd_msds_phys_chemical.ExecuteNonQuery();
                                cmd_msds_phys_chemical.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_Phys_Chem table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS TRANSPORATION Insert
                            try
                            {
                                cmd_msds_transportation.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(r.Af_mmac_code));
                                cmd_msds_transportation.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(r.Certificate_coe));
                                cmd_msds_transportation.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(r.Competent_caa));
                                cmd_msds_transportation.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(r.Dod_id_code));
                                cmd_msds_transportation.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(r.Dot_exemption_no));
                                cmd_msds_transportation.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(r.Dot_rq_ind));
                                cmd_msds_transportation.Parameters.AddWithValue("@EX_NO", dbNull(r.Ex_no));
                                cmd_msds_transportation.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(r.Flash_pt_temp));
                                cmd_msds_transportation.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(r.High_explosive_wt));
                                cmd_msds_transportation.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(r.Ltd_qty_ind));
                                cmd_msds_transportation.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(r.Magnetic_ind));
                                cmd_msds_transportation.Parameters.AddWithValue("@MAGNETISM", dbNull(r.Magnetism));
                                cmd_msds_transportation.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(r.Marine_pollutant_ind));
                                cmd_msds_transportation.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(r.Net_exp_qty_dist));
                                cmd_msds_transportation.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(r.Net_exp_weight));
                                cmd_msds_transportation.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(r.Net_propellant_wt));
                                cmd_msds_transportation.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(r.Nos_technical_shipping_name));
                                cmd_msds_transportation.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(r.Transportation_additional_data));
                                cmd_msds_transportation.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_transportation.ExecuteNonQuery();
                                cmd_msds_transportation.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_TRANSPORTATION table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region MSDS LABEL INFO Insert
                            try
                            {
                                cmd_msds_label_info.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(r.Company_cage_rp));
                                cmd_msds_label_info.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(r.Company_name_rp));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(r.Label_emerg_phone));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(r.Label_item_name));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(r.Label_proc_year));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(r.Label_prod_ident));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(r.Label_prod_serialno));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_SIGNAL_WORD", dbNull(r.Label_signal_word));
                                cmd_msds_label_info.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(r.Label_stock_no));
                                cmd_msds_label_info.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(r.Specific_hazards));
                                cmd_msds_label_info.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_label_info.ExecuteNonQuery();
                                cmd_msds_label_info.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_LABEL_INFO table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }

                            #endregion

                            #region MSDS DOCUMENT TYPES Insert
                            try
                            {
                                //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                                //Get file as a byte array.
                                if (r.Msds_translated != null)
                                {
                                    byte[] rawFile = File.ReadAllBytes(r.Msds_translated);
                                    cmd_msds_document_types.Parameters.AddWithValue("@MSDS_TRANSLATED", rawFile);
                                }
                                else
                                {
                                    cmd_msds_document_types.Parameters.AddWithValue("@MSDS_TRANSLATED", new byte[0]);
                                }

                                //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                                //Get file as a byte array.
                                if (r.Manufacturer_label != null)
                                {
                                    byte[] rawFile = File.ReadAllBytes(r.Manufacturer_label);
                                    cmd_msds_document_types.Parameters.AddWithValue("@MANUFACTURER_LABEL", rawFile);
                                }
                                else
                                {
                                    cmd_msds_document_types.Parameters.AddWithValue("@MANUFACTURER_LABEL", new byte[0]);
                                }

                                //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                                //Get file as a byte array.
                                if (r.Neshap_compliance_cert != null)
                                {
                                    byte[] rawFile = File.ReadAllBytes(r.Neshap_compliance_cert);
                                    cmd_msds_document_types.Parameters.AddWithValue("@NESHAP_COMP_CERT", rawFile);
                                }
                                else
                                {
                                    cmd_msds_document_types.Parameters.AddWithValue("@NESHAP_COMP_CERT", new byte[0]);
                                }


                                //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                                //Get file as a byte array.
                                if (r.Other_docs != null)
                                {
                                    byte[] rawFile = File.ReadAllBytes(r.Other_docs);
                                    cmd_msds_document_types.Parameters.AddWithValue("@OTHER_DOCS", rawFile);
                                }
                                else
                                {
                                    cmd_msds_document_types.Parameters.AddWithValue("@OTHER_DOCS", new byte[0]);
                                }

                                //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                                //Get file as a byte array.
                                if (r.Product_sheet != null)
                                {
                                    byte[] rawFile = File.ReadAllBytes(r.Product_sheet);
                                    cmd_msds_document_types.Parameters.AddWithValue("@PRODUCT_SHEET", rawFile);
                                }
                                else
                                {
                                    cmd_msds_document_types.Parameters.AddWithValue("@PRODUCT_SHEET", new byte[0]);
                                }

                                //Get the ACTUAL MSDS files associated with this record for HILT DB insertion.
                                //Get file as a byte array.
                                if (r.Transportation_cert != null)
                                {
                                    byte[] rawFile = File.ReadAllBytes(r.Transportation_cert);
                                    cmd_msds_document_types.Parameters.AddWithValue("@TRANSPORTATION_CERT", rawFile);
                                }
                                else
                                {
                                    cmd_msds_document_types.Parameters.AddWithValue("@TRANSPORTATION_CERT", new byte[0]);
                                }

                                cmd_msds_document_types.Parameters.AddWithValue("@msds_translated_filename", dbNull(r.Msds_translated));
                                cmd_msds_document_types.Parameters.AddWithValue("@neshap_comp_filename", dbNull(r.Neshap_compliance_cert));
                                cmd_msds_document_types.Parameters.AddWithValue("@other_docs_filename", dbNull(r.Other_docs));
                                cmd_msds_document_types.Parameters.AddWithValue("@product_sheet_filename", dbNull(r.Product_sheet));
                                cmd_msds_document_types.Parameters.AddWithValue("@transportation_cert_filename", dbNull(r.Transportation_cert));
                                cmd_msds_document_types.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(r.Manufacturer_label));
                                cmd_msds_document_types.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_document_types.ExecuteNonQuery();
                                cmd_msds_document_types.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_DOC_TYPES table.  Error: " + ex.Message);
                                break;
                            }
                            #endregion

                            #region MSDS DISPOSAL Insert
                            try
                            {
                                cmd_msds_disposal.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(r.Disposal_add_info));
                                cmd_msds_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(r.Epa_haz_waste_code));
                                cmd_msds_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(r.Epa_haz_waste_ind));
                                cmd_msds_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(r.Epa_haz_waste_name));
                                cmd_msds_disposal.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_disposal.ExecuteNonQuery();
                                cmd_msds_disposal.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_DISPOSAL table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                            #region RADIOLOGICAL INFO Insert
                            try
                            {
                                cmd_msds_radiological_info.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(r.Nrc_lp_num));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@OPERATOR", dbNull(r.TheOperator));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(r.Rad_amount_micro));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@RAD_FORM", dbNull(r.Rad_form));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@RAD_CAS", dbNull(r.Rad_cas));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@RAD_NAME", dbNull(r.Rad_name));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(r.Rad_symbol));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@REP_NSN", dbNull(r.Rep_nsn));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@SEALED", dbNull(r.IsSealed));
                                cmd_msds_radiological_info.Parameters.AddWithValue("@msds_id", msds_id);

                                cmd_msds_radiological_info.ExecuteNonQuery();
                                cmd_msds_radiological_info.Parameters.Clear();

                            }
                            catch (Exception ex)
                            {
                                this.writeToErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occured inserting into the MSDS_RADIOLOGICAL_INFO table.  Error: " + ex.Message);
                                this.validInsert = false;
                            }
                            #endregion

                        } //End of if(msds_id != -1)

                        //Progress bar stuff
                        counter++;

                        if (counter % 200 == 0)
                        {
                            listener.updateProgress(++progress);
                        }

                        try
                        {
                            if (validInsert == true)
                            {
                                //Commit the transaction record by record
                                //if the record is valid.
                                tran.Commit();
                                this.recordsInserted++;
                                tran.Dispose();
                            }
                            else
                            {
                                tran.Dispose();
                            }
                        }
                        catch (Exception ex)
                        {
                            this.writeToRecordInsertErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "Error occurred committing the transaction. Error: " + ex.Message);
                        }
                    } //End of first if niin for the record is null check.
                    else
                    {
                        this.writeToRecordInsertErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "The NIIN for this record does not correspond to a NIIN value in the NIIN Catalog:  " + r.Niin);
                        tran.Dispose();
                    }
                } // End of if niin does not exist in the niin catalog.
                else
                {
                    this.writeToRecordInsertErrorLog(r.ParsedFileName, r.MsdsSerNo, this.importErrorFile, "The NIIN for this record is invalid or blank:  " + r.Niin);
                    tran.Dispose();
                }
            } //End of record looping.

            //Shut it down.
            this.recordInsertStats(this.recordsInsertedFile, recordList.Count, this.recordsInserted);
            this.conn.Close();
        }
        #endregion

        /* Splits a record with multiple part numbers into individual msds records
         * 
         */
        #region splitCTRecord(Record record)
        private List<Record> splitRecord(Record record) {
            List<Record> records = new List<Record>();

            // See if the record's CT_CAGE contains a comma
            if (record.Ct_cage.Contains(",")) {
                // Need to create multiple records based on part #
                // Get the ct_cage
                String[] ct_Cages = record.Ct_cage.Split(',');
                // Get the ct_numbers
                String[] ct_Numbers = record.Ct_number.Split(',');
                // Get the ct_cities
                String[] ct_Cities = record.Ct_city.Split(',');
                // Get the ct_company_names
                String[] ct_Company_Names = record.Ct_company_name.Split(',');
                // Get the ct_countries
                String[] ct_Countries = record.Ct_country.Split(',');
                // Get the ct_PO_box
                String[] ct_PO_Boxes = record.Ct_po_box.Split(',');
                // Get the ct_phone
                String[] ct_Phones = record.Ct_phone.Split(',');
                // Get the purchase_order_numbers
                String[] poNumbers = record.Purchase_order_no.Split(',');
            }
            else {
                // Single Part #
                records.Add(record);
            }

            return records;
        }
        #endregion

        #endregion


        /* Methods for logging valuable information about the
         * Export/Import process.  These logs also allow for the 
         * program to run, even when it encounters errors.  It simply
         * stores the errors and information about the failed record
         * recovery or db insertion for later use.
         */
        #region LOGGING
        //Writes to the error log.
        Boolean firstTimeLogWriter = true;

        // Method for storing errors encountered during the File Parsing process.
        private void writeToErrorLog(String parsedFile, String serNo, FileInfo file, String errMsg)
        {
            //Check if this is our first time to append to the log during this process.
            if(this.firstTimeLogWriter == true) {
                StreamWriter ft = File.AppendText(file.FullName);
                ft.WriteLine("DATE\t" + "SERIAL NO\t" + "FILENAME\t" + "ERROR MESSAGE");
                ft.WriteLine("");
                ft.Close();

                this.firstTimeLogWriter = false;
            }

            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine(date + "\t" + serNo + "\t" + parsedFile + "\t" + errMsg);
            sw.Close();
        }

        Boolean firstTimeParserStats = true;

        //Writes File Parser stats to the error log.
        private void fileParserStats(FileInfo file, String filename, int total, int myRecords, int valid, int invalid) 
        {
            //Check if this is our first time to append to the log during this process.
            if (this.firstTimeParserStats == true) {
                StreamWriter ft = File.AppendText(file.FullName);
                ft.WriteLine("DATE\t" + "FILENAME\t" + "TOTAL RECORDS\t" + "MY RECORDS\t" + "VALID RECORDS\t" + "INVALID RECORDS");
                ft.WriteLine("");
                ft.Close();

                this.firstTimeParserStats = false;
            }

            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine(date + "\t" + filename + "\t" + total + "\t" + myRecords + "\t" + valid + "\t" + invalid);
            sw.Close();
        }

        Boolean firstTimeRecordInsert = true;

        // Method for storing errors encountered during the HILT DB insertion process.
        private void writeToRecordInsertErrorLog(String parsedFile, String serNo, FileInfo file, String errMsg)
        {
            //Check if this is our first time to append to the log during this process.
            if (this.firstTimeRecordInsert == true) {
                StreamWriter ft = File.AppendText(file.FullName);
                ft.WriteLine("DATE\t" + "SERIAL NO\t" + "FILENAME\t" + "ERROR MESSAGE");
                ft.WriteLine("");
                ft.Close();

                this.firstTimeRecordInsert = false;
            }

            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine(date + "\t" + serNo + "\t" + parsedFile + "\t" + errMsg);
            sw.Close();
        }

        // Method for storing stats about the HILT DB insertion process.
        private void recordInsertStats(FileInfo file, int totalRecords, int insertedRecords) {
            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine("DATE\t" + "TOTAL RECORDS\t" + "INSERTED RECORDS\t" + "RECORDS NOT INSERTED");
            sw.WriteLine(date + "\t" + totalRecords + "\t" + insertedRecords + "\t" + (totalRecords - insertedRecords));
            sw.Close();
        }

        Boolean firstTimeExport = true;

        // Method for storing errors encountered during the HMIRS db export process.
        private void writeToExportErrLog(FileInfo file, String errMsg)
        {
            //Check if this is our first time to append to the log during this process.
            if (this.firstTimeExport == true) {
                StreamWriter ft = File.AppendText(file.FullName);
                ft.WriteLine("DATE\t" + "ERROR MESSAGE");
                ft.WriteLine("");
                ft.Close();

                this.firstTimeExport = false;
            }

            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine(date + "\t" + errMsg);
            sw.Close();
        }

        #endregion

        /* Methods for connecting to and a db value insertion
         * validation methods.
         */
        #region Database Methods
        protected string getConnectionString(string host, int port, string user, string pass) {
            return "Data Source=" + host + ";Initial Catalog=ashore;Integrated Security=False;User Id=" + user + ";PWD= " + pass + ";Min pool size=5;Max pool size=100;connection timeout=15;pooling=yes";
        }

        protected SqlConnection createConnection(string host, int port, string user, string pass)  {
            SqlConnection conn = new SqlConnection(this.getConnectionString(host, port, user, pass));
            return conn;
        }

        /* Method created by Adam Hale to check a value before db insertion.
         * If this object is blank or null it replaces it with a DBNull value.
         */
        private object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            } else {
                return DBNull.Value;
            }
        }
        #endregion

        #endregion Methods

        #region Getters and Setters
        public Boolean ErrorFlag
        {
            get { return errorFlag; }
            set { errorFlag = value; }
        }

        public int ImportProgessBar
        {
            get { return importProgessBar; }
            set { importProgessBar = value; }
        }

        public int ExportProgressBar
        {
            get { return exportProgressBar; }
            set { exportProgressBar = value; }
        }

        public FileInfo ParsedRecordsFile
        {
            get { return parsedRecordsFile; }
            set { parsedRecordsFile = value; }
        }

        public FileInfo ParserErrorFile
        {
            get { return parserErrorFile; }
            set { parserErrorFile = value; }
        }

        public string MsdsPath
        {
            get { return msdsPath; }
            set { msdsPath = value; }
        }

        public Boolean ValidInsert
        {
            get { return validInsert; }
            set { validInsert = value; }
        }

        public FileInfo RecordsInsertedFile
        {
            get { return recordsInsertedFile; }
            set { recordsInsertedFile = value; }
        }

        public int RecordsInserted
        {
            get { return recordsInserted; }
            set { recordsInserted = value; }
        }

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        public int ValidRecordCount
        {
            get { return validRecordCount; }
            set { validRecordCount = value; }
        }


        public int InvalidRecordCount
        {
            get { return invalidRecordCount; }
            set { invalidRecordCount = value; }
        }

        public Boolean ValidRecord
        {
            get { return validRecord; }
            set { validRecord = value; }
        }

        public FileInfo ExportErrorFile
        {
            get { return exportErrorFile; }
            set { exportErrorFile = value; }
        }

        public FileInfo ImportErrorFile
        {
            get { return importErrorFile; }
            set { importErrorFile = value; }
        }

        public List<Record> RecordsMSDS
        {
            get { return recordsMSDS; }
            set { recordsMSDS = value; }
        }

        public String[] FilePaths
        {
            get { return filePaths; }
            set { filePaths = value; }
        }


        public int BatchSize
        {
            get { return batchSize; }
        }

        public String TempPath
        {
            get { return tempPath; }
            set { tempPath = value; }
        }

        public String Password
        {
            get { return password; }
            set { password = value; }
        }

        public String Username
        {
            get { return username; }
            set { username = value; }
        }

        public int Port
        {
            get { return port; }
            set { port = value; }
        }


        public String ServerPath
        {
            get { return serverPath; }
            set { serverPath = value; }
        }


        public String CdOnlinePath
        {
            get { return cdOnlinePath; }
            set { cdOnlinePath = value; }
        }

        public SqlConnection Conn
        {
            get { return conn; }
            set { conn = value; }
        }

        #endregion

    } //End Importer class.
        
    // Class to handle the parent/child processes spawned by cdonline.exe
    #region OnlineCDProcess inner class
    public class OnlineCDProcess {

        private System.Diagnostics.Process mainProcess;
        private System.Diagnostics.Process subProcess;

        public System.Diagnostics.Process SubProcess
        {
            get { return subProcess; }
            set { subProcess = value; }
        }

        public System.Diagnostics.Process MainProcess
        {
            get { return mainProcess; }
            set { mainProcess = value; }
        }
        
    
    }
#endregion

}
