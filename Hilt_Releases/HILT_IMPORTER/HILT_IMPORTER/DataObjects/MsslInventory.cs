using System;
using System.Collections.Generic;

namespace HILT_IMPORTER.DataObjects {
    public class MsslInventory {
        private DateTime shelf_life_expiration_date;

        private List<MsslSubs> subs = new List<MsslSubs>();
        private int atc;


        private string niin;

        public string Niin {
            get { return niin; }
            set { niin = value; }
        }
        private string cosal;

        public string Cosal {
            get { return cosal; }
            set { cosal = value; }
        }
        private string cog;

        public string Cog {
            get { return cog; }
            set { cog = value; }
        }
        private string mcc;

        public string Mcc {
            get { return mcc; }
            set { mcc = value; }
        }
        private string ui;

        public string Ui {
            get { return ui; }
            set { ui = value; }
        }
        private double up;

        public double Up {
            get { return up; }
            set { up = value; }
        }
        private string location;

        public string Location {
            get { return location; }
            set { location = value; }
        }

        private string lmc;

        public string Lmc {
            get { return lmc; }
            set { lmc = value; }
        }
        private string irc;

        public string Irc {
            get { return irc; }
            set { irc = value; }
        }
        private int dues;

        public int Dues {
            get { return dues; }
            set { dues = value; }
        }
        private int ro;

        public int Ro {
            get { return ro; }
            set { ro = value; }
        }
        private int rp;

        public int Rp {
            get { return rp; }
            set { rp = value; }
        }
        private double amd;

        public double Amd {
            get { return amd; }
            set { amd = value; }
        }
        private string smic;

        public string Smic {
            get { return smic; }
            set { smic = value; }
        }
        private string slc;

        public string Slc {
            get { return slc; }
            set { slc = value; }
        }
        private string slac;

        public string Slac {
            get { return slac; }
            set { slac = value; }
        }
        private string smcc;

        public string Smcc {
            get { return smcc; }
            set { smcc = value; }
        }
        private string nomenclature;

        public string Nomenclature {
            get { return nomenclature; }
            set { nomenclature = value; }
        }
        private double netup;

        public double Netup {
            get { return netup; }
            set { netup = value; }
        }
        private string arrc;

        public string Arrc {
            get { return arrc; }
            set { arrc = value; }
        }
        private string limitFlag;

        public string LimitFlag {
            get { return limitFlag; }
            set { limitFlag = value; }
        }
        private string noDropInd;

        public string NoDropInd {
            get { return noDropInd; }
            set { noDropInd = value; }
        }

        public int Atc {
            get { return atc; }
            set { atc = value; }
        }

        public List<MsslSubs> Subs {
            get { return subs; }
            set { subs = value; }
        }

        public DateTime Shelf_life_expiration_date {
            get { return shelf_life_expiration_date; }
            set { shelf_life_expiration_date = value; }
        }
        private int location_id;

        public int Location_id {
            get { return location_id; }
            set { location_id = value; }
        }
        private int qty;

        public int Qty {
            get { return qty; }
            set { qty = value; }
        }
        private string serial_number;

        public string Serial_number {
            get { return serial_number; }
            set { serial_number = value; }
        }
        private bool bulk_item;

        public bool Bulk_item {
            get { return bulk_item; }
            set { bulk_item = value; }
        }

        private MsslMfgCatalog mfg_catalog;

        public MsslMfgCatalog Mfg_catalog {
            get { return mfg_catalog; }
            set { mfg_catalog = value; }
        }

        private DateTime manufacturer_date;

        public DateTime Manufacturer_date {
            get { return manufacturer_date; }
            set { manufacturer_date = value; }
        }
        private string ati;

        public string Ati {
            get { return ati; }
            set { ati = value; }
        }

        public DateTime	created {
	        get;
	        set;
        }
        public String	created_by {
	        get;
	        set;
        }
        public DateTime	changed {
	        get;
	        set;
        }
        public String changed_by {
            get;
            set;
        }
    }
}
