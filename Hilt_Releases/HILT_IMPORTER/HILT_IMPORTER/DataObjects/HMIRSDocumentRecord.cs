﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HILT_IMPORTER.DataObjects {
    public class HMIRSDocumentRecord {
        // Build the doc file name
        // Format: MSDSSERNO_DocType_RevisionDate_Country_Language_DocStatusCode.DocFormat
        public int disk_number { get; set; }

        public string doc_file_name { get; set; }

        #region document file name fields
        public long doc_data_id { get; set; }

        public string document_type { get; set; }

        public DateTime revision_date { get; set; }

        public string country_code { get; set; }

        public string language_code { get; set; }

        public string doc_status_code { get; set; }

        public string doc_format { get; set; }
        #endregion

        public int revision_num { get; set; }

        public string proprietary_ind { get; set; }

        public string manufacturer_serial_num { get; set; }

        public int manufacturer_revision_num { get; set; }

        public string doc_group_num { get; set; } // MSDS Serial #
    }
}
