﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HILT_IMPORTER.DataObjects {
    public class MSDSUpdateInfo {
        public string table_name { get; set; }

        public long last_id_value { get; set; }

        public string last_cd_value { get; set; }

        public DateTime last_update { get; set; }
    }
}
