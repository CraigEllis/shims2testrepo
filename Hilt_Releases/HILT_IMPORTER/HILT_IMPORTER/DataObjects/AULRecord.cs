using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class AULRecord {
        #region Constructors
        // Default constructor
        public AULRecord() {
        }

        // Constructor using view models as input

        #endregion

        #region Properties
        public long aul_id {
	        get;
	        set;
        }
        public long hull_type_id {
            get;
            set;
        }
        public long fsc {
	        get;
	        set;
        }
        public String	niin {
	        get;
	        set;
        }
        public String	ui {
	        get;
	        set;
        }
        public String	um {
	        get;
	        set;
        }
        public long usage_category_id {
            get;
            set;
        }
        public String description {
	        get;
	        set;
        }
        public long smcc_id {
            get;
            set;
        }
        public String specs {
	        get;
	        set;
        }
        public long shelf_life_code_id {
            get;
            set;
        }
        public long shelf_life_action_code_id {
            get;
            set;
        }
        public String remarks {
	        get;
	        set;
        }
        public long storage_type_id {
            get;
            set;
        }
        public long cog_id {
            get;
            set;
        }
        public String spmig {
	        get;
	        set;
        }
        public String	nehc_rpt {
	        get;
	        set;
        }
        public long catalog_group_id {
            get;
            set;
        }
        public String catalog_serial_number {
	        get;
	        set;
        }
        public long	allowance_qty {
	        get;
	        set;
        }
        public bool	manually_entered {
	        get;
	        set;
        }
        public bool	dropped_in_error {
	        get;
	        set;
        }
        public String	manufacturer {
	        get;
	        set;
        }
        public String	cage {
	        get;
	        set;
        }
        public String msds_num {
            get;
            set;
        }
        // SHML-specific
        public String qup {
            get;
            set;
        }
        public String aac {
            get;
            set;
        }
        // Treat this as a string rather than a decimal #
        public String price_per_ui {
            get;
            set;
        }
        public String smic {
            get;
            set;
        }
        public String data_source_cd {
	        get;
	        set;
        }
        public DateTime	created {
	        get;
	        set;
        }
        public String created_by {
            get;
            set;
        }
        public DateTime changed {
	        get;
	        set;
        }
        public String changed_by {
            get;
            set;
        }
        #endregion

        #region Support Table Values
        public String Hull_Type {
            get;
            set;
        }
        public String Usage_Category {
            get;
            set;
        }
        public String SMCC {
            get;
            set;
        }
       public String Shelf_Life_Code {
            get;
            set;
        }
        public String Shelf_Life_Action_Code {
            get;
            set;
        }
        public String Storage_Type {
            get;
            set;
        }
        public String COG {
            get;
            set;
        }
        public String Catalog_Group {
            get;
            set;
        }
        #endregion
    }

    public class AULFile {
        public AULFile() {
            FileStats = new AULStats();
        }
        public String FileName { get; set; }

        public DateTime FileDate { get; set; }

        public AULImporter.AUL_TYPE FileType { get; set; }

        public String[] HeaderRecord { get ; set; }

        public List<String[]> dataRows;
        public List<String[]> DataRows { 
            get {
                if (dataRows == null)
                    dataRows = new List<string[]>();

                return dataRows;
            }
            set {
                dataRows = value;
                ; 
            }
        }

        public List<String> newMasterNIINs;
        public List<String> NewMasterNIINs { 
            get {
                if (newMasterNIINs == null)
                    newMasterNIINs = new List<String>();

                return newMasterNIINs;
            }
            set {
                newMasterNIINs = value;
                ; 
            }
        }
        public AULStats FileStats { get; set; }
    }

    public class AULStats {
        private int newMasterRecords = 0;
        public int NewMasterRecords {
            get {
                return newMasterRecords;
            }
            set {
                newMasterRecords = value;
            }
        }

        private int newRecords = 0;
        public int NewRecords {
            get {
                return newRecords;
            }
            set {
                newRecords = value;
            }
        }

        private int changedRecords = 0;
        public int ChangedRecords {
            get {
                return changedRecords;
            }
            set {
                changedRecords = value;
            }
        }

        private int errorRecords = 0;
        public int ErrorRecords {
            get {
                return errorRecords;
            }
            set {
                errorRecords = value;
            }
        }

        private int existingRecords = 0;
        public int ExistingRecords {
            get {
                return existingRecords;
            }
            set {
                existingRecords = value;
            }
        }

        private int deletedRecords = 0;
        public int DeletedRecords {
            get {
                return deletedRecords;
            }
            set {
                deletedRecords = value;
            }
        }

        private int duplicateRecords = 0;
        public int DuplicateRecords {
            get {
                return duplicateRecords;
            }
            set {
                duplicateRecords = value;
            }
        }

        private int blankRecords = 0;
        public int BlankRecords {
            get {
                return blankRecords;
            }
            set {
                blankRecords = value;
            }
        }

        private int totalRecords = 0;
        public int TotalRecords {
            get {
                return totalRecords;
            }
            set {
                totalRecords = value;
            }
        }

    }
}
