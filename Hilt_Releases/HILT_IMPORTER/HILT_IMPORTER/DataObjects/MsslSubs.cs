﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class MsslSubs {
        private string niin;

        public string Niin {
            get {
                return niin;
            }
            set {
                niin = value;
            }
        }
        private string sub_niin;

        public string Sub_niin {
            get {
                return sub_niin;
            }
            set {
                sub_niin = value;
            }
        }
    }
}