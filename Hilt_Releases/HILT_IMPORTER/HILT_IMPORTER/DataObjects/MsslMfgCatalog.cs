﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class MsslMfgCatalog {

        private string cage;

        public string Cage {
            
            get {
                return cage;
            }
            
            set {
                cage = value;
            }
        }
        private string manufacturer;

        public string Manufacturer {
            
            get {
                return manufacturer;
            }
            
            set {
                manufacturer = value;
            }
        }
        //private MsslNiinCatalog niin_catalog;  // Never used anywhere

        //public MsslNiinCatalog Niin_catalog {
        //    
        //    get {
        //        return niin_catalog;
        //    }
        //    
        //    set {
        //        niin_catalog = value;
        //    }
        //}
        private int hazard_id;

        public int Hazard_id {
            
            get {
                return hazard_id;
            }
            
            set {
                hazard_id = value;
            }
        }
    }
}