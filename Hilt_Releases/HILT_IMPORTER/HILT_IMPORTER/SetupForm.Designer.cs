﻿namespace HILT_IMPORTER
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.label1 = new System.Windows.Forms.Label();
            this.txtOnlineCDPath = new System.Windows.Forms.TextBox();
            this.btnBrowseCDOnline = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.msdsGroupBox = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMSDSFilesPath = new System.Windows.Forms.TextBox();
            this.btnBrowseMSDSFiles = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkLocalMachine = new System.Windows.Forms.CheckBox();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkPassword = new System.Windows.Forms.CheckBox();
            this.txtPasswordBox = new System.Windows.Forms.TextBox();
            this.txtUsernameBox = new System.Windows.Forms.TextBox();
            this.txtPortBox = new System.Windows.Forms.TextBox();
            this.txtServerBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtAULFilePath = new System.Windows.Forms.TextBox();
            this.aulGroupBox = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnBrowseAULFiles = new System.Windows.Forms.Button();
            this.restoreDefault = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.smclOptions = new System.Windows.Forms.CheckedListBox();
            this.msdsGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.aulGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Default CD Folder:";
            this.label1.Visible = false;
            // 
            // txtOnlineCDPath
            // 
            this.txtOnlineCDPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOnlineCDPath.Location = new System.Drawing.Point(110, 35);
            this.txtOnlineCDPath.Name = "txtOnlineCDPath";
            this.txtOnlineCDPath.Size = new System.Drawing.Size(292, 20);
            this.txtOnlineCDPath.TabIndex = 2;
            this.txtOnlineCDPath.Text = "C:\\HMIRS Importer";
            this.toolTip1.SetToolTip(this.txtOnlineCDPath, "The folder where the HMIRS OnlineCd application and data are located");
            this.txtOnlineCDPath.Visible = false;
            // 
            // btnBrowseCDOnline
            // 
            this.btnBrowseCDOnline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseCDOnline.Location = new System.Drawing.Point(408, 33);
            this.btnBrowseCDOnline.Name = "btnBrowseCDOnline";
            this.btnBrowseCDOnline.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseCDOnline.TabIndex = 3;
            this.btnBrowseCDOnline.Text = "Browse...";
            this.btnBrowseCDOnline.UseVisualStyleBackColor = true;
            this.btnBrowseCDOnline.Visible = false;
            this.btnBrowseCDOnline.Click += new System.EventHandler(this.btnBrowseCDOnline_Click);
            // 
            // msdsGroupBox
            // 
            this.msdsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.msdsGroupBox.Controls.Add(this.label8);
            this.msdsGroupBox.Controls.Add(this.label7);
            this.msdsGroupBox.Controls.Add(this.label6);
            this.msdsGroupBox.Controls.Add(this.txtMSDSFilesPath);
            this.msdsGroupBox.Controls.Add(this.btnBrowseMSDSFiles);
            this.msdsGroupBox.Controls.Add(this.label1);
            this.msdsGroupBox.Controls.Add(this.txtOnlineCDPath);
            this.msdsGroupBox.Controls.Add(this.btnBrowseCDOnline);
            this.msdsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.msdsGroupBox.Name = "msdsGroupBox";
            this.msdsGroupBox.Size = new System.Drawing.Size(494, 77);
            this.msdsGroupBox.TabIndex = 0;
            this.msdsGroupBox.TabStop = false;
            this.msdsGroupBox.Text = "MSDS Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "SHIMS Import";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "HMIRS Extract";
            this.label7.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "MSDS Files Folder:";
            // 
            // txtMSDSFilesPath
            // 
            this.txtMSDSFilesPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMSDSFilesPath.Location = new System.Drawing.Point(110, 35);
            this.txtMSDSFilesPath.Name = "txtMSDSFilesPath";
            this.txtMSDSFilesPath.Size = new System.Drawing.Size(292, 20);
            this.txtMSDSFilesPath.TabIndex = 6;
            this.txtMSDSFilesPath.Text = "C:\\HILT_HUB\\MSDS_Files";
            this.toolTip1.SetToolTip(this.txtMSDSFilesPath, "The folder in the SHIMS application where the MSDS files will be stored");
            // 
            // btnBrowseMSDSFiles
            // 
            this.btnBrowseMSDSFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseMSDSFiles.Location = new System.Drawing.Point(408, 33);
            this.btnBrowseMSDSFiles.Name = "btnBrowseMSDSFiles";
            this.btnBrowseMSDSFiles.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseMSDSFiles.TabIndex = 7;
            this.btnBrowseMSDSFiles.Text = "Browse...";
            this.btnBrowseMSDSFiles.UseVisualStyleBackColor = true;
            this.btnBrowseMSDSFiles.Click += new System.EventHandler(this.btnBrowseMSDSFiles_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkLocalMachine);
            this.groupBox2.Controls.Add(this.btnTestConnection);
            this.groupBox2.Controls.Add(this.txtDatabase);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.chkPassword);
            this.groupBox2.Controls.Add(this.txtPasswordBox);
            this.groupBox2.Controls.Add(this.txtUsernameBox);
            this.groupBox2.Controls.Add(this.txtPortBox);
            this.groupBox2.Controls.Add(this.txtServerBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 174);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SHIMS Database Settings";
            // 
            // chkLocalMachine
            // 
            this.chkLocalMachine.AutoSize = true;
            this.chkLocalMachine.Location = new System.Drawing.Point(221, 50);
            this.chkLocalMachine.Name = "chkLocalMachine";
            this.chkLocalMachine.Size = new System.Drawing.Size(77, 17);
            this.chkLocalMachine.TabIndex = 4;
            this.chkLocalMachine.Text = "Local Host";
            this.chkLocalMachine.UseVisualStyleBackColor = true;
            this.chkLocalMachine.CheckedChanged += new System.EventHandler(this.chkLocalMachine_CheckedChanged);
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestConnection.Location = new System.Drawing.Point(383, 25);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(100, 23);
            this.btnTestConnection.TabIndex = 13;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(110, 74);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(251, 20);
            this.txtDatabase.TabIndex = 6;
            this.txtDatabase.Text = "hilt_hub";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Database:";
            // 
            // chkPassword
            // 
            this.chkPassword.AutoSize = true;
            this.chkPassword.Location = new System.Drawing.Point(110, 146);
            this.chkPassword.Name = "chkPassword";
            this.chkPassword.Size = new System.Drawing.Size(102, 17);
            this.chkPassword.TabIndex = 11;
            this.chkPassword.Text = "Show Password";
            this.chkPassword.UseVisualStyleBackColor = true;
            this.chkPassword.CheckedChanged += new System.EventHandler(this.chkPassword_CheckedChanged);
            // 
            // txtPasswordBox
            // 
            this.txtPasswordBox.Location = new System.Drawing.Point(110, 123);
            this.txtPasswordBox.Name = "txtPasswordBox";
            this.txtPasswordBox.Size = new System.Drawing.Size(251, 20);
            this.txtPasswordBox.TabIndex = 10;
            this.txtPasswordBox.Text = "ashore";
            this.txtPasswordBox.UseSystemPasswordChar = true;
            // 
            // txtUsernameBox
            // 
            this.txtUsernameBox.Location = new System.Drawing.Point(110, 100);
            this.txtUsernameBox.Name = "txtUsernameBox";
            this.txtUsernameBox.Size = new System.Drawing.Size(251, 20);
            this.txtUsernameBox.TabIndex = 8;
            this.txtUsernameBox.Text = "ashore";
            // 
            // txtPortBox
            // 
            this.txtPortBox.Location = new System.Drawing.Point(110, 48);
            this.txtPortBox.Name = "txtPortBox";
            this.txtPortBox.Size = new System.Drawing.Size(89, 20);
            this.txtPortBox.TabIndex = 3;
            this.txtPortBox.Text = "1434";
            // 
            // txtServerBox
            // 
            this.txtServerBox.Location = new System.Drawing.Point(110, 25);
            this.txtServerBox.Name = "txtServerBox";
            this.txtServerBox.Size = new System.Drawing.Size(251, 20);
            this.txtServerBox.TabIndex = 1;
            this.txtServerBox.Text = ".\\SQLEXPRESS2008";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Username:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Port:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Server Name:";
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.Location = new System.Drawing.Point(302, 390);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(87, 23);
            this.saveBtn.TabIndex = 3;
            this.saveBtn.Text = "&Save";
            this.saveBtn.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.Location = new System.Drawing.Point(395, 390);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 23);
            this.cancelBtn.TabIndex = 4;
            this.cancelBtn.Text = "&Cancel";
            this.saveBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtAULFilePath
            // 
            this.txtAULFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAULFilePath.Location = new System.Drawing.Point(110, 22);
            this.txtAULFilePath.Name = "txtAULFilePath";
            this.txtAULFilePath.Size = new System.Drawing.Size(292, 20);
            this.txtAULFilePath.TabIndex = 1;
            this.txtAULFilePath.Text = "C:\\Import\\AUL";
            this.toolTip1.SetToolTip(this.txtAULFilePath, "The folder where the AUL (SMCL, SHML) files to import are located");
            // 
            // aulGroupBox
            // 
            this.aulGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aulGroupBox.Controls.Add(this.label12);
            this.aulGroupBox.Controls.Add(this.txtAULFilePath);
            this.aulGroupBox.Controls.Add(this.btnBrowseAULFiles);
            this.aulGroupBox.Enabled = false;
            this.aulGroupBox.Location = new System.Drawing.Point(12, 147);
            this.aulGroupBox.Name = "aulGroupBox";
            this.aulGroupBox.Size = new System.Drawing.Size(494, 67);
            this.aulGroupBox.TabIndex = 1;
            this.aulGroupBox.TabStop = false;
            this.aulGroupBox.Text = "AUL Settings";
            this.aulGroupBox.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Default AUL Folder:";
            // 
            // btnBrowseAULFiles
            // 
            this.btnBrowseAULFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseAULFiles.Location = new System.Drawing.Point(408, 20);
            this.btnBrowseAULFiles.Name = "btnBrowseAULFiles";
            this.btnBrowseAULFiles.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseAULFiles.TabIndex = 2;
            this.btnBrowseAULFiles.Text = "Browse...";
            this.btnBrowseAULFiles.UseVisualStyleBackColor = true;
            this.btnBrowseAULFiles.Click += new System.EventHandler(this.btnBrowseAULFiles_Click);
            // 
            // restoreDefault
            // 
            this.restoreDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.restoreDefault.Location = new System.Drawing.Point(22, 390);
            this.restoreDefault.Name = "restoreDefault";
            this.restoreDefault.Size = new System.Drawing.Size(100, 23);
            this.restoreDefault.TabIndex = 5;
            this.restoreDefault.Text = "Restore Defaults";
            this.saveBtn.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.restoreDefault.UseVisualStyleBackColor = true;
            this.restoreDefault.Click += new System.EventHandler(this.restoreDefault_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.smclOptions);
            this.groupBox1.Location = new System.Drawing.Point(12, 275);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SMCL Settings";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // smclOptions
            // 
            this.smclOptions.BackColor = System.Drawing.SystemColors.MenuBar;
            this.smclOptions.CheckOnClick = true;
            this.smclOptions.FormattingEnabled = true;
            this.smclOptions.Location = new System.Drawing.Point(10, 20);
            this.smclOptions.Name = "smclOptions";
            this.smclOptions.Size = new System.Drawing.Size(473, 64);
            this.smclOptions.TabIndex = 0;
            this.smclOptions.ThreeDCheckBoxes = true;
            this.smclOptions.SelectedIndexChanged += new System.EventHandler(this.smclOptions_SelectedIndexChanged);
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 422);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.restoreDefault);
            this.Controls.Add(this.aulGroupBox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.msdsGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "SHIMS MSDS Import Setup";
            this.Load += new System.EventHandler(this.SetupForm_Load);
            this.msdsGroupBox.ResumeLayout(false);
            this.msdsGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.aulGroupBox.ResumeLayout(false);
            this.aulGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOnlineCDPath;
        private System.Windows.Forms.Button btnBrowseCDOnline;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox msdsGroupBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPasswordBox;
        private System.Windows.Forms.TextBox txtUsernameBox;
        private System.Windows.Forms.TextBox txtPortBox;
        private System.Windows.Forms.TextBox txtServerBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkPassword;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMSDSFilesPath;
        private System.Windows.Forms.Button btnBrowseMSDSFiles;
        private System.Windows.Forms.GroupBox aulGroupBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAULFilePath;
        private System.Windows.Forms.Button btnBrowseAULFiles;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.CheckBox chkLocalMachine;
        private System.Windows.Forms.Button restoreDefault;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox smclOptions;
    }
}

