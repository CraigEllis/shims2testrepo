﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HILT_IMPORTER {
    class DBISQLHelper {
        private string m_dbPath = null;
        private string m_dbName = "";

    #region Constructors
        public DBISQLHelper() {
        }

        public DBISQLHelper(string dbPath, string dbName) {
            m_dbPath = dbPath;
            m_dbName = dbName;
        }
    #endregion
        private void executeNonQuery(string sql){
	        System.Diagnostics.ProcessStartInfo psi =
			        new System.Diagnostics.ProcessStartInfo(@"C:\listfiles.bat");

	        psi.RedirectStandardOutput = true;
	        psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
	        psi.UseShellExecute = false;

	        System.Diagnostics.Process listFiles;
	        listFiles = System.Diagnostics.Process.Start(psi);

	        System.IO.StreamReader myOutput = listFiles.StandardOutput;
	        listFiles.WaitForExit(2000);

	        if (listFiles.HasExited)
	        {
		        string output = myOutput.ReadToEnd();
		        this.processResults.Text = output;
	        }
        }

        private object executeScalar(string sqlCommand) {
            string hmirsDBPath = m_dbPath + "\\DBISQLC.exe";

            if (File.Exists(hmirsDBPath)) {
                //Create a command to be used.
                //****************************
                //Name of the file that will store exported Serial Numbers.
                string exportFileLoc = "exportResults.txt";
                //SQL command to be executed.
//                string sqlCommand = "SELECT COUNT(*) FROM msdsonline_dbo.document_index";
                //The name of the file that will store our executed SQL command.
                //string exportSQLPath = tempDirPath + "\\hmirExporter.sql";

                //Create the SQL file and store the SQL command in it.
                //FileInfo t = new FileInfo(exportSQLPath);
                //StreamWriter sw = t.CreateText();
                //this.sqlGenerator.generate(sqlCommand, exportFileLoc, sw);
                //sw.Close();

                //Create the command line executeble statement to be run.
                string dbCmd = "DBISQLC.exe" + " -q -c \"uid=dba;pwd=sql;\" \"" + exportSQLPath + "\"";

                try {
                    /* Create the ProcessStartInfo using "cmd" as the program to be run,
                     * and "/c " as the parameters.
                     * Incidentally, /c tells cmd that we want it to execute the command that follows,
                     * and then exit.  In this case, execute cdonline.exe and pass it our arguments
                     * specified above.
                     */
                    System.Diagnostics.ProcessStartInfo hmirsDBInfo =
                        new System.Diagnostics.ProcessStartInfo("cmd", "/c " + dbCmd);

                    // Do not create the black window.
                    hmirsDBInfo.CreateNoWindow = true;

                    //Change the working directory to cdonline.exe's current directory.
                    hmirsDBInfo.WorkingDirectory = this.cdOnlinePath;

                    // Now create a process and assign its ProcessStartInfo.  Start it
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = hmirsDBInfo;
                    proc.Start();

                    //Give process time to execute
                    Thread.Sleep(5000);

                    //Return the process for later use.
                    return proc;
                }
                catch (Exception) {
                    //Not currently logging these errors.  We want the system to fail if the
                    //default installation fails.
                    throw new ImportException("We were unable to probably invoke the file: " + hmirsDBPath + "." +
                                               "  Please try again and contact support if this problem persists.");
                }

            }
            else {
                //Not currently logging these errors.  We want the system to fail if the
                //default installation fails.
                throw new ImportException("We were unable to find the file:  " + hmirsDBPath + ".  Please check your Online CD path and try again.  " +
                                            "This path should point to where the cdonline.exe file lives.");
            }
            System.Diagnostics.ProcessStartInfo psi =
                    new System.Diagnostics.ProcessStartInfo(@"C:\listfiles.bat");

            psi.RedirectStandardOutput = true;
            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;

            System.Diagnostics.Process listFiles;
            listFiles = System.Diagnostics.Process.Start(psi);

            System.IO.StreamReader myOutput = listFiles.StandardOutput;
            listFiles.WaitForExit(2000);

            if (listFiles.HasExited) {
                string output = myOutput.ReadToEnd();
            }
        }

        private object executeQuery(string sql) {
            System.Diagnostics.ProcessStartInfo psi =
                    new System.Diagnostics.ProcessStartInfo(@"C:\listfiles.bat");

            psi.RedirectStandardOutput = true;
            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;

            System.Diagnostics.Process listFiles;
            listFiles = System.Diagnostics.Process.Start(psi);

            System.IO.StreamReader myOutput = listFiles.StandardOutput;
            listFiles.WaitForExit(2000);

            if (listFiles.HasExited) {
                string output = myOutput.ReadToEnd();
                this.processResults.Text = output;
            }
        }
    }
}
