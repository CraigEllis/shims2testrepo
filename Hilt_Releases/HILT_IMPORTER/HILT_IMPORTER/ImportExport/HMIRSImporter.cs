﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;
using HILT_IMPORTER.DataObjects;
using System.Security.Principal;
using System.Collections;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;
using Ionic.Zip;
using HILT_IMPORTER.ImportExport;



namespace HILT_IMPORTER {
    class HMIRSImporter {
        // Support Table Indexes 
        public const int CONTRACTOR = 0;
        public const int AFJM_PSN = 1;
        public const int DOT_PSN = 2;
        public const int IATA_PSN = 3;
        public const int IMO_PSN = 4;

        private MSDSImportForm m_form = null;
        private BackgroundWorker m_worker = null;
        private int recordTotal = 0;
        private int recordCount = 0;
        private string smclItems = " ";
        private string updateItems = " ";
        long tickStart;

        // Class level Database Managers
        DatabaseManager m_manager = null;
        HMIRSDatabaseManager m_hmirsMgr = null;

        private bool m_firstTimeExport = true;
        private FileInfo m_exportErrorFile = null;

        public HMIRSImporter(MSDSImportForm form, BackgroundWorker worker) {
            m_form = form;
            m_worker = worker;
        }

        private const int m_batchSize = 30;
        public int BatchSize
        {
            get { return m_batchSize; }
        }

        private string m_tempPath;
        public String TempPath
        {
            get { return m_tempPath; }
            set { m_tempPath = value; }
        }
        private string m_tempMsdsPath;

        //TODO - separate database processing from document processing
        // 
        public bool import(DateTime startDate, string smclStmt, string updateStmt) {
            bool result = true;
            tickStart = Environment.TickCount;
            smclItems = smclStmt;
            updateItems = updateStmt;

            //if (m_worker != null)
            if (m_worker.CancellationPending == false)
            {
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                    "---------------------------------------------------------------\r\n");
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "----- Processing HMIRS Import Started "
                    + String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now)
                    + " -----\n");
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                    "---------------------------------------------------------------\r\n\r\n");
            }
            // Create the temp folder
            //if (m_worker != null)
            if (m_worker.CancellationPending == false)
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                "SB_Creating directory...");
            m_tempPath = Configuration.HILT_HUB_DataFolder + "\\temp\\Import" 
                + String.Format("{0:_yyyy_MM_dd_HHmm}",  DateTime.Now);
            if (!Directory.Exists(m_tempPath)) {
                Directory.CreateDirectory(m_tempPath);
                //if (m_worker != null)
                if (m_worker.CancellationPending == false)
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                        "\tProcessing to Temporary directory: " + m_tempPath + "\n\n");
            }

            // Update the support tables
            //if (m_worker != null) {
            /*if(m_worker.CancellationPending == false){
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "SB_Updating SHIMS support tables with latest MSDS data.../r/n");
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "\n----- Updating SHIMS MSDS Support tables -----\n");
            }
            result = updateHubSupportTables(startDate);*/

            // Get the MSDS numbers to process
            //if (m_worker != null) {
            if(m_worker.CancellationPending == false){
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "SB_Retrieving MSDS to process...");
                /*m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "\n----- Updating SHIMS MSDS Data tables -----\n");*/
            }
            List<string> msdsSerNoList = getMSDSSerNoList(startDate, smclItems);
            List<string> msdsUpdateSerNoList = new List<string>();
            if (updateStmt != " " && updateStmt != "") { 
            msdsUpdateSerNoList = getMSDSSerNoList(startDate, updateItems);
            }

            if (msdsSerNoList.Count == 0) {
                //if (m_worker != null)
                if (m_worker.CancellationPending == false)
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                        "\r\n\t***** No MSDS records to process *****\t\r\n");
                result = false;
            }
            else {
                //if (m_worker != null)
                if (m_worker.CancellationPending == false)
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                        "\tMSDS records to process: " + (msdsSerNoList.Count + msdsUpdateSerNoList.Count) 
                        + "\n");
                //if(m_form != null)
                if (m_worker.CancellationPending == false)
                    m_form.ProgressBarMaxValue = msdsSerNoList.Count;

                // Do the database import/update
                //if (m_worker != null)
                if (m_worker.CancellationPending == false)
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                        "SB_Updating SHIMS database...");
                result = updateHubDatabase(msdsSerNoList, startDate);
                if (msdsUpdateSerNoList.Count > 0)
                {
                    result = updateHubDatabase(msdsUpdateSerNoList, startDate);
                }

                // Extract the documents from the CDs
                if (extractHMIRSDocuments(startDate)&& m_worker.CancellationPending == false)                
                    // Create the files to the MSDS_Docs folder
                    result = createMSDSDocuments(msdsSerNoList);

                if (result == true || m_worker.CancellationPending == true)
                {
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                        "\r\n\n\tFinishing Up...\r\n");
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                        "SB_Removing Temporary Files...");

                    /*string[] tempFiles = Directory.GetFiles(m_tempPath);
                    foreach (string file in tempFiles)
                    {
                        File.Delete(file);
                    }*/
                    if (Directory.Exists(Configuration.HILT_HUB_DataFolder + "\\temp"))
                    {
                        Directory.Delete(Configuration.HILT_HUB_DataFolder + "\\temp", true);
                    }

                }

                // Dispose of the database managers
                m_manager = null;
                m_hmirsMgr.disposeConnection();
                m_hmirsMgr = null;
            }

            // Clean up the temp directory if the processing was successful
            /*if (result && Directory.Exists(m_tempPath)) {
                Directory.Delete(m_tempPath, true);
            }*/

            long elapsedTicks = Environment.TickCount - tickStart;
            if (m_worker != null)
            {
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "\n-----------------------------------------------------------------------------");
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "\n----- MSDS Import Complete "
                    + String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now)
                    + " (Elapsed time: "
                    + formatTickCountToHHmmss(elapsedTicks) + ") -----\n");
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "-----------------------------------------------------------------------------\n");
            }
            if (m_worker.CancellationPending == false)
            {
                //Properties.Settings.Default.LastHmirsUpdate = DateTime.Now;
                //Properties.Settings.Default.Save();

                Process.Start(Configuration.MSDS_DocumentFolder);
            }
            return result;
        }

        //TODO - replace createMSDSDocuments with a call to get just the documents that have changed 
        // since the last update - current list is too big
        protected internal bool extractHMIRSDocuments(DateTime lastUpdate) {
            return extractHMIRSDocuments(lastUpdate,
                Configuration.CDROM_DataFolder,
                m_tempPath + "\\HMIRS_Docs\\");
        }
        protected internal bool extractHMIRSDocuments(DateTime lastUpdate, 
                string cdromPath, string tempPath) {
            bool result = false;
            string baseZipFile = cdromPath.Replace("Disk1", "DiskXX") 
                + "\\CDROM-DiskXXTarYY.dat";
            int nTotal = 0;

            //if (m_worker != null)
            if (m_worker.CancellationPending == false)
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "\n\t-------------------------------------"+
                    "\n\t----- HMIRS Document Extraction -----"+
                    "\n\t-------------------------------------\n");
            // Make sure the output folder exists
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);
            if (!tempPath.EndsWith("\\"))
                tempPath += "\\";

            long totalTicks = Environment.TickCount;

            HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

            // Get the disk numbers to process
            List<int> diskNumbers = mgr.getCDNumsToExtract(lastUpdate);

            if (diskNumbers.Count > 0 && m_worker.CancellationPending == false) {
                result = true;

                foreach (int diskNumber in diskNumbers) {
                    if (m_worker.CancellationPending == false)
                    {
                        string zipFilename = baseZipFile.Replace("XX", diskNumber.ToString());

                        long ticks = Environment.TickCount;

                        
                        List<HMIRSDocumentRecord> smcldocData = mgr.getSmclDataMapping(diskNumber, smclItems);

                        // Update the progress bar max value
                        if (m_form != null)
                            m_form.ProgressBarMaxValue = smcldocData.Count;
                        if (m_worker.CancellationPending == false)
                            m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                                "SB_Extracting Documents..." + diskNumber.ToString() + "...");

                        int files = 0;
                        List<string> usedTarFiles = mgr.getMappedTarFiles(diskNumber);                       
                        List<string> extractedTarPaths = new List<string>();

                        foreach (string dat in usedTarFiles)
                        {
                           List<HMIRSDocumentRecord> docData = mgr.getDocDataMapping(diskNumber, dat);

                            HMIRSDocProcessor processor = new HMIRSDocProcessor(m_form, m_worker);

                            string tarPath = zipFilename.Replace("YY", dat);
                            files = processor.unpackDocumentFiles(tarPath,
                                tempPath, docData, smcldocData, nTotal);                      

                            nTotal = files;
                        }

                        totalTicks = Environment.TickCount - tickStart;
                        string ticksToString = " ";
                        if (totalTicks >= 60000) { ticksToString = (((double)totalTicks) / 60000).ToString("#.0") + " min\n"; }
                        else { ticksToString = (((double)totalTicks) / 1000).ToString("#.0") + " sec\n"; }

                        if (m_worker.CancellationPending == false)
                            m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                                "\n\tCD " + diskNumber.ToString() + " - Documents Extracted: "
                                + BaseImportForm.formatNumberThousands(files)
                                + " in " + ticksToString);

                        System.Diagnostics.Debug.WriteLine("\tDisk " + diskNumber.ToString() + ": "
                        + nTotal.ToString() + "\tTiming: "
                        + totalTicks);
                    }
                }



                totalTicks= Environment.TickCount - tickStart;
                string tickString = " ";
                if (totalTicks >= 60000) { tickString = (((double)totalTicks) / 60000).ToString("#.0") + " min\n"; }
                else { tickString = (((double)totalTicks) / 1000).ToString("#.0") + " sec\n"; }

                if (m_worker.CancellationPending == false)
                    m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                        "\n\tHMIRS Document Extraction - " + BaseImportForm.formatNumberThousands(nTotal) 
                        + " Documents extracted "
                        + "in " + tickString);
            }
            else {
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "\tNo Documents to Extract\n");
            }

            return result;
        }

        //TODO - replace createMSDSDocuments with a call to get just the documents that have changed 
        // since the last update - current list is too big
        protected internal bool createMSDSDocuments(List<string> msdsSerNo) {
            return createMSDSDocuments(msdsSerNo,
                m_tempPath + "\\HMIRS_Docs\\",
                Configuration.MSDS_DocumentFolder + "\\");
        }

        protected internal bool createMSDSDocuments(List<string> msdsSerNo,
            string dataDocPath, string msdsDocPath) {
            bool result = true;
            int nCount = 0;
            int nFiles = 0;
            int nMissingFiles = 0;
            int nBadFiles = 0;

            List<string> filesToZip = new List<string>();
            string[] existingMsds = Directory.GetFiles(Configuration.MSDS_DocumentFolder);
            foreach (string file in existingMsds) {
                if (file.Contains(".zip")){ File.Delete(Configuration.MSDS_DocumentFolder + "\\" + file); }
                else { filesToZip.Add(file); }  
            }

            if (m_worker.CancellationPending == false)
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                    "SB_Creating MSDS Documents...");
            //if (m_form != null)
            if (m_worker.CancellationPending == false)
                m_form.ProgressBarMaxValue = msdsSerNo.Count;

            long ticks = Environment.TickCount;

            if (m_hmirsMgr == null)
                m_hmirsMgr = new HMIRSDatabaseManager();

            System.Diagnostics.Debug.WriteLine("**** Create MSDS Documents ****");

            if (m_worker.CancellationPending == false)
            {
                // Loop through the serial #s
                foreach (string serialNum in msdsSerNo)
                {
                    // Get the data_ids for this serial #
                    List<HMIRSDocumentRecord> data_ids = m_hmirsMgr.getDocIDsForSerialNum(serialNum);

                    if (nCount % 10 == 0 && m_worker != null)
                        m_worker.ReportProgress(nCount);

                    // Dictionary to store the unique serial_num - doc_type combos
                    Dictionary<string, int> uniqueIDs = new Dictionary<string, int>();

                    // Process the documents
                    foreach (HMIRSDocumentRecord docData in data_ids)
                    {
                        if (m_worker.CancellationPending == false)
                        {
                            try
                            {
                                string revDate = docData.revision_date.ToString("yyyyMMdd");
                                string docDataFilename = dataDocPath + docData.doc_file_name;
                                string docDataPrefix = docData.doc_data_id.ToString() + "_" + docData.document_type+ "_" + revDate;
                                string newFilePrefix = serialNum + "_" + docData.document_type + "_" + revDate;

                                // Make sure the document was extracted
                                if (File.Exists(docDataFilename))
                                {
                                    // Get the serial # name
                                    int id = 0;
                                    if (uniqueIDs.ContainsKey(newFilePrefix))
                                    {
                                        // Have a dup - increment the uniqueId value & append it to the file prefix
                                        uniqueIDs[newFilePrefix]++;
                                        newFilePrefix += "_" + uniqueIDs[newFilePrefix].ToString();
                                    }
                                    else
                                        uniqueIDs.Add(newFilePrefix, 0);

                                    string msdsFilename = msdsDocPath
                                        + docData.doc_file_name.Replace(docDataPrefix, newFilePrefix);

                                    nFiles++;

                                    File.Copy(docDataFilename, msdsFilename, true);

                                    System.Diagnostics.Debug.WriteLine(serialNum + " - " + msdsFilename);

                                    // Now add the file to the new MSDS Docs zip file list
                                    filesToZip.Add(msdsFilename);
                                }
                                else
                                {
                                    nMissingFiles++;
                                }

                            }
                            catch (Exception ex)
                            {
                                nBadFiles++;
                                result = false;

                                // Write the bad file info to the message box
                                //if (m_worker != null)
                                if (m_worker.CancellationPending == false)
                                    m_worker.ReportProgress(nCount, "\tError creating file for MSDS Serial # "
                                        + serialNum + ": " + ex.Message + "\n");
                            }
                        }
                    }

                    nCount++;
                }
            }
            System.Diagnostics.Debug.WriteLine("**** DONE " + nFiles.ToString() + " ****");

            // Write out the results to the message box
            ticks = Environment.TickCount - ticks;
            //if (m_worker != null)
            if (m_worker.CancellationPending == false)
                m_worker.ReportProgress(recordCount, "\n\tMSDS Files created: " + BaseImportForm.formatNumberThousands(nFiles)
                    + "\n\tMissing Files: " + BaseImportForm.formatNumberThousands(nMissingFiles)
                    + " \n\tProcessing Time: " + (((double)ticks) / 1000).ToString("#.0") + " sec\n");

            // See if the user wants to create a zip file of new/updated documents
            DialogResult response = MessageBox.Show("Do you want to create a zip file containing "
                + "the new/updated MSDS documents?", "Create Zip File", MessageBoxButtons.YesNo);

            if (response == DialogResult.Yes)
                // Create the new docs zip file
                createNewDocZipFile(msdsDocPath, filesToZip);

            return result;
        }

        protected internal bool createNewDocZipFile(string docPath, List<string> filesToZip) {
            bool result = true;
            int nCount = 0;
            Dictionary<string, int> fileNames = new Dictionary<string, int>();

            if (m_worker != null)
                m_worker.ReportProgress(recordCount, "SB_Creating Zip file... ");
            if (m_form != null)
                m_form.ProgressBarMaxValue = filesToZip.Count;

            try {
                // Build the zip file name
                string zipFilename = docPath + "newDocs_" + String.Format("{0:yyyy_MM_dd}", DateTime.Now) + ".zip";

                System.Diagnostics.Debug.WriteLine("**** Zip MSDS Documents " + zipFilename + " ****");

                // Zip the files
                using (ZipFile zipFile = new ZipFile()) {
                    foreach (string fileName in filesToZip) {
                        System.Diagnostics.Debug.Write("\t" + fileName);

                        if (nCount % 10 == 0 && m_worker != null)
                            m_worker.ReportProgress(nCount);

                        try {
                            if (!fileNames.ContainsKey(fileName)) {
                                zipFile.AddFile(fileName, "");
                                fileNames.Add(fileName, 0);

                                System.Diagnostics.Debug.WriteLine(" - Added");
                            }
                            else {
                                System.Diagnostics.Debug.WriteLine(" - Duplicate");
                            }
                        }
                        catch (Exception e) {
                            System.Diagnostics.Debug.WriteLine("ERROR adding file to zip: " + e.Message);
                        }

                        nCount++;
                    }

                    if (m_worker != null)
                    {
                        m_worker.ReportProgress(recordCount, "SB_Writing Zip file");
                        m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK,
                                "\n\n \tWriting Zip file (this could take a few minutes)...");
                    }
                    zipFile.Save(zipFilename);
                }

                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, "\n\tNew Documents archived to Zip file: " 
                         + zipFilename);

            }
            catch (Exception ex) {
                result = false;
            }

            return result;
        }

        private List<string> getMSDSSerNoList(DateTime startDate, string smclStmt) {
            List<string> list = new List<string>();

            if (m_hmirsMgr == null )
                m_hmirsMgr = new HMIRSDatabaseManager();

            list = m_hmirsMgr.getNewMSDSList(startDate, smclStmt);

            return list;
        }

        private int splitMSDSFiles(List<string> msdsList) {
            int fileNum = 0;
            int itemCount = m_batchSize; // Start at batch size so the writer gets created
            StreamWriter writer = null;

            try {
                foreach (string msdsNum in msdsList) {
                    // See if it's time to write the old file
                    if (itemCount == m_batchSize) {
                        //Close the writer for the current file
                        if (writer != null) {
                            writer.Flush();
                            writer.Close();
                        }

                        //Create a new file and reset the count to 0
                        writer = new StreamWriter(m_tempPath + "\\msdsSerNo" + ++fileNum + ".dat");
                        itemCount = 0;
                    }

                    // Write the item to the current file
                    writer.WriteLine(msdsNum);
                    itemCount++;
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - splitMSDSFiles - " + ex.Message);
            }
            finally {
                // See if we have an open writer to close
                if (writer != null) {
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
            }

            if (m_worker != null)
                m_worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, 
                    "\t" + fileNum.ToString() + " batch files created.\n");

            return fileNum;
        }

        private bool updateHubSupportTables(DateTime lastUpdate) {
            bool result = true;

            if (m_hmirsMgr == null)
                m_hmirsMgr = new HMIRSDatabaseManager();

            try {
                // Get the support table changes
                int[] tableCounts = m_hmirsMgr.getSupportDataRevisionCounts(lastUpdate);

                recordCount = 0;
                for (int i = 0; i < tableCounts.Length; i++) {
                    recordTotal += tableCounts[i];
                }
                if (m_form != null)
                    m_form.ProgressBarMaxValue = recordTotal;
 
                // Process the tables that have been updated since the last upload
                // Contractors
                if (tableCounts[CONTRACTOR] > 0) {
                    if (m_worker != null)
                        m_worker.ReportProgress(recordCount, "SB_Processing Contractor Info...");
                    if (!updateContractorInfo(lastUpdate))
                        result = false;
                }
                // AFJM_PSN
                if (tableCounts[AFJM_PSN] > 0) {
                    if (m_worker != null)
                        m_worker.ReportProgress(recordCount, "SB_Processing AFJM PSN Info...");
                    if (!updateAFJMInfo(lastUpdate))
                        result = false;
                }
                // DOT_PSN
                if (tableCounts[DOT_PSN] > 0) {
                    if (m_worker != null)
                        m_worker.ReportProgress(recordCount, "SB_Processing DOT PSN Info...");
                    if (!updateDOTInfo(lastUpdate))
                        result = false;
                }
                // IATA_PSN
                if (tableCounts[IATA_PSN] > 0)
                    if (m_worker != null)
                        m_worker.ReportProgress(recordCount, "SB_Processing IATA PSN Info...");
                if (!updateIATAInfo(lastUpdate))
                        result = false;
                // IMO_PSN
                if (tableCounts[IMO_PSN] > 0) {
                    if (m_worker != null)
                        m_worker.ReportProgress(recordCount, "SB_Processing IMO PSN Info...");
                    if (!updateIMOInfo(lastUpdate))
                        result = false;
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - " + ex.Message);
            }

            return result;
        }

        private bool updateContractorInfo(DateTime lastUpdate) {
            bool result = true;
            int newCount = 0;
            int updateCount = 0;
            DateTime tmsp_last_updt = lastUpdate;

            long ticks = Environment.TickCount;

            try {
                if (m_manager == null)
                    m_manager = new DatabaseManager();
                if (m_hmirsMgr == null)
                    m_hmirsMgr = new HMIRSDatabaseManager();

                // Get the new records 
                List<MSDSContractorInfo> list = m_hmirsMgr.getUpdatedContractorInfo(lastUpdate);

                foreach (MSDSContractorInfo record in list) {
                    recordCount++;

                    // See if this is a new MSDS number
                    if (m_manager.isNewContractorInfo(record.CT_CAGE)) {
                        // Insert a new AFJM record
                        m_manager.insertMSDSContractorRecord(record);

                        newCount++;
                    }
                    else {
                        // Update the existing record
                        m_manager.updateMSDSContractorRecord(record);

                        updateCount++;
                    }

                    // Find the max last_updt
                    if (record.TMSP_LAST_UPDT > tmsp_last_updt)
                        tmsp_last_updt = record.TMSP_LAST_UPDT;

                    if (recordCount % 10 == 0)
                        if (m_worker != null)
                            m_worker.ReportProgress(recordCount);
                }

                // Update the msds_updates table
                updateTableTimestamp("msds_contractor_info", tmsp_last_updt);

                // Write the results
                ticks = Environment.TickCount - ticks;
                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, "\tContractor_Info updated: New records: "
                        + BaseImportForm.formatNumberThousands(newCount)
                        + " Updated records: " + BaseImportForm.formatNumberThousands(updateCount)
                        + " - Processing Time: " + (((double)ticks) / 1000).ToString("#.0") + " sec\n");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - " + ex.Message);
            }

            return result;
        }

        private bool updateAFJMInfo(DateTime lastUpdate) {
            bool result = true;
            int newCount = 0;
            int updateCount = 0;
            DateTime tmsp_last_updt = lastUpdate;

            long ticks = Environment.TickCount;

            try {
                if (m_manager == null)
                    m_manager = new DatabaseManager();
                if (m_hmirsMgr == null)
                    m_hmirsMgr = new HMIRSDatabaseManager();

                // Get the new records 
                List<MSDS_AFJM_PSN> list = m_hmirsMgr.getUpdatedAFJMRecords(lastUpdate);

                foreach (MSDS_AFJM_PSN record in list) {
                    recordCount++;

                    // See if this is a new MSDS number
                    if (m_manager.isNewAFJM_PSN(record.AFJM_PSN_CODE)) {
                        // Insert a new AFJM record
                        m_manager.insertMSDSAFJM_PSNRecord(record);

                        newCount++;
                    }
                    else {
                        // Update the existing record
                        m_manager.updateMSDSAFJM_PSNRecord(record);

                        updateCount++;
                    }

                    // Find the max last_updt
                    if (record.TMSP_LAST_UPDT > tmsp_last_updt)
                        tmsp_last_updt = record.TMSP_LAST_UPDT;

                    if (recordCount % 10 == 0)
                        m_worker.ReportProgress(recordCount);
                }

                // Update the msds_updates table
                updateTableTimestamp("msds_afjm_psn", tmsp_last_updt);

                // Write the results
                ticks = Environment.TickCount - ticks;
                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, "\tAFJM_PSN updated: New records: "
                        + BaseImportForm.formatNumberThousands(newCount)
                        + " Updated records: " + BaseImportForm.formatNumberThousands(updateCount)
                        + " - Processing Time: " + (((double)ticks) / 1000).ToString("#.0") + " sec\n");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - " + ex.Message);
            }

            return result;
        }

        private bool updateDOTInfo(DateTime lastUpdate) {
            bool result = true;
            int newCount = 0;
            int updateCount = 0;
            DateTime tmsp_last_updt = lastUpdate;

            long ticks = Environment.TickCount;

            try {
                if (m_manager == null)
                    m_manager = new DatabaseManager();
                if (m_hmirsMgr == null)
                    m_hmirsMgr = new HMIRSDatabaseManager();

                // Get the new records 
                List<MSDS_DOT_PSN> list = m_hmirsMgr.getUpdatedDOTRecords(lastUpdate);

                foreach (MSDS_DOT_PSN record in list) {
                    recordCount++;

                    // See if this is a new MSDS number
                    if (m_manager.isNewDOT_PSN(record.DOT_PSN_CODE)) {
                        // Insert a new DOT record
                        m_manager.insertMSDSDOT_PSNRecord(record);

                        newCount++;
                    }
                    else {
                        // Update the existing record
                        m_manager.updateMSDSDOT_PSNRecord(record);

                        updateCount++;
                    }

                    // Find the max last_updt
                    if (record.TMSP_LAST_UPDT > tmsp_last_updt)
                        tmsp_last_updt = record.TMSP_LAST_UPDT;

                    if (recordCount % 10 == 0)
                        if (m_worker != null)
                            m_worker.ReportProgress(recordCount);
                }

                // Update the msds_updates table
                updateTableTimestamp("msds_dot_psn", tmsp_last_updt);

                // Write the results
                ticks = Environment.TickCount - ticks;
                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, "\tDOT_PSN updated: New records: "
                        + BaseImportForm.formatNumberThousands(newCount) 
                        + " Updated records: " + BaseImportForm.formatNumberThousands(updateCount)
                        + " - Processing Time: " + (((double)ticks) / 1000).ToString("#.0") + " sec\n");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - " + ex.Message);
            }

            return result;
        }

        private bool updateIATAInfo(DateTime lastUpdate) {
            bool result = true;
            int newCount = 0;
            int updateCount = 0;
            DateTime tmsp_last_updt = lastUpdate;

            long ticks = Environment.TickCount;

            try {
                if (m_manager == null)
                    m_manager = new DatabaseManager();
                if (m_hmirsMgr == null)
                    m_hmirsMgr = new HMIRSDatabaseManager();

                // Get the new records 
                List<MSDS_IATA_PSN> list = m_hmirsMgr.getUpdatedIATARecords(lastUpdate);

                foreach (MSDS_IATA_PSN record in list) {
                    recordCount++;

                    // See if this is a new MSDS number
                    if (m_manager.isNewIATA_PSN(record.IATA_PSN_CODE)) {
                        // Insert a new IATA record
                        m_manager.insertMSDSIATA_PSNRecord(record);

                        newCount++;
                    }
                    else {
                        // Update the existing record
                        m_manager.updateMSDSIATA_PSNRecord(record);

                        updateCount++;
                   }

                    // Find the max last_updt
                    if (record.TMSP_LAST_UPDT > tmsp_last_updt)
                        tmsp_last_updt = record.TMSP_LAST_UPDT;

                    if (recordCount % 10 == 0)
                        if (m_worker != null)
                            m_worker.ReportProgress(recordCount);
                }

                // Update the msds_updates table
                updateTableTimestamp("msds_iata_psn", tmsp_last_updt);

                // Write the results
                ticks = Environment.TickCount - ticks;
                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, "\tIATA_PSN updated: New records: "
                        + BaseImportForm.formatNumberThousands(newCount) 
                        + " Updated records: " + BaseImportForm.formatNumberThousands(updateCount)
                        + " - Processing Time: " + (((double)ticks) / 1000).ToString("#.0") + " sec\n");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - " + ex.Message);
            }

            return result;
        }

        private bool updateIMOInfo(DateTime lastUpdate) {
            bool result = true;
            int newCount = 0;
            int updateCount = 0;
            DateTime tmsp_last_updt = lastUpdate;

            long ticks = Environment.TickCount;

            try {
                if (m_manager == null)
                    m_manager = new DatabaseManager();
                if (m_hmirsMgr == null)
                    m_hmirsMgr = new HMIRSDatabaseManager();

                // Get the new records 
                List<MSDS_IMO_PSN> list = m_hmirsMgr.getUpdatedIMORecords(lastUpdate);

                foreach (MSDS_IMO_PSN record in list) {
                    recordCount++;

                    // See if this is a new MSDS number
                    if (m_manager.isNewIMO_PSN(record.IMO_PSN_CODE)) {
                        // Insert a new IMO record
                        m_manager.insertMSDSIMO_PSNRecord(record);

                        newCount++;
                    }
                    else {
                        // Update the existing record
                        m_manager.updateMSDSIMO_PSNRecord(record);

                        updateCount++;
                    }

                    // Find the max last_updt
                    if (record.TMSP_LAST_UPDT > tmsp_last_updt)
                        tmsp_last_updt = record.TMSP_LAST_UPDT;

                    if (recordCount % 10 == 0)
                        if (m_worker != null)
                            m_worker.ReportProgress(recordCount);
                }

                // Update the msds_updates table
                updateTableTimestamp("msds_imo_psn", tmsp_last_updt);

                // Write the results
                ticks = Environment.TickCount - ticks;
                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, "\tIMO_PSN updated: New records: "
                        + BaseImportForm.formatNumberThousands(newCount) + " Updated records: " 
                        + BaseImportForm.formatNumberThousands(updateCount)
                        + " - Processing Time: " + (((double)ticks) / 1000).ToString("#.0") + " sec\n");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - " + ex.Message);
            }

            return result;
        }

        private bool updateTableTimestamp(string table_name, DateTime tmsp_last_updt) {
            MSDSUpdateInfo info = new MSDSUpdateInfo();
            info.table_name = table_name;
            info.last_update = tmsp_last_updt;

            DatabaseManager mgr = new DatabaseManager();

            bool result = mgr.saveMSDSUpdateInfo(info);

            return result;
        }

        //TODO - look at changing the DatabaseMAnager to use a class-level connection
        // like the HMIRS DB Manager uses - may speed things up 
        private bool updateHubDatabase(List<string> msdsList, DateTime lastUpdate) {
            bool result = true;
            int newCount = 0;
            int updateCount = 0;
            int recordCount = 1;
            StringBuilder msgBuffer = new StringBuilder();
            int bufferCount = 0;
            DateTime lastRevDate = DateTime.MinValue;

            long ticks = Environment.TickCount;

            if (m_form != null)
                m_form.ProgressBarMaxValue = msdsList.Count;

            if (m_manager == null)
                m_manager = new DatabaseManager();
            if (m_hmirsMgr == null)
                m_hmirsMgr = new HMIRSDatabaseManager();

            using (StreamWriter sw = File.CreateText(Configuration.MSDS_DocumentFolder+"\\MSDSINSERT.txt"))
            {
                sw.WriteLine("DECLARE @currentMSDS int;\nBEGIN\n");
                foreach (string msdsNum in msdsList)
                {
                    // Check for cancel
                    if (m_worker != null && !m_worker.CancellationPending)
                    {
                        // Get the MSDS Record from the HMIRS database
                        MSDSRecord msdsMaster = m_hmirsMgr.getMSDSMasterRecord(msdsNum, lastUpdate);

                        if (msdsMaster.MSDSSERNO == null || msdsMaster.MSDSSERNO.Length == 0)
                        {
                            // Write the existing buffer
                            if (bufferCount > 0)
                            {
                                if (m_worker != null)
                                    m_worker.ReportProgress(recordCount, msgBuffer.ToString() + "\n");
                                msgBuffer.Clear();
                                bufferCount = 0;
                            }
                            // Write the error msg
                            if (m_worker != null)
                                m_worker.ReportProgress(recordCount, "\t" + msdsNum
                                    + ": Not found in HMIRS Database\n");
                        }
                        else
                        {
                            //m_worker.ReportProgress(1, "\t: Before if else\n");
                            // See if this is a new MSDS number
                            if (m_manager.isNewMSDSSerNo(msdsNum))
                            {
                                // Insert a new MSDS record
                                // m_worker.ReportProgress(1, "\t: Before insert (if)\n");
                                sw.WriteLine(m_manager.insertMSDSMasterRecord("HMIRS", msdsMaster,
                                    Environment.UserName));
                                // m_worker.ReportProgress(1, "\t: After insert\n");
                                newCount++;
                                bufferCount++;
                                msgBuffer.Append("\t" + msdsNum + ": Added");
                            }
                            else
                            {
                                //m_worker.ReportProgress(1, "\t: Before update\n");
                                // Update the existing record
                                sw.WriteLine(m_manager.updateMSDSMasterRecord("HMIRS", msdsMaster,
                                    Environment.UserName, m_worker));
                                //m_worker.ReportProgress(1, "\t: After update\n");
                                updateCount++;
                                bufferCount++;
                                msgBuffer.Append("\t" + msdsNum + ": Updated");
                            }

                            // update the rev date
                            if (msdsMaster.last_update > lastRevDate)
                                lastRevDate = msdsMaster.last_update;

                            // Print out the msgBuffer
                            if (bufferCount % 5 == 0)
                            {
                                if (m_worker != null)
                                    m_worker.ReportProgress(recordCount, msgBuffer.ToString() + "\n");

                                msgBuffer.Clear();
                                bufferCount = 0;
                            }
                        }

                        //System.Diagnostics.Debug.WriteLine(recordCount + " - " + (Environment.TickCount - ticks).ToString());
                        recordCount++;
                    }
                    else
                    {
                        // Write the existing buffer
                        if (bufferCount > 0)
                        {
                            if (m_worker != null)
                                m_worker.ReportProgress(recordCount, msgBuffer.ToString() + "\n");
                            msgBuffer.Clear();
                            bufferCount = 0;
                        }
                        if (m_worker != null)
                        {
                            m_worker.ReportProgress(recordCount,
                                "\n\t************************************\n");
                            m_worker.ReportProgress(recordCount,
                                "\t**** USER CANCELLED PROCESSING *****\n");
                            m_worker.ReportProgress(recordCount,
                                "\t************************************\n\n");
                        }
                        break;
                    }
                }
                sw.WriteLine("END");
            }

            // Write the remaining buffer
            if (bufferCount > 0) {
                if (m_worker != null)
                    m_worker.ReportProgress(recordCount, msgBuffer);
                msgBuffer.Clear();
                bufferCount = 0;
            }

            // Update the HMIRS update value in DB_Properties
            //m_manager.setLastHMIRSUpdate(lastRevDate);
            if (m_worker.CancellationPending == false) { Properties.Settings.Default.LastHmirsUpdate = lastRevDate; Properties.Settings.Default.Save(); }

            // Write the results
            ticks = Environment.TickCount - ticks;
            string ticksToString = " ";
            if (ticks >= 60000) { ticksToString = (((double)ticks) / 60000).ToString("#.0") + " min\n"; }
            else { ticksToString = (((double)ticks) / 1000).ToString("#.0") + " sec\n"; }

            if (m_worker != null)
                m_worker.ReportProgress(recordCount, "\n\tMSDS data update complete:\n\tNew records: "
                    + BaseImportForm.formatNumberThousands(newCount) + "\n\tUpdated records: " + BaseImportForm.formatNumberThousands(updateCount)
                    + "\n\tProcessing Time: " + ticksToString);//(((double)ticks) / 1000).ToString("#.0") + " sec\n");

            return result;
        }

        //private bool extractDocumentFiles(int count) {
        //    bool result = true;

        //    /* For each batch flat file, create an ini file, an export file, 
        //     * run cdonline.exe, and update progress
        //     * Confirm the text file exists and that the contents are what we expect.
        //     * Notify the user of progress.
        //     */
        //    IniGenerator gen = new IniGenerator();
        //    System.Diagnostics.Process prcsMSDSExport;

        //    m_tempMsdsPath = m_tempPath + "\\msds";
        //    if (!Directory.Exists(m_tempMsdsPath))
        //        Directory.CreateDirectory(m_tempMsdsPath);
        //    m_exportErrorFile = new FileInfo(m_tempPath + "\\exportErrorLog.log");

        //    try {
        //        for (int i = 0; i < count; i++) {
        //            //Initialize file paths.
        //            string inputFile = m_tempPath + "\\msdsSerNo" + (i + 1) + ".dat";
        //            string outputFile = m_tempMsdsPath + "\\" + "msds" + (i + 1) + ".txt";

        //            //Initialize the ini file and open it up for writing/updating.
        //            FileInfo iniFile = new FileInfo(m_tempMsdsPath + "\\hilt.ini");
        //            StreamWriter sw = iniFile.CreateText();
        //            gen.generate("DBA", "SQL", inputFile, outputFile, 
        //                "Delimited", "Proprietary", "Serial Number", sw);
        //            sw.Close();

        //            m_worker.ReportProgress(i, "\tExporting file: " + outputFile + "\n");
                    
        //            //Launch the export process for this batch flat file.
        //            prcsMSDSExport = this.exportMSDS(m_tempMsdsPath + "\\hilt.ini");

        //            /* TESTING PURPOSES TESTTEST
        //             * Use this to create subsets of data.  Tell it to only
        //             * grab x number of batch flat files for MSDS creation.
        //             */
        //            // if (i == 5) {
        //            //     break;
        //            // }
        //        }
        //    }
        //    catch (Exception ex) {
        //        this.writeToExportErrLog(m_exportErrorFile, "An error occurred while exporting MSDS files.  Error:  " + ex.Message);
        //    }

        //    return result;
        //}

        ///* Accepts the .ini file for a Batch Flat File and uses that to 
        // * execute a batch command that pulls all MSDS information
        // * for a specified Product Serial Number from the HMIRS database.
        // */
        //protected System.Diagnostics.Process exportMSDS(String iniFilePath) {
        //    // Initialize variables.
        //    System.Diagnostics.Process proc = null;

        //    // Check that the .ini file exists.
        //    if (File.Exists(iniFilePath)) {
        //        //Create command.
        //        String cmdMSDSExport = "onlinecd.exe" + " -d " + "\"" + iniFilePath + "\"";

        //        try {
        //            /* Create the ProcessStartInfo using "cmd" as the program to be run,
        //             * and then exit.  In this case, execute a MSDS export
        //             */
        //            System.Diagnostics.ProcessStartInfo msdsExportInfo =
        //                new System.Diagnostics.ProcessStartInfo("cmd", "/c " + cmdMSDSExport);

        //            // Do not create the black window.
        //            msdsExportInfo.CreateNoWindow = true;

        //            //Change the working directory to cdonline.exe's current directory.
        //            msdsExportInfo.WorkingDirectory = Configuration.CDROM_DataFolder;

        //            // Now we create a process, assign its ProcessStartInfo and start it
        //            proc = new System.Diagnostics.Process();
        //            proc.StartInfo = msdsExportInfo;
        //            proc.Start();

        //            //Check to see if the process has finished and exited.
        //            while (!proc.HasExited) {
        //                Thread.Sleep(5000);
        //            }

        //        }
        //        catch (Exception ex) {
        //            this.writeToExportErrLog(m_exportErrorFile, "Exception occurred during the MSDS export process.  Error:  " + ex.Message);
        //        }

        //    }
        //    else {
        //        this.writeToExportErrLog(m_exportErrorFile, "The following file could not be located: " + iniFilePath);
        //    }

        //    //Return the process for later use.
        //    return proc;
        //}

        //// Method for storing errors encountered during the HMIRS db export process.
        //private void writeToExportErrLog(FileInfo file, String errMsg) {
        //    //Check if this is our first time to append to the log during this process.
        //    if (m_firstTimeExport == true) {
        //        StreamWriter ft = File.AppendText(file.FullName);
        //        ft.WriteLine("DATE\t" + "ERROR MESSAGE");
        //        ft.WriteLine("");
        //        ft.Close();

        //        m_firstTimeExport = false;
        //    }

        //    String date = "" + System.DateTime.Now;
        //    StreamWriter sw = File.AppendText(file.FullName);
        //    sw.WriteLine(date + "\t" + errMsg);
        //    sw.Close();
        //}

        protected internal static string formatTickCountToHHmmss(long ticks) {
            const long tickHours = 3600000;
            const long tickMin = 60000;

            long remainder = 0;

            // Hours
            int hours = (int) (ticks / tickHours);
            remainder = ticks - hours * tickHours;

            // Minutes
            int minutes = (int) (remainder / tickMin);
            remainder = remainder - minutes * tickMin;

            // Seconds
            int seconds = (int) ((remainder + 500) / 1000);

            return String.Format("{0:D2}:{1:D2}:{2:D2}", hours, minutes, seconds);
        }
    }
}
