﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel;
using HILT_IMPORTER.DataObjects;
using System.Security.Principal;
using System.Collections;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Odbc;

namespace HILT_IMPORTER {
    /// <summary>
    /// Basic class to determine what type of AUL file we are dealing with
    /// </summary>
    public class AULImporter {
        public enum FILE_TYPE {
            EXCEL_8,    //Old format
            EXCEL_12,   // 2007+
            TEXT,
            INVALID,
            NO_FILENAME
        };

        public enum AUL_TYPE {
            AEL,
            SMCL,
            SHML,
            INVALID
        };

        // File Column Definitions
        public static Dictionary<string, int> AEL_COL_MAP = new Dictionary<string, int>
        {
            {"NIIN", 0},
            {"ITEM_NAME", 1},
            {"ANC", 2},             // COSAL
            {"QTY", 3},             // Allowance
            {"UOI", 4},
            {"UNIT_PRICE", 5},
            {"SMR", 6},
            {"COG", 7},
            {"FSC", 8},
            {"CAGE", 9},
            {"PRIME_API_REF_NUM", 10},
            {"P_TYPE", 11}
        };

        public static Dictionary<string, int> SHML_COL_MAP = new Dictionary<string, int>
        {
            {"NIINM_AAC", 0},
            {"NIINM_ALLOWED_OB", 1},
            {"NIINM_COG", 2},
            {"NIINM_FSC", 3},
            {"NIINM_NIIN", 4},
            {"NIINM_NOMEN", 5},
            {"NIINM_QUP", 6},
            {"NIINM_REMARKS", 7},
            {"NIINM_SMIC", 8},
            {"NIINM_SHELF_LIFE", 9},
            {"NIINM_SLAC", 10},
            {"NIINM_SMCC", 11},
            {"NIINM_SPMIG", 12},
            {"NIINM_TYPE_STOR", 13},
            {"NIINM_UI", 14},
            {"NIINM_UM", 15},
            {"NIINM_TYPE", 16},
            {"NIINM_PRICE_PER_UI", 17}
        };

        public static Dictionary<string, int> SMCL_COL_MAP = new Dictionary<string, int>
        {
            {"ID", 0},
            {"RPT_NUM", 1},
            {"NEHC_AVAIL", 2},
            {"TRADEMARK_NAME", 3},
            {"VIEWED", 4},
            {"NALC", 5},
            {"MANUFACTURER", 6},
            {"USAGE_CATEGORY", 7},
            {"ITEM_NAME", 8},
            {"RPT_DATE", 9},
            {"SMCC", 10},
            {"MILSPEC", 11},
            {"RSN_FR_CAT", 12},
            {"TABLE_NAME", 13},
            {"NIIN", 14},
            {"UI", 15},
            {"UM", 16},
            {"CAGE", 17},
            {"SLC", 18},
            {"SLAC", 19},
            {"TYPE_OF_STORAGE", 20},
            {"APPLICATION", 21},
            {"SPMIG", 22},
            {"COG", 23},
            {"FSC", 24},
            {"REMARKS", 25},
            {"MSDS_NUM", 26},
            {"SMIC", 27},
            {"serial_number", 28},
            {"pdf_file_name", 29},
            {"REV_DATE", 30},
            {"ENV_PREFERRED", 31},
            {"P_NICN", 32}
        };

        // Hashtables
        Hashtable m_htNiins = null;
        Hashtable m_htCogs = null;
        Hashtable m_htCatalogGroups = null;
        Hashtable m_htSmccs = null;
        Hashtable m_htSlcs = null;
        Hashtable m_htSlacs = null;
        Hashtable m_htStorageTypes = null;
        Hashtable m_htUsageCategories = null;


        public AULImporter() {
        }

        // Check the file extension to see if we have a valid file type
        // Throws an exception if its not
        public static FILE_TYPE validateFileExtension(String filePath) {
            FILE_TYPE fileType = FILE_TYPE.INVALID;
            // Check to see what type of file we have
            if (filePath.Equals("")) {
                fileType = FILE_TYPE.NO_FILENAME;
            }
            else if (filePath.ToLower().Contains(".xlsx") || filePath.ToLower().Contains(".xlsm")) {
                fileType = FILE_TYPE.EXCEL_12;
            }
            else if (filePath.ToLower().Contains(".xls") || filePath.ToLower().Contains(".xlm")) {
                fileType = FILE_TYPE.EXCEL_8;
            }

            return fileType;
        }
 
        public static AUL_TYPE determineFileType(String[] headerRow) {
            AUL_TYPE fileType = AUL_TYPE.INVALID;
            const int SHML_LEN = 18;
            const int SMCL_LEN = 33;

            // Columns to check for valid file type - check colymns 0, 1, 15
            const String SHML_FILE = "NIINM_AAC";
            const String SHML_CHECK = "NIINM_ALLOWED_OB";
            const String SHML_TYPE = "NIINM_UM";
            const String SMCL_FILE = "ID";
            const String SMCL_CHECK = "RPT_NUM";
            const String SMCL_TYPE = "UI";

            // Get the column names from the file to see what type it is
            String columnFile = headerRow[0];
            String columnCheck = headerRow[1];
            String columnType = headerRow[15];

            // Check the header row length to get started
            switch (headerRow.Length) {
                case SHML_LEN :
                    // SHML_ - Check the header for the keywords
                    if (SHML_FILE.Equals(columnFile) && SHML_CHECK.Equals(columnCheck)
                            && SHML_TYPE.Equals(columnType))
                        fileType = AUL_TYPE.SHML;
                    break;
                case SMCL_LEN :
                    // SMCL_ - Check the header for the keywords
                    if (SMCL_FILE.Equals(columnFile) && SMCL_CHECK.Equals(columnCheck)
                            && SMCL_TYPE.Equals(columnType))
                        fileType = AUL_TYPE.SMCL;
                    break;
            }

            return fileType;
        }

        public static String analyzeAULFile(AULFile fileData, long hull_type_id,
                BackgroundWorker worker) {
            String result = null;
            long niinCol = 0;
            DatabaseManager manager = new DatabaseManager();

            // Check for existing inventory
            int invCount = manager.countAULRecords(hull_type_id);
            int threshold = invCount / 4;
            int nCount = 0;

            if (invCount == 0) {
                // New unit
                //fileData.FileStats.NewRecords = fileData.DataRows.Count;
                // Generate the Initial load message
                result = "INITIAL LOAD" +
                    "\n\tThe database does not currently contain records for this unit type.\n";
            }
            //else {
                // Get the NIIN column number
                if (fileData.FileType == AULImporter.AUL_TYPE.SHML) {
                    niinCol = AULImporter.SHML_COL_MAP["NIINM_NIIN"];
                }
                else {
                    // Default SMCL
                    niinCol = AULImporter.SMCL_COL_MAP["NIIN"];
                }

                long startTick = Environment.TickCount;
                long splitStartTick = startTick;
                long getMasterTicks = 0;
                long changeTicks = 0;

                // Count changes from existing inventory
                Hashtable htExisting = manager.getAULNIINsForHullTypeId(hull_type_id);
                Hashtable htFileNiins = new Hashtable();

                // Go through the data counting new and existing reocrds
                foreach (String[] record in fileData.DataRows) {
                    String key = null;

                    nCount++;
                    Application.DoEvents();
                    
                    key = record[niinCol];

                    // Make sure we have a niin
                    if (key.Length == 0) {
                        fileData.FileStats.BlankRecords++;
                        continue;
                    }
                    // See if this is a duplicate niin
                    if (htFileNiins.ContainsKey(key)) {
                        fileData.FileStats.DuplicateRecords++;
                        continue;
                    }

                    // Add the File NIIN to the hash
                    htFileNiins[key] = null;

                    // See if the record exists in the database
                    if (htExisting.ContainsKey(key)) {
                        fileData.FileStats.ExistingRecords++;
                        splitStartTick = Environment.TickCount;
                        // Get the AUL record and check for changes
                        AULRecord aulRecord = manager.getAULMasterRecord(key);
                        getMasterTicks += Environment.TickCount - splitStartTick; 
                        if (checkForChanges(record, aulRecord, fileData.FileType) > 0) {
                            fileData.FileStats.ChangedRecords++;
                        }

                        changeTicks += Environment.TickCount - splitStartTick;
                    }
                    else {
                        // New niin
                        fileData.FileStats.NewRecords++;

                        // See if the master record exists
                        if (!manager.isNIINInMaster(key)) {
                            // Add it to the new Masters list
                            fileData.NewMasterNIINs.Add(key);
                            fileData.FileStats.NewMasterRecords++;
                        }
                    }

                    // Update the progress bar
                    if (nCount % 10 == 0) {
                        worker.ReportProgress(nCount);
                    }
                }

                // Now switch and find the records to delete
                foreach (String niin in htExisting.Keys) {
                    if (!htFileNiins.ContainsKey(niin)) 
                        fileData.FileStats.DeletedRecords++;
                }

                System.Diagnostics.Debug.WriteLine("Inventory Count time: " 
                    + (Environment.TickCount - startTick - changeTicks).ToString());
                System.Diagnostics.Debug.WriteLine("Change Compare time: " + changeTicks.ToString());

                System.Diagnostics.Debug.WriteLine("Get MAster time: " + getMasterTicks.ToString());

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("\nRecords analyzed:" + fileData.DataRows.Count);
                sb.Append("\tNew:  " + fileData.FileStats.NewRecords);
                sb.Append("\tChanged: " + fileData.FileStats.ChangedRecords);
                sb.AppendLine("\tDeleted: " + fileData.FileStats.DeletedRecords);
                if (fileData.FileStats.NewMasterRecords > 0) 
                    sb.AppendLine("\tNew Master NIINs: "+ fileData.FileStats.NewMasterRecords);
                 if (fileData.FileStats.BlankRecords > 0 || fileData.FileStats.DuplicateRecords > 0) {
                    sb.AppendLine("\nWARNING - File contains records with invalid NIINs");
                    sb.Append("\tMissing NIINs: " + fileData.FileStats.BlankRecords);
                    sb.AppendLine("\t\tDuplicate NIINs: " + fileData.FileStats.DuplicateRecords);
               }

                // See if we have more than 25% changed
                int validRecords = fileData.DataRows.Count
                    - fileData.FileStats.BlankRecords - fileData.FileStats.DuplicateRecords;
                float changedRecords = fileData.FileStats.DeletedRecords + fileData.FileStats.NewRecords
                        + fileData.FileStats.ChangedRecords;
                if (invCount > 0 && changedRecords / validRecords > .25) {
                    sb.AppendLine("\nWARNING - More than 25% of the valid AUL records have changed.");
                }

                result += sb.ToString();
            //}

            return result;
        }

        public bool import(AULFile fileData, String vesselType, BackgroundWorker worker) {
            bool result = true;
            AULFile tempFileData = new AULFile();
            SqlConnection conn = null;
            String returnMsg = "";
            int nCount = 0;

            // Reset the fileStats for porcessing
            fileData.FileStats = new AULStats();

            try {
                conn = new SqlConnection(Configuration.ConnectionInfo);
                conn.Open();
                DatabaseManager manager = new DatabaseManager();

                // Populate the hash tables
                setupImport(manager);

                // Set UIC and upload date
                String uic = fileData.FileType.ToString() + "_" + vesselType;
                String userName = WindowsIdentity.GetCurrent().Name;
                bool processed = true;

                // Get the hull_type_id for the file - note do this in a list as the SMCL covers more than 1 hull type
                List<long> hull_type_ids = manager.getHullIDsFromValue(vesselType, conn);

                if (hull_type_ids.Count == 0) {
                    result = false;
                    worker.ReportProgress(HiltImportMDIForm.STATUS_COUNT_BLANK, "\n\n****************************************\nERROR - The vessel type (" + 
                        vesselType + ") is not a " +
                        "valid type. Please add the vessel type to the HILT Hub database." +
                        "\n\n****************************************");

                    return result;
                }

                DateTime transDate = DateTime.Now;

                foreach (long hull_type_id in hull_type_ids) {
                    // Create a new hash of processed niins
                    Hashtable htProcessedNiins = new Hashtable();

                    // Get the existing NIINs
                    m_htNiins = manager.getAULNIINsForHullTypeId(hull_type_id);

                    // Process the AUL data into the database
                    foreach (String[] row in fileData.DataRows) {
                        transDate = DateTime.Now;
                        AULRecord record = null;

                        // Populate the record based on the file type
                        if (fileData.FileType == AUL_TYPE.SHML) {
                            // SHML data
                            record = populateAULRecordFromSHML(hull_type_id, row);
                        }
                        else {
                            // SMCL data - default
                            record = populateAULRecordFromSMCL(hull_type_id, row);
                        }

                        nCount++;

                        // Don't process if the record doesn't have a NIIN
                        if (record.niin.Length == 0) {
                            tempFileData.FileStats.BlankRecords++;
                            continue;
                        }
                        else if (htProcessedNiins.ContainsKey(record.niin)) {
                            tempFileData.FileStats.DuplicateRecords++;
                            continue;
                        }

                        // Populate the ID values
                        populateAULRecordIDs(record);

                        // Check to make sure we have a master NIIN
                        if (!manager.isNIINInMaster(record.niin)) {
                            // insert the master niin
                            long newID = manager.insertAULMasterRecord(uic, record, userName);

                            record.aul_id = newID;
                            tempFileData.FileStats.NewMasterRecords++;
                        }

                        // What do we do with this record?
                        if (m_htNiins.ContainsKey(record.niin)) {
                            // Do an update
                            // Set the aul_id for the record
                            record.aul_id = (Int64)m_htNiins[record.niin];
                            if (manager.updateAULHullTypeRecord(record, userName) != 0) {
                                // Add the NIIN to the processed list
                                htProcessedNiins[record.niin] = null;
                                tempFileData.FileStats.ChangedRecords++;
                            }
                            else {
                                tempFileData.FileStats.ErrorRecords++;
                                processed = false;
                            }
                        }
                        else {
                            // New record - get the aul_id for this NIIN
                            record.aul_id = manager.getAULIdFromNIIN(record.niin);

                            // Insert the record
                            if (manager.insertAULHullTypeRecord(record, userName) != 0) {
                                // Add the NIIN to the processed list
                                htProcessedNiins[record.niin] = null;
                                tempFileData.FileStats.NewRecords++;
                            }
                            else {
                                tempFileData.FileStats.ErrorRecords++;
                                processed = false;
                            }
                        }

                        // Update the progress bar
                        if (nCount % 10 == 0) {
                            worker.ReportProgress(nCount);
                        }
                    }

                    // Go though the hashs and delete the missing ones
                    foreach (String niin in m_htNiins.Keys) {
                        if (!htProcessedNiins.ContainsKey(niin)) {
                            // Delete this one
                            if (manager.deleteAULHullTypeRecord((Int64)m_htNiins[niin], hull_type_id,
                                conn, userName) != 0) {
                                tempFileData.FileStats.DeletedRecords++;
                            }
                            else {
                                tempFileData.FileStats.ErrorRecords++;
                                processed = false;
                            }
                        }
                    }
                }

                if (processed) {
                    // Insert a record into the file uploads table
                    manager.updateFileTable(DatabaseManager.FILE_TABLES.FileUpload,
                        uic, fileData.FileName, Configuration.UPLOAD_TYPES.AUL.ToString(),
                        fileData.FileDate, transDate, userName, fileData.FileStats.NewRecords,
                        fileData.FileStats.ExistingRecords, fileData.FileStats.DeletedRecords);

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Successfully processed for " + hull_type_ids.Count + " hull types");
                    sb.AppendLine("\tMaster Records inserted: " + tempFileData.FileStats.NewMasterRecords);
                    sb.AppendLine("\tRecords inserted: " + tempFileData.FileStats.NewRecords);
                    sb.AppendLine("\tRecords updated: " + tempFileData.FileStats.ChangedRecords);
                    sb.AppendLine("\tRecords deleted: " + tempFileData.FileStats.DeletedRecords);
                    sb.AppendLine("\tBlank NIIN records: " + tempFileData.FileStats.BlankRecords);
                    sb.AppendLine("\tDuplicate NIIN records: " + tempFileData.FileStats.DuplicateRecords);
                    if (tempFileData.FileStats.ErrorRecords > 0)
                        sb.AppendLine("\tError records (see log file): " + tempFileData.FileStats.ExistingRecords);

                    returnMsg = sb.ToString();
                }
                else {
                    returnMsg = "*********************************************" +
                        "\tERROR - processing AUL file import: " +
                        fileData.FileName + 
                        "\tErrors weere encounterd for: " + fileData.FileStats.ErrorRecords.ToString() +
                        " records\n\tSee log file for details";
                    nCount = 0;
                    result = false;
                }
            }
            catch (Exception ex) {
                returnMsg = "*********************************************" +
                    "\tERROR - processing AUL file import: " + fileData.FileName +
                    " - " + ex.Message +
                    " - See log file for details";
                nCount = 0;
                result = false;
            }
            finally {
                conn.Close();
            }

            // Update the text box with the returnMsg
            worker.ReportProgress(nCount, returnMsg);

            cleanupImport();

            return result;
        }

        private AULRecord populateAULRecordFromSHML(long hull_type_id, String[] row) {
            AULRecord record = new AULRecord();

            record.hull_type_id = hull_type_id;

            // Go through the fields and get the data
            record.aac = row[SHML_COL_MAP["NIINM_AAC"]];
            record.Usage_Category = row[SHML_COL_MAP["NIINM_ALLOWED_OB"]];
            record.COG = row[SHML_COL_MAP["NIINM_COG"]];
            long nTemp = 0;
            Int64.TryParse(row[SHML_COL_MAP["NIINM_FSC"]], out nTemp);
            record.fsc = nTemp;
            record.niin = row[SHML_COL_MAP["NIINM_NIIN"]];
            record.description = row[SHML_COL_MAP["NIINM_NOMEN"]];
            record.qup = row[SHML_COL_MAP["NIINM_QUP"]];
            record.remarks = row[SHML_COL_MAP["NIINM_REMARKS"]];
            record.smic = row[SHML_COL_MAP["NIINM_SMIC"]];
            record.Shelf_Life_Code = row[SHML_COL_MAP["NIINM_SHELF_LIFE"]];
            record.Shelf_Life_Action_Code = row[SHML_COL_MAP["NIINM_SLAC"]];
            record.SMCC = row[SHML_COL_MAP["NIINM_SMCC"]];
            record.spmig = row[SHML_COL_MAP["NIINM_SPMIG"]];
            record.Storage_Type = row[SHML_COL_MAP["NIINM_TYPE_STOR"]];
            record.ui = row[SHML_COL_MAP["NIINM_UI"]];
            record.um = row[SHML_COL_MAP["NIINM_UM"]];
            //[SHML_COL_MAP["NIINM_TYPE"]] - not used
            record.price_per_ui = row[SHML_COL_MAP["NIINM_PRICE_PER_UI"]];
            
            return record;
        }

        private AULRecord populateAULRecordFromSMCL(long hull_type_id, String[] row) {
            AULRecord record = new AULRecord();

            record.hull_type_id = hull_type_id;

            // Go through the fields and get the data
            //record.smcl_master_id = row[SMCL_COL_MAP["ID"]]; - not used
            //record.r = row[SMCL_COL_MAP["RPT_NUM"]];
            //record.n = row[SMCL_COL_MAP["NEHC_AVAIL"]];
            //record.tr = row[SMCL_COL_MAP["TRADEMARK_NAME"]];
            //record = row[SMCL_COL_MAP["VIEWED"]];
            //record = row[SMCL_COL_MAP["NALC"]];
            record.manufacturer = row[SMCL_COL_MAP["MANUFACTURER"]];
            record.Usage_Category = row[SMCL_COL_MAP["USAGE_CATEGORY"]];
            record.description = row[SMCL_COL_MAP["ITEM_NAME"]];
            // record = row[SMCL_COL_MAP["RPT_DATE"]];
            record.SMCC = row[SMCL_COL_MAP["SMCC"]];
            record.specs = row[SMCL_COL_MAP["MILSPEC"]];
            //record. = row[SMCL_COL_MAP["RSN_FR_CAT"]];
            record.Catalog_Group = row[SMCL_COL_MAP["TABLE_NAME"]];
            record.niin = row[SMCL_COL_MAP["NIIN"]];
            record.ui = row[SMCL_COL_MAP["UI"]];
            record.um = row[SMCL_COL_MAP["UM"]];
            record.cage = row[SMCL_COL_MAP["CAGE"]];
            record.Shelf_Life_Code = row[SMCL_COL_MAP["SLC"]];
            record.Shelf_Life_Action_Code = row[SMCL_COL_MAP["SLAC"]];
            record.Storage_Type = row[SMCL_COL_MAP["TYPE_OF_STORAGE"]];
            //record = row[SMCL_COL_MAP["APPLICATION"]];
            record.spmig = row[SMCL_COL_MAP["SPMIG"]];
            record.COG = row[SMCL_COL_MAP["COG"]];
            long nTemp = 0;
            Int64.TryParse(row[SMCL_COL_MAP["FSC"]], out nTemp);
            record.fsc = nTemp;
            record.remarks = row[SMCL_COL_MAP["REMARKS"]];
            record.msds_num = row[SMCL_COL_MAP["MSDS_NUM"]];
            record.smic = row[SMCL_COL_MAP["SMIC"]];
            record.catalog_serial_number = row[SMCL_COL_MAP["serial_number"]];
            //record.p = row[SMCL_COL_MAP["pdf_file_name"]];
            //record = row[SMCL_COL_MAP["REV_DATE"]];
            //record = row[SMCL_COL_MAP["ENV_PREFERRED"]];
            //record = row[SMCL_COL_MAP["P_NICN"]]}

            // Fix the SLAC 00 code
            if (record.Shelf_Life_Action_Code.Equals("0"))
                record.Shelf_Life_Action_Code = "00";
            
            return record;
        }

        private void populateAULRecordIDs(AULRecord record) {
            // COG
            if (record.COG != null && m_htCogs.ContainsKey(record.COG))
                record.cog_id = (Int64) m_htCogs[record.COG];
            // Catalog Group
            if (record.Catalog_Group != null && m_htCatalogGroups.ContainsKey(record.Catalog_Group))
                record.catalog_group_id = (Int64) m_htCatalogGroups[record.Catalog_Group];
            // SMCC
            if (record.SMCC != null && m_htSmccs.ContainsKey(record.SMCC))
                record.smcc_id = (Int64) m_htSmccs[record.SMCC];
            // SLC
            if (record.Shelf_Life_Code != null && m_htSlcs.ContainsKey(record.Shelf_Life_Code))
                record.shelf_life_code_id = (Int64) m_htSlcs[record.Shelf_Life_Code];
            // SLAC
            if (record.Shelf_Life_Action_Code != null && m_htSlacs.ContainsKey(record.Shelf_Life_Action_Code))
                record.shelf_life_action_code_id = (Int64) m_htSlacs[record.Shelf_Life_Action_Code];
            // Storgate Type
            if (record.Storage_Type != null && m_htStorageTypes.ContainsKey(record.Storage_Type))
                record.storage_type_id = (Int64) m_htStorageTypes[record.Storage_Type];
            // Usage Category
            if (record.Usage_Category != null && m_htUsageCategories.ContainsKey(record.Usage_Category))
                record.usage_category_id = (Int64) m_htUsageCategories[record.Usage_Category];
        }

        private void setupImport(DatabaseManager mgr) {
            // Get the hashtables for the ID fields
            m_htCogs = mgr.getKeywordIdHashtableForTable("cog_codes", "cog", "cog_id");
            m_htCatalogGroups = mgr.getKeywordIdHashtableForTable("catalog_groups", "group_name",
                "catalog_group_id");
            m_htSmccs = mgr.getKeywordIdHashtableForTable("smcc", "smcc", "smcc_id");
            m_htSlcs = mgr.getKeywordIdHashtableForTable("shelf_life_code", "slc", 
                "shelf_life_code_id");
            m_htSlacs = mgr.getKeywordIdHashtableForTable("shelf_life_action_code", "slac", 
                "shelf_life_action_code_id");
            m_htStorageTypes = mgr.getKeywordIdHashtableForTable("storage_type", "type", 
                "storage_type_id");
            m_htUsageCategories = mgr.getKeywordIdHashtableForTable("usage_category", "category",
                "usage_category_id");
        }

        private void cleanupImport() {
            // Get the hashtables for the ID fields
            m_htCogs = null;
            m_htCatalogGroups = null;
            m_htSmccs = null;
            m_htSlcs = null;
            m_htSlacs = null;
            m_htStorageTypes = null;
            m_htUsageCategories = null;
        }

        public Configuration.FILE_CHANGES validateAULChanges(AULFile fileData, long hull_type_id,
                DatabaseManager manager) {
            Configuration.FILE_CHANGES result = Configuration.FILE_CHANGES.Acceptable_Changes;
            int changes = 0;

            int invCount = manager.countAULRecords(hull_type_id);
            int threshold = invCount / 4;

            if (invCount == 0) {
                // New unit
                //fileData.FileStats.NewRecords = fileData.DataRows.Count;
                result = Configuration.FILE_CHANGES.Initial_Load;
            }
            //else {
                // Count changes from existing inventory
                Hashtable htExisting = manager.getAULNIINsForHullTypeId(hull_type_id);
                Hashtable htFileNiins = new Hashtable();

                // Go through the data counting new and existing reocrds
                foreach (String[] record in fileData.DataRows) {
                    String key = null;
                    String usage = null;
                    switch (fileData.FileType) {
                        case AULImporter.AUL_TYPE.SHML:
                            key = record[AULImporter.SHML_COL_MAP["NIINM_NIIN"]];
                            usage = record[AULImporter.SHML_COL_MAP["NIINM_ALLOWED_OB"]];
                            break;
                        case AULImporter.AUL_TYPE.SMCL: // SMCL
                            key = record[AULImporter.SMCL_COL_MAP["NIIN"]];
                            usage = record[AULImporter.SMCL_COL_MAP["USAGE_CATEGORY"]];
                            break;
                    }

                    // Make sure we have a niin
                    if (key.Length == 0) {
                        fileData.FileStats.BlankRecords++;
                        continue;
                    }
                    // See if this is a duplicate niin
                    if (htFileNiins.ContainsKey(key)) {
                        fileData.FileStats.DuplicateRecords++;
                        continue;
                    }

                    // Add the File NIIN to the hash
                    htFileNiins[key] = null;

                    // See if the record exists in the database
                    if (htExisting.ContainsKey(key)) {
                        fileData.FileStats.ExistingRecords++;

                        // Get the AUL record and check for changes
                        AULRecord aulRecord = manager.getAULMasterRecord(key, hull_type_id);
                        if (checkForChanges(record, aulRecord, fileData.FileType) > 0) {
                            changes++;
                            fileData.FileStats.ChangedRecords++;
                        }
                    }
                    else {
                        // New niin
                        changes++;
                        fileData.FileStats.NewRecords++;
                    }
                }

                // Now switch and find the records to delete
                foreach (String niin in htExisting.Keys) {
                    if (!htFileNiins.ContainsKey(niin)) {
                        fileData.FileStats.DeletedRecords++;
                    }
                }

                changes = invCount - (fileData.FileStats.NewRecords +
                    fileData.FileStats.ChangedRecords + fileData.FileStats.DeletedRecords);

                // check the threshold
                if (changes > threshold) {
                    result = Configuration.FILE_CHANGES.Threshold_Exceeded;
                }
            //}

            return result;
        }

        private static int checkForChanges(String[] row, AULRecord aulRecord, AUL_TYPE aulType) {
            int changed = 0;

            // Go through and compare the database record against the new  record
            // Exit as soon as we have a change
            try {
                switch (aulType) {
                    case AUL_TYPE.SHML:
                        if (!aulRecord.aac.ToUpper().Equals(row[SHML_COL_MAP["NIINM_AAC"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.Usage_Category.ToUpper().Equals(row[SHML_COL_MAP["NIINM_ALLOWED_OB"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.COG.ToUpper().Equals(row[SHML_COL_MAP["NIINM_COG"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.fsc.ToString().ToUpper().Equals(row[SHML_COL_MAP["NIINM_FSC"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        //if (changed == 0 && !aulRecord.description.ToUpper().Equals(row[SHML_COL_MAP["NIINM_NOMEN"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.qup.ToUpper().Equals(row[SHML_COL_MAP["NIINM_QUP"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.remarks.ToUpper().Equals(row[SHML_COL_MAP["NIINM_REMARKS"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.smic.ToUpper().Equals(row[SHML_COL_MAP["NIINM_SMIC"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.Shelf_Life_Code.ToUpper().Equals(row[SHML_COL_MAP["NIINM_SHELF_LIFE"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.Shelf_Life_Action_Code.ToUpper().Equals(row[SHML_COL_MAP["NIINM_SLAC"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.SMCC.ToUpper().Equals(row[SHML_COL_MAP["NIINM_SMCC"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.spmig.ToUpper().Equals(row[SHML_COL_MAP["NIINM_SPMIG"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.Storage_Type.ToUpper().Equals(row[SHML_COL_MAP["NIINM_TYPE_STOR"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.ui.ToUpper().Equals(row[SHML_COL_MAP["NIINM_UI"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.um.ToUpper().Equals(row[SHML_COL_MAP["NIINM_UM"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.price_per_ui.ToUpper().Equals(row[SHML_COL_MAP["NIINM_PRICE_PER_UI"]].ToUpper())) {
                            changed = 1;
                        }
                        break;
                    case AUL_TYPE.SMCL:
                        //if (!aulRecord.manufacturer.ToUpper().Equals(row[SMCL_COL_MAP["MANUFACTURER"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.Usage_Category.ToUpper().Equals(row[SMCL_COL_MAP["USAGE_CATEGORY"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.description.ToUpper().Equals(row[SMCL_COL_MAP["ITEM_NAME"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.SMCC.ToUpper().Equals(row[SMCL_COL_MAP["SMCC"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.specs.ToUpper().Equals(row[SMCL_COL_MAP["MILSPEC"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.Catalog_Group.ToUpper().Equals(row[SMCL_COL_MAP["TABLE_NAME"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.ui.ToUpper().Equals(row[SMCL_COL_MAP["UI"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.um.ToUpper().Equals(row[SMCL_COL_MAP["UM"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.cage.ToUpper().Equals(row[SMCL_COL_MAP["CAGE"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.Shelf_Life_Code.ToUpper().Equals(row[SMCL_COL_MAP["SLC"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.Shelf_Life_Action_Code.ToUpper().Equals(row[SMCL_COL_MAP["SLAC"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.Storage_Type.ToUpper().Equals(row[SMCL_COL_MAP["TYPE_OF_STORAGE"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.spmig.ToUpper().Equals(row[SMCL_COL_MAP["SPMIG"]].ToUpper())) {
                            changed = 1;
                        }
                        if (changed == 0 && !aulRecord.COG.ToUpper().Equals(row[SMCL_COL_MAP["COG"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.fsc.ToString().ToUpper().Equals(row[SMCL_COL_MAP["FSC"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        //if (changed == 0 && !aulRecord.remarks.ToUpper().Equals(row[SMCL_COL_MAP["REMARKS"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        //if (changed == 0 && !aulRecord.msds_num.ToUpper().Equals(row[SMCL_COL_MAP["MSDS_NUM"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        if (changed == 0 && !aulRecord.smic.ToUpper().Equals(row[SMCL_COL_MAP["SMIC"]].ToUpper())) {
                            changed = 1;
                        }
                        //if (changed == 0 && !aulRecord.catalog_serial_number.ToUpper().Equals(row[SMCL_COL_MAP["serial_number"]].ToUpper())) {
                        //    changed = 1;
                        //}
                        break;
                }
            }
            catch (Exception ex) {
                DatabaseManager.logMessage(DatabaseManager.LOG_LEVELS.Error, "Error in checkForChanges niin = " + aulRecord.niin +
                    "\n\t" + ex.Message);
            }

            return changed;
        }

        public static void normalizeNIINs(AULFile fileData) {
            switch (fileData.FileType) {
                case AUL_TYPE.SHML :
                    foreach (String[] row in fileData.DataRows) {
                        if (row[SHML_COL_MAP["NIINM_NIIN"]].Length > 0 
                                && row[SHML_COL_MAP["NIINM_NIIN"]].Length < 9)
                            row[SHML_COL_MAP["NIINM_NIIN"]] = row[SHML_COL_MAP["NIINM_NIIN"]].PadLeft(9, '0');
                    }
                    break;
                case AUL_TYPE.SMCL :
                    foreach (String[] row in fileData.DataRows) {
                        if (row[SMCL_COL_MAP["NIIN"]].Length > 0
                                && row[SMCL_COL_MAP["NIIN"]].Length < 9)
                            row[SMCL_COL_MAP["NIIN"]] = row[SMCL_COL_MAP["NIIN"]].PadLeft(9, '0');
                    }
                    break;
            }
        }
    }
}
