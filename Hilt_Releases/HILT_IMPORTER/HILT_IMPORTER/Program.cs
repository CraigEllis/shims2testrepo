﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Data.Odbc;
using System.IO;
using HILT_IMPORTER.DataObjects;
using System.Runtime.CompilerServices;


namespace HILT_IMPORTER
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            long tickStart = Environment.TickCount;
            long elapsedTicks = Environment.TickCount - tickStart;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            try {
                Application.Run(new HiltImportMDIForm());
            }
            catch (Exception ex) {
                // Couldn't logon - or some other funky error
                MessageBox.Show("Could not start SHIMS MSDS Importer\n\n" + ex.Message,
                    "SHIMS MSDS Importer");
            }
        }
    }
}
