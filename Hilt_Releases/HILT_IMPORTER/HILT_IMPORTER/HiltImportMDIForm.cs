﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

namespace HILT_IMPORTER {
    public partial class HiltImportMDIForm : Form {
        public const int STATUS_COUNT_BLANK = int.MinValue + 1;

        private int childFormNumber = 0;

        // The child forms
        StartupForm startupForm = null;
        MSDSImportForm msdsImportForm = null;
        AULImportForm aulImportForm = null;
        InventoryImportForm inventoryImportForm = null;

        public HiltImportMDIForm() {
            InitializeComponent();
            this.CenterToScreen();

            //TODO - Prompt the user for credentials
            if (!userLogon()) {
                this.ExitToolsStripMenuItem_Click(null, null);
            }

            // Log a startup message
            DatabaseManager.logMessage(DatabaseManager.LOG_LEVELS.Info,
                "***** SHIMS Importer started: " + DateTime.Now.ToString() + " User: " +
                WindowsIdentity.GetCurrent().Name + " *****");
        }

        #region Exposed Form Properties
        public bool StatusBarLabel_Visible {
            get {
                return mainStatusLabel.Visible;
            }
            set {
                mainStatusLabel.Visible = value;
            }
        }

        public string StatusBarLabel_Value {
            get {
                return mainStatusLabel.Text;
            }
            set {
                mainStatusLabel.Text = value;
            }
        }

        public bool StatusBarCount_Visible {
            get {
                return countStatusLabel.Visible;
            }
            set {
                countStatusLabel.Visible = value;
            }
        }

        public long StatusBarCount_Value {
            get {
                return Int32.Parse(countStatusLabel.Text);
            }
            set {
                countStatusLabel.Text = value.ToString();

                if (value == STATUS_COUNT_BLANK)
                    countStatusLabel.Text = "";
            }
        }

        public bool ProgressBar_Visible {
            get {
                return progressBar.Visible;
            }
            set {
                progressBar.Visible = value;
            }
        }

        public int StatusBarProgress_Value {
            get {
                return progressBar.Value;
            }
            set {
                if (value >= progressBar.Minimum && value <= progressBar.Maximum)
                    progressBar.Value = value;
            }
        }
        #endregion

        private void ShowNewForm(object sender, EventArgs e) {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK) {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK) {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e) {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e) {
            Type nodeType = ActiveMdiChild.ActiveControl.GetType();
            if (nodeType == typeof(RichTextBox)) {
                //Graphics objGraphics;
                Clipboard.SetText(
                    ((RichTextBox) ActiveMdiChild.ActiveControl).SelectedText);
                //Clipboard.Clear();
            }
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e) {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e) {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e) {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e) {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e) {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e) {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e) {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e) {
            foreach (Form childForm in MdiChildren) {
                childForm.Close();
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e) {
            // Display the setup form
            //SetupForm setup = new SetupForm(this);

            //setup.ShowDialog(this);
        }

        private bool userLogon() {
            bool logon = false;
            int tries = 0;
            const int maxTries = 3;
            String errMsg = "";

            // Prompt the user for their usernam and password
            while (tries < maxTries) {
                tries++;

                //TODO - Use windows auth
                logon = true;
            }
            
            // Check DB to see if they have admin privileges
            if (logon) {
                //TODO - use hazmat code to ping the database
            }

            if (!logon) {
                // See if they maxed out on tries
                if (tries == maxTries) {
                    errMsg = "SHIMS Importer:\nYou have exceeded the maximum number of attempts to log on.";
                }

                MessageBox.Show(this, errMsg, "SHIMS Importer");
            }

            return logon;
        }

        private void msdsImportToolStripMenuItem_Click(object sender, EventArgs e) {
            // Show the MSDS Import form
            if (msdsImportForm == null) {
                msdsImportForm = new MSDSImportForm();
                msdsImportForm.MdiParent = this;
                msdsImportForm.WindowState = FormWindowState.Maximized;
                msdsImportForm.Show();
                Application.DoEvents();
            }
            else {
                msdsImportForm.WindowState = FormWindowState.Maximized;
                msdsImportForm.Show();
            }
        }

        private void aulImportToolStripMenuItem_Click(object sender, EventArgs e) {
            // Show the AUL Import form
            if (aulImportForm == null) {
                aulImportForm = new AULImportForm();
                aulImportForm.MdiParent = this;
                aulImportForm.Show();
                aulImportForm.WindowState = FormWindowState.Maximized;
            }
            else {
                aulImportForm.Show();
                aulImportForm.WindowState = FormWindowState.Maximized;
            }
        }

        private void inventoryImportToolStripMenuItem_Click(object sender, EventArgs e) {
            // Show the Inventory Import form
            if (inventoryImportForm == null) {
                inventoryImportForm = new InventoryImportForm();
                inventoryImportForm.MdiParent = this;
                inventoryImportForm.Show();
                inventoryImportForm.WindowState = FormWindowState.Maximized;
            }
            else {
                inventoryImportForm.Show();
                inventoryImportForm.WindowState = FormWindowState.Maximized;
            }
        }

        private void Datetimer_Tick(object sender, EventArgs e) {
            // Update the date status box
            this.dateStatusLabel.Text = String.Format("{0:MM/dd/yyyy : HH:mm}", DateTime.Now);
        }

        private void HiltImportMDIForm_Shown(object sender, EventArgs e) {
            // Start by showing the AUL Import form as default
            //aulImportToolStripMenuItem_Click(null, null);
            // Start by showing the MSDS Import form as default
            //msdsImportToolStripMenuItem_Click(null, null);
            /*if (startupForm == null) {
                startupForm = new StartupForm();
                startupForm.MdiParent = this;
                startupForm.WindowState = FormWindowState.Maximized;
                startupForm.Show();
                Application.DoEvents();
            }
            else {
                startupForm.WindowState = FormWindowState.Maximized;
                startupForm.Show();
            }*/
            if (msdsImportForm == null)
            {
                msdsImportForm = new MSDSImportForm();
                msdsImportForm.MdiParent = this;
                msdsImportForm.WindowState = FormWindowState.Maximized;
                msdsImportForm.Show();
                Application.DoEvents();
            }
            else
            {
                msdsImportForm.WindowState = FormWindowState.Maximized;
                msdsImportForm.Show();
            }
        }

        private bool isFormExist(String formName) {
            bool exists = false;

            // Go through the child forms to see is we already have 

            return exists;
        }

        private void HiltImportMDIForm_FormClosing(object sender, FormClosingEventArgs e) {
            // Log a shutdown message
            DatabaseManager.logMessage(DatabaseManager.LOG_LEVELS.Info,
                "***** SHIMS import ended: " + DateTime.Now.ToString() + " User: " +
                WindowsIdentity.GetCurrent().Name + " *****");
        }

        // Protected methods for the startup form to call
        protected internal void showAULImportForm() {
            aulImportToolStripMenuItem_Click(null, null);
        }

        protected internal void showMSSLImportForm() {
            inventoryImportToolStripMenuItem_Click(null, null);
        }

        protected internal void showMSDSImportForm() {
            msdsImportToolStripMenuItem_Click(null, null);
        }

        private void HiltImportMDIForm_Load(object sender, EventArgs e)
        {
            progressBar.Size = new Size(this.Size.Width-400, this.Size.Height);
        }
        private void HiltImportMDIForm_Resize(object sender, EventArgs e)
        {
            progressBar.Size = this.ClientSize;
        }
    }
}
