﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Collections;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace HILT_IMPORTER {
    public partial class MSDSImportForm : HILT_IMPORTER.BaseImportForm {
        DateTime m_lastUpdate;
        string m_smcl;
        string smclPath;
        string m_msds;
        bool processAll = false;
        HMIRSDatabaseManager hmirsMgr = new HMIRSDatabaseManager();      
        
        public enum DOCUMENT_TYPES {
            HTML,
            PDF,
            RTF,
            TXT,
            NONE
        };

        public string cdPath
        {
            get{ return this.fileTextBox.Text; }
            set{ this.fileTextBox.Text = value; }
        }

        public MSDSImportForm() {
            InitializeComponent();

            // Set the label
            this.Title = "Manufacturer Safety Data Sheets Import";
            this.fileLlabel.Text = "HMIRS Disk:";//"MSDS Folder:";
            Text = "MSDS Import";
            messageRTBox.Text = "";
        }

        private void MSDSImportForm_Shown(object sender, EventArgs e) {
            importButton.Focus();
            Application.DoEvents();
            // Set the rich text box tab size 
            TabSizeTwips = 360;
            this.importButton.Enabled = false;
            cdPath = Configuration.CDROM_DataFolder;

            if (Properties.Settings.Default.isFirstRun)
            {
                messageRTBox.AppendText("\n\n\n\n\n\t\t\t\t\tWELCOME TO THE SHIMS MSDS IMPORTER");
                messageRTBox.AppendText("\n\n\t\t\t\t\t   SINCE THIS IS YOU FIRST RUN");
                messageRTBox.AppendText("\n\t\t\t\t   CLICK THE \"Setup\" BUTTON IN THE BOTTOM LEFT");
            }
            else if(Properties.Settings.Default.smclOption == 0 && smclTextBox.Text == ""){
                messageRTBox.AppendText("\n                                   ************************************");
                messageRTBox.AppendText("\n                                   * PLEASE SELECT A SMCL SPREADSHEET *");
                messageRTBox.AppendText("\n                                   ************************************");
            }
            else
            {
                hmirsMgr.openConnection();
                hmirsMgr.initializeConnection(); 
                previewData(false);
            } 
                     
        }

        long m_ProgressBarMaxValue = 100;
        public long ProgressBarMaxValue {
            get {
                return m_ProgressBarMaxValue;
            }
            set {
                m_ProgressBarMaxValue = value;
            }
        }

        protected override void processCancelButton_Click(object sender, EventArgs e) {
            if (m_processing) {
                // Make sure the user really wants to cancel
                DialogResult response = MessageBox.Show(this,
                    "This will cancel the SHIMS database MSDS update"
                    + "\nAre you sure you want to cancel the update?",
                    "MSDS Update Cancellation Pending", MessageBoxButtons.YesNo);

                if (response == DialogResult.Yes)
                    // Cancel the background task
                    msdsBackgroundWorker.CancelAsync();
                    
                    //importButton.Enabled = true;

                    while (msdsBackgroundWorker.IsBusy)
                    {
                        Application.DoEvents();
                    }
            }
            else {
                // Clear the data and reset the status message 
                clearData();

                messageRTBox.Text = "";
                m_parentMDI.StatusBarLabel_Value = "Ready";
                m_parentMDI.progressBar.Value = 0;
                setCursors(Cursors.Default);
            }
        }

        protected override void processFileButton_Click(object sender, EventArgs e) {
            // Display the directory dialog to select the directory containing the MSDS CD image
            FolderBrowserDialog browseDialog = new FolderBrowserDialog();

            if (browseDialog.ShowDialog() == DialogResult.OK) {
                //this.fileTextBox.Text = browseDialog.SelectedPath;
                cdPath = browseDialog.SelectedPath;
                hmirsMgr.disposeConnection();

                //Properties.Settings.Default.CDFolderName = this.fileTextBox.Text;
                Properties.Settings.Default.CDFolderName = cdPath;
                Properties.Settings.Default.Save();
                if (Properties.Settings.Default.smclOption == 0 && smclTextBox.Text == "")
                {
                    messageRTBox.Clear();
                    messageRTBox.AppendText("\n                                   ************************************");
                    messageRTBox.AppendText("\n                                   * PLEASE SELECT A SMCL SPREADSHEET *");
                    messageRTBox.AppendText("\n                                   ************************************");
                }
                else
                {
                    // hmirsMgr = new HMIRSDatabaseManager(this.fileTextBox.Text);
                    hmirsMgr = new HMIRSDatabaseManager(cdPath);
                    hmirsMgr.openConnection();
                    hmirsMgr.initializeConnection();

                    if (processAllCheckBox.Checked)
                    { previewData(true); }
                    else
                    { previewData(false); }
                }
            }
        }

        protected override void processImportButton_Click(object sender, EventArgs e) {
            m_parentMDI.StatusBarCount_Visible = true;
            m_parentMDI.ProgressBar_Visible = true;
            //setCursors(Cursors.WaitCursor);
            setCursors(Cursors.Default);

            DateTime dtStart = DateTime.Now;
            long nStart = Environment.TickCount;

            // Execute import as a background thread
            // Clear things out
            m_parentMDI.StatusBarLabel_Value = "Processing Data...";
            m_parentMDI.StatusBarProgress_Value = 0;
            resetMessageBox();
            importButton.Enabled = false;

            m_processing = true;
            msdsBackgroundWorker.RunWorkerAsync(processAllCheckBox.Checked);

            while (msdsBackgroundWorker.IsBusy) {
                Application.DoEvents();
            }
        }

        

        public void previewData(bool processAll) {
            int percentComplete = 0;
            m_lastUpdate = DateTime.MinValue;

            m_parentMDI.StatusBarCount_Visible = true;
            m_parentMDI.ProgressBar_Visible = true;

            this.importButton.Enabled = false;
            setCursors(Cursors.WaitCursor);

            try {
                long nStart = Environment.TickCount;
                m_parentMDI.StatusBarLabel_Value = "Analyzing Data...";
                resetMessageBox();
                Application.DoEvents();

                StringBuilder sb = new StringBuilder();
                DatabaseManager manager = new DatabaseManager();

                // Generate the preview info
                sb.AppendLine("\n\t***** SHIMS Database *****");

                // SQL Server MSDS Statistics
                int count = manager.getTableCount(Properties.Settings.Default.Database+".dbo.msds");
                sb.AppendLine("\t\tMSDS Records: " + formatNumberThousands(count));

                percentComplete += 10;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // MSDS Documents
                int[] fileCounts = getFileCount();
                sb.Clear();
                sb.AppendLine("\tDocuments:");
                sb.AppendLine("\t\tMSDS: " + formatNumberThousands(fileCounts[0]));
                sb.AppendLine("\t\tProd Sheets: " + formatNumberThousands(fileCounts[1]));
                sb.AppendLine("\t\tMfg Labels : " + formatNumberThousands(fileCounts[2]));
                sb.AppendLine("\t\tTrans Cert : " + formatNumberThousands(fileCounts[3]));
                //sb.AppendLine("\tMSDS, Trans: " + fileCounts[4].ToString());
                sb.AppendLine("\t\tOther Docs : " + formatNumberThousands(fileCounts[5]));

                percentComplete += 10;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // Last MSDS load date
                sb.Clear();
                if (processAll) {
                    sb.AppendLine("\n\tReprocess all HMIRS Data");
                    m_lastUpdate = DateTime.MinValue;
                    processAll = true;
                }
                else {
                    m_lastUpdate = Properties.Settings.Default.LastHmirsUpdate;//manager.getLastHMIRSUpdate();
                    processAll = false;


                    if (m_lastUpdate == DateTime.Parse("01/01/1950"))
                    {
                        sb.AppendLine("\n\tLast HMIRS Update: Never");
                    }
                    else
                    {
                        sb.AppendLine("\n\tLast HMIRS Update: "
                         + String.Format("{0:MM/dd/yyyy}", m_lastUpdate));
                    }
                }
                percentComplete += 5;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                List<string> curSmcl = new List<string>();

                if (File.Exists(smclPath)
                    &&(Properties.Settings.Default.smclOption == 0
                    || Properties.Settings.Default.smclOption >= 2))
                { curSmcl = CreateSMCLList(smclPath); }
                else { curSmcl = manager.getSMCL(processAll); }

                m_smcl = createSmclWhereStatment(curSmcl);

                if (!processAll)
                {
                    List<string> curMSDS = manager.getMSDS();
                    m_msds = createUpdateWhereStatment(curMSDS);
                }
                else { m_msds = " "; }
                
                // HMIRS Statistics
                //HMIRSDatabaseManager hmirsMgr = new HMIRSDatabaseManager();
                sb.Clear();
                sb.AppendLine("\n\t***** HMIRS Database *****");
                // Get the number of new records
                count = hmirsMgr.getNewRevisionCount(m_lastUpdate, m_smcl);
                if (m_msds != " "&& m_msds != "")
                {
                    count += hmirsMgr.getNewRevisionCount(m_lastUpdate, m_msds);
                }
                sb.AppendLine("\t\tNew/Updated MSDS Records: " + formatNumberThousands(count));
                percentComplete += 20;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // MSDS Documents
                fileCounts = hmirsMgr.getRevisionDocTypeCounts(m_lastUpdate,m_smcl);
                sb.Clear();
                sb.AppendLine("\tDocuments:");
                sb.AppendLine("\t\tMSDS: " + formatNumberThousands(fileCounts[0]));
                sb.AppendLine("\t\tProd Sheets: " + formatNumberThousands(fileCounts[1]));
                sb.AppendLine("\t\tMfg Labels : " + formatNumberThousands(fileCounts[2]));
                sb.AppendLine("\t\tTrans Cert : " + formatNumberThousands(fileCounts[3]));
                //sb.AppendLine("\tMSDS, Trans: " + fileCounts[4].ToString());
                sb.AppendLine("\t\tOther Docs : " + formatNumberThousands(fileCounts[5]));
                percentComplete += 20;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                sb.Clear();
                // Support tables
                /*fileCounts = hmirsMgr.getSupportDataRevisionCounts(m_lastUpdate);
                sb.Clear();
                sb.AppendLine("Support Tables:");
                sb.AppendLine("\tContractors: " + formatNumberThousands(fileCounts[0]));
                sb.AppendLine("\tTrans Sheets: "
                    + formatNumberThousands(fileCounts[1] + fileCounts[2] + fileCounts[3] + fileCounts[4]));*/
                percentComplete = 100;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                messageRTBox.Text += "\n\tEstimated Processing time: "
                    + ((int)(count * .005) + 15).ToString() + " (minutes)";

                long nEnd = Environment.TickCount;
                //messageRTBox.Text += "\nPreview time: "
                //    + (((double)(nEnd - nStart)) / 1000).ToString("#.0") + " (seconds)";

                if (this.importButton.Enabled == false)
                {
                    this.importButton.Enabled = true;
                }
            }
            catch (Exception ex) {
                //messageRTBox.Text = "ERROR previewing MSDS data - check the configuration settings in Tools > Setup" + ex.ToString();
                messageRTBox.Text += "\r\n                                   *************************************\r\n";
                messageRTBox.Text +="                                   * !!!ERROR PROCESSING HMIRS DATA!!! *\r\n";
                messageRTBox.Text +="                                   *************************************\r\n\r\n";
                messageRTBox.Text +="                   Please make sure the disk is in your computer and the path above is correct\r\n";
                this.importButton.Enabled = false;
            }
            m_parentMDI.StatusBarLabel_Value = "Ready";
            m_parentMDI.progressBar.Value = 0;
            setCursors(Cursors.Default);
            Application.DoEvents();
        }

        private void writeFileTypeFound(String fileType, long rowNum, String source) {
            Debug.WriteLine("Found:\t" + fileType + "\t" + rowNum +"\t" + source);
        }

        private void clearData() {
        }

        //**********************************************************************
        // BackgroundWorker Import Stuff
        private void msdsBackgroundWorker_DoWork(object sender, DoWorkEventArgs e) {
            // Run the import           
            try {
                HMIRSImporter importer = new HMIRSImporter(this, msdsBackgroundWorker);

                if (processAll)
                {
                    DirectoryInfo msdsDir = new DirectoryInfo(Properties.Settings.Default.MSDSFolderName);
                    foreach (FileInfo msds in msdsDir.GetFiles())
                    {
                        msds.Delete();
                    }

                    DatabaseManager dlMgr = new DatabaseManager();
                    dlMgr.deleteMsds();
                }
                // Update the database
                bool result = importer.import(m_lastUpdate, m_smcl, m_msds);
            }

            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void msdsBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Update the value of the ProgressBar and the count label to the BackgroundWorker progress.
            m_parentMDI.StatusBarCount_Value = e.ProgressPercentage;

            // change the count to a percentage for the progress bar
            double percent = (e.ProgressPercentage * 100.0) / m_ProgressBarMaxValue;
            m_parentMDI.StatusBarProgress_Value = (int)percent;

            if (e.UserState != null) {
                String msg = e.UserState.ToString();
                // See whether we write this to the message text box, or the status bar
                if (msg.StartsWith("SB_")) {
                    m_parentMDI.StatusBarLabel_Value = msg.Substring(3);
                }
                else {
                    messageRTBox.AppendText(msg);
                }
            }

            Application.DoEvents();
        }

        private void msdsBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // Clean things up
            m_processing = false;
            m_parentMDI.StatusBarLabel_Value = "Ready";
            m_parentMDI.progressBar.Value = 0;
            setCursors(Cursors.Default);
            this.importButton.Enabled = true;
        }

        private int[] getFileCount() {
            // Initialize the document count array
            // Order of types is: MSDS, Mfg Label, 
            int[] result = {0, 0, 0, 0, 0, 0};

            if(!Directory.Exists(Configuration.MSDS_DocumentFolder)){
                Directory.CreateDirectory(Configuration.MSDS_DocumentFolder);
            }

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(
                Configuration.MSDS_DocumentFolder);
            // Loop through the files to count by type
            foreach (FileInfo file in dir.GetFiles()) {
                if (file.Name.Contains("_MSDS_"))
                    result[0]++;
                else if (file.Name.Contains("_PROD SHEET_"))
                    result[1]++;
                else if (file.Name.Contains("_MFR. LABEL_"))
                    result[2]++;
                else if (file.Name.Contains("_TRANS CERT_"))
                    result[3]++;
                else if (file.Name.Contains("_MSDS, TRAN_"))
                    result[4]++;
                else if (file.Name.Contains("_OTHER DOCS_"))
                    result[5]++;
            }

            

            return result;
        }

        private void processAllCheckBox_CheckedChanged(object sender, EventArgs e) {
            if (processAllCheckBox.Checked) {
                // Rerun the preview to show them what they are in for
                previewData(true);
                processAll = true;
            }
            else {
                previewData(false);
                processAll = false;
            }
        }

        private void MSDSImportForm_Load(object sender, EventArgs e)
        {

        }

        private void SetupButton_Click(object sender, EventArgs e)
        {

            SetupForm setup = new SetupForm();
            if (setup.ShowDialog(this) == System.Windows.Forms.DialogResult.Yes)
            {
                setCursors(Cursors.WaitCursor);
                 //this.fileTextBox.Text = browseDialog.SelectedPath;
                cdPath = Configuration.CDROM_DataFolder;
                this.fileTextBox.Text = cdPath;
                this.smclTextBox.Clear();
                hmirsMgr.disposeConnection();

                //Properties.Settings.Default.CDFolderName = this.fileTextBox.Text;
                if (Properties.Settings.Default.smclOption == 0 && smclTextBox.Text == "")
                {
                    messageRTBox.Clear();
                    messageRTBox.AppendText("\n                                   ************************************");
                    messageRTBox.AppendText("\n                                   * PLEASE SELECT A SMCL SPREADSHEET *");
                    messageRTBox.AppendText("\n                                   ************************************");
                }
                else if(Properties.Settings.Default.isFirstRun)
                {
                    messageRTBox.AppendText("\n\n\n\n\n\t\t\t\t\tWELCOME TO THE SHIMS MSDS IMPORTER");
                    messageRTBox.AppendText("\n\n\t\t\t\t\t   SINCE THIS IS YOU FIRST RUN");
                    messageRTBox.AppendText("\n\t\t\t\t   CLICK THE \"Setup\" BUTTON IN THE BOTTOM LEFT");
                }
                else
                {
                    // hmirsMgr = new HMIRSDatabaseManager(this.fileTextBox.Text);
                    hmirsMgr = new HMIRSDatabaseManager(cdPath);
                    hmirsMgr.openConnection();
                    hmirsMgr.initializeConnection();

                    if (processAllCheckBox.Checked)
                    { previewData(true); }
                    else
                    { previewData(false); }
                }
            }
            setCursors(Cursors.Default);
        }


        private void smclButton_Click(object sender, EventArgs e)
        {
            // Display the directory dialog to select the directory containing the MSDS CD image
            //FolderBrowserDialog browseDialog = new FolderBrowserDialog();
            OpenFileDialog browseDialog = new OpenFileDialog();
            browseDialog.Filter = "Excel 98(*.xls)|*.xls| Excel 2000(*.xlsx)|*.xlsx|All files (*.*)|*.*";

            if (browseDialog.ShowDialog() == DialogResult.OK)
            {
                this.smclTextBox.Text = browseDialog.FileName;
                hmirsMgr.disposeConnection();
                smclPath = this.smclTextBox.Text;

               // hmirsMgr = new HMIRSDatabaseManager(this.fileTextBox.Text);

                hmirsMgr = new HMIRSDatabaseManager(cdPath);

                hmirsMgr.openConnection();
                hmirsMgr.initializeConnection();

                if (processAllCheckBox.Checked)
                {
                    // Rerun the preview to show them what they are in for
                    previewData(true);
                }
                else
                {
                    previewData(false);
                }

            }
        }

        private string createSmclWhereStatment(List<String> smclItems)
        {
            string whereStatment = "";
            if(smclItems.Count >0){

                whereStatment = "AND( ";
                int count = 0;
                foreach (string item in smclItems)
                {
                    if (count > 0) { whereStatment += " OR "; }
                    string[] curItem = item.Split('-');

                    whereStatment += "( fsc= " + curItem[0] + " AND iin= '" + curItem[1] + "' )";

                    count += 1;
                }
                whereStatment += " )";
            }
            return whereStatment;         
        }

        private string createUpdateWhereStatment(List<String> updateItems)
        {
            string whereStatment = "";
            if (updateItems.Count > 0)
            {

                whereStatment = "AND( ";
                int count = 0;
                foreach (string item in updateItems)
                {
                    if (count > 0) { whereStatment += " OR "; }
                    string[] curItem = item.Split('-');

                    whereStatment += "( fsc= " + curItem[0] + " AND iin= '" + curItem[1] + "' )";

                    count += 1;
                }
                whereStatment += " )";
            }
            return whereStatment;
        }

        public List<string> CreateSMCLList(string excelPath)
        {
            List<string[]> list = new SMCLExcelParser().parseExcel(excelPath);
            List<string> smclList = new List<string>();

            int count = 0;
            int fscIndex = 0;
            int niinIndex = 0;

            foreach (string[] smclItem in list)
            {
                if (count == 0)
                {
                    int headerIndex = 0;
                    foreach (string header in smclItem)
                    {
                        if (header == "FSC") { fscIndex = headerIndex; }
                        else if (header == "NIIN") { niinIndex = headerIndex; }
                        headerIndex += 1;
                    }
                }
                else
                {
                    string fscItem = smclItem[fscIndex];
                    if (fscItem == "" || fscItem == " ")
                    { fscItem = "0"; }

                    if (smclItem[niinIndex] != "" && smclItem[niinIndex] != " ")
                    { smclList.Add(fscItem + "-" + smclItem[niinIndex]); }
                }
                count += 1;
            }
            return smclList;
        }
    }
}
