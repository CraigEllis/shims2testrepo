﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILT_IMPORTER
{
    /* Extend the Exception class.
     * This allows us to write our own custom exceptions
     * for the user to view.
     */
    public class ImportException : Exception
    {
        public ImportException()
            : base()
        {   }

        public ImportException(string msg)
            : base(msg)
        {   }
    }
}
