using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Common.Logging;
using HILT_IMPORTER.DataObjects;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace HILT_IMPORTER {
    public class DatabaseManager {
        public enum FILE_CHANGES {
            Initial_Load,
            Threshold_Exceeded,
            Acceptable_Changes
        };

        public enum FILE_TABLES {
            FileUpload,
            FileDownLoad
        };

        private static ILog logger = LogManager.GetCurrentClassLogger();

        private static string MDF_CONNECTION = null;
        public static String DB_BACKUP_DIRECTORY = null;

    #region Constructors
        public DatabaseManager() {
            if (MDF_CONNECTION == null) {
                MDF_CONNECTION = Properties.Settings.Default.ConnectionInfo;
            }

            if (DB_BACKUP_DIRECTORY == null) {
                DB_BACKUP_DIRECTORY = Properties.Settings.Default.HILT_HUB_DataFolder + "\\Backups";

                // Make sure the folder exists
                if (!Directory.Exists(DB_BACKUP_DIRECTORY)) {
                    Directory.CreateDirectory(DB_BACKUP_DIRECTORY);
                }
            }
        }

        public DatabaseManager(string connectionString) {
            if (MDF_CONNECTION == null) {
                MDF_CONNECTION = connectionString;
            }
        }
    #endregion

    #region User Management
        //**********************************************************************
        public static bool isAdmin(string username) {
            return isUserInRole(username, "ADMIN");
        }

        public static bool isSupplyOfficer(string username) {
            return isUserInRole(username, "SUPPLY_OFFICER");
        }

        public static bool isSupplyUser(string username) {
            return isUserInRole(username, "SUPPLY_USER");
        }

        public static bool isReports(string username) {
            return isUserInRole(username, "REPORTS");
        }

        public static bool isUserInRole(string username, string role) {
            bool isUserInRole = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "SELECT COUNT(*) FROM user_roles WHERE username=@username AND role=@role";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@username", dbNull(username));
            cmd.Parameters.AddWithValue("@role", dbNull(role));

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                isUserInRole = true;

            return isUserInRole;

        }
    #endregion

    #region Hull_Types
        //**********************************************************************
        // Hull_Types
        public bool checkHullTypeExists(int hull_type_id) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM hull_types WHERE hull_type_id = @hull_type_id";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt.ToString(), conn);
            cmd.Parameters.AddWithValue("@hull_type_id", hull_type_id);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public bool checkHullTypeExists(String hull_type) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM hull_types WHERE hull_type = @hull_type";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt.ToString(), conn);
            cmd.Parameters.AddWithValue("@hull_type", hull_type);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public long insertHull_Type(HullType hullType, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "INSERT INTO hull_Types (" +
                "hull_type, hull_description, active, parent_type_id " +
                "VALUES(" +
                "@hull_type, @hull_description, @active, @parent_type_id); " +
                "SELECT SCOPE_IDENTITY()";

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@hull_type", hullType.hull_type);
                insertCmd.Parameters.AddWithValue("@hull_description", dbNull(hullType.hull_description));
                insertCmd.Parameters.AddWithValue("@active", hullType.active);
                insertCmd.Parameters.AddWithValue("@parent_type_id", hullType.parent_type_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                insertCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Inserted HullType: {0}, {1} ID = {2}",
                    hullType.hull_type, hullType.hull_description, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error insertingHullType for: {0}, {1} - {2}", 
                    hullType.hull_type, hullType.hull_description, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateHull_Type(HullType hullType, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE hull_Types SET " +
                "hull_type = @hull_type, hull_description = @hull_description, " +
                "active = @active, parent_type_id = @parent_type_id " +
                "WHERE hull_type_id = @hull_type_id";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@hull_type_id", hullType.hull_type_id);
                updateCmd.Parameters.AddWithValue("@hull_type", hullType.hull_type);
                updateCmd.Parameters.AddWithValue("@hull_description", dbNull(hullType.hull_description));
                updateCmd.Parameters.AddWithValue("@active", hullType.active);
                updateCmd.Parameters.AddWithValue("@parent_type_id", hullType.parent_type_id);

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Updated HullType: ID = {0} to {1}, {2}, {3}, {4}",
                    hullType.hull_type_id, hullType.hull_type, hullType.hull_description,
                    hullType.parent_type_id, hullType.active);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updatingHullType for ID = {0}, {1} - {2}",
                    hullType.hull_type_id, hullType.hull_type, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long deleteHull_Type(HullType hullType, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM hull_Types " +
                "WHERE hull_type_id = @hull_type_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@hull_type_id", hullType.hull_type_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted HullType: ID = {0}, {1}",
                    hullType.hull_type_id, hullType.hull_type);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingHullType for ID = {0}, {1} - {2}",
                    hullType.hull_type_id, hullType.hull_type, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region Ship Status
        // Ship Statuses
        public bool checkShipExists(int ship_id) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM ships WHERE ship_id = @ship_id";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt.ToString(), conn);
            cmd.Parameters.AddWithValue("@ship_id", ship_id);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public bool checkShipExists(String ship_name) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM ships WHERE name = @ship_name";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt.ToString(), conn);
            cmd.Parameters.AddWithValue("@ship_name", ship_name);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public long insertShipStatus(String description, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "INSERT INTO ship_statuses (" +
                "description " +
                "VALUES(@description); " +
                "SELECT SCOPE_IDENTITY()";

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@description", dbNull(description));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                insertCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Inserted ShipStatus: {0} ID = {1}",
                    description, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error insertingShipStatus for: {0} - {1}",
                    description, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShipStatus(int status_id, String description, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ship_statuses SET " +
                "description = @description " +
                "WHERE status_id = @status_id";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@status_id", status_id);
                updateCmd.Parameters.AddWithValue("@description", dbNull(description));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Updated Ship Status: ID = {0} to {1}",
                    status_id, description);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updatingShipStatus ID = {0}, {1} - {2}",
                    status_id, description, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long deleteShipStatus(int status_id, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM ship_statuses " +
                "WHERE status_id = @status_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@status_id", status_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted ShipStatus: ID = {0}",
                    status_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingShipStatus ID = {0} - {1}",
                    status_id, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region Ships
        // Ships
        public Ship getShipRecord(int ship_id) {
            Ship record = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string sqlStmt = "SELECT * FROM v_Ships WHERE ship_id = @ship_id";

            try {
                SqlCommand cmd = new SqlCommand(sqlStmt, conn);

                //insert rows
                cmd.Parameters.AddWithValue("@ship_id", ship_id);

                SqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read()) {
                    record = new Ship();
                    int temp_id = -1;
                    DateTime tempDate;

                    record.ship_id = ship_id;
                    System.Diagnostics.Debug.WriteLine(rdr["current_status"].ToString());
                    int.TryParse(rdr["current_status"].ToString(), out temp_id);
                    record.current_status = temp_id;
                    record.ship_status = rdr["ship_status"].ToString();
                   record.uic = rdr["uic"].ToString();
                    record.hull_number = rdr["Hull_Number"].ToString();
                    record.name = rdr["Ship_Name"].ToString();
                    int.TryParse(rdr["hull_type_id"].ToString(), out temp_id);
                    record.hull_type_id = temp_id;
                    record.hull_type = rdr["Hull_Type"].ToString();
                    record.hull_description = rdr["Hull_Description"].ToString();
                    record.hull_active = Convert.ToBoolean(rdr["Hull_Active"]).ToString();
                    int.TryParse(rdr["parent_type_id"].ToString(), out temp_id);
                    record.parent_type_id = temp_id;
                    record.parent_hull_type = rdr["Parent_Hull_Type"].ToString();
                    DateTime.TryParse(rdr["AUL_Download"].ToString(), out tempDate);
                    record.download_aul_date = tempDate;
                    DateTime.TryParse(rdr["MSDS_Download"].ToString(), out tempDate);
                    record.download_msds_date = tempDate;
                    DateTime.TryParse(rdr["MSSL_Download"].ToString(), out tempDate);
                    record.download_mssl_date = tempDate;
                    DateTime.TryParse(rdr["AUL_Upload"].ToString(), out tempDate);
                    record.upload_aul_date = tempDate;
                    DateTime.TryParse(rdr["MSDS_Upload"].ToString(), out tempDate);
                    record.upload_msds_date = tempDate;
                    DateTime.TryParse(rdr["MSSL_Upload"].ToString(), out tempDate);
                    record.upload_mssl_date = tempDate;
                    DateTime.TryParse(rdr["Created"].ToString(), out tempDate);
                    record.created = tempDate;
                    if (rdr["Created_By"] != DBNull.Value)
                        record.created_by = rdr["Created_By"].ToString();
                    DateTime.TryParse(rdr["Changed"].ToString(), out tempDate);
                    record.changed = tempDate;
                    if (rdr["Changed_By"] != DBNull.Value)
                        record.changed_by = rdr["Changed_by"].ToString();

                    rdr.Close();
                    cmd.Dispose();
                }
            }
            finally {
                conn.Close();
            }

            return record;
        }

        public long insertShip(Ship ship, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "INSERT INTO ships (" +
                "current_status, name, uic, hull_type_id, hull_number, " +
                "created, created_by) VALUES(" +
                "@current_status, @name, @uic, @hull_type_id, @hull_number, @created, @created_by" +
                ");  SELECT SCOPE_IDENTITY()";

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@current_status", ship.current_status);
                insertCmd.Parameters.AddWithValue("@name", dbNull(ship.name));
                insertCmd.Parameters.AddWithValue("@uic", ship.uic);
                insertCmd.Parameters.AddWithValue("@hull_type_id", ship.hull_type_id);
                insertCmd.Parameters.AddWithValue("@hull_number", ship.hull_number);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", dbNull(userName));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                insertCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Inserted ShipRecord: {0}, {1}, {2}, {3} ID = {4}",
                    ship.hull_number, ship.name, ship.uic, ship.hull_type_id, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error insertingShipRecord: {0}, {1}, {2}, {3} - {4}",
                    ship.hull_number, ship.name, ship.uic, ship.hull_type_id, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShip(Ship ship, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ships SET " +
                "current_status = @current_status, name = @name, uic = @uic, " +
                "hull_type_id = @hull_type_id, hull_number = @hull_number, " +
                "changed = @changed, changed_by = @changed_by " +
                "WHERE ship_id = @ship_id";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@ship_id", ship.ship_id);
                updateCmd.Parameters.AddWithValue("@current_status", ship.current_status);
                updateCmd.Parameters.AddWithValue("@name", dbNull(ship.name));
                updateCmd.Parameters.AddWithValue("@uic", ship.uic);
                updateCmd.Parameters.AddWithValue("@hull_type_id", ship.hull_type_id);
                updateCmd.Parameters.AddWithValue("@hull_number", ship.hull_number);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", dbNull(userName));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Updated ShipRecord ID = {0}, {1}, {2}, {3}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updatingShipRecord ID = {0}, {1}, {2}, {3} - {4}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long deleteShip(Ship ship, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM ships " +
                "WHERE ship_id = @ship_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@ship_id", ship.ship_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted ShipRecord ID = {0}, {1}, {2}, {3}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingShipRecord ID = {0}, {1}, {2}, {3} - {4}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShipFileUpload(String uic, Configuration.UPLOAD_TYPES uploadType, 
            DateTime uploadDate, String userName) {
            long result = 0;

            // Set the upload date field based on the uploadType
            String fieldName = null;
            switch (uploadType) {
                case Configuration.UPLOAD_TYPES.AUL:
                    fieldName = "upload_aul_date";
                    break;
                case Configuration.UPLOAD_TYPES.MSDS:
                    fieldName = "upload_msds_date";
                    break;
                case Configuration.UPLOAD_TYPES.MSSL:
                    fieldName = "upload_mssl_date";
                    break;
                default:
                    break;
            }

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ships SET " +
                fieldName + " = @uploadDate, " +
                "changed = @changed, changed_by = @changed_by " +
                "WHERE uic = @uic";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@uic", uic);
                updateCmd.Parameters.AddWithValue("@uploadDate", uploadDate);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", dbNull(userName));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShipFileDownload(String uic, Configuration.DOWNLOAD_TYPES downloadType,
            DateTime downloadDate, String userName) {
            long result = 0;

            // Set the upload date field based on the uploadType
            String fieldName = null;
            switch (downloadType) {
                case Configuration.DOWNLOAD_TYPES.AUL:
                    fieldName = "download_aul_date";
                    break;
                case Configuration.DOWNLOAD_TYPES.MSDS:
                    fieldName = "download_msds_date";
                    break;
                case Configuration.DOWNLOAD_TYPES.MSSL:
                    fieldName = "download_mssl_date";
                    break;
                default:
                    break;
            }

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ships SET " +
                fieldName + " = @downloadDate, " +
                "changed = @changed, changed_by = @changed_by " +
                "WHERE uic = @uic";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@uic", uic);
                updateCmd.Parameters.AddWithValue("@downloadDate", downloadDate);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", dbNull(userName));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region MSSL
        public Hashtable getMSSLQuantities(String ship_UIC) {
            Hashtable ht = new Hashtable();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            String stmt = "SELECT niin, location, qty FROM mssl_inventory WHERE uic = @uic";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@uic", ship_UIC);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read()) {
                int qty = 0;
                int.TryParse(rdr["qty"].ToString(), out qty);
                ht.Add(rdr["niin"] + "_" + rdr["location"], qty);
            }

            cmd.Dispose();
            conn.Close();

            return ht;
        }

        public Hashtable getMSSLMasterNiins() {
            Hashtable ht = new Hashtable();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            String stmt = "SELECT niin FROM mssl_master";

            SqlCommand cmd = new SqlCommand(stmt, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read()) {
                int qty = 0;
                ht.Add(rdr["niin"], qty);
            }

            cmd.Dispose();
            conn.Close();

            return ht;
        }

        public FILE_CHANGES validateMSSLChanges(MSSLData data, String shipUIC) {
            FILE_CHANGES result = FILE_CHANGES.Acceptable_Changes;
            int changes = 0;
            int foundRecords = 0;

            // Check for existing inventory
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            try {
                conn.Open();

                String stmt = "SELECT COUNT(*) FROM mssl_inventory WHERE uic = @uic";

                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Parameters.AddWithValue("@uic", shipUIC);

                int invCount = (Int32) cmd.ExecuteScalar();
                int threshold = invCount / 4;

                if (invCount == 0) {
                    // New unit
                    data.MsslStats.NewRecords = data.InventoryList.Count;
                    result = FILE_CHANGES.Initial_Load;
                }
                else {
                    // Count changes from existing inventory
                    Hashtable htExisting = getMSSLQuantities(shipUIC);

                    // Go through the new data counting changes
                    foreach (MsslInventory record in data.InventoryList) {
                        String key = record.Niin + "_" + record.Location;
                        // See if the record exists
                        if (htExisting.ContainsKey(key)) {
                            foundRecords++;
                            // Exists - check qty
                            if ((int) htExisting[key] != record.Qty) {
                                changes++;
                                data.MsslStats.ChangedRecords++;
                            }
                        }
                        else {
                            // New niin
                            changes++;
                            data.MsslStats.NewRecords++;
                        }
                    }

                    changes += (invCount - foundRecords);

                    // check the threshold
                    if (changes > threshold) {
                        result = FILE_CHANGES.Threshold_Exceeded;
                    }
                }
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public bool processMSSLInventory(String uic, MSSLData data, String username) {
            bool result = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            try {
                conn.Open();

                // Get the existing MSSL quantities and mssl_master niins
                Hashtable htExisting = getMSSLQuantities(uic);
                Hashtable htMaster = getMSSLMasterNiins();

                // Go through the new  inventory and update, or insert
                foreach (MsslInventory record in data.InventoryList) {
                    String key = record.Niin + "_" + record.Location;
                    // See if the record exists
                    if (htExisting.ContainsKey(key)) {                        
                        // Exists - update the qty
                        updateMSSLInventory(uic, record, conn, username);
                        htExisting[key] = 1;
                        data.MsslStats.ChangedRecords++;
                    }
                    else {
                        // New niin-location - insert the record
                        insertMSSLInventory(uic, record, conn, username);
                    }

                    // Now see if we need to add to the mssl_master;
                    if (!htMaster.ContainsKey(record.Niin)) {
                        insertMSSLMaster(uic, record, conn, username);
                        // Add the niin to the hashtable so we don't try to add duplicates
                        htMaster.Add(record.Niin, 0);
                        data.MsslStats.NewRecords++;
                    }
                }

                // Delete existing records we haven't updated
                char[] delimiter = {'_'};
                foreach (String key in htExisting.Keys) {
                    if ((Int32) htExisting[key] == 0) {
                        String[] niin_location = key.Split(delimiter);
                        this.deleteMSSLInventory(uic, niin_location[0], niin_location[1],
                            conn, username);
                        data.MsslStats.DeletedRecords++;
                    }
                }

                result = true;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public bool updateMSSLInventory(String uic, MsslInventory record, SqlConnection conn,
            String username) {
            bool result = false;
            String stmt = "UPDATE mssl_inventory SET Qty = @Qty, changed = @changed, " +
                "changed_by = @changed_by WHERE uic = @uic AND niin = @niin AND location = @location";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@Qty", record.Qty);
                cmd.Parameters.AddWithValue("@changed", DateTime.Now);
                cmd.Parameters.AddWithValue("@changed_by", username);
                cmd.Parameters.AddWithValue("@uic", uic);
                cmd.Parameters.AddWithValue("@niin", record.Niin);
                cmd.Parameters.AddWithValue("@location", record.Location);

                cmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSSLInventory Updated: {0}, {1}, {2}, Qty = {3}",
                    uic, record.Niin, record.Location, record.Qty);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating MSSLInventory: {0}, {1}, {2} - {3}",
                    uic, record.Niin, record.Location, ex.Message);
                throw ex;
            }

            return result;
        }

        public long insertMSSLInventory(String uic, MsslInventory record, SqlConnection conn,
            String username) {
            long result = 0;
            String stmt = "INSERT INTO mssl_inventory (uic, niin, location, qty, created, created_by) " +
                "VALUES(@uic, @niin, @location, @Qty, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@Qty", record.Qty);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);
                insertCmd.Parameters.AddWithValue("@uic", uic);
                insertCmd.Parameters.AddWithValue("@niin", record.Niin);
                insertCmd.Parameters.AddWithValue("@location", record.Location);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("MSSLInventory Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.Niin, record.Location, record.Qty, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting MSSLInventory: {0}, {1}, {2} - {3}",
                    uic, record.Niin, record.Location, ex.Message);
                throw ex;
            }

            return result;
        }

        public bool deleteMSSLInventory(String uic, String niin, String location, 
            SqlConnection conn, String username) {
            bool result = false;
            String stmt = "DELETE FROM mssl_inventory WHERE " +
                "uic = @uic AND niin = @niin AND location = @location";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@uic", uic);
                cmd.Parameters.AddWithValue("@niin", niin);
                cmd.Parameters.AddWithValue("@location", location);

                cmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSSLInventory Deleted: {0}, {1}, {2}",
                    uic, niin, location);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleting MSSLInventory: {0}, {1}, {2} - {3}",
                    uic, niin, location, ex.Message);
                throw ex;
            }

            return result;
        }

        public long insertMSSLMaster(String uic, MsslInventory record, SqlConnection conn,
            String username) {
            long result = 0;
            String stmt = "INSERT INTO mssl_master (niin, ati, cog, mcc, ui, up, " +
                "netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, " +
                "data_source_cd, created, created_by) " +
                "VALUES(@niin, @ati, @cog, @mcc, @ui, @up, " +
                "@netup, @lmc, @irc, @dues, @ro, @rp, @amd, @smic, @slc, @slac, @smcc, @nomenclature, " +
                "@data_source_cd, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@niin", record.Niin);
                insertCmd.Parameters.AddWithValue("@ati", dbNull(record.Ati));
                insertCmd.Parameters.AddWithValue("@cog", dbNull(record.Cog));
                insertCmd.Parameters.AddWithValue("@mcc", dbNull(record.Mcc));
                insertCmd.Parameters.AddWithValue("@ui", dbNull(record.Ui));
                insertCmd.Parameters.AddWithValue("@up", dbNull(record.Up));
                insertCmd.Parameters.AddWithValue("@netup", dbNull(record.Netup));
                insertCmd.Parameters.AddWithValue("@lmc", dbNull(record.Lmc));
                insertCmd.Parameters.AddWithValue("@irc", dbNull(record.Irc));
                insertCmd.Parameters.AddWithValue("@dues", dbNull(record.Dues));
                insertCmd.Parameters.AddWithValue("@ro", dbNull(record.Ro));
                insertCmd.Parameters.AddWithValue("@rp", dbNull(record.Rp));
                insertCmd.Parameters.AddWithValue("@amd", dbNull(record.Amd));
                insertCmd.Parameters.AddWithValue("@smic", dbNull(record.Smic));
                insertCmd.Parameters.AddWithValue("@slc", dbNull(record.Slc));
                insertCmd.Parameters.AddWithValue("@slac", dbNull(record.Slac));
                insertCmd.Parameters.AddWithValue("@smcc", dbNull(record.Smcc));
                insertCmd.Parameters.AddWithValue("@nomenclature", dbNull(record.Nomenclature));
                insertCmd.Parameters.AddWithValue("@data_source_cd", uic);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("MSSLMaster Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.Niin, record.Nomenclature, record.Smcc, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting MSSLMaster: {0}, {1}, {2} - {3}",
                    uic, record.Niin, record.Location, ex.Message);
                throw ex;
            }

            return result;
        }
    #endregion

    #region Authorized Use List
        #region AUL Gets
        #endregion

        #region Insert
        public long insertAULMasterRecord(String uic, AULRecord record, String username) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            // Check to make sure we have the ID values - just in case
            if (record.hull_type_id == 0) {
                record.hull_type_id = getIDFromValue(
                    "Hull_Types", "hull_type_id", "hull_type", record.Hull_Type, conn);
            }
            if (record.usage_category_id == 0) {
                record.usage_category_id = getIDFromValue(
                    "Usage_Category", "usage_category_id", "category", record.Usage_Category, conn);
            }
            if (record.smcc_id == 0) {
                record.smcc_id = getIDFromValue(
                    "SMCC", "smcc_id", "smcc", record.SMCC, conn);
            }
            if (record.shelf_life_code_id == 0) {
                record.shelf_life_code_id = getIDFromValue(
                    "Shelf_Life_Code", "shelf_life_code_id", "slc", record.Shelf_Life_Code, conn);
            }
            if (record.shelf_life_action_code_id == 0) {
                record.shelf_life_action_code_id = getIDFromValue(
                    "Shelf_Life_Action_Code", "shelf_life_action_code_id", "slac", record.Shelf_Life_Action_Code, conn);
            }
            if (record.storage_type_id == 0) {
                record.storage_type_id = getIDFromValue(
                    "Storage_Type", "storage_type_id", "type", record.Storage_Type, conn);
            }
            if (record.cog_id == 0) {
                record.cog_id = getIDFromValue(
                    "COG_Codes", "cog_id", "cog", record.COG, conn);
            }
            if (record.catalog_group_id == 0) {
                record.catalog_group_id = getIDFromValue(
                    "Catalog_Groups", "catalog_group_id", "group_name", record.Catalog_Group, conn);
            }

            String stmt = "INSERT INTO auth_use_list (hull_type_id, fsc, niin, ui, um, usage_category_id, " +
                "description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, " +
                "remarks, storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, " +
                "catalog_serial_number, allowance_qty, manually_entered, dropped_in_error, " +
                "manufacturer, cage, msds_num, data_source_cd, created, created_by) " +
                "VALUES(@hull_type_id, @fsc, @niin, @ui, @um, @usage_category_id, " +
                "@description, @smcc_id, @specs, @shelf_life_code_id, @shelf_life_action_code_id, " +
                "@remarks, @storage_type_id, @cog_id, @spmig, @nehc_rpt, @catalog_group_id, " +
                "@catalog_serial_number, @allowance_qty, @manually_entered, @dropped_in_error, " +
                "@manufacturer, @cage, @msds_num, @data_source_cd, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@hull_type_id", record.hull_type_id);
                insertCmd.Parameters.AddWithValue("@niin", record.niin);
                insertCmd.Parameters.AddWithValue("@fsc", dbNull(record.fsc));
                insertCmd.Parameters.AddWithValue("@ui", dbNull(record.ui));
                insertCmd.Parameters.AddWithValue("@um", dbNull(record.um));
                insertCmd.Parameters.AddWithValue("@usage_category_id", dbNull(record.usage_category_id));
                insertCmd.Parameters.AddWithValue("@description", dbNull(record.description));
                insertCmd.Parameters.AddWithValue("@smcc_id", dbNull(record.smcc_id));
                insertCmd.Parameters.AddWithValue("@specs", dbNull(record.specs));
                insertCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(record.shelf_life_code_id));
                insertCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(record.shelf_life_action_code_id));
                insertCmd.Parameters.AddWithValue("@remarks", dbNull(record.remarks));
                insertCmd.Parameters.AddWithValue("@storage_type_id", dbNull(record.storage_type_id));
                insertCmd.Parameters.AddWithValue("@cog_id", dbNull(record.cog_id));
                insertCmd.Parameters.AddWithValue("@spmig", dbNull(record.spmig));
                insertCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(record.nehc_rpt));
                insertCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(record.catalog_group_id));
                insertCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(record.catalog_serial_number));
                insertCmd.Parameters.AddWithValue("@allowance_qty", dbNull(record.allowance_qty));
                insertCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.manually_entered));
                insertCmd.Parameters.AddWithValue("@dropped_in_error", dbNull(record.dropped_in_error));
                insertCmd.Parameters.AddWithValue("@manufacturer", dbNull(record.manufacturer));
                insertCmd.Parameters.AddWithValue("@cage", dbNull(record.cage));
                insertCmd.Parameters.AddWithValue("@msds_num", dbNull(record.msds_num));
                insertCmd.Parameters.AddWithValue("@data_source_cd", uic);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("AULMaster Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.niin, record.description, record.hull_type_id, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting AULMaster: {0}, {1}, {2} - {3}",
                    uic, record.niin, record.hull_type_id, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
        #endregion

        #region AUL Update
        public long updateAULMasterRecord(String uic, AULRecord record, SqlConnection conn,
            String username) {
            long result = 0;

            // Check to make sure we have the ID values - just in case
            if (record.hull_type_id == 0) {
                record.hull_type_id = getIDFromValue(
                    "Hull_Types", "hull_type_id", "hull_type", record.Hull_Type, conn);
            }
            if (record.usage_category_id == 0) {
                record.usage_category_id = getIDFromValue(
                    "Usage_Category", "usage_category_id", "category", record.Usage_Category, conn);
            }
            if (record.smcc_id == 0) {
                record.smcc_id = getIDFromValue(
                    "SMCC", "smcc_id", "smcc", record.SMCC, conn);
            }
            if (record.shelf_life_code_id == 0) {
                record.shelf_life_code_id = getIDFromValue(
                    "Shelf_Life_Code", "shelf_life_code_id", "slc", record.Shelf_Life_Code, conn);
            }
            if (record.shelf_life_action_code_id == 0) {
                record.shelf_life_action_code_id = getIDFromValue(
                    "Shelf_Life_Action_Code", "shelf_life_action_code_id", "slac", record.Shelf_Life_Action_Code, conn);
            }
            if (record.storage_type_id == 0) {
                record.storage_type_id = getIDFromValue(
                    "Storage_Type", "storage_type_id", "type", record.Storage_Type, conn);
            }
            if (record.cog_id == 0) {
                record.cog_id = getIDFromValue(
                    "COG_Codes", "cog_id", "cog", record.COG, conn);
            }
            if (record.catalog_group_id == 0) {
                record.catalog_group_id = getIDFromValue(
                    "Catalog_Groups", "catalog_group_id", "group_name", record.Catalog_Group, conn);
            }

            String stmt = "UPDATE auth_use_list (hull_type_id = @hull_type_id, fsc = @fsc, niin = @niin, " +
                "ui = @ui, um = @um, usage_category_id = @usage_category_id, description = @description, " +
                "smcc_id = @smcc_id, specs = @specs, shelf_life_code_id = @shelf_life_code_id, " +
                "shelf_life_action_code_id = @shelf_life_action_code_id, " +
                "remarks = @remarks, storage_type_id = @storage_type_id, cog_id = @cog_id, spmig = @spmig, " +
                "nehc_rpt = @nehc_rpt, catalog_group_id = @catalog_group_id, " +
                "catalog_serial_number = @catalog_serial_number, allowance_qty = @allowance_qty, " +
                "manually_entered = @manually_entered, dropped_in_error = @dropped_in_error, " +
                "manufacturer = @manufacturer, cage = @cage, msds_num = @msds_num, " +
                "data_source_cd = @data_source_cd, changed = @changed, changed_by = @changed_by " +
                "WHERE aul_id = @aul_id ";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@hull_type_id", record.hull_type_id);
                updateCmd.Parameters.AddWithValue("@niin", record.niin);
                updateCmd.Parameters.AddWithValue("@fsc", dbNull(record.fsc));
                updateCmd.Parameters.AddWithValue("@ui", dbNull(record.ui));
                updateCmd.Parameters.AddWithValue("@um", dbNull(record.um));
                updateCmd.Parameters.AddWithValue("@usage_category_id", dbNull(record.usage_category_id));
                updateCmd.Parameters.AddWithValue("@description", dbNull(record.description));
                updateCmd.Parameters.AddWithValue("@smcc_id", dbNull(record.smcc_id));
                updateCmd.Parameters.AddWithValue("@specs", dbNull(record.specs));
                updateCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(record.shelf_life_code_id));
                updateCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(record.shelf_life_action_code_id));
                updateCmd.Parameters.AddWithValue("@remarks", dbNull(record.remarks));
                updateCmd.Parameters.AddWithValue("@storage_type_id", dbNull(record.storage_type_id));
                updateCmd.Parameters.AddWithValue("@cog_id", dbNull(record.cog_id));
                updateCmd.Parameters.AddWithValue("@spmig", dbNull(record.spmig));
                updateCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(record.nehc_rpt));
                updateCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(record.catalog_group_id));
                updateCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(record.catalog_serial_number));
                updateCmd.Parameters.AddWithValue("@allowance_qty", dbNull(record.allowance_qty));
                updateCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.manually_entered));
                updateCmd.Parameters.AddWithValue("@dropped_in_error", dbNull(record.dropped_in_error));
                updateCmd.Parameters.AddWithValue("@manufacturer", dbNull(record.manufacturer));
                updateCmd.Parameters.AddWithValue("@cage", dbNull(record.cage));
                updateCmd.Parameters.AddWithValue("@msds_num", dbNull(record.msds_num));
                updateCmd.Parameters.AddWithValue("@data_source_cd", uic);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", username);
                updateCmd.Parameters.AddWithValue("@aul_id", record.aul_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("AULMaster updated: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.niin, record.description, record.hull_type_id, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating AULMaster: {0}, {1}, {2} - {3}",
                    uic, record.niin, record.hull_type_id, ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion
    #endregion

    #region MSDS
        #region MSDS Gets
        public MsdsEditViewModel getMSDSMasterRecord(long msds_id) {
            MsdsEditViewModel record = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string sqlStmt = "SELECT * FROM msds_master m " +
                "WHERE msds_id = @msds_id";

            try {
                SqlCommand cmd = new SqlCommand(sqlStmt, conn);

                //get the row
                cmd.Parameters.AddWithValue("@msds_id", msds_id);

                SqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read()) {
                    record = new MsdsEditViewModel();
                    int temp_id = -1;
                    DateTime tempDate;

                    // Master data
                    record.MsdsMaster = new msds_master();
                    record.MsdsMaster.msds_id = msds_id;
                    int.TryParse(rdr["hcc_id"].ToString(), out temp_id);
                    record.MsdsMaster.hcc_id = temp_id;
                    record.MsdsMaster.ARTICLE_IND =  rdr["article_ind"].ToString();
                    record.MsdsMaster.CAGE =  rdr["cage"].ToString();
                    record.MsdsMaster.data_source_cd =  rdr["data_source_cd"].ToString();
                    record.MsdsMaster.DESCRIPTION =  rdr["description"].ToString();
                    record.MsdsMaster.EMERGENCY_TEL =  rdr["emergency_tel"].ToString();
                    record.MsdsMaster.END_COMP_IND =  rdr["end_comp_ind"].ToString();
                    record.MsdsMaster.END_ITEM_IND =  rdr["end_item_ind"].ToString();
                    record.MsdsMaster.file_name =  rdr["file_name"].ToString();
                    record.MsdsMaster.KIT_IND =  rdr["kit_ind"].ToString();
                    record.MsdsMaster.KIT_PART_IND =  rdr["kit_part_ind"].ToString();
                    record.MsdsMaster.manually_entered = Convert.ToBoolean(rdr["manually_entered"].ToString());
                    record.MsdsMaster.MANUFACTURER =  rdr["manufacturer"].ToString();
                    record.MsdsMaster.MANUFACTURER_MSDS_NO =  rdr["manufacturer_msds_no"].ToString();
                    record.MsdsMaster.MIXTURE_IND =  rdr["mixture_ind"].ToString();
                    record.MsdsMaster.MSDSSERNO =  rdr["msdsserno"].ToString();
                    record.MsdsMaster.NIIN =  rdr["niin"].ToString();
                    record.MsdsMaster.PARTNO =  rdr["partno"].ToString();
                    record.MsdsMaster.PRODUCT_IDENTITY =  rdr["product_identity"].ToString();
                    record.MsdsMaster.PRODUCT_IND =  rdr["product_ind"].ToString();
                    record.MsdsMaster.PRODUCT_LANGUAGE =  rdr["product_language"].ToString();
                    DateTime.TryParse(rdr["PRODUCT_LOAD_DATE"].ToString(), out tempDate);
                    record.MsdsMaster.PRODUCT_LOAD_DATE = tempDate;
                    record.MsdsMaster.PRODUCT_RECORD_STATUS =  rdr["product_record_status"].ToString();
                    record.MsdsMaster.PRODUCT_REVISION_NO =  rdr["product_revision_no"].ToString();
                    char tempChar;
                    Char.TryParse(rdr["proprietary_ind"].ToString(), out tempChar);
                    record.MsdsMaster.PROPRIETARY_IND = tempChar;
                    Char.TryParse(rdr["published_ind"].ToString(), out tempChar);
                    record.MsdsMaster.PUBLISHED_IND = tempChar;
                    Char.TryParse(rdr["purchased_prod_ind"].ToString(), out tempChar);
                    record.MsdsMaster.PURCHASED_PROD_IND = tempChar;
                    Char.TryParse(rdr["pure_ind"].ToString(), out tempChar);
                    record.MsdsMaster.PURE_IND = tempChar;
                    Char.TryParse(rdr["radioactive_ind"].ToString(), out tempChar);
                    record.MsdsMaster.RADIOACTIVE_IND = tempChar;
                    Char.TryParse(rdr["trade_secret_ind"].ToString(), out tempChar);
                    record.MsdsMaster.TRADE_SECRET_IND = tempChar;
                    record.MsdsMaster.SERVICE_AGENCY = rdr["service_agency"].ToString();
                    record.MsdsMaster.TRADE_NAME =  rdr["trade_name"].ToString();
                    DateTime.TryParse(rdr["created"].ToString(), out tempDate);
                    record.MsdsMaster.created = tempDate;
                    if (rdr["Created_By"] != DBNull.Value)
                        record.MsdsMaster.created_by =  rdr["Created_By"].ToString();
                    DateTime.TryParse(rdr["changed"].ToString(), out tempDate);
                    record.MsdsMaster.changed = tempDate;
                    if (rdr["Changed_By"] != DBNull.Value)
                        record.MsdsMaster.changed_by =  rdr["Changed_by"].ToString();

                    rdr.Close();
                    cmd.Dispose();

                    // Get the subtable data
                    populateMSDSSubTableData(record, conn);

                    // Temp code - push the first contractor_info and ingredients records
                    // into the model objects
                    if (record.MsdsContractorList.Count > 0)
                        record.MsdsContractorInfo = record.MsdsContractorList[0];
                    if (record.MsdsIngredientList.Count > 0)
                        record.MsdsIngredient = record.MsdsIngredientList[0];
                }
            }
            finally {
                conn.Close();
            }

            return record;
        }

        private void populateMSDSSubTableData(MsdsEditViewModel record, SqlConnection conn) {
            // AFJM_PSN
            record.MsdsAfjmPsn = getMSDSAFJM_PSNRecord(record.MsdsMaster.msds_id, conn);
 
            //MSDS CONTRACTOR LIST
            record.MsdsContractorList = getMSDSContractorList(record.MsdsMaster.msds_id, conn);

            //MSDS DISPOSAL
            record.MsdsDisposal = getMSDSDisposalRecord(record.MsdsMaster.msds_id, conn);

            //MSDS DOC TYPES
            record.MsdsDocumentTypes = getMSDSDocumentTypesRecord(record.MsdsMaster.msds_id, conn);

            //MSDS DOT PSN
            record.MsdsDotPsn = getMSDSDOT_PSNRecord(record.MsdsMaster.msds_id, conn);

            //MSDS IATA PSN
            record.MsdsIataPsn = getMSDSIATA_PSNRecord(record.MsdsMaster.msds_id, conn);

            //MSDS IMO PSN
            record.MsdsImoPsn = getMSDSIMO_PSNRecord(record.MsdsMaster.msds_id, conn);

            // MSDS INGREDIENTS
            record.MsdsIngredientList = getMSDSIngredientsList(record.MsdsMaster.msds_id, conn);

            //MSDS ITEM DESCRIPTION
            record.MsdsItemDescription = getMSDSItemDescriptionRecord(record.MsdsMaster.msds_id, conn);

            //MSDS LABEL INFO
            record.MsdsLabelInfo = getMSDSLabelInfoRecord(record.MsdsMaster.msds_id, conn);

            //MSDS PHYS CHEMICAL TABLE
            record.MsdsPhysChemical = getMSDSPhysChemicalRecord(record.MsdsMaster.msds_id, conn);

            //MSDS RADIOLOGICAL INFO
            record.MsdsRadiologicalInfo = getMSDSRadiologicalInfoRecord(record.MsdsMaster.msds_id, conn);

            //MSDS TRANSPORTATION
            record.MsdsTransportation = getMSDSTransportationRecord(record.MsdsMaster.msds_id, conn);

        }

        private msds_afjm_psn getMSDSAFJM_PSNRecord(long msds_id, SqlConnection conn) {
            msds_afjm_psn record = null;

            string sqlStmt = "SELECT * FROM msds_afjm_psn WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_afjm_psn();

                record.AFJM_HAZARD_CLASS = rdr["AFJM_HAZARD_CLASS"].ToString();
                record.AFJM_PACK_GROUP = rdr["AFJM_PACK_GROUP"].ToString();
                record.AFJM_PACK_PARAGRAPH = rdr["AFJM_PACK_PARAGRAPH"].ToString();
                record.AFJM_PROP_SHIP_MODIFIER = rdr["AFJM_PROP_SHIP_MODIFIER"].ToString();
                record.AFJM_PROP_SHIP_NAME = rdr["AFJM_PROP_SHIP_NAME"].ToString();
                record.AFJM_PSN_CODE = rdr["AFJM_PSN_CODE"].ToString();
                record.AFJM_SPECIAL_PROV = rdr["AFJM_SPECIAL_PROV"].ToString();
                record.AFJM_SUBSIDIARY_RISK = rdr["AFJM_SUBSIDIARY_RISK"].ToString();
                record.AFJM_SYMBOLS = rdr["AFJM_SYMBOLS"].ToString();
                record.AFJM_UN_ID_NUMBER = rdr["AFJM_UN_ID_NUMBER"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private List<msds_contractor_info> getMSDSContractorList(long msds_id, SqlConnection conn) {
            List<msds_contractor_info> list = new List<msds_contractor_info>();

            string sqlStmt = "SELECT * FROM msds_contractor_info WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read()) {
                // Create the record
                msds_contractor_info record = new msds_contractor_info();

                record.CT_CAGE = rdr["CT_CAGE"].ToString();
                record.CT_CITY = rdr["CT_CITY"].ToString();
                record.CT_COMPANY_NAME = rdr["CT_COMPANY_NAME"].ToString();
                record.CT_COUNTRY = rdr["CT_COUNTRY"].ToString();
                record.CT_NUMBER = rdr["CT_NUMBER"].ToString();
                record.CT_PHONE = rdr["CT_PHONE"].ToString();
                record.CT_PO_BOX = rdr["CT_PO_BOX"].ToString();
                record.CT_STATE = rdr["CT_STATE"].ToString();
                record.PURCHASE_ORDER_NO = rdr["PURCHASE_ORDER_NO"].ToString();

                // Add it to the list
                list.Add(record);
            }

            rdr.Close();
            cmd.Dispose();

            return list;
        }

        private msds_disposal getMSDSDisposalRecord(long msds_id, SqlConnection conn) {
            msds_disposal record = null;

            string sqlStmt = "SELECT * FROM msds_disposal WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_disposal();

                record.DISPOSAL_ADD_INFO = rdr["DISPOSAL_ADD_INFO"].ToString();
                record.EPA_HAZ_WASTE_CODE = rdr["EPA_HAZ_WASTE_CODE"].ToString();
                char tempChar;
                Char.TryParse(rdr["EPA_HAZ_WASTE_IND"].ToString(), out tempChar);
                record.EPA_HAZ_WASTE_IND = tempChar;
                record.EPA_HAZ_WASTE_NAME = rdr["EPA_HAZ_WASTE_NAME"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_document_type getMSDSDocumentTypesRecord(long msds_id, SqlConnection conn) {
            msds_document_type record = null;

            string sqlStmt = "SELECT * FROM msds_document_types WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_document_type();

                record.manufacturer_label_filename = rdr["manufacturer_label_filename"].ToString();
                record.msds_translated_filename = rdr["msds_translated_filename"].ToString();
                record.neshap_comp_filename = rdr["neshap_comp_filename"].ToString();
                record.other_docs_filename = rdr["other_docs_filename"].ToString();
                record.product_sheet_filename = rdr["product_sheet_filename"].ToString();
                record.transportation_cert_filename = rdr["transportation_cert_filename"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_dot_psn getMSDSDOT_PSNRecord(long msds_id, SqlConnection conn) {
            msds_dot_psn record = null;

            string sqlStmt = "SELECT * FROM msds_dot_psn WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_dot_psn();

                record.DOT_HAZARD_CLASS_DIV = rdr["DOT_HAZARD_CLASS_DIV"].ToString();
                record.DOT_HAZARD_LABEL = rdr["DOT_HAZARD_LABEL"].ToString();
                record.DOT_MAX_CARGO = rdr["DOT_MAX_CARGO"].ToString();
                record.DOT_MAX_PASSENGER = rdr["DOT_MAX_PASSENGER"].ToString();
                record.DOT_PACK_BULK = rdr["DOT_PACK_BULK"].ToString();
                record.DOT_PACK_EXCEPTIONS = rdr["DOT_PACK_EXCEPTIONS"].ToString();
                record.DOT_PACK_GROUP = rdr["DOT_PACK_GROUP"].ToString();
                record.DOT_PACK_NONBULK = rdr["DOT_PACK_NONBULK"].ToString();
                record.DOT_PROP_SHIP_MODIFIER = rdr["DOT_PROP_SHIP_MODIFIER"].ToString();
                record.DOT_PROP_SHIP_NAME = rdr["DOT_PROP_SHIP_NAME"].ToString();
                record.DOT_PSN_CODE = rdr["DOT_PSN_CODE"].ToString();
                record.DOT_SPECIAL_PROVISION = rdr["DOT_SPECIAL_PROVISION"].ToString();
                record.DOT_SYMBOLS = rdr["DOT_SYMBOLS"].ToString();
                record.DOT_UN_ID_NUMBER = rdr["DOT_UN_ID_NUMBER"].ToString();
                record.DOT_WATER_OTHER_REQ = rdr["DOT_WATER_OTHER_REQ"].ToString();
                record.DOT_WATER_VESSEL_STOW = rdr["DOT_WATER_VESSEL_STOW"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_iata_psn getMSDSIATA_PSNRecord(long msds_id, SqlConnection conn) {
            msds_iata_psn record = null;

            string sqlStmt = "SELECT * FROM msds_iata_psn WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_iata_psn();

                record.IATA_CARGO_PACK_MAX_QTY = rdr["IATA_CARGO_PACK_MAX_QTY"].ToString();
                record.IATA_CARGO_PACKING = rdr["IATA_CARGO_PACKING"].ToString();
                record.IATA_HAZARD_CLASS = rdr["IATA_HAZARD_CLASS"].ToString();
                record.IATA_HAZARD_LABEL = rdr["IATA_HAZARD_LABEL"].ToString();
                record.IATA_PACK_GROUP = rdr["IATA_PACK_GROUP"].ToString();
                record.IATA_PASS_AIR_MAX_QTY = rdr["IATA_PASS_AIR_MAX_QTY"].ToString();
                record.IATA_PASS_AIR_PACK_LMT_INSTR = rdr["IATA_PASS_AIR_PACK_LMT_INSTR"].ToString();
                record.IATA_PASS_AIR_PACK_LMT_PER_PKG = rdr["IATA_PASS_AIR_PACK_LMT_PER_PKG"].ToString();
                record.IATA_PASS_AIR_PACK_NOTE = rdr["IATA_PASS_AIR_PACK_NOTE"].ToString();
                record.IATA_PROP_SHIP_MODIFIER = rdr["IATA_PROP_SHIP_MODIFIER"].ToString();
                record.IATA_PROP_SHIP_NAME = rdr["IATA_PROP_SHIP_NAME"].ToString();
                record.IATA_PSN_CODE = rdr["IATA_PSN_CODE"].ToString();
                record.IATA_SPECIAL_PROV = rdr["IATA_SPECIAL_PROV"].ToString();
                record.IATA_SUBSIDIARY_RISK = rdr["IATA_SUBSIDIARY_RISK"].ToString();
                record.IATA_UN_ID_NUMBER = rdr["IATA_UN_ID_NUMBER"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_imo_psn getMSDSIMO_PSNRecord(long msds_id, SqlConnection conn) {
            msds_imo_psn record = null;

            string sqlStmt = "SELECT * FROM msds_imo_psn WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_imo_psn();

                record.IMO_EMS_NO = rdr["IMO_EMS_NO"].ToString();
                record.IMO_HAZARD_CLASS = rdr["IMO_HAZARD_CLASS"].ToString();
                record.IMO_IBC_INSTR = rdr["IMO_IBC_INSTR"].ToString();
                record.IMO_IBC_PROVISIONS = rdr["IMO_IBC_PROVISIONS"].ToString();
                record.IMO_LIMITED_QTY = rdr["IMO_LIMITED_QTY"].ToString();
                record.IMO_PACK_GROUP = rdr["IMO_PACK_GROUP"].ToString();
                record.IMO_PACK_INSTRUCTIONS = rdr["IMO_PACK_INSTRUCTIONS"].ToString();
                record.IMO_PACK_PROVISIONS = rdr["IMO_PACK_PROVISIONS"].ToString();
                record.IMO_PROP_SHIP_MODIFIER = rdr["IMO_PROP_SHIP_MODIFIER"].ToString();
                record.IMO_PROP_SHIP_NAME = rdr["IMO_PROP_SHIP_NAME"].ToString();
                record.IMO_PSN_CODE = rdr["IMO_PSN_CODE"].ToString();
                record.IMO_SPECIAL_PROV = rdr["IMO_SPECIAL_PROV"].ToString();
                record.IMO_STOW_SEGR = rdr["IMO_STOW_SEGR"].ToString();
                record.IMO_SUBSIDIARY_RISK = rdr["IMO_SUBSIDIARY_RISK"].ToString();
                record.IMO_TANK_INSTR_IMO = rdr["IMO_TANK_INSTR_IMO"].ToString();
                record.IMO_TANK_INSTR_PROV = rdr["IMO_TANK_INSTR_PROV"].ToString();
                record.IMO_TANK_INSTR_UN = rdr["IMO_TANK_INSTR_UN"].ToString();
                record.IMO_UN_NUMBER = rdr["IMO_UN_NUMBER"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private List<msds_ingredient> getMSDSIngredientsList(long msds_id, SqlConnection conn) {
            List<msds_ingredient> list = new List<msds_ingredient>();

            string sqlStmt = "SELECT * FROM msds_ingredients WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read()) {
                // Create the record
                msds_ingredient record = new msds_ingredient();

                record.ACGIH_STEL = rdr["ACGIH_STEL"].ToString();
                record.ACGIH_TLV = rdr["ACGIH_TLV"].ToString();
                record.CAS = rdr["CAS"].ToString();
                record.CHEM_MFG_COMP_NAME = rdr["CHEM_MFG_COMP_NAME"].ToString();
                record.DOT_REPORT_QTY = rdr["DOT_REPORT_QTY"].ToString();
                record.EPA_REPORT_QTY = rdr["EPA_REPORT_QTY"].ToString();
                record.INGREDIENT_NAME = rdr["INGREDIENT_NAME"].ToString();
                record.ODS_IND = rdr["ODS_IND"].ToString();
                record.OSHA_PEL = rdr["OSHA_PEL"].ToString();
                record.OSHA_STEL = rdr["OSHA_STEL"].ToString();
                record.OTHER_REC_LIMITS = rdr["OTHER_REC_LIMITS"].ToString();
                record.PRCNT = rdr["PRCNT"].ToString();
                record.PRCNT_VOL_VALUE = rdr["PRCNT_VOL_VALUE"].ToString();
                record.PRCNT_VOL_WEIGHT = rdr["PRCNT_VOL_WEIGHT"].ToString();
                record.RTECS_CODE = rdr["RTECS_CODE"].ToString();
                record.RTECS_NUM = rdr["RTECS_NUM"].ToString();

                // Add it to the list
                list.Add(record);
            }

            rdr.Close();
            cmd.Dispose();

            return list;
        }

        private msds_item_description getMSDSItemDescriptionRecord(long msds_id, SqlConnection conn) {
            msds_item_description record = null;

            string sqlStmt = "SELECT * FROM msds_item_description WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_item_description();
                int temp_id = 0;

                record.BATCH_NUMBER = rdr["BATCH_NUMBER"].ToString();
                record.ITEM_MANAGER = rdr["ITEM_MANAGER"].ToString();
                record.ITEM_NAME = rdr["ITEM_NAME"].ToString();
                char tempChar;
                Char.TryParse(rdr["LOG_FLIS_NIIN_VER"].ToString(), out tempChar);
                record.LOG_FLIS_NIIN_VER = tempChar;
                int.TryParse(rdr["LOG_FSC"].ToString(), out temp_id);
                record.LOG_FSC = temp_id;
                record.LOT_NUMBER = rdr["LOT_NUMBER"].ToString();
                record.NET_UNIT_WEIGHT = rdr["NET_UNIT_WEIGHT"].ToString();
                record.QUANTITATIVE_EXPRESSION = rdr["QUANTITATIVE_EXPRESSION"].ToString();
                record.SHELF_LIFE_CODE = rdr["SHELF_LIFE_CODE"].ToString();
                record.SPECIAL_EMP_CODE = rdr["SPECIAL_EMP_CODE"].ToString();
                record.SPECIFICATION_NUMBER = rdr["SPECIFICATION_NUMBER"].ToString();
                record.TYPE_GRADE_CLASS = rdr["TYPE_GRADE_CLASS"].ToString();
                record.TYPE_OF_CONTAINER = rdr["TYPE_OF_CONTAINER"].ToString();
                record.UI_CONTAINER_QTY = rdr["UI_CONTAINER_QTY"].ToString();
                record.UN_NA_NUMBER = rdr["UN_NA_NUMBER"].ToString();
                record.UNIT_OF_ISSUE = rdr["UNIT_OF_ISSUE"].ToString();
                record.UPC_GTIN = rdr["UPC_GTIN"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_label_info getMSDSLabelInfoRecord(long msds_id, SqlConnection conn) {
            msds_label_info record = null;

            string sqlStmt = "SELECT * FROM msds_label_info WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_label_info();

                record.COMPANY_CAGE_RP = rdr["COMPANY_CAGE_RP"].ToString();
                record.COMPANY_NAME_RP = rdr["COMPANY_NAME_RP"].ToString();
                record.LABEL_EMERG_PHONE = rdr["LABEL_EMERG_PHONE"].ToString();
                record.LABEL_ITEM_NAME = rdr["LABEL_ITEM_NAME"].ToString();
                record.LABEL_PROC_YEAR = rdr["LABEL_PROC_YEAR"].ToString();
                record.LABEL_PROD_IDENT = rdr["LABEL_PROD_IDENT"].ToString();
                record.LABEL_PROD_SERIALNO = rdr["LABEL_PROD_SERIALNO"].ToString();
                record.LABEL_SIGNAL_WORD = rdr["LABEL_SIGNAL_WORD"].ToString();
                record.LABEL_STOCK_NO = rdr["LABEL_STOCK_NO"].ToString();
                record.SPECIFIC_HAZARDS = rdr["SPECIFIC_HAZARDS"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_phys_chemical getMSDSPhysChemicalRecord(long msds_id, SqlConnection conn) {
            msds_phys_chemical record = null;

            string sqlStmt = "SELECT * FROM msds_phys_chemical WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_phys_chemical();

                record.APP_ODOR = rdr["APP_ODOR"].ToString();
                record.AUTOIGNITION_TEMP = rdr["AUTOIGNITION_TEMP"].ToString();
                char tempChar;
                Char.TryParse(rdr["CARCINOGEN_IND"].ToString(), out tempChar);
                record.CARCINOGEN_IND = tempChar;
                Char.TryParse(rdr["EPA_ACUTE"].ToString(), out tempChar);
                record.EPA_ACUTE = tempChar;
                Char.TryParse(rdr["EPA_CHRONIC"].ToString(), out tempChar);
                record.EPA_CHRONIC = tempChar;
                Char.TryParse(rdr["EPA_FIRE"].ToString(), out tempChar);
                record.EPA_FIRE = tempChar;
                Char.TryParse(rdr["EPA_PRESSURE"].ToString(), out tempChar);
                record.EPA_PRESSURE = tempChar;
                Char.TryParse(rdr["EPA_REACTIVITY"].ToString(), out tempChar);
                record.EPA_REACTIVITY = tempChar;
                record.EVAP_RATE_REF = rdr["EVAP_RATE_REF"].ToString();
                record.FLASH_PT_TEMP = rdr["FLASH_PT_TEMP"].ToString();
                record.NEUT_AGENT = rdr["NEUT_AGENT"].ToString();
                record.NFPA_FLAMMABILITY = rdr["NFPA_FLAMMABILITY"].ToString();
                record.NFPA_HEALTH = rdr["NFPA_HEALTH"].ToString();
                record.NFPA_REACTIVITY = rdr["NFPA_REACTIVITY"].ToString();
                record.NFPA_SPECIAL = rdr["NFPA_SPECIAL"].ToString();
                Char.TryParse(rdr["OSHA_CARCINOGENS"].ToString(), out tempChar);
                record.OSHA_CARCINOGENS = tempChar;
                Char.TryParse(rdr["OSHA_COMB_LIQUID"].ToString(), out tempChar);
                record.OSHA_COMB_LIQUID = tempChar;
                Char.TryParse(rdr["OSHA_COMP_GAS"].ToString(), out tempChar);
                record.OSHA_COMP_GAS = tempChar;
                Char.TryParse(rdr["OSHA_CORROSIVE"].ToString(), out tempChar);
                record.OSHA_CORROSIVE = tempChar;
                Char.TryParse(rdr["OSHA_EXPLOSIVE"].ToString(), out tempChar);
                record.OSHA_EXPLOSIVE = tempChar;
                Char.TryParse(rdr["OSHA_FLAMMABLE"].ToString(), out tempChar);
                record.OSHA_FLAMMABLE = tempChar;
                Char.TryParse(rdr["OSHA_HIGH_TOXIC"].ToString(), out tempChar);
                record.OSHA_HIGH_TOXIC = tempChar;
                Char.TryParse(rdr["OSHA_IRRITANT"].ToString(), out tempChar);
                record.OSHA_IRRITANT = tempChar;
                Char.TryParse(rdr["OSHA_ORG_PEROX"].ToString(), out tempChar);
                record.OSHA_ORG_PEROX = tempChar;
                Char.TryParse(rdr["OSHA_OTHERLONGTERM"].ToString(), out tempChar);
                record.OSHA_OTHERLONGTERM = tempChar;
                Char.TryParse(rdr["OSHA_OXIDIZER"].ToString(), out tempChar);
                record.OSHA_OXIDIZER = tempChar;
                Char.TryParse(rdr["OSHA_PYRO"].ToString(), out tempChar);
                record.OSHA_PYRO = tempChar;
                Char.TryParse(rdr["OSHA_SENSITIZER"].ToString(), out tempChar);
                record.OSHA_SENSITIZER = tempChar;
                Char.TryParse(rdr["OSHA_TOXIC"].ToString(), out tempChar);
                record.OSHA_TOXIC = tempChar;
                Char.TryParse(rdr["OSHA_UNST_REACT"].ToString(), out tempChar);
                record.OSHA_UNST_REACT = tempChar;
                Char.TryParse(rdr["OSHA_WATER_REACTIVE"].ToString(), out tempChar);
                record.OSHA_WATER_REACTIVE = tempChar;
                record.OTHER_SHORT_TERM = rdr["OTHER_SHORT_TERM"].ToString();
                record.PERCENT_VOL_VOLUME = rdr["PERCENT_VOL_VOLUME"].ToString();
                record.PH = rdr["PH"].ToString();
                record.PHYS_STATE_CODE = rdr["PHYS_STATE_CODE"].ToString();
                record.SOL_IN_WATER = rdr["SOL_IN_WATER"].ToString();
                record.SPECIFIC_GRAV = rdr["SPECIFIC_GRAV"].ToString();
                record.VAPOR_DENS = rdr["VAPOR_DENS"].ToString();
                record.VAPOR_PRESS = rdr["VAPOR_PRESS"].ToString();
                record.VISCOSITY = rdr["VISCOSITY"].ToString();
                record.VOC_GRAMS_LITER = rdr["VOC_GRAMS_LITER"].ToString();
                record.VOC_POUNDS_GALLON = rdr["VOC_POUNDS_GALLON"].ToString();
                record.VOL_ORG_COMP_WT = rdr["VOL_ORG_COMP_WT"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_radiological_info getMSDSRadiologicalInfoRecord(long msds_id, SqlConnection conn) {
            msds_radiological_info record = null;

            string sqlStmt = "SELECT * FROM msds_radiological_info WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_radiological_info();

                record.NRC_LP_NUM = rdr["NRC_LP_NUM"].ToString();
                record.OPERATOR = rdr["OPERATOR"].ToString();
                record.RAD_AMOUNT_MICRO = rdr["RAD_AMOUNT_MICRO"].ToString();
                record.RAD_CAS = rdr["RAD_CAS"].ToString();
                record.RAD_FORM = rdr["RAD_FORM"].ToString();
                record.RAD_NAME = rdr["RAD_NAME"].ToString();
                record.RAD_SYMBOL = rdr["RAD_SYMBOL"].ToString();
                record.REP_NSN = rdr["REP_NSN"].ToString();
                char tempChar;
                Char.TryParse(rdr["SEALED"].ToString(), out tempChar);
                record.SEALED = tempChar;

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        private msds_transportation getMSDSTransportationRecord(long msds_id, SqlConnection conn) {
            msds_transportation record = null;

            string sqlStmt = "SELECT * FROM msds_transportation WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                record = new msds_transportation();
                int temp_id = -1;

                record.AF_MMAC_CODE = rdr["AF_MMAC_CODE"].ToString();
                record.CERTIFICATE_COE = rdr["CERTIFICATE_COE"].ToString();
                record.COMPETENT_CAA = rdr["COMPETENT_CAA"].ToString();
                record.DOD_ID_CODE = rdr["DOD_ID_CODE"].ToString();
                record.DOT_EXEMPTION_NO = rdr["DOT_EXEMPTION_NO"].ToString();
                char tempChar;
                Char.TryParse(rdr["DOT_RQ_IND"].ToString(), out tempChar);
                record.DOT_RQ_IND = tempChar;
                record.EX_NO = rdr["EX_NO"].ToString();
                int.TryParse(rdr["FLASH_PT_TEMP"].ToString(), out temp_id);
                record.FLASH_PT_TEMP = temp_id;
                record.HCC = rdr["HCC"].ToString();
                int.TryParse(rdr["HIGH_EXPLOSIVE_WT"].ToString(), out temp_id);
                record.HIGH_EXPLOSIVE_WT = temp_id;
                Char.TryParse(rdr["LTD_QTY_IND"].ToString(), out tempChar);
                record.LTD_QTY_IND = tempChar;
                Char.TryParse(rdr["MAGNETIC_IND"].ToString(), out tempChar);
                record.MAGNETIC_IND = tempChar;
                record.MAGNETISM = rdr["MAGNETISM"].ToString();
                Char.TryParse(rdr["MARINE_POLLUTANT_IND"].ToString(), out tempChar);
                record.MARINE_POLLUTANT_IND = tempChar;
                int.TryParse(rdr["NET_EXP_QTY_DIST"].ToString(), out temp_id);
                record.NET_EXP_QTY_DIST = temp_id;
                record.NET_EXP_WEIGHT = rdr["NET_EXP_WEIGHT"].ToString();
                record.NET_PROPELLANT_WT = rdr["NET_PROPELLANT_WT"].ToString();
                record.NOS_TECHNICAL_SHIPPING_NAME = rdr["NOS_TECHNICAL_SHIPPING_NAME"].ToString();
                record.TRANSPORTATION_ADDITIONAL_DATA = rdr["TRANSPORTATION_ADDITIONAL_DATA"].ToString();

                rdr.Close();
                cmd.Dispose();
            }

            return record;
        }

        public long getMSDSIdFromSerNoCage(String msdsSerNo, String cage) {
            long msds_id = -1;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string sqlStmt = "SELECT msds_id FROM msds_master WHERE MSDSSERNO = @msdsSerNo " +
                "AND CAGE = @cage ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msdsSerNo", msdsSerNo);
            cmd.Parameters.AddWithValue("@cage", cage);

            try {
                msds_id = (Int64)cmd.ExecuteScalar();
            }
            catch {
                msds_id = -1;
            }
            finally {
                conn.Close();
            }

            return msds_id;
        }
        #endregion

        #region MSDS Inserts
        public long insertMSDSMasterRecord(String uic, MsdsEditViewModel record, String username) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //// Check to make sure we have the ID values - just in case
            //if (record.hcc_id == 0) {
            //    record.hcc_id = getIDFromValue(
            //        "HCC", "hcc_id", "hcc", record.HCC, conn);
            //}

            String stmt = "INSERT INTO msds_master (MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, " +
                "NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, " +
                "DESCRIPTION, EMERGENCY_TEL, END_COMP_IND, END_ITEM_IND, " +
                "KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, " +
                "PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, " +
                "PRODUCT_REVISION_NO, PROPRIETARY_IND, PUBLISHED_IND, " +
                "PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, " +
                "TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, " +
                "data_source_cd, created, created_by) " +
                "VALUES(@MSDSSERNO, @CAGE, @MANUFACTURER, @PARTNO, @FSC, " +
                "@NIIN, @hcc_id, @file_name, @manually_entered, @ARTICLE_IND, " +
                "@DESCRIPTION, @EMERGENCY_TEL, @END_COMP_IND, @END_ITEM_IND, " +
                "@KIT_IND, @KIT_PART_IND, @MANUFACTURER_MSDS_NO, @MIXTURE_IND, " +
                "@PRODUCT_IDENTITY, @PRODUCT_LOAD_DATE, @PRODUCT_RECORD_STATUS, " +
                "@PRODUCT_REVISION_NO, @PROPRIETARY_IND, @PUBLISHED_IND, " +
                "@PURCHASED_PROD_IND, @PURE_IND, @RADIOACTIVE_IND, @SERVICE_AGENCY, " +
                "@TRADE_NAME, @TRADE_SECRET_IND, @PRODUCT_IND, @PRODUCT_LANGUAGE, " +
                "@data_source_cd, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();
            SqlCommand insertCmd = null;

            try {
                insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@MSDSSERNO", record.MsdsMaster.MSDSSERNO);
                insertCmd.Parameters.AddWithValue("@CAGE", dbNull(record.MsdsMaster.CAGE));
                insertCmd.Parameters.AddWithValue("@MANUFACTURER", dbNull(record.MsdsMaster.MANUFACTURER));
                insertCmd.Parameters.AddWithValue("@PARTNO", dbNull(record.MsdsMaster.PARTNO));
                insertCmd.Parameters.AddWithValue("@FSC", dbNull(record.MsdsMaster.FSC));
                insertCmd.Parameters.AddWithValue("@NIIN", dbNull(record.MsdsMaster.NIIN));
                insertCmd.Parameters.AddWithValue("@hcc_id", dbNull(record.MsdsMaster.hcc_id));
                insertCmd.Parameters.AddWithValue("@file_name", dbNull(record.MsdsMaster.file_name));
                insertCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.MsdsMaster.manually_entered));
                insertCmd.Parameters.AddWithValue("@ARTICLE_IND", dbNull(record.MsdsMaster.ARTICLE_IND));
                insertCmd.Parameters.AddWithValue("@DESCRIPTION", dbNull(record.MsdsMaster.DESCRIPTION));
                insertCmd.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(record.MsdsMaster.EMERGENCY_TEL));
                insertCmd.Parameters.AddWithValue("@END_COMP_IND", dbNull(record.MsdsMaster.END_COMP_IND));
                insertCmd.Parameters.AddWithValue("@END_ITEM_IND", dbNull(record.MsdsMaster.END_ITEM_IND));
                insertCmd.Parameters.AddWithValue("@KIT_IND", dbNull(record.MsdsMaster.KIT_IND));
                insertCmd.Parameters.AddWithValue("@KIT_PART_IND", dbNull(record.MsdsMaster.KIT_PART_IND));
                insertCmd.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(record.MsdsMaster.MANUFACTURER_MSDS_NO));
                insertCmd.Parameters.AddWithValue("@MIXTURE_IND", dbNull(record.MsdsMaster.MIXTURE_IND));
                insertCmd.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(record.MsdsMaster.PRODUCT_IDENTITY));
                insertCmd.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(record.MsdsMaster.PRODUCT_LOAD_DATE));
                insertCmd.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(record.MsdsMaster.PRODUCT_RECORD_STATUS));
                insertCmd.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(record.MsdsMaster.PRODUCT_REVISION_NO));
                insertCmd.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(record.MsdsMaster.PROPRIETARY_IND));
                insertCmd.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(record.MsdsMaster.PUBLISHED_IND));
                insertCmd.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(record.MsdsMaster.PURCHASED_PROD_IND));
                insertCmd.Parameters.AddWithValue("@PURE_IND", dbNull(record.MsdsMaster.PURE_IND));
                insertCmd.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(record.MsdsMaster.RADIOACTIVE_IND));
                insertCmd.Parameters.AddWithValue("@SERVICE_AGENCY", dbNull(record.MsdsMaster.SERVICE_AGENCY));
                insertCmd.Parameters.AddWithValue("@TRADE_NAME", dbNull(record.MsdsMaster.TRADE_NAME));
                insertCmd.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(record.MsdsMaster.TRADE_SECRET_IND));
                insertCmd.Parameters.AddWithValue("@PRODUCT_IND", dbNull(record.MsdsMaster.PRODUCT_IND));
                insertCmd.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(record.MsdsMaster.PRODUCT_LANGUAGE));
                insertCmd.Parameters.AddWithValue("@data_source_cd", uic);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("MSDSMaster Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.MsdsMaster.MSDSSERNO, record.MsdsMaster.CAGE, record.MsdsMaster.PRODUCT_IDENTITY, result);

                // Process the sub table info
                long subresult = insertMSDSSubTables(result, record, conn);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting MSDSMaster: {0}, {1}, {2} - {3}",
                    uic, record.MsdsMaster.MSDSSERNO, record.MsdsMaster.CAGE, ex.Message);
                throw ex;
            }
            finally {
                insertCmd.Dispose();
                conn.Close();
            }

            return result;
        }

        private long insertMSDSSubTables(long msds_id, MsdsEditViewModel record, SqlConnection conn) {
            long result = 0;

            // AFJM_PSN
            if (record.MsdsAfjmPsn != null) {
                insertMSDSAFJM_PSNRecord(msds_id, record.MsdsAfjmPsn, conn);
            }
            //MSDS CONTRACTOR LIST
            if (record.MsdsContractorList.Count > 0) {
                insertMSDSContractorListRecord(msds_id, record.MsdsContractorList, conn);
            }
            //MSDS DISPOSAL
            if (record.MsdsDisposal != null) {
                insertMSDSDisposalRecord(msds_id, record.MsdsDisposal, conn);
            }
            //MSDS DOC TYPES
            if (record.MsdsDocumentTypes != null) {
                insertMSDSDocumentTypesRecord(msds_id, record.MsdsDocumentTypes, conn);
            }
            //MSDS DOT PSN
            if (record.MsdsDotPsn != null) {
                insertMSDSDOT_PSNRecord(msds_id, record.MsdsDotPsn, conn);
            }
            //MSDS IATA PSN
            if (record.MsdsIataPsn != null) {
                insertMSDSIATA_PSNRecord(msds_id, record.MsdsIataPsn, conn);
            }
            //MSDS IMO PSN
            if (record.MsdsImoPsn != null) {
                insertMSDSIMO_PSNRecord(msds_id, record.MsdsImoPsn, conn);
            }
            // MSDS INGREDIENTS
            if (record.MsdsIngredientList.Count > 0) {
                insertMSDSIngredientsListRecord(msds_id, record.MsdsIngredientList, conn);
            }
            //MSDS ITEM DESCRIPTION
            if (record.MsdsItemDescription != null) {
                insertMSDSItemDescriptionRecord(msds_id, record.MsdsItemDescription, conn);
            }
            //MSDS LABEL INFO
            if (record.MsdsLabelInfo != null) {
                insertMSDSLabelInfoRecord(msds_id, record.MsdsLabelInfo, conn);
            }
            //MSDS PHYS CHEMICAL TABLE
            if (record.MsdsPhysChemical != null) {
                insertMSDSPhysChemicalRecord(msds_id, record.MsdsPhysChemical, conn);
            }
            //MSDS RADIOLOGICAL INFO
            if (record.MsdsRadiologicalInfo != null) {
                insertMSDSRadiologicalInfoRecord(msds_id, record.MsdsRadiologicalInfo, conn);
            }
            //MSDS TRANSPORTATION
            if (record.MsdsTransportation != null) {
                insertMSDSTransportationRecord(msds_id, record.MsdsTransportation, conn);
            }

            return result;
        }

        private long insertMSDSAFJM_PSNRecord(long msds_id, msds_afjm_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_afjm_psn (AFJM_HAZARD_CLASS, " +
                "AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP, AFJM_PROP_SHIP_NAME, " +
                "AFJM_PROP_SHIP_MODIFIER, AFJM_PSN_CODE, AFJM_SPECIAL_PROV, " +
                "AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS, AFJM_UN_ID_NUMBER, msds_id) " +
                "VALUES(@AFJM_HAZARD_CLASS, @AFJM_PACK_PARAGRAPH, " +
                "@AFJM_PACK_GROUP, @AFJM_PROP_SHIP_NAME, @AFJM_PROP_SHIP_MODIFIER, " +
                "@AFJM_PSN_CODE, @AFJM_SPECIAL_PROV, @AFJM_SUBSIDIARY_RISK, " +
                "@AFJM_SYMBOLS, @AFJM_UN_ID_NUMBER, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(record.AFJM_HAZARD_CLASS));
                insertCmd.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(record.AFJM_PACK_PARAGRAPH));
                insertCmd.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(record.AFJM_PACK_GROUP));
                insertCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(record.AFJM_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(record.AFJM_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(record.AFJM_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(record.AFJM_SPECIAL_PROV));
                insertCmd.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(record.AFJM_SUBSIDIARY_RISK));
                insertCmd.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(record.AFJM_SYMBOLS));
                insertCmd.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(record.AFJM_UN_ID_NUMBER));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("AFJM_PSN Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.AFJM_HAZARD_CLASS, record.AFJM_PSN_CODE, record.AFJM_UN_ID_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting AFJM_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.AFJM_HAZARD_CLASS, record.AFJM_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS CONTRACTOR LIST
        private long insertMSDSContractorListRecord(long msds_id, List<msds_contractor_info> contractorList, 
            SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_contractor_info (CT_NUMBER, CT_CAGE, " +
                "CT_CITY, CT_COMPANY_NAME, CT_COUNTRY, CT_PO_BOX, CT_PHONE, CT_STATE, " +
                "PURCHASE_ORDER_NO, msds_id) " +
                "VALUES(@CT_NUMBER, @CT_CAGE, @CT_CITY, @CT_COMPANY_NAME, @CT_COUNTRY, " +
                "@CT_PO_BOX, @CT_PHONE, @CT_STATE, @PURCHASE_ORDER_NO, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = null;

            SqlCommand insertCmd = new SqlCommand(stmt, conn);

            // Loop through the contracotr records
            foreach (msds_contractor_info record in contractorList) {
                try {
                    tran = conn.BeginTransaction();
                    
                    // Clear the parameters
                    insertCmd.Parameters.Clear();
                    insertCmd.Transaction = tran;

                    insertCmd.Parameters.AddWithValue("@CT_NUMBER", dbNull(record.CT_NUMBER));
                    insertCmd.Parameters.AddWithValue("@CT_CAGE", dbNull(record.CT_CAGE));
                    insertCmd.Parameters.AddWithValue("@CT_CITY", dbNull(record.CT_CITY));
                    insertCmd.Parameters.AddWithValue("@CT_COMPANY_NAME", dbNull(record.CT_COMPANY_NAME));
                    insertCmd.Parameters.AddWithValue("@CT_COUNTRY", dbNull(record.CT_COUNTRY));
                    insertCmd.Parameters.AddWithValue("@CT_PO_BOX", dbNull(record.CT_PO_BOX));
                    insertCmd.Parameters.AddWithValue("@CT_PHONE", dbNull(record.CT_PHONE));
                    insertCmd.Parameters.AddWithValue("@CT_STATE", dbNull(record.CT_STATE));
                    insertCmd.Parameters.AddWithValue("@PURCHASE_ORDER_NO", dbNull(record.PURCHASE_ORDER_NO));
                    insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                    object o = insertCmd.ExecuteScalar();

                    result = Convert.ToInt32(o);

                    tran.Commit();

                    logger.InfoFormat("ContractorInfo Inserted: {0}, {1}, {2}, {3} ID = {4}",
                        msds_id, record.CT_NUMBER, record.CT_CAGE, record.PURCHASE_ORDER_NO, result);
                }
                catch (Exception ex) {
                    logger.ErrorFormat("Error inserting ContractorInfo: {0}, {1}, {2} - {3}",
                        msds_id, record.CT_NUMBER, record.CT_CAGE, ex.Message);
                }
            }

            return result;
        }

        //MSDS DISPOSAL
        private long insertMSDSDisposalRecord(long msds_id, msds_disposal record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_disposal (DISPOSAL_ADD_INFO, EPA_HAZ_WASTE_CODE, " +
                "EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME, msds_id) " +
                "VALUES(@DISPOSAL_ADD_INFO, @EPA_HAZ_WASTE_CODE, " +
                "@EPA_HAZ_WASTE_IND, @EPA_HAZ_WASTE_NAME, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(record.DISPOSAL_ADD_INFO));
                insertCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(record.EPA_HAZ_WASTE_CODE));
                insertCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(record.EPA_HAZ_WASTE_IND));
                insertCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(record.EPA_HAZ_WASTE_NAME));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("Disposal Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, record.EPA_HAZ_WASTE_IND, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting Disposal: {0}, {1}, {2} - {3}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOC TYPES
        private long insertMSDSDocumentTypesRecord(long msds_id, msds_document_type record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_document_types (msds_id, " +
                "msds_translated_filename, neshap_comp_filename, other_docs_filename, " +
                "product_sheet_filename, transportation_cert_filename, manufacturer_label_filename) " +
                "VALUES(@msds_id, @msds_translated_filename, @neshap_comp_filename, " +
                "@other_docs_filename, @product_sheet_filename, @transportation_cert_filename, " + 
                "@manufacturer_label_filename); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);
                insertCmd.Parameters.AddWithValue("@msds_translated_filename", dbNull(record.msds_translated_filename));
                insertCmd.Parameters.AddWithValue("@neshap_comp_filename", dbNull(record.neshap_comp_filename));
                insertCmd.Parameters.AddWithValue("@other_docs_filename", dbNull(record.other_docs_filename));
                insertCmd.Parameters.AddWithValue("@product_sheet_filename", dbNull(record.product_sheet_filename));
                insertCmd.Parameters.AddWithValue("@transportation_cert_filename", dbNull(record.transportation_cert_filename));
                insertCmd.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(record.manufacturer_label_filename));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("DocumentTypes Inserted: {0}, {1}, {2} ID = {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting DocumentTypes: {0}, {1}, {2} - {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOT PSN
        private long insertMSDSDOT_PSNRecord(long msds_id, msds_dot_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_dot_psn (DOT_HAZARD_CLASS_DIV, DOT_HAZARD_LABEL, " +
                "DOT_MAX_CARGO, DOT_MAX_PASSENGER, DOT_PACK_BULK, DOT_PACK_EXCEPTIONS, " +
                "DOT_PACK_NONBULK, DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER, " +
                "DOT_PSN_CODE, DOT_SPECIAL_PROVISION, DOT_SYMBOLS, DOT_UN_ID_NUMBER, " +
                "DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW, DOT_PACK_GROUP, msds_id) " +
                "VALUES(@DOT_HAZARD_CLASS_DIV, @DOT_HAZARD_LABEL, @DOT_MAX_CARGO, " +
                "@DOT_MAX_PASSENGER, @DOT_PACK_BULK, @DOT_PACK_EXCEPTIONS, " +
                "@DOT_PACK_NONBULK, @DOT_PROP_SHIP_NAME, @DOT_PROP_SHIP_MODIFIER, " +
                "@DOT_PSN_CODE, @DOT_SPECIAL_PROVISION, @DOT_SYMBOLS, @DOT_UN_ID_NUMBER, " +
                "@DOT_WATER_OTHER_REQ, @DOT_WATER_VESSEL_STOW, @DOT_PACK_GROUP, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(record.DOT_HAZARD_CLASS_DIV));
                insertCmd.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(record.DOT_HAZARD_LABEL));
                insertCmd.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(record.DOT_MAX_CARGO));
                insertCmd.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(record.DOT_MAX_PASSENGER));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(record.DOT_PACK_BULK));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(record.DOT_PACK_EXCEPTIONS));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(record.DOT_PACK_NONBULK));
                insertCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(record.DOT_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(record.DOT_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(record.DOT_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(record.DOT_SPECIAL_PROVISION));
                insertCmd.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(record.DOT_SYMBOLS));
                insertCmd.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(record.DOT_UN_ID_NUMBER));
                insertCmd.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(record.DOT_WATER_OTHER_REQ));
                insertCmd.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(record.DOT_WATER_VESSEL_STOW));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(record.DOT_PACK_GROUP));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("DOT_PSN Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.DOT_HAZARD_CLASS_DIV, record.DOT_HAZARD_LABEL, record.DOT_PSN_CODE, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting DOT_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.DOT_HAZARD_CLASS_DIV, record.DOT_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS IATA PSN
        private long insertMSDSIATA_PSNRecord(long msds_id, msds_iata_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_iata_psn (IATA_CARGO_PACKING, " +
                "IATA_HAZARD_CLASS, IATA_HAZARD_LABEL, IATA_PACK_GROUP, " +
                "IATA_PASS_AIR_PACK_LMT_INSTR, IATA_PASS_AIR_PACK_LMT_PER_PKG,  " +
                "IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME, IATA_PROP_SHIP_MODIFIER, " +
                "IATA_CARGO_PACK_MAX_QTY, IATA_PSN_CODE, IATA_PASS_AIR_MAX_QTY, " +
                "IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK, IATA_UN_ID_NUMBER, msds_id) " +
                "VALUES(@IATA_CARGO_PACKING, @IATA_HAZARD_CLASS, @IATA_HAZARD_LABEL, " +
                "@IATA_PACK_GROUP, @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                "@IATA_PASS_AIR_PACK_LMT_PER_PKG, @IATA_PASS_AIR_PACK_NOTE, " +
                "@IATA_PROP_SHIP_NAME, @IATA_PROP_SHIP_MODIFIER, " +
                "@IATA_CARGO_PACK_MAX_QTY, @IATA_PSN_CODE, @IATA_PASS_AIR_MAX_QTY, " +
                "@IATA_SPECIAL_PROV, @IATA_SUBSIDIARY_RISK, @IATA_UN_ID_NUMBER, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(record.IATA_CARGO_PACKING));
                insertCmd.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(record.IATA_HAZARD_CLASS));
                insertCmd.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(record.IATA_HAZARD_LABEL));
                insertCmd.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(record.IATA_PACK_GROUP));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(record.IATA_PASS_AIR_PACK_LMT_INSTR));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(record.IATA_PASS_AIR_PACK_LMT_PER_PKG));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(record.IATA_PASS_AIR_PACK_NOTE));
                insertCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(record.IATA_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(record.IATA_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(record.IATA_CARGO_PACK_MAX_QTY));
                insertCmd.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(record.IATA_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(record.IATA_PASS_AIR_MAX_QTY));
                insertCmd.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(record.IATA_SPECIAL_PROV));
                insertCmd.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(record.IATA_SUBSIDIARY_RISK));
                insertCmd.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(record.IATA_UN_ID_NUMBER));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("IATA_PSN Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.IATA_HAZARD_CLASS, record.IATA_PSN_CODE, record.IATA_UN_ID_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting IATA_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.IATA_HAZARD_CLASS, record.IATA_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS IMO PSN
        private long insertMSDSIMO_PSNRecord(long msds_id, msds_imo_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_imo_psn (IMO_EMS_NO, IMO_HAZARD_CLASS, " +
                "IMO_IBC_INSTR, IMO_LIMITED_QTY, IMO_PACK_GROUP, IMO_PACK_INSTRUCTIONS, " +
                "IMO_PACK_PROVISIONS, IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER, " +
                "IMO_PSN_CODE, IMO_SPECIAL_PROV, IMO_STOW_SEGR, " +
                "IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO, IMO_TANK_INSTR_PROV, " +
                "IMO_TANK_INSTR_UN, IMO_UN_NUMBER, IMO_IBC_PROVISIONS, msds_id) " +
                "VALUES(@IMO_EMS_NO, @IMO_HAZARD_CLASS, @IMO_IBC_INSTR, @IMO_LIMITED_QTY, " +
                "@IMO_PACK_GROUP, @IMO_PACK_INSTRUCTIONS, @IMO_PACK_PROVISIONS, " +
                "@IMO_PROP_SHIP_NAME, @IMO_PROP_SHIP_MODIFIER, @IMO_PSN_CODE, " +
                "@IMO_SPECIAL_PROV, @IMO_STOW_SEGR, @IMO_SUBSIDIARY_RISK, " +
                "@IMO_TANK_INSTR_IMO, @IMO_TANK_INSTR_PROV, @IMO_TANK_INSTR_UN, " +
                "@IMO_UN_NUMBER, @IMO_IBC_PROVISIONS, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(record.IMO_EMS_NO));
                insertCmd.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(record.IMO_HAZARD_CLASS));
                insertCmd.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(record.IMO_IBC_INSTR));
                insertCmd.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(record.IMO_LIMITED_QTY));
                insertCmd.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(record.IMO_PACK_GROUP));
                insertCmd.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(record.IMO_PACK_INSTRUCTIONS));
                insertCmd.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(record.IMO_PACK_PROVISIONS));
                insertCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(record.IMO_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(record.IMO_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(record.IMO_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(record.IMO_SPECIAL_PROV));
                insertCmd.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(record.IMO_STOW_SEGR));
                insertCmd.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(record.IMO_SUBSIDIARY_RISK));
                insertCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(record.IMO_TANK_INSTR_IMO));
                insertCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(record.IMO_TANK_INSTR_PROV));
                insertCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(record.IMO_TANK_INSTR_UN));
                insertCmd.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(record.IMO_UN_NUMBER));
                insertCmd.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(record.IMO_IBC_PROVISIONS));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("IMO_PSN Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.IMO_HAZARD_CLASS, record.IMO_PSN_CODE, record.IMO_UN_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting IMO_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.IMO_HAZARD_CLASS, record.IMO_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        // MSDS INGREDIENTS
        private long insertMSDSIngredientsListRecord(long msds_id, List<msds_ingredient> ingredientsList, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_ingredients (msds_id, CAS, RTECS_NUM, " +
                "RTECS_CODE, INGREDIENT_NAME, PRCNT, OSHA_PEL, OSHA_STEL, ACGIH_TLV, " +
                "ACGIH_STEL, EPA_REPORT_QTY, DOT_REPORT_QTY, PRCNT_VOL_VALUE, PRCNT_VOL_WEIGHT, " +
                "CHEM_MFG_COMP_NAME, ODS_IND, OTHER_REC_LIMITS) " +
                "VALUES(@msds_id, @CAS, @RTECS_NUM, @RTECS_CODE, @INGREDIENT_NAME, " +
                "@PRCNT,  @OSHA_PEL, @OSHA_STEL, @ACGIH_TLV, @ACGIH_STEL, @EPA_REPORT_QTY, " +
                "@DOT_REPORT_QTY, @PRCNT_VOL_VALUE, @PRCNT_VOL_WEIGHT,  @CHEM_MFG_COMP_NAME, " +
                "@ODS_IND, @OTHER_REC_LIMITS); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = null;

            SqlCommand insertCmd = new SqlCommand(stmt, conn);

            // Loop through the contracotr records
            foreach (msds_ingredient record in ingredientsList) {
                try {
                    tran = conn.BeginTransaction();

                    // Clear the parameters
                    insertCmd.Parameters.Clear();
                    insertCmd.Transaction = tran;

                    insertCmd.Parameters.AddWithValue("@msds_id", msds_id);
                    insertCmd.Parameters.AddWithValue("@CAS", dbNull(record.CAS));
                    insertCmd.Parameters.AddWithValue("@RTECS_NUM", dbNull(record.RTECS_NUM));
                    insertCmd.Parameters.AddWithValue("@RTECS_CODE", dbNull(record.RTECS_CODE));
                    insertCmd.Parameters.AddWithValue("@INGREDIENT_NAME", dbNull(record.INGREDIENT_NAME));
                    insertCmd.Parameters.AddWithValue("@PRCNT", dbNull(record.PRCNT));
                    insertCmd.Parameters.AddWithValue("@OSHA_PEL", dbNull(record.OSHA_PEL));
                    insertCmd.Parameters.AddWithValue("@OSHA_STEL", dbNull(record.OSHA_STEL));
                    insertCmd.Parameters.AddWithValue("@ACGIH_TLV", dbNull(record.ACGIH_TLV));
                    insertCmd.Parameters.AddWithValue("@ACGIH_STEL", dbNull(record.ACGIH_STEL));
                    insertCmd.Parameters.AddWithValue("@EPA_REPORT_QTY", dbNull(record.EPA_REPORT_QTY));
                    insertCmd.Parameters.AddWithValue("@DOT_REPORT_QTY", dbNull(record.DOT_REPORT_QTY));
                    insertCmd.Parameters.AddWithValue("@PRCNT_VOL_VALUE", dbNull(record.PRCNT_VOL_VALUE));
                    insertCmd.Parameters.AddWithValue("@PRCNT_VOL_WEIGHT", dbNull(record.PRCNT_VOL_WEIGHT));
                    insertCmd.Parameters.AddWithValue("@CHEM_MFG_COMP_NAME", dbNull(record.CHEM_MFG_COMP_NAME));
                    insertCmd.Parameters.AddWithValue("@ODS_IND", dbNull(record.ODS_IND));
                    insertCmd.Parameters.AddWithValue("@OTHER_REC_LIMITS", dbNull(record.OTHER_REC_LIMITS));

                    object o = insertCmd.ExecuteScalar();

                    result = Convert.ToInt32(o);

                    tran.Commit();

                    logger.InfoFormat("MSDS Ingredient Inserted: {0}, {1}, {2}, {3} ID = {4}",
                        msds_id, record.INGREDIENT_NAME, record.CAS, record.CHEM_MFG_COMP_NAME, result);
                }
                catch (Exception ex) {
                    logger.ErrorFormat("Error inserting MSDS Ingredient: {0}, {1}, {2} - {3}",
                        msds_id, record.INGREDIENT_NAME, record.CAS, ex.Message);
                }
            }

            return result;
        }

        //MSDS ITEM DESCRIPTION
        private long insertMSDSItemDescriptionRecord(long msds_id, msds_item_description record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_item_description (msds_id, ITEM_MANAGER, ITEM_NAME, " +
                "SPECIFICATION_NUMBER, TYPE_GRADE_CLASS, UNIT_OF_ISSUE, " +
                "QUANTITATIVE_EXPRESSION, UI_CONTAINER_QTY, TYPE_OF_CONTAINER, " +
                "BATCH_NUMBER, LOT_NUMBER, LOG_FLIS_NIIN_VER, LOG_FSC, " +
                "NET_UNIT_WEIGHT, SHELF_LIFE_CODE, SPECIAL_EMP_CODE, " +
                "UN_NA_NUMBER, UPC_GTIN) " +
                "VALUES(@msds_id, @ITEM_MANAGER, @ITEM_NAME, @SPECIFICATION_NUMBER, " +
                "@TYPE_GRADE_CLASS, @UNIT_OF_ISSUE, @QUANTITATIVE_EXPRESSION, " +
                "@UI_CONTAINER_QTY, @TYPE_OF_CONTAINER, @BATCH_NUMBER, " +
                "@LOT_NUMBER, @LOG_FLIS_NIIN_VER, @LOG_FSC, @NET_UNIT_WEIGHT, " +
                "@SHELF_LIFE_CODE, @SPECIAL_EMP_CODE, @UN_NA_NUMBER, @UPC_GTIN); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@msds_id", dbNull(msds_id));
                insertCmd.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(record.ITEM_MANAGER));
                insertCmd.Parameters.AddWithValue("@ITEM_NAME", dbNull(record.ITEM_NAME));
                insertCmd.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(record.SPECIFICATION_NUMBER));
                insertCmd.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(record.TYPE_GRADE_CLASS));
                insertCmd.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(record.UNIT_OF_ISSUE));
                insertCmd.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(record.QUANTITATIVE_EXPRESSION));
                insertCmd.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(record.UI_CONTAINER_QTY));
                insertCmd.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(record.TYPE_OF_CONTAINER));
                insertCmd.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(record.BATCH_NUMBER));
                insertCmd.Parameters.AddWithValue("@LOT_NUMBER", dbNull(record.LOT_NUMBER));
                insertCmd.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(record.LOG_FLIS_NIIN_VER));
                insertCmd.Parameters.AddWithValue("@LOG_FSC", dbNull(record.LOG_FSC));
                insertCmd.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(record.NET_UNIT_WEIGHT));
                insertCmd.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(record.SHELF_LIFE_CODE));
                insertCmd.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(record.SPECIAL_EMP_CODE));
                insertCmd.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(record.UN_NA_NUMBER));
                insertCmd.Parameters.AddWithValue("@UPC_GTIN", dbNull(record.UPC_GTIN));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("ItemDescription Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, record.UN_NA_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting ItemDescription: {0}, {1}, {2} - {3}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS LABEL INFO
        private long insertMSDSLabelInfoRecord(long msds_id, msds_label_info record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_label_info (COMPANY_CAGE_RP, " +
                "COMPANY_NAME_RP, LABEL_EMERG_PHONE, LABEL_ITEM_NAME, " +
                "LABEL_PROC_YEAR, LABEL_PROD_IDENT, LABEL_PROD_SERIALNO, " +
                "LABEL_SIGNAL_WORD, LABEL_STOCK_NO, SPECIFIC_HAZARDS, msds_id) " +
                "VALUES(@COMPANY_CAGE_RP, @COMPANY_NAME_RP, @LABEL_EMERG_PHONE, " +
                "@LABEL_ITEM_NAME, @LABEL_PROC_YEAR, @LABEL_PROD_IDENT, " +
                "@LABEL_PROD_SERIALNO, @LABEL_SIGNAL_WORD, @LABEL_STOCK_NO, " +
                "@SPECIFIC_HAZARDS, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(record.COMPANY_CAGE_RP));
                insertCmd.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(record.COMPANY_NAME_RP));
                insertCmd.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(record.LABEL_EMERG_PHONE));
                insertCmd.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(record.LABEL_ITEM_NAME));
                insertCmd.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(record.LABEL_PROC_YEAR));
                insertCmd.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(record.LABEL_PROD_IDENT));
                insertCmd.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(record.LABEL_PROD_SERIALNO));
                insertCmd.Parameters.AddWithValue("@LABEL_SIGNAL_WORD", dbNull(record.LABEL_SIGNAL_WORD));
                insertCmd.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(record.LABEL_STOCK_NO));
                insertCmd.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(record.SPECIFIC_HAZARDS));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("LabelInfo Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, record.COMPANY_NAME_RP, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting LabelInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS PHYS CHEMICAL TABLE
        private long insertMSDSPhysChemicalRecord(long msds_id, msds_phys_chemical record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_phys_chemical (msds_id, " +
                "VAPOR_PRESS, VAPOR_DENS, SPECIFIC_GRAV, " +
                "VOC_POUNDS_GALLON, VOC_GRAMS_LITER, PH, VISCOSITY, " +
                "EVAP_RATE_REF, SOL_IN_WATER, APP_ODOR, " +
                "PERCENT_VOL_VOLUME, AUTOIGNITION_TEMP, CARCINOGEN_IND, " +
                "EPA_ACUTE, EPA_CHRONIC, EPA_FIRE, EPA_PRESSURE, EPA_REACTIVITY, " +
                "FLASH_PT_TEMP, NEUT_AGENT, " +
                "NFPA_FLAMMABILITY, NFPA_HEALTH, NFPA_REACTIVITY, NFPA_SPECIAL, " +
                "OSHA_CARCINOGENS, OSHA_COMB_LIQUID, OSHA_COMP_GAS, " +
                "OSHA_CORROSIVE, OSHA_EXPLOSIVE, OSHA_FLAMMABLE, " +
                "OSHA_HIGH_TOXIC, OSHA_IRRITANT, OSHA_ORG_PEROX, " +
                "OSHA_OTHERLONGTERM, OSHA_OXIDIZER, OSHA_PYRO, " +
                "OSHA_SENSITIZER, OSHA_TOXIC, OSHA_UNST_REACT, " +
                "OTHER_SHORT_TERM, PHYS_STATE_CODE, VOL_ORG_COMP_WT, OSHA_WATER_REACTIVE) " +
                "VALUES(@msds_id, @VAPOR_PRESS, @VAPOR_DENS, @SPECIFIC_GRAV, " +
                "@VOC_POUNDS_GALLON, @VOC_GRAMS_LITER, @PH, @VISCOSITY, " +
                "@EVAP_RATE_REF, @SOL_IN_WATER, @APP_ODOR, @PERCENT_VOL_VOLUME, " +
                "@AUTOIGNITION_TEMP, @CARCINOGEN_IND, " +
                "@EPA_ACUTE, @EPA_CHRONIC, @EPA_FIRE, @EPA_PRESSURE, @EPA_REACTIVITY, " +
                "@FLASH_PT_TEMP, @NEUT_AGENT, " +
                "@NFPA_FLAMMABILITY, @NFPA_HEALTH, @NFPA_REACTIVITY, " +
                "@NFPA_SPECIAL, @OSHA_CARCINOGENS, @OSHA_COMB_LIQUID, " +
                "@OSHA_COMP_GAS, @OSHA_CORROSIVE, @OSHA_EXPLOSIVE, " +
                "@OSHA_FLAMMABLE, @OSHA_HIGH_TOXIC, @OSHA_IRRITANT, " +
                "@OSHA_ORG_PEROX, @OSHA_OTHERLONGTERM, @OSHA_OXIDIZER, " +
                "@OSHA_PYRO, @OSHA_SENSITIZER, @OSHA_TOXIC, @OSHA_UNST_REACT, " +
                "@OTHER_SHORT_TERM, @PHYS_STATE_CODE, @VOL_ORG_COMP_WT, " +
                "@OSHA_WATER_REACTIVE); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);
                insertCmd.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(record.VAPOR_PRESS));
                insertCmd.Parameters.AddWithValue("@VAPOR_DENS", dbNull(record.VAPOR_DENS));
                insertCmd.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(record.SPECIFIC_GRAV));
                insertCmd.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(record.VOC_POUNDS_GALLON));
                insertCmd.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(record.VOC_GRAMS_LITER));
                insertCmd.Parameters.AddWithValue("@PH", dbNull(record.PH));
                insertCmd.Parameters.AddWithValue("@VISCOSITY", dbNull(record.VISCOSITY));
                insertCmd.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(record.EVAP_RATE_REF));
                insertCmd.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(record.SOL_IN_WATER));
                insertCmd.Parameters.AddWithValue("@APP_ODOR", dbNull(record.APP_ODOR));
                insertCmd.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(record.PERCENT_VOL_VOLUME));
                insertCmd.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(record.AUTOIGNITION_TEMP));
                insertCmd.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(record.CARCINOGEN_IND));
                insertCmd.Parameters.AddWithValue("@EPA_ACUTE", dbNull(record.EPA_ACUTE));
                insertCmd.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(record.EPA_CHRONIC));
                insertCmd.Parameters.AddWithValue("@EPA_FIRE", dbNull(record.EPA_FIRE));
                insertCmd.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(record.EPA_PRESSURE));
                insertCmd.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(record.EPA_REACTIVITY));
                insertCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                insertCmd.Parameters.AddWithValue("@NEUT_AGENT", dbNull(record.NEUT_AGENT));
                insertCmd.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(record.NFPA_FLAMMABILITY));
                insertCmd.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(record.NFPA_HEALTH));
                insertCmd.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(record.NFPA_REACTIVITY));
                insertCmd.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(record.NFPA_SPECIAL));
                insertCmd.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(record.OSHA_CARCINOGENS));
                insertCmd.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(record.OSHA_COMB_LIQUID));
                insertCmd.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(record.OSHA_COMP_GAS));
                insertCmd.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(record.OSHA_CORROSIVE));
                insertCmd.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(record.OSHA_EXPLOSIVE));
                insertCmd.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(record.OSHA_FLAMMABLE));
                insertCmd.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(record.OSHA_HIGH_TOXIC));
                insertCmd.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(record.OSHA_IRRITANT));
                insertCmd.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(record.OSHA_ORG_PEROX));
                insertCmd.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(record.OSHA_OTHERLONGTERM));
                insertCmd.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(record.OSHA_OXIDIZER));
                insertCmd.Parameters.AddWithValue("@OSHA_PYRO", dbNull(record.OSHA_PYRO));
                insertCmd.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(record.OSHA_SENSITIZER));
                insertCmd.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(record.OSHA_TOXIC));
                insertCmd.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(record.OSHA_UNST_REACT));
                insertCmd.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(record.OTHER_SHORT_TERM));
                insertCmd.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(record.PHYS_STATE_CODE));
                insertCmd.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(record.VOL_ORG_COMP_WT));
                insertCmd.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(record.OSHA_WATER_REACTIVE));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("PhysChemical Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, record.APP_ODOR, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting PhysChemical: {0}, {1}, {2} - {3}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS RADIOLOGICAL INFO
        private long insertMSDSRadiologicalInfoRecord(long msds_id, msds_radiological_info record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_radiological_info (NRC_LP_NUM, " +
                "OPERATOR, RAD_AMOUNT_MICRO, RAD_FORM, RAD_CAS, " +
                "RAD_NAME, RAD_SYMBOL, REP_NSN, SEALED, msds_id) " +
                "VALUES(@NRC_LP_NUM, @OPERATOR, @RAD_AMOUNT_MICRO, @RAD_FORM, " +
                "@RAD_CAS, @RAD_NAME, @RAD_SYMBOL, @REP_NSN, @SEALED, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(record.NRC_LP_NUM));
                insertCmd.Parameters.AddWithValue("@OPERATOR", dbNull(record.OPERATOR));
                insertCmd.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(record.RAD_AMOUNT_MICRO));
                insertCmd.Parameters.AddWithValue("@RAD_FORM", dbNull(record.RAD_FORM));
                insertCmd.Parameters.AddWithValue("@RAD_CAS", dbNull(record.RAD_CAS));
                insertCmd.Parameters.AddWithValue("@RAD_NAME", dbNull(record.RAD_NAME));
                insertCmd.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(record.RAD_SYMBOL));
                insertCmd.Parameters.AddWithValue("@REP_NSN", dbNull(record.REP_NSN));
                insertCmd.Parameters.AddWithValue("@SEALED", dbNull(record.SEALED));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("RadiologicalInfo Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.RAD_CAS, record.RAD_SYMBOL, record.RAD_FORM, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting RadiologicalInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.RAD_CAS, record.RAD_SYMBOL, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS TRANSPORTATION
        private long insertMSDSTransportationRecord(long msds_id, msds_transportation record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_transportation (AF_MMAC_CODE, CERTIFICATE_COE, " +
                "COMPETENT_CAA, DOD_ID_CODE, DOT_EXEMPTION_NO, DOT_RQ_IND,  " +
                "EX_NO, FLASH_PT_TEMP, HCC, HIGH_EXPLOSIVE_WT, " +
                "LTD_QTY_IND, MAGNETIC_IND, MAGNETISM, MARINE_POLLUTANT_IND, " +
                "NET_EXP_QTY_DIST, NET_EXP_WEIGHT, NET_PROPELLANT_WT, " +
                "NOS_TECHNICAL_SHIPPING_NAME, TRANSPORTATION_ADDITIONAL_DATA, msds_id) " +
                "VALUES(@AF_MMAC_CODE, @CERTIFICATE_COE, @COMPETENT_CAA, @DOD_ID_CODE, " +
                "@DOT_EXEMPTION_NO, @DOT_RQ_IND, @EX_NO, @FLASH_PT_TEMP, " +
                "@HCC, @HIGH_EXPLOSIVE_WT, @LTD_QTY_IND, @MAGNETIC_IND, " +
                "@MAGNETISM, @MARINE_POLLUTANT_IND, @NET_EXP_QTY_DIST, " +
                "@NET_EXP_WEIGHT, @NET_PROPELLANT_WT, @NOS_TECHNICAL_SHIPPING_NAME, " +
                "@TRANSPORTATION_ADDITIONAL_DATA, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(record.AF_MMAC_CODE));
                insertCmd.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(record.CERTIFICATE_COE));
                insertCmd.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(record.COMPETENT_CAA));
                insertCmd.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(record.DOD_ID_CODE));
                insertCmd.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(record.DOT_EXEMPTION_NO));
                insertCmd.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(record.DOT_RQ_IND));
                insertCmd.Parameters.AddWithValue("@EX_NO", dbNull(record.EX_NO));
                insertCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                insertCmd.Parameters.AddWithValue("@HCC", dbNull(record.HCC));
                insertCmd.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(record.HIGH_EXPLOSIVE_WT));
                insertCmd.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(record.LTD_QTY_IND));
                insertCmd.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(record.MAGNETIC_IND));
                insertCmd.Parameters.AddWithValue("@MAGNETISM", dbNull(record.MAGNETISM));
                insertCmd.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(record.MARINE_POLLUTANT_IND));
                insertCmd.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(record.NET_EXP_QTY_DIST));
                insertCmd.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(record.NET_EXP_WEIGHT));
                insertCmd.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(record.NET_PROPELLANT_WT));
                insertCmd.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(record.NOS_TECHNICAL_SHIPPING_NAME));
                insertCmd.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(record.TRANSPORTATION_ADDITIONAL_DATA));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("Transportation Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, record.MARINE_POLLUTANT_IND, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting Transportation: {0}, {1}, {2} - {3}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion

        #region MSDS Update
        public long updateMSDSMasterRecord(String uic, MsdsEditViewModel record, String username) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //// Check to make sure we have the ID values - just in case
            //if (record.hcc_id == 0) {
            //    record.hcc_id = getIDFromValue(
            //        "HCC", "hcc_id", "hcc", record.HCC, conn);
            //}

            String stmt = "UPDATE msds_master SET MSDSSERNO = @MSDSSERNO, " +
                "CAGE = @CAGE, MANUFACTURER = @MANUFACTURER, PARTNO = @PARTNO, " +
                "FSC = @FSC, NIIN = @NIIN, hcc_id = @hcc_id, file_name = @file_name, " +
                "manually_entered = @manually_entered, ARTICLE_IND = @ARTICLE_IND, " +
                "DESCRIPTION = @DESCRIPTION, EMERGENCY_TEL = @EMERGENCY_TEL, " +
                "END_COMP_IND = @END_COMP_IND, END_ITEM_IND = @END_ITEM_IND, " +
                "KIT_IND = @KIT_IND, KIT_PART_IND = @KIT_PART_IND, " +
                "MANUFACTURER_MSDS_NO = @MANUFACTURER_MSDS_NO, MIXTURE_IND = @MIXTURE_IND, " +
                "PRODUCT_IDENTITY = @PRODUCT_IDENTITY, PRODUCT_LOAD_DATE = @PRODUCT_LOAD_DATE, " +
                "PRODUCT_RECORD_STATUS = @PRODUCT_RECORD_STATUS, " +
                "PRODUCT_REVISION_NO = @PRODUCT_REVISION_NO, PROPRIETARY_IND = @PROPRIETARY_IND, " +
                "PUBLISHED_IND = @PUBLISHED_IND, PURCHASED_PROD_IND = @PURCHASED_PROD_IND, " +
                "PURE_IND = @PURE_IND, RADIOACTIVE_IND = @RADIOACTIVE_IND, " +
                "SERVICE_AGENCY = @SERVICE_AGENCY, TRADE_NAME = @TRADE_NAME, " +
                "TRADE_SECRET_IND = @TRADE_SECRET_IND, PRODUCT_IND = @PRODUCT_IND, " +
                "PRODUCT_LANGUAGE = @PRODUCT_LANGUAGE, " +
                "data_source_cd = @data_source_cd, changed = @changed, changed_by = @changed_by " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();
            SqlCommand updateCmd = null;

            try {
                updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@MSDSSERNO", dbNull(record.MsdsMaster.MSDSSERNO));
                updateCmd.Parameters.AddWithValue("@CAGE", dbNull(record.MsdsMaster.CAGE));
                updateCmd.Parameters.AddWithValue("@MANUFACTURER", dbNull(record.MsdsMaster.MANUFACTURER));
                updateCmd.Parameters.AddWithValue("@PARTNO", dbNull(record.MsdsMaster.PARTNO));
                updateCmd.Parameters.AddWithValue("@FSC", dbNull(record.MsdsMaster.FSC));
                updateCmd.Parameters.AddWithValue("@NIIN", dbNull(record.MsdsMaster.NIIN));
                updateCmd.Parameters.AddWithValue("@hcc_id", dbNull(record.MsdsMaster.hcc_id));
                updateCmd.Parameters.AddWithValue("@file_name", dbNull(record.MsdsMaster.file_name));
                updateCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.MsdsMaster.manually_entered));
                updateCmd.Parameters.AddWithValue("@ARTICLE_IND", dbNull(record.MsdsMaster.ARTICLE_IND));
                updateCmd.Parameters.AddWithValue("@DESCRIPTION", dbNull(record.MsdsMaster.DESCRIPTION));
                updateCmd.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(record.MsdsMaster.EMERGENCY_TEL));
                updateCmd.Parameters.AddWithValue("@END_COMP_IND", dbNull(record.MsdsMaster.END_COMP_IND));
                updateCmd.Parameters.AddWithValue("@END_ITEM_IND", dbNull(record.MsdsMaster.END_ITEM_IND));
                updateCmd.Parameters.AddWithValue("@KIT_IND", dbNull(record.MsdsMaster.KIT_IND));
                updateCmd.Parameters.AddWithValue("@KIT_PART_IND", dbNull(record.MsdsMaster.KIT_PART_IND));
                updateCmd.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(record.MsdsMaster.MANUFACTURER_MSDS_NO));
                updateCmd.Parameters.AddWithValue("@MIXTURE_IND", dbNull(record.MsdsMaster.MIXTURE_IND));
                updateCmd.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(record.MsdsMaster.PRODUCT_IDENTITY));
                updateCmd.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(record.MsdsMaster.PRODUCT_LOAD_DATE));
                updateCmd.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(record.MsdsMaster.PRODUCT_RECORD_STATUS));
                updateCmd.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(record.MsdsMaster.PRODUCT_REVISION_NO));
                updateCmd.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(record.MsdsMaster.PROPRIETARY_IND));
                updateCmd.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(record.MsdsMaster.PUBLISHED_IND));
                updateCmd.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(record.MsdsMaster.PURCHASED_PROD_IND));
                updateCmd.Parameters.AddWithValue("@PURE_IND", dbNull(record.MsdsMaster.PURE_IND));
                updateCmd.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(record.MsdsMaster.RADIOACTIVE_IND));
                updateCmd.Parameters.AddWithValue("@SERVICE_AGENCY", dbNull(record.MsdsMaster.SERVICE_AGENCY));
                updateCmd.Parameters.AddWithValue("@TRADE_NAME", dbNull(record.MsdsMaster.TRADE_NAME));
                updateCmd.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(record.MsdsMaster.TRADE_SECRET_IND));
                updateCmd.Parameters.AddWithValue("@PRODUCT_IND", dbNull(record.MsdsMaster.PRODUCT_IND));
                updateCmd.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(record.MsdsMaster.PRODUCT_LANGUAGE));
                updateCmd.Parameters.AddWithValue("@data_source_cd", uic);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", username);
                updateCmd.Parameters.AddWithValue("@msds_id", record.MsdsMaster.msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSDSMaster Updated: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.MsdsMaster.MSDSSERNO, record.MsdsMaster.CAGE, record.MsdsMaster.PRODUCT_IDENTITY, record.MsdsMaster.msds_id);

                // Process the sub table info
                long subresult = updateMSDSSubTables(record, conn);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating MSDSMaster: {0}, {1}, {2} - {3}",
                    uic, record.MsdsMaster.MSDSSERNO, record.MsdsMaster.CAGE, ex.Message);
                throw ex;
            }
            finally {
                updateCmd.Dispose();
                conn.Close();
            }

            return result;
        }

        private long updateMSDSSubTables(MsdsEditViewModel record, SqlConnection conn) {
            long result = 0;

            // AFJM_PSN
            if (record.MsdsAfjmPsn != null) {
                updateMSDSAFJM_PSNRecord(record.MsdsMaster.msds_id, record.MsdsAfjmPsn, conn);
            }
            //MSDS CONTRACTOR LIST
            if (record.MsdsContractorList.Count > 0) {
                updateMSDSContractorListRecord(record.MsdsMaster.msds_id, record.MsdsContractorList, conn);
            }
            //MSDS DISPOSAL
            if (record.MsdsDisposal != null) {
                updateMSDSDisposalRecord(record.MsdsMaster.msds_id, record.MsdsDisposal, conn);
            }
            //MSDS DOC TYPES
            if (record.MsdsDocumentTypes != null) {
                updateMSDSDocumentTypesRecord(record.MsdsMaster.msds_id, record.MsdsDocumentTypes, conn);
            }
            //MSDS DOT PSN
            if (record.MsdsDotPsn != null) {
                updateMSDSDOT_PSNRecord(record.MsdsMaster.msds_id, record.MsdsDotPsn, conn);
            }
            //MSDS IATA PSN
            if (record.MsdsIataPsn != null) {
                updateMSDSIATA_PSNRecord(record.MsdsMaster.msds_id, record.MsdsIataPsn, conn);
            }
            //MSDS IMO PSN
            if (record.MsdsImoPsn != null) {
                updateMSDSIMO_PSNRecord(record.MsdsMaster.msds_id, record.MsdsImoPsn, conn);
            }
            // MSDS INGREDIENTS
            if (record.MsdsIngredientList.Count > 0) {
                updateMSDSIngredientsListRecord(record.MsdsMaster.msds_id, record.MsdsIngredientList, conn);
            }
            //MSDS ITEM DESCRIPTION
            if (record.MsdsItemDescription != null) {
                updateMSDSItemDescriptionRecord(record.MsdsMaster.msds_id, record.MsdsItemDescription, conn);
            }
            //MSDS LABEL INFO
            if (record.MsdsLabelInfo != null) {
                updateMSDSLabelInfoRecord(record.MsdsMaster.msds_id, record.MsdsLabelInfo, conn);
            }
            //MSDS PHYS CHEMICAL TABLE
            if (record.MsdsPhysChemical != null) {
                updateMSDSPhysChemicalRecord(record.MsdsMaster.msds_id, record.MsdsPhysChemical, conn);
            }
            //MSDS RADIOLOGICAL INFO
            if (record.MsdsRadiologicalInfo != null) {
                updateMSDSRadiologicalInfoRecord(record.MsdsMaster.msds_id, record.MsdsRadiologicalInfo, conn);
            }
            //MSDS TRANSPORTATION
            if (record.MsdsTransportation != null) {
                updateMSDSTransportationRecord(record.MsdsMaster.msds_id, record.MsdsTransportation, conn);
            }

            return result;
        }

        // AFJM_PSN
        private long updateMSDSAFJM_PSNRecord(long msds_id, msds_afjm_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_afjm_psn SET AFJM_HAZARD_CLASS = @AFJM_HAZARD_CLASS, " +
                "AFJM_PACK_PARAGRAPH = @AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP = @AFJM_PACK_GROUP, " +
                "AFJM_PROP_SHIP_NAME = @AFJM_PROP_SHIP_NAME, " +
                "AFJM_PROP_SHIP_MODIFIER = @AFJM_PROP_SHIP_MODIFIER, " +
                "AFJM_PSN_CODE = @AFJM_PSN_CODE, AFJM_SPECIAL_PROV = @AFJM_SPECIAL_PROV, " +
                "AFJM_SUBSIDIARY_RISK = @AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS = @AFJM_SYMBOLS, " +
                "AFJM_UN_ID_NUMBER = @AFJM_UN_ID_NUMBER " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(record.AFJM_HAZARD_CLASS));
                updateCmd.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(record.AFJM_PACK_PARAGRAPH));
                updateCmd.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(record.AFJM_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(record.AFJM_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(record.AFJM_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(record.AFJM_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(record.AFJM_SPECIAL_PROV));
                updateCmd.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(record.AFJM_SUBSIDIARY_RISK));
                updateCmd.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(record.AFJM_SYMBOLS));
                updateCmd.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(record.AFJM_UN_ID_NUMBER));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("AFJM_PSN Updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.AFJM_HAZARD_CLASS, record.AFJM_PSN_CODE, record.AFJM_UN_ID_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating AFJM_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.AFJM_HAZARD_CLASS, record.AFJM_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS CONTRACTOR LIST
        private long updateMSDSContractorListRecord(long msds_id, List<msds_contractor_info> contractorList,
            SqlConnection conn) {
            long result = 0;

            // Delete any existing contractor records for this msds_id
            result = deleteMSDSSubTableRecords(msds_id, "msds_contractor_info", conn);

            // Insert the new list
            result = insertMSDSContractorListRecord(msds_id, contractorList, conn);

            return result;
        }

        //MSDS DISPOSAL
        private long updateMSDSDisposalRecord(long msds_id, msds_disposal record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_disposal SET DISPOSAL_ADD_INFO = @DISPOSAL_ADD_INFO, " +
                "EPA_HAZ_WASTE_CODE = @EPA_HAZ_WASTE_CODE, " +
                "EPA_HAZ_WASTE_IND = @EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME = @EPA_HAZ_WASTE_NAME " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(record.DISPOSAL_ADD_INFO));
                updateCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(record.EPA_HAZ_WASTE_CODE));
                updateCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(record.EPA_HAZ_WASTE_IND));
                updateCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(record.EPA_HAZ_WASTE_NAME));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("Disposal updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, record.EPA_HAZ_WASTE_IND, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating Disposal: {0}, {1}, {2} - {3}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOC TYPES
        private long updateMSDSDocumentTypesRecord(long msds_id, msds_document_type record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_document_types SET " +
                "msds_translated_filename = @msds_translated_filename, " +
                "neshap_comp_filename = @neshap_comp_filename, " +
                "other_docs_filename = @other_docs_filename, " +
                "product_sheet_filename = @product_sheet_filename, " +
                "transportation_cert_filename = @transportation_cert_filename, " +
                "manufacturer_label_filename = @manufacturer_label_filename " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);
                updateCmd.Parameters.AddWithValue("@msds_translated_filename", dbNull(record.msds_translated_filename));
                updateCmd.Parameters.AddWithValue("@neshap_comp_filename", dbNull(record.neshap_comp_filename));
                updateCmd.Parameters.AddWithValue("@other_docs_filename", dbNull(record.other_docs_filename));
                updateCmd.Parameters.AddWithValue("@product_sheet_filename", dbNull(record.product_sheet_filename));
                updateCmd.Parameters.AddWithValue("@transportation_cert_filename", dbNull(record.transportation_cert_filename));
                updateCmd.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(record.manufacturer_label_filename));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("DocumentTypes updated: {0}, {1}, {2} ID = {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating DocumentTypes: {0}, {1}, {2} - {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOT PSN
        private long updateMSDSDOT_PSNRecord(long msds_id, msds_dot_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_dot_psn SET DOT_HAZARD_CLASS_DIV = @DOT_HAZARD_CLASS_DIV, " +
                "DOT_HAZARD_LABEL = @DOT_HAZARD_LABEL, DOT_MAX_CARGO = @DOT_MAX_CARGO, " +
                "DOT_MAX_PASSENGER = @DOT_MAX_PASSENGER, DOT_PACK_BULK = @DOT_PACK_BULK, " +
                "DOT_PACK_EXCEPTIONS = @DOT_PACK_EXCEPTIONS, DOT_PACK_NONBULK = @DOT_PACK_NONBULK, " +
                "DOT_PROP_SHIP_NAME = @DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER = @DOT_PROP_SHIP_MODIFIER, " +
                "DOT_PSN_CODE = @DOT_PSN_CODE, DOT_SPECIAL_PROVISION = @DOT_SPECIAL_PROVISION, " +
                "DOT_SYMBOLS = @DOT_SYMBOLS, DOT_UN_ID_NUMBER = @DOT_UN_ID_NUMBER, " +
                "DOT_WATER_OTHER_REQ = @DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW = @DOT_WATER_VESSEL_STOW " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(record.DOT_HAZARD_CLASS_DIV));
                updateCmd.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(record.DOT_HAZARD_LABEL));
                updateCmd.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(record.DOT_MAX_CARGO));
                updateCmd.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(record.DOT_MAX_PASSENGER));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(record.DOT_PACK_BULK));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(record.DOT_PACK_EXCEPTIONS));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(record.DOT_PACK_NONBULK));
                updateCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(record.DOT_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(record.DOT_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(record.DOT_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(record.DOT_SPECIAL_PROVISION));
                updateCmd.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(record.DOT_SYMBOLS));
                updateCmd.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(record.DOT_UN_ID_NUMBER));
                updateCmd.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(record.DOT_WATER_OTHER_REQ));
                updateCmd.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(record.DOT_WATER_VESSEL_STOW));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(record.DOT_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("DOT_PSN updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.DOT_HAZARD_CLASS_DIV, record.DOT_HAZARD_LABEL, record.DOT_PSN_CODE, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating DOT_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.DOT_HAZARD_CLASS_DIV, record.DOT_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS IATA PSN
        private long updateMSDSIATA_PSNRecord(long msds_id, msds_iata_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_iata_psn SET IATA_CARGO_PACKING = @IATA_CARGO_PACKING, " +
                "IATA_HAZARD_CLASS = @IATA_HAZARD_CLASS, IATA_HAZARD_LABEL = @IATA_HAZARD_LABEL, " +
                "IATA_PACK_GROUP = @IATA_PACK_GROUP, IATA_PASS_AIR_PACK_LMT_INSTR = @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                "IATA_PASS_AIR_PACK_LMT_PER_PKG = @IATA_PASS_AIR_PACK_LMT_PER_PKG,  " +
                "IATA_PASS_AIR_PACK_NOTE = @IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME = @IATA_PROP_SHIP_NAME, " +
                "IATA_PROP_SHIP_MODIFIER = @IATA_PROP_SHIP_MODIFIER, IATA_CARGO_PACK_MAX_QTY = @IATA_CARGO_PACK_MAX_QTY, " +
                "IATA_PSN_CODE = @IATA_PSN_CODE, IATA_PASS_AIR_MAX_QTY = @IATA_PASS_AIR_MAX_QTY, " +
                "IATA_SPECIAL_PROV = @IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK = @IATA_SUBSIDIARY_RISK, " +
                "IATA_UN_ID_NUMBER = @IATA_UN_ID_NUMBER " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(record.IATA_CARGO_PACKING));
                updateCmd.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(record.IATA_HAZARD_CLASS));
                updateCmd.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(record.IATA_HAZARD_LABEL));
                updateCmd.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(record.IATA_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(record.IATA_PASS_AIR_PACK_LMT_INSTR));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(record.IATA_PASS_AIR_PACK_LMT_PER_PKG));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(record.IATA_PASS_AIR_PACK_NOTE));
                updateCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(record.IATA_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(record.IATA_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(record.IATA_CARGO_PACK_MAX_QTY));
                updateCmd.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(record.IATA_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(record.IATA_PASS_AIR_MAX_QTY));
                updateCmd.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(record.IATA_SPECIAL_PROV));
                updateCmd.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(record.IATA_SUBSIDIARY_RISK));
                updateCmd.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(record.IATA_UN_ID_NUMBER));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("IATA_PSN updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.IATA_HAZARD_CLASS, record.IATA_PSN_CODE, record.IATA_UN_ID_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating IATA_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.IATA_HAZARD_CLASS, record.IATA_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS IMO PSN
        private long updateMSDSIMO_PSNRecord(long msds_id, msds_imo_psn record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_imo_psn SET IMO_EMS_NO = @IMO_EMS_NO, IMO_HAZARD_CLASS = @IMO_HAZARD_CLASS, " +
                "IMO_IBC_INSTR = @IMO_IBC_INSTR, IMO_LIMITED_QTY = @IMO_LIMITED_QTY, IMO_PACK_GROUP = @IMO_PACK_GROUP, " +
                "IMO_PACK_INSTRUCTIONS = @IMO_PACK_INSTRUCTIONS, IMO_PACK_PROVISIONS = @IMO_PACK_PROVISIONS, " +
                "IMO_PROP_SHIP_NAME = @IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER = @IMO_PROP_SHIP_MODIFIER, " +
                "IMO_PSN_CODE = @IMO_PSN_CODE, IMO_SPECIAL_PROV = @IMO_SPECIAL_PROV, IMO_STOW_SEGR = @IMO_STOW_SEGR, " +
                "IMO_SUBSIDIARY_RISK = @IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO = @IMO_TANK_INSTR_IMO, " +
                "IMO_TANK_INSTR_PROV = @IMO_TANK_INSTR_PROV, IMO_TANK_INSTR_UN = @IMO_TANK_INSTR_UN, " +
                "IMO_UN_NUMBER = @IMO_UN_NUMBER, IMO_IBC_PROVISIONS = @IMO_IBC_PROVISIONS " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(record.IMO_EMS_NO));
                updateCmd.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(record.IMO_HAZARD_CLASS));
                updateCmd.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(record.IMO_IBC_INSTR));
                updateCmd.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(record.IMO_LIMITED_QTY));
                updateCmd.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(record.IMO_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(record.IMO_PACK_INSTRUCTIONS));
                updateCmd.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(record.IMO_PACK_PROVISIONS));
                updateCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(record.IMO_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(record.IMO_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(record.IMO_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(record.IMO_SPECIAL_PROV));
                updateCmd.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(record.IMO_STOW_SEGR));
                updateCmd.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(record.IMO_SUBSIDIARY_RISK));
                updateCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(record.IMO_TANK_INSTR_IMO));
                updateCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(record.IMO_TANK_INSTR_PROV));
                updateCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(record.IMO_TANK_INSTR_UN));
                updateCmd.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(record.IMO_UN_NUMBER));
                updateCmd.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(record.IMO_IBC_PROVISIONS));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("IMO_PSN updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.IMO_HAZARD_CLASS, record.IMO_PSN_CODE, record.IMO_UN_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating IMO_PSN: {0}, {1}, {2} - {3}",
                    msds_id, record.IMO_HAZARD_CLASS, record.IMO_PSN_CODE, ex.Message);
                throw ex;
            }

            return result;
        }

        // MSDS INGREDIENTS
        private long updateMSDSIngredientsListRecord(long msds_id, List<msds_ingredient> ingredientsList, SqlConnection conn) {
            long result = 0;

            // Delete any existing ingredients for this msds_id
            result = deleteMSDSSubTableRecords(msds_id, "msds_ingredients", conn);

            // Insert the new list
            result = insertMSDSIngredientsListRecord(msds_id, ingredientsList, conn);

            return result;
        }

        //MSDS ITEM DESCRIPTION
        private long updateMSDSItemDescriptionRecord(long msds_id, msds_item_description record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_item_description SET ITEM_MANAGER = @ITEM_MANAGER, ITEM_NAME = @ITEM_NAME, " +
                "SPECIFICATION_NUMBER = @SPECIFICATION_NUMBER, TYPE_GRADE_CLASS = @TYPE_GRADE_CLASS, " +
                "UNIT_OF_ISSUE = @UNIT_OF_ISSUE, QUANTITATIVE_EXPRESSION = @QUANTITATIVE_EXPRESSION, " +
                "UI_CONTAINER_QTY = @UI_CONTAINER_QTY, TYPE_OF_CONTAINER = @TYPE_OF_CONTAINER, " +
                "BATCH_NUMBER = @BATCH_NUMBER, LOT_NUMBER = @LOT_NUMBER, " +
                "LOG_FLIS_NIIN_VER = @LOG_FLIS_NIIN_VER, LOG_FSC = @LOG_FSC, " +
                "NET_UNIT_WEIGHT = @NET_UNIT_WEIGHT, SHELF_LIFE_CODE = @SHELF_LIFE_CODE, " +
                "SPECIAL_EMP_CODE = @SPECIAL_EMP_CODE, UN_NA_NUMBER = @UN_NA_NUMBER, UPC_GTIN = @UPC_GTIN " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@msds_id", dbNull(msds_id));
                updateCmd.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(record.ITEM_MANAGER));
                updateCmd.Parameters.AddWithValue("@ITEM_NAME", dbNull(record.ITEM_NAME));
                updateCmd.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(record.SPECIFICATION_NUMBER));
                updateCmd.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(record.TYPE_GRADE_CLASS));
                updateCmd.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(record.UNIT_OF_ISSUE));
                updateCmd.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(record.QUANTITATIVE_EXPRESSION));
                updateCmd.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(record.UI_CONTAINER_QTY));
                updateCmd.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(record.TYPE_OF_CONTAINER));
                updateCmd.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(record.BATCH_NUMBER));
                updateCmd.Parameters.AddWithValue("@LOT_NUMBER", dbNull(record.LOT_NUMBER));
                updateCmd.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(record.LOG_FLIS_NIIN_VER));
                updateCmd.Parameters.AddWithValue("@LOG_FSC", dbNull(record.LOG_FSC));
                updateCmd.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(record.NET_UNIT_WEIGHT));
                updateCmd.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(record.SHELF_LIFE_CODE));
                updateCmd.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(record.SPECIAL_EMP_CODE));
                updateCmd.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(record.UN_NA_NUMBER));
                updateCmd.Parameters.AddWithValue("@UPC_GTIN", dbNull(record.UPC_GTIN));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("ItemDescription updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, record.UN_NA_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating ItemDescription: {0}, {1}, {2} - {3}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS LABEL INFO
        private long updateMSDSLabelInfoRecord(long msds_id, msds_label_info record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_label_info SET COMPANY_CAGE_RP = @COMPANY_CAGE_RP, " +
                "COMPANY_NAME_RP = @COMPANY_NAME_RP, LABEL_EMERG_PHONE = @LABEL_EMERG_PHONE, " +
                "LABEL_ITEM_NAME = @LABEL_ITEM_NAME, LABEL_PROC_YEAR = @LABEL_PROC_YEAR, " +
                "LABEL_PROD_IDENT = @LABEL_PROD_IDENT, LABEL_PROD_SERIALNO = @LABEL_PROD_SERIALNO, " +
                "LABEL_SIGNAL_WORD = @LABEL_SIGNAL_WORD, LABEL_STOCK_NO = @LABEL_STOCK_NO, " +
                "SPECIFIC_HAZARDS = @SPECIFIC_HAZARDS " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(record.COMPANY_CAGE_RP));
                updateCmd.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(record.COMPANY_NAME_RP));
                updateCmd.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(record.LABEL_EMERG_PHONE));
                updateCmd.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(record.LABEL_ITEM_NAME));
                updateCmd.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(record.LABEL_PROC_YEAR));
                updateCmd.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(record.LABEL_PROD_IDENT));
                updateCmd.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(record.LABEL_PROD_SERIALNO));
                updateCmd.Parameters.AddWithValue("@LABEL_SIGNAL_WORD", dbNull(record.LABEL_SIGNAL_WORD));
                updateCmd.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(record.LABEL_STOCK_NO));
                updateCmd.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(record.SPECIFIC_HAZARDS));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();


                tran.Commit();

                logger.InfoFormat("LabelInfo updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, record.COMPANY_NAME_RP, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating LabelInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS PHYS CHEMICAL TABLE
        private long updateMSDSPhysChemicalRecord(long msds_id, msds_phys_chemical record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_phys_chemical SET " +
                "VAPOR_PRESS = @VAPOR_PRESS, VAPOR_DENS = @VAPOR_DENS, SPECIFIC_GRAV = @SPECIFIC_GRAV, " +
                "VOC_POUNDS_GALLON = @VOC_POUNDS_GALLON, VOC_GRAMS_LITER = @VOC_GRAMS_LITER, PH = @PH, VISCOSITY = @VISCOSITY, " +
                "EVAP_RATE_REF = @EVAP_RATE_REF, SOL_IN_WATER = @SOL_IN_WATER, APP_ODOR = @APP_ODOR, " +
                "PERCENT_VOL_VOLUME = @PERCENT_VOL_VOLUME, AUTOIGNITION_TEMP = @AUTOIGNITION_TEMP, CARCINOGEN_IND = @CARCINOGEN_IND, " +
                "EPA_ACUTE = @EPA_ACUTE, EPA_CHRONIC = @EPA_CHRONIC, EPA_FIRE = @EPA_FIRE, EPA_PRESSURE = @EPA_PRESSURE, " +
                "EPA_REACTIVITY = @EPA_REACTIVITY, FLASH_PT_TEMP = @FLASH_PT_TEMP, NEUT_AGENT = @NEUT_AGENT, " +
                "NFPA_FLAMMABILITY = @NFPA_FLAMMABILITY, NFPA_HEALTH = @NFPA_HEALTH, NFPA_REACTIVITY = @NFPA_REACTIVITY, " +
                "NFPA_SPECIAL = @NFPA_SPECIAL, " +
                "OSHA_CARCINOGENS = @OSHA_CARCINOGENS, OSHA_COMB_LIQUID = @OSHA_COMB_LIQUID, OSHA_COMP_GAS = @OSHA_COMP_GAS, " +
                "OSHA_CORROSIVE = @OSHA_CORROSIVE, OSHA_EXPLOSIVE = @OSHA_EXPLOSIVE, OSHA_FLAMMABLE = @OSHA_FLAMMABLE, " +
                "OSHA_HIGH_TOXIC = @OSHA_HIGH_TOXIC, OSHA_IRRITANT = @OSHA_IRRITANT, OSHA_ORG_PEROX = @OSHA_ORG_PEROX, " +
                "OSHA_OTHERLONGTERM = @OSHA_OTHERLONGTERM, OSHA_OXIDIZER = @OSHA_OXIDIZER, OSHA_PYRO = @OSHA_PYRO, " +
                "OSHA_SENSITIZER = @OSHA_SENSITIZER, OSHA_TOXIC = @OSHA_TOXIC, OSHA_UNST_REACT = @OSHA_UNST_REACT, " +
                "OTHER_SHORT_TERM = @OTHER_SHORT_TERM, PHYS_STATE_CODE = @PHYS_STATE_CODE, VOL_ORG_COMP_WT = @VOL_ORG_COMP_WT, " +
                "OSHA_WATER_REACTIVE = @OSHA_WATER_REACTIVE " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);
                updateCmd.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(record.VAPOR_PRESS));
                updateCmd.Parameters.AddWithValue("@VAPOR_DENS", dbNull(record.VAPOR_DENS));
                updateCmd.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(record.SPECIFIC_GRAV));
                updateCmd.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(record.VOC_POUNDS_GALLON));
                updateCmd.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(record.VOC_GRAMS_LITER));
                updateCmd.Parameters.AddWithValue("@PH", dbNull(record.PH));
                updateCmd.Parameters.AddWithValue("@VISCOSITY", dbNull(record.VISCOSITY));
                updateCmd.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(record.EVAP_RATE_REF));
                updateCmd.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(record.SOL_IN_WATER));
                updateCmd.Parameters.AddWithValue("@APP_ODOR", dbNull(record.APP_ODOR));
                updateCmd.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(record.PERCENT_VOL_VOLUME));
                updateCmd.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(record.AUTOIGNITION_TEMP));
                updateCmd.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(record.CARCINOGEN_IND));
                updateCmd.Parameters.AddWithValue("@EPA_ACUTE", dbNull(record.EPA_ACUTE));
                updateCmd.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(record.EPA_CHRONIC));
                updateCmd.Parameters.AddWithValue("@EPA_FIRE", dbNull(record.EPA_FIRE));
                updateCmd.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(record.EPA_PRESSURE));
                updateCmd.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(record.EPA_REACTIVITY));
                updateCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                updateCmd.Parameters.AddWithValue("@NEUT_AGENT", dbNull(record.NEUT_AGENT));
                updateCmd.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(record.NFPA_FLAMMABILITY));
                updateCmd.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(record.NFPA_HEALTH));
                updateCmd.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(record.NFPA_REACTIVITY));
                updateCmd.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(record.NFPA_SPECIAL));
                updateCmd.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(record.OSHA_CARCINOGENS));
                updateCmd.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(record.OSHA_COMB_LIQUID));
                updateCmd.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(record.OSHA_COMP_GAS));
                updateCmd.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(record.OSHA_CORROSIVE));
                updateCmd.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(record.OSHA_EXPLOSIVE));
                updateCmd.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(record.OSHA_FLAMMABLE));
                updateCmd.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(record.OSHA_HIGH_TOXIC));
                updateCmd.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(record.OSHA_IRRITANT));
                updateCmd.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(record.OSHA_ORG_PEROX));
                updateCmd.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(record.OSHA_OTHERLONGTERM));
                updateCmd.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(record.OSHA_OXIDIZER));
                updateCmd.Parameters.AddWithValue("@OSHA_PYRO", dbNull(record.OSHA_PYRO));
                updateCmd.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(record.OSHA_SENSITIZER));
                updateCmd.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(record.OSHA_TOXIC));
                updateCmd.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(record.OSHA_UNST_REACT));
                updateCmd.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(record.OTHER_SHORT_TERM));
                updateCmd.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(record.PHYS_STATE_CODE));
                updateCmd.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(record.VOL_ORG_COMP_WT));
                updateCmd.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(record.OSHA_WATER_REACTIVE));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("PhysChemical updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, record.APP_ODOR, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating PhysChemical: {0}, {1}, {2} - {3}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS RADIOLOGICAL INFO
        private long updateMSDSRadiologicalInfoRecord(long msds_id, msds_radiological_info record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_radiological_info SET NRC_LP_NUM = @NRC_LP_NUM, " +
                "OPERATOR = @OPERATOR, RAD_AMOUNT_MICRO = @RAD_AMOUNT_MICRO, RAD_FORM = @RAD_FORM, RAD_CAS = @RAD_CAS, " +
                "RAD_NAME = @RAD_NAME, RAD_SYMBOL = @RAD_SYMBOL, REP_NSN = @REP_NSN, SEALED = @SEALED " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(record.NRC_LP_NUM));
                updateCmd.Parameters.AddWithValue("@OPERATOR", dbNull(record.OPERATOR));
                updateCmd.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(record.RAD_AMOUNT_MICRO));
                updateCmd.Parameters.AddWithValue("@RAD_FORM", dbNull(record.RAD_FORM));
                updateCmd.Parameters.AddWithValue("@RAD_CAS", dbNull(record.RAD_CAS));
                updateCmd.Parameters.AddWithValue("@RAD_NAME", dbNull(record.RAD_NAME));
                updateCmd.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(record.RAD_SYMBOL));
                updateCmd.Parameters.AddWithValue("@REP_NSN", dbNull(record.REP_NSN));
                updateCmd.Parameters.AddWithValue("@SEALED", dbNull(record.SEALED));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("RadiologicalInfo updateed: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.RAD_CAS, record.RAD_SYMBOL, record.RAD_FORM, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updateing RadiologicalInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.RAD_CAS, record.RAD_SYMBOL, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS TRANSPORTATION
        private long updateMSDSTransportationRecord(long msds_id, msds_transportation record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_transportation SET AF_MMAC_CODE = @AF_MMAC_CODE, CERTIFICATE_COE = @CERTIFICATE_COE, " +
                "COMPETENT_CAA = @COMPETENT_CAA, DOD_ID_CODE = @DOD_ID_CODE, DOT_EXEMPTION_NO = @DOT_EXEMPTION_NO, " +
                "DOT_RQ_IND = @DOT_RQ_IND, EX_NO = @EX_NO, FLASH_PT_TEMP = @FLASH_PT_TEMP, HCC = @HCC, " +
                "HIGH_EXPLOSIVE_WT = @HIGH_EXPLOSIVE_WT, LTD_QTY_IND = @LTD_QTY_IND, " +
                "MAGNETIC_IND = @MAGNETIC_IND, MAGNETISM = @MAGNETISM, MARINE_POLLUTANT_IND = @MARINE_POLLUTANT_IND, " +
                "NET_EXP_QTY_DIST = @NET_EXP_QTY_DIST, NET_EXP_WEIGHT = @NET_EXP_WEIGHT, NET_PROPELLANT_WT = @NET_PROPELLANT_WT, " +
                "NOS_TECHNICAL_SHIPPING_NAME = @NOS_TECHNICAL_SHIPPING_NAME, " +
                "TRANSPORTATION_ADDITIONAL_DATA = @TRANSPORTATION_ADDITIONAL_DATA " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(record.AF_MMAC_CODE));
                updateCmd.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(record.CERTIFICATE_COE));
                updateCmd.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(record.COMPETENT_CAA));
                updateCmd.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(record.DOD_ID_CODE));
                updateCmd.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(record.DOT_EXEMPTION_NO));
                updateCmd.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(record.DOT_RQ_IND));
                updateCmd.Parameters.AddWithValue("@EX_NO", dbNull(record.EX_NO));
                updateCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                updateCmd.Parameters.AddWithValue("@HCC", dbNull(record.HCC));
                updateCmd.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(record.HIGH_EXPLOSIVE_WT));
                updateCmd.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(record.LTD_QTY_IND));
                updateCmd.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(record.MAGNETIC_IND));
                updateCmd.Parameters.AddWithValue("@MAGNETISM", dbNull(record.MAGNETISM));
                updateCmd.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(record.MARINE_POLLUTANT_IND));
                updateCmd.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(record.NET_EXP_QTY_DIST));
                updateCmd.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(record.NET_EXP_WEIGHT));
                updateCmd.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(record.NET_PROPELLANT_WT));
                updateCmd.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(record.NOS_TECHNICAL_SHIPPING_NAME));
                updateCmd.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(record.TRANSPORTATION_ADDITIONAL_DATA));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("Transportation updated: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, record.MARINE_POLLUTANT_IND, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating Transportation: {0}, {1}, {2} - {3}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion

        #region MSDS Deletes
        public long deleteMSDSMasterRecord(String uic, MsdsEditViewModel record, String userName) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            String stmt = "DELETE FROM msds_master WHERE msds_id = @msds_id";

            SqlTransaction tran = null;

            SqlCommand deleteCmd = new SqlCommand(stmt, conn);

            try {

                // Subtable data will be deleted automatically from FK CASCADE

                tran = conn.BeginTransaction();

                deleteCmd.Transaction = tran;

                deleteCmd.Parameters.AddWithValue("@msds_id", record.MsdsMaster.msds_id);

                result = deleteCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSDS Record deleted: {0} ID = {1}",
                    result, record.MsdsMaster.msds_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleting MSDS Record: {0} - {1}",
                    record.MsdsMaster.msds_id, ex.Message);
            }
            finally {
                deleteCmd.Dispose();
                conn.Close();
            }

            return result;
        }

        private long deleteMSDSSubTables(MsdsEditViewModel record, SqlConnection conn) {
            long result = 0;

            // AFJM_PSN
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_afjm_psn", conn);

            //MSDS CONTRACTOR LIST
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_contractor_info", conn);

            //MSDS DISPOSAL
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_disposal", conn);

            //MSDS DOC TYPES
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_document_types", conn);

            //MSDS DOT PSN
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_dot_psn", conn);

            //MSDS IATA PSN
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_iata_psn", conn);

            //MSDS IMO PSN
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_imo_psn", conn);

            // MSDS INGREDIENTS
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_ingredients", conn);

            //MSDS ITEM DESCRIPTION
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_item_description", conn);

            //MSDS LABEL INFO
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_label_info", conn);

            //MSDS PHYS CHEMICAL TABLE
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_phys_chemical", conn);

            //MSDS RADIOLOGICAL INFO
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_radiological_info", conn);

            //MSDS TRANSPORTATION
            result = deleteMSDSSubTableRecords(record.MsdsMaster.msds_id, "msds_transportation", conn);

            return result;
        }

        private long deleteMSDSSubTableRecords(long msds_id, String tableName, SqlConnection conn) {
            long result = 0;

            String stmt = "DELETE FROM " + tableName + " WHERE msds_id = @msds_id";

            SqlTransaction tran = null;

            SqlCommand deleteCmd = new SqlCommand(stmt, conn);

            try {
                tran = conn.BeginTransaction();

                deleteCmd.Transaction = tran;

                deleteCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = deleteCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSDS {0} data deleted: {1} ID = {2}",
                    tableName, result, msds_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleting MSDS {0} data: {1} - {2}",
                    tableName, msds_id, ex.Message);
            }

            return result;
        }
        #endregion
    #endregion

    #region File Tables
        public bool updateFileTable(FILE_TABLES fileTable, String uic, String fileName, String fileType,
            DateTime fileDate, DateTime processedDate, String username, 
            int records_added, int records_changed = 0, int records_deleted = 0) {
            bool result = false;
            string sqlStmt = "INSERT INTO file_uploads (" +
                "file_name, file_type, file_date, upload_date, uploaded_by, uic, " +
                "records_added, records_changed, records_deleted) " +
                "VALUES(@file_name, @file_type, @file_date, @process_date, @processed_by, " +
                "@uic , @records_added, @records_changed, @records_deleted)";
            
            if (fileTable == FILE_TABLES.FileDownLoad)
                sqlStmt = "INSERT INTO file_downloads (" +
                    "file_name, file_type, file_date, download_date, downloaded_by, " +
                    "uic, records_added) " +
                    "VALUES(@file_name, @file_type, @file_date, @process_date, @processed_by, " +
                    "@uic, @records_added)";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@file_name", fileName);
                insertCmd.Parameters.AddWithValue("@file_type", fileType);
                insertCmd.Parameters.AddWithValue("@file_date", fileDate);
                insertCmd.Parameters.AddWithValue("@process_date", processedDate);
                insertCmd.Parameters.AddWithValue("@processed_by", username);
                insertCmd.Parameters.AddWithValue("@uic", uic);
                insertCmd.Parameters.AddWithValue("@records_added", records_added);
                insertCmd.Parameters.AddWithValue("@records_changed", records_changed);
                insertCmd.Parameters.AddWithValue("@records_deleted", records_deleted);

                insertCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("FileRecord Inserted to: {0} - {1}, {2}, {3}, {4}",
                    fileTable.ToString(), uic, fileType, fileName, fileDate);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting FileRecord to: {0} - {1}, {2}, {3}, {4} - {5}",
                    fileTable.ToString(), uic, fileType, fileName, fileDate, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region Utilities
        private long getIDFromValue(String tableName, String idField,
            String descField, String value, SqlConnection conn) {
            long result = 0;

            String stmt = "SELECT " + idField + " FROM " + tableName + " WHERE " + descField + " = @value";
            SqlCommand cmd = null;

            try {
                cmd = new SqlCommand(stmt, conn);
                cmd.Parameters.AddWithValue("@value", value);

                Object o = cmd.ExecuteScalar();

                result = (Int64)o;
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error in getIDFromDescription: {0}, {1}, - {2}",
                    tableName, value, ex.Message);
            }
            finally {
                cmd.Dispose();
            }

            return result;
        }

        public void checkForUpdates() {
            DatabaseUpdater databaseUpdater = new DatabaseUpdater();
            SqlConnection connection = null;

            try {
                if (MDF_CONNECTION != null) {
                    connection = new SqlConnection(MDF_CONNECTION);

                    databaseUpdater.update(connection);
                }
                else
                    logger.Error("No HAZMAT ConnectionString");
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error occured while applying database updates - {0}",
                    ex.Message + ex.StackTrace);
            }
            finally {
                connection.Close();
            }

            // Make sure we have backup directory
            checkForBackupFolder();
        }

        private void checkForBackupFolder() {
            try {
                // See if the backup folder exists
                if (!Directory.Exists(DB_BACKUP_DIRECTORY)) {
                    // Create the folder
                    Directory.CreateDirectory(DB_BACKUP_DIRECTORY);
                    logger.InfoFormat("Created backup directory - {0}", DB_BACKUP_DIRECTORY);
                }
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error creating backup directory - {0}", ex.Message);
            }
        }

        private static object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            }
            else {
                return DBNull.Value;
            }
        }

    #endregion
    }
}
