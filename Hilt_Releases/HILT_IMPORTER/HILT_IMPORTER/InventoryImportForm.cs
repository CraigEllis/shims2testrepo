﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HILT_IMPORTER {
    public partial class InventoryImportForm : HILT_IMPORTER.BaseImportForm {
        public InventoryImportForm() {
            InitializeComponent();

            // Set the label
            this.Title = "Inventory Import";
        }

        protected override void processFileButton_Click(object sender, EventArgs e) {
            // Display the directory dialog to select the directory containing the MSDS CD image
        }

        protected override void processImportButton_Click(object sender, EventArgs e) {
            MessageBox.Show("Please implement the processSaveButton_Click method in your child form");
        }
    }
}
