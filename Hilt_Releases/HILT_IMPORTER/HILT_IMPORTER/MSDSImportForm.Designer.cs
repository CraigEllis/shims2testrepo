﻿namespace HILT_IMPORTER {
    partial class MSDSImportForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.msdsBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.processAllCheckBox = new System.Windows.Forms.CheckBox();
            this.SetupButton = new System.Windows.Forms.Button();
            this.smclTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.smclButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileTextBox
            // 
            this.fileTextBox.ReadOnly = true;
            this.fileTextBox.TabIndex = 1;
            // 
            // messageRTBox
            // 
            this.messageRTBox.BackColor = System.Drawing.SystemColors.Menu;
            this.messageRTBox.Location = new System.Drawing.Point(16, 136);
            this.messageRTBox.ReadOnly = true;
            this.messageRTBox.Size = new System.Drawing.Size(640, 249);
            this.messageRTBox.TabIndex = 9;
            // 
            // fileButton
            // 
            this.fileButton.TabIndex = 2;
            // 
            // msdsBackgroundWorker
            // 
            this.msdsBackgroundWorker.WorkerReportsProgress = true;
            this.msdsBackgroundWorker.WorkerSupportsCancellation = true;
            this.msdsBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.msdsBackgroundWorker_DoWork);
            this.msdsBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.msdsBackgroundWorker_ProgressChanged);
            this.msdsBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.msdsBackgroundWorker_RunWorkerCompleted);
            // 
            // processAllCheckBox
            // 
            this.processAllCheckBox.AutoSize = true;
            this.processAllCheckBox.Location = new System.Drawing.Point(93, 113);
            this.processAllCheckBox.Name = "processAllCheckBox";
            this.processAllCheckBox.Size = new System.Drawing.Size(155, 17);
            this.processAllCheckBox.TabIndex = 5;
            this.processAllCheckBox.Text = "Reprocess All HMIRS Data";
            this.processAllCheckBox.UseVisualStyleBackColor = true;
            this.processAllCheckBox.CheckedChanged += new System.EventHandler(this.processAllCheckBox_CheckedChanged);
            // 
            // SetupButton
            // 
            this.SetupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SetupButton.Location = new System.Drawing.Point(16, 397);
            this.SetupButton.Name = "SetupButton";
            this.SetupButton.Size = new System.Drawing.Size(75, 23);
            this.SetupButton.TabIndex = 8;
            this.SetupButton.Text = "Setup";
            this.SetupButton.UseVisualStyleBackColor = true;
            this.SetupButton.Click += new System.EventHandler(this.SetupButton_Click);
            // 
            // smclTextBox
            // 
            this.smclTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
|               System.Windows.Forms.AnchorStyles.Right)));
            this.smclTextBox.Location = new System.Drawing.Point(94, 87);
            this.smclTextBox.Name = "smclTextBox";
            this.smclTextBox.ReadOnly = true;
            this.smclTextBox.Size = new System.Drawing.Size(481, 20);
            this.smclTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "SMCL (.XLS):";
            // 
            // smclButton
            // 
            this.smclButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.smclButton.Location = new System.Drawing.Point(581, 85);
            this.smclButton.Name = "smclButton";
            this.smclButton.Size = new System.Drawing.Size(75, 23);
            this.smclButton.TabIndex = 4;
            this.smclButton.Text = "Browse";
            this.smclButton.UseVisualStyleBackColor = true;
            this.smclButton.Click += new System.EventHandler(this.smclButton_Click);
            // 
            // MSDSImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(668, 432);
            this.Controls.Add(this.smclButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.smclTextBox);
            this.Controls.Add(this.SetupButton);
            this.Controls.Add(this.processAllCheckBox);
            this.Name = "MSDSImportForm";
            this.Text = "MSDS Import";
            this.Load += new System.EventHandler(this.MSDSImportForm_Load);
            this.Shown += new System.EventHandler(this.MSDSImportForm_Shown);
            this.Controls.SetChildIndex(this.processAllCheckBox, 0);
            this.Controls.SetChildIndex(this.fileLlabel, 0);
            this.Controls.SetChildIndex(this.fileButton, 0);
            this.Controls.SetChildIndex(this.importButton, 0);
            this.Controls.SetChildIndex(this.cancelButton, 0);
            this.Controls.SetChildIndex(this.fileTextBox, 0);
            this.Controls.SetChildIndex(this.messageRTBox, 0);
            this.Controls.SetChildIndex(this.SetupButton, 0);
            this.Controls.SetChildIndex(this.smclTextBox, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.smclButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker msdsBackgroundWorker;
        private System.Windows.Forms.CheckBox processAllCheckBox;
        private System.Windows.Forms.Button SetupButton;
        private System.Windows.Forms.TextBox smclTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button smclButton;
    }
}
