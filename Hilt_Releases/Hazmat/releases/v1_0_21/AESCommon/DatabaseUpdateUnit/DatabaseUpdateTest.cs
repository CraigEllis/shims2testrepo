﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
// Required imports
using System.Data;
using System.Data.SqlClient;
using AESCommon.DatabaseUpdate;

namespace DatabaseUpdateUnit {
    [TestFixture]
    public class DatabaseUpdateTest {
        // Global objects
        TestDatabaseUpdater oTest = new TestDatabaseUpdater();
        // SQL Server connection
        SqlConnection connection = null;
        String connectionString = "Data Source=.\\sqlexpress;Initial Catalog=databaseupdatetest;Integrated Security=True;";

        [SetUp]
        public void SetUp() {
            // Create the SQL Server connection object
            connection = new SqlConnection(connectionString);
            int result = 0;

            // Reset the test database
            connection.Open();

            SqlCommand command = new SqlCommand("ResetDatabase", connection);
            result = command.ExecuteNonQuery();

            connection.Close();

            Console.WriteLine("Setup - result: " + result);
            Assert.AreEqual(result, 2);
            Console.WriteLine("**** Setup complete ****");            
        }

        [TearDown]
        public void TearDown() {
        }

        [Test]
        public void AppDBVersionTest() {
            Int32 value = 20110902;
            Console.WriteLine("AppDBVersionTest - expect: " + oTest.AppDBVersion);
            Assert.AreEqual(value, oTest.AppDBVersion);
        }

        [Test]
        public void checkDatabaseStatusTest_1() {
            DatabaseStatus status;
            Int32 value = 20110830;

            Console.WriteLine("**** checkDatabaseStatusTest_1 started");
            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            // Call the test
            status = oTest.checkDatabaseStatus(connection);

            Assert.AreEqual(DatabaseStatus.DBStatus.NeedsUpdate, status.Status);
            Assert.AreEqual(value, status.Version);
            Console.WriteLine("**** checkDatabaseStatusTest_1 complete ****");
        }

        [Test]
        public void checkDatabaseStatusTest_2() {
            DatabaseStatus status;
            Int32 value = 0;
            int result = 0;

            Console.WriteLine("**** checkDatabaseStatusTest_2 started");
            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            // Nuke the database
            SqlCommand command = new SqlCommand("ClearDatabase", connection);
            result = command.ExecuteNonQuery();

            status = oTest.checkDatabaseStatus(connection);

            Assert.AreEqual(DatabaseStatus.DBStatus.NeedsUpdate, status.Status);
            Assert.AreEqual(value, status.Version);

            // Reset the database
            command = new SqlCommand("ResetDatabase", connection);
            result = command.ExecuteNonQuery();

            connection.Close();
            Console.WriteLine("**** checkDatabaseStatusTest_2 complete ****");
        }

        [Test]
        public void setDatabaseVersionTest() {
            DatabaseStatus status;
            Int32 value = 20100101;
            int result = 0;

            Console.WriteLine("**** setDatabaseVersionTest started");
            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            result = oTest.updateDBVersion(value, connection);
            Console.WriteLine("\tupdate complete");

            Assert.AreEqual(1, result);

            status = oTest.checkDatabaseStatus(connection);
            Assert.AreEqual(value, status.Version);

            connection.Close();
            Console.WriteLine("**** checkDatabaseStatusTest_2 complete ****");
        }

        [Test]
        public void updateTest() {
            Console.WriteLine("**** updateTest started ****");
            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            oTest.update(connection);

            // Check the results
            Console.WriteLine("**** updateTest complete ****");
        }
    }
}
