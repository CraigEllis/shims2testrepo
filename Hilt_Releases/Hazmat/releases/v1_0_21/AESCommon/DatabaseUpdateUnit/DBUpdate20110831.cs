﻿using System;
using System.Collections.Generic;
using System.Text;
// Required imports
using System.Data;
using System.Data.SqlClient;
using AESCommon.DatabaseUpdate;

namespace DatabaseUpdateUnit {
    /// <summary>
    /// Adds a new table to the database
    /// Used to test DatabaseUpdate package
    /// </summary>
    class DBUpdate20110831 : IDatabaseUpdate {
        public int AppDBVersion {
            get {
                return 20110831;
            }
        }

        public int update(IDbConnection connection) {
            SqlCommand command = ((SqlConnection) connection).CreateCommand();

            // Create the TestTemp Table
            command.CommandText = "CREATE TABLE TestTemp (" +
                    "TempValue int DEFAULT 0," +
                    "TempName varchar(50) NULL" +
                    ")";

            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            int result = command.ExecuteNonQuery();

            return result;
        }
    }
}
