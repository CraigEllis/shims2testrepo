﻿using System;
using System.Data;
using System.Diagnostics;

namespace AESCommon.DatabaseUpdate
{
    /// <summary>
    /// Abstract class an individual application overrides to provide database 
    /// update capabilities for the application
    /// 
    /// </summary>
    public abstract class DatabaseUpdater
    {
        /// <summary>
        /// Application DB Version - the current version of the application data model.
        /// This is usually the date changes were made to the database.
        /// e.g. DBVersion = 20110822
        /// </summary>
        public abstract int AppDBVersion
        {
            get;
        }

        /// <summary>
        /// The application's DatabaseUpdate update method contains the logic for:
        /// 1. comparing the current user applicaton database DBVersion against the 
        /// various DatabaseUpdate classes to determine which updates are needed, 
        /// 2. applying the updates, and 
        /// 3. updating the user application database's DBVersion value to the latest 
        /// AppDBVersion. 
        /// </summary>
        /// <param name="connection">reference to the application database's 
        /// SQLConnection object</param>
        /// <returns></returns>
        public abstract int update(IDbConnection connection);

        /// <summary>
        /// Updates the database DB_Properties table to the current version of the
        /// application DBVersion
        /// </summary>
        /// <param name="version">the application's current DBVersion value</param>
        /// <param name="connection">reference to the application database's 
        /// SQLConnection object</param>
        /// <returns></returns>
        public int updateDBVersion(Int32 version, IDbConnection connection) {
            IDbCommand command = connection.CreateCommand();

            command.CommandText = "UPDATE DB_Properties SET DB_Version = @version";
            IDbDataParameter parm = command.CreateParameter();
            parm.ParameterName = "@version";
            parm.Value = version;
            command.Parameters.Add(parm);

            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Debug.WriteLine("\tconnection opened");
            }

            int results = command.ExecuteNonQuery();

            command.Dispose();
            connection.Close();

            return results;
        }

        /// <summary>
        /// Query the appplication database to get the current DBVersion from the
        /// DB_Properties table. 
        /// If the DB_Properties table does not exist, it is created.
        /// </summary>
        /// <param name="connection">reference to the application database's 
        /// SQLConnection object</param>
        /// <returns>DatabaseStatus object</returns>
        public DatabaseStatus checkDatabaseStatus(IDbConnection connection) {
            int currentDatabaseVersion = 0;
            DatabaseStatus status = new DatabaseStatus();

            if (connection.State != ConnectionState.Open) {
                connection.Open();
            }

            IDbCommand command = connection.CreateCommand();

            // See if the DB_Properites table exists in the application database
            command.CommandText = "SELECT count(*) FROM sysobjects " + 
                "WHERE xtype = 'u' AND name = 'DB_Properties'";

            Int32 count = (Int32) command.ExecuteScalar();

            if (count == 0) {
                // DB_Properties table doesn't exit - create it
                Debug.WriteLine("checkDatabaseStatus - creating DB_Properties table");

                command.CommandText = "CREATE TABLE [dbo].[DB_Properties] (" +
                        "DB_Version int DEFAULT 0" +
                        ")";

                int result = command.ExecuteNonQuery();
            }

            // Make sure the properties table has a record in it
            command.CommandText = "SELECT COUNT(*) FROM [dbo].[DB_Properties]";

            count = (Int32)command.ExecuteScalar();

            if (count == 0) {
                // DB_Properties record doesn't exit - insert one
                command.CommandText = "INSERT INTO DB_Properties (DB_Version) VALUES(0)";
                int result = command.ExecuteNonQuery();
            }

            // Get the existing DBVersion value
            command.CommandText = "SELECT DB_Version FROM DB_Properties";

            currentDatabaseVersion = Convert.ToInt32(command.ExecuteScalar());

            status.Version = currentDatabaseVersion;

            Debug.WriteLine("checkDatabaseStatus - version = " + currentDatabaseVersion);

            if (currentDatabaseVersion < AppDBVersion) {
                status.Status = DatabaseStatus.DBStatus.NeedsUpdate;
            }
            else {
                status.Status = DatabaseStatus.DBStatus.Current;
            }

            return status;
        }
    }
}
