﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Diagnostics;
using NUnit.Framework;
using Common.Logging;
//using NLog; // This one works

using AESCommon.SafeSQL;

namespace SafeSQLUnit {
    [TestFixture]
    public class SafeSQLTest {
        ILog log = null;
        //private static Logger logger = LogManager.GetCurrentClassLogger(); // This one works
        private static ILog logger = LogManager.GetCurrentClassLogger();

        [SetUp]
        public void Setup() {
            // create properties
            //NameValueCollection properties = new NameValueCollection();
            //properties["level"] = "Debug";
            //properties["showDateTime"] = "true";

            // alternatively you can call the Log() method 
            // and pass log level as the parameter.
            logger.Fatal("Logger type: " + logger.GetType().ToString());

            // set Adapter
//            Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter(properties);

//            log = LogManager.GetLogger("ConsoleOutLoggerFactoryAdapter");

            logger.InfoFormat("***** Logger: {0}" +
                "\n\tInfo mode: {1}" +
                "\n\tInfo mode: {2}" + 
                "\n\tWarn mode: {3} *****",
                logger.ToString(), logger.IsDebugEnabled.ToString(),
                logger.IsInfoEnabled.ToString(), logger.IsWarnEnabled.ToString());
        }

        [Test]
        public void replaceSingleQuoteTest() {
            String input = "Test to make sure a single quote(') gets replace with 2 single quotes('')";
            String expect = "Test to make sure a single quote('') gets replace with 2 single quotes('''')";

            String output = SafeSQL.makeSafe(input);

            //Debug.WriteLine("replaceSingleQuoteTest - output: " + output);
            logger.Info("replaceSingleQuoteTest - output: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceSemicolonTest() {
            String input = "Test to make sure any semicolons(;); get removed from the string";
            String expect = "Test to make sure any semicolons() get removed from the string";

            String output = SafeSQL.makeSafe(input);

            Debug.WriteLine("replaceSingleQuoteTest - output: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceSQLCommentsTest() {
            String input = "Test to make sure a SQL comment(--) get replaced with with an en Dash - here are two comments(----)";
            String expect = "Test to make sure a SQL comment(-) get replaced with with an en Dash - here are two comments(-)";

            String output = SafeSQL.makeSafe(input);

            Debug.WriteLine("replaceSQLCommentsTest - output: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceAllAtOnceTest() {
            String input = "Test to make sure everything(', ;, --) gets replaced properly";
            String expect = "Test to make sure everything('', , -) gets replaced properly";

            String output = SafeSQL.makeSafe(input);

            Debug.WriteLine("replaceAllAtOnceTest - output: " + output);
            Assert.AreEqual(output, expect);
        }

        [Test]
        public void validateStatementTest() {
            String input = "Test to make sure everything(', ;, --) gets replaced properly";
            Boolean expect = false;

            Boolean output = SafeSQL.validateSQL(input);

            Debug.WriteLine("validateStatementTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);

            input = "Test to make sure everything gets replaced - SELECT * FROM Test";
            expect = true;

            output = SafeSQL.validateSQL(input);

            Debug.WriteLine("validateStatementTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }
    }
}
