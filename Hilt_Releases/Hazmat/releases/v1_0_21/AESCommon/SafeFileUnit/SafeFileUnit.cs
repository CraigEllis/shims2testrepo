﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using System.Diagnostics;
using NUnit.Framework;

using AESCommon.SafeFile;

namespace SafeFileUnit {
    [TestFixture]
    public class SafeFileUnit {
        [SetUp]
        public void Setup() {
        }

        [Test]
        public void replaceDoublePeriodTest() {
            String input = "Test to make sure double periods(..) get replaced with a single underscore(_)";
            String expect = "Test to make sure double periods(_) get replaced with a single underscore(_)";

            String output = SafeFile.makeFileNameSafe(input);

            Debug.WriteLine("replaceDoublePeriodTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceColonTest() {
            String input = "Test to make sure any colons(:) or (::) get replaced with a single underscore(_)";
            String expect = "Test to make sure any colons(_) or (__) get replaced with a single underscore(_)";

            String output = SafeFile.makeFileNameSafe(input);

            Debug.WriteLine("replaceColonTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceBackSlashTest() {
            String input = "Test to make sure any back slashes(\\) or (\\\\) get replaced with a single underscore(_)";
            String expect = "Test to make sure any back slashes(_) or (__) get replaced with a single underscore(_)";

            String output = SafeFile.makeFileNameSafe(input);

            Debug.WriteLine("replaceBackSlashTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceForwardSlashTest() {
            String input = "Test to make sure any forward slashes(/) or (//) get replaced with a single underscore(_)";
            String expect = "Test to make sure any forward slashes(_) or (__) get replaced with a single underscore(_)";

            String output = SafeFile.makeFileNameSafe(input);

            Debug.WriteLine("replaceForwardSlashTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }
        [Test]
        public void replaceAllAtOnceTest() {
            String input = "Test to make sure everything gets replaced - ..\\\\TestFile\\Drive:Path/SubPath/filename.ext";
            String expect = "Test to make sure everything gets replaced - ___TestFile_Drive_Path_SubPath_filename.ext";

            String output = SafeFile.makeFileNameSafe(input);

            Debug.WriteLine("replaceAllAtOnceTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }

        [Test]
        public void validateFileNameTest() {
            String input = "Test to make sure everything gets replaced - ..\\\\TestFile\\Drive:Path/SubPath/filename.ext";
            Boolean expect = false;

            Boolean output = SafeFile.isValidFileName(input);

            Debug.WriteLine("validateFileNameTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);

            input = "Test to make sure everything gets replaced - filename.ext";
            expect = true;

            output = SafeFile.isValidFileName(input);

            Debug.WriteLine("validateFileNameTest - input: " + input +
                "\n\t\toutput: " + output);
            Assert.AreEqual(output, expect);
        }
    }
}
