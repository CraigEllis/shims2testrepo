﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;


namespace HAZMATUnit
{
    class InsurvDownloadTests
    {

        //server db connection
        private string connectionString = Configuration.ConnectionInfo;

        //handheld db template path
        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");


        [Test]
        public void testinsertHHInsurvInventory()
        {

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //insert niin_catalog for decanting - based on the selected task
            try
            {

               
                //create Sqlite connection 
                SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
                liteCon.Open();

                //sqlite Transaction - begin on connection
                SQLiteTransaction liteTran = liteCon.BeginTransaction();

                //insert inventory data into the handheld database
                new HHDatabaseManager().insertHHInsurvInventory(tempFile, liteTran, liteCon);
                liteTran.Commit();

                //close connection
                liteCon.Close();

                //delete file
                File.Delete(tempFile);

                Console.Out.WriteLine("Insurv Checkout: Inventory inserted successfully");

                

            }
            catch (Exception ex)
            {
                //delete task and decanted inentory

                Assert.Fail("Error inserting inventory data. " + ex.Message);
            }


            Assert.Pass();


        }


        [Test]
        public void testinsertHHInsurvNiinCatalog()
        {

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //insert niin_catalog for decanting - based on the selected task
            try
            {

                //create Sqlite connection 
                SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
                liteCon.Open();

                //sqlite Transaction - begin on connection
                SQLiteTransaction liteTran = liteCon.BeginTransaction();

                //insert niin_catalog data into the handheld database
                new HHDatabaseManager().insertHHInsurvNiinCatalog(tempFile, liteTran, liteCon);
                liteTran.Commit();

                Console.Out.WriteLine("Insurv Checkout: Niin_Catalog inserted successfully");

             

                //check that the niin_catalog data in the handheld database is the same as what is on the server
                DataTable serverData = this.getServerNiinData();

                //handheld data
                DataTable hhData = this.getHHNiinData(tempFile);

                Console.Out.WriteLine("ServerDataCount_Niin: " + serverData.Rows.Count);
                Console.Out.WriteLine("HHDataCount_Niin: " + hhData.Rows.Count);

                //close hh connection
                liteCon.Close();

                int count = 0;

                //compare data
                foreach (DataRow servRow in serverData.Rows)
                {
                    int niin_catalog_id = Convert.ToInt32(servRow["niin_catalog_id"]);
                    string niin = Convert.ToString(servRow["niin"]);
                    string description = Convert.ToString(servRow["description"]);

                    foreach (DataRow hhRow in hhData.Rows)
                    {

                        int found_niin_catalog_id = Convert.ToInt32(hhRow["niin_catalog_id"]);

                        if (niin_catalog_id == found_niin_catalog_id)
                        {
                            string hh_niin = Convert.ToString(servRow["niin"]);
                            string hh_description = Convert.ToString(servRow["description"]);

                            Assert.AreEqual(niin_catalog_id, found_niin_catalog_id);
                            Assert.AreEqual(hh_niin, niin);

                            count++;

                        }

                    }

                }

                if (count == 0)
                {
                    
                    Assert.Fail("Data server does not match data in hh db");
                }
               
                //delete temp file
                File.Delete(tempFile);

            }
            catch (Exception ex)
            {
               
                Assert.Fail("Error inserting niin_catalog data. " + ex.Message);
            }


            Assert.Pass();


        }



        [Test]
        public void testInsertHHInsurvMfgCatalog()
        {

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //insert mfg_catalog
            try
            {

                //create Sqlite connection 
                SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
                liteCon.Open();

                //sqlite Transaction - begin on connection
                SQLiteTransaction liteTran = liteCon.BeginTransaction();

                //insert niin_catalog data into the handheld database
                new HHDatabaseManager().insertHHInsurvMfgCatalog(tempFile, liteTran, liteCon);
                liteTran.Commit();

                Console.Out.WriteLine("Insurv Checkout: Mfg_catalog inserted successfully");



                //check that the niin_catalog data in the handheld database is the same as what is on the server
                DataTable serverData = this.getServerMfgData();

                //handheld data
                DataTable hhData = this.getHHMfgData(tempFile);

                Console.Out.WriteLine("ServerDataCount_Mfg: " + serverData.Rows.Count);
                Console.Out.WriteLine("HHDataCount_Mfg: " + hhData.Rows.Count);

                //close hh connection
                liteCon.Close();

                int count = 0;

                //compare data
                foreach (DataRow servRow in serverData.Rows)
                {
                    int mfg_catalog_id = Convert.ToInt32(servRow["mfg_catalog_id"]);
                    string man = Convert.ToString(servRow["manufacturer"]);
                    string cage = Convert.ToString(servRow["cage"]);

                    foreach (DataRow hhRow in hhData.Rows)
                    {

                        int found_niin_catalog_id = Convert.ToInt32(hhRow["niin_catalog_id"]);

                        if (mfg_catalog_id == found_niin_catalog_id)
                        {
                            string hh_man = Convert.ToString(servRow["manufacturer"]);
                            string hh_cage = Convert.ToString(servRow["cage"]);

                            Assert.AreEqual(mfg_catalog_id, found_niin_catalog_id);
                            Assert.AreEqual(man, hh_man);
                            Assert.AreEqual(cage, hh_cage);

                            count++;

                        }

                    }

                }

                if (count == 0)
                {

                    Assert.Fail("Data server does not match data in hh db");
                }

                //delete temp file
                File.Delete(tempFile);

            }
            catch (Exception ex)
            {

                Assert.Fail("Error inserting mfg_catalog data. " + ex.Message);
            }


            Assert.Pass();


        }



        [Test]
        public void testInsertHHInsurvLocations()
        {

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //insert mfg_catalog
            try
            {

                //create Sqlite connection 
                SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
                liteCon.Open();

                //sqlite Transaction - begin on connection
                SQLiteTransaction liteTran = liteCon.BeginTransaction();

                //insert niin_catalog data into the handheld database
                new HHDatabaseManager().insertHHInsurvLocations(tempFile, liteTran, liteCon);
                liteTran.Commit();

                Console.Out.WriteLine("Insurv Checkout: Locations inserted successfully");



                //check that the niin_catalog data in the handheld database is the same as what is on the server
                DataTable serverData = this.getServerLocationData();

                //handheld data
                DataTable hhData = this.getHHLocationData(tempFile);

                Console.Out.WriteLine("ServerDataCount_Locations: " + serverData.Rows.Count);
                Console.Out.WriteLine("HHDataCount_Locations: " + hhData.Rows.Count);

                //close hh connection
                liteCon.Close();

                int count = 0;

                //compare data
                foreach (DataRow servRow in serverData.Rows)
                {
                    int location_id = Convert.ToInt32(servRow["location_id"]);
                    string name = Convert.ToString(servRow["name"]);
                    

                    foreach (DataRow hhRow in hhData.Rows)
                    {

                        int found_location_id = Convert.ToInt32(hhRow["location_id"]);

                        if (location_id == found_location_id)
                        {
                            string hh_name = Convert.ToString(hhRow["name"]);

                            Assert.AreEqual(location_id, found_location_id);
                            Assert.AreEqual(name, hh_name);
                           

                            count++;

                        }

                    }

                }

                if (count == 0)
                {

                    Assert.Fail("Data server does not match data in hh db");
                }

                //delete temp file
                File.Delete(tempFile);

            }
            catch (Exception ex)
            {

                Assert.Fail("Error inserting location data. " + ex.Message);
            }


            Assert.Pass();


        }


        [Test]
        public void testInsertHHInsurvNiin()
        {

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //insert mfg_catalog
            try
            {

                //create Sqlite connection 
                SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
                liteCon.Open();

                //sqlite Transaction - begin on connection
                SQLiteTransaction liteTran = liteCon.BeginTransaction();

                // Insert a test record into the insurv_niin table
                populateInsurvNiinTable();

                //insert niin_catalog data into the handheld database
                new HHDatabaseManager().insertHHInsurvNiin(tempFile, liteTran, liteCon);
                liteTran.Commit();

                Console.Out.WriteLine("Insurv Checkout: Insurv Niin inserted successfully");

                //check that the niin_catalog data in the handheld database is the same as what is on the server
                DataTable serverData = this.getServerInsurvNiinData();

                //handheld data
                DataTable hhData = this.getHHInsurvNiindata(tempFile);

                Console.Out.WriteLine("ServerDataCount_InsurvNiin: " + serverData.Rows.Count);
                Console.Out.WriteLine("HHDataCount_InsurvNiin: " + hhData.Rows.Count);

                //close hh connection
                liteCon.Close();

                int count = 0;

                //compare data
                foreach (DataRow servRow in serverData.Rows)
                {
                    int insurv_id = Convert.ToInt32(servRow["insurv_id"]);
                    string name = Convert.ToString(servRow["surv_name"]);
                    int niinCatId = Convert.ToInt32(servRow["niin_catalog_id"]);


                    foreach (DataRow hhRow in hhData.Rows)
                    {

                        int found_insurv_id = Convert.ToInt32(hhRow["insurv_id"]);

                        if (insurv_id == found_insurv_id)
                        {
                            string hh_name = Convert.ToString(servRow["surv_name"]);

                            Assert.AreEqual(hh_name, name);
                           
                            count++;

                        }

                    }

                }

                if (count == 0)
                {

                    Assert.Fail("Data server does not match data in hh db");
                }

                //delete temp file
                File.Delete(tempFile);

            }
            catch (Exception ex)
            {

                Assert.Fail("Error inserting insurvNiin data. " + ex.Message);
            }


            Assert.Pass();


        }

       


        private DataTable getHHNiinData(string dbLocation)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select * from niin_catalog", con);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
        }

        private DataTable getHHInsurvNiindata(string dbLocation)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select * from insurv_niin", con);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
        }


        private DataTable getHHMfgData(string dbLocation)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select * from mfg_catalog", con);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
        }


        private DataTable getHHLocationData(string dbLocation)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select * from locations l", con);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
        }

        private DataTable getServerLocationData()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from locations n where n.name <> 'NEWLY ISSUED'", con);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;

        }


        private DataTable getServerNiinData()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from niin_catalog n", con);            

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;

        }

        private DataTable getServerInsurvNiinData()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from insurv_niin n", con);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;

        }

        private DataTable getServerMfgData()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from mfg_catalog n", con);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;

        }

        private void populateInsurvNiinTable() {
            SqlConnection conn = new SqlConnection(connectionString);

            conn.Open();

            // See if we have any existing records
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM insurv_niin", conn);
            int count = (Int32)cmd.ExecuteScalar();

            Console.WriteLine("Insurv_niin count: " + count);

            if (count == 0) {
                cmd.CommandText = "INSERT INTO insurv_niin (surv_name, niin_catalog_id) "
                    + "VALUES('TESTSURV', (SELECT TOP 1 niin_catalog_id FROM niin_catalog "
                    + "ORDER BY NEWID()))";

                cmd.ExecuteNonQuery();
            }

            conn.Close();
        }
    }
}
