﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;

namespace HAZMATUnit
{
    class InsurvUploadTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");


        [Test]
        public void testImportInsurvTagCr()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();
            int inventoryid = this.getInventory();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            insertHHTagCRData(tempFile, location_id, niin_catalog_id, mfg_catalog_id, inventoryid, "XXX");

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();
            try
            {
                new HHDatabaseManager(connectionString).importInsurvTagCr(con, tran, tempFile);
                tran.Commit();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            //compare data

            File.Delete(tempFile);

            //remove test data
             this.removeTagCr();

            Console.Out.WriteLine("TAG HH insert executed successfully");
            Assert.Pass("yes");
        }



        [Test]
        public void testImportInsurvBromineCr()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();
            int inventoryid = this.getInventory();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            this.insertHHBromineCRData(tempFile, location_id, niin_catalog_id, mfg_catalog_id, inventoryid, "XXX");

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();
            try
            {
                new HHDatabaseManager(connectionString).importInsurvBromineCr(con, tran, tempFile);
                tran.Commit();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            //compare data

            File.Delete(tempFile);

            //remove test data           
            this.removeBromineCr();

            Console.Out.WriteLine("Bromine HH Insert executed successfully");
            Assert.Pass();
        }
    

        [Test]
        public void testImportInsurvCalciumCr()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();
            int inventoryid = this.getInventory();


            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);
            this.insertHHCalciumCRData(tempFile, location_id, niin_catalog_id, mfg_catalog_id, inventoryid, "XXX");

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();
            try
            {
                new HHDatabaseManager(connectionString).importInsurvCalciumCr(con, tran, tempFile);
                tran.Commit();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            //compare data

            File.Delete(tempFile);

            //remove test data           
            this.removeCalciumCr();

            Console.Out.WriteLine("Calcium HH Insert executed successfully");
            Assert.Pass();
        }

        [Test]
        public void testImportInsurvLocationCr()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();
            int inventoryid = this.getInventory();


            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);
            this.insertHHLocationCRData(tempFile, location_id, niin_catalog_id, mfg_catalog_id, inventoryid, "XXX");

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();
            try
            {
                new HHDatabaseManager(connectionString).importInsurvLocationCr(con, tran, tempFile);
                tran.Commit();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            //compare data

            File.Delete(tempFile);

            //remove test data           
            this.removeLocationCr();

            Console.Out.WriteLine("Location HH Insert executed successfully");
            Assert.Pass();
        }

        private void removeTagCr()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from atm_tag_audit where a = @a", con);
            cmd.Parameters.AddWithValue("@a", "XXX");

            cmd.ExecuteNonQuery();

            con.Close();

        }

        private void removeCalciumCr()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from calcium_hypochlorite_audit where a = @a", con);
            cmd.Parameters.AddWithValue("@a", "XXX");

            cmd.ExecuteNonQuery();

            con.Close();

            

        }

        private void removeBromineCr()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from bromine_audit where a = @a", con);
            cmd.Parameters.AddWithValue("@a", "XXX");

            cmd.ExecuteNonQuery();

            con.Close();



        }

        private void removeLocationCr()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from insurv_location_cr where surv_name = @a", con);
            cmd.Parameters.AddWithValue("@a", "XXX");

            cmd.ExecuteNonQuery();

            con.Close();



        }

        private int getLocation()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        private int getInventory()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "select TOP 1 inventory_id from inventory";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        private void insertHHBromineCRData(string dbLocation, int locationId, int niinCatId, int mfgCatId, int inventoryId, string val)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("insert into insurv_bromine_cr (mfg_catalog_id, a,b,c,d,location_id,qty,action_complete,inventory_id,niin_catalog_id) VALUES(@mfg_catalog_id, @a,@b,@c,@d,@location_id,@qty,@action_complete,@inventory_id,@niin_catalog_id)", con);

            DateTime d = DateTime.Now;

            cmd.Parameters.AddWithValue("@mfg_catalog_id", mfgCatId);
            cmd.Parameters.AddWithValue("@a", val);
            cmd.Parameters.AddWithValue("@b", val);
            cmd.Parameters.AddWithValue("@c", val);
            cmd.Parameters.AddWithValue("@d", val);
            cmd.Parameters.AddWithValue("@location_id", locationId);
            cmd.Parameters.AddWithValue("@qty", 9999);
            cmd.Parameters.AddWithValue("@action_complete", d);
            cmd.Parameters.AddWithValue("@inventory_id", inventoryId);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niinCatId);


            cmd.ExecuteNonQuery();

            con.Close();
         
        }



        private void insertHHTagCRData(string dbLocation, int locationId, int niinCatId, int mfgCatId, int inventoryId, string val)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            DateTime d = DateTime.Now;

            SQLiteCommand cmd = new SQLiteCommand("insert into insurv_bromine_cr (mfg_catalog_id, a,b,location_id,qty,action_complete,inventory_id,niin_catalog_id) VALUES(@mfg_catalog_id, @a,@b,@location_id,@qty,@action_complete,@inventory_id,@niin_catalog_id)", con);

            cmd.Parameters.AddWithValue("@mfg_catalog_id", mfgCatId);
            cmd.Parameters.AddWithValue("@a", val);
            cmd.Parameters.AddWithValue("@b", val);
            
            cmd.Parameters.AddWithValue("@location_id", locationId);
            cmd.Parameters.AddWithValue("@qty", 9999);
            cmd.Parameters.AddWithValue("@action_complete", d);
            cmd.Parameters.AddWithValue("@inventory_id", inventoryId);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niinCatId);


            cmd.ExecuteNonQuery();

            con.Close();

        }


        private void insertHHCalciumCRData(string dbLocation, int locationId, int niinCatId, int mfgCatId, int inventoryId, string val)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            DateTime d = DateTime.Now;

            SQLiteCommand cmd = new SQLiteCommand("insert into insurv_calcium_cr (mfg_catalog_id,a,b,c,d,e,f,g,h,i,j,location_id,qty,action_complete,inventory_id,niin_catalog_id) VALUES(@mfg_catalog_id, @a,@b,@c,@d,@e,@f,@g,@h,@i,@j,@location_id,@qty,@action_complete,@inventory_id,@niin_catalog_id)", con);

            cmd.Parameters.AddWithValue("@mfg_catalog_id", mfgCatId);
            cmd.Parameters.AddWithValue("@a", val);
            cmd.Parameters.AddWithValue("@b", val);
            cmd.Parameters.AddWithValue("@c", val);
            cmd.Parameters.AddWithValue("@d", val);
            cmd.Parameters.AddWithValue("@e", val);
            cmd.Parameters.AddWithValue("@f", val);
            cmd.Parameters.AddWithValue("@g", val);
            cmd.Parameters.AddWithValue("@h", val);
            cmd.Parameters.AddWithValue("@i", val);
            cmd.Parameters.AddWithValue("@j", val);

            cmd.Parameters.AddWithValue("@location_id", locationId);
            cmd.Parameters.AddWithValue("@qty", 9999);
            cmd.Parameters.AddWithValue("@action_complete", d);
            cmd.Parameters.AddWithValue("@inventory_id", inventoryId);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niinCatId);


            cmd.ExecuteNonQuery();

            con.Close();

        }

        private void insertHHLocationCRData(string dbLocation, int locationId, int niinCatId, int mfgCatId, int inventoryId, string val)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            DateTime d = DateTime.Now;

            SQLiteCommand cmd = new SQLiteCommand("insert into insurv_location_cr (location_id,location_done,action_complete,surv_name) VALUES (@location_id,@location_done,@action_complete,@surv_name)", con);

            cmd.Parameters.AddWithValue("@location_id", locationId);
            cmd.Parameters.AddWithValue("@location_done", true);
            cmd.Parameters.AddWithValue("@action_complete", d);
            cmd.Parameters.AddWithValue("@surv_name", val);


            cmd.ExecuteNonQuery();

            con.Close();

        }

    }
}
