﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;

namespace HAZMATUnit
{


    //test the clipboard download process for decanting
    [TestFixture]
    class DecantingDownloadTests
    {

        //server db connection
        private string connectionString = Configuration.ConnectionInfo;

        //handheld db template path
        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");

        private int taskId;
        
        [Test]
        public void testInsertHHDecantingNiinCatalogByTask()
        {

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //insert niin_catalog for decanting - based on the selected task
            try
            {

                //before we begin testing, a task needs to be created. Also, insert a row into decanted_inventory. This task will be deleted at the end of testing
                taskId = setup();


                //create Sqlite connection 
                SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
                liteCon.Open();                                

                //sqlite Transaction - begin on connection
                SQLiteTransaction liteTran = liteCon.BeginTransaction();

                //insert niin_catalog data into the handheld database

                new HHDatabaseManager(connectionString).insertHHNiinCatalogForDecanting("", liteTran, liteCon, taskId);
                new HHDatabaseManager(connectionString).insertHHMfgCatalogForDecanting("", liteTran, liteCon, taskId);
                new HHDatabaseManager(connectionString).insertHHInventoryInDecantTaskByTask("", liteTran, liteCon, taskId);
                new HHDatabaseManager(connectionString).insertHHHazardWarningsForReceiving("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHHazardsForReceiving("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHShelfLifeCodeForReceiving("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHConfigForDecanting("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHDecantingLocations("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHAtmosphereControl("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHDecantingInventory("", liteTran, liteCon);
                new HHDatabaseManager(connectionString).insertHHDecantingHazardousHCC("", liteTran, liteCon);

                //new DatabaseManager().insertHHDecantingNiinCatalogByTask("", liteTran, liteCon, taskId);
                //new DatabaseManager().insertHHInventoryInDecantTaskByTask("", liteTran, liteCon, taskId);
                liteTran.Commit();
                liteCon.Close();

                //remove test data
                //this.deleteDecantedInventory(taskid);
                this.deleteTask(taskId);

                //delete temp file
                File.Delete(tempFile);

                

            }
            catch (Exception ex)
            {
                //delete task and decanted inentory
                this.deleteTask(taskId);
                Assert.Fail("Error inserting niin_catalog data. " + ex.Message);
            }



            Assert.Pass("Handheld decanting test checkout completed successfully");

        }




        private DataTable getServerNiinDataByTask(int taskid)
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from niin_catalog n where n.niin_catalog_id in(select niin_catalog_id from inventory_in_decant_task i INNER JOIN mfg_catalog m ON (i.mfg_catalog_id=m.mfg_catalog_id) where decanting_task_id = @decanting)", con);
            cmd.Parameters.AddWithValue("@decanting", taskid);
            
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
            
        }

        [CoverageExclude]
        private void deleteDecantedInventory(int taskid)
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from decanted_inventory where decanting_task_id = @decanting", con);
            cmd.Parameters.AddWithValue("@decanting", taskid);

            cmd.ExecuteNonQuery();

            con.Close();
        }

        private void deleteTask(int taskid)
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from decanting_tasks where decanting_task_id = @decanting", con);
            cmd.Parameters.AddWithValue("@decanting", taskid);

            cmd.ExecuteNonQuery();

            con.Close();

        }


        private DataTable getHHNiinData(string dbLocation)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select * from niin_catalog", con);           

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
        }

        private DataTable getHHdecantTaskData(string dbLocation)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select * from inventory_in_decant_task", con);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;
        }


        private int setup()
        {
            //create task
            int taskid = this.createTask();

            //insert into decanted_inventory
            createDecantInventoryFromTask(taskid);

            return taskid;
        }


        private void createDecantInventoryFromTask(int taskid)
        {

            DatabaseManager manager = new DatabaseManager();


            List<DecantingInfo> decantingInfoList = new List<DecantingInfo>();

            DecantingInfo info = new DecantingInfo();
            info.inventory_id = manager.getExistingInventoryId();
            info.ContainerType = "UT";
            info.Volume = "10 GL";

            decantingInfoList.Add(info);

            //insert into inventory_in_decant_task
            manager.insertIntoDecantedInventoryFromDecantNew(decantingInfoList, taskid);
            
        }

        private int createTask()
        {
            
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "insert into decanting_tasks (description, date, active) VALUES (@description, @date, @active); SELECT SCOPE_IDENTITY()";
            SqlCommand cmd = new SqlCommand(stmt, con);

            cmd.Parameters.AddWithValue("@description", "UnitTestSampleTask");
            cmd.Parameters.AddWithValue("@date", DateTime.Now);
            cmd.Parameters.AddWithValue("@active", true);

            int taskid = Convert.ToInt32(cmd.ExecuteScalar());

            Console.Write("taskid- " + taskid);

            con.Close();

            return taskid;
        }

        [CoverageExclude]   
        private int getLocation() 
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }




    }
}
