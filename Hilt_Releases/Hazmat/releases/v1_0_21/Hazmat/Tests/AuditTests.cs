﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Collections;


namespace HAZMATUnit
{

     [TestFixture]
    class AuditTests
    {

        //server db connection
        private string connectionString = Configuration.ConnectionInfo;

        //handheld db template path
        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");

        //getInventoryAuditLocationSelectionCollection



        [Test]
        public void testGetInventoryAuditLocationSelectionCollection()
        {
           

            Assert.Pass();

        }

        [Test]
        public void testGetInventoryAuditIdByRecurringDates()
        {
            new DatabaseManager(connectionString).getRecurringIdsByDate(new DateTime(2000,1,1));

            Assert.Pass();

        }

        [Test]
        public void testDeleteAuditsInRecurringById()
        {
            DatabaseManager man = new DatabaseManager(connectionString);
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlTransaction tran = con.BeginTransaction();

            man.deleteAuditsInRecurringById(100009900, tran, con);
            man.deleteRecurringAuditById(10990000, tran, con);
            tran.Commit();

            con.Close();

            Assert.Pass("deleteAuditsInRecurringById Passed");
        }


        //deleteInventoryAuditAndRecurringAudits
        [Test]
        public void testDeleteInventoryAuditAndRecurringAudits()
        {
            DatabaseManager man = new DatabaseManager(connectionString);
            DataTable locations = man.getLocationList();

            List<string> statusList = man.getStatusList();

            DateTime time = DateTime.Now;

            int auditid = man.createAudit("UnitTestAudits", statusList[0], time, locations, null);
            
            man.deleteInventoryAuditAndRecurringAudits(auditid);

            this.removeAudit(auditid);

            Assert.Pass("deleteInventoryAuditAndRecurringAudits Passed!");
        }


        [Test]
        public void testCreateAuditClipboard()
        {

            DatabaseManager man = new DatabaseManager(connectionString);
            DataTable locations = man.getLocationList();

            List<string> statusList = man.getStatusList();

            DateTime time = DateTime.Now;

            int auditid = man.createAudit("UnitTestAudits", statusList[0], time, locations, null);

            //create tempFile to copy the template to - 
            string tempFileLocation = System.IO.Path.GetTempFileName();

            //copy template
            File.Copy(this.dbTemplate, tempFileLocation, true);

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFileLocation);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();

            new HHDatabaseManager(connectionString).insertHHWorkCenters(tempFileLocation, tran, liteCon);
            new HHDatabaseManager(connectionString).insertHHLocationsByAudit(tempFileLocation, tran, liteCon, auditid);            
            new HHDatabaseManager(connectionString).insertHHLocationCR(tempFileLocation, tran, liteCon, auditid);
            new HHDatabaseManager(connectionString).insertHHInventoryAuditCR(tempFileLocation, tran, liteCon, auditid);
            new HHDatabaseManager(connectionString).insertHHConfigForAudit(tempFileLocation, tran, liteCon, auditid);
            new HHDatabaseManager(connectionString).insertHHNiinCatalog(tempFileLocation, tran, liteCon);
            new HHDatabaseManager(connectionString).insertHHMfgCatalog(tempFileLocation, tran, liteCon);
            new HHDatabaseManager(connectionString).insertHHInventoryForAudit(tempFileLocation, tran, liteCon, auditid);
            new HHDatabaseManager(connectionString).insertHHShelfLifeCodeForAudit(tempFileLocation, tran, liteCon);
            new HHDatabaseManager(connectionString).insertHHVolumeForAudit(tempFileLocation, tran, liteCon);


            tran.Commit();

            this.removeAudit(auditid);

            Assert.Pass("Audit clipboard created successfully!");
            
        }


        [Test]
        public void testAuditExists()
        {

            try
            {
                new DatabaseManager(connectionString).checkAuditByIdAndDate(new DateTime(2000, 1, 1), 1);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

         [Test]
        public void testgetRecurringAuditInformation()
        {
            new DatabaseManager(connectionString).getRecurringAuditInformation();
            Assert.Pass();
        }

        [Test]
        public void testCreateAudit()
        {

            DatabaseManager man = new DatabaseManager(connectionString);            

            DataTable locations = man.getLocationList();

            List <string> statusList = man.getStatusList();

            DateTime time = DateTime.Now;

            int newAuditId = man.createAudit("UnitTestAudits", statusList[0], time, locations, null);

            new DatabaseManager(connectionString).getInventoryAuditLocationSelectionCollection(newAuditId);
            new DatabaseManager(connectionString).getLocationListFromRecurringAudit(newAuditId);
            new DatabaseManager(connectionString).getRecurringAuditInfoCollectionById(newAuditId);
            //getRecurringAuditInfoCollectionById

           
            //get location list
            man.getLocationListFromAudit(newAuditId);

            //get auditid
            List<int> auditList = man.getInventoryAuditID(time);
           
            //delete audit
            this.removeAudit(newAuditId);


            Assert.Pass("createAudit and createClipboard passed!");
        }


        [Test]
        public void testAuditWorkcenterList()
        {
            List<string> workList = new DatabaseManager(connectionString).getWorkCenterList();
            Assert.Pass("Workcenter count: " + workList.Count);
        }

        [Test]
        public void testGetLocationList()
        {
            DataTable selectedTable = new DatabaseManager(connectionString).getLocationList();
            
            DataTable filteredTable = new DatabaseManager(connectionString).getFilteredAvailableLocationsAuditNew(selectedTable, "", "");

            Assert.Pass("getLocationList passed!");
        }

        [Test]
        public void testPopulateStatus()
        {
            List<string> statusList = new DatabaseManager(connectionString).getStatusList();

            Assert.Pass("populateStatus passed!");
        }

        [Test]
        public void testGetAuditDescription()
        {
            new DatabaseManager(connectionString).getAuditDescription(1);

            Assert.Pass("getAuditDescription passed!");
        }

        [Test]
        public void testGetAuditInformation()
        {
           List<AuditInfo> infoList = new DatabaseManager(connectionString).getAuditInformation();

           Debug.WriteLine("Audit Info: " + infoList.Count);
            Assert.Pass("getAuditDescription passed!");
        }

        [Test]
        public void testGetFilteredAvailableInventoryAuditCollection()
        {
            DataTable tab = new DatabaseManager(connectionString).getFilteredAvailableInventoryAuditCollection("niin","A");

            Debug.WriteLine("Filtered Audit Collection Info: " + tab.Rows.Count);
            Assert.Pass("getFilteredAvailableInventoryAuditCollection passed!");
        }

        //getFilteredAvailableInventoryAuditCollection

         [CoverageExclude]
        private void removeAudit(int auditid)
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string stmt = "delete from locations_in_audit where inventory_audit_id = @id";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@id", auditid);
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            stmt = "delete from clipboard where inventory_audit_id = @id";
            cmd.CommandText = stmt;
            cmd.Parameters.AddWithValue("@id", auditid);
            cmd.ExecuteNonQuery();

            cmd.Parameters.Clear();
            stmt = "delete from inventory_audits  where inventory_audit_id = @id";
            cmd.CommandText = stmt;
            cmd.Parameters.AddWithValue("@id", auditid);
            cmd.ExecuteNonQuery();

            con.Close();



        }

         [Test]
         public void testFoundClipboardHash()
         {
             new DatabaseManager().foundClipboardHash("HASH");
             Assert.Pass("foundClipboardHash Passed!");

         }
        

         [Test]
         public void TestLocationListLoad()
         {
             // Delete any existing items
             deleteLocationByName("UNITTEST");

             //Insert new audit, recurring audit, workcenter, location, and location_cr for an audit
             DatabaseManager manager = new DatabaseManager(connectionString);
             int inventory_audit_id = manager.createAuditForTest();
             int recurring_audit_id = manager.createRecurringAuditForTest(inventory_audit_id);
             int workcenterId = manager.createWorkcenterForTest();
             int location_id = manager.insertLocation("UNITTEST", workcenterId);
             int location_in_audit_id = this.insertLocationInAudit(location_id, inventory_audit_id);
             int inventory_id = this.insertInventory(location_id);
             int first_location_cr_id = manager.insertLocationCr(location_id, 1, inventory_audit_id, 1);
             int second_location_cr_id = manager.insertLocationCr(location_id, 1, inventory_audit_id, 2);

             //Volume test
             List<VolumeOffloadItem> volumes = new List<VolumeOffloadItem>();
             for (int i = 1; i <= 6; i++)
             {
                 volumes.Add(new VolumeOffloadItem(i));
             }

             Console.WriteLine("Starting insertInventoryAuditCr");

             int first_audit_cr = manager.insertInventoryAuditCr(
                 volumes, inventory_audit_id, location_id, 294, 2, true, "NULL", 
                 "2011-12-16 00:00:00", "2010-12-16 00:00:00", 5, "GL", 1, "NULL", 
                 "HME", null, null);
             int second_audit_cr = manager.insertInventoryAuditCr(
                 volumes, inventory_audit_id, location_id, 294, 1, true, "NULL", 
                 "2011-12-16 00:00:00", "2010-12-16 00:00:00", 5, "GL", 2, "NULL", 
                 "HME", null, null);

             Console.WriteLine("Done insertinserting Audit records");


             //Volume test
             DataTable volList = manager.getVolumesInInventoryAudit(first_audit_cr);
             Assert.True(volList.Rows.Count == 6);


             Debug.WriteLine("Audit: " + inventory_audit_id + ", Recurring: " + recurring_audit_id + ", Workcenter: " + workcenterId + ", Location: " + location_id + ", Inventory: " + inventory_id + ", first loc_cr: " + first_location_cr_id + ", second loc_cr: " + second_location_cr_id + ", first audit_cr: " + first_audit_cr + ", second audit_cr: " + second_audit_cr);
             //Test loading discrepant locations from audit counts
             DataTable firstdt = manager.getInventoryAuditLocationWithDiscrepanciesCollection(inventory_audit_id, 1,"test",false);
             int firstCount = 0;
             
             for (int i = 0; i < firstdt.Rows.Count; i++)
             {
                 DataRow row = firstdt.Rows[i];                 
                 if (Convert.ToInt32(row["location_id"]) == location_id)
                 {                     
                     firstCount++;
                 }
             }

             this.deleteLocationInAudit(location_in_audit_id);
             manager.deleteLocation(location_id);
             manager.removeWorkcenterForTest(workcenterId);
             manager.removeRecurringAuditForTest(recurring_audit_id, inventory_audit_id);
             manager.removeAuditForTest(inventory_audit_id);
             manager.deleteInventoryAuditCr(first_audit_cr);
             manager.deleteInventoryAuditCr(second_audit_cr);
             manager.deleteLocationCr(first_location_cr_id);
             manager.deleteLocationCr(second_location_cr_id);
             this.deleteInventory(inventory_id);
            
             Debug.WriteLine("First Count: " + firstdt.Rows.Count);
             if (firstCount == 1)
             {
                 Assert.Pass();
             }
             else
             {
                 Assert.Fail();
             }             
         }

         [CoverageExclude]
         private int insertInventory(int location_id)
         {
             SqlConnection con = new SqlConnection(connectionString);
             con.Open();
             string stmt = "insert into inventory ( " 
                + "shelf_life_expiration_date, location_id, qty, bulk_item, "
                + "mfg_catalog_id, manufacturer_date) VALUES ('2011-12-16 00:00:00', " 
                + location_id + ", 1, 1, 294, '2010-12-16 00:00:00'); SELECT SCOPE_IDENTITY()";
             SqlCommand cmd = new SqlCommand(stmt, con);
             object o = cmd.ExecuteScalar();

             int inventory_id = System.Convert.ToInt32(o);

             con.Close();
             return inventory_id;
         }

         [CoverageExclude]
         private void deleteInventory(int inventory_id)
         {
             SqlConnection con = new SqlConnection(connectionString);
             con.Open();
             string stmt = "delete from inventory where inventory_id = @inventory_id";
             SqlCommand cmd = new SqlCommand(stmt, con);
             cmd.Parameters.AddWithValue("@inventory_id", inventory_id);
             cmd.ExecuteNonQuery();

             con.Close();
         }

         [CoverageExclude]
         private int insertLocationInAudit(int location_id, int audit_id)
         {
             SqlConnection con = new SqlConnection(connectionString);
             con.Open();
             string stmt = "insert into locations_in_audit (inventory_audit_id, location_id) VALUES (" + audit_id + ", " + location_id + "); SELECT SCOPE_IDENTITY()";
             SqlCommand cmd = new SqlCommand(stmt, con);
             object o = cmd.ExecuteScalar();

             int location_in_audit_id = System.Convert.ToInt32(o);

             con.Close();
             return location_in_audit_id;
         }

         [CoverageExclude]
         private void deleteLocationByName(string locationName) {
             SqlConnection con = new SqlConnection(connectionString);
             con.Open();
             string stmt = "delete from locations where [name] = '" + locationName + "'";
             SqlCommand cmd = new SqlCommand(stmt, con);
             cmd.ExecuteNonQuery();

             con.Close();
         }

         [CoverageExclude]
         private void deleteLocationInAudit(int location_in_audit_id)
         {
             SqlConnection con = new SqlConnection(connectionString);
             con.Open();
             string stmt = "delete from locations_in_audit where location_in_audit_id = " + location_in_audit_id;
             SqlCommand cmd = new SqlCommand(stmt, con);
             object o = cmd.ExecuteScalar();


             con.Close();
         }

         [Test]
         public void testDeleteInventoryAuditAndRecurringAudit()
         {
             //insert new inventory audit, recurring audit, location, and workcenter
             DatabaseManager manager = new DatabaseManager(connectionString);
             int inventory_audit_id = manager.createAuditForTest();
             int recurring_audit_id = manager.createRecurringAuditForTest(inventory_audit_id);
             int workcenterId = manager.createWorkcenterForTest();
             int location_id = manager.insertLocation("UNITTEST", workcenterId);

             DatabaseManager manage = new DatabaseManager();
             manage.deleteInventoryAuditAndRecurringAudits(inventory_audit_id);

             int deleted = manage.getIfInventoryAuditDeletedById(inventory_audit_id);
             int recurringDeleted = manage.getIfRecurringAuditDeletedById(recurring_audit_id);

             if (deleted == 1)
             {
                 Assert.Pass();
             }
             else
             {
                 manager.deleteLocation(location_id);
                 manager.removeWorkcenterForTest(workcenterId);
                 manager.removeRecurringAuditForTest(recurring_audit_id, inventory_audit_id);
                 manager.removeAuditForTest(inventory_audit_id);
                 Assert.Fail();
             }
             if (recurringDeleted == 1)
             {
                 Assert.Pass();
             }
             else
             {
                 manager.deleteLocation(location_id);
                 manager.removeWorkcenterForTest(workcenterId);
                 manager.removeRecurringAuditForTest(recurring_audit_id, inventory_audit_id);
                 manager.removeAuditForTest(inventory_audit_id);
                 Assert.Fail();
             }

             manager.deleteLocation(location_id);
             manager.removeWorkcenterForTest(workcenterId);
             manager.removeRecurringAuditForTest(recurring_audit_id, inventory_audit_id);
             manager.removeAuditForTest(inventory_audit_id);
         }

         [Test]
         public void Test_getHazardListsForMSDSERNO()
         {
             DatabaseManager manager = new DatabaseManager(connectionString);

             string MSDSSERNO = manager.getExistingMSDSSERNO();

             Object[] hazardObjects = manager.getHazardListsForMSDSERNO(MSDSSERNO);

             List<int> hazardList = (List<int>)hazardObjects[0];
             List<string> hccList = (List<string>)hazardObjects[1];

             Assert.True(hazardList.Count > 0);

             int hazard_id = hazardList[0];

             Assert.True(hazard_id != 0);

             string hcc = hccList[0];

             Assert.True(hcc != null);

             Assert.True(!hcc.Equals(""));

             Console.WriteLine("Test_getHazardListsForMSDSERNO:" + hcc + "  /id- " + hazard_id);
          

             Assert.Pass();

         }

         [Test]
         public void Test_getIncompatibilitiesInventoryListInvAuditCr()
         {
             DatabaseManager manager = new DatabaseManager(connectionString);

             string MSDSSERNO = manager.getExistingMSDSSERNO();

             Object[] hazardObjects = manager.getHazardListsForMSDSERNO(MSDSSERNO);

             List<int> hazardList = (List<int>)hazardObjects[0];
             List<string> hccList = (List<string>)hazardObjects[1];

             int location_id = manager.getExistingLocationId();

             int audit_id = manager.getExistingInventoryAuditId();

             int? inventory_audit_cr_id = null;

             int count = 1;

             DataTable dt = manager.getIncompatibilitiesInventoryListInvAuditCr(hccList, hazardList, location_id, audit_id, count, inventory_audit_cr_id);

             Console.WriteLine("Test_getIncompatibilitiesInventoryListInvAuditCr rows:" + dt.Rows.Count + " / " + dt.Columns.Count);

             //confirm columns
             Assert.True(dt.Columns.Contains("warning_level"));
             Assert.True(dt.Columns.Contains("hazard_name"));
             Assert.True(dt.Columns.Contains("niin"));
             Assert.True(dt.Columns.Contains("description"));
             Assert.True(dt.Columns.Contains("cage"));
             Assert.True(dt.Columns.Contains("manufacturer"));
             Assert.True(dt.Columns.Contains("shelf_life_expiration_date"));
             Assert.True(dt.Columns.Contains("location_id"));
             Assert.True(dt.Columns.Contains("ui"));
             Assert.True(dt.Columns.Contains("um"));
             Assert.True(dt.Columns.Contains("qty"));
             Assert.True(dt.Columns.Contains("inventory_audit_cr_id"));
             Assert.True(dt.Columns.Contains("inventory_audit_id"));
             Assert.True(dt.Columns.Contains("count"));

             Assert.Pass();

         }

    }
}
