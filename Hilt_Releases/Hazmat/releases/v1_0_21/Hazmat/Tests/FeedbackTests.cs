﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;


namespace HAZMATUnit
{
     [TestFixture]
    class FeedbackTests
    {
        private string connectionString = Configuration.ConnectionInfo;



        [Test]
        public void testCreateFeedbackReport()
        {

            FeedbackInfo info = new FeedbackInfo();

            info.Date = DateTime.Now;
            info.Feedback = "This is a feedback test.";
            info.Page = "TestPage";
            info.Username = "unitTest user";

            List<FeedbackInfo> infoList = new List<FeedbackInfo>();
            infoList.Add(info);

            string tempFile = Path.GetTempFileName();
            byte[] file = new DatabaseManager(connectionString).createFeedbackReport(infoList);

            FileStream stream = new FileStream(tempFile, FileMode.Create, FileAccess.Write);
            stream.Write(file, 0, file.Length);
            stream.Close();

            StreamReader reader = new StreamReader(tempFile);


            bool headerFound = false;
            bool dataFound = false;
            while (!reader.EndOfStream)
            {
                string data = reader.ReadLine();

                if (data.StartsWith("Page"))
                {
                    headerFound = true;

                }

                if (data.Contains("This is a feedback test."))
                {
                    dataFound = true;
                }
            }

            reader.Close();

            File.Delete(tempFile);

            if (headerFound && dataFound)
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }

        }

         [Test]
        public void testInsertFeedback()
        {

            try
            {
                new DatabaseManager(connectionString).saveFeedback("UNITTESTFEEDBACK", "TESTPAGE", "TESTUSER");

                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                string stmt = "select * from feedback f where f.feedback = 'UNITTESTFEEDBACK'";

                SqlCommand cmd = new SqlCommand(stmt, con);

                SqlDataReader rdr = cmd.ExecuteReader();

                bool found = false;

                while (rdr.Read())
                {
                    found = true;
                    break;
                }

                rdr.Close();
                con.Close();

                if (found)
                {
                    this.removeFeedback();
                    Debug.WriteLine("Feedback inserted successfully");
                    Assert.Pass("Inserted feedback passed!");
                }
                else
                {
                    this.removeFeedback();
                    Debug.WriteLine("Error inserting feedback");
                    Assert.Fail("Feedback information not found in the database");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                this.removeFeedback();
            }
        }


        private void removeFeedback()
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "delete from feedback where feedback LIKE 'UNITTESTFEEDBACK%'";
            SqlCommand cmd = new SqlCommand(stmt, con);

            cmd.ExecuteNonQuery();

            con.Close();
        }

    }


}
