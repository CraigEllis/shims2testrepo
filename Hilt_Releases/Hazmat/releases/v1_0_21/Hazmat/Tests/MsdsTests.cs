﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace HAZMATUnit
{
    class MsdsTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestHazardIdForHCC()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            int? value = manager.getHazardIdForHCC("nonsense");

            Assert.AreEqual(null, value);

            int? hazard_id = manager.getHazardIdForHCC("G2");

            Assert.AreEqual(1, hazard_id);

            value = manager.getHazardIdForHCC(1);

            Assert.AreEqual(14, value);

            hazard_id = manager.getHazardIdForHCC(25);

            Assert.AreEqual(1, hazard_id);

        }

       
        [Test]
        public void TestExistsMFGCatalog()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            bool alreadyExists = manager.alreadyExistsMFGCatalog(1, "CAGE");

            Assert.Pass();

        }

        [Test]
        public void TestMSDSexport()
        {
            DataTable dt = new DatabaseManager(connectionString).getManuallyEnteredMSDS();

            Assert.Pass();
        }

        [Test]
        public void TestMSDSexportFiles()
        {
            List<KeyValuePair<string, byte[]>> files = new DatabaseManager(connectionString).getManuallyEnteredMSDSfiles();

            Assert.Pass();
        }


        [Test]
        public void TestMSDSbuildCsv()
        {
            MemoryStream s = new MSDSExport().getZipFileExport();

            Assert.Pass();
        }


        [Test]
        public void TestMSDSfinds()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            int id = 1;

            manager.findMSDS(id);
            manager.findMSDSafjmPSN(id);
            manager.findMSDScontractorInfo(id);
            manager.findMSDSdocTypes(id);
            manager.findMSDSdisposal(id);
            manager.findMSDSdotPSN(id);
            manager.findMSDSiataPSN(id);
            manager.findMSDSimoPSN(id);
            manager.findMSDSingredients(id);
            manager.findMSDSitemDescription(id);
            manager.findMSDSlabelInfo(id);
            manager.findMSDSphysChemical(id);
            manager.findMSDSradiologicalInfo(id);
            manager.findMSDStransportation(id);

            Assert.Pass();
        }
         

        [Test]
        public void TestMSDSinsert()
        {

            string content = "1";
            MsdsRecord msds = new MsdsRecord();

            //msds
            msds.MSDSSERNO = content;
            msds.CAGE = content;
            msds.MANUFACTURER = content;
            msds.PARTNO = content;
            msds.FSC = content;
            msds.NIIN = content;
            msds.ARTICLE_IND = content;
            msds.DESCRIPTION = content;
            msds.EMERGENCY_TEL = content;
            msds.END_COMP_IND = content;
            msds.END_ITEM_IND = content;
            msds.KIT_IND = content;
            msds.KIT_PART_IND = content;
            msds.MANUFACTURER_MSDS_NO = content;
            msds.MIXTURE_IND = content;
            msds.PRODUCT_IDENTITY = content;
            msds.PRODUCT_LOAD_DATE = DateTime.Now.ToShortDateString();
            msds.PRODUCT_RECORD_STATUS = content;
            msds.PRODUCT_REVISION_NO = content;
            msds.PROPRIETARY_IND = content;
            msds.PUBLISHED_IND = content;
            msds.PURCHASED_PROD_IND = content;
            msds.PURE_IND = content;
            msds.RADIOACTIVE_IND = content;
            msds.SERVICE_AGENCY = content;
            msds.TRADE_NAME = content;
            msds.TRADE_SECRET_IND = content;
            msds.PRODUCT_IND = content;
            msds.PRODUCT_LANGUAGE = content;
            msds.hcc_id = 1;

            msds.msds_file = new byte[1];
            string filename = "blank.txt";
            msds.file_name = filename;


            //phys chemical
            msds.APP_ODOR = content;
            msds.VAPOR_PRESS = content;
            msds.VAPOR_DENS = content;
            msds.SPECIFIC_GRAV = content;
            msds.EVAP_RATE_REF = content;
            msds.SOL_IN_WATER = content;
            msds.PERCENT_VOL_VOLUME = content;
            msds.VISCOSITY = content;
            msds.VOC_POUNDS_GALLON = content;
            msds.VOC_GRAMS_LITER = content;
            msds.PH = content;
            msds.AUTOIGNITION_TEMP = content;
            msds.CARCINOGEN_IND = content;
            msds.EPA_ACUTE = content;
            msds.EPA_CHRONIC = content;
            msds.EPA_FIRE = content;
            msds.EPA_PRESSURE = content;
            msds.EPA_REACTIVITY = content;
            msds.FLASH_PT_TEMP =content;
            msds.NEUT_AGENT = content;
            msds.NFPA_FLAMMABILITY = content;
            msds.NFPA_HEALTH = content;
            msds.NFPA_REACTIVITY = content;
            msds.NFPA_SPECIAL = content;
            msds.OSHA_CARCINOGENS = content;
            msds.OSHA_COMB_LIQUID = content;
            msds.OSHA_COMP_GAS = content;
            msds.OSHA_CORROSIVE = content;
            msds.OSHA_EXPLOSIVE = content;
            msds.OSHA_FLAMMABLE = content;
            msds.OSHA_HIGH_TOXIC = content;
            msds.OSHA_IRRITANT = content;
            msds.OSHA_ORG_PEROX = content;
            msds.OSHA_OTHERLONGTERM = content;
            msds.OSHA_OXIDIZER = content;
            msds.OSHA_PYRO = content;
            msds.OSHA_SENSITIZER = content;
            msds.OSHA_TOXIC = content;
            msds.OSHA_UNST_REACT = content;
            msds.OTHER_SHORT_TERM = content;
            msds.PHYS_STATE_CODE = content;
            msds.VOL_ORG_COMP_WT = content;
            msds.OSHA_WATER_REACTIVE = content;



            //ingredients
            MsdsIngredients ingredient = new MsdsIngredients();
            ingredient.INGREDIENT_NAME = content;
            ingredient.PRCNT = content;
            ingredient.CAS = content;
            ingredient.RTECS_NUM = content;
            ingredient.RTECS_CODE = content;
            ingredient.OSHA_PEL = content;
            ingredient.OSHA_STEL = content;
            ingredient.ACGIH_TLV = content;
            ingredient.ACGIH_STEL = content;
            ingredient.EPA_REPORT_QTY = content;
            ingredient.DOT_REPORT_QTY = content;
            ingredient.PRCNT_VOL_VALUE = content;
            ingredient.PRCNT_VOL_WEIGHT = content;
            ingredient.CHEM_MFG_COMP_NAME = content;
            ingredient.ODS_IND = content;
            ingredient.OTHER_REC_LIMITS = content;
            msds.ingredientsList.Add(ingredient);
            
            //contractor information
            MsdsContractors contractor = new MsdsContractors();
            contractor.CT_NUMBER = content;
            contractor.CT_CAGE = content;
            contractor.CT_CITY = content;
            contractor.CT_COMPANY_NAME = content;
            contractor.CT_COUNTRY = content;
            contractor.CT_PO_BOX = content;
            contractor.CT_PHONE = content;
            contractor.PURCHASE_ORDER_NO = content;
            contractor.CT_STATE = content;
            msds.contractorsList.Add(contractor);

            //radiological info
            msds.NRC_LP_NUM = content;
            msds.OPERATOR = content;
            msds.RAD_AMOUNT_MICRO = content;
            msds.RAD_FORM = content;
            msds.RAD_CAS = content;
            msds.RAD_NAME = content;
            msds.RAD_SYMBOL = content;
            msds.REP_NSN = content;
            msds.SEALED = content;

            //transportation
            msds.AF_MMAC_CODE = content;
            msds.CERTIFICATE_COE = content;
            msds.COMPETENT_CAA = content;
            msds.DOD_ID_CODE = content;
            msds.DOT_EXEMPTION_NO = content;
            msds.DOT_RQ_IND = content;
            msds.EX_NO = content;
            msds.TRAN_FLASH_PT_TEMP = content;
            msds.HIGH_EXPLOSIVE_WT = this.getInt(content);
            msds.LTD_QTY_IND = content;
            msds.MAGNETIC_IND = content;
            msds.MAGNETISM = content;
            msds.MARINE_POLLUTANT_IND = content;
            msds.NET_EXP_QTY_DIST = this.getInt(content);
            msds.NET_EXP_WEIGHT = content;
            msds.NET_PROPELLANT_WT = content;
            msds.NOS_TECHNICAL_SHIPPING_NAME = content;
            msds.TRANSPORTATION_ADDITIONAL_DATA = content;


            //dot psn
            msds.DOT_HAZARD_CLASS_DIV = content;
            msds.DOT_HAZARD_LABEL = content;
            msds.DOT_MAX_CARGO = content;
            msds.DOT_MAX_PASSENGER = content;
            msds.DOT_PACK_BULK = content;
            msds.DOT_PACK_EXCEPTIONS = content;
            msds.DOT_PACK_NONBULK = content;
            msds.DOT_PROP_SHIP_NAME = content;
            msds.DOT_PROP_SHIP_MODIFIER = content;
            msds.DOT_PSN_CODE = content;
            msds.DOT_SPECIAL_PROVISION = content;
            msds.DOT_SYMBOLS = content;
            msds.DOT_UN_ID_NUMBER = content;
            msds.DOT_WATER_OTHER_REQ = content;
            msds.DOT_WATER_VESSEL_STOW = content;
            msds.DOT_PACK_GROUP = content;

            //afjm psn
            msds.AFJM_HAZARD_CLASS = content;
            msds.AFJM_PACK_PARAGRAPH = content;
            msds.AFJM_PACK_GROUP = content;
            msds.AFJM_PROP_SHIP_NAME = content;
            msds.AFJM_PROP_SHIP_MODIFIER = content;
            msds.AFJM_PSN_CODE = content;
            msds.AFJM_SPECIAL_PROV = content;
            msds.AFJM_SUBSIDIARY_RISK = content;
            msds.AFJM_SYMBOLS = content;
            msds.AFJM_UN_ID_NUMBER = content;
            

            //iata psn
            msds.IATA_CARGO_PACKING = content;
            msds.IATA_HAZARD_CLASS = content;
            msds.IATA_HAZARD_LABEL = content;
            msds.IATA_PACK_GROUP = content;
            msds.IATA_PASS_AIR_PACK_LMT_INSTR = content;
            msds.IATA_PASS_AIR_PACK_LMT_PER_PKG = content;
            msds.IATA_PASS_AIR_PACK_NOTE = content;
            msds.IATA_PROP_SHIP_NAME = content;
            msds.IATA_PROP_SHIP_MODIFIER = content;
            msds.IATA_CARGO_PACK_MAX_QTY = content;
            msds.IATA_PSN_CODE = content;
            msds.IATA_PASS_AIR_MAX_QTY = content;
            msds.IATA_SPECIAL_PROV = content;
            msds.IATA_SUBSIDIARY_RISK = content;
            msds.IATA_UN_ID_NUMBER = content;



            //imo psn           
            msds.IMO_EMS_NO = content;
            msds.IMO_HAZARD_CLASS = content;
            msds.IMO_IBC_INSTR = content;
            msds.IMO_LIMITED_QTY = content;
            msds.IMO_PACK_GROUP = content;
            msds.IMO_PACK_INSTRUCTIONS = content;
            msds.IMO_PACK_PROVISIONS = content;
            msds.IMO_PROP_SHIP_NAME = content;
            msds.IMO_PROP_SHIP_MODIFIER = content;
            msds.IMO_PSN_CODE = content;
            msds.IMO_SPECIAL_PROV = content;
            msds.IMO_STOW_SEGR = content;
            msds.IMO_SUBSIDIARY_RISK = content;
            msds.IMO_TANK_INSTR_IMO = content;
            msds.IMO_TANK_INSTR_PROV = content;
            msds.IMO_TANK_INSTR_UN = content;
            msds.IMO_UN_NUMBER = content;
            msds.IMO_IBC_PROVISIONS = content;
            

            //item description
            msds.ITEM_MANAGER = content;
            msds.ITEM_NAME = content;
            msds.SPECIFICATION_NUMBER = content;
            msds.TYPE_GRADE_CLASS = content;
            msds.UNIT_OF_ISSUE = content;
            msds.QUANTITATIVE_EXPRESSION = content;
            msds.UI_CONTAINER_QTY = content;
            msds.TYPE_OF_CONTAINER = content;
            msds.BATCH_NUMBER = content;
            msds.LOT_NUMBER = content;
            msds.LOG_FLIS_NIIN_VER = content;
            msds.LOG_FSC = this.getInt(content);
            msds.NET_UNIT_WEIGHT = content;
            msds.SHELF_LIFE_CODE = content;
            msds.SPECIAL_EMP_CODE = content;
            msds.UN_NA_NUMBER = content;
            msds.UPC_GTIN = content;


            //label information
            msds.COMPANY_CAGE_RP = content;
            msds.COMPANY_NAME_RP = content;
            msds.LABEL_EMERG_PHONE = content;
            msds.LABEL_ITEM_NAME = content;
            msds.LABEL_PROC_YEAR = content;
            msds.LABEL_PROD_IDENT = content;
            msds.LABEL_PROD_SERIALNO = content;
            msds.LABEL_SIGNAL_WORD = content;
            msds.LABEL_STOCK_NO = content;
            msds.SPECIFIC_HAZARDS = content;


            //disposal info
            msds.DISPOSAL_ADD_INFO = content;
            msds.EPA_HAZ_WASTE_CODE = content;
            msds.EPA_HAZ_WASTE_IND = content;
            msds.EPA_HAZ_WASTE_NAME = content;

            //doc types
            msds.MANUFACTURER_LABEL = new byte[1];
            msds.MSDS_TRANSLATED = new byte[1];
            msds.NESHAP_COMP_CERT = null;
            msds.OTHER_DOCS = null;
            msds.PRODUCT_SHEET = null;
            msds.TRANSPORTATION_CERT = null;
            
            filename = null;
            msds.msds_translated_filename = "blank.txt";
            msds.neshap_comp_filename = filename;
            msds.other_docs_filename = filename;
            msds.product_sheet_filename = filename;
            msds.transportation_cert_filename = filename;
            msds.manufacturer_label_filename = "blank.txt";

            
            DatabaseManager manager = new DatabaseManager(connectionString);

            manager.insertMSDS(msds, true);
            Assert.Pass();
        }

        private int getInt(string s)
        {
            if (s == null || s.Equals(""))
                return 0;
            else
                return Int32.Parse(s);

        }

        [Test]
        public void TestgetMSDSRecordforSerialNumber()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            String msdsSerNo = null; 
            MsdsRecord temp = null;

            // Get a random MSDS record where the CAGE has been edited            
            string sql = "SELECT TOP 1 a.msdsserno, a.CAGE AS orgCAGE, b.CAGE AS newCAGE "
                + "FROM msds a, msds b WHERE a.MSDSSERNO = b.MSDSSERNO "
                + "AND a.manually_entered = 0 AND b.manually_entered = 1 "
                + "AND a.CAGE != b.CAGE and b.msds_id = "
                + "(SELECT MAX(msds_id) from msds where MSDSSERNO = b.MSDSSERNO) "
                + "ORDER BY NEWID();";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                msdsSerNo = rdr["MSDSSERNO"].ToString();
            }

            Console.WriteLine(msdsSerNo + " - " + rdr["orgCAGE"].ToString()
                 + " - " + rdr["newCAGE"].ToString());

            cmd.Dispose();
            conn.Close();
            conn.Dispose();

            // Get latest version of MSDS
            MsdsRecord latest = manager.getMSDSRecordforSerialNumber(msdsSerNo);
            Console.WriteLine(latest.CAGE);

            // Get orginal version of MSDS
            MsdsRecord original = manager.getMSDSRecordforSerialNumber(msdsSerNo, "N");
            Console.WriteLine(original.CAGE);
            Assert.AreNotEqual(original.CAGE, latest.CAGE);

            // Get edited version of MSDS
            MsdsRecord edited = manager.getMSDSRecordforSerialNumber(msdsSerNo, "Y");
            Console.WriteLine(edited.CAGE);
            Assert.AreNotEqual(original.CAGE, edited.CAGE);

            Assert.Pass();

            
        }
    }
}
