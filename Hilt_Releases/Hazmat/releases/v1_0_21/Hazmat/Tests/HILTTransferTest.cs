﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Collections;
using System.Runtime.CompilerServices;
using AESCommon.SafeFile;

namespace HAZMATUnit
{

    [TestFixture]
    class HILTTransferTest
    {
        private List<string> decantingPathList = new List<string>();

        //server db connection
        private string connectionString = Configuration.ConnectionInfo;

        //handheld db template path
        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");

        //downloadPath / uploadPath

        [Test]
        public void testOffloadDownload()
        {
            string destination = Path.GetTempPath();
            destination += "hazmat_template.s3db";

            //remove and copy template if it exists
            if (File.Exists(destination))
            {
                File.Delete(destination);
                
            }

            File.Copy(dbTemplate, destination);

            int Offset = 0;

            DatabaseManager man = new DatabaseManager();

            Hashtable downloadTable = new Hashtable();

            string tempFileLocation = System.IO.Path.GetTempFileName();
            Debug.WriteLine(tempFileLocation);
            HILTTransfer tran = new HILTTransfer(Path.GetTempPath(), Path.GetTempPath());

            int ChunkSize = 16 * 1024;

            //List<int> offloadList = tran.(new DateTime(2000, 1, 1), new DateTime(2020, 1, 1));
            //Debug.WriteLine("Offload List count: " + decantingList.Count);
           
                //string hash = this.buildFileHash(tempFileLocation);
                string guid = System.Guid.NewGuid().ToString();

                long FileSize = tran.GetFileSize(1, guid, HILTTransfer.ClipboardType.Offload, new DateTime(2000, 1, 1), new DateTime(2020, 1, 1));

                // open a file stream for the file we will write to
                using (FileStream fs = new FileStream(tempFileLocation, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    fs.Seek(Offset, SeekOrigin.Begin);

                    // download the chunks from the web service one by one, until all the bytes have been read, meaning the entire file has been downloaded.
                    while (Offset < FileSize)
                    {

                        try
                        {
                            // although the DownloadChunk returns a byte[], it is actually sent using MTOM because of the configuration settings. 
                            byte[] Buffer = tran.DownloadChunk(HILTTransfer.ClipboardType.Offload, 1, guid, Offset, ChunkSize);
                            fs.Write(Buffer, 0, Buffer.Length);
                            Offset += Buffer.Length;	// save the offset position for resume
                        }
                        catch (Exception ex)
                        {
                            // swallow the exception and try again
                            Debug.WriteLine("Exception: " + ex.ToString());
                            Assert.Fail(ex.Message);
                            throw new Exception(ex.Message);
                        }

                    }
                }
            

            Assert.Pass("Offload download Passed!");
        }


        [Test]
        public void testInsurvDownload()
        {
            string destination = Path.GetTempPath();
            destination += "hazmat_template.s3db";

            //remove and copy template if it exists
            if (File.Exists(destination))
            {
                File.Delete(destination);                
            }

            File.Copy(dbTemplate, destination);

            int Offset = 0;

            DatabaseManager man = new DatabaseManager();

            Hashtable downloadTable = new Hashtable();

            string tempFileLocation = System.IO.Path.GetTempFileName();
            Debug.WriteLine(tempFileLocation);
            HILTTransfer tran = new HILTTransfer(Path.GetTempPath(), Path.GetTempPath());

            int ChunkSize = 16 * 1024;

            string hash = this.buildFileHash(tempFileLocation);
            string guid = System.Guid.NewGuid().ToString();

            long FileSize = tran.GetFileSize(1, guid, HILTTransfer.ClipboardType.INSURV, new DateTime(2000, 1, 1), new DateTime(2020, 1, 1));
            Console.WriteLine("File size: " + FileSize);

            // open a file stream for the file we will write to
            using (FileStream fs = new FileStream(tempFileLocation, FileMode.OpenOrCreate, FileAccess.Write))
            {
                fs.Seek(Offset, SeekOrigin.Begin);

                // download the chunks from the web service one by one, until all the bytes have been read, meaning the entire file has been downloaded.
                while (Offset < FileSize)
                {
                    try
                    {
                        Debug.WriteLine("Starting Audit Download - " + Offset + " of " + FileSize);
                        // although the DownloadChunk returns a byte[], it is actually sent using MTOM because of the configuration settings. 
                        byte[] Buffer = tran.DownloadChunk(HILTTransfer.ClipboardType.INSURV, 1, guid, Offset, ChunkSize);
                        fs.Write(Buffer, 0, Buffer.Length);
                        Offset += Buffer.Length;	// save the offset position for resume
                    }
                    catch (Exception ex)
                    {
                        // swallow the exception and try again
                        Assert.Fail(ex.Message);
                        Console.WriteLine("Exception: " + ex.ToString());
                        throw new Exception(ex.Message + "\n\t" + ex.StackTrace);
                    }

                }
            }


            Assert.Pass("Insurv download Passed!");


        }

        [Test]
        public void testDecantingDownload()
        {
            string destination = Path.GetTempPath();
            destination += "hazmat_template.s3db";

            //remove and copy template if it exists
            if (File.Exists(destination))
            {
                File.Delete(destination);
                
            }


            File.Copy(dbTemplate, destination);

            int Offset = 0;

            DatabaseManager man = new DatabaseManager();

            Hashtable downloadTable = new Hashtable();

            string tempFileLocation = System.IO.Path.GetTempFileName();
            Debug.WriteLine(tempFileLocation);
            HILTTransfer tran = new HILTTransfer(Path.GetTempPath(), Path.GetTempPath());

            int ChunkSize = 16 * 1024;            

            List<int> decantingList = tran.getDecantingList(new DateTime(2000,1,1),new DateTime(2020,1,1));
            Debug.WriteLine("Decanting Task count: " + decantingList.Count);


            foreach (int id in decantingList)
            {
                //string hash = this.buildFileHash(tempFileLocation);
                string guid = System.Guid.NewGuid().ToString();

                long FileSize = tran.GetFileSize(id, guid, HILTTransfer.ClipboardType.Decanting, new DateTime(2000, 1, 1), new DateTime(2020, 1, 1));

                // open a file stream for the file we will write to
                using (FileStream fs = new FileStream(tempFileLocation, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    fs.Seek(Offset, SeekOrigin.Begin);

                    // download the chunks from the web service one by one, until all the bytes have been read, meaning the entire file has been downloaded.
                    while (Offset < FileSize)
                    {

                        try
                        {
                            // although the DownloadChunk returns a byte[], it is actually sent using MTOM because of the configuration settings. 
                            byte[] Buffer = tran.DownloadChunk(HILTTransfer.ClipboardType.Decanting, id, guid, Offset, ChunkSize);
                            fs.Write(Buffer, 0, Buffer.Length);
                            Offset += Buffer.Length;	// save the offset position for resume
                        }
                        catch (Exception ex)
                        {
                            // swallow the exception and try again
                            Debug.WriteLine("Exception: " + ex.ToString());
                            Assert.Fail(ex.Message);
                            throw new Exception(ex.Message);
                        }

                    }
                }
            }

            Assert.Pass("Decanting Download Passed!");


        }



        [Test]
        public void testAuditDownload()
        {
            string destination = Path.GetTempPath();
            destination += "hazmat_template.s3db";

            //remove and copy template if it exists
            if (File.Exists(destination))
            {
                File.Delete(destination);
                
            }

            File.Copy(dbTemplate, destination);

            int Offset = 0;

            DatabaseManager man = new DatabaseManager();

            Hashtable downloadTable = new Hashtable();

            string tempFileLocation = System.IO.Path.GetTempFileName();
            Debug.WriteLine(tempFileLocation);
            HILTTransfer tran = new HILTTransfer(Path.GetTempPath(), Path.GetTempPath());

            int ChunkSize = 16 * 1024;

            // Pick one Audit at Random
            int id = getInventoryAuditIDForTest();

            string guid = System.Guid.NewGuid().ToString();

            long FileSize = tran.GetFileSize(id, guid, HILTTransfer.ClipboardType.Audit, new DateTime(2000, 1, 1), new DateTime(2020, 1, 1));

            // open a file stream for the file we will write to
            using (FileStream fs = new FileStream(tempFileLocation, FileMode.OpenOrCreate, FileAccess.Write))
            {
                fs.Seek(Offset, SeekOrigin.Begin);

                // download the chunks from the web service one by one, until all the bytes have been read, meaning the entire file has been downloaded.
                while (Offset < FileSize)
                {
                    try
                    {
                        Debug.WriteLine("Starting Audit Download");
                        // although the DownloadChunk returns a byte[], it is actually sent using MTOM because of the configuration settings. 
                        byte[] Buffer = tran.DownloadChunk(HILTTransfer.ClipboardType.Audit, id, guid, Offset, ChunkSize);
                        fs.Write(Buffer, 0, Buffer.Length);
                        Offset += Buffer.Length;	// save the offset position for resume
                    }
                    catch (Exception ex)
                    {
                        // swallow the exception and try again
                        Debug.WriteLine("Exception: " + ex.ToString());
                        Assert.Fail(ex.Message);
                        throw new Exception(ex.Message);
                    }

                }
            }
            //}

            Assert.Pass("Audit Download Passed!");


        }



        [Test]
        public void testRecurringAuditDownload()
        {
            string destination = Path.GetTempPath();
            destination += "hazmat_template.s3db";

            //remove and copy template if it exists
            if (File.Exists(destination))
            {
                File.Delete(destination);
                
            }

            File.Copy(dbTemplate, destination);

            int Offset = 0;

            DatabaseManager man = new DatabaseManager();

            int inventory_audit_id = man.createAuditForTest();
            int recurring_audit_id = man.createRecurringAuditForTest(inventory_audit_id);
            int workcenterId = man.createWorkcenterForTest();
            int location_id = man.insertLocation("UNITTEST", workcenterId);

            Hashtable downloadTable = new Hashtable();

            string tempFileLocation = System.IO.Path.GetTempFileName();
            Debug.WriteLine(tempFileLocation);
            HILTTransfer tran = new HILTTransfer(Path.GetTempPath(), Path.GetTempPath());

            int ChunkSize = 16 * 1024;

            string hash = this.buildFileHash(tempFileLocation);
            string guid = System.Guid.NewGuid().ToString();

            long FileSize = tran.GetFileSize(1, guid,
                    HILTTransfer.ClipboardType.RecurringAudit, new DateTime(2000, 1, 1),
                    new DateTime(2020, 1, 1));

            // open a file stream for the file we will write to
            using (FileStream fs = new FileStream(tempFileLocation, FileMode.OpenOrCreate, FileAccess.Write))
            {
                fs.Seek(Offset, SeekOrigin.Begin);

                // download the chunks from the web service one by one, until all the bytes have been read, meaning the entire file has been downloaded.
                while (Offset < FileSize)
                {
                    try
                    {
                        // although the DownloadChunk returns a byte[], it is actually sent using MTOM because of the configuration settings. 
                        byte[] Buffer = tran.DownloadChunk(HILTTransfer.ClipboardType.RecurringAudit, recurring_audit_id, guid, Offset, ChunkSize);
                        fs.Write(Buffer, 0, Buffer.Length);
                        Offset += Buffer.Length;	// save the offset position for resume
                    }
                    catch (Exception ex)
                    {
                        // swallow the exception and try again
                        Debug.WriteLine("Exception: " + ex.ToString());
                        Assert.Fail(ex.Message);
                        throw new Exception(ex.Message);
                    }
                }
            }

            Assert.Pass("Recurring Audit Download Passed!");

        }
        

        [Test]
        public void testImportAudit()
        {




        }


        [Test]
        public void testImportDecanting()
        {




        }

      

        protected string buildFileHash(string path)
        {          

            //FileStream file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096))
            {
                hash = md5.ComputeHash(fs);
                fs.Close();
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        [Test]
        public void testIsPathRooted() {
            // Get a list of invalid path characters.
            char[] invalidPathChars = Path.GetInvalidPathChars();

            Debug.WriteLine("The following characters are invalid in a path:");

            ShowChars(invalidPathChars);
            Console.WriteLine();

            //// Get a list of invalid file characters.
            //char[] invalidFileChars = Path.GetInvalidFileNameChars();

            //Debug.WriteLine("The following characters are invalid in a filename:");
            //ShowChars(invalidFileChars);

            string testPath = "C:/pathTest";
            Debug.WriteLine(testPath + " - " + Path.IsPathRooted(testPath));
            Debug.WriteLine("File - " + SafeFile.makeFileNameSafe(testPath));
            Debug.WriteLine("Path - " + SafeFile.makePathSafe(testPath) + "\n");

            testPath = "C:\\pathTest";
            Debug.WriteLine(testPath + " - " + Path.IsPathRooted(testPath));
            Debug.WriteLine("File - " + SafeFile.makeFileNameSafe(testPath));
            Debug.WriteLine("Path - " + SafeFile.makePathSafe(testPath) + "\n");

            testPath = "\\pathTest";
            Debug.WriteLine(testPath + " - " + Path.IsPathRooted(testPath));
            Debug.WriteLine("File - " + SafeFile.makeFileNameSafe(testPath));
            Debug.WriteLine("Path - " + SafeFile.makePathSafe(testPath) + "\n");

            testPath = "\\\\pathTest";
            Debug.WriteLine(testPath + " - " + Path.IsPathRooted(testPath));
            Debug.WriteLine("File - " + SafeFile.makeFileNameSafe(testPath));
            Debug.WriteLine("Path - " + SafeFile.makePathSafe(testPath) + "\n");

            testPath = ".\\pathTest";
            Debug.WriteLine(testPath + " - " + Path.IsPathRooted(testPath));
            Debug.WriteLine("File - " + SafeFile.makeFileNameSafe(testPath));
            Debug.WriteLine("Path - " + SafeFile.makePathSafe(testPath) + "\n");

            testPath = "..\\\\pathTest";
            Debug.WriteLine(testPath + " - " + Path.IsPathRooted(testPath));
            Debug.WriteLine("File - " + SafeFile.makeFileNameSafe(testPath));
            Debug.WriteLine("Path - " + SafeFile.makePathSafe(testPath) + "\n");
        }

        public static void ShowChars(char[] charArray) {
            Debug.WriteLine("Char\tHex Value");
            // Display each invalid character to the console.
            foreach (char someChar in charArray) {
                if (Char.IsWhiteSpace(someChar)) {
                    string temp = String.Format(",\t{0:X4}", (int)someChar);
                    Debug.WriteLine(temp);
                }
                else {
                    string temp = String.Format("{0:c},\t{1:X4}", someChar, (int)someChar);
                    Debug.WriteLine(temp);
                }
            }
        }

        private int getInventoryAuditIDForTest() {
            // Gets a random inventory audit from the test database 
            int randomID = 0;

            DatabaseManager manager = new DatabaseManager(connectionString);

            string sql = "SELECT TOP 1 inventory_audit_id "
                + "FROM inventory_audits WHERE inventory_audit_id IN "
                + "(SELECT DISTINCT inventory_audit_id FROM locations_in_audit) "
                + "AND deleted = 0 "
                + "ORDER BY NEWID()";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            int.TryParse(cmd.ExecuteScalar().ToString(), out randomID);

            Console.WriteLine("Audit ID: " + randomID);

            return randomID;
        }

        private int getRecurringAuditIDForTest() {
            // Gets a random recurring audit item from the test database 
            int randomID = 0;

            string sql = "SELECT TOP 1 recurring_audit_id "
                + "FROM recurring_audits WHERE recurring_audit_id IN "
                + "(SELECT DISTINCT recurring_audit_id FROM locations_in_recurring_audits) "
                + "AND deleted = 0 "
                + "ORDER BY NEWID()";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            object o = cmd.ExecuteScalar();

            if ( o == null) {
                throw new Exception("No recurring_audit_id found"
                    + "\n\tCheck that there is at least one active recurring with locations");
            }

            int.TryParse(o.ToString(), out randomID);

            Console.WriteLine("Recurring Audit ID: " + randomID);

            // Now delete the audit id from the audits_in_recurring table
            cmd.CommandText = "DELETE FROM audits_in_recurring WHERE recurring_id = " + randomID.ToString();
            cmd.ExecuteNonQuery();

            conn.Close();

            return randomID;
        }
    }
}
