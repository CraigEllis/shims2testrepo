﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Data.SqlClient;

namespace HAZMATUnit
{

    [TestFixture]
    class DecantingTests
    {

        private string connectionString = Configuration.ConnectionInfo;


        [Test]
        public void testDeleteTask()
        {
            new DatabaseManager(connectionString).deleteTask(10090900);

            Assert.Pass("deleteTask passed!");

        }

        [Test]
        public void Test_getHazardIdForMfgAndIncompatibleInventory()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            int mfg_catalog_id = manager.getExistingMfgCatalogId();

            Object[] hazardObjects = manager.getHazardListsForInventoryId(mfg_catalog_id);

            List<int> hazardList = (List<int>)hazardObjects[0];
            List<string> hccList = (List<string>)hazardObjects[1];

            int location_id = manager.getExistingLocationId();

            DataTable dt = manager.getIncompatibilitiesInventoryList(hccList, hazardList, location_id);

            Assert.Pass();

        }

        [Test]
        public void Test_DecantingNewGetFilteredInventoryAvailable()
        {

            //Test grabbing all records and each filter column.
            List<int> inventoryList = new List<int>();
            inventoryList.Add(1);
            inventoryList.Add(5);

            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            string locationName = "";
            string workcenter_id = "";
            DataTable dt = manager.getFilteredInventoryAvailable(filterColumn, filterText, locationName, workcenter_id, inventoryList, false, "test");
            Console.WriteLine("Test_DecantingNewGetFilteredInventoryAvailable rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getFilteredInventoryAvailable(filterColumn, filterText, locationName, workcenter_id, inventoryList, false, "test");
            Console.WriteLine("Test_DecantingNewGetFilteredInventoryAvailable rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getFilteredInventoryAvailable(filterColumn, filterText, locationName, workcenter_id, inventoryList, false, "test");
            Console.WriteLine("Test_DecantingNewGetFilteredInventoryAvailable rows:" + dt.Rows.Count);
            filterColumn = "SPMIG";
            filterText = "A";
            dt = manager.getFilteredInventoryAvailable(filterColumn, filterText, locationName, workcenter_id, inventoryList, false, "test");
            Console.WriteLine("Test_DecantingNewGetFilteredInventoryAvailable rows:" + dt.Rows.Count);
            filterColumn = "";
            filterText = "";
            locationName = "A";
            workcenter_id = "";
            dt = manager.getFilteredInventoryAvailable(filterColumn, filterText, locationName, workcenter_id, inventoryList, false, "test");
            Console.WriteLine("Test_DecantingNewGetFilteredInventoryAvailable rows:" + dt.Rows.Count);
            filterColumn = "";
            filterText = "";
            locationName = "";
            workcenter_id = "2";
            dt = manager.getFilteredInventoryAvailable(filterColumn, filterText, locationName, workcenter_id, inventoryList, false, "test");
            Console.WriteLine("Test_DecantingNewGetFilteredInventoryAvailable rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void Test_CreatingDecantingTaskAndDecantingInventory()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            //create task
            int decanting_task_id = manager.createDecantingTask("UNIT TEST");

            DataTable table = new DatabaseManager(connectionString).getTasks();

            if (table.Rows.Count < 1)
            {
                Assert.Fail("getTasks fail");
            }

            List<DecantingInfo> decantingInfoList = new List<DecantingInfo>();

            DecantingInfo info = new DecantingInfo();
            info.inventory_id = manager.getExistingInventoryId();
            info.ContainerType = "UT";
            info.Volume = "10 GL";

            decantingInfoList.Add(info);

            //insert into inventory_in_decant_task
            manager.insertIntoDecantedInventoryFromDecantNew(decantingInfoList, decanting_task_id);

            DataTable dt = manager.getInventoryForDecantTask(decanting_task_id);

            Assert.AreEqual(1, dt.Rows.Count);

            DataRow r = dt.Rows[0];

            int inventory_in_decant_task_id = Int32.Parse(r["inventory_in_decant_task_id"].ToString());
            //verify
            Assert.AreEqual(info.inventory_id, Int32.Parse(r["inventory_id"].ToString()));
            Assert.AreEqual(info.ContainerType, r["decant_container_type"].ToString());
            Assert.AreEqual(info.Volume, r["decant_container_volume"].ToString());

            Console.WriteLine("Test_DecantingDetailGetInventoryForDecantTask rows:" + dt.Rows.Count);

            int location_id = manager.getExistingLocationId();

            List<int> ids = manager.insertDecantedInventoryFromWebDecanting(null, inventory_in_decant_task_id, 2010, location_id);

            int decanted_inventory_id = ids[0];
            int inventory_id = ids[1];

            Assert.True(inventory_id != 0);
            Assert.True(decanted_inventory_id != 0);

            DataTable decanted_inventory = manager.getDecantedInventoryForInvInDecantTask(inventory_in_decant_task_id);

            Assert.AreEqual(1, decanted_inventory.Rows.Count);

            DataRow decanted = decanted_inventory.Rows[0];
            //verify
            Assert.AreEqual(2010, Int32.Parse(decanted["qty"].ToString()));
            Assert.AreEqual(location_id, Int32.Parse(decanted["location_id"].ToString()));

            //cleanup
            manager.deleteInventory(inventory_id);
            manager.deleteDecantingTask(decanting_task_id);

            Assert.Pass();
        }


       // [Test]
       // public void TestCompletedDecantedInventory()
       // {
       //     DatabaseManager manager = new DatabaseManager(connectionString);

       //     // insert into decanted inventory
       //     SqlConnection con = new SqlConnection(connectionString);
       //     con.Open();            

       //     //Grab a mfg/niin to use in the test.
       //     DataTable mfg = manager.getMFGCatalogCollection("", "");
       //     int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
       //     int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
       //     int location_id = this.getLocation(con);
             
       //     //insert unlabeld (10)
       //     List<string> containerList = insertUnlabeledIntoDecanted(con, "test container unit testing", "test volume unit testing adam hale", mfg_catalog_id, niin_catalog_id, 10, location_id);                                    

       //     //get all decanted
       //     List<DecantInventoryData> decantList = new DatabaseManager().getCompletedDecantedInventoryAll();           

       //     Console.Out.WriteLine("MFG:" + mfg_catalog_id + " NIIN:" + niin_catalog_id);
       //     Console.Out.WriteLine(containerList.Count);
       //     Console.Out.WriteLine(decantList.Count);


       //     int count = 0;
       //     int foundCount = 0;
       //     foreach (DecantInventoryData r in decantList)
       //     {
       //         foreach(string s in containerList)
       //         {
       //             if (s.ToLower().Equals(r.Container_type.ToLower()))
       //             {                      
       //                 foundCount = r.Qty;
       //                 count++;                                                                     
       //             }
                  
       //         }
       //     }

       //     //found count should be 10
       //     Console.Out.WriteLine("FOUND COUNT: " + foundCount);
       //     //total count should be 10
       //     Console.Out.WriteLine("COUNT: " + count);

       //     if (count != 10)
       //     {
       //         this.deleteDecanted(con);
       //         Assert.Fail("Did not find 10 rows inserted in the decanted_inventory table.");
       //     }

       //     //this is the qty which came from the method..  This value should be the same value we inserted (count)
       //     if (foundCount != 10)
       //     {
       //         this.deleteDecanted(con);
       //         Assert.Fail("Did not find 10 rows inserted in the decanted_inventory table.");
       //     }

       ////   DataTable  dt = null;// manager.getCompletedDecantedInventoryFromNiinCatalogId(niin_catalog_id);

       //   List<DecantInventoryData> decantingList = new DatabaseManager().getCompletedDecantedInventoryFromNiinCatalogId(niin_catalog_id);
          
       //     //Confirm that the labled is in there but not the others
       //     count = 0;
       //     foreach (DecantInventoryData r in decantingList)
       //     {
       //         foreach (string s in containerList)
       //         {
       //             if (s.ToLower().Equals(r.Container_type.ToLower()))
       //             {
       //                 count++;

       //             }                  
       //         }
       //     }

       //     if (count != 10)
       //     {
       //         this.deleteDecanted(con);
       //         Assert.Fail("Did not find 10 rows based on the niin_catalog_id");
       //     }

       //     // blow out the decanted inventory we inserted
           
       //      this.deleteDecanted(con);            

       //     Assert.Pass();
       // }

        [CoverageExclude]
        private List<string> insertUnlabeledIntoDecanted(SqlConnection con, string containerType, string containerVolume, int mfgId, int niinId, int qty, int location_id)
        {
            string stmt = "insert into decanted_inventory (container_type, container_volume, mfg_catalog_id, niin_catalog_id, location_id) VALUES (@type, @volume, @mfg, @niin, @locationId);SELECT SCOPE_IDENTITY()";
            SqlCommand cmd = new SqlCommand(stmt, con);
            List<string> containerList = new List<string>();
            for (int i = 0; i < qty; i++)
            {
                cmd.Parameters.AddWithValue("@type", containerType);
                cmd.Parameters.AddWithValue("@volume", containerVolume);
                cmd.Parameters.AddWithValue("@mfg", mfgId);
                cmd.Parameters.AddWithValue("@niin", niinId);
                cmd.Parameters.AddWithValue("@locationId", location_id);

                object o = cmd.ExecuteScalar();
                int decanted_inventory_id = Convert.ToInt32(o);
                containerList.Add(containerType);

                cmd.Parameters.Clear();
            }
            
            //con.Close();
            return containerList;
            
        }
        [CoverageExclude]
        private void labelDecanted(SqlConnection con, int decantId, int labelId)
        {
            string stmt = "UPDATE decanted_inventory set decanted_label_id=@decanted_label_id WHERE decanted_inventory_id=@decanted_inventory_id;";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@decanted_label_id", labelId);
            cmd.Parameters.AddWithValue("@decanted_inventory_id", decantId);
            cmd.ExecuteNonQuery();
                       
        }
        [CoverageExclude]
        private void deleteDecanted(SqlConnection con)
        {
            string stmt = "delete from decanted_inventory where container_volume=@container";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@container", "test volume unit testing adam hale");
            cmd.ExecuteNonQuery();
            con.Close();

        }
        [CoverageExclude]
        private int getLocation(SqlConnection con)
        {

            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());           

            return locationId;

        }

       

    }
}
