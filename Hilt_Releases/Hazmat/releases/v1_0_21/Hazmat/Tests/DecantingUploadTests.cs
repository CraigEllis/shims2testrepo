﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;

namespace HAZMATUnit
{

    [TestFixture]
    class DecantingUploadTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");

        [Test]
        public void testImportDecantInventory()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);


            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();

            
            string stmt = "";
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand(stmt, con);

            int decant_id = 1000;

            //insert decanted inventory row in handheld db
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
            liteCon.Open();
            stmt = "insert into decanted_inventory (decanted_inventory_id, location_id, qty, inventory_in_decant_task_id) VALUES (@decanted_inventory_id, @location_id, @qty, @inventory_in_decant_task_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(stmt, liteCon);
            liteCmd.Parameters.AddWithValue("@decanted_inventory_id", decant_id);
            liteCmd.Parameters.AddWithValue("@location_id", location_id);
            liteCmd.Parameters.AddWithValue("@qty", 2011);

            int? inventory_in_decant_task_id = null;
            //try
            //{
            //    inventory_in_decant_task_id = manager.getExistingInventoryInDecantTaskId();
            //}
            //catch { }

            liteCmd.Parameters.AddWithValue("@inventory_in_decant_task_id", inventory_in_decant_task_id);
            
           
            liteCmd.ExecuteNonQuery();
            liteCon.Close();

            Debug.WriteLine(decant_id);

            con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();

            //insert decant_inventory data           
            List<int> insertedInvIds =  new HHDatabaseManager().importDecantInventory(con, tran, tempFile);            
            tran.Commit();
            con.Close();

            con = new SqlConnection(connectionString);
            con.Open();


            //check the database on the server
            cmd = new SqlCommand("select * from decanted_inventory where inventory_in_decant_task_id IS NULL", con);
           // cmd.Parameters.AddWithValue("@inventory_in_decant_task_id", inventory_in_decant_task_id);
            SqlDataReader rdr = cmd.ExecuteReader();
            bool pass = false;
            string msg = "";
            int count = 0;
            while (rdr.Read())
            {

                count++;

               

            }

            //test passed
            if (count > 0)
            {
                con.Close();
                removeTestData(insertedInvIds);
                File.Delete(tempFile);
                Assert.True(true, "decanting upload data inserted successfully");
               // Assert.Pass("decanting upload data inserted successfully");
            }
            else
            {
                con.Close();
                removeTestData(insertedInvIds);
                File.Delete(tempFile);
                Assert.Fail("Decanting Upload fail: Unable to upload the database back into the system - " + msg);

            }

            con.Close();

        }


        private void removeTestData(List<int> insertedInvIds)
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("delete from decanted_inventory where inventory_in_decant_task_id IS NULL", con);
           
            cmd.ExecuteNonQuery();

            con.Close();

            DatabaseManager manager = new DatabaseManager();
            //delete inserted inventory
            foreach (int inventory_id in insertedInvIds)
            {
                manager.deleteInventory(inventory_id);
            }

        }

        private int getLocation()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }


    }
}
