﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

namespace HAZMATUnit
{
    [TestFixture]
    class InventoryAuditDiscrepanciesTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestDiscrepancyListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";

            int location_id = manager.getExistingLocationId();
            int inventory_audit_id = manager.getExistingInventoryAuditId();

            DataTable dt = manager.getDiscrepanciesInventoryByLocationAndAudit(location_id, inventory_audit_id, filterColumn, filterText);
            Console.WriteLine("TestDiscrepancyListLoad rows:" + dt.Rows.Count);
            dt = manager.getDiscrepanciesInventoryByLocationAndAudit(location_id, inventory_audit_id, filterColumn, filterText);
            Console.WriteLine("TestDiscrepancyListLoad rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "0";
            dt = manager.getDiscrepanciesInventoryByLocationAndAudit(location_id, inventory_audit_id, filterColumn, filterText);
            Console.WriteLine("TestDiscrepancyListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getDiscrepanciesInventoryByLocationAndAudit(location_id, inventory_audit_id, filterColumn, filterText);
            Console.WriteLine("TestDiscrepancyListLoad rows:" + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getDiscrepanciesInventoryByLocationAndAudit(location_id, inventory_audit_id, filterColumn, filterText);
            Console.WriteLine("TestDiscrepancyListLoad rows:" + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "C";
            dt = manager.getDiscrepanciesInventoryByLocationAndAudit(location_id, inventory_audit_id, filterColumn, filterText);
            Console.WriteLine("TestDiscrepancyListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void Test_acceptInventoryChangeRecordCount()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            int mfg_catalog_id  = manager.getExistingMfgCatalogId();
            int inventory_audit_id = manager.getExistingInventoryAuditId();

            //create an inventory record.
            int inventory_id = manager.insertInventory(DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(),
                0, location_id, 2011, null, true, mfg_catalog_id, null, null, null, "HME", null);

            int counted_qty = 55555;

            int inventory_audit_cr_id = manager.insertInventoryAuditCr(new List<VolumeOffloadItem>(),inventory_audit_id, location_id, 
                mfg_catalog_id, counted_qty, true, null, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), 0, "EA", 1, "16 OZ", "HME", null, null);

            string acceptance_reason = "GBI";
            string acceptance_note = "UNIT TEST";

            manager.acceptInventoryChangeRecordCount(inventory_id, counted_qty, inventory_audit_cr_id, 
           acceptance_reason,  acceptance_note);

            //verify
           DataTable dt = manager.findInventory(inventory_id);
           Assert.AreEqual(1, dt.Rows.Count); 
           
            DataRow inventory = dt.Rows[0];
            Assert.AreEqual(location_id, Int32.Parse(inventory["location_id"].ToString()));
            Assert.AreEqual(mfg_catalog_id, Int32.Parse(inventory["mfg_catalog_id"].ToString()));
            Assert.AreEqual(counted_qty, Int32.Parse(inventory["qty"].ToString())); 

            //verify
            DataTable cr = manager.findInvAuditCr(inventory_audit_cr_id);
            Assert.AreEqual(1, cr.Rows.Count);

            DataRow inventoryCr = cr.Rows[0];
            Assert.AreEqual(location_id, Int32.Parse(inventoryCr["location_id"].ToString()));
            Assert.AreEqual(inventory_audit_id, Int32.Parse(inventoryCr["inventory_audit_id"].ToString()));
            Assert.AreEqual(counted_qty, Int32.Parse(inventoryCr["qty"].ToString()));
            Assert.AreEqual(acceptance_reason, inventoryCr["acceptance_reason"].ToString());
            Assert.AreEqual(acceptance_note, inventoryCr["acceptance_note"].ToString()); 

            //clean up
            manager.deleteInventory(inventory_id);
            manager.deleteInventoryAuditCr(inventory_audit_cr_id);

        }


        [Test]
        public void Test_acceptInventoryGain()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            int inventory_audit_id = manager.getExistingInventoryAuditId();

            DateTime? manufacturer_date = null;
            DateTime? shelf_life_expiration_date = DateTime.Now;

            string alternate_ui = "EA";
            string alternate_um = "12 OZ";
            string atm = "ATM";
            int counted_qty = 2011;

            int inventory_audit_cr_id = manager.insertInventoryAuditCr(new List<VolumeOffloadItem>(),inventory_audit_id, location_id,
               mfg_catalog_id, counted_qty, true, null, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), 0, "EA", 1, "16 OZ", "HME", null, null);

            string acceptance_reason = "GBI";
            string acceptance_note = "UNIT TEST";

           int inventory_id =  manager.acceptInventoryGain(manufacturer_date, shelf_life_expiration_date,
                  location_id, counted_qty, mfg_catalog_id,
                  alternate_ui, alternate_um,
                   inventory_audit_cr_id, acceptance_reason, acceptance_note, "HME", null, null, atm);

           //verify
           DataTable dt = manager.findInventory(inventory_id);
           Assert.AreEqual(1, dt.Rows.Count);

           DataRow inventory = dt.Rows[0];
           Assert.AreEqual(location_id, Int32.Parse(inventory["location_id"].ToString()));
           Assert.AreEqual(mfg_catalog_id, Int32.Parse(inventory["mfg_catalog_id"].ToString()));          
           Assert.AreEqual(counted_qty, Int32.Parse(inventory["qty"].ToString()));
           Assert.AreEqual(alternate_ui, inventory["alternate_ui"].ToString());
           Assert.AreEqual(alternate_um, inventory["alternate_um"].ToString());
           Assert.AreEqual(atm, inventory["atm"].ToString());  

           //verify
           DataTable cr = manager.findInvAuditCr(inventory_audit_cr_id);
           Assert.AreEqual(1, cr.Rows.Count);

           DataRow inventoryCr = cr.Rows[0];
           Assert.AreEqual(location_id, Int32.Parse(inventoryCr["location_id"].ToString()));
           Assert.AreEqual(inventory_audit_id, Int32.Parse(inventoryCr["inventory_audit_id"].ToString()));
           Assert.AreEqual(counted_qty, Int32.Parse(inventoryCr["qty"].ToString()));
           Assert.AreEqual(acceptance_reason, inventoryCr["acceptance_reason"].ToString());
           Assert.AreEqual(acceptance_note, inventoryCr["acceptance_note"].ToString());

           //clean up
           manager.deleteInventory(inventory_id);
           manager.deleteInventoryAuditCr(inventory_audit_cr_id);

        }


        [Test]
        public void Test_acceptInventoryLoss()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            int inventory_audit_id = manager.getExistingInventoryAuditId();

            DateTime? manufacturer_date = null;
            DateTime? shelf_life_expiration_date = DateTime.Now;

            //create an inventory record.
            int inventory_id = manager.insertInventory(DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(),
                0, location_id, 2011, null, true, mfg_catalog_id, null, null, null, "HME", null);

            string alternate_ui = "EA";
            string alternate_um = "12 OZ";

            string acceptance_reason = "GBI";
            string acceptance_note = "UNIT TEST";

            int count = 1;
            DateTime? server_synctime = null;

           int inventory_audit_cr_id= manager.acceptInventoryLoss(inventory_id, inventory_audit_id, 
               location_id, mfg_catalog_id,
                    shelf_life_expiration_date, manufacturer_date, alternate_ui,
                    alternate_um,
                    acceptance_reason, acceptance_note, count, server_synctime, "HME", null);

           //verify
           DataTable dt = manager.findInventory(inventory_id);
           Assert.AreEqual(0, dt.Rows.Count);

           //verify
           DataTable cr = manager.findInvAuditCr(inventory_audit_cr_id);
           Assert.AreEqual(1, cr.Rows.Count);

           DataRow inventoryCr = cr.Rows[0];
           Assert.AreEqual(location_id, Int32.Parse(inventoryCr["location_id"].ToString()));
           Assert.AreEqual(mfg_catalog_id, Int32.Parse(inventoryCr["mfg_catalog_id"].ToString()));
           Assert.AreEqual(inventory_audit_id, Int32.Parse(inventoryCr["inventory_audit_id"].ToString()));
           Assert.AreEqual(0, Int32.Parse(inventoryCr["qty"].ToString()));
           Assert.AreEqual(acceptance_reason, inventoryCr["acceptance_reason"].ToString());
           Assert.AreEqual(acceptance_note, inventoryCr["acceptance_note"].ToString());

           //clean up
           manager.deleteInventory(inventory_id);
           manager.deleteInventoryAuditCr(inventory_audit_cr_id);

        }

        [Test]
        public void Test_insertInventoryAuditCrForDiscrepancy()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            int inventory_audit_id = manager.getExistingInventoryAuditId();

            DateTime? shelf_life_expiration_date = DateTime.Now;

            string alternate_ui = "EA";
            string alternate_um = "12 OZ";

            int counted_qty = 2011;

           int inventory_audit_cr_id = manager.insertInventoryAuditCrForDiscrepancy(inventory_audit_id, location_id, null, 1,
                    mfg_catalog_id, shelf_life_expiration_date, alternate_ui, alternate_um, counted_qty, true, "HME", null);

           //verify
           DataTable cr = manager.findInvAuditCr(inventory_audit_cr_id);
           Assert.AreEqual(1, cr.Rows.Count);

           DataRow inventoryCr = cr.Rows[0];
           Assert.AreEqual(location_id, Int32.Parse(inventoryCr["location_id"].ToString()));
           Assert.AreEqual(mfg_catalog_id, Int32.Parse(inventoryCr["mfg_catalog_id"].ToString()));
           Assert.AreEqual(inventory_audit_id, Int32.Parse(inventoryCr["inventory_audit_id"].ToString()));
           Assert.AreEqual(counted_qty, Int32.Parse(inventoryCr["qty"].ToString()));
           Assert.AreEqual(alternate_ui, inventoryCr["alternate_ui"].ToString());
           Assert.AreEqual(alternate_um, inventoryCr["alternate_um"].ToString());

           //clean up
          manager.deleteInventoryAuditCr(inventory_audit_cr_id);
        }


        [Test]
        public void Test_updateInventoryAuditCrForDiscrepancy()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            int inventory_audit_id = manager.getExistingInventoryAuditId();

            DateTime? shelf_life_expiration_date = null;

            string alternate_ui = "EA";
            string alternate_um = "12 OZ";

            int counted_qty = 2011;

            int inventory_audit_cr_id = manager.insertInventoryAuditCr(new List<VolumeOffloadItem>(),inventory_audit_id, location_id,
               mfg_catalog_id, counted_qty, true, null, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), 0, "EA", 1, "16 OZ", "HME", null, null);

            manager.updateInventoryAuditCrForDiscrepancy(inventory_audit_cr_id, mfg_catalog_id, shelf_life_expiration_date, alternate_ui,
                alternate_um, counted_qty, true);

            //verify
            DataTable cr = manager.findInvAuditCr(inventory_audit_cr_id);
            Assert.AreEqual(1, cr.Rows.Count);

            DataRow inventoryCr = cr.Rows[0];
            Assert.AreEqual(location_id, Int32.Parse(inventoryCr["location_id"].ToString()));
            Assert.AreEqual(mfg_catalog_id, Int32.Parse(inventoryCr["mfg_catalog_id"].ToString()));
            Assert.AreEqual(inventory_audit_id, Int32.Parse(inventoryCr["inventory_audit_id"].ToString()));
            Assert.AreEqual(counted_qty, Int32.Parse(inventoryCr["qty"].ToString()));
            Assert.AreEqual(alternate_ui, inventoryCr["alternate_ui"].ToString());
            Assert.AreEqual(alternate_um, inventoryCr["alternate_um"].ToString());

            //clean up
            manager.deleteInventoryAuditCr(inventory_audit_cr_id);

        }

    }
}
