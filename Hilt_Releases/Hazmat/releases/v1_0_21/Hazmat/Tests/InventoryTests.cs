﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Data.SqlClient;


namespace HAZMATUnit
{
    class InventoryTests
    {
        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void Test_getATMforNiinCageATM()
        {
            //Test 
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getATMforNiinATM("000000000","12345");

            Assert.AreEqual(0, dt.Rows.Count);

            Assert.Pass();

        }

        [Test]
        public void Test_updateInventoryForATM()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            int inventory_id = manager.getExistingInventoryId();

            string msds_serial_number = "UNKNOWN";

            DateTime? shelf_life = null;

            String atm = "2011";

            manager.updateInventoryForATM(inventory_id, msds_serial_number, shelf_life, atm);

            shelf_life = DateTime.Now;

            manager.updateInventoryForATM(inventory_id, msds_serial_number, shelf_life, atm);
                        
            Assert.Pass();

        }

        [Test]
        public void Test_saveCosalNiinAllowance()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);

            int cosal_niin_id = manager.saveCosalNiinAllowance(null, "COSAL", "TESTING", 2011, 2011);

            int updated_cosal_niin_id = manager.saveCosalNiinAllowance(cosal_niin_id, "COSAL", "TESTING", 2011, 2011);

            Assert.AreEqual(cosal_niin_id, updated_cosal_niin_id);

            manager.deleteCosalNiinAllowance(cosal_niin_id);

            Assert.Pass();

        }

        [Test]
        public void Test_cosalAllowancesListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getCosalNiinAllowances(filterColumn, filterText);
            Console.WriteLine("Test_cosalAllowancesListLoad rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getCosalNiinAllowances(filterColumn, filterText);
            Console.WriteLine("Test_cosalAllowancesListLoad rows:" + dt.Rows.Count);
            filterColumn = "cosal";
            filterText = "h";
            dt = manager.getCosalNiinAllowances(filterColumn, filterText);
            Console.WriteLine("Test_cosalAllowancesListLoad rows:" + dt.Rows.Count);
            filterColumn = "at";
            filterText = "1";
            dt = manager.getCosalNiinAllowances(filterColumn, filterText);
            Console.WriteLine("Test_cosalAllowancesListLoad rows:" + dt.Rows.Count);           
            Assert.Pass();
        }

  
        [Test]
        public void TestDistinctLocationListAllLoad()
        {
            //Test grabbing distinct locations, and test that it is distinct.
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getDistinctLocationListAll();
            Console.WriteLine("TestDistinctLocationListAllLoad rows:" + dt.Rows.Count);

            int count = 0;
            foreach (DataRow r in dt.Rows)
            {
                string name = r["Location"].ToString();

                if (name.Equals("NEWLY ISSUED"))
                    count++;
            }

            Assert.True(count <= 1);
        }

        [Test]
        public void TestGetLocationListLoad()
        {
            //Test grabbing all locations except for Newly Issued
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getLocationList();
            Console.WriteLine("TestGetLocationListLoad rows:" + dt.Rows.Count);

            int count = 0;
            foreach (DataRow r in dt.Rows)
            {
                string name = r["Location"].ToString();

                if (name.Equals("NEWLY ISSUED"))
                    count++;
            }

            Assert.AreEqual(count, 0);

            DataTable dtDistinct = manager.getDistinctLocationListAll();
            Assert.False(dtDistinct.Rows.Count == dt.Rows.Count);
        }

        [Test]
        public void TestGetInventoryItemQtyCount() {
            //Test grabbing qty for a random inventory record
            DatabaseManager manager = new DatabaseManager(connectionString);

            Dictionary<String, int> randomRec = getInventoryRecordForTest();

            int count = manager.getInventoryItemQty(randomRec["inventory_id"]);

            Console.WriteLine("TestGetInventoryItemQtyCount qty:" + count);

            Assert.AreEqual(randomRec["qty"], count);
        }

        [Test]
        public void TestGetInventoryItemLocationID() {
            //Test grabbing location_id for a random inventory record
            DatabaseManager manager = new DatabaseManager(connectionString);

            Dictionary<String, int> randomRec = getInventoryRecordForTest();

            int location_id = manager.getInventoryItemLocationID(randomRec["inventory_id"]);
            Console.WriteLine("TestGetInventoryItemLocationID location_id:" + location_id);

            Assert.AreEqual(randomRec["location_id"], location_id);
        }

        [Test]
        public void TestGetInventoryItemMfgCatalogID() {
            //Test grabbing mfg_catalog_id for a random inventory record
            DatabaseManager manager = new DatabaseManager(connectionString);

            Dictionary<String, int> randomRec = getInventoryRecordForTest();

            int catalog_id = manager.getInventoryItemMfgCatalogID(randomRec["inventory_id"]);
            Console.WriteLine("TestGetInventoryItemLocationID catalog_id:" + catalog_id);

            Assert.AreEqual(randomRec["mfg_catalog_id"], catalog_id);
        }

        private Dictionary<String, int> getInventoryRecordForTest() {
            // Gets a random inventory item from the test database 
            Dictionary<String, int> randomRec = new Dictionary<String, int>();

            DatabaseManager manager = new DatabaseManager(connectionString);

            string sql = "SELECT TOP 1 inventory_id, location_id, mfg_catalog_id, qty "
                + "FROM inventory "
                + "ORDER BY NEWID();";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                int temp = 0;

                // inventory_id
                int.TryParse(rdr["inventory_id"].ToString(), out temp);
                randomRec.Add("inventory_id", temp);

                // location_id
                int.TryParse(rdr["location_id"].ToString(), out temp);
                randomRec.Add("location_id", temp);

                // mfg_catalog_id
                int.TryParse(rdr["mfg_catalog_id"].ToString(), out temp);
                randomRec.Add("mfg_catalog_id", temp);

                // qty
                int.TryParse(rdr["qty"].ToString(), out temp);
                randomRec.Add("qty", temp);

                Console.WriteLine("Inventory record: " + randomRec["inventory_id"].ToString()
                    + " , " + randomRec["location_id"].ToString()
                    + " , " + randomRec["mfg_catalog_id"].ToString()
                    + " , " + randomRec["qty"].ToString());
            }

            return randomRec;
        }
    }
}
