﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace HAZMATUnit
{
    [TestFixture]
    class InsertBromineAuditTest
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void testBromineInsert()
        {

            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            string a = "QQQ";

            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();

            //insert row
            try
            {
                manager.insertBromineAuditInformation(a, a, a, a, mfg_catalog_id, location_id, new DateTime().Date, 1);
            }
            catch (Exception ex)
            {
                this.removeData(a);
                Assert.Fail("Error inserting data: " + ex.Message);
            }

            DataTable dt = this.selectBromine(a);

            foreach (DataRow r in dt.Rows)
            {
                string a_ = r["a"].ToString();
                string b_ = r["b"].ToString();
                string c_ = r["c"].ToString();
                string d_ = r["d"].ToString();

                if (!a_.Equals("XXX") || !b_.Equals("XXX") || !c_.Equals("XXX") || !d_.Equals("XXX"))
                {
                    this.removeData(a);
                    Assert.Fail("Data found in the Bromine_audit table is not the data expected:  EXPECTED: " + a + " FOUND: " + a_);
                }
                else
                {
                    this.removeData(a);
                    Assert.Pass("Successfully verified the bromine_audit insert method");
                }
            }
           
            Console.WriteLine("Successfully verified the bromine_audit insert method");

            
        }


        private int getLocation()
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }


        private void removeData(string value)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "delete from bromine_audit b where b.a = @name";
            SqlCommand cmd = new SqlCommand(stmt, con);

            cmd.Parameters.AddWithValue("@name", value);

            cmd.ExecuteNonQuery();

            con.Close();



        }

        private DataTable selectBromine(string value){

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "select * from bromine_audit b where b.a = @name AND b.b=@name AND b.c=@name AND b.d=@name";
            SqlCommand cmd = new SqlCommand(stmt, con);

            cmd.Parameters.AddWithValue("@name", value);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            con.Close();

            return dt;
            
        }

    }
}
