﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;

namespace HAZMATUnit
{
    class ReceivingTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestReceivingListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getReceivingList(filterColumn, filterText);
            Console.WriteLine("TestReceivingListLoad rows:" + dt.Rows.Count);           
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getReceivingList(filterColumn, filterText);
            Console.WriteLine("TestReceivingListLoad rows:" + dt.Rows.Count);  
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getReceivingList(filterColumn, filterText);
            Console.WriteLine("TestReceivingListLoad rows:" + dt.Rows.Count);  
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getReceivingList(filterColumn, filterText);
            Console.WriteLine("TestReceivingListLoad rows:" + dt.Rows.Count);  
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getReceivingList(filterColumn, filterText);
            Console.WriteLine("TestReceivingListLoad rows:" + dt.Rows.Count);  
            Assert.Pass();
        }

        [Test]
        public void TestMFGListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getMFGCatalogCollection(filterColumn, filterText);
            Console.WriteLine("TestMFGListLoad rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getMFGCatalogCollection(filterColumn, filterText);
            Console.WriteLine("TestMFGListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getMFGCatalogCollection(filterColumn, filterText);
            Console.WriteLine("TestMFGListLoad rows:" + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getMFGCatalogCollection(filterColumn, filterText);
            Console.WriteLine("TestMFGListLoad rows:" + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getMFGCatalogCollection(filterColumn, filterText);
            Console.WriteLine("TestMFGListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestReceivingInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            string manufacturer_date = DateTime.Now.ToShortDateString();
            int expected_qty = 2010;
            int receiving_id = manager.insertReceiving(mfg_catalog_id, expected_qty, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), 10, "123", "456", "7890");

            Console.WriteLine("TestReceivingInsertandDelete issue_id:" + receiving_id);
            Assert.True(receiving_id > 0);

            //Confirm  receiving insert
            DataTable receiving = manager.findReceiving(receiving_id);
            Assert.True(receiving.Rows.Count == 1);
            DataRow row = receiving.Rows[0];
            Assert.True(row["mfg_catalog_id"].ToString() == mfg_catalog_id.ToString());
            Assert.True(row["expected_qty"].ToString() == expected_qty.ToString());
            Assert.True(row["batch_number"].ToString() == "123");
            Assert.True(row["lot_number"].ToString() == "456");
            Assert.True(row["contract_number"].ToString() == "7890");
         
            //Delete what was inserted test
            manager.deleteReceiving(receiving_id);
            receiving = manager.findReceiving(receiving_id);
            Assert.True(receiving.Rows.Count == 0);
            
        }

    }
}
