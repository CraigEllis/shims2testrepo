﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Extensions.Asp;
using System.Net;

namespace HAZMATUnit
{
    [TestFixture]
    public class PageLoadTests : WebFormTestCase
    {

        private string path = Configuration.Path;

        [SetUp]
        public void Init()
        {
           Browser.Credentials = System.Net.CredentialCache.DefaultCredentials;
           Console.WriteLine("Browser - " + Browser.ToString() 
               + "\n\t" + Browser.Credentials.ToString()
               + "\n\t" + path);
        }

        [Test]
        public void TestAddSMCL()
        {
            
            Browser.GetPage(path + "AddSMCL.aspx");
           
        }

        [Test]
        public void TestAddAllowanceReport()
        {
            Browser.GetPage(path + "AllowanceReport.aspx");
        }

        //[Test]
        //public void TestAuditDetail()
        //{
        //    Browser.GetPage(path + "AuditDetail.aspx");           
        //}

        //[Test]
        //public void TestAuditEdit()
        //{
        //    Browser.GetPage(path + "AuditEdit.aspx");
        //}
        [Test]
        public void TestApprovedIncompatibles()
        {
            Browser.GetPage(path + "ApprovedIncompatibles.aspx");
        }

        [Test]
        public void TestAtmTags()
        {
            Browser.GetPage(path + "AtmTag.aspx");
        }

        [Test]
        public void TestBromine()
        {
            Browser.GetPage(path + "Bromine.aspx");
        }

        [Test]
        public void TestCalcium()
        {
            Browser.GetPage(path + "Calcium.aspx");
        }

        [Test]
        public void TestDecanting()
        {
            Browser.GetPage(path + "Decanting.aspx");
        }

        //[Test]
        //public void TestDecantingDetail()
        //{
        //    Browser.GetPage(path + "DecantingDetail.aspx");
        //}

        [Test]
        public void TestDecantingNew()
        {
            Browser.GetPage(path + "DecantingNew.aspx");
        }

        [Test]
        public void TestINSURV()
        {
            Browser.GetPage(path + "INSURV.aspx");
        }

        [Test]
        public void TestINSURVChecklist()
        {
            Browser.GetPage(path + "INSURVChecklist.aspx");
        }

        [Test]
        public void TestInventory()
        {
            Browser.GetPage(path + "Inventory.aspx");
        }

        [Test]
        public void TestInventoryAudit()
        {
            Browser.GetPage(path + "InventoryAudit.aspx");
        }

        [Test]
        public void TestIssue()
        {
            Browser.GetPage(path + "Issue.aspx");
        }

        //[Test]
        //public void TestInventoryAuditLocationSelection()
        //{
        //    Browser.GetPage(path + "InventoryAuditLocationSelection.aspx");
        //}

        [Test]
        public void TestLocationManagement()
        {
            Browser.GetPage(path + "LocationManagement.aspx");
        }

        [Test]
        public void TestMSDS()
        {
            Browser.GetPage(path + "MSDS.aspx");
        }

        [Test]
        public void TestOffload()
        {
            Browser.GetPage(path + "Offload.aspx");
        }

        [Test]
        public void TestOffloadArchived()
        {
            Browser.GetPage(path + "OffloadArchived.aspx");
        }

        [Test]
        public void TestReceiveInventory()
        {
            Browser.GetPage(path + "ReceiveInventory.aspx");
        }

        [Test]
        public void TestReceiving()
        {
            Browser.GetPage(path + "Receiving.aspx");
        }

        [Test]
        public void TestReports()
        {
            Browser.GetPage(path + "Reports.aspx");
        }

        [Test]
        public void TestShipManagement()
        {
            Browser.GetPage(path + "ShipManagement.aspx");
        }

        [Test]
        public void TestSMCL()
        {
            Browser.GetPage(path + "SMCL.aspx");
        }

        [Test]
        public void TestUserManagement()
        {
            Browser.GetPage(path + "UserManagement.aspx");
        }

        [Test]
        public void TestUsageCategoryReport()
        {
            Browser.GetPage(path + "UsageCategoryReport.aspx");
        }

        [Test]
        public void TestMsslReport()
        {
            Browser.GetPage(path + "MSSLReport.aspx");
        }

    }
}
