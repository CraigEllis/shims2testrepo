﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NUnit.Framework;

namespace HAZMATUnit {
    class PropertiesTest {
        [Test]
        public void testHAZMATResourcesPath() {
            Console.WriteLine("Resources path: " + Configuration.ResourcesPath);

            Assert.IsTrue(Directory.Exists(Configuration.ResourcesPath),
                Configuration.ResourcesPath + " doesn't exist");
        }

        [Test]
        public void testConnectionString() {
            Console.WriteLine("Connection String: " + Configuration.ConnectionInfo);

            Assert.IsTrue(Configuration.ConnectionInfo.Length > 0, 
                "Missing Configuration ConnectionInfo property");
        }

        [Test]
        public void testPath() {
            Console.WriteLine("Asp Project Path: " + Configuration.Path);

            Assert.IsTrue(Configuration.Path.Length > 0,
                "Missing Configuration App Project Path property");
        }

        [Test]
        public void testHAZMATImagesPath() {
            Console.WriteLine(Configuration.ImagesPath + " - "
                + Directory.Exists(Configuration.ImagesPath));

            Assert.IsTrue(Directory.Exists(Configuration.ImagesPath),
                Configuration.ImagesPath + " doesn't exist");
        }
    }
}
