﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;

namespace HAZMATUnit
{
    class LocationManagementTests
    {
        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestInsertDeleteNewLocation()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            //grab a workcenter
            int workcenter_id = Int32.Parse(manager.getWorkcenterList(false)[0].Value);

            int location_id = manager.insertLocation("UNIT TEST LOCATION", workcenter_id);

            DataTable locationTable = manager.getSingleLocationList(location_id);

            if (locationTable.Rows.Count < 1)
            {
                Assert.Fail("getSingleLocationList Fail");
            }

            Assert.True(location_id > 0);

             //cleanup
            manager.deleteLocation(location_id);

            Assert.Pass();
        }

        [Test]
        public void TestLocationFilterLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string workcenter = "";
            string filterText = "";
            DataTable dt = manager.getLocationCollection(filterText, workcenter);
            Console.WriteLine("TestLocationFilterLoad rows:" + dt.Rows.Count);
            workcenter = "1";
            filterText = "L";
            dt = manager.getLocationCollection(filterText, workcenter);
            Console.WriteLine("TestLocationFilterLoad rows:" + dt.Rows.Count);
            workcenter = "";
            filterText = "S";
            dt = manager.getLocationCollection(filterText, workcenter);
            Console.WriteLine("TestLocationFilterLoad rows:" + dt.Rows.Count);
            workcenter = "2";
            filterText = "A";
            dt = manager.getLocationCollection(filterText, workcenter);
            Console.WriteLine("TestLocationFilterLoad rows:" + dt.Rows.Count);
            workcenter = "3";
            filterText = "C";
            dt = manager.getLocationCollection(filterText, workcenter);
            Console.WriteLine("TestLocationFilterLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }
    }
}
