﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.IO;
using System.Reflection;
using System.Data;

namespace HAZMATUnit
{
    [TestFixture]
    class MSSLParserTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        //getMSSLDate

        [Test]
        public void TestGetMsslDate()
        {
            new DatabaseManager(connectionString).getMSSLDate();

            Assert.Pass("getMsslDate passed!");
        }

        [Test]
        public void TestMSSLParseInventoryAndSubs()
        {
            //parse MSSL file            
            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream dbStream = assembly.GetManifestResourceStream("HAZMATUnit.MSSLTestSMMCs.txt"); //Items with correct SMCC's = 25.
                     

            MSSLParser_archive parse = new MSSLParser_archive();
            StreamReader reader = new StreamReader(dbStream);

            MSSLParser m = new MSSLParser();

            //parse the file and get a list of msslInventory
            MSSLData data  = m.parse(reader);

            List<MsslInventory> inventoryList = data.InventoryList;
            
            //Confirm that SMCCs that should be ignored ARE ignored.
            int inventoryCount = inventoryList.Count;
            if (inventoryCount != 25)
            {
                Assert.Fail("Expected 25, got item count:" + inventoryList.Count);
            }

            //Verify all fields for a record   
            dbStream = assembly.GetManifestResourceStream("HAZMATUnit.MSSLExpectedResults.xls");
           
            List<string[]> expectedList = new CSVparser().parseCSV(dbStream);

            for (int i = 0; i < 25; i++)
            {
                MsslInventory invn = inventoryList[i];
                string[] expected = expectedList[i];
                if (!invn.Niin.Equals(expected[0]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Niin:" + invn.Niin + " instead got " + expected[0]);
                }
                if (!invn.Ati.Equals(expected[1]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Ati:" + invn.Ati + " instead got " + expected[1]);
                }
                if (!invn.Cog.Equals(expected[2]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Cog:" + invn.Cog + " instead got " + expected[2]);
                }
                if (!invn.Mcc.Equals(expected[3]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Mcc:" + invn.Mcc + " instead got " + expected[3]);
                }
                if (!invn.Ui.Equals(expected[4]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Ui:" + invn.Ui + " instead got " + expected[4]);
                }
                if (!invn.Up.ToString().Equals(expected[5]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Up:" + invn.Up + " instead got " + expected[5]);
                }
                if (!invn.Netup.ToString().Equals(expected[6]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Netup:" + invn.Netup + " instead got " + expected[6]);
                }
                if (!invn.Location.Equals(expected[7]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Location:" + invn.Location + " instead got " + expected[7]);
                }
                if (!invn.Qty.ToString().Equals(expected[8]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Qty:" + invn.Qty + " instead got " + expected[8]);
                }
                if (!invn.Lmc.Equals(expected[9]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Lmc:" + invn.Lmc + " instead got " + expected[9]);
                }
                if (!invn.Irc.Equals(expected[10]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Irc:" + invn.Irc + " instead got " + expected[10]);
                }
                if (!invn.Dues.ToString().Equals(expected[11]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Dues:" + invn.Dues + " instead got " + expected[11]);
                }
                if (!invn.Ro.ToString().Equals(expected[12]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Ro:" + invn.Ro + " instead got " + expected[12]);
                }
                if (!invn.Rp.ToString().Equals(expected[13]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Rp:" + invn.Rp + " instead got " + expected[13]);
                }
                if (!invn.Amd.ToString().Equals(expected[14]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Amd:" + invn.Amd + " instead got " + expected[14]);
                }
                if (!invn.Smic.Equals(expected[15]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Smic:" + invn.Smic + " instead got " + expected[15]);
                }
                if (!invn.Slc.Equals(expected[16]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Slc:" + invn.Slc + " instead got " + expected[16]);
                }
                if (!invn.Slac.Equals(expected[17]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Slac:" + invn.Slac + " instead got " + expected[17]);
                }
                if (!invn.Smcc.Equals(expected[18]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Smcc:" + invn.Smcc + " instead got " + expected[18]);
                }
                if (!invn.Nomenclature.Equals(expected[19]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Nomenclature:" + invn.Nomenclature + " instead got " + expected[19]);
                }
                if (!((expected[20] == "" && invn.Arrc == null) || (invn.Arrc != null && invn.Arrc.Equals(expected[20]))))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Arrc:" + invn.Arrc + " instead got expected[20]");
                }
                if (!((expected[21] == "" && invn.LimitFlag == null) || (invn.Arrc != null && invn.LimitFlag.Equals(expected[21]))))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected LimitFlag:" + invn.LimitFlag + " instead got expected[21]");
                }
                if (!((expected[22] == "" && invn.NoDropInd == null) || (invn.Arrc != null && invn.NoDropInd.Equals(expected[22]))))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected NoDropInd:" + invn.NoDropInd + " instead got expected[22]");
                }
            }

            //Verify Sub count
            int subCount = data.SubList.Count;
            if (subCount != 4)
            {
                Assert.Fail("Expected 4, got item count:" + data.SubList.Count);
            }

            //Verify Sub values
            dbStream = assembly.GetManifestResourceStream("HAZMATUnit.MSSLExpectedSubs.xls");

            List<string[]> subList = new CSVparser().parseCSV(dbStream);

            for (int i = 0; i < 4; i++)
            {
                MsslSubs invn = data.SubList[i];
                string[] expected = subList[i];
                if (!invn.Niin.Equals(expected[0]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Niin:" + invn.Niin + " instead got " + expected[0]);
                }
                if (!invn.Sub_niin.Equals(expected[1]))
                {
                    Assert.Fail("For niin " + invn.Niin + " Expected Ati:" + invn.Sub_niin + " instead got " + expected[1]);
                }
            }
            
            //foreach (MsslSubs inv in data.SubList)
            //{
            //    Console.WriteLine(inv.Niin+"\t" + inv.Sub_niin);             
            //   // Console.WriteLine(inv.Niin + "\t" + inv.Ati + "\t" + inv.Cog + "\t" + inv.Mcc + "\t" + inv.Ui + "\t" + inv.Up + "\t" + inv.Netup + "\t" + inv.Location + "\t" + inv.Qty + "\t" + inv.Lmc + "\t" + inv.Irc + "\t" + inv.Dues + "\t" + inv.Ro + "\t" + inv.Rp + "\t" + inv.Amd + "\t" + inv.Smic + "\t" + inv.Slc + "\t" + inv.Slac + "\t" + inv.Smcc + "\t" + inv.Nomenclature + "\t" + inv.Arrc + "\t" + inv.LimitFlag + "\t" + inv.NoDropInd);
            //   // Console.WriteLine(inv.Niin + "," + inv.Ati + "," + inv.Cog + "," + inv.Mcc + "," + inv.Ui + "," + inv.Up + "," + inv.Netup + "," + inv.Location + "," + inv.Qty + "," + inv.Lmc + "," + inv.Irc + "," + inv.Dues + "," + inv.Ro + "," + inv.Rp + "," + inv.Amd + "," + inv.Smic + "," + inv.Slc + "," + inv.Slac + "," + inv.Smcc + "," + inv.Nomenclature + "," + inv.Arrc + "," + inv.LimitFlag + "," + inv.NoDropInd);
            //   //Console.WriteLine(inv.Niin + " [ati] " + inv.Ati + " [cog]" + inv.Cog + " [mcc]" + inv.Mcc + " [ui]" + inv.Ui + " [up]" + inv.Up + " [netup]" + inv.Netup + " [location]" + inv.Location + " [qty]" + inv.Qty + " [lmc]" + inv.Lmc + " [irc]" + inv.Irc + " [dues]" + inv.Dues + " [ro]" + inv.Ro + " [rp]" + inv.Rp + " [amd]" + inv.Amd + " [smic]" + inv.Smic + " [slc]" + inv.Slc + " [slac]" + inv.Slac + " [smcc]" + inv.Smcc + " [nomenclature]" + inv.Nomenclature + " [arrc]" + inv.Arrc + " [limitFlag]" + inv.LimitFlag + " [nodrop]" + inv.NoDropInd );
            //   // Console.WriteLine("\r\r");
            //}

        }

        [Test]
        public void TestMSSLParseSMCCCountCheck()
        {
            //parse MSSL file            
            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream dbStream = assembly.GetManifestResourceStream("HAZMATUnit.779MSSLHME.txt"); //Items with correct SMCC's = 25.
            
            MSSLParser_archive parse = new MSSLParser_archive();
            StreamReader reader = new StreamReader(dbStream);

            MSSLParser m = new MSSLParser();

            //parse the file and get a list of msslInventory
            MSSLData data = m.parse(reader);

            List<MsslInventory> inventoryList = data.InventoryList;

            //Confirm that SMCCs that should be ignored ARE ignored.
            int inventoryCount = inventoryList.Count;
            if (inventoryCount != 64)
            {
                Assert.Fail("Expected 64, got item count:" + inventoryList.Count);
            }

            //Verify Sub count
            int subCount = data.SubList.Count;
            if (subCount != 3)
            {
                Assert.Fail("Expected 3, got item count:" + data.SubList.Count);
            }

        }

        [Test]
        public void TestMSSLParseInsertData()
        {
            //parse MSSL file            
            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream dbStream = assembly.GetManifestResourceStream("HAZMATUnit.MSSLTestSMMCs.txt"); //Items with correct SMCC's = 25.

            MSSLParser_archive parse = new MSSLParser_archive();
            StreamReader reader = new StreamReader(dbStream);

            MSSLParser m = new MSSLParser();

            //parse the file and get a list of msslInventory
            MSSLData data = m.parse(reader);

            new DatabaseManager(connectionString).insertMSSLData(data);

            //compare inventory
            DataTable invDT = new DatabaseManager(connectionString).getMsslInventory();

            //Confirm insert.
            int inventoryCount = invDT.Rows.Count;
            if (inventoryCount != 25)
            {
                Assert.Fail("Expected 25, got item count:" + invDT.Rows.Count);
            }

            //Verify all fields inserted into the db for a record   
            dbStream = assembly.GetManifestResourceStream("HAZMATUnit.MSSLExpectedResults.xls");

            List<string[]> expectedList = new CSVparser().parseCSV(dbStream);

            for (int i = 0; i < 25; i++)
            {
                DataRow invn = invDT.Rows[i];
                string[] expected = expectedList[i];
                if (!Equals(invn["Niin"],(expected[0])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Niin:" + invn["Niin"] + " instead got " + expected[0]);
                }
                if (!Equals(invn["Ati"],(expected[1])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Ati:" + invn["Ati"] + " instead got " + expected[1]);
                }
                if (!Equals(invn["Cog"],(expected[2])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Cog:" + invn["Cog"] + " instead got " + expected[2]);
                }
                if (!Equals(invn["Mcc"],(expected[3])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Mcc:" + invn["Mcc"] + " instead got " + expected[3]);
                }
                if (!Equals(invn["Ui"],(expected[4])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Ui:" + invn["Ui"] + " instead got " + expected[4]);
                }
                if (!EqualsDouble(invn["Up"].ToString(), (expected[5])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Up:" + invn["Up"] + " instead got " + expected[5]);
                }
                if (!EqualsDouble(invn["Netup"].ToString(), (expected[6])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Netup:" + invn["Netup"] + " instead got " + expected[6]);
                }
                if (!Equals(invn["Location"],(expected[7])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Location:" + invn["Location"] + " instead got " + expected[7]);
                }
                if (!Equals(invn["Qty"].ToString(), (expected[8])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Qty:" + invn["Qty"] + " instead got " + expected[8]);
                }
                if (!Equals(invn["Lmc"],(expected[9])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Lmc:" + invn["Lmc"] + " instead got " + expected[9]);
                }
                if (!Equals(invn["Irc"],(expected[10])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Irc:" + invn["Irc"] + " instead got " + expected[10]);
                }
                if (!Equals(invn["Dues"].ToString(), (expected[11])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Dues:" + invn["Dues"] + " instead got " + expected[11]);
                }
                if (!Equals(invn["Ro"].ToString(), (expected[12])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Ro:" + invn["Ro"] + " instead got " + expected[12]);
                }
                if (!Equals(invn["Rp"].ToString(), (expected[13])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Rp:" + invn["Rp"] + " instead got " + expected[13]);
                }
                if (!EqualsDouble(invn["Amd"].ToString(), (expected[14])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Amd:" + invn["Amd"] + " instead got " + expected[14]);
                }
                if (!Equals(invn["Smic"],(expected[15])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Smic:" + invn["Smic"] + " instead got " + expected[15]);
                }
                if (!Equals(invn["Slc"],(expected[16])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Slc:" + invn["Slc"] + " instead got " + expected[16]);
                }
                if (!Equals(invn["Slac"],(expected[17])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Slac:" + invn["Slac"] + " instead got " + expected[17]);
                }
                if (!Equals(invn["Smcc"],(expected[18])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Smcc:" + invn["Smcc"] + " instead got " + expected[18]);
                }
                if (!Equals(invn["Nomenclature"],(expected[19])))
                {
                    Assert.Fail("For niin " + invn["Niin"] + " Expected Nomenclature:" + invn["Nomenclature"] + " instead got " + expected[19]);
                }
               
            }

            //compare subs
            DataTable subDT = new DatabaseManager(connectionString).getMsslSubs();

            //Confirm insert.
            int subCount = subDT.Rows.Count;
            if (subCount != 4)
            {
                Assert.Fail("Expected 4, got item count:" + subDT.Rows.Count);
            }

        }

        private bool Equals(object first, string second)
        {

            if (first.Equals(second))
                return true;

            //To take in account null values in db, but blank strings in text file
            if (first == null && second.Equals("") ||
                first.Equals("") && second == null || first == System.DBNull.Value && second.Equals(""))
                return true;

            return false;
        }

        private bool EqualsDouble(string first, string second)
        {

            if (first.Equals(second))
                return true;

            //To take in account null values in db, but blank strings in text file
            if (first == null && second.Equals("") ||
                first.Equals("") && second == null || Double.Parse(first).Equals(Double.Parse(second)))
                return true;

            return false;
        }

    }
}
