﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Data.SqlClient;

namespace HAZMATUnit
{
    [TestFixture]
    class AtmTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void Test_insertAtmosphereControl()
        {
            //Test 
            DatabaseManager manager = new DatabaseManager(connectionString);

            string atm = manager.insertAtmosphereControl("niin00000", "00000", "TEST", null);

            DataTable atmData = manager.getATMforNiinATM("niin00000", atm);

            Assert.True(atmData.Rows.Count > 0);

            Assert.Pass();

        }

        [Test]
        public void Test_updateAtmosphereControl()
        {
            //Test 
            DatabaseManager manager = new DatabaseManager(connectionString);

            string atm = manager.insertAtmosphereControl("niin22222", "00000", "TEST", null);

            manager.updateAtmosphereControl(atm, "11111", "TEST", null);

            DataTable atmData = manager.getATMforNiinATM("niin22222", atm);

            Assert.True(atmData.Rows.Count > 0);

            Assert.AreEqual("11111", atmData.Rows[0]["cage"].ToString());

            Assert.Pass();

        }

        [TearDown]
        public void TearDown() {
            // Delete the test atms
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            string sql = "DELETE FROM atmosphere_control WHERE niin like 'niin%'";

            SqlCommand cmd = new SqlCommand(sql, conn);

            cmd.ExecuteNonQuery();
        }
    }
}
