﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using System.Data.SqlClient;
using HAZMAT;
using DatabaseUpdate;

namespace HAZMATUnit {
    [TestFixture]
    class DatabaseUpdaterTest {

        [Test]
        public void TestDatabaseUpdater(){
            SqlConnection conn =
                new SqlConnection(Configuration.ConnectionInfo);

            // Run the updater
            DatabaseUpdater databaseUpdater = new DatabaseUpdater();

            databaseUpdater.update(conn);

            // Updater version
            int updaterVer = databaseUpdater.AppDBVersion;

            conn.Open();

            // Get the current DB_Version
            string sql = "SELECT db_version FROM db_properties";
            SqlCommand cmd = new SqlCommand(sql, conn);

            int dbVer = (Int32) cmd.ExecuteScalar();

            conn.Close();

            Assert.AreEqual(updaterVer, dbVer);

        }
    }
}
