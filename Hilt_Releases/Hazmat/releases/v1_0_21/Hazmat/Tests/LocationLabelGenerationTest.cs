﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace HAZMATUnit
{
    [TestFixture]
    class LocationLabelGenerationTest
    {

         private string connectionString = Configuration.ConnectionInfo;

         [Test]
         public void testGenerateLocationLabels()
         {

             //HAZMAT.Barcode code = new HAZMAT.Barcode();

             //List<string> locationList = new DatabaseManager(connectionString).getLocationNameList();

             //foreach (String location in locationList) {
             //    Console.WriteLine(location);
             //}

             //string pdfFilePath = code.createLocationLabels(locationList);

             List<string> locationList = new DatabaseManager().getLocationNameList();
             //get the pdf template
             String templateFilePath = "/ksunLocationLabel.pdf";
             templateFilePath = Properties.Settings.Default.HAZMAT_Resources_Path + templateFilePath;

             GenerateLocationLabel generate = new GenerateLocationLabel(templateFilePath);
             string completedPath = generate.generateLabel(locationList);

             Assert.AreNotEqual(0, new FileInfo(completedPath).Length);

             File.Delete(completedPath);

             Assert.Pass("Location Label Pass!");
         }

    }
}
