﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.IO;
using System.Data;

namespace HAZMATUnit
{
    [TestFixture]
    public class RSupplyExporterTests
    {

        //private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestLoadRSupplyInventory()
        {
            string connectionString = Configuration.ConnectionInfo;
            DatabaseManager manager = new DatabaseManager(connectionString);
            Dictionary<string, List<RSupplyInventory>> inventoryMap = manager.getInventoryForRSupplyExport();
            Console.WriteLine("TestLoadRSupplyInventory rows:" + inventoryMap.Count.ToString());
            Assert.Pass();

        }

        [Test]
        public void TestGenerateIBSFile()
        {

            Dictionary<string, List<RSupplyInventory>> inventoryMap = new DatabaseManager().getInventoryForRSupplyExport();

            RSupplyExporter exporter = new RSupplyExporter();
            MemoryStream s = new MemoryStream();
           
            exporter.export(s, inventoryMap); ;

         
            s.Seek(0, 0);
            
            StreamReader sr = new StreamReader(s);
            string str = sr.ReadToEnd();

            System.Console.WriteLine(str);

            s.Seek(0, 0);
            StreamReader sr2 = new StreamReader(s);
            string firstline = sr2.ReadLine();
                       
            Assert.True(firstline.Contains("JSI235"));

            string content = sr2.ReadToEnd();

            Assert.False(content.Contains("NEWLY ISSUED"));
            
        }


        [Test]
        public void ValidateFile()
        {

            Dictionary<string, List<RSupplyInventory>> inventoryMap = new DatabaseManager().getInventoryForRSupplyExport();

            RSupplyExporter exporter = new RSupplyExporter();
            MemoryStream s = new MemoryStream();

            exporter.export(s, inventoryMap); ;


            s.Seek(0, 0);

            StreamReader sr = new StreamReader(s);

            string line = sr.ReadLine();
            Console.WriteLine(line);
            string strIdentifier = line.Substring(10, 6);
            Assert.AreEqual("JSI235", strIdentifier, "Bad File Identifier");
            string strDeclaredCount = line.Substring(16, 6);
            int declaredCount = int.Parse(strDeclaredCount);
            int actualCount = 0;

            while ((line = sr.ReadLine()) != null)
            {
                Console.WriteLine(line);
                actualCount++;

                string niin = line.Substring(0, 9);
                string strClass = line.Substring(9, 4);
                Assert.True(strClass.Equals("0000"),"Bad Class - " + strClass);
                string ati = line.Substring(13, 1);
                Assert.True(ati.Equals("Q") || ati.Equals("H"), "Bad ATI - " + ati);
                string updated = line.Substring(14, 5);
                Int32 updatedNumber;
                bool updatedBool = Int32.TryParse(updated, out updatedNumber);
                Assert.True(updatedBool, "Bad updated - " + updated);
                string notInvIndicator = line.Substring(19, 1);
                Assert.True(notInvIndicator.Equals("0"), "Bad notInv - " + notInvIndicator);
                string strLocCount = line.Substring(20, 2);
                Int32 locCountNumber;
                bool locCountBool = Int32.TryParse(strLocCount, out locCountNumber);
                Assert.True(locCountBool, "Bad locCount - " + locCountNumber);

                string loc1 = line.Substring(22, 15).Trim();
                string qty1 = int.Parse(line.Substring(37, 5)).ToString();
                Int32 qty1Number;
                bool qty1Bool = Int32.TryParse(qty1, out qty1Number);
                Assert.True(qty1Bool, "Bad qty - " + qty1);

                string deleted = int.Parse(line.Substring(42, 1)).ToString();
                Assert.True(deleted.Equals("0"), "Bad deleted - " + deleted);

                string loc2 = "";
                string qty2 = "";
                string loc3 = "";
                string qty3 = "";
                if (line.Length > 43)
                {
                    loc2 = line.Substring(43, 15).Trim();
                    qty2 = int.Parse(line.Substring(58, 5)).ToString();
                    Int32 qty2Number;
                    bool qty2Bool = Int32.TryParse(qty2, out qty2Number);
                    Assert.True(qty2Bool);
                    string deleted2 = int.Parse(line.Substring(63, 1)).ToString();
                    Assert.True(deleted2.Equals("0"));
                }

                if (line.Length > 64)
                {
                    loc3 = line.Substring(64, 15).Trim();
                    qty3 = int.Parse(line.Substring(79, 5)).ToString();
                    Int32 qty3Number;
                    bool qty3Bool = Int32.TryParse(qty3, out qty3Number);
                    Assert.True(qty3Bool);
                    string deleted3 = int.Parse(line.Substring(84, 1)).ToString();
                    Assert.True(deleted3.Equals("0"));
                }
            }
        }
    }
}
