﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace HAZMATUnit
{
    [TestFixture]
    class OffloadTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestOffloadListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";          
            DataTable dt = manager.getOffloadList(filterColumn, filterText);
            Console.WriteLine("TestOffloadListLoad rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getOffloadList(filterColumn, filterText);
            Console.WriteLine("TestOffloadInventoryListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getOffloadList(filterColumn, filterText);
            Console.WriteLine("TestOffloadInventoryListLoad rows:" + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getOffloadList(filterColumn, filterText);
            Console.WriteLine("TestOffloadInventoryListLoad rows:" + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getOffloadList(filterColumn, filterText);
            Console.WriteLine("TestOffloadInventoryListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestOffloadTopViewListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getOffloadTopViewList(filterColumn, filterText);
            Console.WriteLine("TestOffloadTopViewListLoad rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getOffloadTopViewList(filterColumn, filterText);
            Console.WriteLine("TestOffloadTopViewListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getOffloadTopViewList(filterColumn, filterText);
            Console.WriteLine("TestOffloadTopViewListLoad rows:" + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getOffloadTopViewList(filterColumn, filterText);
            Console.WriteLine("TestOffloadTopViewListLoad rows:" + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getOffloadTopViewList(filterColumn, filterText);
            Console.WriteLine("TestOffloadTopViewListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestOffloadListInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();
            int location_id = manager.getExistingLocationId();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            string manufacturer_date = DateTime.Now.ToShortDateString();
            int offload_qty = 2010;

            List<VolumeOffloadItem> volumes = new List<VolumeOffloadItem>();
            volumes.Add(new VolumeOffloadItem(1));
            volumes.Add(new VolumeOffloadItem(2));
            volumes.Add(new VolumeOffloadItem(3));
            volumes.Add(new VolumeOffloadItem(4));

            int offload_list_id = manager.insertOffload(0, null, 2011, shelf_life_expiration_date, location_id, null, true, mfg_catalog_id, manufacturer_date, offload_qty, "EA", 1, volumes, "123", "456", "7890", "12OZ", "HME");
            Console.WriteLine("TestOffloadListInsertandDelete offload_list_id:" + offload_list_id);
            Assert.True(offload_list_id > 0);

            //Test get Volumes in Offload
            DataTable vo = manager.getVolumesInOffload(offload_list_id);
            Assert.AreEqual(vo.Rows.Count,4);

            //Delete what was inserted test
            manager.flagDeletedOffload(offload_list_id);
            manager.deleteOffload(offload_list_id);
            bool deleted = true;
            DataTable dt = manager.getOffloadList("", "");
            foreach (DataRow row in dt.Rows)
            {
                int id = Int32.Parse(row["offload_list_id"].ToString());
                if (id == offload_list_id)
                    deleted = false;
            }
            Assert.True(deleted);   
         
            //Test volumes were deleted
            vo = manager.getVolumesInOffload(offload_list_id);
            Assert.AreEqual(vo.Rows.Count, 0);
        }

        [Test]
        public void TestOffloadCrInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();            
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            int offload_list_id = manager.getExistingOffloadListId();
            string manufacturer_date = DateTime.Now.ToShortDateString();
            int qty_offloaded = 2010;
            int expected_qty_offloaded = 2000;
            string device_type = "Handheld";
            string username = "Sphillips";
            int offload_cr_id = manager.insertOffloadCR(shelf_life_expiration_date, mfg_catalog_id, manufacturer_date, qty_offloaded, expected_qty_offloaded, device_type, offload_list_id, username);
            Console.WriteLine("TestOffloadListInsertandDelete offload_cr_id:" + offload_cr_id);
            Assert.True(offload_cr_id > 0);

            //Delete what was inserted test
            manager.deleteOffloadCr(offload_cr_id);
            bool deleted = true;
            DataTable dt = manager.getServerOffloadCR();
            foreach (DataRow row in dt.Rows)
            {
                int id = Int32.Parse(row["offload_cr_id"].ToString());
                if (id == offload_cr_id)
                    deleted = false;
            }
            Assert.True(deleted);
        }

        [Test]
        public void TestGarbageOffloadCrInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);            
            int garbage_offload_id = 0;
            int offload_qty = 1;
            int expected_offload_qty = 1;
            string username = "Sphillips";
            string device_type = "Handheld";
            int garbage_offload_cr_id = manager.insertGarbageOffloadCR(offload_qty, expected_offload_qty, device_type, garbage_offload_id, username);
            Console.WriteLine("TestOffloadListInsertandDelete garbage_offload_cr_id:" + garbage_offload_cr_id);
            Assert.True(garbage_offload_cr_id > 0);

            //Delete what was inserted test
            manager.deleteGarbageOffloadCr(garbage_offload_cr_id);
            bool deleted = true;
            DataTable dt = manager.getGarbageOffloadCrList();
            foreach (DataRow row in dt.Rows)
            {
                int id = Int32.Parse(row["garbage_offload_cr_id"].ToString());
                if (id == garbage_offload_cr_id)
                    deleted = false;
            }
            Assert.True(deleted);
        }

        [Test]
        public void TestGarbageTopViewListLoad()
        {           
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getGarbageTopViewList(filterColumn, filterText);
            Console.WriteLine("TestGarbageTopViewListLoad rows:" + dt.Rows.Count); 
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getGarbageTopViewList(filterColumn, filterText);
            Console.WriteLine("TestGarbageTopViewListLoad rows:" + dt.Rows.Count);            
            Assert.Pass();
        }

        [Test]
        public void TestGarbageItemsLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            DataTable dt = manager.getGarbageItems();
            Console.WriteLine("TestGarbageItemsLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestGarbageOffloadInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();
            int garbage_item_id = manager.getExistingGarbageItemId();
            int qty = 2010;
            int garbage_offload_id = manager.insertGarbageOffload(garbage_item_id, qty, "BG");
            Console.WriteLine("TestGarbageOffloadInsertandDelete garbage_offload_id:" + garbage_offload_id);
            //Assert.True(garbage_offload_id > 0);

            //Delete what was inserted test
            manager.flagDeletedGarbageOffload(garbage_offload_id);
            manager.deleteGarbageOffload(garbage_offload_id);
            bool deleted = true;
            DataTable dt = manager.getGarbageTopViewList("", "");
            foreach (DataRow row in dt.Rows)
            {
                int id = Int32.Parse(row["garbage_offload_id"].ToString());
                if (id == garbage_offload_id)
                    deleted = false;
            }
            Assert.True(deleted);
        }

        [Test]
        public void TestGarbageItemsInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string description = "UNIT TEST";
            int garbage_item_id = manager.insertGarbageItem(description);
            Console.WriteLine("TestGarbageItemsInsertandDelete garbage_item_id:" + garbage_item_id);
           // Assert.True(garbage_item_id > 0);

            //Delete what was inserted test
            manager.deleteGarbageItem(garbage_item_id);
            bool deleted = true;
            DataTable dt = manager.getGarbageItems();
            foreach (DataRow row in dt.Rows)
            {
                int id = Int32.Parse(row["garbage_item_id"].ToString());
                if (id == garbage_item_id)
                    deleted = false;
            }
            Assert.True(deleted);

            manager.deleteGarbageItemCompletely(garbage_item_id);
            Assert.Pass();
        }

        [Test]
        public void TestFindGarbageItem()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int garbage_item_id = manager.findGarbageItem("TEST DESCRIPTION"+ DateTime.Now.Ticks);
            Assert.AreEqual(garbage_item_id, 0);
        }

        [Test]
        public void TestArchiveOffloadLists()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int archived_id = manager.archiveOffloadLists();

            manager.unarchive(archived_id);

            Console.WriteLine("TestArchiveOffloadLists archived_id:" + archived_id);

            manager.deleteArchived(archived_id);

            Assert.True(archived_id > 0);
        }

        [Test]
        public void TestArchivedListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            DataTable dt = manager.getArchivedList();
            Assert.Pass();
        }

        [Test]
        public void TestArchivedOffloadListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            int archived_id = 3;
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getArchivedOffloadTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedOffloadListLoad rows:" + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getArchivedOffloadTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedOffloadListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getArchivedOffloadTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedOffloadListLoad rows:" + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getArchivedOffloadTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedOffloadListLoad rows:" + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getArchivedOffloadTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedOffloadListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestArchivedGarbageTopViewListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int archived_id = 3;
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getArchivedGarbageTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedGarbageTopViewListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getArchivedGarbageTopViewList(filterColumn, filterText, archived_id);
            Console.WriteLine("TestArchivedGarbageTopViewListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestGetDD1348DataForOffloadId()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int offload_list_id = manager.getExistingOffloadListId();
            DD1348Item item = manager.getDD1348DataForOffloadId(offload_list_id);
            Assert.Pass();
        }

        [Test]
        public void TestGetDD1348DataForGarbageOffloadId()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int garbage_offload_id = manager.getExistingGarbageOffloadListId();
            DD1348Item item = manager.getDD1348DataForGarbageId(garbage_offload_id);
            Assert.Pass();
        }

        [Test]
        public void TestShipToListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<System.Web.UI.WebControls.ListItem> list = manager.getShipToList();
            Console.WriteLine("TestShipToListLoad rows:" + list.Count);
            Assert.Pass();
        }

        [Test]
        public void TestInsertDeleteShipTo()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int ship_to_id = manager.insertShipTo("12345", "Organization", "Plate Title", "Oxford", "MS", "38655");

            Console.WriteLine("TestInsertDeleteShipTo archived_id:" + ship_to_id);

            manager.deleteShipTo(ship_to_id);

            Assert.True(ship_to_id > 0);
        }

        [Test]
        public void TestFindShipTo()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            DataTable dt = manager.findShipTo(1);
            Assert.Pass();
        }

        [Test]
        public void TestDD1348GenerateSingle()
        {
            String file = Properties.Settings.Default.HAZMAT_Resources_Path + "\\dd1348.pdf";
            
            if (!File.Exists(file))
                Assert.Fail("Can't find " + file);
           
            string Nomenclature = "OIL ASSEMBLY";
            string FSC = "1234";
            string NIIN = "123456789";

            string CAGE = "4321";

            string MSDSSERNO = "ADFSD";
            string OffloadQuantity = "400";
            string UI = "OZ";
            string SMCC = "H1";
            string SLCDescription = "60 Months";

            string SLACDescription = " Some really long description here it is";

            DD1348Item itemData = new DD1348Item(Nomenclature, FSC, NIIN, CAGE, MSDSSERNO, OffloadQuantity, UI, SMCC, SLCDescription, SLACDescription);

            //DataTable shipFrom = new DatabaseManager().getCurrentShip();
            //string ShipFromUIC = shipFrom.Rows[0]["uic"].ToString();
            //string ShipFromBoat = shipFrom.Rows[0]["name"].ToString();
            ////string ShipFromAddressLine1 = shipFrom.Rows[0]["city"].ToString();
            /////string ShipFromAddressLine2 = shipFrom.Rows[0]["state"].ToString() + " " + shipFrom.Rows[0]["zip"].ToString();

            //string ShipToUIC = "54321";
            //string ShipToAddressLine1 = "address 1";
            //string ShipToAddressLine2 = "address 2";
            //string ShipToAddressLine3 = "address 3";
            
            string templateFilePath = file;

            DD1348Generator generator = new DD1348Generator(templateFilePath);

            string dd1328File = generator.generateSingleDD1348(itemData);

            Assert.True(File.Exists(dd1328File));
        }


        [Test]
        public void TestDD1348GenerateMultiple()
        {
            String file = Properties.Settings.Default.HAZMAT_Resources_Path + "\\dd1348.pdf";

            if (!File.Exists(file))
                Assert.Fail("Can't find " + file);

            string Nomenclature = "OIL ASSEMBLY";
            string FSC = "1234";
            string NIIN = "123456789";

            string CAGE = "4321";

            string MSDSSERNO = "ADFSD";
            string OffloadQuantity = "400";
            string UI = "OZ";
            string SMCC = "H1";
            string SLCDescription = "60 Months";

            string SLACDescription = " Some really long description here it is";

            DD1348Item itemData = new DD1348Item(Nomenclature, FSC, NIIN, CAGE, MSDSSERNO, OffloadQuantity, UI, SMCC, SLCDescription, SLACDescription);

           // DataTable shipFrom = new DatabaseManager().getCurrentShip();
           // string ShipFromUIC = shipFrom.Rows[0]["uic"].ToString();
           // string ShipFromBoat = shipFrom.Rows[0]["name"].ToString();
           //// string ShipFromAddressLine1 = shipFrom.Rows[0]["city"].ToString();
           // ///string ShipFromAddressLine2 = shipFrom.Rows[0]["state"].ToString() + " " + shipFrom.Rows[0]["zip"].ToString();

           // string ShipToUIC = "54321";
           // string ShipToAddressLine1 = "address 1";
           // string ShipToAddressLine2 = "address 2";
           // string ShipToAddressLine3 = "address 3";

            string templateFilePath = file;

            DD1348Generator generator = new DD1348Generator(templateFilePath);

            List<DD1348Item> itemDataList = new List<DD1348Item>();
            itemDataList.Add(itemData);
            itemDataList.Add(itemData);

            string dd1328File = generator.generateDD1348s(itemDataList);

            Assert.True(File.Exists(dd1328File));
        }

        [Test]
        public void TestGetDD1348sOffloads()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<DD1348Item> items = manager.getDD1348DatasForOffloadList();
            Assert.Pass();
        }

        [Test]
        public void TestGarbageListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            DataTable dt = manager.getGarbageItemList(filterColumn, filterText);
            Console.WriteLine("TestGarbageListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getGarbageItemList(filterColumn, filterText);
            Console.WriteLine("TestGarbageDescriptionListLoad rows:" + dt.Rows.Count);            
            Assert.Pass();
        }


        [Test]
        public void TestInsertDeleteOffloadDetail()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            DD1348Item item = new DD1348Item();
            item.DOC_IDENT = "TES";
            item.RI_FROM = "TES";
            item.M_S = "T";
            item.SER = "T";
            item.SUPP_ADDRESS = "TEST";
            item.SIG = "T";
            item.FUND = "TE";
            item.DISTRIBUTION = "TES";
            item.PROJECT = "TES";
            item.PRI = "TE";
            item.REQ_DEL_DATE = "TES";
            item.RI = "TES";
            item.O_P = "T";
            item.COND = "T";
            item.MGT = "T";
            item.SHIP_FROM = "TEST";
            item.SHIP_TO = "TEST";
            item.MARK_FOR = "TEST";
            item.DOC_DATE = "TEST";
            item.NMFC = "TEST";
            item.TY_CARGO_MSG = "TEST";
            item.PS = "T";
            item.UP = "TEST";
            item.UFC = "TEST";
            item.FRGHT_CLASS_NOM = "Unit Test";
            item.ITEM_NOM = "Unit Test";
            item.HCC_MSG = "Unit Test";
            item.HCC = "T";
            item.DOCUMENT_NUMBER = "1234567890";
            int offload_detail_id = manager.editOffloadDetails(0, 0, item, false);
            
            Console.WriteLine("TestInsertDeleteOffloadDetail offload_detail_id:" + offload_detail_id);

            manager.deleteOffloadDetail(offload_detail_id);

            Assert.True(offload_detail_id > 0);
        }

        [Test]
        public void TestInsertDeleteGarbageOffloadDetail()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            DD1348Item item = new DD1348Item();
            item.DOC_IDENT = "TES";
            item.RI_FROM = "TES";
            item.M_S = "T";
            item.SER = "T";
            item.SUPP_ADDRESS = "TEST";
            item.SIG = "T";
            item.FUND = "TE";
            item.DISTRIBUTION = "TES";
            item.PROJECT = "TES";
            item.PRI = "TE";
            item.REQ_DEL_DATE = "TES";
            item.RI = "TES";
            item.O_P = "T";
            item.COND = "T";
            item.MGT = "T";
            item.SHIP_FROM = "TEST";
            item.SHIP_TO = "TEST";
            item.MARK_FOR = "TEST";
            item.DOC_DATE = "TEST";
            item.NMFC = "TEST";
            item.TY_CARGO_MSG = "TEST";
            item.PS = "T";
            item.UP = "TEST";
            item.UFC = "TEST";
            item.FRGHT_CLASS_NOM = "Unit Test";
            item.ITEM_NOM = "Unit Test";
            item.HCC_MSG = "Unit Test";
            item.HCC = "T";
            item.DOCUMENT_NUMBER = "1234567890";
            int offload_detail_id = manager.editOffloadDetails(0, 0, item, true);

            Console.WriteLine("TestInsertDeleteGarbageOffloadDetail offload_detail_id:" + offload_detail_id);

            manager.deleteOffloadDetail(offload_detail_id);

            Assert.True(offload_detail_id > 0);
        }

        [Test]
        public void TestPDF417Generation()
        {

            DD1348Item itemData = new DD1348Item();
            itemData.ITEM_NOM = "HELIUM, TECHNICAL";
            itemData.UnitIss = "CY";
            itemData.Quant = "1";
            itemData.SUPP_ADDRESS = "ADSFE";
            itemData.RI_FROM = "sdf";
            itemData.SER = "Y";
            itemData.DISTRIBUTION = "RSD";
            itemData.PROJECT = "TES";
            itemData.PRI = "TE";
            itemData.REQ_DEL_DATE = "999";
            itemData.RI = "ASD";
            itemData.UNIT_DOLLARS = "345";
            itemData.UNIT_CTS = "12";
            itemData.SHIP_TO = "796587";
            itemData.NMFC = "000000";
            itemData.DOCUMENT_NUMBER = "W000000000";

            DD1348Generator generator = new DD1348Generator();
            string expected = "[)>0612SW0000000007Q1CYVSDF12Q345.12USD5P0000000703TESB6RSD2779658738HELIUM, TECHNICAL32999B7TE81YADSFE7VASD";
            string results = generator.getPDf417Value(itemData);
            //Console.WriteLine(results);

            Assert.AreEqual(expected, results);
        }

        [Test]
        public void TestGetOffloadReasons()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<System.Web.UI.WebControls.ListItem> items = manager.getOffloadReasonList();
            Assert.AreEqual(items.Count, 5);
        }


        [Test]
        public void TestOffloadTotalVolumes()
        {
           
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getOffloadTotalVolumes();

            String filePath = Path.GetFullPath("units.xml");
            UnitConverter converter = new UnitConverter(filePath);            
            //converter.setXml(file);

            //convertToTotal
            foreach(DataRow r in dt.Rows)
            {
                string ui = r["ui"].ToString();
                string um = r["um"].ToString();
                double multiplier = Double.Parse(r["total_volume"].ToString());

                string total_um = "";
                try
                {
                    if (um != null && !um.Equals(""))
                        total_um = converter.convertToTotal(um, multiplier);
                    else
                    {
                        //If UM is null, try the UI.
                        total_um = converter.convertToTotal("1 " + ui, multiplier);
                    }
                }
                catch (Exception ex)
                {
                    total_um = ex.Message;
                    
                }

                Console.WriteLine(um + "  " + multiplier + " = " + total_um);

                

            }

            //convertToUnit
            foreach (DataRow r in dt.Rows)
            {
                string ui = r["ui"].ToString();
                string um = r["um"].ToString();
                double multiplier = Double.Parse(r["total_volume"].ToString());

                string total_um = "";
                try
                {
                    if (um != null && !um.Equals(""))
                        total_um = converter.convertToUnit(um, multiplier, "GL");
                    else
                    {
                        //If UM is null, try the UI.
                        total_um = converter.convertToUnit("1 " + ui, multiplier, "GL");
                    }
                }
                catch (Exception ex)
                {
                    total_um = ex.Message;

                }

                Console.WriteLine(um + "  " + multiplier + " = " + total_um);



            }

            string total = converter.convertToTotal("12 X 24OZ", 0.25);

            Console.WriteLine("12 X 24OZ" + "  " + 0.25 + " = " + total);

            total = converter.convertToTotal("6X6.3OZ", 3.75);

           Console.WriteLine("6X6.3OZ" + "  " + 3.75 + " = " + total);

           total = converter.convertToTotal("3 x 100GL", 1.2);

           Console.WriteLine("3 x 100GL" + "  " + 1.2 + " = " + total);

            Assert.Pass();
        }
       
    }
}
