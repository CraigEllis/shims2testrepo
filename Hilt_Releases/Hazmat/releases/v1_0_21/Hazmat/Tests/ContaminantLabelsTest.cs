﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace HAZMATUnit
{

    [TestFixture]
    class ContaminantLabelsTest
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void testPdfDownload()
        {           
            //temp file for the pdf
            String pdfTemplate = Properties.Settings.Default.HAZMAT_Resources_Path + "\\Contaminantlabels.pdf";

            //file location of the white image used for covering labels
            String imageLocation = Properties.Settings.Default.HAZMAT_Resources_Path + "\\whiteContaminant.png";

            //generate pdf
            GenerateContaminant gen = new GenerateContaminant(pdfTemplate, imageLocation);
            string completedPdf = gen.generate(1);


            PdfReader reader = new PdfReader(completedPdf);

            Rectangle rec = reader.GetPageSize(1);

            //i was unable at this time to figure out how to read the field data from the pdf. 
            //I will check the width and height of the page created. 
            if (rec != null)
            {
                if (rec.Height == 1056 && rec.Width == 816)
                {
                    reader.Close();
                    Debug.WriteLine("Height: " + rec.Height + " Width: " + rec.Width);
                    File.Delete(completedPdf);
                    Assert.Pass("pdf created successfully");
                }
                else
                {
                    reader.Close();
                    Debug.WriteLine("Height: " + rec.Height + " Width: " + rec.Width);
                    File.Delete(completedPdf);
                    Assert.Fail("PDF file has not been created. Unable to get the page size.");
                }

            }
            else
            {


                reader.Close();
                File.Delete(completedPdf);
                Assert.Fail("PDF file has not been created. Unable to get the page size.");

            }



        }
       

    }

}