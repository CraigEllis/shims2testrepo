using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;

namespace HAZMATUnit
{
    [TestFixture]
    class InventoryAuditTests
    {

        private string connectionString = Configuration.ConnectionInfo;


        [Test]
        public void TestLocationListLoad()
        {
            //Insert new location_cr for an audit
            DatabaseManager manager = new DatabaseManager(connectionString);
            int first_location_cr_id = manager.insertLocationCr(3, 1, 77, 1);
            int second_location_cr_id = manager.insertLocationCr(3, 1, 77, 2);
            int first_audit_cr = manager.insertInventoryAuditCr(77, 3, 45, 45, true, "NULL", "2011-12-16 00:00:00", "2010-12-16 00:00:00", 5, "GL", 1, "NULL");
            int second_audit_cr = manager.insertInventoryAuditCr(77, 3, 45, 44, true, "NULL", "2011-12-16 00:00:00", "2010-12-16 00:00:00", 5, "GL", 2, "NULL");

            //Test loading discrepant locations from audit counts
            DataTable dt = manager.getInventoryAuditLocationWithDiscrepanciesCollection(77, 1);
            int count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                if (Convert.ToInt32(row["location_id"]) == 3)
                {
                    count++;
                }
            }
            if (count == 1)
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail();
            }

            dt = manager.getInventoryAuditLocationWithDiscrepanciesCollection(77, 2);
            count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                if (Convert.ToInt32(row["location_id"]) == 3)
                {
                    count++;
                }
            }
            if(count == 0)
            {
                Assert.Pass();
            }
            else if(count >= 1)
            {
                Assert.Fail();
            }

            //Remove test location_cr's and audit_cr's
            manager.deleteInventoryAuditCr(first_audit_cr);
            manager.deleteInventoryAuditCr(second_audit_cr);
            manager.deleteLocationCr(first_location_cr_id);
            manager.deleteLocationCr(second_location_cr_id);
        }
    }
}
