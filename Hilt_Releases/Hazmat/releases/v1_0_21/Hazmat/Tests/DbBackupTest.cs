﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

namespace HAZMATUnit
{
     [TestFixture]
    class DbBackupTest
    {
         private string connectionString = Configuration.ConnectionInfo;

         [Test]
         public void Test_backupDatabase()
         {
             //Test
             DatabaseManager manager = new DatabaseManager(connectionString);

             String folder = "C:/temp/";

             string fileName = "UNIT_TEST_backup.bak";

             manager.executeDatabaseBackup(folder, fileName);

             File.Delete(folder + fileName);

             Assert.Pass();
         }


    }
}
