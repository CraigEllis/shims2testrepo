﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using HAZMAT;
using NUnit.Framework;

namespace HAZMATUnit
{
    [TestFixture]
    class RequisitionParserTests
    {

        [Test]
        public void TestSMCCParseTest()
        {

            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream s = assembly.GetManifestResourceStream("HAZMATUnit.Requisition.txt");
            StreamReader reader = new StreamReader(s);

            RequisitionParser p = new RequisitionParser();
            List<RequisitionItem> fileInventory = p.parse(reader);

            Assert.AreEqual(20, fileInventory.Count);
            Assert.AreEqual("A0A", fileInventory[0].Di);
            Assert.AreEqual("NIZ", fileInventory[1].Ri);
            Assert.AreEqual("0", fileInventory[2].Ms);
            Assert.AreEqual("0099LLH690061", fileInventory[3].Nsn);
            Assert.AreEqual("L1", fileInventory[4].Smic);
            Assert.AreEqual("EA", fileInventory[5].Ui);
            Assert.AreEqual(1, fileInventory[5].Qty);
            Assert.AreEqual("231729095C442", fileInventory[6].Contract_number);
            Assert.AreEqual("R", fileInventory[7].Dmd);
            Assert.AreEqual("N48096", fileInventory[8].Sadd);
            Assert.AreEqual("C", fileInventory[9].Sig);
            Assert.AreEqual("V7", fileInventory[10].Fc);
            Assert.AreEqual("E", fileInventory[11].Dist);
            Assert.AreEqual("9B", fileInventory[12].Cog);
            Assert.AreEqual("ZS0", fileInventory[13].Prj);
            Assert.AreEqual(12, fileInventory[14].Pri);
            Assert.AreEqual("5G", fileInventory[15].Adv);
            Assert.AreEqual(114.90, fileInventory[16].Up);
            Assert.AreEqual("AF1 NRP 286", fileInventory[17].Sts_desc);
            Assert.AreEqual("0274  S1042009", fileInventory[18].Location_number);

        }
    }
}
