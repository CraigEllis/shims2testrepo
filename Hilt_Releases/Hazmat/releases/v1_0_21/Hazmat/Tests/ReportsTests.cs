﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls;

namespace HAZMATUnit
{

    [TestFixture]
    class ReportsTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestAddApprovedIncompatibles()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int inventory_id1 = manager.getExistingInventoryId();
            int inventory_id2 = inventory_id1;
            int location_id = manager.getExistingLocationId();
            string username = "UNIT_TEST";
            string explanation = "THIS IS JUST A TEST";

            int approved_id = manager.insertApprovedIncompatibles(inventory_id1, inventory_id2, location_id, username, explanation);

            Assert.True(approved_id > 0);

            //Cleanup
            manager.deleteApprovedIncompatible(approved_id);

        }

        [Test]
        public void TestClearApprovedIncompatibles()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            manager.updateApprovedIncompatiblesFlagAllInvalidated();

            Assert.Pass();
        }


        [Test]
        public void TestLoadWrongLocations()
        {
            List<string> visibleHazardsList = new List<string>();
            DatabaseManager manager = new DatabaseManager(connectionString);
            string location = "";
            string workCenter = "";
            DataTable dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestWrongLocationsList rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 34);
            location = "1";
            workCenter = "";
            dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestWrongLocationsList with location rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 27);
            location = "";
            workCenter = "1";
            dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestWrongLocationsList with workcenter rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 30);
            location = "1";
            workCenter = "1";
            dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestWrongLocationsList with both rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 27);
            Assert.Pass();

        }

        [Test]
        public void TestLoadApprovedWrongLocations()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);
            List<string> visibleHazardsList = new List<string>();
            string location = "";
            string workCenter = "";
            DataTable dt = manager.getApprovedWrongLocationsList(location, workCenter);
            Console.WriteLine("TestLoadApprovedWrongLocations rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 34);
            location = "1";
            workCenter = "";
            dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestLoadApprovedWrongLocations with location rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 27);
            location = "";
            workCenter = "1";
            dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestLoadApprovedWrongLocations with workcenter rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 30);
            location = "1";
            workCenter = "1";
            dt = manager.getWrongLocationsList(location, workCenter);
            Console.WriteLine("TestLoadApprovedWrongLocations with both rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 27);
            Assert.Pass();

        }

        [Test]
        public void TestLimResProListLoad()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);
            string location = "";
            string workCenter = "";
            string usageCategory = "";
            DataTable dt = manager.getLimResProList(location, workCenter,  usageCategory);
            Console.WriteLine("TestLimResProList rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 6);
            location = "";
            workCenter = "";
            usageCategory = "Prohibited";
            dt = manager.getLimResProList(location, workCenter, usageCategory);
            Console.WriteLine("TestWrongLocationsList with Prohibited rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 5);
            location = "";
            workCenter = "";
            usageCategory = "Restricted";
            dt = manager.getLimResProList(location, workCenter, usageCategory);
            Console.WriteLine("TestWrongLocationsList with Restricted rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 0);
            location = "1";
            workCenter = "";
            usageCategory = "";
            dt = manager.getLimResProList(location, workCenter, usageCategory);
            Console.WriteLine("TestWrongLocationsList with location rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 3);
            location = "";
            workCenter = "1";
            usageCategory = "";
            dt = manager.getLimResProList(location, workCenter, usageCategory);
            Console.WriteLine("TestWrongLocationsList with workcenter rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 3);
            location = "1";
            workCenter = "1";
            usageCategory = "";
            dt = manager.getLimResProList(location, workCenter, usageCategory);
            Console.WriteLine("TestWrongLocationsList with both rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 3);
            location = "1";
            workCenter = "1";
            usageCategory = "Prohibited";
            dt = manager.getLimResProList(location, workCenter, usageCategory);
            Console.WriteLine("TestWrongLocationsList with all rows:" + dt.Rows.Count);
            //Assert.True(dt.Rows.Count == 2);
            Assert.Pass();

        }

        [Test]
        public void TestLimResProList_ReportsPdfGenerator()
        {
            ReportsPdfGenerator generator = new ReportsPdfGenerator();
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable reportData = manager.getLimResProListForReport("", "", "");
            Console.WriteLine("TestLimResProList_PDFReportLoad rows:" + reportData.Rows.Count);

            string reportFile = generator.generateRestrictedMaterialReport("Restricted and Prohibited Materials Report", reportData);

           
            Assert.Pass();

        }


        [Test]
        public void TestAllowanceReportListLoad()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable smcl = manager.getSMCLCollection();
            DataRow r = smcl.Rows[0];

            int niin_catalog_id = Int32.Parse(r["niin_catalog_id"].ToString());
            int orig_allowance_qty = Int32.Parse(r["allowance_qty"].ToString());

            int allowance = -200;
            //update
            manager.updateSMCLAllowanceQty(niin_catalog_id, allowance);

            //confirm
            smcl = manager.findNiinCatalog(niin_catalog_id);
            r = smcl.Rows[0];

            int curr_allowance_qty = Int32.Parse(r["allowance_qty"].ToString());

            Assert.AreEqual(allowance, curr_allowance_qty);


            //test various filters
            //DatabaseManager manager = new DatabaseManager(connectionString);
            string whereColumn = "";
            string filterText = "";
            string usageCategory = "";
            string catalogGroup = "";
            DataTable dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad rows:" + dt.Rows.Count);

            bool containedNiin = false;//confirm the niin's allowance qty we changed above shows up.

            //Confirm that only allowance qty discrepancies show up:
            foreach (DataRow row in dt.Rows)
            {
                int niin_id = Int32.Parse(row["niin_catalog_id"].ToString());
                //Console.WriteLine("TestAllowanceReportListLoad rows:" + niin_id + " / " + niin_catalog_id);
                if (niin_id == niin_catalog_id)
                    containedNiin = true;

                int allowance_qty = Int32.Parse(row["allowance_qty"].ToString());
                int qty = Int32.Parse(row["qty"].ToString());
                int qty_difference = Int32.Parse(row["qty_difference"].ToString());

                int diff = qty -allowance_qty;

                Assert.AreEqual(qty_difference, diff);

                Assert.True(diff > 0);

            }
            Assert.AreEqual(true, containedNiin, "Confirm the niin with the allowance qty overage was found in the query");
            //reset
            manager.updateSMCLAllowanceQty(niin_catalog_id, orig_allowance_qty);

            whereColumn = "NIIN";
            filterText = "0";
            usageCategory = "1";
            catalogGroup = "1";
            dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad  rows:" + dt.Rows.Count);

            whereColumn = "description";
            filterText = "t";
            usageCategory = "";
            catalogGroup = "";
            dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad rows:" + dt.Rows.Count);

            whereColumn = "CAGE";
            filterText = "0";
            usageCategory = "";
            catalogGroup = "1";
            dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad  rows:" + dt.Rows.Count);

            whereColumn = "SPECS";
            filterText = "a";
            usageCategory = "1";
            catalogGroup = "1";
            dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad  rows:" + dt.Rows.Count);

            whereColumn = "";
            filterText = "";
            usageCategory = "3";
            catalogGroup = "";
            dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad  rows:" + dt.Rows.Count);

            whereColumn = "";
            filterText = "";
            usageCategory = "";
            catalogGroup = "18";
            dt = manager.getFilteredAllowanceQtyCollection(whereColumn, filterText, usageCategory, catalogGroup);
            Console.WriteLine("TestAllowanceReportListLoad  rows:" + dt.Rows.Count);
            
            Assert.Pass();

        }


        [Test]
        public void TestMSSLDiscrepancyReport()
        {
            System.Diagnostics.Debug.WriteLine("connectionString: " + connectionString);

            this.removeMSSLInventory();

            //test various filters
            DatabaseManager manager = new DatabaseManager(connectionString);

            int locationId = manager.getExistingLocationId();
            string locationName = manager.getExistingLocationName();
            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            string inc_niin = mfg.Rows[0]["niin"].ToString();

            int niin_id = manager.getExistingNiinIdWithNoInventory();
            int mfg_id = manager.getExistingMfgForNiin(niin_id);
            string niin = manager.getExistingNiinWithNoInventory();

            //insert mssl test data (incorrect data)
            this.insertMSSLInventory("testniin", "unitdesc", 9999, "UI", locationName, "HME");

            //insert inventory test data (incorrect data)
            int inc_inventory_id = manager.insertInventory(null, null, 1, locationId, 9999, null, true, mfg_catalog_id, null, null, null, "HME", null);
           

            //insert matching data (correct data)
            int inventory_id = manager.insertInventory(null, null, 1, locationId, 9999, null, true, mfg_id, null, null, null, "HME", null);
            this.insertMSSLInventory(niin, "unitTESTdesc", 9999, "UI", locationName, "HME");


            string whereColumn = "";
            string filterText = "";
            List<MSSLDataDiscrepancy> dataList = manager.getMsslDiscrepancy(locationName, "", "", true);
            Console.WriteLine("TestMSSLDiscrepancyReport rows:" + dataList.Count);

            //look for the niin
            bool found = false;

            //look for the other niin
            bool found2 = false;

            bool matchFound = false;
            foreach (MSSLDataDiscrepancy d in dataList)
            {

                if (d.Niin.ToUpper().Equals("TESTNIIN"))
                {
                    found = true; //This should be found. It is a discrepancy (mssl, not in the inventory)
                    System.Diagnostics.Debug.WriteLine("TESTNIIN: " + found);
                }

                if (d.Niin.ToUpper().Equals(inc_niin))
                {
                    found2 = true; //This should be found. It is a discrepancy (inventory, not in the mssl)
                    System.Diagnostics.Debug.WriteLine("inc_niin: " + found2);
                }

                if (d.Niin.ToUpper().Equals(niin))
                {
                    matchFound = true; //The matching item should NOT be found. (mssl==inventory)
                    System.Diagnostics.Debug.WriteLine("niin: " + matchFound);
                }

                System.Diagnostics.Debug.WriteLine("Assert descrepancies: " + d.Difference);
                //Confirm only discrepancies are shown.
                Assert.AreNotEqual(0, d.Difference);

            }

            System.Diagnostics.Debug.WriteLine("Assert matchFound: " + matchFound);
            Assert.False(matchFound);

            System.Diagnostics.Debug.WriteLine("Assert found2: " + found2);
            Assert.True(found2);

            manager.deleteInventory(inventory_id);
            manager.deleteInventory(inc_inventory_id);

            if (!found)
            {
                System.Diagnostics.Debug.WriteLine("Remove MMSL Inventory");
                this.removeMSSLInventory();
                Assert.Fail("Failed on found");
            }


            whereColumn = "";
            filterText = "";            
            dataList = manager.getMsslDiscrepancy("","","", true);
            Console.WriteLine("TestMSSLDiscrepancyReport No Filter  rows:" + dataList.Count);

            whereColumn = "NIIN";
            filterText = "0";
            dataList = manager.getMsslDiscrepancy("",whereColumn, filterText, true);
            Console.WriteLine("TestMSSLDiscrepancyReport NIIN Filter rows:" + dataList.Count);
                      
            this.removeMSSLInventory();
            Assert.Pass();

        }


        private void insertMSSLInventory(string niin, string desc, int qty, string ui, string location, string cosal)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "insert into mssl_inventory(niin,nomenclature,ui,qty,location, ati) VALUES (@niin,@nomenclature,@ui,@qty,@location, @ati)";
            SqlCommand cmd = new SqlCommand(stmt, con);

            cmd.Parameters.AddWithValue("@niin", niin);
            cmd.Parameters.AddWithValue("@nomenclature", desc);
            cmd.Parameters.AddWithValue("@ui", ui);
            cmd.Parameters.AddWithValue("@qty", qty);
            cmd.Parameters.AddWithValue("@location", location);
            cmd.Parameters.AddWithValue("@ati", cosal);

            cmd.ExecuteNonQuery();

            con.Close();
        }

        private void removeMSSLInventory()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string stmt = "delete from mssl_inventory";
            SqlCommand cmd = new SqlCommand(stmt, con);          

            cmd.ExecuteNonQuery();

            con.Close();

        }


        [Test]
        public void Test_getFilteredTotalInventoryAuditVolumes()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);
            string whereColumn = "";
            string filterText = "";
            String unitsFilePath = Path.GetFullPath("units.xml");

            DataTable dt = manager.getFilteredTotalInventoryAuditVolumes(unitsFilePath, whereColumn, filterText, false);
            Console.WriteLine("Test_getFilteredTotalInventoryAuditVolumes  rows:" + dt.Rows.Count);

            whereColumn = "NIIN";
            filterText = "0";

            dt = manager.getFilteredTotalInventoryAuditVolumes(unitsFilePath, whereColumn, filterText, false);
            Console.WriteLine("Test_getFilteredTotalInventoryAuditVolumes  rows:" + dt.Rows.Count);

            whereColumn = "Description";
            filterText = "t";

            dt = manager.getFilteredTotalInventoryAuditVolumes(unitsFilePath, whereColumn, filterText, true);
            Console.WriteLine("Test_getFilteredTotalInventoryAuditVolumes  rows:" + dt.Rows.Count);

            Assert.Pass();

        }

        [Test]
        public void Test_getFilteredTotalOffloadVolumes()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);
            string whereColumn = "";
            string filterText = "";
            String unitsFilePath = Path.GetFullPath("units.xml");

            DataTable dt = manager.getFilteredTotalOffloadVolumes(unitsFilePath, whereColumn, filterText, false);
            Console.WriteLine("Test_getFilteredTotalOffloadVolumes  rows:" + dt.Rows.Count);

            whereColumn = "NIIN";
            filterText = "0";

            dt = manager.getFilteredTotalOffloadVolumes(unitsFilePath, whereColumn, filterText, true);
            Console.WriteLine("Test_getFilteredTotalOffloadVolumes  rows:" + dt.Rows.Count);

            whereColumn = "Description";
            filterText = "t";

            dt = manager.getFilteredTotalOffloadVolumes(unitsFilePath, whereColumn, filterText, false);
            Console.WriteLine("Test_getFilteredTotalOffloadVolumes  rows:" + dt.Rows.Count);

            Assert.Pass();

        }

        [Test]
        public void Test_insertAndDeleteVolumeIgnoredNiins()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            string niin = "unit_test";

            manager.insertVolumeIgnoredNiin(niin);

            manager.deletedVolumeIgnoredNiin(niin);

            Assert.Pass();

        }

        [Test]
        public void Test_newSMCLReport()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getNewSMCLEntries();

            Console.WriteLine("Test_newSMCLReport  rows:" + dt.Rows.Count);

            Assert.Pass();

        }

        [Test]
        public void Test_auditsByWorkcenterCorrectnessReport()
        {

            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getWorkcenterAuditCorrectness();

            Console.WriteLine("Test_auditsByWorkcenterCorrectnessReport  rows:" + dt.Rows.Count);

            Assert.Pass();

        }


        [Test]
        public void Test_offloadReport()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getOffloadReport();

            Console.WriteLine("Test_offloadReport rows:" + dt.Rows.Count);

            Assert.Pass();
        }


        [Test]
        public void Test_usageCatInventoryReport()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getUsageCatReport();

            Console.WriteLine("Test_usageCatInventoryReport rows:" + dt.Rows.Count);

            Assert.Pass();
        }


        [Test]
        public void TestExportExcelReports()
        {
            String file = Properties.Settings.Default.HAZMAT_Resources_Path + "\\Reports.xls";
            
            string s = new ExcelReports().exportReports(file);

            Assert.Pass();
        }


        [Test]
        public void TestGeneralExcelExport()
        {
            String file = Properties.Settings.Default.HAZMAT_Resources_Path + "\\ExcelReport.xls";
            DatabaseManager manager = new DatabaseManager(connectionString);
            string location = "";
            string workCenter = "";            
            GridView grid = new GridView();
            List<string> visibleHazardsList = new List<string>();
            grid.DataSource = manager.getWrongLocationsList(location, workCenter).DefaultView;
            List<string> ignoredColumns = new List<string>();
            string s = new ExcelReports().exportExcelReport(file, grid, ignoredColumns);

            Assert.Pass();
        }


    }
}
