﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace HAZMATUnit
{
    [TestFixture]
    class IssueTests
    {
        private string connectionString = Configuration.ConnectionInfo;
        private int wc_id = 0;
        private int user_id = 0;
        private int role_id = 0;
        private int loc_id = 0;
        
        [TestFixtureSetUp]
        public void Setup() {
            wc_id = getWorkcenterIDForTest();
            user_id = getAuthUserIDForTest();
            role_id = getUserRoleIDForTest();
            loc_id = getLocationIDForTest();
        }

        [Test]
        public void TestIssueListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            string workcenterId = "";
            // Filter combinations
            DataTable dt = manager.getFilteredIssuedCollection(filterColumn, filterText, workcenterId);
            Console.WriteLine("TestIssueListLoad rows (NONE): " + dt.Rows.Count);
            workcenterId = wc_id.ToString();
            dt = manager.getFilteredIssuedCollection(filterColumn, filterText, workcenterId);
            Console.WriteLine("TestIssueListLoad rows (WC_ID): " + dt.Rows.Count);
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getFilteredIssuedCollection(filterColumn, filterText, workcenterId);
            Console.WriteLine("TestIssueListLoad rows (NIIN): " + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getFilteredIssuedCollection(filterColumn, filterText, workcenterId);
            Console.WriteLine("TestIssueListLoad rows (DESC): " + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getFilteredIssuedCollection(filterColumn, filterText, workcenterId);
            Console.WriteLine("TestIssueListLoad rows (CAGE): " + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getFilteredIssuedCollection(filterColumn, filterText, workcenterId);
            Console.WriteLine("TestIssueListLoad rows (MFG): " + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestIssueBreakoutInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            string manufacturer_date = DateTime.Now.ToShortDateString();
            int issued_qty = 2010;

            List<int> idList = manager.createIssue(new List<VolumeOffloadItem>(),
                mfg_catalog_id, issued_qty, user_id, wc_id, shelf_life_expiration_date, 
                shelf_life_expiration_date, 0, 2010, "BT", null, null, null, null, 
                "HME", null, "ZZTOP9");

            int issue_id = idList[0];
            int inventory_id = idList[1];
            Console.WriteLine("TestIssueBreakoutInsertandDelete issue_id:" + issue_id);
            Assert.True(issue_id > 0);

            //Confirm  issue insert
            DataTable issue = manager.findIssue(issue_id);
            Assert.True(issue.Rows.Count==1);
            DataRow row = issue.Rows[0];
            Assert.AreEqual(mfg_catalog_id.ToString(), row["mfg_catalog_id"].ToString());
            Assert.AreEqual(issued_qty.ToString(), row["issued_qty"].ToString());
            Assert.AreEqual("BT", row["alternate_ui"].ToString());
            Assert.AreEqual("ZZTOP9", row["serial_number"].ToString());

            //Confirm inventory insert
            DataTable inventory = manager.findInventory(inventory_id);
            Assert.True(inventory.Rows.Count == 1);
            row = inventory.Rows[0];
            Assert.True(row["mfg_catalog_id"].ToString() == mfg_catalog_id.ToString());
            Assert.True(row["qty"].ToString() == "2010");
            Assert.True(row["alternate_ui"].ToString() == "BT");

            //Delete what was inserted test
            manager.deleteIssue(issue_id);
            manager.deleteInventory(inventory_id);
            issue = manager.findIssue(issue_id);
            Assert.True(issue.Rows.Count == 0);
            inventory = manager.findInventory(inventory_id);
            Assert.True(inventory.Rows.Count == 0);
        }

        [Test]
        public void TestIssueNonBreakoutInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            string manufacturer_date = DateTime.Now.ToShortDateString();
            int issued_qty = 2010;

            //Volume test
            List<VolumeOffloadItem> volumes = new List<VolumeOffloadItem>();
            for (int i = 1; i <= 6; i++)
            {
                volumes.Add(new VolumeOffloadItem(i));
            }



            List<int> idList = manager.createIssue(volumes, mfg_catalog_id, issued_qty, 
                user_id, wc_id, shelf_life_expiration_date, shelf_life_expiration_date, 0, 
                2010, null, "123", "456", "789", "EA", "HME", null, "ZZTOP9");
            
            int issue_id = idList[0];
            int inventory_id = idList[1];
            Console.WriteLine("TestIssueNonBreakoutInsertandDelete issue_id:" + issue_id);
            Assert.True(issue_id > 0);


            //Volume test
            DataTable volList = manager.getVolumesInIssue(issue_id);
            Assert.True(volList.Rows.Count == 6);


            //Confirm  issue insert
            DataTable issue = manager.findIssue(issue_id);
            Assert.True(issue.Rows.Count == 1);
            DataRow row = issue.Rows[0];
            Assert.True(row["mfg_catalog_id"].ToString() == mfg_catalog_id.ToString());
            Assert.True(row["issued_qty"].ToString() == issued_qty.ToString());
            Assert.True(row["alternate_ui"] == DBNull.Value);

            //Confirm inventory insert
            DataTable inventory = manager.findInventory(inventory_id);
            Assert.True(inventory.Rows.Count == 1);
            row = inventory.Rows[0];
            Assert.True(row["mfg_catalog_id"].ToString() == mfg_catalog_id.ToString());
            Assert.True(row["qty"].ToString() == "2010");
            Assert.True(row["alternate_ui"] == DBNull.Value);

            //Delete what was inserted test
            manager.deleteIssue(issue_id);
            manager.deleteInventory(inventory_id);
            issue = manager.findIssue(issue_id);
            Assert.True(issue.Rows.Count == 0);
            inventory = manager.findInventory(inventory_id);
            Assert.True(inventory.Rows.Count == 0);
        }

        [Test]
        public void TestIssuedInvDeleteOnInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string shelf_life_expiration_date = DateTime.Now.ToShortDateString();
            int mfg_catalog_id = manager.getExistingMfgCatalogId();
            string manufacturer_date = DateTime.Now.ToShortDateString();
            int issued_qty = 2010;

            List<int> idList = manager.createIssue(new List<VolumeOffloadItem>(), 
                mfg_catalog_id, issued_qty, user_id, wc_id, shelf_life_expiration_date, 
                shelf_life_expiration_date, 0, 2010, null, "B1233223", null, 
                "CN4432001", "EA", "HME", null, "ZZTOP");

            int issue_id = idList[0];
            int inventory_id = idList[1];
            Console.WriteLine("TestIssuedInvDeleteOnInsertandDelete issue_id:" + issue_id + " /inventory_id:" + inventory_id);
            Assert.True(issue_id > 0);

            //Confirm  issue insert
            DataTable issue = manager.findIssue(issue_id);
            Assert.True(issue.Rows.Count == 1);
            DataRow row = issue.Rows[0];
            Assert.True(row["mfg_catalog_id"].ToString() == mfg_catalog_id.ToString());
            Assert.True(row["issued_qty"].ToString() == issued_qty.ToString());
            Assert.True(row["alternate_ui"] == DBNull.Value);

            //Confirm inventory insert
            DataTable inventory = manager.findInventory(inventory_id);
            Assert.True(inventory.Rows.Count == 1);
            row = inventory.Rows[0];
            Assert.True(row["mfg_catalog_id"].ToString() == mfg_catalog_id.ToString());
            Assert.True(row["qty"].ToString() == "2010");
            Assert.True(row["alternate_ui"] == DBNull.Value);

            //Delete what was inserted test
            manager.deleteIssue(issue_id);           
            issue = manager.findIssue(issue_id);
            Assert.True(issue.Rows.Count == 0);          

            //Re-issue the inventory we've just created with a new decremented_qty of 0 passed in to createIssue
            //That inventory should get deleted.

            idList = manager.createIssue(new List<VolumeOffloadItem>(), 
                mfg_catalog_id, issued_qty, user_id, wc_id, 
                shelf_life_expiration_date, shelf_life_expiration_date, 
                inventory_id, 0, null, "11111", "22222", null, null, "HME", null,
                "ZZTOP9");

            int issue_id2 = idList[0];
            int inventory_id2 = idList[1];
            Console.WriteLine("TestIssuedInvDeleteOnInsertandDelete issue_id:" + issue_id2 + " /inventory_id:"+inventory_id2);
            Assert.True(issue_id2 > 0);

            //confirm that the inventory_id passed in has been deleted.
            inventory = manager.findInventory(inventory_id);
            Assert.True(inventory.Rows.Count == 0);

            //Delete what was inserted
            manager.deleteIssue(issue_id2);
            manager.deleteInventory(inventory_id2);
            issue = manager.findIssue(issue_id2);
            Assert.True(issue.Rows.Count == 0);
            inventory = manager.findInventory(inventory_id2);
            Assert.True(inventory.Rows.Count == 0);           
        }

        [Test]
        public void TestAuthorizedUserListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);            
            string workcenterId = "";
            DataTable dt = manager.getAuthorizedUserCollection(workcenterId, false);
            Console.WriteLine("TestAuthorizedUserListLoad rows:" + dt.Rows.Count);
            workcenterId = wc_id.ToString();
            dt = manager.getAuthorizedUserCollection(workcenterId, false);
            Console.WriteLine("TestAuthorizedUserListLoad rows:" + dt.Rows.Count);            
            Assert.Pass();
        }

        [Test]
        public void TestAuthorizedUserInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            // The user needs to be inserted into both the
            // user_roles table and the authorized_users table
            string manufacturer_date = DateTime.Now.ToShortDateString(); 
            
            // Insert the user into the user_roles table
            int user_role_id = manager.insertAdminUser("USERTEST", "WORKCENTER USER");
            Console.WriteLine("TestAuthorizedUserInsertandDelete user_role_id:" + user_role_id);
            Assert.True(user_role_id > 0);

            // Insert the user into the authorized_users table
            int auth_user_id = manager.insertAuthorizedUser("Unit", "Test", "USERTEST", wc_id);
            Console.WriteLine("TestAuthorizedUserInsertandDelete authorized_user_id:" + auth_user_id);
            Assert.True(auth_user_id > 0);

            DataTable dt = manager.getAuthorizedUserCollection("", false);
            Console.WriteLine("User rows: " + dt.Rows.Count);
            foreach (DataRow row in dt.Rows) {
                Console.WriteLine("User: " + row["authorized_user_id"].ToString() + "\t" + row["username"].ToString());
            }

            // Delete what was inserted test
            manager.deleteAuthorizedUser(auth_user_id);
            manager.deleteAdminUser(user_role_id);
            bool deleted = true;

            dt = manager.getAuthorizedUserCollection("", false);
            Console.WriteLine("User rows: " + dt.Rows.Count);
            
            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine("User: " + row["authorized_user_id"].ToString());
                int id = 0;
                int.TryParse(row["authorized_user_id"].ToString(), out id);
                int role_id = 0;
                int.TryParse(row["user_role_id"].ToString(), out role_id);

                if (id == auth_user_id || role_id == user_role_id)
                    deleted = false;
            }
            Assert.True(deleted);
        }

        [Test]
        public void TestSS01InventoryListLoad()
        {
            //Test grabbing all records and each filter column.
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";           
            DataTable dt = manager.getFilteredInventoryCollectionForIssue(filterColumn, filterText);
            Console.WriteLine("TestSS01InventoryListLoad rows:" + dt.Rows.Count);           
            filterColumn = "NIIN";
            filterText = "00";
            dt = manager.getFilteredInventoryCollectionForIssue(filterColumn, filterText);
            Console.WriteLine("TestSS01InventoryListLoad rows:" + dt.Rows.Count);
            filterColumn = "Description";
            filterText = "S";
            dt = manager.getFilteredInventoryCollectionForIssue(filterColumn, filterText);
            Console.WriteLine("TestSS01InventoryListLoad rows:" + dt.Rows.Count);
            filterColumn = "CAGE";
            filterText = "A";
            dt = manager.getFilteredInventoryCollectionForIssue(filterColumn, filterText);
            Console.WriteLine("TestSS01InventoryListLoad rows:" + dt.Rows.Count);
            filterColumn = "MANUFACTURER";
            filterText = "CO";
            dt = manager.getFilteredInventoryCollectionForIssue(filterColumn, filterText);
            Console.WriteLine("TestSS01InventoryListLoad rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestNoSS01WorkcenterListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<ListItem> list = manager.getWorkcenterListNoSS01(true);
            Console.WriteLine("TestNoSS01WorkcenterListLoad rows:" + list.Count);
            foreach (ListItem item in list)
            {
                if (item.Text.StartsWith("SS01"))
                    Assert.Fail("Found SS01 where there should be none");
            }
        }

        private int getWorkcenterIDForTest() {
            // Gets a random inventory item from the test database 
            int wc_id = 0;

            DatabaseManager manager = new DatabaseManager(connectionString);

            string sql = "SELECT TOP 1 workcenter_id "
                + "FROM workcenter "
                + "WHERE workcenter_id IN (SELECT DISTINCT workcenter_id FROM locations) "
                + "ORDER BY NEWID();";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                // inventory_id
                int.TryParse(rdr["workcenter_id"].ToString(), out wc_id);

                Console.WriteLine("Workcenter ID for Test: " + wc_id.ToString());
            }

            return wc_id;

        }

        private int getAuthUserIDForTest() {
            // Gets a random inventory item from the test database 
            int id = 0;

            DatabaseManager manager = new DatabaseManager(connectionString);

            string sql = "SELECT TOP 1 authorized_user_id "
                + "FROM authorized_users "
                + "ORDER BY NEWID();";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                // inventory_id
                int.TryParse(rdr["authorized_user_id"].ToString(), out id);

                Console.WriteLine("Auth User ID for Test: " + id.ToString());
            }

            return id;
        }

        private int getUserRoleIDForTest() {
            // Gets a random inventory item from the test database 
            int id = 0;

            DatabaseManager manager = new DatabaseManager(connectionString);

            string sql = "SELECT TOP 1 user_role_id "
                + "FROM user_roles "
                + "ORDER BY NEWID();";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                // inventory_id
                int.TryParse(rdr["user_role_id"].ToString(), out id);

                Console.WriteLine("User Role ID for Test: " + id.ToString());
            }

            return id;
        }

        private int getLocationIDForTest() {
            // Gets a random inventory item from the test database 
            int id = 0;

            DatabaseManager manager = new DatabaseManager(connectionString);

            string sql = "SELECT TOP 1 location_id "
                + "FROM locations "
                + "ORDER BY NEWID();";

            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read()) {
                // inventory_id
                int.TryParse(rdr["location_id"].ToString(), out id);

                Console.WriteLine("Location ID for Test: " + id.ToString());
            }

            return id;
        }
    }
}
