﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

namespace HAZMATUnit
{
    [TestFixture]
    class InsurvChecklistTests
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestInsurvChecklistLoad()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getLatestInsurvChecklist();

            Console.WriteLine("TestInsurvChecklistLoad rows:" + dt.Rows.Count);

            Assert.AreEqual(dt.Rows.Count, 1);
        }

        [Test]
        public void TestInsurvChecklistUpdate()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            ChecklistInsurv checklist = new ChecklistInsurv();

            string yes = "YES";
            string no = "NO";
            string DEFAULT =  "N/A";

            //set some test values
            checklist.ATM_CON_TAGS_A = yes;
            checklist.ATM_AUTHORIZATION_A = yes;
            checklist.ATM_CONTROL_REVIEW_A = yes;
            checklist.ATM_MATERIAL_CON_A = yes;
            checklist.BROMINE_A = yes;
            checklist.CALCIUM_A = yes;
            checklist.COMPRESSED_GAS_A = yes;
            checklist.HAZMAT_COORD_A = yes;
            checklist.HAZMAT_INVENTORY_A = yes;
            checklist.HAZMAT_STOWAGE_A = yes;
            checklist.HAZMAT_SUPERVISOR_A = yes;
            checklist.IN_USE_FLAM_A = yes;
            checklist.LABELING_A = yes;
            checklist.LOG_ENTRIES_A = yes;
            checklist.MSDS_A = yes;
            checklist.OPEN_PURCHASE_A = yes;
            checklist.OTTO_FUEL_A = yes;
            checklist.SPILL_TRAIN_A = yes;
            checklist.SUBMARINE_MANAGEMENT_A = yes;

            checklist.ATM_CONTROL_REVIEW_C = no;
            checklist.BROMINE_C = no;
            checklist.CALCIUM_C = no;
            checklist.COMPRESSED_GAS_C = no;
            checklist.HAZMAT_COORD_C = no;
            checklist.HAZMAT_STOWAGE_C = no;
            checklist.LABELING_C = no;
            checklist.LOG_ENTRIES_C = no;
            checklist.MSDS_C = no;
            checklist.OPEN_PURCHASE_C = no;
            checklist.OTTO_FUEL_C = no;
            checklist.SUBMARINE_MANAGEMENT_C = no;

            //update database
            manager.updateInsurvChecklist(checklist);

            //grab record back
            DataTable dt = manager.getLatestInsurvChecklist();

            //confirm results
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.EndsWith("A"))
                    {
                        //confirm all the A's as yes
                        string value = row[col].ToString();
                        Assert.AreEqual(value, yes);
                    }
                    else if (col.ColumnName.EndsWith("C"))
                    {
                        //confirm all the C's as no
                        string value = row[col].ToString();
                        Assert.AreEqual(value, no);
                    }
                    else if (!col.ColumnName.Equals("checklist_id"))
                    {
                        //confirm rest as 'N/A'
                        string value = row[col].ToString();
                        Assert.AreEqual(value, DEFAULT);
                    }
                }
            }

        }

        [Test]
        public void TestAdminUserListLoad()
        {
            //Test grabbing all records
            DatabaseManager manager = new DatabaseManager(connectionString);
            DataTable dt = manager.getAdminUsersCollection(true);
            dt = manager.getAdminUsersCollection(false);
            Console.WriteLine("TestAdminUserListLoad rows:" + dt.Rows.Count);            
            Assert.Pass();
        }

        [Test]
        public void TestAdminUserInsertandDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            int user_role_id = manager.insertAdminUser("UNIT/TEST", "ADMIN");
            Console.WriteLine("TestAdminUserInsertandDelete user_role_id:" + user_role_id);
            Assert.True(user_role_id > 0);

            //Delete what was inserted test
            manager.deleteAdminUser(user_role_id);
            bool deleted = true;
            DataTable dt = manager.getAdminUsersCollection(false);
            foreach (DataRow row in dt.Rows)
            {
                int id = Int32.Parse(row["user_role_id"].ToString());
                if (id == user_role_id)
                    deleted = false;
            }
            Assert.True(deleted);
        }


        [Test]
        public void TestInsurvChecklist_BromineProgessBarsLoad()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getBromineAuditCounts();

            Console.WriteLine("TestInsurvChecklist_BromineProgessBarsLoad rows:" + dt.Rows.Count);

            Assert.AreEqual(dt.Rows.Count, 1);

            DataRow row = dt.Rows[0];

            double total = Int32.Parse(row["total"].ToString());
            double answered = Int32.Parse(row["answered"].ToString());

            Assert.True(answered <= total);//answered cannot be greater than the total.

            List<string> letters = new List<string>();
            letters.Add("A");
            letters.Add("B");
            letters.Add("C");
            letters.Add("D");

            foreach (string questionLetter in letters)
            {
                //Get Question values from datarow. columns from view must use same naming conventions.
                double yesQ = Int32.Parse(row["yes" + questionLetter].ToString());
                double noQ = Int32.Parse(row["no" + questionLetter].ToString());
                double n_aQ = Int32.Parse(row["n_a" + questionLetter].ToString());

                double combined = yesQ + noQ + n_aQ;

                //Confirm count of each answer is equal to the number answered.
                Assert.AreEqual(answered, combined);
            }

        }

        [Test]
        public void TestInsurvChecklist_CalciumProgessBarsLoad()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getCalciumAuditCounts();

            Console.WriteLine("TestInsurvChecklist_CalciumProgessBarsLoad rows:" + dt.Rows.Count);

            Assert.AreEqual(dt.Rows.Count, 1);

            DataRow row = dt.Rows[0];

            double total = Int32.Parse(row["total"].ToString());
            double answered = Int32.Parse(row["answered"].ToString());

            Assert.True(answered <= total);//answered cannot be greater than the total.

            List<string> letters = new List<string>();
            letters.Add("A");
            letters.Add("B");
            letters.Add("C");
            letters.Add("D");
            letters.Add("E");
            letters.Add("F");
            letters.Add("G");
            letters.Add("H");
            letters.Add("I");
            letters.Add("J");


            foreach (string questionLetter in letters)
            {
                //Get Question values from datarow. columns from view must use same naming conventions.
                double yesQ = Int32.Parse(row["yes" + questionLetter].ToString());
                double noQ = Int32.Parse(row["no" + questionLetter].ToString());
                double n_aQ = Int32.Parse(row["n_a" + questionLetter].ToString());

                double combined = yesQ + noQ + n_aQ;

                //Confirm count of each answer is equal to the number answered.
                Assert.AreEqual(answered, combined);
            }

        }

        [Test]
        public void TestInsurvChecklist_AtmTagsProgessBarsLoad()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getTagsAuditCounts();

            Console.WriteLine("TestInsurvChecklist_AtmTagsProgessBarsLoad rows:" + dt.Rows.Count);

            Assert.AreEqual(dt.Rows.Count, 1);

            DataRow row = dt.Rows[0];

            double total = Int32.Parse(row["total"].ToString());
            double answered = Int32.Parse(row["answered"].ToString());

            Assert.True(answered <= total);//answered cannot be greater than the total.

            List<string> letters = new List<string>();

            letters.Add("A");
            letters.Add("B");


            foreach (string questionLetter in letters)
            {
                //Get Question values from datarow. columns from view must use same naming conventions.
                double yesQ = Int32.Parse(row["yes" + questionLetter].ToString());
                double noQ = Int32.Parse(row["no" + questionLetter].ToString());
                double n_aQ = Int32.Parse(row["n_a" + questionLetter].ToString());

                double combined = yesQ + noQ + n_aQ;

                //Confirm count of each answer is equal to the number answered.
                Assert.AreEqual(answered, combined);
            }

        }

    }
}
