﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

namespace HAZMATUnit
{
     [TestFixture]
    class MSDSWithChangedHCCTest
    {
         private string connectionString = Configuration.ConnectionInfo;

         [Test]
         public void Test_getMSDSWithChangedHCCTest()
         {
             // Filter Parameters
             String filterColumn = "";
             String filterText = "";
             //Test
             DatabaseManager manager = new DatabaseManager(connectionString);

             DataTable dt = manager.getMSDSWithChangedHCC(filterColumn, filterText);

             int nRows = dt.Rows.Count;

             Console.WriteLine("DataTable rows = " + nRows);

             // Get the expected count
             string sql = "select COUNT(*) from msds m1 "
                + "join msds m2 on m2.MSDSSERNO = m1.MSDSSERNO "
                + "where ISNULL(m2.hcc_id, 0) != ISNULL(m1.hcc_id, 0) " 
	            + "    and m1.msds_id = (SELECT MIN(msds_id) FROM msds where MSDSSERNO = m1.MSDSSERNO) "
	            + "    and m2.msds_id = (SELECT MAX(msds_id) FROM msds where MSDSSERNO = m1.MSDSSERNO)";
             SqlConnection conn =
                new SqlConnection(Configuration.ConnectionInfo);

             conn.Open();

             SqlCommand cmd = new SqlCommand(sql, conn);

             int expected = (Int32)cmd.ExecuteScalar();

             conn.Close();

             Assert.AreEqual(expected, nRows);
         }


    }
}
