﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

namespace HAZMATUnit
{
    [TestFixture]
    class INSURVTests
    {
        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void TestInsurvAuditNiinTest()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string surv_name = "BROMINE";
            DataTable dt = manager.getINSURVAuditNIINs(surv_name);
            Console.WriteLine("TestInsurvAuditNiinTest rows:" + dt.Rows.Count);
            surv_name = "CALCIUM";
            dt = manager.getINSURVAuditNIINs(surv_name);
            Console.WriteLine("TestInsurvAuditNiinTest rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        //Bromine tests

        [Test]
        public void TestBromine_locationsListLoad()
        {
            //test load and filter combinations
            DatabaseManager manager = new DatabaseManager(connectionString);
            string location = "";
            string workcenter = "";
            string surv_name = "BROMINE";
            string location_done = "";
            DataTable dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            location = "a";
            workcenter = "";
            location_done = "";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "2";
            location_done = "";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "";
            location_done = "DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "";
           location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            location = "c";
            workcenter = "3";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            location = "1";
            workcenter = "";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestBromine_locationsListLoad rows:" + dt.Rows.Count);

            Assert.Pass();
        }

        [Test]
        public void TestBromine_inventoryListLoad()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            string location_id = "1";
            string surv_name = "BROMINE";
            DataTable dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestBromine_inventoryListLoad rows:" + dt.Rows.Count);

            filterColumn = "NIIN";
            filterText = "h";
            location_id = "2";
            dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestBromine_inventoryListLoad rows:" + dt.Rows.Count);

            filterColumn = "description";
            filterText = "t";
            location_id = "3";
            dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestBromine_inventoryListLoad rows:" + dt.Rows.Count);

            filterColumn = "SPMIG";
            filterText = "1";
            location_id = "4";
            dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestBromine_inventoryListLoad rows:" + dt.Rows.Count);

            Assert.Pass();
        }


        [Test]
        public void TestBromine_auditInsertAndDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            
            string A = "YES";
            string B = "NO";
            string C = "N/A";
            string D = "NO";
            int location_id = manager.getExistingLocationId();
            DataTable bromineRecords = manager.getFilteredInventoryCollectionForINSURVAudit("", "", ""+location_id, "BROMINE");
            Console.WriteLine("TestBromine_auditInsertAndDelete rows:" + bromineRecords.Rows.Count);
            DateTime action_complete  = DateTime.Now;

            //insert
            List<int> bromindIds = manager.insertBromineChangeRecords(location_id, bromineRecords, A, B, C, D, action_complete);

            //confirm data
            foreach (int bromine_id in bromindIds)
            {
                DataTable dt = manager.findBromineAudit(bromine_id);
                Assert.AreEqual(1, dt.Rows.Count);
                DataRow row = dt.Rows[0];
                Assert.AreEqual(A, row["A"].ToString());
                Assert.AreEqual(B, row["B"].ToString());
                Assert.AreEqual(C, row["C"].ToString());
                Assert.AreEqual(D, row["D"].ToString());
                Assert.AreEqual("" + location_id, row["location_id"].ToString());
                Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());

            }
           
            //delete
            manager.deleteBromineChangeRecords(bromindIds);

            Assert.Pass();
        }

        [Test]
        public void TestBromine_locationCrInsertAndDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            DateTime action_complete = DateTime.Now;
            //insert
            int insurv_location_cr = manager.insertInsurvLocationChangeRecord(location_id, null, true, action_complete, "BROMINE");

            DataTable insurvData = manager.findInsurvLocationCr(insurv_location_cr);
            DataRow row = insurvData.Rows[0];

            //confirm
            Assert.AreEqual("" + location_id, row["location_id"].ToString());
            Assert.AreEqual("True", row["location_done"].ToString());
            Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());
            Assert.AreEqual("BROMINE", row["surv_name"].ToString());

            //update
            action_complete = DateTime.Now;
            insurv_location_cr = manager.insertInsurvLocationChangeRecord(location_id, insurv_location_cr, true, action_complete, "BROMINE");

            insurvData = manager.findInsurvLocationCr(insurv_location_cr);
            row = insurvData.Rows[0];

            //confirm
            Assert.AreEqual("" + location_id, row["location_id"].ToString());
            Assert.AreEqual("True", row["location_done"].ToString());
            Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());
            Assert.AreEqual("BROMINE", row["surv_name"].ToString());

            //delete
            manager.deleteInsurvLocationChangeRecord(insurv_location_cr);

            Assert.Pass();
        }


        //Calcium tests

        [Test]
        public void TestCalcium_locationsListLoad()
        {
            //test load and filter combinations
            DatabaseManager manager = new DatabaseManager(connectionString);
            string location = "";
            string workcenter = "";
            string surv_name = "CALCIUM";
            string location_done = "";
            DataTable dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            location = "a";
            workcenter = "";
            location_done = "";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "2";
            location_done = "";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "";
            location_done = "DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            location = "c";
            workcenter = "3";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            location = "1";
            workcenter = "";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollection(location, workcenter, surv_name, location_done);
            Console.WriteLine("TestCalcium_locationsListLoad rows:" + dt.Rows.Count);

            Assert.Pass();
        }

        [Test]
        public void TestCalcium_inventoryListLoad()
        {
            //Test
            DatabaseManager manager = new DatabaseManager(connectionString);
            string filterColumn = "";
            string filterText = "";
            string location_id = "1";
            string surv_name = "CALCIUM";
            DataTable dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestCalcium_inventoryListLoad rows:" + dt.Rows.Count);

            filterColumn = "NIIN";
            filterText = "h";
            location_id = "2";
            dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestCalcium_inventoryListLoad rows:" + dt.Rows.Count);

            filterColumn = "description";
            filterText = "t";
            location_id = "3";
            dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestCalcium_inventoryListLoad rows:" + dt.Rows.Count);

            filterColumn = "SPMIG";
            filterText = "1";
            location_id = "4";
            dt = new DatabaseManager().getFilteredInventoryCollectionForINSURVAudit(filterColumn, filterText, location_id, surv_name);
            Console.WriteLine("TestCalcium_inventoryListLoad rows:" + dt.Rows.Count);

            Assert.Pass();
        }


        [Test]
        public void TestCalcium_auditInsertAndDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            string A = "YES";
            string B = "NO";
            string C = "N/A";
            string D = "NO";

            string E = "YES";
            string F = "NO";
            string G = "N/A";
            string H = "NO";

            string I = "N/A";
            string J = "NO";

            int location_id = manager.getExistingLocationId();
            DataTable calciumRecords = manager.getFilteredInventoryCollectionForINSURVAudit("", "", "" + location_id, "CALCIUM");
            Console.WriteLine("TestCalcium_auditInsertAndDelete rows:" + calciumRecords.Rows.Count);
            DateTime action_complete = DateTime.Now;

            //insert
            List<int> calciumIds = manager.insertCalciumChangeRecords(location_id, calciumRecords, A, B, C, D,E,F,G,H,I,J, action_complete);

            //confirm data
            foreach (int calcium_id in calciumIds)
            {
                DataTable dt = manager.findCalciumAudit(calcium_id);
                Assert.AreEqual(1, dt.Rows.Count);
                DataRow row = dt.Rows[0];
                Assert.AreEqual(A, row["A"].ToString());
                Assert.AreEqual(B, row["B"].ToString());
                Assert.AreEqual(C, row["C"].ToString());
                Assert.AreEqual(D, row["D"].ToString());
                Assert.AreEqual(E, row["E"].ToString());
                Assert.AreEqual(F, row["F"].ToString());
                Assert.AreEqual(G, row["G"].ToString());
                Assert.AreEqual(H, row["H"].ToString());
                Assert.AreEqual(I, row["I"].ToString());
                Assert.AreEqual(J, row["J"].ToString());            
                Assert.AreEqual("" + location_id, row["location_id"].ToString());
                Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());

            }

            //delete
            manager.deleteCalciumChangeRecords(calciumIds);

            Assert.Pass();
        }

        [Test]
        public void TestCalcium_locationCrInsertAndDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            DateTime action_complete = DateTime.Now;
            //insert
            int insurv_location_cr = manager.insertInsurvLocationChangeRecord(location_id, null, true, action_complete, "CALCIUM");

            DataTable insurvData = manager.findInsurvLocationCr(insurv_location_cr);
            DataRow row = insurvData.Rows[0];

            //confirm
            Assert.AreEqual("" + location_id, row["location_id"].ToString());
            Assert.AreEqual("True", row["location_done"].ToString());
            Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());
            Assert.AreEqual("CALCIUM", row["surv_name"].ToString());

            //update
            action_complete = DateTime.Now;
            insurv_location_cr = manager.insertInsurvLocationChangeRecord(location_id, insurv_location_cr, true, action_complete, "CALCIUM");

            insurvData = manager.findInsurvLocationCr(insurv_location_cr);
            row = insurvData.Rows[0];

            //confirm
            Assert.AreEqual("" + location_id, row["location_id"].ToString());
            Assert.AreEqual("True", row["location_done"].ToString());
            Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());
            Assert.AreEqual("CALCIUM", row["surv_name"].ToString());

            //delete
            manager.deleteInsurvLocationChangeRecord(insurv_location_cr);

            Assert.Pass();
        }


        //TAG tests

        [Test]
        public void TestTag_locationsListLoad()
        {
            //test load and filter combinations
            DatabaseManager manager = new DatabaseManager(connectionString);
            string location = "";
            string workcenter = "";           
            string location_done = "";
            //getInsurvLocationCollectionForAtmTags(location, workCenter, location_done);
            DataTable dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            location = "a";
            workcenter = "";
            location_done = "";
            dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "2";
            location_done = "";
            dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "";
            location_done = "DONE";
            dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            location = "";
            workcenter = "";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            location = "c";
            workcenter = "3";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            location = "1";
            workcenter = "";
            location_done = "NOT DONE";
            dt = new DatabaseManager().getInsurvLocationCollectionForAtmTags(location, workcenter, location_done);
            Console.WriteLine("TestTag_locationsListLoad rows:" + dt.Rows.Count);

            Assert.Pass();
        }



        [Test]
        public void TestTag_auditInsertAndDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            string A = "YES";
            string B = "NO";
           

            int location_id = manager.getExistingLocationId();
            string location = Convert.ToString(location_id);
            DataTable tagRecords = manager.getFilteredInventoryCollectionForINSURVAtmTag("","",location);
            Console.WriteLine("TestTag_auditInsertAndDelete rows:" + tagRecords.Rows.Count);
            DateTime action_complete = DateTime.Now;

            //insert
            List<int> tagIds = manager.insertAtmChangeRecords(location_id, tagRecords, A, B, action_complete);

            //confirm data
            foreach (int tagId in tagIds)
            {
                DataTable dt = manager.findTagAudit(tagId);
                Assert.AreEqual(1, dt.Rows.Count);
                DataRow row = dt.Rows[0];
                Assert.AreEqual(A, row["A"].ToString());
                Assert.AreEqual(B, row["B"].ToString());
               
                Assert.AreEqual("" + location_id, row["location_id"].ToString());
                Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());

            }

            //delete
            manager.deleteTagChangeRecords(tagIds);

            Assert.Pass();
        }


        [Test]
        public void TestTag_locationCrInsertAndDelete()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);

            int location_id = manager.getExistingLocationId();
            DateTime action_complete = DateTime.Now;
            //insert
            int insurv_location_cr = manager.insertInsurvLocationChangeRecord(location_id, null, true, action_complete, "TAGS");

            DataTable insurvData = manager.findInsurvLocationCr(insurv_location_cr);
            DataRow row = insurvData.Rows[0];

            //confirm
            Assert.AreEqual("" + location_id, row["location_id"].ToString());
            Assert.AreEqual("True", row["location_done"].ToString());
            Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());
            Assert.AreEqual("TAGS", row["surv_name"].ToString());

            //update
            action_complete = DateTime.Now;
            insurv_location_cr = manager.insertInsurvLocationChangeRecord(location_id, insurv_location_cr, true, action_complete, "TAGS");

            insurvData = manager.findInsurvLocationCr(insurv_location_cr);
            row = insurvData.Rows[0];

            //confirm
            Assert.AreEqual("" + location_id, row["location_id"].ToString());
            Assert.AreEqual("True", row["location_done"].ToString());
            Assert.AreEqual(action_complete.ToString(), row["action_complete"].ToString());
            Assert.AreEqual("TAGS", row["surv_name"].ToString());

            //delete
            manager.deleteInsurvLocationChangeRecord(insurv_location_cr);

            Assert.Pass();
        }

        [Test]
        public void Test_insertAndDeleteInsurvNiin()
        {
            //Insert test
            DatabaseManager manager = new DatabaseManager(connectionString);
            int niin_catalog_id = manager.getExistingNiinIdWithNoInventory();
            int id = manager.insertInsurvNiin("BROMINE", niin_catalog_id);

            manager.deleteInsurvNiin(id);

            Assert.Pass();
        }

    }
}
