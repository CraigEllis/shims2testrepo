﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace HAZMATUnit
{
    public class Configuration {
        public static string ConnectionInfo  {
            get {
                return ConfigurationManager.ConnectionStrings["HAZMAT"].ConnectionString;                 
            }
        }

        public static string Path {
            get {
                return ConfigurationManager.ConnectionStrings["AspProjectPath"].ConnectionString;
            }
        }

        public static string ResourcesPath {
            get {
                Console.WriteLine(ConfigurationManager.AppSettings["HAZMATResourcesPath"]);

                return ConfigurationManager.AppSettings["HAZMATResourcesPath"];
            }
        }

        public static string ImagesPath {
            get {
                Console.WriteLine(ConfigurationManager.AppSettings["HAZMATImagesPath"]);

                return ConfigurationManager.AppSettings["HAZMATImagesPath"];
            }
        }
    }
}
