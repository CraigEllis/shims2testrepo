﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;

namespace HAZMATUnit
{
    [TestFixture]
    class SMCLTests
    {

        private string connectionString = Configuration.ConnectionInfo;
      

        [Test]
        public void TestMSDSGridLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            string niin = null;          
            DataTable dt = manager.getMSDSforSMCL(niin, true);
            Console.WriteLine("TestMSDSGridLoad rows:" + dt.Rows.Count);
            Assert.True(dt.Rows.Count == 0);
        }

        [Test]
        public void Test_getMatchingMSDSForNiinCageContractSerno()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            string niin = "006600027";
            string cage = "0";
            string contract_number = "";
            string msdsserno = "C";

            DataTable dt = manager.getMSDSforNiinCageContractSerno(niin, cage, contract_number, msdsserno, true);

            Console.WriteLine("Test_getMatchingMSDSForNiinCageContractSerno rows:" + dt.Rows.Count);
            Assert.Pass();
        }

        [Test]
        public void TestInventoryGridLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int catalog_id = 0;
            DataTable dt = manager.getInventoryForSMCLCollection(catalog_id);
            Console.WriteLine("TestInventoryGridLoad rows:" + dt.Rows.Count);
            Assert.True(dt.Rows.Count == 0);
        }

        [Test]
        public void TestCageManufacturerListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            int niin_catalog_id = 0;
            List<ListItem> list = manager.getCageManufacturerList(niin_catalog_id);
            Console.WriteLine("TestCageManufacturerListLoad rows:" + list.Count);
            Assert.True(list.Count==0);
        }

        [Test]
        public void TestUsageCategoryListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<ListItem> list = manager.getUsageCategoryList(false);
            list = manager.getUsageCategoryList(true);
            Console.WriteLine("TestUsageCategoryListLoad rows:" + list.Count);
            Assert.True(list.Count > 0);
        }

        [Test]
        public void TestCatalogGroupListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<ListItem> list = manager.getCatalogGroupList();
            Console.WriteLine("TestCatalogGroupListLoad rows:" + list.Count);
            Assert.True(list.Count > 0);
        }

        [Test]
        public void TestLocationWorkcenterListLoad()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            List<ListItem> list = manager.getLocationWorkcenterList(true);
            Console.WriteLine("TestLocationWorkcenterListLoad rows:" + list.Count);
            Assert.Pass();
        }

        [Test]
        public void Test_SMCLFileUpdate_FullFile()
        {           
            String file = Path.GetFullPath("TestFiles/SMCL/SMCLMASTER_012711-2.xls");

            Console.WriteLine("Input File: " + file);

            if (!File.Exists(file))
                Assert.Fail("Can't find " + file);

            List<string[]> list = new ExcelParser().parseExcel(file);

            int expectedCount = 3759;

            Assert.AreEqual(expectedCount, list.Count);

            string successDetails = new DatabaseManager().updateSMCLFromList(list, file);

            string expectedSuccessDetails = "2499 SMCL records Inserted/Updated.\n1203 blank and 56 duplicate records ignored.\n0 SFR records were marked completed. ";

            Assert.AreEqual(expectedSuccessDetails, successDetails);

            Assert.Pass();
        }        

        [Test]
        public void Test_SMCLFileUpdate_SingleRecord()
        {

            String file = Path.GetFullPath("TestFiles/SMCL/SMCLMASTER_1_record.xls");

            if (!File.Exists(file))
                Assert.Fail("Can't find " + file);


            List<string[]> list = new ExcelParser().parseExcel(file);

            int expectedCount = 3759;

            Assert.AreEqual(expectedCount, list.Count);

            string successDetails = new DatabaseManager().updateSMCLFromList(list, file);

            string expectedSuccessDetails = "1 SMCL records Inserted/Updated. 3757 blank and 0 duplicate records ignored. 0 SFR records were marked completed. ";

            Assert.AreEqual(expectedSuccessDetails, successDetails);

            Assert.Pass();
        }

             [Test]
             public void Test_SMCLFileUpdate_WrongHeaders()
             {

                 String file = Path.GetFullPath("TestFiles/SMCL/SMCLMASTER_bad_header.xls");

                 if (!File.Exists(file))
                     Assert.Fail("Can't find " + file);


                 List<string[]> list = new ExcelParser().parseExcel(file);

                 int expectedCount = 3759;

                 Assert.AreEqual(expectedCount, list.Count);

                 string result = "";
                 try
                 {                                     
                    string successDetails = new DatabaseManager().updateSMCLFromList(list, file);
                 }
                 catch (Exception ex)
                 {
                     result = ex.Message;
                 }

                 string expectedSuccessDetails = "File Format Error, the column header 'NIIN' was not found in the uploaded file.";

                 Assert.AreEqual(expectedSuccessDetails, result);

                 Assert.Pass();
             }

             [Test]
             public void Test_SMCLFileUpdate_IncorrectFileType()
             {

                 String file = Path.GetFullPath("TestFiles/SMCL/SMCL_tab_delimited_import.txt");

                 if (!File.Exists(file))
                     Assert.Fail("Can't find " + file);

                 string result = "";
                 try
                 {
                     List<string[]> list = new ExcelParser().parseExcel(file);
                 }
                 catch (Exception ex)
                 {
                     result = ex.Message;
                 }

                 string expectedSuccessDetails = "Error reading the excel file. External table is not in the expected format.";

                 Assert.AreEqual(expectedSuccessDetails, result);

                 Assert.Pass();
             }

        [Test]
        public void TestInsertDeleteNewMFGcage()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            string cage = "CAGE";
            string manufacturer = "MANUFACTURER";
            int niin_catalog_id = 0;
            int mfg_catalog_id = manager.insertMfgCatalog(cage, manufacturer, niin_catalog_id, null);
            manufacturer = "manufacturer number 2";
            manager.updateMfgCatalog(manufacturer, mfg_catalog_id);

            manager.deleteMfgCatalog(mfg_catalog_id);
            Assert.Pass();

        }

        [Test]
        public void TestInsertNewInventory()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            string manufacturerDateString = DateTime.Now.ToShortDateString();
            string shelfLifeDateString = null;
            int shelf_life_code_id = 5;
            int location_id = 0;
            int qty = 1;
            string serial_number = null;
            bool bulk_item = true;            
            int inventory_id =  manager.insertInventory(manufacturerDateString, 
                shelfLifeDateString, shelf_life_code_id, location_id, qty, serial_number, 
                bulk_item, 0, "123", "345", "456", "HME", null);
            
            Assert.True(inventory_id > 0);

            //Update the inventory we just added.
            manager.updateInventory(inventory_id, manufacturerDateString, shelfLifeDateString, 
                shelf_life_code_id, location_id, qty, serial_number, bulk_item, 0, 
                null, null, null, "HME", null);
            
            //cleanup
            manager.deleteInventory(inventory_id);            
            Assert.Pass();
        }

        [Test]
        public void TestUpdateInventory()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);
            string manufacturerDateString = null;
            string shelfLifeDateString = null;
            int shelf_life_code_id = 0;
            int location_id = 0;
            int qty = 1;
            string serial_number = null;
            bool bulk_item = true;
            int mfg_catalog_id = 0;
            int inventory_id = 0;
            manager.updateInventory(inventory_id, manufacturerDateString, shelfLifeDateString, 
                shelf_life_code_id, location_id, qty, serial_number, bulk_item, mfg_catalog_id, 
                null, null, null, "HME", null);
            Assert.Pass();
        }


        [Test]
        public void TestAllowanceQtyUpdate()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getSMCLCollection();
            DataRow row = dt.Rows[0];
            
            int niin_catalog_id = Int32.Parse(row["niin_catalog_id"].ToString());
            int orig_allowance_qty = Int32.Parse(row["allowance_qty"].ToString());

            int allowance_qty = 2010;
            //update
            manager.updateSMCLAllowanceQty(niin_catalog_id, allowance_qty);

            //confirm
            dt = manager.findNiinCatalog(niin_catalog_id);
            row = dt.Rows[0];

            int curr_allowance_qty = Int32.Parse(row["allowance_qty"].ToString());

            Assert.AreEqual(allowance_qty, curr_allowance_qty);

            //reset
            manager.updateSMCLAllowanceQty(niin_catalog_id, orig_allowance_qty);
        }

        [Test]
        public void TestRemarksUpdate()
        {
            DatabaseManager manager = new DatabaseManager(connectionString);

            DataTable dt = manager.getSMCLCollection();
            DataRow row = dt.Rows[0];

            int niin_catalog_id = Int32.Parse(row["niin_catalog_id"].ToString());
            string orig_remarks = row["remarks"].ToString();

            string remarks = "2010";
            //update
            manager.updateSMCLRemarks(niin_catalog_id, remarks);

            //confirm
            dt = manager.findNiinCatalog(niin_catalog_id);
            row = dt.Rows[0];

            string curr_remarks = row["remarks"].ToString();

            Assert.AreEqual(remarks, curr_remarks);

            //reset
            manager.updateSMCLRemarks(niin_catalog_id, orig_remarks);
        }
               
    }
}
