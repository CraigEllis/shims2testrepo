﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Diagnostics;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace HAZMATUnit
{
    [TestFixture]
    class DecantingPdfTest
    {

        private string connectionString = Configuration.ConnectionInfo;

        [Test]
        public void testPdfDownload()
        {
            //build a list of DecantingInfo
            DecantingInfo info = this.insertDecantedInfo();

            //temp file for the pdf
            String pdfTemplate = Properties.Settings.Default.HAZMAT_Resources_Path + "\\decantingLabels.pdf";
            //String pdfTemplate = Path.GetFullPath("decantingLabels.pdf");

            //file location of the white image used for covering labels
            String imageLocation = Properties.Settings.Default.HAZMAT_Resources_Path + "\\white.png";
            //String imageLocation = Path.GetFullPath("white.png");

            Console.WriteLine("PDF path: " + pdfTemplate + "\nImage location: " + imageLocation);

            //generate pdf
            DD2522Generator gen = new DD2522Generator(pdfTemplate, imageLocation);
            string completedPdf = gen.generateDD2522(info, 1);


            PdfReader reader = new PdfReader(completedPdf);

            Rectangle rec = reader.GetPageSize(1);
                        
            //i was unable at this time to figure out how to read the field data from the pdf. 
            //I will check the width and height of the page created. 
            if (rec != null)
            {
                if (rec.Height == 816 && rec.Width == 1056)
                {
                    reader.Close();
                    Debug.WriteLine("Height: " + rec.Height + " Width: " + rec.Width);
                    File.Delete(completedPdf);
                    Assert.Pass("pdf created successfully");
                }
                else
                {
                    reader.Close();
                    Debug.WriteLine("Height: " + rec.Height + " Width: " + rec.Width);
                    File.Delete(completedPdf);
                    Assert.Fail("PDF file has not been created. Unable to get the page size.");
                }
               
            }else{

               
                reader.Close();
                File.Delete(completedPdf);
                Assert.Fail("PDF file has not been created. Unable to get the page size.");

            }

         

            //read pdf and check the data

            //remove data

        }


       

        private DecantingInfo insertDecantedInfo()
        {        
            DecantingInfo info = new DecantingInfo();

            info.Cage = "3G1Q5";
            info.City = "Oxford";
            info.CompanyName = "Applied Enterprise Solutions";
            info.Country = "USA";
            info.Hazards = "This is the hazards section. This section can handle a lot of text.  This is the last random sentence I will type";
            info.Hcc = "CD";
            info.ItemName = "Helium Tank";
            info.Manufacturer = "Manufacturer";
            info.Phone = "555-555-5555";
            info.PoBox = "1210 Office Park Dr";
            info.ProductIdentity = "Helium ind.";
            info.ProductSerial = "S444443333";
            info.SignalWord = "signalWord";
            info.State = "MS";
            info.StockNumber = "223";          

            return info;
        }

    }
}
