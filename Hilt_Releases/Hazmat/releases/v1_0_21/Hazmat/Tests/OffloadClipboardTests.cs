﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using HAZMAT;
using System.Data;
using System.IO;
using System.Data.SQLite;
using System.Data.SqlClient;

namespace HAZMATUnit
{

    [TestFixture]
    class OffloadClipboardTests
    {
        private string connectionString = Configuration.ConnectionInfo;

        string dbTemplate = Path.GetFullPath("hazmat_template.s3db");

        [Test]
        public void testHHUploadGarbageCR()
        {
            HHDatabaseManager manager = new HHDatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);


            //insert row into garbage_offload_list 
            int garbage_offload_id = insertGarbageOffloadList();

            Console.Out.WriteLine("Garbage_offload_id: " + garbage_offload_id);

            //insert test data into handheld database           
            int qty_offloaded = 10;
            int expected_qty_offload = 10;
            DateTime action_complete = DateTime.Now;
            string username = "hale";
            string device_type = "device_type";
           
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
            liteCon.Open();

            string insertStmt = "insert into garbage_offload_cr (garbage_offload_id,qty_offloaded,expected_qty_offload,action_complete,username,device_type) " +
                                                        "VALUES (@garbage_offload_id,@qty_offloaded,@expected_qty_offload,@action_complete,@username,@device_type)";


            SQLiteCommand liteCmd = new SQLiteCommand(insertStmt, liteCon);

            liteCmd.Parameters.AddWithValue("@garbage_offload_id", garbage_offload_id);
            liteCmd.Parameters.AddWithValue("@qty_offloaded", qty_offloaded);
            liteCmd.Parameters.AddWithValue("@expected_qty_offload", expected_qty_offload);
            liteCmd.Parameters.AddWithValue("@action_complete", action_complete);
            liteCmd.Parameters.AddWithValue("@username", username);
            liteCmd.Parameters.AddWithValue("@device_type", device_type);
            
            liteCmd.ExecuteNonQuery();

            liteCon.Close();

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();

            //insert change_record into server database from handheld database
            manager.importGarbageOffloadCR(con, tran, tempFile);
            tran.Commit();

            //get data table of garbage_cr from handheld
            DataTable dtHandheld = manager.getHHGarbageCRData(tempFile);

            //get data table of garbage_cr from server
            DataTable dtServer = new DatabaseManager().getServerGarbageCR();

            foreach (DataRow servRow in dtServer.Rows)
            {

                int found_garbage_offload_id = Convert.ToInt32(servRow["garbage_offload_id"]);

                if (found_garbage_offload_id == garbage_offload_id)
                {                    
                    int found_qty_offloaded = Convert.ToInt32(servRow["offload_qty"]);
                    int found_expected_qty_offload = Convert.ToInt32(servRow["expected_offload_qty"]);                            
                    DateTime found_action_complete = Convert.ToDateTime(servRow["action_complete"]);
                    string found_username = servRow["username"].ToString();
                    string found_device_type = servRow["device_type"].ToString();

                    Assert.AreEqual(found_garbage_offload_id, garbage_offload_id);
                    Assert.AreEqual(found_qty_offloaded, qty_offloaded);
                    Assert.AreEqual(found_expected_qty_offload, expected_qty_offload);                   
                    Assert.AreEqual(found_username, username);
                    Assert.AreEqual(found_device_type, device_type);

                    File.Delete(tempFile);

                    break;

                }

            }


            //blow out inserts into cr and offload
            deleteGarbageCR(garbage_offload_id);
            deleteGarbageOffloadList(garbage_offload_id);


        }


        private void deleteGarbageCR(int garbage_offload_id)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            string stmt = "delete from garbage_offload_cr where garbage_offload_id = @garbage";
            cmd.CommandText = stmt;
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@garbage", garbage_offload_id);

            cmd.ExecuteNonQuery();

            con.Close();


        }

        private void deleteGarbageOffloadList(int garbage_offload_id)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            string stmt = "delete from garbage_offload_list where garbage_offload_id = @garbage";
            cmd.CommandText = stmt;
            cmd.Parameters.AddWithValue("@garbage", garbage_offload_id);

            cmd.ExecuteNonQuery();

            con.Close();
        }



        [Test]
        public void testHHUploadOffloadCR()
        {
            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);
                        
            //insert test data into handheld database
            //int offload_list_id = 5000;

            int offload_list_id = insertOffloadList();
             
            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();


           
            int qty_offloaded = 1000;
            DateTime action_complete = DateTime.Now;
            string username = "hale";
            string device_type = "device_type";
            DateTime manufacturer_date = DateTime.Now;
            DateTime shelf_life_expiration_date = DateTime.Now;
            int expected_qty_offload = 10;
            string serial_number = "serial_number";
            Boolean bulk_item = false;

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + tempFile);
            liteCon.Open();

            string insertStmt = "insert into offload_cr (offload_list_id,mfg_catalog_id,qty_offloaded,action_complete,username,device_type,manufacturer_date,shelf_life_expiration_date,expected_qty_offload,serial_number, bulk_item) " +
                               "VALUES (@offload_list_id,@mfg_catalog_id,@qty_offloaded,@action_complete,@username,@device_type,@manufacturer_date,@shelf_life_expiration_date,@expected_qty_offload,@serial_number,@bulk_item)";


            SQLiteCommand liteCmd = new SQLiteCommand(insertStmt, liteCon);

            liteCmd.Parameters.AddWithValue("@offload_list_id", offload_list_id);
            liteCmd.Parameters.AddWithValue("@mfg_catalog_id", mfg_catalog_id);
            liteCmd.Parameters.AddWithValue("@qty_offloaded", qty_offloaded);
            liteCmd.Parameters.AddWithValue("@bulk_item", bulk_item);
            liteCmd.Parameters.AddWithValue("@serial_number", serial_number);
            liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", shelf_life_expiration_date);
            liteCmd.Parameters.AddWithValue("@action_complete", action_complete);
            liteCmd.Parameters.AddWithValue("@username", username);
            liteCmd.Parameters.AddWithValue("@device_type", device_type);
            liteCmd.Parameters.AddWithValue("@manufacturer_date", manufacturer_date);
            liteCmd.Parameters.AddWithValue("@expected_qty_offload", expected_qty_offload);

            liteCmd.ExecuteNonQuery();

            liteCon.Close();

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlTransaction tran = con.BeginTransaction();

            //insert change_record into server database from handheld database
            HHDatabaseManager hhmanager = new HHDatabaseManager();
            hhmanager.importOffloadCR(con, tran, tempFile);
            tran.Commit();

            //get data table of offload_cr from handheld
            DataTable dtHandheld = hhmanager.getHHOffloadCRData(tempFile);

            //get data table of offload_cr from server
            DataTable dtServer = manager.getServerOffloadCR();

            foreach (DataRow servRow in dtServer.Rows)
            {
           
                int found_offload_list_id = Convert.ToInt32(servRow["offload_list_id"]);

                if (found_offload_list_id == 5000)
                {
                    int found_mfg_catalog_id = Convert.ToInt32(servRow["mfg_catalog_id"]);
                    int found_qty_offloaded = Convert.ToInt32(servRow["qty_offloaded"]);
                    int found_expected_qty_offloaded = Convert.ToInt32(servRow["expected_qty_offloaded"]);
                    bool found_bulk_item = Convert.ToBoolean(servRow["bulk_item"]);
                    string found_serial_number = servRow["serial_number"].ToString();
                    DateTime found_shelf_life_expiration_date = Convert.ToDateTime(servRow["shelf_life_expiration_date"]);
                    DateTime found_manufacturer_date = Convert.ToDateTime(servRow["manufacturer_date"]);
                    DateTime found_action_complete = Convert.ToDateTime(servRow["action_complete"]);
                    string found_username = servRow["username"].ToString();
                    string found_device_type = servRow["device_type"].ToString();

                    Assert.AreEqual(found_offload_list_id, offload_list_id);
                    Assert.AreEqual(found_qty_offloaded, qty_offloaded);
                    Assert.AreEqual(found_expected_qty_offloaded, expected_qty_offload);
                    Assert.AreEqual(found_bulk_item, bulk_item);
                    Assert.AreEqual(found_serial_number, serial_number);                   
                    Assert.AreEqual(found_username, username);
                    Assert.AreEqual(found_device_type, device_type);

                    File.Delete(tempFile);

                    break;

                }

            }

            //blow out data
            deleteOffloadCR(offload_list_id);
            deleteOffloadList(offload_list_id);

        }





        private void deleteOffloadCR(int offload_id)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            string stmt = "delete from offload_cr where offload_list_id = @id";
            cmd.CommandText = stmt;
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@id", offload_id);

            cmd.ExecuteNonQuery();

            con.Close();


        }

        private void deleteOffloadList(int offload_id)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            string stmt = "delete from offload_list where offload_list_id = @id";
            cmd.CommandText = stmt;
            cmd.Parameters.AddWithValue("@id", offload_id);

            cmd.ExecuteNonQuery();

            con.Close();
        }

        [Test]
        public void testInsertHHOffloadList()
        {
            HHDatabaseManager manager = new HHDatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = new DatabaseManager().getAllOffloadList();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();

            //insert garbage_items data           
            manager.insertHHOffloadList(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the garbage_offload_list data was inserted
            DataTable dtHandheld = manager.checkHHOffloadOffloadList(tempFile);

            Console.WriteLine("TestOffloadListInsertHHOffloadList server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHOffloadList handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The offload_list row count from the server an the row count from the handheld do not match");
            }

            int count = 0;
            foreach (DataRow row in dtHandheld.Rows)
            {
                int offload_list_id = Convert.ToInt32(row["offload_list_id"].ToString());
                int location_id = Convert.ToInt32(row["location_id"].ToString());
              //  bool bulk_item = Convert.ToBoolean(row["bulk_item"]);
                int mfg_catalog_id = Convert.ToInt32(row["mfg_catalog_id"].ToString());
                int offload_qty = Convert.ToInt32(row["expected_qty_offload"].ToString());

                foreach (DataRow serverRow in dtServer.Rows)
                {
                    int server_offload_list_id = Convert.ToInt32(serverRow["offload_list_id"].ToString());
                    int server_location_id = Convert.ToInt32(serverRow["location_id"].ToString());
                    //bool server_bulk_item = Convert.ToBoolean(serverRow["bulk_item"]);
                    int server_mfg_catalog_id = Convert.ToInt32(serverRow["mfg_catalog_id"].ToString());
                    int server_offload_qty = Convert.ToInt32(serverRow["offload_qty"].ToString());


                    //compare the data
                    if (offload_list_id == server_offload_list_id)
                    {

                        //Console.WriteLine(server_garbage_item_id);
                        count++;
                        if (!location_id.Equals(server_location_id))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The location_id found on the handheld does not match the location_id found on the server where the offload_list_id = " + offload_list_id);

                        }
                        //if (!bulk_item.Equals(server_bulk_item))
                        //{
                        //    File.Delete(tempFile);
                        //    Assert.Fail("The bulk_item found on the handheld does not match the bulk_item found on the server where the offload_list_id = " + offload_list_id);

                        //}
                        if (!mfg_catalog_id.Equals(server_mfg_catalog_id))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The mfg_catalog_id found on the handheld does not match the mfg_catalog_id found on the server where the offload_list_id = " + offload_list_id);

                        }
                        if (!server_offload_qty.Equals(offload_qty))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The offload_qty found on the handheld does not match the offload_qty found on the server where the offload_list_id = " + offload_list_id);

                        }
                                              
                    }

                }

            }

            File.Delete(tempFile);

            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the offload_list_id did not all match. This means some rows are different.");

            }


            Assert.True(true, "offload_list data inserted successfully in sqlite database during testing");


        }

        [Test]
        public void testInsertHHWorkcenters()
        {

            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = manager.getAllWorkcenters();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();

            //insert locations data           
            new HHDatabaseManager().insertHHWorkCentersForOffload(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the Workcenter data was inserted
            DataTable dtHandheld = new HHDatabaseManager().checkHHOffloadWorkcenters(tempFile);

            Console.WriteLine("TestOffloadListInsertHHWorkcenters server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHWorkcenters handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The Workcenter row count from the server an the row count from the handheld do not match");
            }

            int count = 0;
            foreach (DataRow row in dtHandheld.Rows)
            {
                int workcenter_id = Convert.ToInt32(row["workcenter_id"].ToString());
                string description = Convert.ToString(row["description"]);
                string wid = Convert.ToString(row["wid"]);

                foreach (DataRow serverRow in dtServer.Rows)
                {
                    int server_workcenter_id = Convert.ToInt32(serverRow["workcenter_id"].ToString());
                    string server_description = Convert.ToString(serverRow["description"]);
                    string server_wid = Convert.ToString(serverRow["wid"]);

                    //compare the data
                    if (server_workcenter_id == workcenter_id)
                    {
                        count++;
                        if (!description.Equals(server_description))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The description found on the handheld does not match the description found on the server where the workcenter_id = " + workcenter_id);

                        }
                        if (!wid.Equals(server_wid))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The wid found on the handheld does not match the description found on the server where the workcenter_id = " + workcenter_id);

                        }

                    }

                }

            }

            File.Delete(tempFile);

            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the workcenter_id did not all match. This means some rows are different.");

            }


            Assert.True(true, "workcenter data inserted successfully in sqlite database during testing");



        }

        [Test]
        public void testInsertHHLocations()
        {

            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = manager.getAllLocations();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();

            //insert locations data           
            new HHDatabaseManager().insertHHLocationsForOffload(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the locations data was inserted
            DataTable dtHandheld = new HHDatabaseManager().checkHHOffloadGarbageLocations(tempFile);

            Console.WriteLine("TestOffloadListInsertHHGarbagelocations server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHGarbagelocations handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The garbage_offload_list row count from the server an the row count from the handheld do not match");
            }

            int count = 0;
            foreach (DataRow row in dtHandheld.Rows)
            {
                int location_id = Convert.ToInt32(row["location_id"].ToString());
                string name = Convert.ToString(row["name"]);
                int workcenter_id = Convert.ToInt32(row["workcenter_id"]);
               
                foreach (DataRow serverRow in dtServer.Rows)
                {
                    int server_location_id = Convert.ToInt32(serverRow["location_id"].ToString());
                    string server_name = Convert.ToString(serverRow["name"]);
                    int server_workcenter_id = Convert.ToInt32(serverRow["workcenter_id"]);
                   
                    //compare the data
                    if (server_location_id == location_id)
                    {                       
                        count++;
                        if (!server_name.Equals(name))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The description found on the handheld does not match the description found on the server where the location_id = " + location_id);

                        }
                        if (!workcenter_id.Equals(server_workcenter_id))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The quantity found on the handheld does not match the description found on the server where the location_id = " + location_id);

                        }                       

                    }

                }

            }

            File.Delete(tempFile);

            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the location_id did not all match. This means some rows are different.");

            }


            Assert.True(true, "location data inserted successfully in sqlite database during testing");



        }

        //test to make sure the garbage_offload_list get inserted correctly in the handheld database
        [Test]
        public void testInsertHHGarbageOffloadList()
        {
            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = manager.getAllGarbageOffloadList();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();

            //insert garbage_items data           
            new HHDatabaseManager().insertHHGarbageOffloadList(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the garbage_offload_list data was inserted
            DataTable dtHandheld = new HHDatabaseManager().checkHHOffloadGarbageOffloadList(tempFile);

            Console.WriteLine("TestOffloadListInsertHHGarbageOffloadList server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHGarbageOffloadList handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The garbage_offload_list row count from the server an the row count from the handheld do not match");
            }

            int count = 0;
            foreach (DataRow row in dtHandheld.Rows)
            {
                int garbage_offload_id = Convert.ToInt32(row["garbage_offload_id"].ToString());

                int garbage_item_id = 0;
                try
                {
                    garbage_item_id = Convert.ToInt32(row["garbage_item_id"]);
                }
                catch (Exception ex)
                {

                }
                int qty = Convert.ToInt32(row["expected_qty_offload"]);
                //int archived_id = Convert.ToInt32(row["archived_id"]);         

                foreach (DataRow serverRow in dtServer.Rows)
                {
                    int server_garbage_offload_id = Convert.ToInt32(serverRow["garbage_offload_id"].ToString());
                    int server_garbage_item_id = 0;
                    try{
                     server_garbage_item_id = Convert.ToInt32(serverRow["garbage_item_id"]);
                    }catch(Exception ex){

                    }
                    int server_qty = Convert.ToInt32(serverRow["qty"]);
                    //int server_archived_id = Convert.ToInt32(serverRow["archived_id"]);


                    //compare the data
                    if (server_garbage_offload_id == garbage_offload_id)
                    {

                        //Console.WriteLine(server_garbage_item_id);
                        count++;                       
                        //if (!server_garbage_item_id.Equals(garbage_item_id))
                        //{
                        //    File.Delete(tempFile);
                        //    Assert.Fail("The description found on the handheld does not match the description found on the server where the garbage_offload_id = " + garbage_item_id);

                        //}
                        if (!server_qty.Equals(qty))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The quantity found on the handheld does not match the description found on the server where the garbage_offload_id = " + garbage_item_id);

                        }
                        //if (!server_archived_id.Equals(archived_id))
                        //{
                        //    File.Delete(tempFile);
                        //    Assert.Fail("The archived_id found on the handheld does not match the description found on the server where the garbage_offload_id = " + garbage_item_id);

                        //}

                    }

                }

            }

            File.Delete(tempFile);

            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the garbage_item_id did not all match. This means some rows are different.");

            }


            Assert.True(true, "garbage_offload_list data inserted successfully in sqlite database during testing");


        }

        //tes to make sure garbage_items get inserted correctly in the handheld database
        [Test]
        public void testInsertHHGarbageItems()
        {

            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = manager.getAllGarbageItems();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert garbage_items data           
            new HHDatabaseManager().insertHHGarbageItems(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the garbage_items data was inserted
            DataTable dtHandheld = new HHDatabaseManager().checkHHOffloadGarbageItems(tempFile);

            Console.WriteLine("TestOffloadListInsertHHGarbageItems server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHGarbageItems handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The garbage_item row count from the server an the row count from the handheld do not match");
            }

            int count = 0;
            foreach (DataRow row in dtHandheld.Rows)
            {
                int garbage_item_id = Convert.ToInt32(row["garbage_item_id"].ToString());
                string description = Convert.ToString(row["description"]);

                //Console.WriteLine(garbage_item_id);
               

                foreach (DataRow serverRow in dtServer.Rows)
                {
                    int server_garbage_item_id = Convert.ToInt32(serverRow["garbage_item_id"].ToString());
                    string server_description = Convert.ToString(serverRow["description"]);
                   

                    //compare the data
                    if (server_garbage_item_id == garbage_item_id)
                    {

                        //Console.WriteLine(server_garbage_item_id);
                        count++;
                        Console.WriteLine(count);
                        if (!description.Equals(server_description))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The description found on the handheld does not match the description found on the server where the garbage_item_id = " + garbage_item_id);

                        }                                              

                    }

                }

            }

            File.Delete(tempFile);

            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the garbage_item_id did not all match. This means some rows are different.");

            }


            Assert.True(true, "garbage_item data inserted successfully in sqlite database during testing");



        }

        //test to make sure niin_catalog gets inserted correctly in the handheld database
        [Test]
        public void testInsertHHNiinCatalog()
        {

            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = manager.getAllNiinCatalog();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert niin_catalog data           
            new HHDatabaseManager().insertHHNiinCatalogForOffload(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the niin_catalog data was inserted
            DataTable dtHandheld = new HHDatabaseManager().checkHHOffloadNIINCatalog(tempFile);

            Console.WriteLine("TestOffloadListInsertHHNiinCatalog server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHNiinCatalog handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The niin row count from the server an the row count from the handheld do not match");
            }
            int count = 0;
            foreach (DataRow row in dtHandheld.Rows)
            {
                int niin_catalog_id = Convert.ToInt32(row["niin_catalog_id"].ToString());
                int fsc = Convert.ToInt32(row["fsc"]);
                string niin = Convert.ToString(row["niin"]);
                string ui = Convert.ToString(row["ui"]);
                string um = Convert.ToString(row["um"]);
                int usage_category_id = Convert.ToInt32(row["usage_category_id"]);
                string description = Convert.ToString(row["description"]);
                int smcc_id = Convert.ToInt32(row["smcc_id"]);
                string specs = Convert.ToString(row["specs"]);
                int shelf_life_code_id = Convert.ToInt32(row["shelf_life_code_id"]);
                int shelf_life_action_code_id = Convert.ToInt32(row["shelf_life_action_code_id"]);
                string remarks = Convert.ToString(row["remarks"]);
                int storage_type_id = Convert.ToInt32(row["storage_type_id"]);
                int cog_id = Convert.ToInt32(row["cog_id"]);
                string spmig = row["spmig"].ToString();
                string nehc_rpt = row["nehc_rpt"].ToString();
                int catalog_group_id = Convert.ToInt32(row["catalog_group_id"]);
                string catalog_serial_number = row["catalog_serial_number"].ToString();
                int allowance_qty = Convert.ToInt32(row["allowance_qty"]);                


                // int hazard_id = Int32.Parse(row["hazard_id"].ToString());

                foreach (DataRow serverRow in dtServer.Rows)
                {
                    int server_niin_catalog_id = Convert.ToInt32(serverRow["niin_catalog_id"].ToString());
                    int server_fsc = Convert.ToInt32(serverRow["fsc"]);
                    string server_niin = Convert.ToString(serverRow["niin"]);
                    string server_ui = Convert.ToString(serverRow["ui"]);
                    string server_um = Convert.ToString(serverRow["um"]);
                    int server_usage_category_id = Convert.ToInt32(serverRow["usage_category_id"]);
                    string server_description = Convert.ToString(serverRow["description"]);
                    int server_smcc_id = Convert.ToInt32(serverRow["smcc_id"]);
                    string server_specs = Convert.ToString(serverRow["specs"]);
                    int server_shelf_life_code_id = Convert.ToInt32(serverRow["shelf_life_code_id"]);
                    int server_shelf_life_action_code_id = Convert.ToInt32(serverRow["shelf_life_action_code_id"]);
                    string server_remarks = Convert.ToString(serverRow["remarks"]);
                    int server_storage_type_id = Convert.ToInt32(serverRow["storage_type_id"]);
                    int server_cog_id = Convert.ToInt32(serverRow["cog_id"]);
                    string server_spmig = serverRow["spmig"].ToString();
                    string server_nehc_rpt = serverRow["nehc_rpt"].ToString();
                    int server_catalog_group_id = Convert.ToInt32(serverRow["catalog_group_id"]);
                    string server_catalog_serial_number = serverRow["catalog_serial_number"].ToString();
                    int server_allowance_qty = Convert.ToInt32(serverRow["allowance_qty"]);                

                    //compare the data
                    if (niin_catalog_id == server_niin_catalog_id)
                    {
                        count++;

                        if (!niin.Equals(server_niin))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The niin found on the handheld does not match the niin found on the server where the niin_catalog_id = " + niin_catalog_id);

                        }

                        else if (!fsc.Equals(server_fsc))
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The fsc found on the handheld does not match the fsc found on the server where the niin_catalog_id = " + niin_catalog_id);

                        }

                        else if (ui != server_ui)
                        {
                            File.Delete(tempFile);
                            Assert.Fail("The ui found on the handheld does not match the ui found on the server where the niin_catalog_id = " + niin_catalog_id);

                        }

                       
                    }

                }

            }

            File.Delete(tempFile);

            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the niin_catalog_ids did not all match. This means some rows are different.");

            }

            
            Assert.True(true, "niin_catalog data inserted successfully in sqlite database during testing");


        }

        private int insertGarbageOffloadList()
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            string stmt = "insert into garbage_offload_list (qty, ui) VALUES (@qty, @ui); SELECT SCOPE_IDENTITY()";
            cmd.CommandText = stmt;
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@qty",1000);
            cmd.Parameters.AddWithValue("@ui","unithale");

            int id = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return id;

        }


        private int getLocation()
        {

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        private int insertOffloadList()
        {
            DatabaseManager manager = new DatabaseManager();
            //Grab a mfg/niin to use in the test.
            DataTable mfg = manager.getMFGCatalogCollection("", "");
            int mfg_catalog_id = Int32.Parse(mfg.Rows[0]["mfg_catalog_id"].ToString());
            int niin_catalog_id = Int32.Parse(mfg.Rows[0]["niin_catalog_id"].ToString());
            int location_id = this.getLocation();


            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            string stmt = "insert into offload_list (location_id,serial_number,mfg_catalog_id,offload_qty) VALUES (@location_id,@serial_number,@mfg_catalog_id,@offload_qty); SELECT SCOPE_IDENTITY()";
            cmd.CommandText = stmt;
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@location_id", location_id);
            cmd.Parameters.AddWithValue("@serial_number", "UnitTestAdam");
            cmd.Parameters.AddWithValue("@mfg_catalog_id", mfg_catalog_id);
            cmd.Parameters.AddWithValue("@offload_qty", 1000);
            

            int id = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return id;

        }



        //test to make sure mfg_catalog gets inserted correctly to the handheld
        [Test]
        public void testInsertHHManufacturerCatalog()
        {          

            DatabaseManager manager = new DatabaseManager();

            //create tempFile to copy the template to - 
            string tempFile = System.IO.Path.GetTempFileName();
            //copy template
            File.Copy(dbTemplate, tempFile, true);

            //get data to be inserted into the handheld. This will be used to check the count as well as some data in the handheld database.
            DataTable dtServer = manager.getAllMfgCatalog();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + tempFile);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert mfgCatalog data
           // manager.insertHHOffloadMFGCatalog(tempFile);
            new HHDatabaseManager().insertHHMfgCatalogForOffload(tempFile, tran, liteCon);
            tran.Commit();
            liteCon.Close();

            //check the database to ensure the mfg_catalog data was inserted
            DataTable dtHandheld = new HHDatabaseManager().checkHHOffloadMFGCatalog(tempFile);
            
            Console.WriteLine("TestOffloadListInsertHHManufacturerCatalog server row count: " + dtServer.Rows.Count);
            Console.WriteLine("TestOffloadListInsertHHManufacturerCatalog handheld row count: " + dtHandheld.Rows.Count);

            //make sure both DataTables have the same number of rows
            if (dtHandheld.Rows.Count != dtServer.Rows.Count)
            {
                File.Delete(tempFile);
                Assert.Fail("The manufacturer row count from the server an the row count from the handheld do not match");
            }

            int count = 0;

            foreach (DataRow row in dtHandheld.Rows)
            {
                int mfg_catalog_id = Int32.Parse(row["mfg_catalog_id"].ToString());
                string cage = Convert.ToString(row["cage"].ToString());
               // Console.WriteLine("handheld cage: " + cage);
                string manufacturer = Convert.ToString(row["manufacturer"].ToString());
                int niin_catalog_id = Int32.Parse(row["niin_catalog_id"].ToString());
               // int hazard_id = Int32.Parse(row["hazard_id"].ToString());

                foreach (DataRow serverRow in dtServer.Rows)
                {
                     int mfg_id = Int32.Parse(serverRow["mfg_catalog_id"].ToString());
                     string server_cage = Convert.ToString(serverRow["cage"].ToString());
                    // Console.WriteLine("server cage: " + server_cage);
                     string server_manufacturer = Convert.ToString(serverRow["manufacturer"].ToString());
                     int server_niin_catalog_id = Int32.Parse(serverRow["niin_catalog_id"].ToString());
                    // int server_hazard_id = Int32.Parse(row["hazard_id"].ToString());

                    //compare the data
                     if (mfg_id == mfg_catalog_id)
                     {
                         count++;                         
                         if (!cage.ToLower().Equals(server_cage.ToLower()))
                         {
                             File.Delete(tempFile);
                             Assert.Fail("The cage found on the handheld does not match the cage found on the server where the mfg_catalog_id = " + mfg_catalog_id);

                         }

                         else if (!manufacturer.Equals(server_manufacturer))
                         {
                             File.Delete(tempFile);
                             Assert.Fail("The manufacturer found on the handheld does not match the manufacturer found on the server where the mfg_catalog_id = " + mfg_catalog_id);

                         }

                         else if (niin_catalog_id != server_niin_catalog_id)
                         {
                             File.Delete(tempFile);
                             Assert.Fail("The niin_catalog_id found on the handheld does not match the niin_catalog_id found on the server where the mfg_catalog_id = " + mfg_catalog_id);

                         }

                         //else if (hazard_id != server_hazard_id)
                         //{
                         //    Assert.Fail("The hazard_id found on the handheld does not match the hazard_id found on the server where the mfg_catalog_id = " + mfg_catalog_id);

                         //}

                     }

                }

            }


            if (count != dtServer.Rows.Count || count != dtHandheld.Rows.Count)
            {
                Assert.Fail("Although the datatable for the server and the handheld returned the same number of rows, the rows based on the mfg_catalog_ids did not all match. This means some rows are different.");

            }

            File.Delete(tempFile);
            Assert.True(true,"mfg_catalog data inserted successfully in sqlite database during testing");

        }
           
     

    }
}
