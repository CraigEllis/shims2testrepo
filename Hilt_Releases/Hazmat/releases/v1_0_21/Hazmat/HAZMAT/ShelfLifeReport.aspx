﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShelfLifeReport.aspx.cs" Inherits="HAZMAT.ShelfLifeReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="resources/css/smoothness/jquery-ui-1.8.17.custom.css"/>
    <script src="resources/js/jquery-ui-1.8.17.custom.min.js"></script>
    <script>
        $(document).ready(function ()
        {
            $(".monthYearDatePicker").datepicker({
			    changeMonth: true,
			    changeYear: true
		    });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br /><br />
<table style="width: 430px"><tr><td align="left" class="style1">
    <asp:Image ID="Image1" runat="server" ImageUrl="images/hazard_icon.png"  /></td><td>
    <asp:Label  ID="Label1" runat="server" Text="Reports"
     SkinID="LabelPageHeader"></asp:Label></td>
      <td align="right">
                <asp:ImageButton ID="btnFeedback" runat="server" ImageUrl="images/feedback.gif" 
                    ToolTip="Click to provide feedback" onclick="btnFeedback_Click" />
                    <asp:ImageButton ID="btnHelp" runat="server" ImageUrl="images/help.gif" 
                    ToolTip="Click to access manuals and training material" onclick="btnHelp_Click" />
            </td>
     </tr></table>

     <asp:Button runat="server" ID="btnExcelReport" OnClick="btnExcelReport_Click" Text="Download Dashboard Excel Reports" /> 
        <br /><br />
                
    Choose Report:<asp:DropDownList ID="DropDownListReport" runat="server">
        
    </asp:DropDownList>
    <asp:Button runat="server" ID="btnChooseReport" OnClick="btnChooseReport_Click" Text="Submit" />
    <br /><br />
    <table class="filterTable">
        
        <tr>
            <td>
                Start Date:
            </td>
            <td>
                <asp:TextBox class="monthYearDatePicker" ID="txtStartDate" runat="server" />
                <asp:CompareValidator id="CompareValidator1" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtStartDate" ErrorMessage="Please enter a valid date." /> 
            </td>
        </tr>   
        <tr>
            <td>
                To Date:
            </td>
            <td>
                <asp:TextBox class="monthYearDatePicker" ID="txtToDate" runat="server" />
                <asp:CompareValidator id="dateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtToDate" ErrorMessage="Please enter a valid date." /> 
            </td>
        </tr>       
        <tr>
            <td valign="top" style="padding-top:4px;">
    Location:</td><td  valign="top"  style="padding-top:4px;">
    <asp:CheckBox ID="chkbox_location" runat="server" />
    <asp:TextBox ID="txtLocation" runat="server" />
    </td><td  valign="top">
     
    
    </td>
        </tr>
        <tr>
            <td>
                NIIN:
            </td>
            <td>
                <asp:CheckBox ID="chkbox_Niin" runat="server" />
                <asp:TextBox ID="txtNiin" runat="server" />                
            </td>
        </tr>      
        <tr>
            <td>
                Workcenter:
            </td>
            <td>
                <asp:CheckBox ID="chkbox_workcenter" runat="server" />
                <asp:DropDownList ID="DropDownListWorkcenter" runat="server" Width="374px">
                </asp:DropDownList>
                <asp:Button ID="btnLoadReport" runat="server" OnClick="btnLoadReport_Click" Text="Submit" />
            </td>
        </tr>      
    </table>
    <br />
   
    <table width="75%">
        <tr>
            <td style="width: 50%">
            <div class="tableHeaderDivReport" style="width:95%;">
    <asp:Label CssClass="floatLeft" ID="Label2" runat="server" SkinID="LabelTableHeaderReport" Text="Shelf Life Report" 
               Font-Bold="True"></asp:Label>               
                <asp:Button CssClass="floatRight" ID="btnDownloadExcel" runat="server" 
                    Text="Download Shelf Life Excel Report" onclick="btnDownloadExcel_Click" />
               <br />               
               <br />               
                <asp:GridView CssClass="gridWidth" DataKeyNames=""
                    SkinID="GridViewReport" ID="shelfLifeGridView" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" OnPageIndexChanging="shelfLifeGridView_PageIndexChanging"
                    ShowFooter="True" EmptyDataText=" No data to display">
                    <Columns>
                        <asp:BoundField Visible="false" DataField="inventory_id" HeaderText="inventory_id" SortExpression="inventory_id" />
                        <asp:BoundField DataField="atm" HeaderText="ATM" />
                        <asp:BoundField DataField="NIIN" HeaderText="NIIN" />
                        <asp:BoundField DataField="MANUFACTURER" HeaderText="MANUFACTURER" /> 
                        <asp:BoundField DataField="description" HeaderText="Item Name" />
                        <asp:BoundField DataField="shelf_life_expiration_date" HeaderText="Shelf Life Expiration Date" />
                        <asp:BoundField DataField="cage" HeaderText="CAGE" />                          
                        <asp:BoundField DataField="qty" HeaderText="Qty" />                      
                        <asp:BoundField DataField="location_name" HeaderText="Location" />
                        <asp:BoundField DataField="Workcenter" HeaderText="Workcenter" />                        
                    </Columns>
                    <SelectedRowStyle BackColor="#003399" ForeColor="White" />
                </asp:GridView>
                </div>
            </td>
            
        </tr>
    </table>
    <br />
</asp:Content>
