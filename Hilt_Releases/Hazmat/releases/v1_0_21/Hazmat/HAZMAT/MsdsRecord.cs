﻿using System.Collections.Generic;
using System;

namespace HAZMAT
{
    public class MsdsRecord
    {

        public MsdsRecord()
        {

            ingredientsList = new List<MsdsIngredients>();
            contractorsList = new List<MsdsContractors>();

        }


        //MSDS TABLE INFORMATION.
        //***********************
        #region MSDS TABLE
        //ARTICLE INDEX.
       

        public string ARTICLE_IND
        {
            get;
            set;
        }
        //RESPONSIBLE PARTY CAGE.
        

        public string CAGE
        {
            get;
            set;
        }
        //RESPONSIBLE PARTY COMPANY NAME
        

        public string MANUFACTURER
        {
            get;
            set;
        }
        //DESCRIPTION
      

        public string DESCRIPTION
        {
            get;
            set;
        }
        //EMERGENCY TELEPHONE RESPONSIBLE PARTY
       
        public string EMERGENCY_TEL
        {
            get;
            set;
        }
        //END ITEM COMPONENT INDICATOR.
       

        public string END_COMP_IND
        {
            get;
            set;
        }
        //END ITEM INDICATOR.
       

        public string END_ITEM_IND
        {
            get;
            set;
        }
        //FSC
       

        public string FSC
        {
            get;
            set;
        }
        //KIT INDICATOR.
       

        public string KIT_IND
        {
            get;
            set;
        }
        //KIT PART INDICATOR.
        

        public string KIT_PART_IND
        {
            get;
            set;
        }
        //MANUFACTURER MSDS NUMBER
       

        public string MANUFACTURER_MSDS_NO
        {
            get;
            set;
        }
        //MIXTURE INDICATOR.
       

        public string MIXTURE_IND
        {
            get;
            set;
        }
        //NIIN
       

        public string NIIN
        {
            get;
            set;
        }
        //PART NUMBER
       

        public string PARTNO
        {
            get;
            set;
        }
        //PRODUCT IDENTITY
       

        public string PRODUCT_IDENTITY
        {
            get;
            set;
        }
        //PRODUCT INDICATOR.
        

        public string PRODUCT_IND
        {
            get;
            set;
        }
        //PRODUCT LANGUAGE
       

        public string PRODUCT_LANGUAGE
        {
            get;
            set;
        }
        //PRODUCT LOAD DATE.
        

        public string PRODUCT_LOAD_DATE
        {
            get;
            set;
        }
        //PRODUCT RECORD STATUS.
       

        public string PRODUCT_RECORD_STATUS
        {
            get;
            set;
        }
        //PRODUCT REVISION NUMBER.
        

        public string PRODUCT_REVISION_NO
        {
            get;
            set;
        }
        //PRODUCT SERIAL NUMBER
      

        public string MSDSSERNO
        {
            get;
            set;
        }
        //PUBLISHED INDICATOR
       

        public string PROPRIETARY_IND
        {
            get;
            set;
        }

        public string PUBLISHED_IND
        {
            get;
            set;
        }
        //PURCHASED PRODUCT INDICATOR
       
        public string PURCHASED_PROD_IND
        {
            get;
            set;
        }
        //PURE INDICATOR
        

        public string PURE_IND
        {
            get;
            set;
        }
        //RADIOACTIVE INDICATOR
       

        public string RADIOACTIVE_IND
        {
            get;
            set;
        }
        //SERVICE AGENCY
       

        public string SERVICE_AGENCY
        {
            get;
            set;
        }
        //TRADE NAME
       

        public string TRADE_NAME
        {
            get;
            set;
        }
        //TRADE SECRET INDICATOR
        

        public string TRADE_SECRET_IND
        {
            get;
            set;
        }
        //MSDS FILE PATH
       

        public string MSDSFILEPATH
        {
            get;
            set;
        }

        public DateTime CreatedDate {
            get;
            set;
        }

        public String CreatedString {
            get;
            set;
        }
        #endregion

        //MSDS PHYS CHEMICAL TABLE
        #region MSDS PHYS CHEM TABLE


        //OSHA_WATER_REACTIVE
        public string OSHA_WATER_REACTIVE
        {
            get;
            set;
        }

        //PERCENT VOLATILES BY VOLUME
       

        public string PERCENT_VOL_VOLUME
        {
            get;
            set;
        }
        //APPEARANCE ODOR TEXT
       

        public string APP_ODOR
        {
            get;
            set;
        }
        //AUTOIGNITION TEMP. (C)
       

        public string AUTOIGNITION_TEMP
        {
            get;
            set;
        }
        //CARCINOGEN INDICATOR
      

        public string CARCINOGEN_IND
        {
            get;
            set;
        }
        //EPA ACUTE
      

        public string EPA_ACUTE
        {
            get;
            set;
        }
        //EPA CHRONIC
      

        public string EPA_CHRONIC
        {
            get;
            set;
        }
        //EPA FIRE
       

        public string EPA_FIRE
        {
            get;
            set;
        }
        //EPA PRESSURE
       

        public string EPA_PRESSURE
        {
            get;
            set;
        }
        //EPA REACTIVITY
        

        public string EPA_REACTIVITY
        {
            get;
            set;
        }
        //EVAPORATION RATE
        

        public string EVAP_RATE_REF
        {
            get;
            set;
        }
        //FLASH POint TEMP (C)
       

        public string FLASH_PT_TEMP
        {
            get;
            set;
        }
        //NEUTRALIZING AGENT TEXT
       

        public string NEUT_AGENT
        {
            get;
            set;
        }
        //NFPA FLAMMABILITY
        

        public string NFPA_FLAMMABILITY
        {
            get;
            set;
        }
        //NFPA HEALTH
        

        public string NFPA_HEALTH
        {
            get;
            set;
        }
        //NFPA REACTIVITY
       

        public string NFPA_REACTIVITY
        {
            get;
            set;
        }
        //NFPA SPECIAL
        

        public string NFPA_SPECIAL
        {
            get;
            set;
        }
        //OSHA CARCINOGENS
        

        public string OSHA_CARCINOGENS
        {
            get;
            set;
        }
        //OSHA COMBUSTION LIQUID
       

        public string OSHA_COMB_LIQUID
        {
            get;
            set;
        }
        //OSHA COMPRESSED GAS
       
        public string OSHA_COMP_GAS
        {
            get;
            set;
        }
        //OSHA CORROSIVE
      

        public string OSHA_CORROSIVE
        {
            get;
            set;
        }
        //OSHA EXPLOSIVE
       

        public string OSHA_EXPLOSIVE
        {
            get;
            set;
        }
        //OSHA FLAMMABLE
       

        public string OSHA_FLAMMABLE
        {
            get;
            set;
        }
        //OSHA HIGHLY TOXIC
       

        public string OSHA_HIGH_TOXIC
        {
            get;
            set;
        }
        //OSHA IRRITANT
      

        public string OSHA_IRRITANT
        {
            get;
            set;
        }
        //OSHA ORGANIC PEROXIDE
       

        public string OSHA_ORG_PEROX
        {
            get;
            set;
        }
        //OSHA OTHER/LONG TERM
       

        public string OSHA_OTHERLONGTERM
        {
            get;
            set;
        }
        //OSHA OXIDIZER
        

        public string OSHA_OXIDIZER
        {
            get;
            set;
        }
        //OSHA PYROPHORIC
      

        public string OSHA_PYRO
        {
            get;
            set;
        }
        //OSHA SENSITIZER
      

        public string OSHA_SENSITIZER
        {
            get;
            set;
        }
        //OSHA TOXIC
      

        public string OSHA_TOXIC
        {
            get;
            set;
        }
        //OSHA UNSTABLE REACTIVE
       

        public string OSHA_UNST_REACT
        {
            get;
            set;
        }

        //OTHER/SHORT TERM
     

        public string OTHER_SHORT_TERM
        {
            get;
            set;
        }
        //PH
       

        public string PH
        {
            get;
            set;
        }
        //PHYSICAL STATE CODE
       

        public string PHYS_STATE_CODE
        {
            get;
            set;
        }
        //SOLUBILITY IN WATER
       

        public string SOL_IN_WATER
        {
            get;
            set;
        }
        //SPECIFIC GRAVITY
        

        public string SPECIFIC_GRAV
        {
            get;
            set;
        }
        //VAPOR DENSITY
       

        public string VAPOR_DENS
        {
            get;
            set;
        }
        //VAPOR PRESSURE
      

        public string VAPOR_PRESS
        {
            get;
            set;
        }
        //VISCOSITY
      

        public string VISCOSITY
        {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (GM/L)
      

        public string VOC_GRAMS_LITER
        {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (LB/G)
      

        public string VOC_POUNDS_GALLON
        {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (WT%)
      

        public string VOL_ORG_COMP_WT
        {
            get;
            set;
        }
        #endregion

        //MSDS INGREDIENTS
        #region MSDS INGREDIENTS
        public List<MsdsIngredients> ingredientsList { get; set; }
        #endregion

        //MSDS CONTRACTOR INFO
        #region MSDS CONTRACTOR INFO
        public List<MsdsContractors> contractorsList { get; set; }
        #endregion

        //MSDS RADIOLOGICAL INFO
        #region MSDS RADIOLOGICAL INFO
        //NRC LICENSE/PERMIT NUMBER
        

        public string NRC_LP_NUM
        {
            get;
            set;
        }
        //OPERATOR
       

        public string OPERATOR
        {
            get;
            set;
        }
        //RADIOACTIVE AMOUNT (MICROCURIES)
     

        public string RAD_AMOUNT_MICRO
        {
            get;
            set;
        }
        //RADIOACTIVE FORM
     

        public string RAD_FORM
        {
            get;
            set;
        }
        //RADIOISOTOPE CAS
      

        public string RAD_CAS
        {
            get;
            set;
        }
        //RADIOISOTOPE NAME
    

        public string RAD_NAME
        {
            get;
            set;
        }
        //RADIOISOTOPE SYMBOL
      

        public string RAD_SYMBOL
        {
            get;
            set;
        }
        //REPLACEMENT NSN
      

        public string REP_NSN
        {
            get;
            set;
        }
        //SEALED
     

        public string SEALED
        {
            get;
            set;
        }
        #endregion

        //MSDS TRANSPORTATION
        #region MSDS TRANSPORTATION
        //AF MMAC CODE
       

        public string AF_MMAC_CODE
        {
            get;
            set;
        }
        //CERTIFICATE OF EQUIVALENCY
       

        public string CERTIFICATE_COE
        {
            get;
            set;
        }
        //COMPETENT AUTHORITY APPROVAL
     

        public string COMPETENT_CAA
        {
            get;
            set;
        }
        //DOD ID CODE
       

        public string DOD_ID_CODE
        {
            get;
            set;
        }
        //DOT EXEMPTION NUMBER
      

        public string DOT_EXEMPTION_NO
        {
            get;
            set;
        }
        //DOT RQ INDICATOR
      

        public string DOT_RQ_IND
        {
            get;
            set;
        }
        //EX NUMBER
     
        public string EX_NO
        {
            get;
            set;
        }
        //TRAN FLASH PT TEMP (C)


        public string TRAN_FLASH_PT_TEMP {
            get;
            set;
        }
        //HCC
    

        public string HCC
        {
            get;
            set;
        }

        //HIGH EXPLOSIVE WEIGHT
      

        public int HIGH_EXPLOSIVE_WT
        {
            get;
            set;
        }
        //LTD QTY INDICATOR
      

        public string LTD_QTY_IND
        {
            get;
            set;
        }
        //MAGNETIC INDICATOR
       

        public string MAGNETIC_IND
        {
            get;
            set;
        }
        //MAGNETISM (MAGNETIC STRENGTH)
       
        public string MAGNETISM
        {
            get;
            set;
        }
        //MARINE POLLUTANT INDICATOR
       

        public string MARINE_POLLUTANT_IND
        {
            get;
            set;
        }
        //NET EXPLOSIVE QTY. DISTANCE WEIGHT
       

        public int NET_EXP_QTY_DIST
        {
            get;
            set;
        }
        //NET EXPLOSIVE WEIGHT (KG)
       
        public string NET_EXP_WEIGHT
        {
            get;
            set;
        }
        //NET PROPELLANT WEIGHT (KG)
       

        public string NET_PROPELLANT_WT
        {
            get;
            set;
        }
        //NOS TECHNICAL SHIPPING NAME
        

        public string NOS_TECHNICAL_SHIPPING_NAME
        {
            get;
            set;
        }
        //TRANSPORTATION ADDITIONAL DATA
       

        public string TRANSPORTATION_ADDITIONAL_DATA
        {
            get;
            set;
        }
        #endregion

        //MSDS DOT PSN
        #region MSDS DOT PSN
        //DOT HAZARD CLASS/DIV
        
        public string DOT_HAZARD_CLASS_DIV
        {
            get;
            set;
        }
        //DOT HAZARD LABEL
        

        public string DOT_HAZARD_LABEL
        {
            get;
            set;
        }
        //DOT MAX QUANTITY: CARGO AIRCRAFT ONLY
       
        public string DOT_MAX_CARGO
        {
            get;
            set;
        }
        //DOT MAX QUANTITY: PASSENGER AIRCRAFT/RAIL
       

        public string DOT_MAX_PASSENGER
        {
            get;
            set;
        }
        //DOT PACKAGING BULK
       

        public string DOT_PACK_BULK
        {
            get;
            set;
        }
        //DOT PACKAGING EXCEPTIONS
       

        public string DOT_PACK_EXCEPTIONS
        {
            get;
            set;
        }
        //DOT PACKAGING NON BULK
       

        public string DOT_PACK_NONBULK
        {
            get;
            set;
        }
        //DOT PACKING GROUP
     

        public string DOT_PACK_GROUP
        {
            get;
            set;
        }
        //DOT PROPER SHIPPING NAME
       

        public string DOT_PROP_SHIP_NAME
        {
            get;
            set;
        }
        //DOT PROPER SHIPPING NAME MODIFIER
       

        public string DOT_PROP_SHIP_MODIFIER
        {
            get;
            set;
        }
        //DOT PSN CODE
       

        public string DOT_PSN_CODE
        {
            get;
            set;
        }
        //DOT SPECIAL PROVISION
      

        public string DOT_SPECIAL_PROVISION
        {
            get;
            set;
        }
        //DOT SYMBOLS
      

        public string DOT_SYMBOLS
        {
            get;
            set;
        }
        //DOT UN ID NUMBER
      

        public string DOT_UN_ID_NUMBER
        {
            get;
            set;
        }
        //DOT WATER SHIPMENT OTHER REQS
      

        public string DOT_WATER_OTHER_REQ
        {
            get;
            set;
        }
        //DOT WATER SHIPMENT VESSEL STOWAGE
      

        public string DOT_WATER_VESSEL_STOW
        {
            get;
            set;
        }
        #endregion

        //MSDS AFJM PSN
        #region MSDS AFJM PSN
        //AFJM HAZARD CLASS/DIV
       

        public string AFJM_HAZARD_CLASS
        {
            get;
            set;
        }
        //AFJM PACKAGING PARAGRAPH
        

        public string AFJM_PACK_PARAGRAPH
        {
            get;
            set;
        }
        //AFJM PACKING GROUP
       

        public string AFJM_PACK_GROUP
        {
            get;
            set;
        }
        //AFJM PROPER SHIPPING NAME
       

        public string AFJM_PROP_SHIP_NAME
        {
            get;
            set;
        }
        //AFJM PROPER SHIPPING NAME MODIFIER
      

        public string AFJM_PROP_SHIP_MODIFIER
        {
            get;
            set;
        }
        //AFJM PSN CODE
     

        public string AFJM_PSN_CODE
        {
            get;
            set;
        }
        //AFJM SPECIAL PROVISIONS
      

        public string AFJM_SPECIAL_PROV
        {
            get;
            set;
        }
        //AFJM SUBSIDIARY RISK
     

        public string AFJM_SUBSIDIARY_RISK
        {
            get;
            set;
        }
        //AFJM SYMBOLS
   

        public string AFJM_SYMBOLS
        {
            get;
            set;
        }
        //AFJM UN ID NUMBER
    

        public string AFJM_UN_ID_NUMBER
        {
            get;
            set;
        }
        #endregion

        //MSDS IATA PSN
        #region MSDS IATA PSN
        //IATA CARGO PACKING: NOTE CARGO AIRCRAFT PACKING INSTRUCTIONS
       

        public string IATA_CARGO_PACKING
        {
            get;
            set;
        }
        //IATA HAZARD CLASS/DIV
       

        public string IATA_HAZARD_CLASS
        {
            get;
            set;
        }
        //IATA HAZARD LABEL
      

        public string IATA_HAZARD_LABEL
        {
            get;
            set;
        }
        //IATA PACKING GROUP
      

        public string IATA_PACK_GROUP
        {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: LMT QTY PKG INSTR
      

        public string IATA_PASS_AIR_PACK_LMT_INSTR
        {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: LMT QTY MAX QTY PER PKG
     

        public string IATA_PASS_AIR_PACK_LMT_PER_PKG
        {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING MAX QUANTITY
       

        public string IATA_PASS_AIR_MAX_QTY
        {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: NOTE PASSENGER AIR PACKING INSTR
       

        public string IATA_PASS_AIR_PACK_NOTE
        {
            get;
            set;
        }
        //IATA PROPER SHIPPING NAME
       

        public string IATA_PROP_SHIP_NAME
        {
            get;
            set;
        }
        //IATA PROPER SHIPPING NAME MODIFIER
       

        public string IATA_PROP_SHIP_MODIFIER
        {
            get;
            set;
        }
        //IATA CARGO PACKING: MAX QUANTITY
       

        public string IATA_CARGO_PACK_MAX_QTY
        {
            get;
            set;
        }
        //IATA PSN CODE
      

        public string IATA_PSN_CODE
        {
            get;
            set;
        }
        //IATA SPECIAL PROVISIONS
      

        public string IATA_SPECIAL_PROV
        {
            get;
            set;
        }
        //IATA SUBSIDIARY RISK
      

        public string IATA_SUBSIDIARY_RISK
        {
            get;
            set;
        }
        //IATA UN ID NUMBER
      

        public string IATA_UN_ID_NUMBER
        {
            get;
            set;
        }
        #endregion

        //MSDS IMO PSN
        #region MSDS IMO PSN
        //IMO EMS NUMBER
      

        public string IMO_EMS_NO
        {
            get;
            set;
        }
        //IMO HAZARD CLASS/DIV
      

        public string IMO_HAZARD_CLASS
        {
            get;
            set;
        }
        //IMO IBC INSTRUCTIONS
    

        public string IMO_IBC_INSTR
        {
            get;
            set;
        }
        //IMO IBC PROVISIONS
      

        public string IMO_IBC_PROVISIONS
        {
            get;
            set;
        }
        //IMO LIMITED QUANTITY
      

        public string IMO_LIMITED_QTY
        {
            get;
            set;
        }
        //IMO PACKING GROUP
     

        public string IMO_PACK_GROUP
        {
            get;
            set;
        }
        //IMO PACKING INSTRUCTIONS
    

        public string IMO_PACK_INSTRUCTIONS
        {
            get;
            set;
        }
        //IMO PACKING PROVISIONS
       

        public string IMO_PACK_PROVISIONS
        {
            get;
            set;
        }
        //IMO PROPER SHIPPING NAME
    

        public string IMO_PROP_SHIP_NAME
        {
            get;
            set;
        }
        //IMO PROPER SHIPPING NAME MODIFIER
     

        public string IMO_PROP_SHIP_MODIFIER
        {
            get;
            set;
        }
        //IMO PSN CODE
      

        public string IMO_PSN_CODE
        {
            get;
            set;
        }
        //IMO SPECIAL PROVISIONS
     

        public string IMO_SPECIAL_PROV
        {
            get;
            set;
        }
        //IMO STOWAGE AND SEGREGATION
      

        public string IMO_STOW_SEGR
        {
            get;
            set;
        }
        //IMO SUBSIDIARY RISK LABEL
     

        public string IMO_SUBSIDIARY_RISK
        {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, IMO
     

        public string IMO_TANK_INSTR_IMO
        {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, PROVISIONS
      

        public string IMO_TANK_INSTR_PROV
        {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, UN
     

        public string IMO_TANK_INSTR_UN
        {
            get;
            set;
        }
        //IMO UN NUMBER
      

        public string IMO_UN_NUMBER
        {
            get;
            set;
        }
        #endregion

        //MSDS ITEM DESCRIPTION
        #region MSDS ITEM DESC
        //BATCH NUMBER
      

        public string BATCH_NUMBER
        {
            get;
            set;
        }
        //LOT NUMBER
     

        public string LOT_NUMBER
        {
            get;
            set;
        }

        //ITEM MANAGER
      

        public string ITEM_MANAGER
        {
            get;
            set;
        }
        //ITEM NAME
     
        public string ITEM_NAME
        {
            get;
            set;
        }
        //LOGISTICS FLSI NIIN VERIFIED
     

        public string LOG_FLIS_NIIN_VER
        {
            get;
            set;
        }
        //LOGISTICS FSC
       

        public int LOG_FSC
        {
            get;
            set;
        }
        //NET UNIT WEIGHT
      

        public string NET_UNIT_WEIGHT
        {
            get;
            set;
        }
        //QUANTITATIVE EXPRESSION EXP
       

        public string QUANTITATIVE_EXPRESSION
        {
            get;
            set;
        }
        //SHELF LIFE CODE
       
        public string SHELF_LIFE_CODE
        {
            get;
            set;
        }
        //SPECIAL EMPHASIS CODE
      
        public string SPECIAL_EMP_CODE
        {
            get;
            set;
        }
        //SPECIFICATION NUMBER
       
        public string SPECIFICATION_NUMBER
        {
            get;
            set;
        }
        //SPECIFICATION TYPE/GRADE/CLASS
       
        public string TYPE_GRADE_CLASS
        {
            get;
            set;
        }
        //TYPE OF CONTAINER
      
        public string TYPE_OF_CONTAINER
        {
            get;
            set;
        }
        //UN/NA NUMBER
      
        public string UN_NA_NUMBER
        {
            get;
            set;
        }
        //UNIT OF ISSUE
      
        public string UNIT_OF_ISSUE
        {
            get;
            set;
        }
        //UNIT OF ISSUE CONTAINER QUANTITY
      
        public string UI_CONTAINER_QTY
        {
            get;
            set;
        }
        //UPC/GTIN
      
        public string UPC_GTIN
        {
            get;
            set;
        }
        #endregion

        //MSDS LABEL INFO
        #region MSDS LABEL INFO
        //COMPANY CAGE(RESPONSIBLE PARTY)
      
        public string COMPANY_CAGE_RP
        {
            get;
            set;
        }
        //COMPANY NAME(RESPONSIBLE PARTY)
     
        public string COMPANY_NAME_RP
        {
            get;
            set;
        }
        //LABEL EMERGENCY TELE NUMBER
     
        public string LABEL_EMERG_PHONE
        {
            get;
            set;
        }
        //LABEL ITEM NAME
      
        public string LABEL_ITEM_NAME
        {
            get;
            set;
        }
        //LABEL PROCUREMENT YEAR
     
        public string LABEL_PROC_YEAR
        {
            get;
            set;
        }
        //LABEL PRODUCT IDENTITY
      
        public string LABEL_PROD_IDENT
        {
            get;
            set;
        }
        //LABEL PRODUCT SERIAL NUMBER
     
        public string LABEL_PROD_SERIALNO
        {
            get;
            set;
        }
        //LABEL SIGNAL WORD
     
        public string LABEL_SIGNAL_WORD
        {
            get;
            set;
        }
        //LABEL STOCK NUMBER
      
        public string LABEL_STOCK_NO
        {
            get;
            set;
        }
        //SPECIFIC HAZARDS
     
        public string SPECIFIC_HAZARDS
        {
            get;
            set;
        }
        #endregion

        //MSDS DISPOSAL
        #region MSDS DISPOSAL
        //DISPOSAL ADDITIONAL INFO
     
        public string DISPOSAL_ADD_INFO
        {
            get;
            set;
        }
        //EPA HAZARDOUS WASTE CODE
    
        public string EPA_HAZ_WASTE_CODE
        {
            get;
            set;
        }
        //EPA HAZARDOUS WAST INDICATOR
     
        public string EPA_HAZ_WASTE_IND
        {
            get;
            set;
        }
        //EPA HAZARDOUS WASTE NAME
    
        public string EPA_HAZ_WASTE_NAME
        {
            get;
            set;
        }
        #endregion

        //MSDS DOC TYPES
        //THESE ARE ALL SIMPLY FILE PATHS, ACTUAL
        //FILES WILL BE OBTAINED AND INSERTED AT HILT INSERTION TIME.
        #region MSDS DOCS
        //MSDS - SEE MSDS TABLE.
        //MSDS MANUFACTURER LABEL
        public string manufacturer_label_filename
        {
            get;
            set;
        }
        public byte[] MANUFACTURER_LABEL
        {
            get;
            set;
        }
        //MSDS TRANSLATED
        public string msds_translated_filename
        {
            get;
            set;
        }
        public byte[] MSDS_TRANSLATED
        {
            get;
            set;
        }
        //NESHAP COMPLIANCE CERTIFICATE
        public string neshap_comp_filename
        {
            get;
            set;
        }
        public byte[] NESHAP_COMP_CERT
        {
            get;
            set;
        }
        //OTHER DOCS
        public string other_docs_filename
        {
            get;
            set;
        }
        public byte[] OTHER_DOCS
        {
            get;
            set;
        }
        //PRODUCT SHEET
        public string product_sheet_filename
        {
            get;
            set;
        }
        public byte[] PRODUCT_SHEET
        {
            get;
            set;
        }
        //TRANSPORTATION CERT
        public string transportation_cert_filename
        {
            get;
            set;
        }
        public byte[] TRANSPORTATION_CERT
        {
            get;
            set;
        }
        #endregion


        //MAIN msds doc
        public byte[] msds_file
        {
            get;
            set;
        }
        public string file_name
        {
            get;
            set;
        }


        //HCC_id
        public int hcc_id
        {
            get;
            set;
        }


    }

}