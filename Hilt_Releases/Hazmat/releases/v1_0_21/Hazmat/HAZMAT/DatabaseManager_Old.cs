﻿using System; 
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
//using Common.Logging;
using System.IO;
using System.Data.SQLite;
using System.Collections;
using System.Diagnostics;
using System.Security.Cryptography;
using Microsoft.SqlServer.Server;
using DatabaseUpdate;

namespace HAZMAT
{
    public class DatabaseManager
    {
//        private static ILog logger = LogManager.GetCurrentClassLogger();

        private static string MDF_CONNECTION = null;

        public static String DB_BACKUP_DIRECTORY = "C:\\HILT_Data\\Backups";

        public enum FILE_TABLES {
            FileUpload,
            FileDownLoad
        };

        public enum FILE_TYPES {
            MSDS,
            MSSL,
            SMCL
        };

        public DatabaseManager()
        {
            if (MDF_CONNECTION == null)
            {
                MDF_CONNECTION = Configuration.ConnectionInfo;
            }

            //if (DB_BACKUP_DIRECTORY == null) {
            //    DB_BACKUP_DIRECTORY = Configuration.HILT_DataFolder + "\\Backups";

            //    // Make sure the folder exists
            //    if (!System.IO.Directory.Exists(DB_BACKUP_DIRECTORY)) {
            //        System.IO.Directory.CreateDirectory(DB_BACKUP_DIRECTORY);
            //    }
            //}
        }

        public DatabaseManager(string connectionString)
        {
            if (MDF_CONNECTION == null)
            {
                  MDF_CONNECTION = connectionString;
            }
        }


        [CoverageExclude]
        private void importReceivingInventory(SqlConnection mdfConn, SqlTransaction tran, string databaseLocation)
        {

            string invStmt = "insert into inventory (" +

            "shelf_life_expiration_date, manufacturer_date, location_id, qty, serial_number,"
            + "bulk_item, mfg_catalog_id"

            + ")"
            + "VALUES(" +
             "@shelf_life_expiration_date, @manufacturer_date, @location_id, @qty, @serial_number,"
            + "@bulk_item, @mfg_catalog_id"
            + ")";

            SqlCommand invCmd = new SqlCommand(invStmt, mdfConn);
            invCmd.Transaction = tran;

            SqlCommand tempCmd = new SqlCommand("select count(*) from receiving_cr where receiving_id = @receiving", mdfConn);
            tempCmd.Transaction = tran;
            
            

            //connect to the database the user uploaded into the system
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();


            SQLiteCommand liteCmd = new SQLiteCommand("select * from receiving_cr", liteCon);            

            SQLiteDataReader liteRdr = liteCmd.ExecuteReader();

            while (liteRdr.Read())
            {

                //check if receiving exists in change_records before we add the inventory
                tempCmd.Parameters.Clear();
                tempCmd.Parameters.AddWithValue("@receiving", Convert.ToInt32(liteRdr["receiving_id"]));
                object o = tempCmd.ExecuteScalar();
                int count = Convert.ToInt32(o);

                invCmd.Parameters.Clear();

                int mfg_catalog_id = Convert.ToInt32(liteRdr["mfg_catalog_id"]);
                int location_id = Convert.ToInt32(liteRdr["location_id"]);

                int? qty = null;
                try
                {
                    qty = Convert.ToInt32(liteRdr["qty"]);
                }
                catch 
                {

                }

                bool? bulk_item = null;
                try
                {
                    bulk_item = Convert.ToBoolean(liteRdr["bulk_item"]);
                }
                catch
                {

                }

                string serial_number = null;
                try
                {
                    serial_number = Convert.ToString(liteRdr["serial_number"]);
                }
                catch {}


                DateTime? shelf_life_expiration_date = null;
                try
                {
                    shelf_life_expiration_date = Convert.ToDateTime(liteRdr["shelf_life_expiration_date"]);
                }
                catch {}


                DateTime? manufacturer_date = null;
                try
                {
                    manufacturer_date = Convert.ToDateTime(liteRdr["manufacturer_date"]);
                }
                catch {}


                //insert new inventory at the location
                
                //set parameters and insert into inventory on the server
                invCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                invCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                invCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                invCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                invCmd.Parameters.AddWithValue("@qty", dbNull(qty));
                invCmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                invCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));

                if (count == 0)
                {
                    invCmd.ExecuteNonQuery();
                }

            }

           
            liteCmd.Dispose();
            liteCon.Close();

            
        }


        [CoverageExclude]
        public void updateATM(int atm_id, string cage, string niin, string serial, string slDate)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();


            string insertStmt = "update atmosphere_control set  msds_serial_number = @msds_serial_number, niin = @niin, cage = @cage, shelf_life = @shelf_life where atmosphere_control_id = @atmosphere_control_id";

            //create new command
            SqlCommand cmd = new SqlCommand(insertStmt, con);
            cmd.Parameters.AddWithValue("@msds_serial_number", serial);
            cmd.Parameters.AddWithValue("@niin", serial);
            cmd.Parameters.AddWithValue("@cage", serial);
            DateTime sl = DateTime.Parse(slDate);
            cmd.Parameters.AddWithValue("@shelf_life", sl);
            cmd.Parameters.AddWithValue("@atmosphere_control_id", atm_id); 

            cmd.ExecuteNonQuery();

            con.Close();
        }

        [CoverageExclude]
        public void importReceivingCR(SqlConnection mdfConn, SqlTransaction tran, string databaseLocation)
        {

            string insertStmt = "insert into receiving_cr(receiving_id,qty,location_id,action_complete,shelf_life_expiration_date,bulk_item,serial_number,mfg_catalog_id,expected_qty,manufacturer_date) " +
                               "VALUES(@receiving_id,@qty,@location_id,@action_complete,@shelf_life_expiration_date,@bulk_item,@serial_number,@mfg_catalog_id,@expected_qty,@manufacturer_date)";

            //create new command
            SqlCommand cmd = new SqlCommand(insertStmt, mdfConn);

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();
            string countStmt = "select count(*) from receiving_list where receiving_id = @id";

            SqlCommand cmd2 = new SqlCommand(countStmt, con);

            //set the transaction
            cmd.Transaction = tran;

            //connect to the database the user uploaded into the system
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();

            //select all records
            SQLiteCommand liteCmd = new SQLiteCommand("select * from receiving_cr", liteCon);

            SQLiteDataReader rdr = liteCmd.ExecuteReader();

            //loop through the results
            while (rdr.Read())
            {
                
                //clear parameters for each new insert
                cmd.Parameters.Clear();

                int receiving_id = Convert.ToInt32(rdr["receiving_id"]);

                //get the count to check for the id
                cmd2.Parameters.Clear();
                cmd2.Parameters.AddWithValue("@id", receiving_id);
                int count = Convert.ToInt32(cmd2.ExecuteScalar());


                int location_id = Convert.ToInt32(rdr["location_id"]);
                DateTime action_complete = Convert.ToDateTime(rdr["action_complete"]);

                DateTime? shelf_life_expiration_date = null;
                try
                {
                    shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}

                bool? bulk_item = null;
                try
                {
                    bulk_item = Convert.ToBoolean(rdr["bulk_item"]);
                }
                catch {}


                string serial_number = null;
                try
                {
                    serial_number = Convert.ToString(rdr["serial_number"]);
                }
                catch {}

                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                int? expected_qty = null;
                try
                {
                    expected_qty = Convert.ToInt32(rdr["expected_qty"]);
                }
                catch {}

                DateTime? manufacturer_date = null;
                try
                {
                    manufacturer_date = Convert.ToDateTime(rdr["manufacturer_date"]);
                }
                catch {}

                int? qty = null;
                try
                {
                    qty = Convert.ToInt32(rdr["qty"]);
                }
                catch {}


                cmd.Parameters.AddWithValue("@receiving_id", dbNull(receiving_id));
                cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
                cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                cmd.Parameters.AddWithValue("@expected_qty", dbNull(expected_qty));                
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                cmd.Parameters.AddWithValue("@qty", dbNull(qty));

                if (count > 0)
                {
                    cmd.ExecuteNonQuery();
                }

            }

            liteCmd.Dispose();
            liteCon.Close();
            con.Close();


        }



        public byte[] createFeedbackReport(List<FeedbackInfo> infoList)
        {

            //create temp file for creating the feedback report
            string tempFile = Path.GetTempFileName();

            //header
            string header = "Page" + "\t" + "Username" + "\t" + "Feedback" + "\t" + "Date Submitted";

            StreamWriter writer = new StreamWriter(tempFile);

            writer.WriteLine(header);

            foreach (FeedbackInfo f in infoList)
            {
                string feedback = f.Feedback.Replace("\r", "");
                feedback = feedback.Replace("\n", "");
                feedback = feedback.Replace("\t", "");

                string data = f.Page + "\t" + f.Username + "\t" + feedback + "\t" + f.Date.ToShortDateString();
                writer.WriteLine(data);

            }

            writer.Flush();
            writer.Close();

            //byte[]
            byte[] buff = null;
            FileStream fs = new FileStream(tempFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(tempFile).Length;
            buff = br.ReadBytes((int)numBytes);


            fs.Close();
            br.Close();

            //delete temp file
            File.Delete(tempFile);

            return buff;


        }
     
        public List<FeedbackInfo> getFeedbackInfo()
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from feedback", con);
            SqlDataReader rdr = cmd.ExecuteReader();

            List<FeedbackInfo> infoList = new List<FeedbackInfo>();

            while (rdr.Read())
            {
                FeedbackInfo info = new FeedbackInfo();

                string feedback = Convert.ToString(rdr["feedback"]);
                string username = Convert.ToString(rdr["username"]);
                string page = Convert.ToString(rdr["page_name"]);
                DateTime date = Convert.ToDateTime(rdr["date_submitted"]);

                info.Date = date;
                info.Username = username;
                info.Page = page;
                info.Feedback = feedback;

                infoList.Add(info);

            }

            con.Close();

            return infoList;


        }

        [CoverageExclude]
        public void importReceivingClipboard(string databaseLocation)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();


            try
            {
                importReceivingInventory(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing inventory(receiving): " + ex.Message);
            }


            try
            {
                importReceivingCR(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing offload_cr: " + ex.Message);
            }

            

            //commit all the work
            tran.Commit();

            //close the connection
            mdfConn.Close();

        }

        [CoverageExclude]
        public void insertHHReceivingList(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select * from receiving_list r " +
                          "where r.receiving_id not in (select c.receiving_id from receiving_cr c)";
           
            SqlCommand cmd = new SqlCommand(stmt, mdfConn);            
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into receiving_list (receiving_id,mfg_catalog_id,expected_qty,manufacturer_date,shelf_life_expiration_date,receive_item,show_item) " +
                              "VALUES (@receiving_id,@mfg_catalog_id,@expected_qty,@manufacturer_date,@shelf_life_expiration_date,@receive_item,@show_item)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int receiving_id = Convert.ToInt32(rdr["receiving_id"].ToString());
                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"].ToString());
                int expected_qty = Convert.ToInt32(rdr["expected_qty"].ToString());
                DateTime? manufacturer_date = null;
                    try{
                    manufacturer_date = Convert.ToDateTime(rdr["manufacturer_date"]);
                    }
                    catch
                    {
                    }


                    DateTime? shelf_life_expiration_date = null;
                    try
                    {
                        shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"].ToString());
                    }
                    catch
                    {

                    }
                



                liteCmd.Parameters.AddWithValue("@receiving_id", receiving_id);
                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", mfg_catalog_id);
                liteCmd.Parameters.AddWithValue("@expected_qty", expected_qty);
                liteCmd.Parameters.AddWithValue("@manufacturer_date", manufacturer_date);
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", shelf_life_expiration_date);              
                liteCmd.Parameters.AddWithValue("@receive_item", true);
                liteCmd.Parameters.AddWithValue("@show_item", true);
              
                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        public void insertHHInvInReceivingList(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select * from inventory";
                          

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into receiving_list (mfg_catalog_id,expected_qty,manufacturer_date,shelf_life_expiration_date,receive_item,show_item) " +
                              "VALUES (@mfg_catalog_id,@expected_qty,@manufacturer_date,@shelf_life_expiration_date,@receive_item,@show_item)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int? mfg_catalog_id = null;

                try
                {
                    mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"].ToString());
                }
                catch
                {

                }


                int? expected_qty = null;
                try
                {
                   expected_qty = Convert.ToInt32(rdr["qty"].ToString());
                }
                catch
                {

                }
                DateTime? manufacturer_date = null;
                try
                {
                    manufacturer_date = Convert.ToDateTime(rdr["manufacturer_date"]);
                }
                catch {}

                DateTime? shelf_life_expiration_date = null;
                try
                {
                    shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"].ToString());
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@expected_qty", dbNull(expected_qty));
                liteCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                liteCmd.Parameters.AddWithValue("@receive_item", false);
                liteCmd.Parameters.AddWithValue("@show_item", false);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        public void insertHHMfgCatalogForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            con.Open();


            //sqlite connection and statement
            string liteStmt = "insert into mfg_catalog (mfg_catalog_id,cage,manufacturer,niin_catalog_id) " +
                              "VALUES (@mfg_catalog_id,@cage,@manufacturer,@niin_catalog_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select * from mfg_catalog";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            
            while (rdr.Read())
            {
                cmd.Parameters.Clear();
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);

                string cage = null;
                try
                {
                    cage = Convert.ToString(rdr["cage"]);
                }
                catch {}
                string manufacturer = null;

                try
                {
                    manufacturer = Convert.ToString(rdr["manufacturer"]);
                }
                catch {}


                int? niin_catalog_id = null;

                try
                {
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}



                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                liteCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
            con.Close();


        }


        [CoverageExclude]
        public void insertHHNiinCatalogForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into niin_catalog (niin_catalog_id,fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,shelf_life_code_id,shelf_life_action_code_id, remarks,storage_type_id,cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,allowance_qty,created,ship_id) " +
                              "VALUES (@niin_catalog_id,@fsc,@niin,@ui,@um,@usage_category_id,@description,@smcc_id,@specs,@shelf_life_code_id,@shelf_life_action_code_id, @remarks,@storage_type_id,@cog_id,@spmig,@nehc_rpt,@catalog_group_id,@catalog_serial_number,@allowance_qty,@created,@ship_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            string stmt = "select * from niin_catalog n " +
                          "where n.niin_catalog_id in " +
                          "(select m.niin_catalog_id  from mfg_catalog m where m.mfg_catalog_id in " +
                          "(select r.mfg_catalog_id from receiving_list r)) " +
                          "OR n.niin_catalog_id in " +
                          "(select m.niin_catalog_id  from mfg_catalog m where m.mfg_catalog_id in " +
                          "(select i.mfg_catalog_id from inventory i))";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"].ToString());
                int fsc = Convert.ToInt32(rdr["fsc"]);
                string niin = Convert.ToString(rdr["niin"]);
                string ui = Convert.ToString(rdr["ui"]);
                string um = Convert.ToString(rdr["um"]);
                int usage_category_id = Convert.ToInt32(rdr["usage_category_id"]);
                string description = Convert.ToString(rdr["description"]);
                int smcc_id = Convert.ToInt32(rdr["smcc_id"]);
                string specs = Convert.ToString(rdr["specs"]);
                int shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);
                int shelf_life_action_code_id = Convert.ToInt32(rdr["shelf_life_action_code_id"]);
                string remarks = Convert.ToString(rdr["remarks"]);
                int storage_type_id = Convert.ToInt32(rdr["storage_type_id"]);
                int cog_id = Convert.ToInt32(rdr["cog_id"]);
                string spmig = rdr["spmig"].ToString();
                string nehc_rpt = rdr["nehc_rpt"].ToString();
                int catalog_group_id = Convert.ToInt32(rdr["catalog_group_id"]);
                string catalog_serial_number = rdr["catalog_serial_number"].ToString();
                int allowance_qty = Convert.ToInt32(rdr["allowance_qty"]);

                DateTime created = DateTime.Now;
                if (rdr["created"].ToString() != "")
                {
                    created = Convert.ToDateTime(rdr["created"]);
                }
                int ship_id = Convert.ToInt32(rdr["ship_id"]);

                liteCmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);
                liteCmd.Parameters.AddWithValue("@fsc", fsc);
                liteCmd.Parameters.AddWithValue("@niin", niin);
                liteCmd.Parameters.AddWithValue("@ui", ui);
                liteCmd.Parameters.AddWithValue("@um", um);
                liteCmd.Parameters.AddWithValue("@usage_category_id", usage_category_id);
                liteCmd.Parameters.AddWithValue("@description", description);
                liteCmd.Parameters.AddWithValue("@smcc_id", smcc_id);
                liteCmd.Parameters.AddWithValue("@specs", specs);
                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                liteCmd.Parameters.AddWithValue("@shelf_life_action_code_id", shelf_life_action_code_id);
                liteCmd.Parameters.AddWithValue("@remarks", remarks);
                liteCmd.Parameters.AddWithValue("@storage_type_id", storage_type_id);
                liteCmd.Parameters.AddWithValue("@cog_id", cog_id);
                liteCmd.Parameters.AddWithValue("@spmig", spmig);
                liteCmd.Parameters.AddWithValue("@nehc_rpt", nehc_rpt);
                liteCmd.Parameters.AddWithValue("@catalog_group_id", catalog_group_id);
                liteCmd.Parameters.AddWithValue("@catalog_serial_number", catalog_serial_number);
                liteCmd.Parameters.AddWithValue("@allowance_qty", allowance_qty);
                liteCmd.Parameters.AddWithValue("@created", created);
                liteCmd.Parameters.AddWithValue("@ship_id", ship_id);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        public void insertHHLocationsForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            SqlCommand cmd = new SqlCommand("select * from locations", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into locations (location_id,name,workcenter_id) VALUES (@location_id,@name,@workcenter_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"].ToString());
    string name = null;
                try
                {
                  name = rdr["name"].ToString();
                }
                catch {}
                string wid = null;
                try
                {
                   wid = rdr["workcenter_id"].ToString();
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@name",  dbNull(name));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@workcenter_id", dbNull(wid));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        public void insertHHHazardWarningsForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            
            SqlCommand cmd = new SqlCommand("select * from hazard_warnings", mdfConn);
           
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into hazard_warnings (hazard_warning_id,hazard_id_1,hazard_id_2, warning_level) VALUES (@hazard_warning_id,@hazard_id_1,@hazard_id_2, @warning_level)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int hazard_warning_id = Convert.ToInt32(rdr["hazard_warning_id"].ToString());
                int? hazard_id_1 = null;
                try
                {
                  hazard_id_1 = Convert.ToInt32(rdr["hazard_id_1"]);
                }
                catch {}

                int? hazard_id_2 = null;
                try
                {
                  hazard_id_2 = Convert.ToInt32(rdr["hazard_id_2"]);
                }
                catch { 
                }

                string warning_level = null;
                try
                {
                 warning_level = rdr["warning_level"].ToString();
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@hazard_warning_id", dbNull(hazard_warning_id));
                liteCmd.Parameters.AddWithValue("@hazard_id_1", dbNull(hazard_id_1));
                liteCmd.Parameters.AddWithValue("@hazard_id_2", dbNull(hazard_id_2));
                liteCmd.Parameters.AddWithValue("@warning_level", dbNull(warning_level));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        private void insertHHConfigForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            DateTime now = DateTime.Now.ToUniversalTime();

            //sqlite connection and statement
            string liteStmt = "insert into config (inventory_audit,decant,offload,receive, insurv, checkout_time) VALUES (@inventory_audit,@decant,@offload,@receive,@insurv,@checkout_time)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            liteCmd.Parameters.AddWithValue("@inventory_audit", false);
            liteCmd.Parameters.AddWithValue("@decant", false);
            liteCmd.Parameters.AddWithValue("@offload", false);
            liteCmd.Parameters.AddWithValue("@receive", true);
            liteCmd.Parameters.AddWithValue("@insurv", false);
            liteCmd.Parameters.AddWithValue("@checkout_time", now);

            liteCmd.ExecuteNonQuery();


            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }
        [CoverageExclude]
        public void insertHHHazardsForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {


            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            SqlCommand cmd = new SqlCommand("select * from hazards", mdfConn);

            SqlDataReader rdr = cmd.ExecuteReader();



            //sqlite connection and statement
            string liteStmt = "insert into hazards (hazard_id,hazard_name) VALUES (@hazard_id,@hazard_name)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();

                int hazardid = Convert.ToInt32(rdr["hazard_id"]);
               string hazardName = null;
                try
                {
                   hazardName = Convert.ToString(rdr["hazard_name"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@hazard_id", hazardid);
                liteCmd.Parameters.AddWithValue("@hazard_name", hazardName);

                liteCmd.ExecuteNonQuery();
            }

            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        public void insertHHShelfLifeCodeForReceiving(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {


            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            SqlCommand cmd = new SqlCommand("select * from shelf_life_code", mdfConn);

            SqlDataReader rdr = cmd.ExecuteReader();



            //sqlite connection and statement
            string liteStmt = "insert into shelf_life_code (shelf_life_code_id,slc,time_in_months,description,type) VALUES (@shelf_life_code_id,@slc,@time_in_months,@description,@type)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();

                int shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);
                string slc = Convert.ToString(rdr["slc"]);

                int? time_in_months = null;
                try
                {
                    time_in_months = Convert.ToInt32(rdr["time_in_months"]);
                }
                catch {}
                string description = Convert.ToString(rdr["description"]);
                string type = Convert.ToString(rdr["type"]);

                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                liteCmd.Parameters.AddWithValue("@slc", dbNull(slc));
                liteCmd.Parameters.AddWithValue("@time_in_months", dbNull(time_in_months));
                liteCmd.Parameters.AddWithValue("@description", dbNull(description));
                liteCmd.Parameters.AddWithValue("@type", dbNull(type));

                liteCmd.ExecuteNonQuery();
            }

            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        private void insertReceivingHandheldData(string dbLocation)
        {
            //connect to database which was copied from the template.  the temp file will be deleted after db is saved to server database
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert receiving_list
            try
            {
                insertHHReceivingList(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into receiving_list : " + ex.Message);

            }

            //insert receiving_list (merging inventory)
            try
            {
                insertHHInvInReceivingList(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into receiving_list (merging inventory) : " + ex.Message);

            }

            //insert mfg_catalog
            try
            {
                insertHHMfgCatalogForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into mfg_catalog (receiving) : " + ex.Message);
            }

            //insert niin_catalog (filtered)
            try
            {
                insertHHNiinCatalogForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into niin_catalog : " + ex.Message);
            }

            

            //insert locations
            try
            {
                insertHHLocationsForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into locations : " + ex.Message);
            }

            try
            {
                insertHHHazardWarningsForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazard warnings : " + ex.Message);
            }
           
            //insert config
            try
            {
                insertHHConfigForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into config : " + ex.Message);
            }

            
            //insert hazards
            try
            {
                insertHHHazardsForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazards : " + ex.Message);
            }


            //insert offload_CR
            try
            {
                insertHHShelfLifeCodeForReceiving(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into shelf_life_code : " + ex.Message);
            }



            try
            {
                insertHHAtmosphereControl(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into atmosphere_control : " + ex.Message);
            }

            tran.Commit();
            liteCon.Close();


        }

        [CoverageExclude]
        public Hashtable createReceivingClipboard(string dbLocation)
        {

            Hashtable table = new Hashtable();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();


            //create tempFile to copy the template to - 
            string tempFileLocation = Path.GetTempFileName();

            //copy template
            File.Copy(dbLocation, tempFileLocation, true);



            try
            {                
                insertReceivingHandheldData(tempFileLocation);
            }
            catch (Exception ex)
            {
                File.Delete(tempFileLocation);
                throw new Exception(ex.Message);
            }
            //database has the data inserted at this point.  save the handheld database to the server database - pass to the user to download

            FileStream fs = new FileStream(tempFileLocation, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];

            // read in the file stream to the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            // close the file stream
            fs.Close();

            string selectStmt = "insert into receiving_clipboard (clip_file,date_created) VALUES (@clip,@date_created);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Transaction = tran;

            string time = DateTime.Now.ToShortDateString();

            cmd.Parameters.AddWithValue("@date_created", time);
            cmd.Parameters.AddWithValue("@clip", fileData);

            object o = cmd.ExecuteScalar();

            int clipid = Convert.ToInt32(o);

            tran.Commit();

            mdfConn.Close();

            //delete the temporary file - it is no longer needed
            File.Delete(tempFileLocation);

            table.Add("file", fileData);
            table.Add("clipid", clipid);

            return table;

        }


        public Boolean checkAuditByIdAndDate(DateTime selectedDate, int recurringId)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select count(*) from inventory_audits a where a.scheduled_on = @date " +
                          "AND a.inventory_audit_id in( " +
                          "select r.inventory_audit_id from audits_in_recurring r where r.recurring_id = @recurring)";

            //SqlCommand cmd = new SqlCommand("select * from inventory_audits where inventory_audit_id = @auditid", con);
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@recurring", recurringId);
            cmd.Parameters.AddWithValue("@date", selectedDate);

            object o = cmd.ExecuteScalar();
            int count = 0;
            try
            {
              count = Convert.ToInt32(o);
            }
            catch {}

            con.Close();

            if (count == 0)
            {
                return false;
            }
            else 
            {
                return true;
            }
           
        }


        public List<string> getLocationNameList() 
        {

            List<string> locationList = new List<string>();

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from locations  WHERE name<>'NEWLY ISSUED'", con);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {

                locationList.Add(rdr["name"].ToString());

            }


            con.Close();

            return locationList;
        }

        public void insertMSSLSubs(List<MsslSubs> subList, SqlTransaction tran, SqlConnection con)
        {

            string stmt = "insert into subs(niin,sub_niin) VALUES (@niin,@sub_niin)";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Transaction = tran;

            foreach (MsslSubs m in subList)
            {

                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@niin", dbNull(m.Niin));
                cmd.Parameters.AddWithValue("@sub_niin", dbNull(m.Sub_niin));                

                cmd.ExecuteNonQuery();
            }


        }

        public void insertMSSLInventory(List<MsslInventory> inventoryList, SqlTransaction tran, SqlConnection con)
        {
            DateTime time = DateTime.Now;

            string stmt = "insert into mssl_inventory(niin,ati,cog,mcc,ui,up,netup,location,qty,lmc,irc,dues,ro,rp,amd,smic,slc,slac,smcc,nomenclature, date_inserted) " +
                            "Values(@niin,@ati,@cog,@mcc,@ui,@up,@netup,@location,@qty,@lmc,@irc,@dues,@ro,@rp,@amd,@smic,@slc,@slac,@smcc,@nomenclature, @date_inserted)";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Transaction = tran;

            foreach(MsslInventory m in inventoryList)
            {

                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@niin", dbNull(m.Niin));
                cmd.Parameters.AddWithValue("@ati", dbNull(m.Ati));
                cmd.Parameters.AddWithValue("@cog", dbNull(m.Cog));
                cmd.Parameters.AddWithValue("@mcc", dbNull(m.Mcc));
                cmd.Parameters.AddWithValue("@ui", dbNull(m.Ui));
                cmd.Parameters.AddWithValue("@up", dbNull(m.Up));
                cmd.Parameters.AddWithValue("@netup", dbNull(m.Netup));
                cmd.Parameters.AddWithValue("@location", dbNull(m.Location));
                cmd.Parameters.AddWithValue("@qty", dbNull(m.Qty));
                cmd.Parameters.AddWithValue("@lmc", dbNull(m.Lmc));
                cmd.Parameters.AddWithValue("@irc", dbNull(m.Irc));
                cmd.Parameters.AddWithValue("@dues", dbNull(m.Dues));
                cmd.Parameters.AddWithValue("@ro", dbNull(m.Ro));
                cmd.Parameters.AddWithValue("@rp", dbNull(m.Rp));
                cmd.Parameters.AddWithValue("@amd", dbNull(m.Amd));
                cmd.Parameters.AddWithValue("@smic", dbNull(m.Smic));
                cmd.Parameters.AddWithValue("@slc", dbNull(m.Slc));
                cmd.Parameters.AddWithValue("@slac", dbNull(m.Slac));
                cmd.Parameters.AddWithValue("@smcc", dbNull(m.Smcc));
                cmd.Parameters.AddWithValue("@nomenclature", dbNull(m.Nomenclature));
                cmd.Parameters.AddWithValue("@date_inserted", dbNull(time));

                cmd.ExecuteNonQuery();
            }

        }


        public DateTime getMSSLDate()
        {
            return new DateTime();
        }

        public void removeMSSLInventory(SqlTransaction tran, SqlConnection con)
        {

            SqlCommand cmd = new SqlCommand("delete from mssl_inventory",con);
            cmd.Transaction = tran;
            cmd.ExecuteNonQuery();

        }

        public void removeMSSLSubs(SqlTransaction tran, SqlConnection con)
        {

            SqlCommand cmd = new SqlCommand("delete from subs", con);
            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

        }

        public void insertMSSLData(MSSLData data)
        {

            //create connection to server db
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            //create transaction for all the mssl data work
            SqlTransaction tran = con.BeginTransaction();


            //remove mssl_inventory
            try
            {
                removeMSSLInventory(tran, con);
            }
            catch (Exception ex)
            {
                con.Close();
                throw new Exception("Error removing MSSL Inventory: " + ex.Message);
            }

            //remove subs
            try
            {
                removeMSSLSubs(tran, con);
            }
            catch (Exception ex)
            {
                con.Close();
                throw new Exception("Error removing MSSL Subs: " + ex.Message);
            }

            //insert msslInventory
            try
            {
                insertMSSLInventory(data.InventoryList, tran, con);
            }
            catch (Exception ex)
            {
                con.Close();
                throw new Exception("Error insert MSSL Inventory: " + ex.Message);
            }

            //insert mssl subs
            try
            {
                insertMSSLSubs(data.SubList, tran, con);
            }
            catch (Exception ex)
            {
                con.Close();
                throw new Exception("Error inserting MSSL Subs: " + ex.Message);
            }



            //commit
            tran.Commit();

            con.Close();

        }

    
        public DataTable getFilteredAvailableLocationsAuditNew(DataTable selectedTable, string filterValue, string workCenter)
        {

            List<Int32> selectedValues = new List<Int32>();
            string[] selectedParameters = new string[selectedTable.Rows.Count];
            int count = 0;

            foreach (DataRow row in selectedTable.Rows)
            {
                selectedValues.Add(Convert.ToInt32(row[0]));
                selectedParameters[count] = "@sp" + count.ToString();
                count++;
            }
            
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //select locations where their id does not match the ID from the selected DataTable (parameter)
            string selectStmt = "";
            SqlCommand cmd;

            if (count > 0)
            {

                selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                   "FROM locations l, workcenter w " +
                                  "where l.name<>'NEWLY ISSUED' AND l.workcenter_id = w.workcenter_id " +
                                  "AND l.location_id not in(" + string.Join(",", selectedParameters) + ")" +
                                  "AND l.name like '%' + @filter + '%' " +
                                  "AND w.description like '%' + @workFilter + '%'";
                cmd = new SqlCommand(selectStmt, mdfConn);
                cmd.Parameters.AddWithValue("@filter", filterValue);
                cmd.Parameters.AddWithValue("@workFilter", workCenter);
                for (int i = 0; i < count; i++)
                    cmd.Parameters.AddWithValue(selectedParameters[i], selectedValues[i]);
            }
            else
            {

                selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                  "FROM locations l, workcenter w " +
                                 "where l.name<>'NEWLY ISSUED' AND l.workcenter_id = w.workcenter_id " +
                                 "AND l.name like '%' + @filter + '%' " +
                                 "AND w.description like '%' + @workFilter + '%'";
                cmd = new SqlCommand(selectStmt, mdfConn);
                cmd.Parameters.AddWithValue("@filter", filterValue);
                cmd.Parameters.AddWithValue("@workFilter", workCenter);
            }

            

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }



        public DataTable getSingleLocationTableByLocationId(int location_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                "FROM locations l, workcenter w " +
                               "where l.workcenter_id = w.workcenter_id " + 
                               "AND l.location_id = @location";


            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            cmd.Parameters.AddWithValue("@location", location_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;


        }

        public DataTable getMsslInventory()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select niin, ati, cog, mcc, ui, up, netup, location, qty, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature from mssl_inventory";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

          
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;


        }
        public DataTable getMsslSubs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select niin, sub_niin from subs";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;


        }




        private void insertHHGarbageCR(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            string insertStmt = "insert into garbage_offload_cr (garbage_offload_id,qty_offloaded,expected_qty_offload,action_complete,username,device_type, from_server) " +
                                                       "VALUES (@garbage_offload_id,@qty_offloaded,@expected_qty_offload,@action_complete,@username,@device_type,@from_server)";

            SQLiteCommand liteCmd = new SQLiteCommand(insertStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select * from garbage_offload_cr g where g.garbage_offload_id in " + 
                          "(select l.garbage_offload_id from garbage_offload_list l where " + 
                          "l.archived_id is null)";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int garbage_offload_id = Convert.ToInt32(rdr["garbage_offload_id"]);
                int offload_qty = Convert.ToInt32(rdr["offload_qty"]);


                int? expected_offload_qty = null;
                try
                {
                   expected_offload_qty = Convert.ToInt32(rdr["expected_qty_offload"]);
                }
                catch {}

                DateTime? action_complete = null;
                try
                {
                    action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch {}

                string username = null;
                try
                {
                    username = Convert.ToString(rdr["username"]);
                }
                catch {}

                string device_type = null;
                try
                {
                    device_type = rdr["device_type"].ToString();
                }
                catch {}


                liteCmd.Parameters.AddWithValue("@garbage_offload_id", garbage_offload_id);
                liteCmd.Parameters.AddWithValue("@qty_offloaded", offload_qty);
                liteCmd.Parameters.AddWithValue("@expected_qty_offload", expected_offload_qty);
                liteCmd.Parameters.AddWithValue("@action_complete", action_complete);
                liteCmd.Parameters.AddWithValue("@username", username);
                liteCmd.Parameters.AddWithValue("@device_type", device_type);
                liteCmd.Parameters.AddWithValue("@from_server", true);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();




        }


        private void insertHHOffloadCR(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            string insertStmt = "insert into offload_cr (offload_list_id,mfg_catalog_id,qty_offloaded,action_complete,username,device_type,manufacturer_date,shelf_life_expiration_date,expected_qty_offload,serial_number, bulk_item, from_server) " +
                               "VALUES (@offload_list_id,@mfg_catalog_id,@qty_offloaded,@action_complete,@username,@device_type,@manufacturer_date,@shelf_life_expiration_date,@expected_qty_offload,@serial_number,@bulk_item, @from_server)";


            SQLiteCommand liteCmd = new SQLiteCommand(insertStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select * from offload_cr c where c.offload_list_id in " +
                          "(select offload_list_id from offload_list o where o.archived_id is null)";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int offload_list_id = Convert.ToInt32(rdr["offload_list_id"]);

                int? mfg_catalog_id = null;
                try
                {
                  mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}
                int qty_offloaded = Convert.ToInt32(rdr["qty_offloaded"]);


                int? expected_qty_offload = null;
                try

                {
                    expected_qty_offload = Convert.ToInt32(rdr["expected_qty_offload"]);
                }
                catch {}

                bool? bulk_item = null;
                try
                {
                    bulk_item = Convert.ToBoolean(rdr["bulk_item"]);
                }
                catch {}

                string serial_number = null;
                try
                {
                    serial_number = rdr["serial_number"].ToString();
                }
                catch {}

                DateTime? shelf_life_expiration_date = null;
                try
                {
                    shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}

                DateTime? manufacturer_date = null;
                try
                {
                    manufacturer_date = Convert.ToDateTime(rdr["manufacturer_date"]);
                }
                catch {}

                DateTime? action_complete = null;
                try
                {
                    action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch (Exception)
                {
                }

                string username = null;
                try
                {
                    username = rdr["username"].ToString();
                }
                catch {}

                string device_type = null;
                try
                {
                    device_type = rdr["device_type"].ToString();
                }
                catch {}


                liteCmd.Parameters.AddWithValue("@offload_list_id", dbNull(offload_list_id));
                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@qty_offloaded", dbNull(qty_offloaded));
                liteCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
                liteCmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                liteCmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                liteCmd.Parameters.AddWithValue("@username", dbNull(username));
                liteCmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
                liteCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                liteCmd.Parameters.AddWithValue("@expected_qty_offload", dbNull(expected_qty_offload));
                liteCmd.Parameters.AddWithValue("@from_server", dbNull(true));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();




        }


        public void insertHHConfigForDecanting(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            DateTime now = DateTime.Now.ToUniversalTime();

            //sqlite connection and statement
            string liteStmt = "insert into config (inventory_audit,decant,offload, insurv, receive, checkout_time) VALUES (@inventory_audit,@decant,@offload, @insurv, @receive, @checkout_time)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            liteCmd.Parameters.AddWithValue("@inventory_audit", false);
            liteCmd.Parameters.AddWithValue("@decant", true);
            liteCmd.Parameters.AddWithValue("@offload", false);
            liteCmd.Parameters.AddWithValue("@insurv", false);
            liteCmd.Parameters.AddWithValue("@receive", false);
            liteCmd.Parameters.AddWithValue("@checkout_time", dbNull(now));

            liteCmd.ExecuteNonQuery();


            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }

        private void insertHHConfigForOffload(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            DateTime now = DateTime.Now.ToUniversalTime();

            //sqlite connection and statement
            string liteStmt = "insert into config (inventory_audit,decant,offload, insurv, receive, checkout_time) VALUES (@inventory_audit,@decant,@offload, @insurv, @receive, @checkout_time)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            liteCmd.Parameters.AddWithValue("@inventory_audit", false);
            liteCmd.Parameters.AddWithValue("@decant", false);
            liteCmd.Parameters.AddWithValue("@offload", true);
            liteCmd.Parameters.AddWithValue("@insurv", false);
            liteCmd.Parameters.AddWithValue("@receive", false);
            liteCmd.Parameters.AddWithValue("@checkout_time", dbNull(now));





            liteCmd.ExecuteNonQuery();


            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHConfigForAudit(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int auditid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            DateTime now = DateTime.Now.ToUniversalTime();

            //sqlite connection and statement
            string liteStmt = "insert into config (inventory_audit,decant,offload,inventory_audit_id, receive, insurv, checkout_time) VALUES (@inventory_audit,@decant,@offload,@inventory_audit_id, @receive, @insurv, @checkout_time)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            

            liteCmd.Parameters.AddWithValue("@inventory_audit", true);
            liteCmd.Parameters.AddWithValue("@decant", false);
            liteCmd.Parameters.AddWithValue("@offload", false);
            liteCmd.Parameters.AddWithValue("@receive", false);
            liteCmd.Parameters.AddWithValue("@insurv", false);
            liteCmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(auditid));
            liteCmd.Parameters.AddWithValue("@checkout_time", dbNull(now));

            liteCmd.ExecuteNonQuery();


            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }


        public Dictionary<string, List<RSupplyInventory>> getInventoryForRSupplyExport()
        {
            Dictionary<string, List<RSupplyInventory>> invMap = new Dictionary<string, List<RSupplyInventory>>();
            List<RSupplyInventory> list = new List<RSupplyInventory>();
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select qty, cosal, niin, name, last_audit_date "
                + "from vInventory where name <> 'NEWLY ISSUED' AND cosal IN ('HME','Q')";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                RSupplyInventory r = new RSupplyInventory();

                int nTemp = 0;
                int.TryParse(rdr["qty"].ToString(), out nTemp);
                r.Qty = nTemp;

                r.Ati = rdr["cosal"].ToString().Trim();
                r.Niin = rdr["niin"].ToString();
                r.Location = rdr["name"].ToString();
                DateTime dtTemp = new DateTime(0);

                Debug.WriteLine("Last_Audit_Date: " + rdr["last_audit_date"].ToString());

                DateTime.TryParse(rdr.GetValue(4).ToString(), out dtTemp);
                r.Last_Audit_Date = dtTemp;

                list.Add(r);
            }
            rdr.Dispose();
            cmd.Dispose();

            foreach (RSupplyInventory inv in list) {
               List<RSupplyInventory> currentList = null;
               if (!invMap.ContainsKey(inv.Niin)) {
                   currentList = new List<RSupplyInventory>();
                    invMap.Add(inv.Niin, currentList);
               } else {
                   currentList = (List<RSupplyInventory>)invMap[inv.Niin];
               }

               currentList.Add(inv);
          }

            mdfConn.Close();

            return invMap;
        }

        public void insertHHMfgCatalogForOffload(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into mfg_catalog (mfg_catalog_id,cage,manufacturer,niin_catalog_id) " +
                              "VALUES (@mfg_catalog_id,@cage,@manufacturer,@niin_catalog_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select * from mfg_catalog m " +
                          "where m.mfg_catalog_id in " +
                          "(select mfg_catalog_id from offload_list o where o.archived_id is null)";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                string cage = Convert.ToString(rdr["cage"]);
                string manufacturer = Convert.ToString(rdr["manufacturer"]);
                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);

                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", mfg_catalog_id);
                liteCmd.Parameters.AddWithValue("@cage", cage);
                liteCmd.Parameters.AddWithValue("@manufacturer", manufacturer);
                liteCmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHNiinCatalogForOffload(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into niin_catalog (niin_catalog_id,fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,shelf_life_code_id,shelf_life_action_code_id, remarks,storage_type_id,cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,allowance_qty,created,ship_id) " +
                              "VALUES (@niin_catalog_id,@fsc,@niin,@ui,@um,@usage_category_id,@description,@smcc_id,@specs,@shelf_life_code_id,@shelf_life_action_code_id, @remarks,@storage_type_id,@cog_id,@spmig,@nehc_rpt,@catalog_group_id,@catalog_serial_number,@allowance_qty,@created,@ship_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            string stmt = "select * from niin_catalog n " +
                          "where n.niin_catalog_id in " +
                          "(select m.niin_catalog_id from mfg_catalog m where m.mfg_catalog_id in " +
                          "(select mfg_catalog_id from offload_list o where o.archived_id is null))";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"].ToString());
                int fsc = Convert.ToInt32(rdr["fsc"]);
                string niin = Convert.ToString(rdr["niin"]);
                string ui = Convert.ToString(rdr["ui"]);
                string um = Convert.ToString(rdr["um"]);
                int usage_category_id = Convert.ToInt32(rdr["usage_category_id"]);
                string description = Convert.ToString(rdr["description"]);
                int smcc_id = Convert.ToInt32(rdr["smcc_id"]);
                string specs = Convert.ToString(rdr["specs"]);
                int shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);
                int shelf_life_action_code_id = Convert.ToInt32(rdr["shelf_life_action_code_id"]);
                string remarks = Convert.ToString(rdr["remarks"]);
                int storage_type_id = Convert.ToInt32(rdr["storage_type_id"]);
                int cog_id = Convert.ToInt32(rdr["cog_id"]);
                string spmig = rdr["spmig"].ToString();
                string nehc_rpt = rdr["nehc_rpt"].ToString();
                int catalog_group_id = Convert.ToInt32(rdr["catalog_group_id"]);
                string catalog_serial_number = rdr["catalog_serial_number"].ToString();
                int allowance_qty = Convert.ToInt32(rdr["allowance_qty"]);

                DateTime created = DateTime.Now;
                if (rdr["created"].ToString() != "")
                {
                    created = Convert.ToDateTime(rdr["created"]);
                }
                int ship_id = Convert.ToInt32(rdr["ship_id"]);

                liteCmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);
                liteCmd.Parameters.AddWithValue("@fsc", fsc);
                liteCmd.Parameters.AddWithValue("@niin", niin);
                liteCmd.Parameters.AddWithValue("@ui", ui);
                liteCmd.Parameters.AddWithValue("@um", um);
                liteCmd.Parameters.AddWithValue("@usage_category_id", usage_category_id);
                liteCmd.Parameters.AddWithValue("@description", description);
                liteCmd.Parameters.AddWithValue("@smcc_id", smcc_id);
                liteCmd.Parameters.AddWithValue("@specs", specs);
                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                liteCmd.Parameters.AddWithValue("@shelf_life_action_code_id", shelf_life_action_code_id);
                liteCmd.Parameters.AddWithValue("@remarks", remarks);
                liteCmd.Parameters.AddWithValue("@storage_type_id", storage_type_id);
                liteCmd.Parameters.AddWithValue("@cog_id", cog_id);
                liteCmd.Parameters.AddWithValue("@spmig", spmig);
                liteCmd.Parameters.AddWithValue("@nehc_rpt", nehc_rpt);
                liteCmd.Parameters.AddWithValue("@catalog_group_id", catalog_group_id);
                liteCmd.Parameters.AddWithValue("@catalog_serial_number", catalog_serial_number);
                liteCmd.Parameters.AddWithValue("@allowance_qty", allowance_qty);
                liteCmd.Parameters.AddWithValue("@created", created);
                liteCmd.Parameters.AddWithValue("@ship_id", ship_id);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHWorkCentersForOffload(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into workcenter (workcenter_id,description,wid) VALUES (@workcenter_id,@desc,@wid)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select distinct w.workcenter_id,w.description,w.wid " +
                          "from workcenter w, offload_list o, locations l " +
                          "where w.workcenter_id = l.workcenter_id " +
                          "AND o.location_id = l.location_id " +
                          "AND o.archived_id is null";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int workcenter_id = Convert.ToInt32(rdr["workcenter_id"].ToString());
                string description = rdr["description"].ToString();
                string wid = rdr["wid"].ToString();

                liteCmd.Parameters.AddWithValue("@workcenter_id", workcenter_id);
                liteCmd.Parameters.AddWithValue("@desc", description);
                liteCmd.Parameters.AddWithValue("@wid", wid);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHLocationsForOffload(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            SqlCommand cmd = new SqlCommand("select distinct l.location_id,l.name,l.workcenter_id from locations l, offload_list o where l.location_id = o.location_id AND o.archived_id is null", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into locations (location_id,name,workcenter_id) VALUES (@location_id,@name,@workcenter_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"].ToString());
                string name = rdr["name"].ToString();
                string wid = rdr["workcenter_id"].ToString();

                liteCmd.Parameters.AddWithValue("@name", name);
                liteCmd.Parameters.AddWithValue("@location_id", location_id);
                liteCmd.Parameters.AddWithValue("@workcenter_id", wid);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        [CoverageExclude]
        private void insertOffloadHandheldData(string dbLocation)
        {
            //connect to database which was copied from the template.  the temp file will be deleted after db is saved to server database
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert offload_list -
            try
            {
                insertHHOffloadList(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into offload_list : " + ex.Message);
            }

            try
            {
                insertHHGarbageOffloadList(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into garbage_offload_list : " + ex.Message);
            }

            //insert garbage_items
            try
            {
                insertHHGarbageItems(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into garbage_items : " + ex.Message);
            }

            //insert locations
            try
            {
                insertHHLocationsForOffload(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into locations : " + ex.Message);
            }


            //insert workcenters
            try
            {
                insertHHWorkCentersForOffload(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into workcenter : " + ex.Message);
            }

            try
            {
                insertHHNiinCatalogForOffload(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into niin_catalog : " + ex.Message);
            }

            try
            {
                insertHHMfgCatalogForOffload(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into mfg_catalog : " + ex.Message);
            }

            try
            {
                insertHHConfigForOffload(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into config : " + ex.Message);
            }

            //insert offload_CR
            try
            {
                insertHHOffloadCR(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into offload_cr : " + ex.Message);
            }


            //insert offload_CR
            try
            {
                insertHHGarbageCR(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into garbage_cr : " + ex.Message);
            }


            try
            {
                insertHHAtmosphereControl(dbLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into atmosphere_control : " + ex.Message);
            }

            tran.Commit();
            liteCon.Close();


        }




        public Hashtable createOffloadClipboard(string dbLocation)
        {

            Hashtable table = new Hashtable();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();


            //create tempFile to copy the template to - 
            string tempFileLocation = Path.GetTempFileName();

            //copy template
            File.Copy(dbLocation, tempFileLocation, true);



            try
            {
                insertOffloadHandheldData(tempFileLocation);
            }
            catch (Exception ex)
            {
                File.Delete(tempFileLocation);
                throw new Exception(ex.Message);
            }
            //database has the data inserted at this point.  save the handheld database to the server database - pass to the user to download

            FileStream fs = new FileStream(tempFileLocation, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];

            // read in the file stream to the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            // close the file stream
            fs.Close();

            string selectStmt = "insert into offload_clipboard (clip_file,date_created) VALUES (@clip,@date_created);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Transaction = tran;

            string time = DateTime.Now.ToShortDateString();

            cmd.Parameters.AddWithValue("@date_created", time);
            cmd.Parameters.AddWithValue("@clip", fileData);

            object o = cmd.ExecuteScalar();

            int clipid = Convert.ToInt32(o);

            tran.Commit();

            mdfConn.Close();

            //delete the temporary file - it is no longer needed
            File.Delete(tempFileLocation);

            table.Add("file", fileData);
            table.Add("clipid", clipid);

            return table;

        }


        public DataTable checkHHOffloadOffloadList(string dbLocation)
        {
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from offload_list", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }

        public DataTable checkHHOffloadWorkcenters(string dbLocation)
        {
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from workcenter", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }

        [CoverageExclude]
        public DataTable checkHHReceivingLocations(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from locations", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }

        public DataTable checkHHOffloadGarbageLocations(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from locations", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }

        public DataTable checkHHOffloadGarbageItems(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from garbage_items", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }

        public DataTable checkHHOffloadNIINCatalog(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from niin_catalog", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }


        public DataTable checkHHOffloadGarbageOffloadList(string dbLocation)
        {
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from garbage_offload_list", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;

        }

        public DataTable checkHHOffloadMFGCatalog(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dbLocation);
            liteCon.Open();

            SQLiteCommand liteCom = new SQLiteCommand("select * from mfg_catalog", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(liteCom).Fill(dt);

            liteCom.Dispose();
            liteCon.Close();

            return dt;
        }


        public DataTable getAllOffloadList()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand("select * from offload_list where archived_id is null", conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;
        }

        public DataTable getAllGarbageOffloadList()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand("select * from garbage_offload_list where archived_id is null", conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;
        }

        public DataTable getVolumesInOffload(int offload_list_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from volumes_in_offload WHERE offload_list_id=@offload_list_id";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@offload_list_id", dbNull(offload_list_id));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;


        }

        public DataTable getVolumesInInventoryAudit(int inventory_audit_cr_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from volumes_in_inventory_audit WHERE inventory_audit_cr_id=@inventory_audit_cr_id";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@inventory_audit_cr_id", dbNull(inventory_audit_cr_id));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;


        }
        public DataTable getVolumesInIssue(int issue_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from volumes_in_issue WHERE issue_id=@issue_id";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@issue_id", dbNull(issue_id));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;


        }

        public DataTable getAllGarbageItems()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from garbage_items g where g.garbage_item_id in (select garbage_item_id from garbage_offload_list l where l.archived_id is null)";

            SqlCommand cmd = new SqlCommand(stmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;


        }

        public DataTable getAllNiinCatalog()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from niin_catalog n " +
                          "where n.niin_catalog_id in " +
                          "(select m.niin_catalog_id from mfg_catalog m where m.mfg_catalog_id in " +
                          "(select mfg_catalog_id from offload_list o where o.archived_id is null))";

            SqlCommand cmd = new SqlCommand(stmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;
        }

        public DataTable getAllMfgCatalog()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from mfg_catalog m " +
                          "where m.mfg_catalog_id in " +
                          "(select mfg_catalog_id from offload_list o where o.archived_id is null)";

            SqlCommand cmd = new SqlCommand(stmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            return dt;

        }


        public bool isAdmin(string username)
        {
            return isUserInRole(username, "ADMIN");
        }

        public bool isSupplyOfficer(string username)
        {
            return isUserInRole(username, "SUPPLY OFFICER");
        }

        public bool isSupplyUser(string username)
        {
            return isUserInRole(username, "SUPPLY USER");
        }

        public bool isWorkCenterUser(string username)
        {
            return isUserInRole(username, "WORKCENTER USER");
        }



        public bool isUserInRole(string username, string role)
        {
            bool isUserInRole = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * FROM user_roles WHERE username=@username AND role=@role";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@username", dbNull(username));
            cmd.Parameters.AddWithValue("@role", dbNull(role));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            if (dt.Rows.Count > 0)
                isUserInRole = true;

            return isUserInRole;

        }

        public DataTable getLatestInsurvChecklist()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * from insurv_checklist where checklist_id=(Select MAX(checklist_id) from insurv_checklist)";

            SqlCommand cmd = new SqlCommand(stmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            if (dt.Rows.Count == 0)
            {
                //If no checklist in db yet, add one
                stmt = "INSERT INTO insurv_checklist "
                + "(ATM_CON_TAGS_A) "
                + " VALUES ('N/A')";
                cmd = new SqlCommand(stmt, conn);

                cmd.ExecuteNonQuery();

                //Update newly created record with default values
                updateInsurvChecklist(new ChecklistInsurv());

                //return newly inserted record
                stmt = "select * from insurv_checklist where checklist_id=(Select MAX(checklist_id) from insurv_checklist)";
                cmd = new SqlCommand(stmt, conn);
                dt = new DataTable();
                new SqlDataAdapter(cmd).Fill(dt);
            }

            cmd.Dispose();
            conn.Close();

            return dt;

        }

        public void insertHHOffloadMFGCatalog(string hhDBLocation)
        {

         

        }

        public void updateInsurvChecklist(ChecklistInsurv checklist)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();


            string stmt = "update insurv_checklist " +
            "set ATM_CON_TAGS_A = @ATM_CON_TAGS_A, ATM_CON_TAGS_B = @ATM_CON_TAGS_B,  ATM_AUTHORIZATION_A = @ATM_AUTHORIZATION_A,"
            + " ATM_AUTHORIZATION_B = @ATM_AUTHORIZATION_B, BROMINE_A = @BROMINE_A,  BROMINE_B = @BROMINE_B,"
            + " BROMINE_C = @BROMINE_C, BROMINE_D = @BROMINE_D,  CALCIUM_A = @CALCIUM_A,"
             + " CALCIUM_B = @CALCIUM_B, CALCIUM_C = @CALCIUM_C,  CALCIUM_D = @CALCIUM_D,"
            + " CALCIUM_E = @CALCIUM_E, CALCIUM_F = @CALCIUM_F,  CALCIUM_G = @CALCIUM_G,"
            + " CALCIUM_H = @CALCIUM_H, CALCIUM_I = @CALCIUM_I,  CALCIUM_J = @CALCIUM_J,"
            + " COMPRESSED_GAS_A = @COMPRESSED_GAS_A, COMPRESSED_GAS_B = @COMPRESSED_GAS_B,  COMPRESSED_GAS_C = @COMPRESSED_GAS_C,"
            + " COMPRESSED_GAS_D = @COMPRESSED_GAS_D, COMPRESSED_GAS_E = @COMPRESSED_GAS_E,  COMPRESSED_GAS_F = @COMPRESSED_GAS_F,"
            + " COMPRESSED_GAS_G = @COMPRESSED_GAS_G, HAZMAT_COORD_A = @HAZMAT_COORD_A,  HAZMAT_COORD_B = @HAZMAT_COORD_B,"
            + " HAZMAT_COORD_C = @HAZMAT_COORD_C, HAZMAT_COORD_D = @HAZMAT_COORD_D,  HAZMAT_SUPERVISOR_A = @HAZMAT_SUPERVISOR_A,"
            + " OTTO_FUEL_A = @OTTO_FUEL_A, OTTO_FUEL_B = @OTTO_FUEL_B,  OTTO_FUEL_C = @OTTO_FUEL_C,"
            + " OTTO_FUEL_D = @OTTO_FUEL_D, SPILL_TRAIN_A = @SPILL_TRAIN_A,  SPILL_TRAIN_B = @SPILL_TRAIN_B,"
            + " HAZMAT_STOWAGE_A = @HAZMAT_STOWAGE_A, HAZMAT_STOWAGE_B = @HAZMAT_STOWAGE_B,  HAZMAT_STOWAGE_C = @HAZMAT_STOWAGE_C,"
            + " HAZMAT_STOWAGE_D = @HAZMAT_STOWAGE_D, HAZMAT_STOWAGE_E = @HAZMAT_STOWAGE_E,  HAZMAT_STOWAGE_F = @HAZMAT_STOWAGE_F,"
            + " IN_USE_FLAM_A = @IN_USE_FLAM_A, IN_USE_FLAM_B = @IN_USE_FLAM_B,  HAZMAT_INVENTORY_A = @HAZMAT_INVENTORY_A,"
            + " LABELING_A = @LABELING_A, LABELING_B = @LABELING_B,  LABELING_C = @LABELING_C,"
            + " LOG_ENTRIES_A = @LOG_ENTRIES_A, LOG_ENTRIES_B = @LOG_ENTRIES_B,  LOG_ENTRIES_C = @LOG_ENTRIES_C,"
            + " LOG_ENTRIES_D = @LOG_ENTRIES_D, LOG_ENTRIES_E = @LOG_ENTRIES_E,  LOG_ENTRIES_F = @LOG_ENTRIES_F,"
            + " LOG_ENTRIES_G = @LOG_ENTRIES_G, ATM_CONTROL_REVIEW_A = @ATM_CONTROL_REVIEW_A,  ATM_CONTROL_REVIEW_B = @ATM_CONTROL_REVIEW_B,"
            + " ATM_CONTROL_REVIEW_C = @ATM_CONTROL_REVIEW_C, ATM_MATERIAL_CON_A = @ATM_MATERIAL_CON_A,  ATM_MATERIAL_CON_B = @ATM_MATERIAL_CON_B,"
            + " MSDS_A = @MSDS_A, MSDS_B = @MSDS_B,  MSDS_C = @MSDS_C,"
            + " MSDS_D = @MSDS_D, OPEN_PURCHASE_A = @OPEN_PURCHASE_A,  OPEN_PURCHASE_B = @OPEN_PURCHASE_B,"
            + " OPEN_PURCHASE_C = @OPEN_PURCHASE_C, OPEN_PURCHASE_D = @OPEN_PURCHASE_D,  SUBMARINE_MANAGEMENT_A = @SUBMARINE_MANAGEMENT_A,"
            + " SUBMARINE_MANAGEMENT_B = @SUBMARINE_MANAGEMENT_B, SUBMARINE_MANAGEMENT_C = @SUBMARINE_MANAGEMENT_C "
                + "where checklist_id=(SELECT MAX(checklist_id) from insurv_checklist)";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            cmd.Parameters.AddWithValue("@ATM_CON_TAGS_A", dbNull(checklist.ATM_CON_TAGS_A));
            cmd.Parameters.AddWithValue("@ATM_CON_TAGS_B", dbNull(checklist.ATM_CON_TAGS_B));
            cmd.Parameters.AddWithValue("@ATM_AUTHORIZATION_A", dbNull(checklist.ATM_AUTHORIZATION_A));
            cmd.Parameters.AddWithValue("@ATM_AUTHORIZATION_B", dbNull(checklist.ATM_AUTHORIZATION_B));
            cmd.Parameters.AddWithValue("@BROMINE_A", dbNull(checklist.BROMINE_A));
            cmd.Parameters.AddWithValue("@BROMINE_B", dbNull(checklist.BROMINE_B));
            cmd.Parameters.AddWithValue("@BROMINE_C", dbNull(checklist.BROMINE_C));
            cmd.Parameters.AddWithValue("@BROMINE_D", dbNull(checklist.BROMINE_D));
            cmd.Parameters.AddWithValue("@CALCIUM_A", dbNull(checklist.CALCIUM_A));
            cmd.Parameters.AddWithValue("@CALCIUM_B", dbNull(checklist.CALCIUM_B));
            cmd.Parameters.AddWithValue("@CALCIUM_C", dbNull(checklist.CALCIUM_C));
            cmd.Parameters.AddWithValue("@CALCIUM_D", dbNull(checklist.CALCIUM_D));
            cmd.Parameters.AddWithValue("@CALCIUM_E", dbNull(checklist.CALCIUM_E));
            cmd.Parameters.AddWithValue("@CALCIUM_F", dbNull(checklist.CALCIUM_F));
            cmd.Parameters.AddWithValue("@CALCIUM_G", dbNull(checklist.CALCIUM_G));
            cmd.Parameters.AddWithValue("@CALCIUM_H", dbNull(checklist.CALCIUM_H));
            cmd.Parameters.AddWithValue("@CALCIUM_I", dbNull(checklist.CALCIUM_I));
            cmd.Parameters.AddWithValue("@CALCIUM_J", dbNull(checklist.CALCIUM_J));
            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_A", dbNull(checklist.COMPRESSED_GAS_A));
            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_B", dbNull(checklist.COMPRESSED_GAS_B));
            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_C", dbNull(checklist.COMPRESSED_GAS_C));
            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_D", dbNull(checklist.COMPRESSED_GAS_D));

            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_E", dbNull(checklist.COMPRESSED_GAS_E));
            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_F", dbNull(checklist.COMPRESSED_GAS_F));
            cmd.Parameters.AddWithValue("@COMPRESSED_GAS_G", dbNull(checklist.COMPRESSED_GAS_G));
            cmd.Parameters.AddWithValue("@HAZMAT_COORD_A", dbNull(checklist.HAZMAT_COORD_A));
            cmd.Parameters.AddWithValue("@HAZMAT_COORD_B", dbNull(checklist.HAZMAT_COORD_B));
            cmd.Parameters.AddWithValue("@HAZMAT_COORD_C", dbNull(checklist.HAZMAT_COORD_C));
            cmd.Parameters.AddWithValue("@HAZMAT_COORD_D", dbNull(checklist.HAZMAT_COORD_D));
            cmd.Parameters.AddWithValue("@HAZMAT_SUPERVISOR_A", dbNull(checklist.HAZMAT_SUPERVISOR_A));
            cmd.Parameters.AddWithValue("@OTTO_FUEL_A", dbNull(checklist.OTTO_FUEL_A));
            cmd.Parameters.AddWithValue("@OTTO_FUEL_B", dbNull(checklist.OTTO_FUEL_B));
            cmd.Parameters.AddWithValue("@OTTO_FUEL_C", dbNull(checklist.OTTO_FUEL_C));
            cmd.Parameters.AddWithValue("@OTTO_FUEL_D", dbNull(checklist.OTTO_FUEL_D));
            cmd.Parameters.AddWithValue("@SPILL_TRAIN_A", dbNull(checklist.SPILL_TRAIN_A));
            cmd.Parameters.AddWithValue("@SPILL_TRAIN_B", dbNull(checklist.SPILL_TRAIN_B));
            cmd.Parameters.AddWithValue("@HAZMAT_STOWAGE_A", dbNull(checklist.HAZMAT_STOWAGE_A));
            cmd.Parameters.AddWithValue("@HAZMAT_STOWAGE_B", dbNull(checklist.HAZMAT_STOWAGE_B));
            cmd.Parameters.AddWithValue("@HAZMAT_STOWAGE_C", dbNull(checklist.HAZMAT_STOWAGE_C));
            cmd.Parameters.AddWithValue("@HAZMAT_STOWAGE_D", dbNull(checklist.HAZMAT_STOWAGE_D));
            cmd.Parameters.AddWithValue("@HAZMAT_STOWAGE_E", dbNull(checklist.HAZMAT_STOWAGE_E));
            cmd.Parameters.AddWithValue("@HAZMAT_STOWAGE_F", dbNull(checklist.HAZMAT_STOWAGE_F));

            cmd.Parameters.AddWithValue("@IN_USE_FLAM_A", dbNull(checklist.IN_USE_FLAM_A));
            cmd.Parameters.AddWithValue("@IN_USE_FLAM_B", dbNull(checklist.IN_USE_FLAM_B));
            cmd.Parameters.AddWithValue("@HAZMAT_INVENTORY_A", dbNull(checklist.HAZMAT_INVENTORY_A));
            cmd.Parameters.AddWithValue("@LABELING_A", dbNull(checklist.LABELING_A));
            cmd.Parameters.AddWithValue("@LABELING_B", dbNull(checklist.LABELING_B));
            cmd.Parameters.AddWithValue("@LABELING_C", dbNull(checklist.LABELING_C));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_A", dbNull(checklist.LOG_ENTRIES_A));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_B", dbNull(checklist.LOG_ENTRIES_B));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_C", dbNull(checklist.LOG_ENTRIES_C));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_D", dbNull(checklist.LOG_ENTRIES_D));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_E", dbNull(checklist.LOG_ENTRIES_E));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_F", dbNull(checklist.LOG_ENTRIES_F));
            cmd.Parameters.AddWithValue("@LOG_ENTRIES_G", dbNull(checklist.LOG_ENTRIES_G));
            cmd.Parameters.AddWithValue("@ATM_CONTROL_REVIEW_A", dbNull(checklist.ATM_CONTROL_REVIEW_A));
            cmd.Parameters.AddWithValue("@ATM_CONTROL_REVIEW_B", dbNull(checklist.ATM_CONTROL_REVIEW_B));
            cmd.Parameters.AddWithValue("@ATM_CONTROL_REVIEW_C", dbNull(checklist.ATM_CONTROL_REVIEW_C));
            cmd.Parameters.AddWithValue("@ATM_MATERIAL_CON_A", dbNull(checklist.ATM_MATERIAL_CON_A));
            cmd.Parameters.AddWithValue("@ATM_MATERIAL_CON_B", dbNull(checklist.ATM_MATERIAL_CON_B));
            cmd.Parameters.AddWithValue("@MSDS_A", dbNull(checklist.MSDS_A));

            cmd.Parameters.AddWithValue("@MSDS_B", dbNull(checklist.MSDS_B));
            cmd.Parameters.AddWithValue("@MSDS_C", dbNull(checklist.MSDS_C));
            cmd.Parameters.AddWithValue("@MSDS_D", dbNull(checklist.MSDS_D));
            cmd.Parameters.AddWithValue("@OPEN_PURCHASE_A", dbNull(checklist.OPEN_PURCHASE_A));
            cmd.Parameters.AddWithValue("@OPEN_PURCHASE_B", dbNull(checklist.OPEN_PURCHASE_B));
            cmd.Parameters.AddWithValue("@OPEN_PURCHASE_C", dbNull(checklist.OPEN_PURCHASE_C));
            cmd.Parameters.AddWithValue("@OPEN_PURCHASE_D", dbNull(checklist.OPEN_PURCHASE_D));
            cmd.Parameters.AddWithValue("@SUBMARINE_MANAGEMENT_A", dbNull(checklist.SUBMARINE_MANAGEMENT_A));
            cmd.Parameters.AddWithValue("@SUBMARINE_MANAGEMENT_B", dbNull(checklist.SUBMARINE_MANAGEMENT_B));
            cmd.Parameters.AddWithValue("@SUBMARINE_MANAGEMENT_C", dbNull(checklist.SUBMARINE_MANAGEMENT_C));
         
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            mdfConn.Close();
        }

        public void deleteTask(int taskid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            SqlCommand cmd = new SqlCommand("update decanting_tasks set active = @active where decanting_task_id = @taskid", mdfConn);
            cmd.Parameters.AddWithValue("@taskid", taskid);
            cmd.Parameters.AddWithValue("@active", false);



            cmd.ExecuteNonQuery();
            
            mdfConn.Close();
        }

        [CoverageExclude]
        public byte[] getPDFByTask(int taskid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            SqlCommand cmd = new SqlCommand("select pdf_file from labels l where l.task_id = @taskid", mdfConn);
            cmd.Parameters.AddWithValue("@taskid", taskid);


            object o = cmd.ExecuteScalar();

            byte[] pdfFile = (byte[])o;


            mdfConn.Close();

            return pdfFile;

        }


        public int getTotalDecantingInventoryByTask(int taskid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

           
            SqlCommand cmd = new SqlCommand("select count(*) from decanted_inventory d where d.decanting_task_id = @taskid", mdfConn);
            cmd.Parameters.AddWithValue("@taskid", taskid);


            object o = cmd.ExecuteScalar();

            int count = Convert.ToInt32(o);

            mdfConn.Close();

            return count;
        }


        public List<DecantingInfo> getDecantingInfoByTask(int niinCatalogId, int mfgCatalogId, string contractNumber, int msdsId, string invSerialNumber)
        {            

            List<DecantingInfo> infoList = new List<DecantingInfo>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            string stmt = "select l.label_info_id,l.COMPANY_CAGE_RP,l.COMPANY_NAME_RP,l.LABEL_EMERG_PHONE,l.LABEL_ITEM_NAME,l.LABEL_PROC_YEAR, " +
                          "l.LABEL_PROD_IDENT,l.LABEL_PROD_SERIALNO, l.LABEL_SIGNAL_WORD,l.LABEL_STOCK_NO,l.SPECIFIC_HAZARDS,l.msds_id, h.hcc, c.ct_po_box, c.ct_city, c.ct_state, c.ct_country " +

                           "from msds_label_info l, msds s, hcc h, msds_contractor_info c " +

                           "where " +
                           " l.msds_id = s.msds_id " +
                           "AND h.hcc_id = s.hcc_id " +
                           "AND s.msds_id = c.msds_id ";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);


            //find by the serial number if it exists.. only compare msdsserno
            if (!invSerialNumber.Equals(""))
            {
                stmt += "AND s.msdsserno = @msds ";
                cmd.CommandText = stmt;
                cmd.Parameters.AddWithValue("@msds", dbNull(invSerialNumber));
            }

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {                
                DecantingInfo info = new DecantingInfo();               

                info.Cage = rdr["company_cage_rp"].ToString();
                info.CompanyName = rdr["company_name_rp"].ToString();
                info.Phone = rdr["label_emerg_phone"].ToString();
                info.ItemName = rdr["label_item_name"].ToString();
                info.Year = rdr["label_proc_year"].ToString();
                info.ProductIdentity = rdr["label_prod_ident"].ToString();
                info.ProductSerial = rdr["label_prod_serialno"].ToString();
                info.SignalWord = rdr["label_signal_word"].ToString();
                info.StockNumber = rdr["label_stock_no"].ToString();
                info.Hazards = rdr["specific_hazards"].ToString();
                info.MsdsId = Convert.ToInt32(rdr["msds_id"].ToString());
                info.City = rdr["ct_city"].ToString();
                info.State = rdr["ct_state"].ToString();
                info.Country = rdr["ct_country"].ToString();
                info.PoBox = rdr["ct_po_box"].ToString();
                info.Hcc = rdr["hcc"].ToString();

                infoList.Add(info);

            }

           
            mdfConn.Close();

            return infoList;
        }


        public void saveFeedback(string feedback, string page, string user)
        {
            DateTime now = DateTime.Now;

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string insertStmt = "insert into feedback (feedback, page_name,date_submitted, username) VALUES (@feedback, @page_name,@date_submitted, @username)";

            SqlCommand cmd = new SqlCommand(insertStmt, con);

            cmd.Parameters.AddWithValue("@feedback", feedback);
            cmd.Parameters.AddWithValue("@page_name", page);
            cmd.Parameters.AddWithValue("@username", user);
            cmd.Parameters.AddWithValue("@date_submitted", now);

            cmd.ExecuteNonQuery();

            con.Close();
        }


        public void insertDecantingHandheldData(string databaseLocation, int taskid)
        {

            //connect to database which was copied from the template.  the temp file will be deleted after db is saved to server database
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert niin_catalog for decanting - based on the selected task
            try
            {
             //   insertHHDecantingNiinCatalogByTask(databaseLocation, tran, liteCon, taskid);
                insertHHNiinCatalogForDecanting(databaseLocation, tran, liteCon, taskid);
           }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into niin_catalog : " + ex.Message);
            }

            //insert mfg_catalog
            try
            {
               // insertHHDecantingMfgCatalogByTask(databaseLocation, tran, liteCon, taskid);
                insertHHMfgCatalogForDecanting(databaseLocation, tran, liteCon, taskid);
              }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into mfg_catalog : " + ex.Message);
            }

            //insert inventory_in_decant_task
            try
            {
                insertHHInventoryInDecantTaskByTask(databaseLocation, tran, liteCon, taskid);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into inventory_in_decant_task : " + ex.Message);
            } 
  ////insert locations
            //try
            try
            {
                insertHHHazardWarningsForReceiving(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazard warnings : " + ex.Message);
            }

            //insert hazards
            try
            {
                insertHHHazardsForReceiving(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazards : " + ex.Message);
            }


            //insert offload_CR
            try
            {
                insertHHShelfLifeCodeForReceiving(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into shelf_life_code : " + ex.Message);
            }

           
            try
            {
                insertHHConfigForDecanting(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into config (decanting) : " + ex.Message);

            }


            //insert locations
            try
            {
                insertHHDecantingLocations(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into location (decanting) : " + ex.Message);

            }

            try
            {
                insertHHAtmosphereControl(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into atmosphere_control : " + ex.Message);
            }
            

    try
            {
        
                insertHHDecantingInventory(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into inventory : " + ex.Message);
            }


        try
        {

            insertHHDecantingHazardousHCC(databaseLocation, tran, liteCon);
        }
        catch (Exception ex)
        {
            liteCon.Close();
            throw new Exception("Error Inserting into hazardous_hcc : " + ex.Message);
        }


            tran.Commit();
            liteCon.Close();


        }

        //insertHHDecantingHazardousHCC
        public void insertHHInsurvNiinCatalog(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into niin_catalog (niin_catalog_id,fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,shelf_life_code_id,shelf_life_action_code_id, remarks,storage_type_id,cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,allowance_qty,created,ship_id) " +
                              "VALUES (@niin_catalog_id,@fsc,@niin,@ui,@um,@usage_category_id,@description,@smcc_id,@specs,@shelf_life_code_id,@shelf_life_action_code_id, @remarks,@storage_type_id,@cog_id,@spmig,@nehc_rpt,@catalog_group_id,@catalog_serial_number,@allowance_qty,@created,@ship_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from niin_catalog n", mdfConn);            
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                object niin_catalog_id = rdr["niin_catalog_id"].ToString();
                object fsc = rdr["fsc"].ToString();
                string niin = rdr["niin"].ToString();
                string ui = rdr["ui"].ToString();
                string um = rdr["um"].ToString();
                object usage_category_id = rdr["usage_category_id"];
                string description = rdr["description"].ToString();
                object smcc_id = rdr["smcc_id"];
                string specs = rdr["specs"].ToString();
                object shelf_life_code_id = rdr["shelf_life_code_id"];
                object shelf_life_action_code_id = rdr["shelf_life_action_code_id"];
                string remarks = rdr["remarks"].ToString();
                object storage_type_id = rdr["storage_type_id"];
                object cog_id = rdr["cog_id"];
                string spmig = rdr["spmig"].ToString();
                string nehc_rpt = rdr["nehc_rpt"].ToString();
                object catalog_group_id = rdr["catalog_group_id"];
                string catalog_serial_number = rdr["catalog_serial_number"].ToString();
                object allowance_qty = rdr["allowance_qty"];

                DateTime created = DateTime.Now;
                if (rdr["created"].ToString() != "")
                {
                    created = Convert.ToDateTime(rdr["created"]);
                }

                int ship_id = 0;

                try
                {
                    ship_id = Convert.ToInt32(rdr["ship_id"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                liteCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                liteCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@um", dbNull(um));
                liteCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                liteCmd.Parameters.AddWithValue("@description", dbNull(description));
                liteCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                liteCmd.Parameters.AddWithValue("@specs", dbNull(specs));
                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                liteCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                liteCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));
                liteCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                liteCmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
                liteCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                liteCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));
                liteCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                liteCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                liteCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                liteCmd.Parameters.AddWithValue("@created", dbNull(created));
                liteCmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
        }



        public void insertHHInsurvUsageCategory(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into usage_category (usage_category_id,category,description) " +
                              "VALUES (@usage_category_id,@category,@description)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from usage_category u", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int usageCatId = Convert.ToInt32(rdr["usage_category_id"].ToString());
                string category = Convert.ToString(rdr["category"]);
                string desc = Convert.ToString(rdr["description"]);

                liteCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usageCatId));
                liteCmd.Parameters.AddWithValue("@category", dbNull(category));
                liteCmd.Parameters.AddWithValue("@description", dbNull(desc));
               
                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
        }



        public void insertHHInsurvMfgCatalog(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into mfg_catalog (mfg_catalog_id,cage,manufacturer,niin_catalog_id) " +
                              "VALUES (@mfg_catalog_id,@cage,@manufacturer,@niin_catalog_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from mfg_catalog m", mdfConn);
            
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                string cage = Convert.ToString(rdr["cage"]);
                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);


                string manufacturer = "";
                int niin_catalog_id = 0;

                try
                {
                    manufacturer = Convert.ToString(rdr["manufacturer"]);
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}


                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                liteCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHInsurvLocations(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {
            
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            SqlCommand cmd = new SqlCommand("select * from locations where name != @name", mdfConn);
            cmd.Parameters.AddWithValue("@name", "NEWLY ISSUED");
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into locations (location_id,name,workcenter_id) VALUES (@location_id,@name,@workcenter_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"].ToString());
                string name = rdr["name"].ToString();
                string wid = rdr["workcenter_id"].ToString();

                liteCmd.Parameters.AddWithValue("@name", name);
                liteCmd.Parameters.AddWithValue("@location_id", location_id);
                liteCmd.Parameters.AddWithValue("@workcenter_id", wid);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHInsurvNiin(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            SqlCommand cmd = new SqlCommand("select * from insurv_niin", mdfConn);            
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into insurv_niin (insurv_id, surv_name,niin_catalog_id) VALUES (@insurv_id,@name,@catid)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();
                
                string name = rdr["surv_name"].ToString();
                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                int insurv_id = Convert.ToInt32(rdr["insurv_id"]);
                liteCmd.Parameters.AddWithValue("@name", name);
                liteCmd.Parameters.AddWithValue("@catid", niin_catalog_id);
                liteCmd.Parameters.AddWithValue("@insurv_id", insurv_id);
               
                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        private void insertHHConfigForInsurv(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            DateTime now = DateTime.Now.ToUniversalTime();

            //sqlite connection and statement
            string liteStmt = "insert into config (inventory_audit,decant,offload,receive,insurv,checkout_time) VALUES (@inventory_audit,@decant,@offload,@receive,@insurv,@checkout_time)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            liteCmd.Parameters.AddWithValue("@inventory_audit", false);
            liteCmd.Parameters.AddWithValue("@decant", false);
            liteCmd.Parameters.AddWithValue("@offload", false);
            liteCmd.Parameters.AddWithValue("@receive", false);
            liteCmd.Parameters.AddWithValue("@insurv", true);
            liteCmd.Parameters.AddWithValue("@checkout_time", now);

            liteCmd.ExecuteNonQuery();


            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHInsurvInventory(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

          
            //sqlite connection and statement
            string liteStmt = "insert into inventory (inventory_id,niin_cat_id,mfg_cat_id,location_id,workcenter_id,qty,ui,shelf_life_expiration_date) " +
                              "VALUES (@inventory_id,@niin_cat_id,@mfg_cat_id,@location_id,@workcenter_id,@qty,@ui,@shelf_life_expiration_date)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select i.alternate_ui as alternate_ui, i.mfg_catalog_id,  i.shelf_life_expiration_date, " +
                            "i.qty, i.location_id, i.inventory_id, n.niin_catalog_id, l.workcenter_id, n.ui as ui " +

                            "from inventory i, niin_catalog n, mfg_catalog m, locations l " +

                            "where l.location_id = i.location_id " +
                            "AND i.mfg_catalog_id = m.mfg_catalog_id " +
                            "AND n.niin_catalog_id = m.niin_catalog_id";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {                
                liteCmd.Parameters.Clear();



                int? mfg_catalog_id = null;

                try
                {
                    mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}

                DateTime? shelfLifeExpirationDate = null;

                try
                {
                    shelfLifeExpirationDate = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}


                int? location_id = null;

                try
                {

                    location_id = Convert.ToInt32(rdr["location_id"]);
                }
                catch {}

                int? qty = null;

                try
                {
                    qty = Convert.ToInt32(rdr["qty"]);
                }
                catch {}

                int? niin_catalog_id = null;

                try
                {
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}

                int? inventoryId = null;

                try
                {
                    inventoryId = Convert.ToInt32(rdr["inventory_id"]);
                }
                catch {}

                int? workcenterId = null;

                try
                {
                    workcenterId = Convert.ToInt32(rdr["workcenter_id"]);
                }
                catch {}
                string alternateUi = null;

                try
                {
                    alternateUi = Convert.ToString(rdr["alternate_ui"]);
                }
                catch {}


                string ui = null;

                try
                {
                    ui = Convert.ToString(rdr["ui"]);
                }
                catch {}

                if (alternateUi != null && !alternateUi.Equals(""))
                {
                    ui = alternateUi;

                    //make the UI value uppercase before sending to the handheld
                    ui = ui.ToUpper();
                }



                liteCmd.Parameters.AddWithValue("@mfg_cat_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@inventory_id", dbNull(inventoryId));
                liteCmd.Parameters.AddWithValue("@niin_cat_id", dbNull(niin_catalog_id));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenterId));
                liteCmd.Parameters.AddWithValue("@qty", dbNull(qty));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelfLifeExpirationDate));


                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();





        }



        public void insertHHDecantingHazardousHCC(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into hazardous_hcc (hazardous_hcc_id, hazard_id, hcc) " +
                              "VALUES (@hazardous_hcc_id, @hazard_id, @hcc)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select * from hazardous_hcc";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();



                int? hazardousHccId = null;

                try
                {
                    hazardousHccId = Convert.ToInt32(rdr["hazardous_hcc_id"]);
                }
                catch {}

                int? hazard_id = null;

                try
                {
                    hazard_id = Convert.ToInt32(rdr["hazard_id"]);
                }
                catch {}


                string hcc = null;

                try
                {

                    hcc = Convert.ToString(rdr["hcc"]);
                }
                catch {}




                liteCmd.Parameters.AddWithValue("@hazardous_hcc_id", dbNull(hazardousHccId));
                liteCmd.Parameters.AddWithValue("@hazard_id", dbNull(hazard_id));
                liteCmd.Parameters.AddWithValue("@hcc", dbNull(hcc));               


                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();





        }


        public void insertHHDecantingInventory(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            List<int> invList = new List<int>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into inventory (inventory_id,niin_cat_id,mfg_cat_id,location_id,workcenter_id,qty,ui,shelf_life_expiration_date, hcc, msds_serial_no, atmosphere_control_number) " +
                              "VALUES (@inventory_id,@niin_cat_id,@mfg_cat_id,@location_id,@workcenter_id,@qty,@ui,@shelf_life_expiration_date, @hcc, @msds_serial_no, @atmosphere_control_number)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select distinct v.hcc, i.alternate_ui as alternate_ui, i.mfg_catalog_id,  i.shelf_life_expiration_date, " +
                            "i.qty, i.location_id, i.inventory_id, n.niin_catalog_id, l.workcenter_id, i.serial_number, n.ui as ui, i.atm as atm " +

                            "from niin_catalog n, mfg_catalog m, locations l, inventory i " +
                            "join v_top_hazard_items v on v.inventory_id = i.inventory_id " +

                            "where l.location_id = i.location_id " +
                            "AND i.mfg_catalog_id = m.mfg_catalog_id " +
                            "AND n.niin_catalog_id = m.niin_catalog_id";


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();



                int? mfg_catalog_id = null;

                try
                {
                    mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}

                DateTime? shelfLifeExpirationDate = null;

                try
                {
                    shelfLifeExpirationDate = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}


                int? location_id = null;

                try
                {

                    location_id = Convert.ToInt32(rdr["location_id"]);
                }
                catch {}

                int? qty = null;

                try
                {
                    qty = Convert.ToInt32(rdr["qty"]);
                }
                catch {}

                int? niin_catalog_id = null;

                try
                {
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}

                int? inventoryId = null;

                try
                {
                    inventoryId = Convert.ToInt32(rdr["inventory_id"]);
                }
                catch {}

                int? workcenterId = null;

                try
                {
                    workcenterId = Convert.ToInt32(rdr["workcenter_id"]);
                }
                catch {}
                string alternateUi = null;

                try
                {
                    alternateUi = Convert.ToString(rdr["alternate_ui"]);
                }
                catch {}


                string ui = null;

                try
                {
                    ui = Convert.ToString(rdr["ui"]);
                }
                catch {}

                string hcc = null;

                try
                {
                    hcc = Convert.ToString(rdr["hcc"]);
                    hcc = hcc.ToUpper();
                }
                catch {}

                string serialNumber = null;

                try
                {
                    serialNumber = Convert.ToString(rdr["serial_number"]);
                    serialNumber = serialNumber.ToUpper();
                }
                catch {}

                if (alternateUi != null && !alternateUi.Equals(""))
                {
                    ui = alternateUi;

                    //make the UI value uppercase before sending to the handheld
                    ui = ui.ToUpper();
                }

                string atmosphere_control_number = null;

                try
                {
                    atmosphere_control_number = Convert.ToString(rdr["atm"]);                   
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@mfg_cat_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@inventory_id", dbNull(inventoryId));
                liteCmd.Parameters.AddWithValue("@niin_cat_id", dbNull(niin_catalog_id));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenterId));
                liteCmd.Parameters.AddWithValue("@qty", dbNull(qty));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelfLifeExpirationDate));
                liteCmd.Parameters.AddWithValue("@hcc", dbNull(hcc));
                liteCmd.Parameters.AddWithValue("@msds_serial_no", dbNull(serialNumber));
                liteCmd.Parameters.AddWithValue("@atmosphere_control_number", dbNull(atmosphere_control_number));


                if (!invList.Contains((int)inventoryId))
                {
                    liteCmd.ExecuteNonQuery();
                    invList.Add((int)inventoryId);
                }

                

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();





        }

        [CoverageExclude]
        private void insertInsurvHandheldData(string databaseLocation)
        {

            //connect to database which was copied from the template.  the temp file will be deleted after db is saved to server database
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert inventory
            try
            {
                insertHHInsurvInventory(databaseLocation,tran,liteCon);
            }
            catch(Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into inventory : " + ex.Message);
            }


            //insert insurv niin_catalog
            try
            {
                insertHHInsurvNiinCatalog(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into niin_catalog : " + ex.Message);
            }

            //insert mfg_catalog
            try
            {
                insertHHInsurvMfgCatalog(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into mfg_catalog : " + ex.Message);
            }

           
            //insert config
            try
            {
                insertHHConfigForInsurv(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into config (Insurv) : " + ex.Message);

            }


            //insert locations
            try
            {
                insertHHInsurvLocations(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into location (Insurv) : " + ex.Message);

            }


            //insert workcenters
            try
            {
                insertHHWorkCenters(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into workcenters (Insurv) : " + ex.Message);

            }
          
            //insert insurv_niin
            try
            {
                insertHHInsurvNiin(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into insurv_niin : " + ex.Message);

            }

            //insert usage category
            try
            {
                insertHHInsurvUsageCategory(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {

                liteCon.Close();
                throw new Exception("Error Inserting into usage_category : " + ex.Message);

            }


            try
            {
                insertHHAtmosphereControl(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into atmosphere_control : " + ex.Message);
            }


            tran.Commit();
            liteCon.Close();


        }


        public Hashtable createInsurvClipboard(string dbLocation, string username)
        {          

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();

            Hashtable hashTable = new Hashtable();

            //create tempFile to copy the template to - 
            string tempFileLocation = Path.GetTempFileName();

            //copy template
            File.Copy(dbLocation, tempFileLocation, true);

            try
            {
                insertInsurvHandheldData(tempFileLocation);
            }
            catch (Exception ex)
            {
                File.Delete(tempFileLocation);
                throw new Exception(ex.Message);
            }
            //database has the data inserted at this point.  save the handheld database to the server database - pass to the user to download

            FileStream fs = new FileStream(tempFileLocation, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];

            // read in the file stream to the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            // close the file stream
            fs.Close();

            string selectStmt = "insert into insurv_clipboard (date_created, clip_file, username) VALUES (@date_created,@clip,@name);  SELECT SCOPE_IDENTITY()";
           

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Transaction = tran;

            string time = DateTime.Now.ToShortDateString();

            cmd.Parameters.AddWithValue("@name", username);           
            cmd.Parameters.AddWithValue("@date_created", time);
            cmd.Parameters.AddWithValue("@clip", fileData);
            cmd.ExecuteScalar();

            
            tran.Commit();

            mdfConn.Close();

            //delete the temporary file - it is no longer needed
            File.Delete(tempFileLocation);

            hashTable.Add("file", fileData);

            return hashTable;

        }

        


        public Hashtable createDecantingClipboard(int taskid, string dbLocation)
        {

            Hashtable table = new Hashtable();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();


            //create tempFile to copy the template to - 
            string tempFileLocation = Path.GetTempFileName();

            //copy template
            File.Copy(dbLocation, tempFileLocation, true);



            try
            {
                insertDecantingHandheldData(tempFileLocation, taskid);
            }
            catch (Exception ex)
            {
                File.Delete(tempFileLocation);
                throw new Exception(ex.Message);
            }
            //database has the data inserted at this point.  save the handheld database to the server database - pass to the user to download

            FileStream fs = new FileStream(tempFileLocation, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];

            // read in the file stream to the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            // close the file stream
            fs.Close();

            string selectStmt = "insert into decanting_clipboard (name, deleted, date_created, clip_file, task_id) VALUES (@name,@deleted,@date_created,@clip,@task);  SELECT SCOPE_IDENTITY()";

            string descripton = getTaskDescription(taskid);

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Transaction = tran;

            string time = DateTime.Now.ToShortDateString();

            cmd.Parameters.AddWithValue("@name", "clip_" + descripton);
            cmd.Parameters.AddWithValue("@deleted", false);
            cmd.Parameters.AddWithValue("@date_created", time);
            cmd.Parameters.AddWithValue("@clip", fileData);
            cmd.Parameters.AddWithValue("@task", taskid);
            
            object o = cmd.ExecuteScalar();

            int clipid = Convert.ToInt32(o);

            tran.Commit();

            mdfConn.Close();

            //delete the temporary file - it is no longer needed
            File.Delete(tempFileLocation);

            table.Add("file", fileData);
            table.Add("clipid", clipid);

            return table;

        }


        public DataTable getDecantedInventoryForInvInDecantTask(int inventory_in_decant_task_id)
        {


            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select d.*, i.*, m.*, l.name, l.description as workcenter, u.category as usage_category, m.manually_entered from decanted_inventory d INNER JOIN "
                + "inventory_in_decant_task i ON (d.inventory_in_decant_task_id=i.inventory_in_decant_task_id) INNER JOIN vMfgCatNiinCat m ON(i.mfg_catalog_id=m.mfg_catalog_id)"
                + " INNER JOIN usage_category u ON (m.usage_category_id=u.usage_category_id) INNER JOIN vLocationWorkcenters l ON (d.location_id=l.location_id)"
                + " where d.inventory_in_decant_task_id = @inventory_in_decant_task_id";
            
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_in_decant_task_id", inventory_in_decant_task_id);


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }


        public string getInventorySerialByInvId(int inventoryId)
        {


            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.serial_number from Inventory i where i.inventory_id = @inventory_id";            

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_id", inventoryId);

            object o = cmd.ExecuteScalar();

            string serialNumber = "";

            try
            {
                serialNumber = Convert.ToString(o);
            }
            catch {}


            conn.Close();

            return serialNumber;
        }


        public DataTable getInventoryForDecantTask(int taskid)
        {


            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.*, m.*, u.category as usage_category, m.manually_entered from inventory_in_decant_task i INNER JOIN vMfgCatNiinCat m ON(i.mfg_catalog_id=m.mfg_catalog_id)"
                + " LEFT OUTER JOIN usage_category u ON (m.usage_category_id=u.usage_category_id) "
                +" where decanting_task_id = @taskid " +                
                "ORDER BY m.niin_catalog_id, m.mfg_catalog_id, i.shelf_life_expiration_date, i.alternate_ui, i.alternate_um";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@taskid", taskid);            


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public void insertIntoDecantedInventoryFromDecantNew(List<DecantingInfo> decantedInfo, int taskid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "insert into inventory_in_decant_task(shelf_life_expiration_date, manufacturer_date, location_id,"
                +" qty, mfg_catalog_id, alternate_ui, alternate_um, contract_number, batch_number, lot_number, "
                +"decant_container_type, decant_container_volume, decanting_task_id, inventory_id, ati, cosal, atm) VALUES "
                + "(@shelf_life_expiration_date, @manufacturer_date, @location_id,"
                + " @qty, @mfg_catalog_id, @alternate_ui, @alternate_um, @contract_number, @batch_number, @lot_number, "
                + "@decant_container_type, @decant_container_volume, @decanting_task_id, @inventory_id, @ati, @cosal, @atm)";



            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

            SqlTransaction tran = mdfConn.BeginTransaction();
            cmd.Transaction = tran;

            
            foreach (DecantingInfo info in decantedInfo)
            {

                int inventory_id = info.inventory_id;
                DataTable dt = findInventory(inventory_id);
                if(dt.Rows.Count==1)
                {
                     DataRow data = dt.Rows[0];
                                     
                    cmd.Parameters.Clear();

                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(data["shelf_life_expiration_date"]));
                    cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(data["manufacturer_date"]));
                    cmd.Parameters.AddWithValue("@location_id", dbNull(data["location_id"]));
                    cmd.Parameters.AddWithValue("@qty", dbNull(data["qty"]));
                    cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(data["mfg_catalog_id"]));
                    cmd.Parameters.AddWithValue("@alternate_ui", dbNull(data["alternate_ui"]));
                    cmd.Parameters.AddWithValue("@alternate_um", dbNull(data["alternate_um"]));
                    cmd.Parameters.AddWithValue("@contract_number", dbNull(data["contract_number"]));
                    cmd.Parameters.AddWithValue("@batch_number", dbNull(data["batch_number"]));
                    cmd.Parameters.AddWithValue("@lot_number", dbNull(data["lot_number"]));
                    cmd.Parameters.AddWithValue("@ati", dbNull(data["ati"]));
                    cmd.Parameters.AddWithValue("@decant_container_type", dbNull(info.ContainerType.ToUpper()));
                    cmd.Parameters.AddWithValue("@decant_container_volume", dbNull(info.Volume.ToUpper()));
                    cmd.Parameters.AddWithValue("@decanting_task_id", dbNull(taskid));
                    cmd.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));

                    cmd.Parameters.AddWithValue("@cosal", dbNull(data["cosal"]));
                    cmd.Parameters.AddWithValue("@atm", dbNull(data["atm"]));
                    cmd.ExecuteNonQuery();
                   
                }
                
            }

            tran.Commit();

            cmd.Dispose();
            mdfConn.Close();
            
        }


        public List<int> insertDecantedInventoryFromWebDecanting(List<DecantInventoryData> decantedInfo, int inventory_in_decant_task_id, int qty, int location_id)
        {
            List<int> ids = new List<int>();
            int decanted_inventory_id = 0;
            int inventory_id = 0;

            ////get data
            //DecantInventoryData data = decantedInfo[0];

            DataTable dt = findInventoryInDecantTask(inventory_in_decant_task_id);
            DataRow invTask = dt.Rows[0];

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
        
            mdfConn.Open();
           
            SqlCommand cmd = new SqlCommand("insert into decanted_inventory (location_id, inventory_in_decant_task_id, qty, action_complete)" +
                "VALUES (@location_id, @inventory_in_decant_task_id, @qty, @action_complete); SELECT SCOPE_IDENTITY()", mdfConn);

            SqlTransaction tran = mdfConn.BeginTransaction();
            cmd.Transaction = tran;

            DateTime action_complete = DateTime.Now;
           
                
                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                cmd.Parameters.AddWithValue("@inventory_in_decant_task_id", dbNull(inventory_in_decant_task_id));
                cmd.Parameters.AddWithValue("@qty", dbNull(qty));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));

                try
                {
                    object o = cmd.ExecuteScalar();

                    decanted_inventory_id = Convert.ToInt32(o);
                }
                catch (Exception ex)
                {
                    mdfConn.Close();
                    throw new Exception(ex.Message);                   
                }

                string invStmt = "insert into inventory (shelf_life_expiration_date, location_id, qty, serial_number, "
                    +"bulk_item, mfg_catalog_id, manufacturer_date, ati, alternate_ui, batch_number, lot_number, "
                    +"contract_number, alternate_um, cosal, atm) VALUES "+
                    "(@shelf_life_expiration_date, @location_id, @qty, @serial_number, "
                    + "@bulk_item, @mfg_catalog_id, @manufacturer_date, @ati, @alternate_ui, @batch_number, @lot_number, "
                    + "@contract_number, @alternate_um, @cosal, @atm); SELECT SCOPE_IDENTITY()";

                SqlCommand cmd2 = new SqlCommand(invStmt, mdfConn);
                cmd2.Transaction = tran;

                cmd2.Parameters.AddWithValue("@location_id", dbNull(location_id));
                cmd2.Parameters.AddWithValue("@qty", dbNull(qty));

                cmd2.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(invTask["shelf_life_expiration_date"]));
                cmd2.Parameters.AddWithValue("@serial_number", dbNull(invTask["serial_number"]));
                cmd2.Parameters.AddWithValue("@bulk_item", dbNull(true));
                cmd2.Parameters.AddWithValue("@mfg_catalog_id", dbNull(invTask["mfg_catalog_id"]));
                cmd2.Parameters.AddWithValue("@manufacturer_date", dbNull(invTask["manufacturer_date"]));
                cmd2.Parameters.AddWithValue("@ati", dbNull(invTask["ati"]));
                cmd2.Parameters.AddWithValue("@alternate_ui", dbNull(invTask["decant_container_type"]));
                cmd2.Parameters.AddWithValue("@batch_number", dbNull(invTask["batch_number"]));
                cmd2.Parameters.AddWithValue("@lot_number", dbNull(invTask["lot_number"]));
                cmd2.Parameters.AddWithValue("@contract_number", dbNull(invTask["contract_number"]));
                cmd2.Parameters.AddWithValue("@alternate_um", dbNull(invTask["decant_container_volume"]));

                cmd2.Parameters.AddWithValue("@cosal", dbNull(invTask["cosal"]));
                cmd2.Parameters.AddWithValue("@atm", dbNull(invTask["atm"]));

                try
                {
                    object o = cmd2.ExecuteScalar();

                    inventory_id = Convert.ToInt32(o);
                }
                catch (Exception ex)
                {
                    mdfConn.Close();
                    throw new Exception(ex.Message);
                }


            tran.Commit();

            cmd.Dispose();
            mdfConn.Close();

            ids.Add(decanted_inventory_id);
            ids.Add(inventory_id);

            return ids;

        }


        public int createDecantingTask(string description)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("insert into decanting_tasks (description, date, active) VALUES (@description,@date,@active); SELECT SCOPE_IDENTITY()", mdfConn);

            SqlTransaction tran = mdfConn.BeginTransaction();
            cmd.Transaction = tran;

            string time = DateTime.Now.ToShortDateString();            
                
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@date", time);
            cmd.Parameters.AddWithValue("@active", true);

            object o = cmd.ExecuteScalar();

            int taskid = Convert.ToInt32(o);

            tran.Commit();

            cmd.Dispose();
            mdfConn.Close();

            return taskid;

        }


        public DataTable getTasksByUser(string username)
        {

            string stmt = "select distinct d.* from decanting_tasks d, inventory_in_decant_task i " +
                           "where d.decanting_task_id = i.decanting_task_id " +
                           "AND d.active = @active";

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

            cmd.Parameters.AddWithValue("@active", true);
            
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            mdfConn.Close();

            return dt;


        }

        public DataTable getTasks()
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select * from decanting_tasks where active = @active", mdfConn);                       

            cmd.Parameters.AddWithValue("@active", true);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            mdfConn.Close();

            return dt;

        }


        public void insertAuditsInRecurring(int auditId, int recurringId)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();
            //insert audits_in_recurring
            string auditInRecurStmt = "insert into audits_in_recurring (inventory_audit_id, recurring_id) VALUES (@inventory_audit_id,@recurring_id)";
            SqlCommand command = new SqlCommand(auditInRecurStmt, con);            

            command.Parameters.AddWithValue("@inventory_audit_id", auditId);
            command.Parameters.AddWithValue("@recurring_id", recurringId);

            command.ExecuteNonQuery();

            con.Close();

        }

        public int getStatusIdByName(string name)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select status_id from ship_statuses where description = @description",mdfConn);
            cmd.Parameters.AddWithValue("@description", name);
        

            long i = (long)cmd.ExecuteScalar();

            int id = Convert.ToInt32(i);

            cmd.Dispose();
            mdfConn.Close();

            return id;
        }


        public string getrecurringByID(int recurringid)
        {


            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select recurring from recurring_audits where recurring_audit_id = @recurid", mdfConn);
            cmd.Parameters.AddWithValue("@recurid", recurringid);

            object o = cmd.ExecuteScalar();

            if (o == null)
            {

                cmd.Dispose();
                mdfConn.Close();

                return null;

            }
            else
            {

                cmd.Dispose();
                mdfConn.Close();

                string recur = o.ToString();
                return recur;

            }

        }

        public int getRecurringID(int auditID)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select distinct recurring_id from audits_in_recurring where inventory_audit_id = @auditid";
            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            cmd.Parameters.AddWithValue("@auditid", auditID);

            object o = cmd.ExecuteScalar();

            if (o == null)
            {

                cmd.Dispose();
                mdfConn.Close();

                return 0;
            }
            else
            {

                //recurring id
                int reoccuring_id = Convert.ToInt32(o);

                cmd.Dispose();
                mdfConn.Close();

                return reoccuring_id;

            }


        }

        public void createNewAuditFromRecurring(string description, string status, DateTime selectedDate, DataTable locationsTable, int recurring_id, bool editRecurringAudit, string recur, bool chkBoxChecked)
        {

            int statusid = getStatusIdByName(status);

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "insert into inventory_audits(scheduled_on, created, description, ship_status_id, deleted) VALUES (@scheduled,@created, @description, @status, @flag); SELECT SCOPE_IDENTITY()";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Transaction = tran;

            DateTime date = DateTime.Now;

            cmd.Parameters.AddWithValue("@scheduled", selectedDate);
            cmd.Parameters.AddWithValue("@created", date);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@status", statusid);
            cmd.Parameters.AddWithValue("@flag", false);

            object o = cmd.ExecuteScalar();

            int inventory_audit_id = Convert.ToInt32(o);

            string stmt1 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 1);";
            SqlCommand locationCR1Cmd = new SqlCommand(stmt1, mdfConn);

            locationCR1Cmd.Transaction = tran;
            DateTime time = DateTime.Now;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR1Cmd.Parameters.Clear();

                locationCR1Cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                locationCR1Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR1Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR1Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR1Cmd.ExecuteNonQuery();
            }

            string stmt2 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 2);";
            SqlCommand locationCR2Cmd = new SqlCommand(stmt2, mdfConn);

            locationCR2Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR2Cmd.Parameters.Clear();

                locationCR2Cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                locationCR2Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR2Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR2Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR2Cmd.ExecuteNonQuery();
            }

            string stmt3 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 3);";
            SqlCommand locationCR3Cmd = new SqlCommand(stmt3, mdfConn);

            locationCR3Cmd.Transaction = tran;
            

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR3Cmd.Parameters.Clear();

                locationCR3Cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                locationCR3Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR3Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR3Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR3Cmd.ExecuteNonQuery();
            }



            string stmt = "insert into locations_in_audit (inventory_audit_id, location_id) Values (@auditid, @location_id)";
            SqlCommand locationCmd = new SqlCommand(stmt, mdfConn);

            locationCmd.Transaction = tran;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCmd.Parameters.Clear();

                locationCmd.Parameters.AddWithValue("@auditid", inventory_audit_id);
                locationCmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));

                locationCmd.ExecuteNonQuery();
            }

            //user wants to edit the recurring audit information too
            if (editRecurringAudit)
            {
                //user still wants a recurring audit, so we'll update the information
                if (chkBoxChecked)
                {
                    //update location information for the recurring audit
                    updateRecurringLocationsByID(recurring_id, tran, mdfConn, locationsTable);

                    //update recurring information (description, etc.)
                    updateRecurringInfoByID(recurring_id, tran, mdfConn, selectedDate, description, statusid, recur);
                }

                //user wants to delete the recurring audit - check box is not checked
                else
                {
                    //delete audits in recurring where recurring id = @recurring id
                    deleteAuditsInRecurringById(recurring_id, tran, mdfConn);

                    //delete recurring_audits where recurring_id = @recurring_id
                    deleteRecurringAuditById(recurring_id, tran, mdfConn);

                }

                

            }

            
            //commit all the work
            tran.Commit();

            locationCmd.Dispose();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void deleteAuditsInRecurringById(int recurring_id, SqlTransaction tran, SqlConnection con)
        {          

            SqlCommand cmd = new SqlCommand("delete from audits_in_recurring where recurring_id = @recurring", con);
            cmd.Transaction = tran;
            cmd.Parameters.AddWithValue("@recurring", recurring_id);

            cmd.ExecuteNonQuery();

        }

        public void deleteRecurringAuditById(int recurring_id, SqlTransaction tran, SqlConnection con)
        {           

            SqlCommand cmd = new SqlCommand("delete from recurring_audits where recurring_audit_id = @recurring", con);
            cmd.Transaction = tran;
            cmd.Parameters.AddWithValue("@recurring", recurring_id);

            cmd.ExecuteNonQuery();
            
        }


        private void updateRecurringInfoByID(int recurring_id, SqlTransaction tran, SqlConnection con, DateTime scheduled, string description, int status, string recur)
        {

            //string selectStmt = "insert into inventory_audits(scheduled_on, created, description, ship_status_id, deleted) VALUES (@scheduled,@created, @description, @status, @flag); SELECT SCOPE_IDENTITY()";
            string selectStmt = "update recurring_audits set description = @desc, " +
                "ship_status = @status, recurring = @recurring";


            SqlCommand cmd = new SqlCommand(selectStmt, con);

            cmd.Transaction = tran;

            
            cmd.Parameters.AddWithValue("@desc", description);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@recurring", recur);            

            cmd.ExecuteNonQuery();

        }

        private void updateRecurringLocationsByID(int recurring_id, SqlTransaction tran, SqlConnection con, DataTable locationsTable)
        {

            //delete locations for the selected recurring audit
            SqlCommand cmd = new SqlCommand("delete from locations_in_recurring_audits where recurring_audit_id = @recurring_id", con);
            cmd.Parameters.AddWithValue("@recurring_id", recurring_id);
            cmd.Transaction = tran;
            cmd.ExecuteNonQuery();

            //insert new locations
            string stmt = "insert into locations_in_recurring_audits (recurring_audit_id, location_id) Values (@auditid, @location_id)";
            SqlCommand locationCmd = new SqlCommand(stmt, con);
            locationCmd.Transaction = tran;            

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCmd.Parameters.Clear();

                locationCmd.Parameters.AddWithValue("@auditid", recurring_id);
                locationCmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));

                locationCmd.ExecuteNonQuery();
            }


        }

        public void editAuditRemoveRecurring(string description, string status, DateTime selectedDate, DataTable locationsTable, int auditid, string recurringStr, bool editRecurringAudits, bool chkRecurChecked, int recurringID)
        {
            //get status from ship_statuses
            int statusid = getStatusIdByName(status);

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            //edit the exisiting audit
            string selectStmt = "update inventory_audits set description = @description, scheduled_on = @scheduled, created=@created, ship_status_id = @status where inventory_audit_id = @auditid";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Transaction = tran;

            DateTime date = DateTime.Now;

            cmd.Parameters.AddWithValue("@scheduled", selectedDate);
            cmd.Parameters.AddWithValue("@created", date);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@status", statusid);
            cmd.Parameters.AddWithValue("@auditid", auditid);

            cmd.ExecuteNonQuery();

            
            string stmt1 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 1);";
            SqlCommand locationCR1Cmd = new SqlCommand(stmt1, mdfConn);

            locationCR1Cmd.Transaction = tran;
            DateTime time = DateTime.Now;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR1Cmd.Parameters.Clear();

                locationCR1Cmd.Parameters.AddWithValue("@inventory_audit_id", auditid);
                locationCR1Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR1Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR1Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR1Cmd.ExecuteNonQuery();
            }

            string stmt2 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 2);";
            SqlCommand locationCR2Cmd = new SqlCommand(stmt2, mdfConn);

            locationCR2Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR2Cmd.Parameters.Clear();

                locationCR2Cmd.Parameters.AddWithValue("@inventory_audit_id", auditid);
                locationCR2Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR2Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR2Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR2Cmd.ExecuteNonQuery();
            }

            string stmt3 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 3);";
            SqlCommand locationCR3Cmd = new SqlCommand(stmt3, mdfConn);

            locationCR3Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR3Cmd.Parameters.Clear();

                locationCR3Cmd.Parameters.AddWithValue("@inventory_audit_id", auditid);
                locationCR3Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR3Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR3Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR3Cmd.ExecuteNonQuery();
            }


            //delete locations
            string deleteStmt = "delete from locations_in_audit where inventory_audit_id = @auditid";
            SqlCommand deleteCmd = new SqlCommand(deleteStmt, mdfConn);
            deleteCmd.Parameters.AddWithValue("@auditid", auditid);
            deleteCmd.Transaction = tran;
            deleteCmd.ExecuteNonQuery();


            //insert new locations for audit
            string stmt = "insert into locations_in_audit (inventory_audit_id, location_id) Values (@auditid, @location_id)";
            SqlCommand locationCmd = new SqlCommand(stmt, mdfConn);

            locationCmd.Transaction = tran;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCmd.Parameters.Clear();

                locationCmd.Parameters.AddWithValue("@auditid", auditid);
                locationCmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));

                locationCmd.ExecuteNonQuery();
            }

            //recurring audit currently doesn't exist..so we'll insert into audits_in_recurring as opposed to updating
            if (recurringID != 0)
            {
                //delete audits in recurring where recurring id = @recurring id
                deleteAuditsInRecurringById(recurringID, tran, mdfConn);

                //delete recurring_audits where recurring_id = @recurring_id
                deleteRecurringAuditById(recurringID, tran, mdfConn);

            }


            //commit all the work
            tran.Commit();
            mdfConn.Close();
        }
                    
        //user will edit the existing audit, and user will create (or update) a recurring audit
        public void editAuditAddRecurring(string description, string status, DateTime selectedDate, DataTable locationsTable, int auditid, string recurringStr, bool editRecurringAudits, bool chkRecurChecked, int recurringID)
        {

            //get status from ship_statuses
            int statusid = getStatusIdByName(status);

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            int newRecurringId = 0;

            //if recurringID is 0, a recurring audit does not exist for this audit yet..we'll create a recurring audit because the user selected the recurring checkbox
            if (recurringID == 0)
            {
                string insertStr = "insert into recurring_audits(description, initially_scheduled_on, ship_status, recurring, deleted) VALUES (@description, @scheduled, @status, @recurring, @deleted);  SELECT SCOPE_IDENTITY()";
                SqlCommand reCmd = new SqlCommand(insertStr, mdfConn);
                reCmd.Transaction = tran;

                reCmd.Parameters.AddWithValue("@description", description);
                reCmd.Parameters.AddWithValue("@status", statusid);
                reCmd.Parameters.AddWithValue("@scheduled", selectedDate);
                reCmd.Parameters.AddWithValue("@recurring", recurringStr);
                reCmd.Parameters.AddWithValue("@deleted", false);



                object oo = reCmd.ExecuteScalar();

                newRecurringId = Convert.ToInt32(oo);


            }
                //update the current recurring audit
            else
            {
                updateRecurringInfoByID(recurringID, tran, mdfConn, selectedDate, description, statusid, recurringStr);


            }

            //edit the exisiting audit
            string selectStmt = "update inventory_audits set description = @description, scheduled_on = @scheduled, created=@created, ship_status_id = @status where inventory_audit_id = @auditid";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Transaction = tran;

            DateTime date = DateTime.Now;

            cmd.Parameters.AddWithValue("@scheduled", selectedDate);
            cmd.Parameters.AddWithValue("@created", date);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@status", statusid);
            cmd.Parameters.AddWithValue("@auditid", auditid);

            cmd.ExecuteNonQuery();

            string stmt1 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 1);";
            SqlCommand locationCR1Cmd = new SqlCommand(stmt1, mdfConn);

            locationCR1Cmd.Transaction = tran;
            DateTime time = DateTime.Now;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR1Cmd.Parameters.Clear();

                locationCR1Cmd.Parameters.AddWithValue("@inventory_audit_id", auditid);
                locationCR1Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR1Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR1Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR1Cmd.ExecuteNonQuery();
            }

            string stmt2 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 2);";
            SqlCommand locationCR2Cmd = new SqlCommand(stmt2, mdfConn);

            locationCR2Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR2Cmd.Parameters.Clear();

                locationCR2Cmd.Parameters.AddWithValue("@inventory_audit_id", auditid);
                locationCR2Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR2Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR2Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR2Cmd.ExecuteNonQuery();
            }

            string stmt3 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 3);";
            SqlCommand locationCR3Cmd = new SqlCommand(stmt3, mdfConn);

            locationCR3Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR3Cmd.Parameters.Clear();

                locationCR3Cmd.Parameters.AddWithValue("@inventory_audit_id", auditid);
                locationCR3Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR3Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR3Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR3Cmd.ExecuteNonQuery();
            }



            //delete locations
            string deleteStmt = "delete from locations_in_audit where inventory_audit_id = @auditid";
            SqlCommand deleteCmd = new SqlCommand(deleteStmt, mdfConn);
            deleteCmd.Parameters.AddWithValue("@auditid", auditid);
            deleteCmd.Transaction = tran;
            deleteCmd.ExecuteNonQuery();


            //insert new locations for audit
            string stmt = "insert into locations_in_audit (inventory_audit_id, location_id) Values (@auditid, @location_id)";
            SqlCommand locationCmd = new SqlCommand(stmt, mdfConn);

            locationCmd.Transaction = tran;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCmd.Parameters.Clear();

                locationCmd.Parameters.AddWithValue("@auditid", auditid);
                locationCmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));

                locationCmd.ExecuteNonQuery();
            }

            //recurring audit currently doesn't exist..so we'll insert into audits_in_recurring as opposed to updating
            if (recurringID == 0)
            {

                //lastly, insert into audits_in_recurring            
                string auditInRecurStmt = "insert into audits_in_recurring (inventory_audit_id, recurring_id) VALUES (@inventory_audit_id,@recurring_id)";
                SqlCommand command = new SqlCommand(auditInRecurStmt, mdfConn);
                command.Transaction = tran;

                command.Parameters.AddWithValue("@inventory_audit_id", auditid);
                command.Parameters.AddWithValue("@recurring_id", newRecurringId);

                command.ExecuteNonQuery();



                //insert into locations_in_recurring_audits
                string insertStmt = "insert into locations_in_recurring_audits (recurring_audit_id,location_id) VALUES (@recurringId, @locationId)";
                SqlCommand cmd2 = new SqlCommand(insertStmt, mdfConn);
                cmd2.Transaction = tran;

                foreach (DataRow row in locationsTable.Rows)
                {
                    cmd2.Parameters.Clear();

                    cmd2.Parameters.AddWithValue("@recurringId", dbNull(newRecurringId));
                    cmd2.Parameters.AddWithValue("@locationId", dbNull(Convert.ToInt32(row[0])));

                    cmd2.ExecuteNonQuery();
                }



            }
           
            //commit all the work
            tran.Commit();
            mdfConn.Close();


        }

        public int createAudit(string description, string status, DateTime selectedDate, DataTable locationsTable, string reoccuringStr)
        {

            if (locationsTable.Rows.Count < 1)
            {
                throw new Exception("You must first select at least one location before creating the audit");
            }

            int statusid = getStatusIdByName(status);

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            
            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();                       

            string selectStmt = "insert into inventory_audits(scheduled_on, created, description, ship_status_id, deleted) VALUES (@scheduled,@created, @description, @status, @flag); SELECT SCOPE_IDENTITY()";
            

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Transaction = tran;

            DateTime date = DateTime.Now;

            cmd.Parameters.AddWithValue("@scheduled", selectedDate);
            cmd.Parameters.AddWithValue("@created", date);
            cmd.Parameters.AddWithValue("@description", description);         
            cmd.Parameters.AddWithValue("@status", statusid);
            cmd.Parameters.AddWithValue("@flag", false);
           
            object o = cmd.ExecuteScalar();

            int inventory_audit_id = Convert.ToInt32(o);


            int reoccuring_id = 0;

            //check if the user has asked for a reccuring audit
            if (reoccuringStr != null)
            {
                //reoccuring audit - 
                string reStmt = "insert into recurring_audits (description, initially_scheduled_on, ship_status, recurring, deleted) VALUES (@description, @intially, @ship_status, @recurring,@flag); SELECT SCOPE_IDENTITY()";
                SqlCommand reCmd = new SqlCommand(reStmt, mdfConn);

                reCmd.Transaction = tran;

                reCmd.Parameters.AddWithValue("@description", description);
                reCmd.Parameters.AddWithValue("@intially", selectedDate);
                reCmd.Parameters.AddWithValue("@ship_status", statusid);
                reCmd.Parameters.AddWithValue("@recurring", reoccuringStr);
                reCmd.Parameters.AddWithValue("@flag", false);

                object oo = reCmd.ExecuteScalar();

                reoccuring_id = Convert.ToInt32(oo);


                //insert audits_in_recurring
                string auditInRecurStmt = "insert into audits_in_recurring (inventory_audit_id, recurring_id) VALUES (@inventory_audit_id,@recurring_id)";
                SqlCommand command = new SqlCommand(auditInRecurStmt, mdfConn);
                command.Transaction = tran;

                command.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                command.Parameters.AddWithValue("@recurring_id", reoccuring_id);

                command.ExecuteNonQuery();



            }
            
            string stmt1 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 1);";
            SqlCommand locationCR1Cmd = new SqlCommand(stmt1, mdfConn);

            locationCR1Cmd.Transaction = tran;
            DateTime time = DateTime.Now;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR1Cmd.Parameters.Clear();

                locationCR1Cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                locationCR1Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR1Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR1Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR1Cmd.ExecuteNonQuery();
            }

            string stmt2 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 2);";
            SqlCommand locationCR2Cmd = new SqlCommand(stmt2, mdfConn);

            locationCR2Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR2Cmd.Parameters.Clear();

                locationCR2Cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                locationCR2Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR2Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR2Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR2Cmd.ExecuteNonQuery();
            }

            string stmt3 = "insert into location_cr (location_id, location_done, inventory_audit_id, action_complete, count) Values (@location_id, @location_done, @inventory_audit_id,@action_complete, 3);";
            SqlCommand locationCR3Cmd = new SqlCommand(stmt3, mdfConn);

            locationCR3Cmd.Transaction = tran;


            foreach (DataRow row in locationsTable.Rows)
            {
                locationCR3Cmd.Parameters.Clear();

                locationCR3Cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
                locationCR3Cmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));
                locationCR3Cmd.Parameters.AddWithValue("@location_done", false);
                locationCR3Cmd.Parameters.AddWithValue("@action_complete", time);


                locationCR3Cmd.ExecuteNonQuery();
            }




            //insert locations in recurring audits

            string stmt = "insert into locations_in_audit (inventory_audit_id, location_id) Values (@auditid, @location_id)";
            SqlCommand locationCmd = new SqlCommand(stmt, mdfConn);

            locationCmd.Transaction = tran;

            foreach (DataRow row in locationsTable.Rows)
            {
                locationCmd.Parameters.Clear();

                locationCmd.Parameters.AddWithValue("@auditid", inventory_audit_id);
                locationCmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));

                locationCmd.ExecuteNonQuery();
            }



            if (reoccuring_id > 0)
            {

                //insert locations for audit

                stmt = "insert into locations_in_recurring_audits (recurring_audit_id, location_id) Values (@auditid, @location_id)";
                locationCmd = new SqlCommand(stmt, mdfConn);

                locationCmd.Transaction = tran;

                foreach (DataRow row in locationsTable.Rows)
                {
                    locationCmd.Parameters.Clear();

                    locationCmd.Parameters.AddWithValue("@auditid", reoccuring_id);
                    locationCmd.Parameters.AddWithValue("@location_id", Convert.ToInt32(row[0]));

                    locationCmd.ExecuteNonQuery();
                }

            }

            //commit all the work
            tran.Commit();

            locationCmd.Dispose();
            cmd.Dispose();
            mdfConn.Close();

            return inventory_audit_id;
        }


        public DataTable getSingleLocationList(int locationid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            string selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location from locations l, workcenter w " +                               
                               "where l.workcenter_id = w.workcenter_id " +
                               "AND l.location_id = @location";
            
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

             cmd.Parameters.AddWithValue("@location", locationid);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }

        public String getCurrentShipName()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            String shipName = null;

            mdfConn.Open();

            string selectStmt = "select s.name, t.description from ships s LEFT JOIN ship_statuses t ON (s.current_status=t.status_id)";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                shipName = rdr["name"].ToString();

                string status = rdr["description"].ToString();  
                
                shipName += " ("+status+ ")";
            }

            cmd.Dispose();
            mdfConn.Close();

            return shipName;

        }

        public DataTable getShip()
        { 

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select * from Ships";

            SqlCommand selectCmd = new SqlCommand(selectStmt, mdfConn);

            SqlDataReader rdr = selectCmd.ExecuteReader();
            string shipName = "";
            while (rdr.Read())
            {
                shipName = rdr["name"].ToString();                
            }
            rdr.Close();
            string stmt = "select * from ships where name = @name";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

            cmd.Parameters.AddWithValue("@name", dbNull(shipName));
             
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);            

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getShipById(int shipId)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select * from Ships where ship_id = @shipId";

            SqlCommand selectCmd = new SqlCommand(selectStmt, mdfConn);
            selectCmd.Parameters.AddWithValue("@shipId", dbNull(shipId));
            

            DataTable dt = new DataTable();

            new SqlDataAdapter(selectCmd).Fill(dt);

            selectCmd.Dispose();

            mdfConn.Close();

            return dt;

        }

        public DataTable getCurrentShip()
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select * from ships";
               
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            if (dt.Rows.Count == 0)
            {
                string insertStmt = "insert into ships (current_status, name, uic, hull_type, hull_number) VALUES(1, '', '', '')";
                SqlCommand insertCmd = new SqlCommand(insertStmt, mdfConn);
                insertCmd.ExecuteNonQuery();

                new SqlDataAdapter(cmd).Fill(dt);
            }

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getAllWorkcenters()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select distinct w.workcenter_id,w.description,w.wid " +
                          "from workcenter w, offload_list o, locations l " +
                          "where w.workcenter_id = l.workcenter_id " +
                          "AND o.location_id = l.location_id " +
                          "AND o.archived_id is null";
      
            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getIncompatibilitiesInventoryList(List<string> hccList, List<int>hazardList, int location_id)
        {
            int hazard_id = hazardList[0];
            int hazard_id2 = hazardList[0];

            if (hazardList.Count == 2)
                hazard_id2 = hazardList[1];

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select v.*, h.warning_level, i.*, m.* from vHazardousItems v " +
                          " INNER JOIN hazard_warnings h ON ((h.hazard_id_1=v.hazard_id) AND (h.hazard_id_2 = @hazard_id OR h.hazard_id_2 = @hazard_id2))" +
                          " INNER JOIN inventory i ON (v.inventory_id=i.inventory_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          +" WHERE v.location_id = @location_id " +
                          " " +
                          " UNION ALL select v.*, 'U', i.*, m.* from v_Top_Hazard_Items v"+
                          " INNER JOIN inventory i ON (v.inventory_id=i.inventory_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE v.location_id = @location_id AND v.hazard_id=0";

            if (hazard_id == 0)
            {
                //If 0 show all items in that location with their hazards.
                stmt = "select v.*, 'U' as warning_level, i.*, m.* from v_Top_Hazard_Items v" +
                          " INNER JOIN inventory i ON (v.inventory_id=i.inventory_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE v.location_id = @location_id";
            }

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

           
           
            cmd.Parameters.AddWithValue("@hazard_id", dbNull(hazard_id));
            cmd.Parameters.AddWithValue("@hazard_id2", dbNull(hazard_id2));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));

            DataTable dt = new DataTable();           

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            List<string> usedInvIds = new List<string>();
            List<DataRow> rowsToRemove = new List<DataRow>();
            //remove duplicates
            foreach (DataRow r in dt.Rows)
            {
                string inventory_id = r["inventory_id"].ToString();
                if (usedInvIds.Contains(inventory_id))
                    rowsToRemove.Add(r);
                else
                    usedInvIds.Add(inventory_id);
            }

            
            //FILTER OUT SPECIAL CASE
            foreach (DataRow row in dt.Rows)
            {
                string hcc1 = hccList[0];
                string hcc2 = Convert.ToString(row["hcc"]);              

                bool ignore = false;

                if (hcc1.Equals("V2") && hcc2.Equals("V3"))
                    ignore = true;
                else if (hcc1.Equals(hcc2) && hcc1 != null && hcc2 != null && !hcc1.Equals("") && !hcc2.Equals("")) //If HCC=HCC they are compatible
                    ignore = true;   
                else if (hcc1.Equals("V3") && hcc2.Equals("V2"))
                    ignore = true;

                if (ignore)
                {
                    if (!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }


            }//end for each
            //Remove rows
            foreach (DataRow row in rowsToRemove)
            {
                dt.Rows.Remove(row);
            }//end

            return dt;

        }



        public DataTable getIncompatibilitiesInventoryListInvAuditCr(List<string> hccList, List<int> hazardList, int location_id, int audit_id, int count, int? inventory_audit_cr_id)
        {
            int hazard_id = hazardList[0];
            int hazard_id2 = hazardList[0];

            if (hazardList.Count == 2)
                hazard_id2 = hazardList[1];

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string ignoreIfParentId = " i.inventory_audit_cr_id NOT IN (select x.parent_id from inventory_audit_cr x WHERE x.parent_id IS NOT NULL) ";
           
            //This method is grabbing hazardous items from the correct count, location, AND audit.
            string stmt = "select v.*, h.warning_level, i.*, m.* from vHazardousItemsInvAuditCr v " +
                          " INNER JOIN hazard_warnings h ON ((h.hazard_id_1=v.hazard_id) AND (h.hazard_id_2 = @hazard_id OR h.hazard_id_2 = @hazard_id2))" +
                          " INNER JOIN inventory_audit_cr i ON (v.inventory_audit_cr_id=i.inventory_audit_cr_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE " + ignoreIfParentId + " AND v.location_id = @location_id " +
                          " AND v.inventory_audit_id = @audit_id "
                + "and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                + "where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)" +
                          " " +
                          " UNION ALL select v.*, 'U', i.*, m.* from v_Top_Hazard_ItemsInvAuditCr v" +
                          " INNER JOIN inventory_audit_cr i ON (v.inventory_audit_cr_id=i.inventory_audit_cr_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE " + ignoreIfParentId + " AND v.location_id = @location_id AND v.hazard_id=0"
                          + " AND v.inventory_audit_id = @audit_id "
                + "and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                + "where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)";

            if (hazard_id == 0)
            {
                //If 0 show all items in that location with their hazards.
                stmt = "select v.*, 'U' as warning_level, i.*, m.* from v_Top_Hazard_ItemsInvAuditCr v" +
                          " INNER JOIN inventory_audit_cr i ON (v.inventory_audit_cr_id=i.inventory_audit_cr_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE " + ignoreIfParentId + " AND v.location_id = @location_id"
                          + " AND v.inventory_audit_id = @audit_id "
                + "and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                + "where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)";
            }

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);



            cmd.Parameters.AddWithValue("@hazard_id", dbNull(hazard_id));
            cmd.Parameters.AddWithValue("@hazard_id2", dbNull(hazard_id2));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("audit_id", audit_id);
            cmd.Parameters.AddWithValue("count", count);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            List<string> usedInvIds = new List<string>();
            List<DataRow> rowsToRemove = new List<DataRow>();

            //This stmt must be included in the case of an InvAuditCr update so that an item does not show up as incompatible with itself.
            if(inventory_audit_cr_id!=null)
               usedInvIds.Add(inventory_audit_cr_id.ToString());

            //remove duplicates
            foreach (DataRow r in dt.Rows)
            {
                string cr_id = r["inventory_audit_cr_id"].ToString();
                if (usedInvIds.Contains(cr_id))
                    rowsToRemove.Add(r);
                else
                    usedInvIds.Add(cr_id);
            }

            //FILTER OUT SPECIAL CASE
            foreach (DataRow row in dt.Rows)
            {
                string hcc1 = hccList[0];
                string hcc2 = Convert.ToString(row["hcc"]);

                bool ignore = false;

                if (hcc1.Equals("V2") && hcc2.Equals("V3"))
                    ignore = true;
                else if (hcc1.Equals(hcc2) && hcc1!=null && hcc2!=null && !hcc1.Equals("") && !hcc2.Equals("")) //If HCC=HCC they are compatible
                    ignore = true;               
                else if (hcc1.Equals("V3") && hcc2.Equals("V2"))
                    ignore = true;

                if (ignore)
                {
                    if(!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }


            }//end for each
            //Remove rows
            foreach (DataRow row in rowsToRemove)
            {
                dt.Rows.Remove(row);
            }//end

            return dt;

        }


        public DataTable getAdminUsersCollection(bool hideAdmin)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string hide = null;
            
            if(hideAdmin)
               hide= " WHERE role <> 'ADMIN' "; //show only SUPPLY OFFICER

            string selectStmt = "select * from user_roles " + hide;

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }
        public DataTable getAllLocations()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select distinct l.location_id,l.name,l.workcenter_id from locations l, offload_list o where l.location_id = o.location_id AND o.archived_id is null";
      
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getDistinctLocationListAll() //includes NEWLY ISSUED locations
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select DISTINCT l.name as Location " +
                                 "FROM locations l ";
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public void insertUploadedClipboard(string guid, string path, string hash)
        {

            DateTime time = DateTime.Now;

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("insert into uploaded_clipboards(hash, path, guid, date) VALUES (@hash,@path,@guid,@date)", con);

            cmd.Parameters.AddWithValue("@hash", dbNull(hash));
            cmd.Parameters.AddWithValue("@path", dbNull(path));
            cmd.Parameters.AddWithValue("@guid", dbNull(guid));
            cmd.Parameters.AddWithValue("@date", dbNull(time));

            cmd.ExecuteNonQuery();

            con.Close();

        }

        public bool foundClipboardHash(string hash)
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select count(*) from uploaded_clipboards u where u.hash = @hash", con);
            cmd.Parameters.AddWithValue("@hash", hash);

            object o = cmd.ExecuteScalar();

            int count = Convert.ToInt32(o);

            con.Close();

            if (count < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }

//ATM - use this one.
        public string insertAtmosphereControl(string niin, string cage, string msds_serial_number, DateTime? shelf_life)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string stmt1 = "execute atm_seq";
            SqlCommand cmd0 = new SqlCommand(stmt1, mdfConn);
            cmd0.Transaction = tran;

            object o = cmd0.ExecuteScalar();
            int atmosphere_control_number = Int32.Parse(o.ToString());

            string stmt2 = "insert into atmosphere_control(atmosphere_control_number, cage, niin, msds_serial_number, shelf_life)"
                + " VALUES(@atmosphere_control_number, @cage, @niin, @msds_serial_number, @shelf_life)";
            SqlCommand cmd = new SqlCommand(stmt2, mdfConn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@shelf_life", dbNull(shelf_life));
            cmd.Parameters.AddWithValue("@cage", dbNull(cage));
            cmd.Parameters.AddWithValue("@niin", dbNull(niin));
            cmd.Parameters.AddWithValue("@msds_serial_number", dbNull(msds_serial_number));
            cmd.Parameters.AddWithValue("@atmosphere_control_number", dbNull(atmosphere_control_number));

            cmd.ExecuteNonQuery();

            //commit
            tran.Commit();

            tran.Dispose();
            cmd.Dispose();
            mdfConn.Close();

            return atmosphere_control_number.ToString();

        }

        public void insertAtmosphereControl(string niin, string cage, string msdsserno)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            
            mdfConn.Open();
            

            SqlTransaction tran = mdfConn.BeginTransaction();

            string stmt = "select max(atmosphere_control_number) from atmosphere_control";
            SqlCommand maxCmd = new SqlCommand(stmt, mdfConn);
            maxCmd.Transaction = tran;

            object o = maxCmd.ExecuteScalar();
            int max = 0;
            try
            {
                max = Convert.ToInt32(o);
            }
            catch {}
            maxCmd.Dispose();

            //no rows in the database - start at 999
            if (max == 0)
            {
                max = 999;
            }
            else
            {
                max = max + 1;

            }

            string stmt2 = "insert into atmosphere_control(atmosphere_control_number, cage, niin, msds_serial_number) VALUES(@acn, @cage, @niin, @msdsserno)";
            SqlCommand cmd = new SqlCommand(stmt2, mdfConn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@acn", dbNull(max));
            cmd.Parameters.AddWithValue("@cage", dbNull(cage));
            cmd.Parameters.AddWithValue("@niin", dbNull(niin));
            cmd.Parameters.AddWithValue("@msdsserno", dbNull(msdsserno));

            cmd.ExecuteNonQuery();

            //commit
            tran.Commit();

            tran.Dispose();
            cmd.Dispose();
            mdfConn.Close();
         
        }

        //TODO MSDDSERNO - This method should not be used anywhere
        public string getserFromMsdsByNiinCage(string niin, string cage, string contractNumber)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select v.msdsserno from v_msds_contractor v where v.cage = @cage AND v.niin = @niin AND v.ct_number like @contract";
            SqlCommand msdsCmd = new SqlCommand(stmt, mdfConn);
            msdsCmd.Parameters.AddWithValue("@cage", dbNull(cage));
            msdsCmd.Parameters.AddWithValue("@niin", dbNull(niin));
            msdsCmd.Parameters.AddWithValue("@contract", dbNull(contractNumber));

            object o = null;
            try
            {
               o = msdsCmd.ExecuteScalar();
            }
            catch {}
            string msdsserno = "";
            try
            {
                msdsserno = Convert.ToString(o);
            }
            catch 
            {
            }

            mdfConn.Close();

            return msdsserno;

        }

        public string getAtmNumber(string niin, string cage, string msdsSerNo)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();

            string stmt = "select atmosphere_control_number from atmosphere_control where cage = @cage AND niin = @niin AND msds_serial_number = @msds_serial_number";
            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            cmd.Parameters.AddWithValue("@cage", dbNull(cage));
            cmd.Parameters.AddWithValue("@niin", dbNull(niin));
            cmd.Parameters.AddWithValue("@msds_serial_number", dbNull(msdsSerNo));

            SqlDataReader rdr = cmd.ExecuteReader();

            string atmNumber = null;
            if (rdr.Read())
            {
                atmNumber = rdr.GetString(0);
            }


            mdfConn.Close();

            return atmNumber;
        }

        public List<ATMPrintInfo> getAtmInfoByNiinCage(string niin, string cage, string expirationDate, string contractNumber, string invSerialNumber)
        {
            List<ATMPrintInfo> infoList = new List<ATMPrintInfo>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            SqlConnection mdfConn2 = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();
            mdfConn2.Open();
            string msdsserno = null;
            
            if (!invSerialNumber.Equals(""))
            {
                msdsserno = invSerialNumber;
            }

            //TODO MSDSSERNO - This statement is wrong and will change once the atmosphere_control table has changed.
            string selectStmt = "select a.niin, a.cage, a.msds_serial_number as msds, a.atmosphere_control_number as acn from atmosphere_control a, niin_catalog n, mfg_catalog m " +
                                "where a.niin = n.niin " +
                                "AND a.cage = m.cage " +
                                "AND m.niin_catalog_id = n.niin_catalog_id " +
                                "AND a.cage = @cage " +                                
                                "AND a.niin = @niin ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@cage", dbNull(cage));
            cmd.Parameters.AddWithValue("@niin", dbNull(niin));


            if (!msdsserno.Equals(""))
            {
                selectStmt += "AND a.msds_serial_number = @msds ";
                cmd.CommandText = selectStmt;
                cmd.Parameters.AddWithValue("@msds", dbNull(msdsserno));
            }
            else
            {
                selectStmt += "AND a.msds_serial_number is null ";
                cmd.CommandText = selectStmt;
            }
            

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {

                ATMPrintInfo info = new ATMPrintInfo();

                string cag = null;

                try
                {
                    cag = Convert.ToString(rdr["cage"]);
                }
                catch {}

                string nin = null;
                try
                {
                    nin = Convert.ToString(rdr["niin"]);
                }
                catch {}

                string msds = null;
                try
                {
                    msds = Convert.ToString(rdr["msds"]);
                }
                catch {}


                int? acn = null;
                try
                {
                    acn = Convert.ToInt32(rdr["acn"]);
                }
                catch {}

                if (acn != null)
                {
                    info.ControlNumber = (int)acn;
                }
                info.Cage = cag;             
                info.Date = expirationDate;
                info.Niin = nin;
                info.Msds = msdsserno;

                infoList.Add(info);

                                

            }

            mdfConn.Close();
            mdfConn2.Close();

            return infoList;
        }

        public DataTable getLocationList()
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                 "FROM locations l, workcenter w " +
                                "where l.workcenter_id = w.workcenter_id AND l.name<>'NEWLY ISSUED'";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }

        public DataTable getFilteredTotalInventoryAuditVolumes(string unitsFilePath, string filterColumn, string filterText, bool includeIgnored)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            string ignore = " n.niin NOT IN (SELECT niin from volume_ignored_niins)";

            if (!filterText.Equals("") && !includeIgnored)
            {
                where = " AND  n." + filterColumn + " like '%' + @filter + '%' " + " AND " + ignore;
            }
            else if (!filterText.Equals("") && includeIgnored)
            {
                where = " AND  n." + filterColumn + " like '%' + @filter + '%' " ;
            }
            else if (!includeIgnored)
            {
                where = " AND " + ignore;
            }
            

            string selectStmt = "select v.*, n.*, '?' as total_oz, '?' as total_gl, v.um as unknown_um, ' ' as other, "+
                
                " (SELECT COUNT(*) AS Expr1 FROM volume_ignored_niins WHERE (niin = n.niin)) AS ignored_count "

                
                +" from niin_catalog n INNER JOIN "+
" v_top_total_audit_volumes v ON (v.niin_catalog_id=n.niin_catalog_id) " + where;

            

            selectStmt += where + " ORDER BY n.niin_catalog_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            conn.Close();

            UnitConverter converter = new UnitConverter(unitsFilePath);

            Dictionary<string, double> niinOzMap = new Dictionary<string, double>();
            Dictionary<string, double> niinGlMap = new Dictionary<string, double>();

            //calculate totals
            foreach (DataRow r in dt.Rows)
            {
                string niin_catalog_id = r["niin_catalog_id"].ToString() + r["cosal"].ToString();
                string um = r["um"].ToString();
                double multiplier = Double.Parse(r["total_volume"].ToString());

                double total_oz = 0.0;
                double total_gl = 0.0;

                if (!niinOzMap.ContainsKey(niin_catalog_id))
                    niinOzMap.Add(niin_catalog_id, total_oz);

                if (!niinGlMap.ContainsKey(niin_catalog_id))
                    niinGlMap.Add(niin_catalog_id, total_gl);

                try
                {
                    if (um != null && !um.Equals(""))
                    {
                        um = um.ToUpper();
                        string unit = "";

                        //Convert to OZ
                        string oz_result = converter.convertToUnit(um, multiplier, "OZ");                        
                        converter.uc.ParseUnitString(oz_result, out total_oz, out unit);

                        //Add for unlike units
                        total_oz = niinOzMap[niin_catalog_id] + total_oz;
                        niinOzMap.Remove(niin_catalog_id);
                        niinOzMap.Add(niin_catalog_id, total_oz);

                        //Convert to GL
                        string gl_result = converter.convertToUnit(um, multiplier, "GL");
                        converter.uc.ParseUnitString(gl_result, out total_gl, out unit);

                        //Add for unlike units
                        total_gl = niinGlMap[niin_catalog_id] + total_gl;
                        niinGlMap.Remove(niin_catalog_id);
                        niinGlMap.Add(niin_catalog_id, total_gl);

                        r["total_oz"] = string.Format("{0:#,###,##0.##}",Math.Round(total_oz, 2)) + " OZ";
                        r["total_gl"] =  string.Format("{0:#,###,##0.##}",Math.Round(total_gl, 2)) + " GL";
                        r["unknown_um"] = "";
                    }
                   
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                   //Conversion failed. Units were unknown.
                    string other = "";
                    try
                    {
                        string unit = "";
                        double unit_number = 0.0;
                        converter.uc.ParseUnitString(um, out unit_number, out unit);

                        if (unit_number == 0.0)
                            unit_number = 1;

                        unit = um.Replace("" + (int)unit_number, "");

                        unit_number = unit_number * multiplier;                        

                        other = unit_number + " " + unit;

                        r["other"] = other + " ["+multiplier+" x " + um + "]" ;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        //Unit unparsible, just show the total volume x the UM
                        other = multiplier + " x (" + um + ")";
                        r["other"] = other;
                    }

                                       
                }
            }

            //Get rid of duplicate niin records that successfully
            //had the total volume figured. Leave question mark items whose UM was unreadable.
            List<string> usedNiins = new List<string>();

            //Loop through and clear out duplicate rows.
            foreach (DataRow r in dt.Rows)
            {
                string niin_catalog_id = r["niin_catalog_id"].ToString() + r["cosal"].ToString();
               
                if (!r["total_oz"].ToString().Equals("?"))
                {
                    if (usedNiins.Contains(niin_catalog_id))
                        r.Delete();
                    else
                        usedNiins.Add(niin_catalog_id);
                }
            }

            return dt;
        }

        public DataTable getFilteredTotalOffloadVolumes(string unitsFilePath, string filterColumn, string filterText, bool includeIgnored)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            string ignore = " n.niin NOT IN (SELECT niin from volume_ignored_niins)";

            if (!filterText.Equals("") && !includeIgnored)
            {
                where = " AND  n." + filterColumn + " like '%' + @filter + '%' " + " AND " + ignore;
            }
            else if (!filterText.Equals("") && includeIgnored)
            {
                where = " AND  n." + filterColumn + " like '%' + @filter + '%' ";
            }
            else if (!includeIgnored)
            {
                where = " AND " + ignore;
            }


            string selectStmt = "select v.*, n.*, '?' as total_oz, '?' as total_gl, v.um as unknown_um, ' ' as other, " +

                " (SELECT COUNT(*) AS Expr1 FROM volume_ignored_niins WHERE (niin = n.niin)) AS ignored_count "


                + " from niin_catalog n INNER JOIN " +
" v_top_total_offload_volumes v ON (v.niin_catalog_id=n.niin_catalog_id) " + where;



            selectStmt += where + " ORDER BY n.niin_catalog_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            conn.Close();

            UnitConverter converter = new UnitConverter(unitsFilePath);

            Dictionary<string, double> niinOzMap = new Dictionary<string, double>();
            Dictionary<string, double> niinGlMap = new Dictionary<string, double>();

            //calculate totals
            foreach (DataRow r in dt.Rows)
            {
                string niin_catalog_id = r["niin_catalog_id"].ToString() + r["cosal"].ToString();
                string reason = r["reason_name"].ToString();

                //key = niin + reason + cosal
                niin_catalog_id += reason;

                string um = r["um"].ToString();
                double multiplier = Double.Parse(r["total_volume"].ToString());

                double total_oz = 0.0;
                double total_gl = 0.0;

                if (!niinOzMap.ContainsKey(niin_catalog_id))
                    niinOzMap.Add(niin_catalog_id, total_oz);

                if (!niinGlMap.ContainsKey(niin_catalog_id))
                    niinGlMap.Add(niin_catalog_id, total_gl);

                try
                {
                    if (um != null && !um.Equals(""))
                    {
                        um = um.ToUpper();
                        string unit = "";

                        //Convert to OZ
                        string oz_result = converter.convertToUnit(um, multiplier, "OZ");
                        converter.uc.ParseUnitString(oz_result, out total_oz, out unit);

                        //Add for unlike units
                        total_oz = niinOzMap[niin_catalog_id] + total_oz;
                        niinOzMap.Remove(niin_catalog_id);
                        niinOzMap.Add(niin_catalog_id, total_oz);

                        //Convert to GL
                        string gl_result = converter.convertToUnit(um, multiplier, "GL");
                        converter.uc.ParseUnitString(gl_result, out total_gl, out unit);

                        //Add for unlike units
                        total_gl = niinGlMap[niin_catalog_id] + total_gl;
                        niinGlMap.Remove(niin_catalog_id);
                        niinGlMap.Add(niin_catalog_id, total_gl);

                        r["total_oz"] = string.Format("{0:#,###,##0.##}",Math.Round(total_oz, 2)) + " OZ";
                        r["total_gl"] = string.Format("{0:#,###,##0.##}",Math.Round(total_gl, 2)) + " GL";
                        r["unknown_um"] = "";
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //Conversion failed. Units were unknown.
                    string other = "";
                    try
                    {
                        string unit = "";
                        double unit_number = 0.0;
                        converter.uc.ParseUnitString(um, out unit_number, out unit);

                        if (unit_number == 0.0)
                            unit_number = 1;

                        unit = um.Replace("" + (int)unit_number, "");

                        unit_number = unit_number * multiplier;

                        other = unit_number + " " + unit;

                        r["other"] = other + " [" + multiplier + " x " + um + "]";
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        //Unit unparsible, just show the total volume x the UM
                        other = multiplier + " x (" + um + ")";
                        r["other"] = other;
                    }


                }
            }

            //Get rid of duplicate niin records that successfully
            //had the total volume figured. Leave question mark items whose UM was unreadable.
            List<string> usedNiins = new List<string>();

            //Loop through and clear out duplicate rows.
            foreach (DataRow r in dt.Rows)
            {
                string niin_catalog_id = r["niin_catalog_id"].ToString() + r["cosal"].ToString();
                string reason = r["reason_name"].ToString();

                //key = niin + reason
                niin_catalog_id += reason;

                if (!r["total_oz"].ToString().Equals("?"))
                {
                    if (usedNiins.Contains(niin_catalog_id))
                        r.Delete();
                    else
                        usedNiins.Add(niin_catalog_id);
                }
            }

            return dt;
        }


        public DataTable getAvailableLocationsFromSelected(DataTable selectedTable)
        {


            List<Int32> selectedValues = new List<Int32>();
            string[] selectedParameters = new string[selectedTable.Rows.Count];
            int count = 0;

            foreach (DataRow row in selectedTable.Rows)
            {
                selectedValues.Add(Convert.ToInt32(row[0]));
                selectedParameters[count] = "@sp" + count.ToString();
                count++;
            }
            
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();

            //select locations where their id does not match the ID from the selected DataTable (parameter)

            string selectStmt = "";
            SqlCommand cmd;
            if (count > 0)
            {
                selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                    "FROM locations l, workcenter w " +
                                   "where l.name<>'NEWLY ISSUED' AND l.workcenter_id = w.workcenter_id " +
                                   "AND l.location_id not in(" + string.Join(",", selectedParameters) + ")";
                cmd = new SqlCommand(selectStmt, mdfConn);
                for (int i = 0; i < count; i++)
                    cmd.Parameters.AddWithValue(selectedParameters[i], selectedValues[i]);
            }
            else
            {
                selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                    "FROM locations l, workcenter w " +
                                   "where  l.name<>'NEWLY ISSUED' AND l.workcenter_id = w.workcenter_id";

                cmd = new SqlCommand(selectStmt, mdfConn);
            }


            
          //  cmd.Parameters.AddWithValue("@auditid", auditid);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public void insertHHWorkCenters(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into workcenter (workcenter_id,description,wid) VALUES (@workcenter_id,@desc,@wid)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            SqlCommand cmd = new SqlCommand("select * from workcenter", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int workcenter_id = Convert.ToInt32(rdr["workcenter_id"].ToString());
                string description = rdr["description"].ToString();
                string wid = rdr["wid"].ToString();

                liteCmd.Parameters.AddWithValue("@workcenter_id", workcenter_id);
                liteCmd.Parameters.AddWithValue("@desc", description);
                liteCmd.Parameters.AddWithValue("@wid", wid);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        public void updateShip(string name, string uic, string hull_type, string hull_number, int current_status, string address, string pocName, string pocTelephone, string pocEmail)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "UPDATE ships set name=@name, uic=@uic, hull_type=@hull_type,"
                + " hull_number=@hull_number, current_status=@current_status, address=@address, poc_Name=@pocName, poc_Telephone=@pocTelephone, poc_Email=@pocEmail ";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@name", dbNull(name));
            cmd.Parameters.AddWithValue("@uic", dbNull(uic));
            cmd.Parameters.AddWithValue("@hull_type", dbNull(hull_type));
            cmd.Parameters.AddWithValue("@hull_number", dbNull(hull_number));
            cmd.Parameters.AddWithValue("@current_status", dbNull(current_status));
            cmd.Parameters.AddWithValue("@address", dbNull(address));
            cmd.Parameters.AddWithValue("@pocName", dbNull(pocName));
            cmd.Parameters.AddWithValue("@pocTelephone", dbNull(pocTelephone));
            cmd.Parameters.AddWithValue("@pocEmail", dbNull(pocEmail));

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public int insertSFR(SFR sfr)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            int sfr_id;             

            if (sfr.SFRType == SFR.NEW_TYPE)
            {
                string insertStmt = "insert into sfr (sfr_type, action_complete, username, POC_Name, POC_Telephone, POC_Email, fsc, niin, niin_catalog_id, description, usage_category_id, ui, um, specs, spmig, nehc_rpt, cog_id, storage_type_id, shelf_life_code_id, shelf_life_action_code_id, smcc_id, catalog_group_id, catalog_serial_number, ship_id, remarks, msds_num, manufacturer, cage, part_no, poc_address, poc_city, poc_state, poc_zip, system_equipment_material, method_of_application, proposed_usage, negative_impact, special_training, precautions, properties, advantages, comments)"
                                  + "VALUES (@sfr_type, @action_complete, @username, @POC_Name, @POC_Telephone, @POC_Email, @fsc, @niin, @niin_catalog_id, @description, @usage_category_id, @ui, @um, @specs, @spmig, @nehc_rpt, @cog_id, @storage_type_id, @shelf_life_code_id, @shelf_life_action_code_id, @smcc_id, @catalog_group_id, @catalog_serial_number, @ship_id,@remarks, @msds_num, @manufacturer, @cage, @part_no, @poc_address, @poc_city, @poc_state, @poc_zip, @system_equipment_material, @method_of_application, @proposed_usage, @negative_impact, @special_training, @precautions, @properties, @advantages, @comments);  SELECT SCOPE_IDENTITY()";

                SqlCommand cmd = new SqlCommand(insertStmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@sfr_type", dbNull(sfr.SFRType));
                cmd.Parameters.AddWithValue("@fsc", dbNull(sfr.Fsc));
                cmd.Parameters.AddWithValue("@niin", dbNull(sfr.Niin));
                cmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(sfr.NiinCatalogId));
                cmd.Parameters.AddWithValue("@description", dbNull(sfr.Description));
                cmd.Parameters.AddWithValue("@usage_category_id", dbNull(sfr.Usage_category_id));
                cmd.Parameters.AddWithValue("@ui", dbNull(sfr.Ui));
                cmd.Parameters.AddWithValue("@um", dbNull(sfr.Um));
                cmd.Parameters.AddWithValue("@specs", dbNull(sfr.Specs));
                cmd.Parameters.AddWithValue("@spmig", dbNull(sfr.Spmig));
                cmd.Parameters.AddWithValue("@nehc_rpt", dbNull(sfr.NehcRpt));
                cmd.Parameters.AddWithValue("@cog_id", dbNull(sfr.Cog_id));
                cmd.Parameters.AddWithValue("@storage_type_id", dbNull(sfr.Storage_type_id));
                cmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(sfr.Shelf_life_code_id));
                cmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(sfr.Shelf_life_action_code_id));
                cmd.Parameters.AddWithValue("@smcc_id", dbNull(sfr.SmccId));
                cmd.Parameters.AddWithValue("@catalog_group_id", dbNull(sfr.CatalogGroupId));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
                cmd.Parameters.AddWithValue("@username", dbNull(sfr.UserName));
                cmd.Parameters.AddWithValue("@POC_Name", dbNull(sfr.POCName));
                cmd.Parameters.AddWithValue("@POC_Telephone", dbNull(sfr.POCTelephone));
                cmd.Parameters.AddWithValue("@POC_Email", dbNull(sfr.POCEmail));
                cmd.Parameters.AddWithValue("@ship_id", dbNull(sfr.ShipId));
                cmd.Parameters.AddWithValue("@manufacturer", dbNull(sfr.Manufacturer));
                cmd.Parameters.AddWithValue("@cage", dbNull(sfr.Cage));
                cmd.Parameters.AddWithValue("@msds_num", dbNull(sfr.MsdsNum));
                cmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(sfr.CatalogSerialNumber));
                cmd.Parameters.AddWithValue("@remarks", dbNull(sfr.Remarks));
                cmd.Parameters.AddWithValue("@part_no", dbNull(sfr.partNo));
                cmd.Parameters.AddWithValue("@poc_address", dbNull(sfr.pocAddress));
                cmd.Parameters.AddWithValue("@poc_city", dbNull(sfr.pocCity));
                cmd.Parameters.AddWithValue("@poc_state", dbNull(sfr.pocState));
                cmd.Parameters.AddWithValue("@poc_zip", dbNull(sfr.pocZip));
                cmd.Parameters.AddWithValue("@system_equipment_material", dbNull(sfr.systemEquipmentMaterial));
                cmd.Parameters.AddWithValue("@method_of_application", dbNull(sfr.methodOfApplication));
                cmd.Parameters.AddWithValue("@proposed_usage", dbNull(sfr.proposedUsage));
                cmd.Parameters.AddWithValue("@negative_impact", dbNull(sfr.negativeImpact));
                cmd.Parameters.AddWithValue("@special_training", dbNull(sfr.specialTraining));
                cmd.Parameters.AddWithValue("@precautions", dbNull(sfr.Precautions));
                cmd.Parameters.AddWithValue("@properties", dbNull(sfr.Properties));
                cmd.Parameters.AddWithValue("@advantages", dbNull(sfr.Advantages));
                cmd.Parameters.AddWithValue("@comments", dbNull(sfr.Comments));

                object o = cmd.ExecuteScalar();
                sfr_id = Convert.ToInt32(o);

                tran.Commit();
            }
            // NEW!
            else
            {

                string insertStmt = "insert into sfr (sfr_type, action_complete, username, POC_Name, POC_Telephone, POC_Email, fsc, niin, niin_catalog_id, description, usage_category_id, ui, um, specs, spmig, cog_id, storage_type_id, shelf_life_code_id, shelf_life_action_code_id, smcc_id, catalog_group_id, ship_id, remarks, msds_num, allowance_qty, manufacturer, cage)"
                                  + "VALUES (@sfr_type, @action_complete, @username, @POC_Name, @POC_Telephone, @POC_Email, @fsc, @niin, @niin_catalog_id, @description, @usage_category_id, @ui, @um, @specs, @spmig, @cog_id, @storage_type_id, @shelf_life_code_id, @shelf_life_action_code_id, @smcc_id, @catalog_group_id, @ship_id, @remarks, @msds_num, @allowance_qty, @manufacturer, @cage);  SELECT SCOPE_IDENTITY()";

                SqlCommand cmd = new SqlCommand(insertStmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@sfr_type", dbNull(sfr.SFRType));
                cmd.Parameters.AddWithValue("@fsc", dbNull(sfr.Fsc));
                cmd.Parameters.AddWithValue("@niin", dbNull(sfr.Niin));
                cmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(sfr.NiinCatalogId));
                cmd.Parameters.AddWithValue("@description", dbNull(sfr.Description));
                cmd.Parameters.AddWithValue("@usage_category_id", dbNull(sfr.Usage_category_id));
                cmd.Parameters.AddWithValue("@ui", dbNull(sfr.Ui));
                cmd.Parameters.AddWithValue("@um", dbNull(sfr.Um));
                cmd.Parameters.AddWithValue("@specs", dbNull(sfr.Specs));
                cmd.Parameters.AddWithValue("@spmig", dbNull(sfr.Spmig));
                cmd.Parameters.AddWithValue("@cog_id", dbNull(sfr.Cog_id));
                cmd.Parameters.AddWithValue("@storage_type_id", dbNull(sfr.Storage_type_id));
                cmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(sfr.Shelf_life_code_id));
                cmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(sfr.Shelf_life_action_code_id));
                cmd.Parameters.AddWithValue("@smcc_id", dbNull(sfr.SmccId));
                cmd.Parameters.AddWithValue("@catalog_group_id", dbNull(sfr.CatalogGroupId));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
                cmd.Parameters.AddWithValue("@username", dbNull(sfr.UserName));
                cmd.Parameters.AddWithValue("@POC_Name", dbNull(sfr.POCName));
                cmd.Parameters.AddWithValue("@POC_Telephone", dbNull(sfr.POCTelephone));
                cmd.Parameters.AddWithValue("@POC_Email", dbNull(sfr.POCEmail));
                cmd.Parameters.AddWithValue("@ship_id", dbNull(sfr.ShipId));
                cmd.Parameters.AddWithValue("@manufacturer", dbNull(sfr.Manufacturer));
                cmd.Parameters.AddWithValue("@cage", dbNull(sfr.Cage));
                cmd.Parameters.AddWithValue("@allowance_qty", dbNull(sfr.AllowanceQty));
                cmd.Parameters.AddWithValue("@msds_num", dbNull(sfr.MsdsNum));
                cmd.Parameters.AddWithValue("@remarks", dbNull(sfr.Remarks));

                cmd.ExecuteNonQuery();
                tran.Commit();
                sfr_id = 0;
            }

            conn.Close();

            return sfr_id;
        }

        public void setUIUM(int niin_catalog_id, string Ui, string Um, string SLC, string SPMIG)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "UPDATE niin_catalog set ui=@ui, um=@um, shelf_life_code_id=@shelf_life_code_id, spmig=@spmig where niin_catalog_id=@niin_catalog_id";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
            cmd.Parameters.AddWithValue("@ui", dbNull(Ui));
            cmd.Parameters.AddWithValue("@um", dbNull(Um));
            cmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(SLC));
            cmd.Parameters.AddWithValue("@spmig", dbNull(SPMIG));

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void updateSMCL(string fsc, string niin, string description, string usage_category_id, string ui, string um, string specs, string spmig, string cog_id, string storage_type_id, string shelf_life_code_id, string shelf_life_action_code_id, string smcc_id, int sfr_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertSFRStmt = "UPDATE sfr set fsc=@fsc, description=@description, usage_category_id=@usage_category_id,"
                + " ui=@ui, um=@um, specs=@specs, spmig=@spmig, cog_id=@cog_id, storage_type_id=@storage_type_id, shelf_life_code_id=@shelf_life_code_id, shelf_life_action_code_id=@shelf_life_action_code_id, smcc_id=@smcc_id ";

            SqlCommand cmd = new SqlCommand(insertSFRStmt, conn);

            cmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
            cmd.Parameters.AddWithValue("@description", dbNull(description));
            cmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
            cmd.Parameters.AddWithValue("@ui", dbNull(ui));
            cmd.Parameters.AddWithValue("@um", dbNull(um));
            cmd.Parameters.AddWithValue("@specs", dbNull(specs));
            cmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
            cmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
            cmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
            cmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
            cmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
            cmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));

            cmd.ExecuteNonQuery();
            
            conn.Close();


          
        }

        public SFR getSFRById(int id)
        {
            SFR s = new SFR();

            return s;
        }

        public void updateSMCLAllowanceQty(int? niin_catalog_id, int allowance_qty)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "UPDATE niin_catalog set allowance_qty=@allowance_qty "
                + " WHERE niin_catalog_id=@niin_catalog_id ";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
            cmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
           
            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public void updateSMCLRemarks(int? niin_catalog_id, string remarks)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "UPDATE niin_catalog set remarks=@remarks "
                + " WHERE niin_catalog_id=@niin_catalog_id ";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
            cmd.Parameters.AddWithValue("@remarks", dbNull(remarks));

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public void updateApprovedIncompatiblesFlagAllInvalidated()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "UPDATE approved_incompatibles set invalidated_flag=1 ";               

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public void updateApprovedIncompatiblesFlagInvalidated(int inventory_id1, int inventory_id2,
                int location_id) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string insertStmt = "UPDATE approved_incompatibles set invalidated_flag=1 "
                + "WHERE location_id = @location_id AND "
                + "((inventory_id1 =  @inventory_id1 and inventory_id2 =  @inventory_id2) "
                + "OR (inventory_id2 =  @inventory_id1 and inventory_id1 =  @inventory_id2))";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_id1", inventory_id1);
            cmd.Parameters.AddWithValue("@inventory_id2", inventory_id2);
            cmd.Parameters.AddWithValue("@location_id", location_id);

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public int editOffloadDetails(int offload_list_id, int offload_detail_id, DD1348Item offloadDetails, bool isGarbageItem)
        {
                        
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            //insert offload detail
            string insertStmt = "insert into offload_details ("
                + "DOC_IDENT, RI_FROM, M_S, SER, SUPP_ADDRESS, SIG, FUND, DISTRIBUTION, PROJECT, PRI, REQ_DEL_DATE, RI, O_P, COND,  "
                + "MGT, UNIT_DOLLARS, UNIT_CTS,TOTAL_DOLLARS, TOTAL_CTS, SHIP_FROM, SHIP_TO, MARK_FOR, DOC_DATE, NMFC, TYPE_CARGO, PS, UP, "
                + "UNIT_WEIGHT, UNIT_CUBE,UFC,SL,FRGHT_CLASS_NOM, ITEM_NOM, HCC_MSG,DMIL, JON,HCC, CIIC, TY_CARGO_MSG,MSDS, DOCUMENT_NUMBER,RICUIQTY, PCN , date_value, serial_number, iteration"
                +" )"
            + "VALUES ("
             + "@DOC_IDENT, @RI_FROM, @M_S, @SER, @SUPP_ADDRESS, @SIG, @FUND, @DISTRIBUTION, @PROJECT, @PRI, @REQ_DEL_DATE, @RI, @O_P, @COND,  "
                + "@MGT, @UNIT_DOLLARS, @UNIT_CTS,@TOTAL_DOLLARS, @TOTAL_CTS, @SHIP_FROM, @SHIP_TO, @MARK_FOR, @DOC_DATE, @NMFC, @TYPE_CARGO, @PS, @UP, "
                + "@UNIT_WEIGHT, @UNIT_CUBE,@UFC,@SL,@FRGHT_CLASS_NOM, @ITEM_NOM, @HCC_MSG,@DMIL, @JON,@HCC, @CIIC, @TY_CARGO_MSG,@MSDS, @DOCUMENT_NUMBER,@RICUIQTY, @PCN, @date_value, @serial_number, @iteration "
               
            +");  SELECT SCOPE_IDENTITY()";

            //update offload detail
            string updateStmt = "UPDATE offload_details SET "
             + "DOC_IDENT=@DOC_IDENT, RI_FROM=@RI_FROM, M_S=@M_S, SER=@SER, SUPP_ADDRESS=@SUPP_ADDRESS, SIG=@SIG, FUND=@FUND, DISTRIBUTION=@DISTRIBUTION, PROJECT=@PROJECT, PRI=@PRI, REQ_DEL_DATE=@REQ_DEL_DATE, RI=@RI, O_P=@O_P, COND=@COND,  "
                + "MGT=@MGT, UNIT_DOLLARS=@UNIT_DOLLARS, UNIT_CTS=@UNIT_CTS,TOTAL_DOLLARS=@TOTAL_DOLLARS, TOTAL_CTS=@TOTAL_CTS, SHIP_FROM=@SHIP_FROM, SHIP_TO=@SHIP_TO, MARK_FOR=@MARK_FOR, DOC_DATE=@DOC_DATE, NMFC=@NMFC, TYPE_CARGO=@TYPE_CARGO, PS=@PS, UP=@UP, "
                + "UNIT_WEIGHT=@UNIT_WEIGHT, UNIT_CUBE=@UNIT_CUBE,UFC=@UFC,SL=@SL,FRGHT_CLASS_NOM=@FRGHT_CLASS_NOM, ITEM_NOM=@ITEM_NOM, HCC_MSG=@HCC_MSG,DMIL=@DMIL, JON=@JON,HCC=@HCC, CIIC=@CIIC, TY_CARGO_MSG=@TY_CARGO_MSG,MSDS=@MSDS, DOCUMENT_NUMBER=@DOCUMENT_NUMBER,RICUIQTY=@RICUIQTY, PCN=@PCN, date_value=@date_value "

            + " WHERE offload_detail_id=@offload_detail_id";

            if (offload_detail_id != 0)
                insertStmt = updateStmt;

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@DOC_IDENT", dbNull(offloadDetails.DOC_IDENT));
            cmd.Parameters.AddWithValue("@RI_FROM", dbNull(offloadDetails.RI_FROM));
            cmd.Parameters.AddWithValue("@M_S", dbNull(offloadDetails.M_S));
            cmd.Parameters.AddWithValue("@SER", dbNull(offloadDetails.SER));
            cmd.Parameters.AddWithValue("@SUPP_ADDRESS", dbNull(offloadDetails.SUPP_ADDRESS));
            cmd.Parameters.AddWithValue("@SIG", dbNull(offloadDetails.SIG));
            cmd.Parameters.AddWithValue("@FUND", dbNull(offloadDetails.FUND));
            cmd.Parameters.AddWithValue("@DISTRIBUTION", dbNull(offloadDetails.DISTRIBUTION));
            cmd.Parameters.AddWithValue("@PROJECT", dbNull(offloadDetails.PROJECT));
            cmd.Parameters.AddWithValue("@PRI", dbNull(offloadDetails.PRI));

            cmd.Parameters.AddWithValue("@REQ_DEL_DATE", dbNull(offloadDetails.REQ_DEL_DATE));
            cmd.Parameters.AddWithValue("@RI", dbNull(offloadDetails.RI));
            cmd.Parameters.AddWithValue("@O_P", dbNull(offloadDetails.O_P));
            cmd.Parameters.AddWithValue("@COND", dbNull(offloadDetails.COND));
            cmd.Parameters.AddWithValue("@MGT", dbNull(offloadDetails.MGT));
            cmd.Parameters.AddWithValue("@UNIT_DOLLARS", dbNull(offloadDetails.UNIT_DOLLARS));
            cmd.Parameters.AddWithValue("@UNIT_CTS", dbNull(offloadDetails.UNIT_CTS));
            cmd.Parameters.AddWithValue("@TOTAL_DOLLARS", dbNull(offloadDetails.TOTAL_DOLLARS));
            cmd.Parameters.AddWithValue("@TOTAL_CTS", dbNull(offloadDetails.TOTAL_CTS));
            cmd.Parameters.AddWithValue("@SHIP_FROM", dbNull(offloadDetails.SHIP_FROM));
            cmd.Parameters.AddWithValue("@SHIP_TO", dbNull(offloadDetails.SHIP_TO));

            cmd.Parameters.AddWithValue("@MARK_FOR", dbNull(offloadDetails.MARK_FOR));
            cmd.Parameters.AddWithValue("@DOC_DATE", dbNull(offloadDetails.DOC_DATE));
            cmd.Parameters.AddWithValue("@NMFC", dbNull(offloadDetails.NMFC));
            cmd.Parameters.AddWithValue("@TYPE_CARGO", dbNull(offloadDetails.TYPE_CARGO));
            cmd.Parameters.AddWithValue("@PS", dbNull(offloadDetails.PS));
            cmd.Parameters.AddWithValue("@UP", dbNull(offloadDetails.UP));
            cmd.Parameters.AddWithValue("@UNIT_WEIGHT", dbNull(offloadDetails.UNIT_WEIGHT));
            cmd.Parameters.AddWithValue("@UNIT_CUBE", dbNull(offloadDetails.UNIT_CUBE));
            cmd.Parameters.AddWithValue("@UFC", dbNull(offloadDetails.UFC));
            cmd.Parameters.AddWithValue("@SL", dbNull(offloadDetails.SL));

            cmd.Parameters.AddWithValue("@FRGHT_CLASS_NOM", dbNull(offloadDetails.FRGHT_CLASS_NOM));
            cmd.Parameters.AddWithValue("@ITEM_NOM", dbNull(offloadDetails.ITEM_NOM));
            cmd.Parameters.AddWithValue("@HCC_MSG", dbNull(offloadDetails.HCC_MSG));
            cmd.Parameters.AddWithValue("@DMIL", dbNull(offloadDetails.DMIL));
            cmd.Parameters.AddWithValue("@JON", dbNull(offloadDetails.JON));
            cmd.Parameters.AddWithValue("@HCC", dbNull(offloadDetails.HCC));
            cmd.Parameters.AddWithValue("@CIIC", dbNull(offloadDetails.CIIC));
            cmd.Parameters.AddWithValue("@TY_CARGO_MSG", dbNull(offloadDetails.TY_CARGO_MSG));
            cmd.Parameters.AddWithValue("@MSDS", dbNull(offloadDetails.MSDS));
            cmd.Parameters.AddWithValue("@DOCUMENT_NUMBER", dbNull(offloadDetails.DOCUMENT_NUMBER));

            cmd.Parameters.AddWithValue("@RICUIQTY", dbNull(offloadDetails.RICUIQTY));
            cmd.Parameters.AddWithValue("@PCN", dbNull(offloadDetails.PCN));
            cmd.Parameters.AddWithValue("@date_value", dbNull(offloadDetails.date_value));

            if (offload_detail_id == 0)
            {
                int serial = 1;
                int iteration = 1;
                DataTable offloadDetailSerial = getNextOffloadDetailSerialIteration();
                if (offloadDetailSerial.Rows.Count!=0)
                {
                    serial = Convert.ToInt32(offloadDetailSerial.Rows[0]["serial_number"]);
                    iteration = Convert.ToInt32(offloadDetailSerial.Rows[0]["iteration"]);

                    if (serial == 99)
                    {
                        iteration++;
                        serial = 0;
                    }
                }

                cmd.Parameters.AddWithValue("@serial_number", dbNull(++serial));
                cmd.Parameters.AddWithValue("@iteration", dbNull(iteration));


                object o = cmd.ExecuteScalar();
                offload_detail_id = Convert.ToInt32(o);
            }
            else
            {
                cmd.Parameters.AddWithValue("@offload_detail_id", dbNull(offload_detail_id));
                cmd.ExecuteNonQuery();
                
            }


            if (!isGarbageItem)
            {
                //update offload_list item    
                insertStmt = "UPDATE offload_list set offload_detail_id=@offload_detail_id WHERE offload_list_id=@offload_list_id";

                SqlCommand cmd3 = new SqlCommand(insertStmt, conn);
                cmd3.Transaction = tran;

                cmd3.Parameters.AddWithValue("@offload_detail_id", dbNull(offload_detail_id));
                cmd3.Parameters.AddWithValue("@offload_list_id", dbNull(offload_list_id));

                cmd3.ExecuteNonQuery();
            }
            else
            {
                //update garbage item    
                insertStmt = "UPDATE garbage_offload_list set offload_detail_id=@offload_detail_id WHERE garbage_offload_id=@garbage_offload_id";

                SqlCommand cmd3 = new SqlCommand(insertStmt, conn);
                cmd3.Transaction = tran;

                cmd3.Parameters.AddWithValue("@offload_detail_id", dbNull(offload_detail_id));
                cmd3.Parameters.AddWithValue("@garbage_offload_id", dbNull(offload_list_id));

                cmd3.ExecuteNonQuery();

            }
           
            
            tran.Commit();

            conn.Close();

            //returns a list of ids just so that the unit test can delete what it creates.
            return offload_detail_id;

        }

        public int saveCosalNiinAllowance(int? cosal_niin_id, string cosal, string niin, int at, int allowance_qty)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            //get cosal_niin_id for this cosal/niin combination if it already exists:
            //if (cosal_niin_id == null)
            //{
                string selectStmt = "select cosal_niin_id from cosal_niin_allowances where cosal=@cosal AND niin=@niin";

                SqlCommand cmd2 = new SqlCommand(selectStmt, conn);
                cmd2.Transaction = tran;

                cmd2.Parameters.AddWithValue("@cosal", dbNull(cosal));
                cmd2.Parameters.AddWithValue("@niin", dbNull(niin));               

                object o = cmd2.ExecuteScalar();
                if(o!=null && !o.Equals(""))
                    cosal_niin_id = Convert.ToInt32(o);
            //}
            
            //insert new cosal_niin_allowances
            if (cosal_niin_id == null)
            {
                string insertStmt = "insert into cosal_niin_allowances (cosal, niin, at, allowance_qty)"
                + "VALUES (@cosal, @niin, @at, @allowance_qty);  SELECT SCOPE_IDENTITY()";

                SqlCommand cmd = new SqlCommand(insertStmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
                cmd.Parameters.AddWithValue("@niin", dbNull(niin));
                cmd.Parameters.AddWithValue("@at", dbNull(at));
                cmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));

                object o2 = cmd.ExecuteScalar();
                cosal_niin_id = Convert.ToInt32(o2);
            }
            else
            {
                //update existing cosal_niin_allowances
                String updateStmt = "update cosal_niin_allowances set cosal=@cosal, "
                    + "niin=@niin, at=@at, allowance_qty=@allowance_qty where cosal_niin_id=@cosal_niin_id";

                SqlCommand cmd3 = new SqlCommand(updateStmt, conn);
                cmd3.Transaction = tran;
                cmd3.Parameters.AddWithValue("@cosal", dbNull(cosal));
                cmd3.Parameters.AddWithValue("@niin", dbNull(niin));
                cmd3.Parameters.AddWithValue("@at", dbNull(at));
                cmd3.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                cmd3.Parameters.AddWithValue("@cosal_niin_id", dbNull(cosal_niin_id));

                cmd3.ExecuteNonQuery();

            }

            //update matching inventory AT values:        
            String updateInvStmt = "UPDATE inventory set at=@at WHERE cosal=@cosal AND inventory_id IN (select inventory_id from inventory AS i INNER JOIN vMfgCatNiinCat AS v"
                + " ON (i.mfg_catalog_id=v.mfg_catalog_id) WHERE niin=@niin)";

            SqlCommand cmd4 = new SqlCommand(updateInvStmt, conn);
                cmd4.Transaction = tran;
                cmd4.Parameters.AddWithValue("@cosal", dbNull(cosal));
                cmd4.Parameters.AddWithValue("@niin", dbNull(niin));
                cmd4.Parameters.AddWithValue("@at", dbNull(at));

                cmd4.ExecuteNonQuery();           

            tran.Commit();

            conn.Close();

            return cosal_niin_id.Value;

        }

        public List<int> createIssue(List<VolumeOffloadItem> volumes, int mfg_catalog_id, int issued_qty, int authorized_user_id, int workcenter_id, string shelf_life_expiration_date, string manufacturer_date, int inventory_id, int decremented_qty, string alternate_ui,
            string batch_number, string lot_number,string contract_number, string alternate_um, string cosal, string atm)
        {

            List<int> idList = new List<int>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            

            //insert issue
            string insertStmt = "insert into issues (atm, alternate_ui, mfg_catalog_id, issued_qty, authorized_user_id, date_issued, workcenter_id, shelf_life_expiration_date, manufacturer_date, batch_number, lot_number, contract_number, alternate_um, cosal)"
            + "VALUES (@atm, @alternate_ui, @mfg_catalog_id, @issued_qty, @authorized_user_id, @date_issued, @workcenter_id, @shelf_life_expiration_date, @manufacturer_date, @batch_number, @lot_number, @contract_number, @alternate_um, @cosal);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@atm", dbNull(atm));
            cmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            cmd.Parameters.AddWithValue("@issued_qty", dbNull(issued_qty));
            cmd.Parameters.AddWithValue("@authorized_user_id", dbNull(authorized_user_id));
            cmd.Parameters.AddWithValue("@date_issued", dbNull(DateTime.Now));
            cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));
            cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
            cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));

            cmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            cmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            cmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));
            cmd.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));

            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));

            object o = cmd.ExecuteScalar();
            int issue_id = Convert.ToInt32(o);

            //grab default location for issued inventory based on workcenter
            int location_id = findNewlyIssuedLocationForWorkcenter(workcenter_id);

            //insert inventory
            insertStmt = "insert into inventory (" +
            "shelf_life_expiration_date, manufacturer_date,  location_id, qty, serial_number,"
            + "bulk_item, mfg_catalog_id, alternate_ui, batch_number, lot_number, contract_number, alternate_um, cosal, atm"

            + ")"
            + "VALUES(" +
             "@shelf_life_expiration_date, @manufacturer_date,  @location_id, @qty, @serial_number,"
            + "@bulk_item, @mfg_catalog_id, @alternate_ui, @batch_number, @lot_number, @contract_number, @alternate_um, @cosal, @atm"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd2 = new SqlCommand(insertStmt, conn);
            cmd2.Transaction = tran;

            cmd2.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
            cmd2.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
            cmd2.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd2.Parameters.AddWithValue("@qty", dbNull(issued_qty));
            cmd2.Parameters.AddWithValue("@serial_number", dbNull(null));
            cmd2.Parameters.AddWithValue("@bulk_item", dbNull(true));
            cmd2.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            cmd2.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));

            cmd2.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            cmd2.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            cmd2.Parameters.AddWithValue("@contract_number", dbNull(contract_number));

            cmd2.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));

            cmd2.Parameters.AddWithValue("@cosal", dbNull(cosal));
            cmd2.Parameters.AddWithValue("@atm", dbNull(atm));
            
            object o2 = cmd2.ExecuteScalar();
            int new_inventory_id = Convert.ToInt32(o2);

            //decrement parent inventory           
            insertStmt = "UPDATE inventory set qty=@qty WHERE inventory_id=@inventory_id";
            String deleteStmt = "DELETE from inventory WHERE inventory_id = @inventory_id";

            if (decremented_qty > 0)
            {
                SqlCommand cmd3 = new SqlCommand(insertStmt, conn);
                cmd3.Transaction = tran;

                cmd3.Parameters.AddWithValue("@qty", dbNull(decremented_qty));
                cmd3.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));

                cmd3.ExecuteNonQuery();
            }
            else
            {
                SqlCommand cmd3 = new SqlCommand(deleteStmt, conn);
                cmd3.Transaction = tran;
                cmd3.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));

                cmd3.ExecuteNonQuery();
            }

            insertStmt = "insert into volumes_in_issue (issue_id, volume_id) VALUES (@issue_id, @volume_id)";

            SqlCommand insertCmd2 = new SqlCommand(insertStmt, conn);
            insertCmd2.Transaction = tran;

            foreach (VolumeOffloadItem item in volumes)
            {
                //insert rows
                insertCmd2.Parameters.AddWithValue("@issue_id", dbNull(issue_id));
                insertCmd2.Parameters.AddWithValue("@volume_id", dbNull(item.volume));

                insertCmd2.ExecuteNonQuery();
                insertCmd2.Parameters.Clear();
            }

            tran.Commit();

            conn.Close();

            idList.Add(issue_id);
            idList.Add(new_inventory_id);

            //returns a list of ids just so that the unit test can delete what it creates.
            return idList;

        }

        public int insertLocation(string name, int workcenter_id)
        {
            
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = "insert into locations (name,workcenter_id) VALUES (@name,@workcenter_id);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@name", dbNull(name));
            cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));

            object o = cmd.ExecuteScalar();
            int location_id = Convert.ToInt32(o);

            conn.Close();

            return location_id;

        }

        public int insertAdminUser(string username, string role)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = "insert into user_roles (username, role) VALUES (@username, @role);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@username", dbNull(username));
            cmd.Parameters.AddWithValue("@role", dbNull(role));
            
            object o = cmd.ExecuteScalar();
            int user_role_id = Convert.ToInt32(o);

            conn.Close();

            return user_role_id;

        }

        public int insertInsurvLocationChangeRecord(int location_id, int? insurv_location_cr_id, bool location_done, DateTime action_complete, string surv_name)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = null;
            
            if(insurv_location_cr_id==null)
            insertStmt=  "insert into insurv_location_cr (location_id, location_done, action_complete, surv_name) VALUES (@location_id, @location_done, @action_complete, @surv_name);  SELECT SCOPE_IDENTITY()";
            else
                insertStmt = "update insurv_location_cr set location_id=@location_id, location_done=@location_done, action_complete=@action_complete, surv_name=@surv_name"
                    + " WHERE insurv_location_cr_id=@insurv_location_cr_id";
           


            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@location_done", dbNull(location_done));
            cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
            cmd.Parameters.AddWithValue("@surv_name", dbNull(surv_name));

           
            if (insurv_location_cr_id == null)
            {
                object o = cmd.ExecuteScalar();
                insurv_location_cr_id = Convert.ToInt32(o);
            }
            else
            {
                cmd.Parameters.AddWithValue("@insurv_location_cr_id", dbNull(insurv_location_cr_id));
                cmd.ExecuteNonQuery();                
            }

            conn.Close();

            return insurv_location_cr_id.Value;

        }


        public List<int> insertAtmChangeRecords(int location_id, DataTable bromineRecords, string A, string B, DateTime action_complete)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //create transaction
            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into atm_tag_audit " +
                "(action_complete, A,B, mfg_catalog_id, location_id, shelf_life_expiration_date, qty, alternate_ui, contract_number, batch_number, lot_number, manufacturer_date) VALUES " +
                "(@action_complete, @A,@B, @mfg_catalog_id, @location_id, @shelf_life_expiration_date, @qty, @alternate_ui, @contract_number, @batch_number, @lot_number, @manufacturer_date); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;
            List<int> idList = new List<int>();
            foreach (DataRow row in bromineRecords.Rows)
            {
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@A", dbNull(A));
                cmd.Parameters.AddWithValue("@B", dbNull(B));               
                cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(row["mfg_catalog_id"]));
                cmd.Parameters.AddWithValue("@location_id", dbNull(row["location_id"]));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(row["shelf_life_expiration_date"]));
                cmd.Parameters.AddWithValue("@qty", dbNull(row["qty"]));
                cmd.Parameters.AddWithValue("@alternate_ui", dbNull(row["alternate_ui"]));
                cmd.Parameters.AddWithValue("@contract_number", dbNull(row["contract_number"]));
                cmd.Parameters.AddWithValue("@batch_number", dbNull(row["batch_number"]));
                cmd.Parameters.AddWithValue("@lot_number", dbNull(row["lot_number"]));
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(row["manufacturer_date"]));


                object o = cmd.ExecuteScalar();
                int bromine_id = Convert.ToInt32(o);
                idList.Add(bromine_id);

                cmd.Parameters.Clear();

                cmd.Parameters.Clear();
            }

            //commit
            tran.Commit();

            //close transaction
            conn.Close();

            return idList;


        }

        public List<int> insertBromineChangeRecords(int location_id, DataTable bromineRecords, string A, string B, string C, string D, DateTime action_complete)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //create transaction
            SqlTransaction tran = conn.BeginTransaction();            

            string insertStmt = "insert into bromine_audit "+
                "(action_complete, A,B,C,D, mfg_catalog_id, location_id, shelf_life_expiration_date, qty, alternate_ui, contract_number, batch_number, lot_number, manufacturer_date) VALUES " +
                "(@action_complete, @A,@B,@C,@D, @mfg_catalog_id, @location_id, @shelf_life_expiration_date, @qty, @alternate_ui, @contract_number, @batch_number, @lot_number, @manufacturer_date);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;
            List<int> bromineIdList = new List<int>();
            foreach (DataRow row in bromineRecords.Rows)
            {
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@A", dbNull(A));
                cmd.Parameters.AddWithValue("@B", dbNull(B));
                cmd.Parameters.AddWithValue("@C", dbNull(C));
                cmd.Parameters.AddWithValue("@D", dbNull(D));
                cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(row["mfg_catalog_id"]));
                cmd.Parameters.AddWithValue("@location_id", dbNull(row["location_id"]));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(row["shelf_life_expiration_date"]));
                cmd.Parameters.AddWithValue("@qty", dbNull(row["qty"]));
                cmd.Parameters.AddWithValue("@alternate_ui", dbNull(row["alternate_ui"]));
                cmd.Parameters.AddWithValue("@contract_number", dbNull(row["contract_number"]));
                cmd.Parameters.AddWithValue("@batch_number", dbNull(row["batch_number"]));
                cmd.Parameters.AddWithValue("@lot_number", dbNull(row["lot_number"]));
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(row["manufacturer_date"]));
                
                object o = cmd.ExecuteScalar();
                int bromine_id = Convert.ToInt32(o);
                bromineIdList.Add(bromine_id);

                cmd.Parameters.Clear();
            }

            tran.Commit();

            conn.Close();

            return bromineIdList;

        }

        public void deleteBromineChangeRecords(List<int> bromindIds)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = "Delete from bromine_audit where bromine_id=@bromine_id";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
           
            foreach (int bromine_id in bromindIds)
            {
                cmd.Parameters.AddWithValue("@bromine_id", dbNull(bromine_id));

                cmd.ExecuteNonQuery();

                cmd.Parameters.Clear();
            }

            conn.Close();
        }

        public List<int> insertCalciumChangeRecords(int location_id, DataTable calciumRecords, string A, string B, string C, string D,
            string E, string F, string G, string H, string I, string J, DateTime action_complete)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //create transaction
            SqlTransaction tran = conn.BeginTransaction();


            string insertStmt = "insert into calcium_hypochlorite_audit " +
                "(action_complete, A,B,C,D,E,F,G,H,I,J, mfg_catalog_id, location_id, shelf_life_expiration_date, qty, alternate_ui, contract_number, batch_number, lot_number, manufacturer_date) VALUES " +
                "(@action_complete, @A,@B,@C,@D,@E,@F,@G,@H,@I,@J, @mfg_catalog_id, @location_id, @shelf_life_expiration_date, @qty, @alternate_ui, @contract_number, @batch_number, @lot_number, @manufacturer_date);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;
            List<int> bromineIdList = new List<int>();
            foreach (DataRow row in calciumRecords.Rows)
            {
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@A", dbNull(A));
                cmd.Parameters.AddWithValue("@B", dbNull(B));
                cmd.Parameters.AddWithValue("@C", dbNull(C));
                cmd.Parameters.AddWithValue("@D", dbNull(D));
                cmd.Parameters.AddWithValue("@E", dbNull(E));
                cmd.Parameters.AddWithValue("@F", dbNull(F));
                cmd.Parameters.AddWithValue("@G", dbNull(G));
                cmd.Parameters.AddWithValue("@H", dbNull(H));
                cmd.Parameters.AddWithValue("@I", dbNull(I));
                cmd.Parameters.AddWithValue("@J", dbNull(J));
                cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(row["mfg_catalog_id"]));
                cmd.Parameters.AddWithValue("@location_id", dbNull(row["location_id"]));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(row["shelf_life_expiration_date"]));
                cmd.Parameters.AddWithValue("@qty", dbNull(row["qty"]));
                cmd.Parameters.AddWithValue("@alternate_ui", dbNull(row["alternate_ui"]));
                cmd.Parameters.AddWithValue("@contract_number", dbNull(row["contract_number"]));
                cmd.Parameters.AddWithValue("@batch_number", dbNull(row["batch_number"]));
                cmd.Parameters.AddWithValue("@lot_number", dbNull(row["lot_number"]));
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(row["manufacturer_date"]));

                object o = cmd.ExecuteScalar();
                int bromine_id = Convert.ToInt32(o);
                bromineIdList.Add(bromine_id);

                cmd.Parameters.Clear();
            }

            tran.Commit();

            conn.Close();

            return bromineIdList;

        }

        public void deleteCalciumChangeRecords(List<int> calciumIds)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "Delete from calcium_hypochlorite_audit where calcium_hypochlorite_id=@calcium_hypochlorite_id";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            foreach (int calcium_hypochlorite_id in calciumIds)
            {
                cmd.Parameters.AddWithValue("@calcium_hypochlorite_id", dbNull(calcium_hypochlorite_id));

                cmd.ExecuteNonQuery();

                cmd.Parameters.Clear();
            }

            conn.Close();
        }


        public void deleteTagChangeRecords(List<int> tagIds)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "Delete from atm_tag_audit where atm_id=@id";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            foreach (int tagId in tagIds)
            {
                cmd.Parameters.AddWithValue("@id", dbNull(tagId));

                cmd.ExecuteNonQuery();

                cmd.Parameters.Clear();
            }

            conn.Close();
        }

        public int insertInsurvNiin(string surv_name, int? niin_catalog_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "insert into insurv_niin (surv_name, niin_catalog_id) VALUES (@surv_name, @niin_catalog_id);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@surv_name", dbNull(surv_name));
            cmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));

            object o = cmd.ExecuteScalar();
            int insurv_id = Convert.ToInt32(o);

            conn.Close();

            return insurv_id;

        }

        public int insertVolumeIgnoredNiin(string niin)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "insert into volume_ignored_niins (niin) VALUES (@niin);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@niin", dbNull(niin));

            object o = cmd.ExecuteScalar();
            int ignored_niin_id = Convert.ToInt32(o);

            conn.Close();

            return ignored_niin_id;

        }

        public void deletedVolumeIgnoredNiin(string niin)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "delete from volume_ignored_niins where niin=@niin;";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@niin", dbNull(niin));

            cmd.ExecuteNonQuery();

            conn.Close();
            
        }

        public List<int> getWorkcenterListByUser(string username)
        {
            List<int> workList = new List<int>();
            
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select u.workcenter_id from authorized_users u " +
                          "where u.username = @username";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@username", username);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                workList.Add(Convert.ToInt32(rdr["workcenter_id"]));
            }

            con.Close();

            return workList;
        }

        public int insertAuthorizedUser(string firstname, string lastname,string username, int workcenter_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "insert into authorized_users (firstname, lastname, username, workcenter_id, deleted) VALUES (@firstname, @lastname, @username, @workcenter_id, @deleted);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@firstname", dbNull(firstname));
            cmd.Parameters.AddWithValue("@lastname", dbNull(lastname));
            cmd.Parameters.AddWithValue("@username", dbNull(username));
            cmd.Parameters.AddWithValue("@deleted", dbNull(false));
            cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));

            object o = cmd.ExecuteScalar();
            int authorized_user_id = Convert.ToInt32(o);

            conn.Close();

            return authorized_user_id;

        }

        public void flagAuthorizedUserDeleted(int authorized_user_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "update authorized_users set deleted=@deleted where authorized_user_id=@authorized_user_id";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@deleted", dbNull(true));
            cmd.Parameters.AddWithValue("@authorized_user_id", dbNull(authorized_user_id));
            

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public void insertWorkcenter(string wid, string description)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "insert into workcenter (wid,description) VALUES (@wid,@description); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@wid", dbNull(wid));
            cmd.Parameters.AddWithValue("@description", dbNull(description));

            object o = cmd.ExecuteScalar();
            int workcenter_id = Convert.ToInt32(o);

            conn.Close();

            insertLocation("NEWLY ISSUED", workcenter_id);

        }



        public void insertHHDecantingLocations(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            SqlCommand cmd = new SqlCommand("select * from locations ", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            //sqlite connection and statement
            string liteStmt = "insert into locations (location_id,name,workcenter_id) VALUES (@location_id,@name,@workcenter_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"].ToString());
            string name = null;
                try
                {
                     name = rdr["name"].ToString();
                }
                catch {}
                string wid = null;
                try
                {
                     wid = rdr["workcenter_id"].ToString();
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@name", dbNull(name));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@workcenter_id", dbNull(wid));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHLocationsByAudit(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int auditid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            List<int> locationList = new List<int>();
            SqlCommand cmd = new SqlCommand("select location_id from locations_in_audit where inventory_audit_id = @auditid", mdfConn);
            cmd.Parameters.AddWithValue("auditid", auditid);
            SqlDataReader rdr = cmd.ExecuteReader();

            //loop through results and add them to the list
            while (rdr.Read())
            {
                locationList.Add(Convert.ToInt32(rdr["location_id"]));
            }

            //close reader            
            rdr.Close();
            cmd.Dispose();


            //check to make sure at least one location is associated with this audit
            if (locationList.Count < 1)
            {
                //close connection
                mdfConn.Close();

                throw new Exception("No locations have been added to this audit");

            }

            string locStr = "";
            int count = 1;

            foreach (int i in locationList)
            {

                if (count != locationList.Count)
                {

                    locStr += i + ",";
                }
                else
                {

                    locStr += i;

                }

                count++;
            }


            //sqlite connection and statement
            string liteStmt = "insert into locations (location_id,name,workcenter_id) VALUES (@location_id,@name,@workcenter_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            cmd = new SqlCommand("select * from locations where location_id in(" + locStr + ")", mdfConn);
            rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"].ToString());
                string name = rdr["name"].ToString();
                string wid = rdr["workcenter_id"].ToString();


                liteCmd.Parameters.AddWithValue("@name", name);
                liteCmd.Parameters.AddWithValue("@location_id", location_id);
                liteCmd.Parameters.AddWithValue("@workcenter_id", wid);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHLocationCR(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int auditid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //first, get the list of locations associated with the selected audit
            List<int> locationList = new List<int>();
            SqlCommand cmd = new SqlCommand("select location_id from locations_in_audit where inventory_audit_id = @auditid", mdfConn);
            cmd.Parameters.AddWithValue("@auditid", auditid);
            SqlDataReader rdr = cmd.ExecuteReader();

            //loop through results and add them to the list
            while (rdr.Read())
            {
                locationList.Add(Convert.ToInt32(rdr["location_id"]));
            }

            //close reader            
            rdr.Close();
            cmd.Dispose();


            //check to make sure at least one location is associated with this audit
            if (locationList.Count < 1)
            {
                //close connection
                mdfConn.Close();

                throw new Exception("No locations have been added to this audit");

            }

            string locStr = "";
            int count = 1;

            foreach (int i in locationList)
            {

                if (count != locationList.Count)
                {

                    locStr += i + ",";
                }
                else
                {

                    locStr += i;

                }

                count++;
            }

            //sqlite connection and statement
            string liteStmt = "insert into location_cr (location_id,location_done,action_complete, count) VALUES (@location_id,@location_done,@action_complete, @count)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            cmd.Parameters.Clear();
            // cmd = new SqlCommand("select * from location_cr where location_id in(" + locStr + ")", mdfConn);
            cmd = new SqlCommand("select * from location_cr where inventory_audit_id = @auditid", mdfConn);
            cmd.Parameters.AddWithValue("@auditid", auditid);
            rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"]);
                bool location_done = Convert.ToBoolean(rdr["location_done"]);

                DateTime? action_complete = null;
                try
                {
                    action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch {}


                int? locCount = null;

                try
                {
                    locCount = Convert.ToInt32(rdr["count"]);
                }
                catch {}


                liteCmd.Parameters.AddWithValue("@location_id",dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@location_done", dbNull(location_done));
                liteCmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                liteCmd.Parameters.AddWithValue("@count", dbNull(locCount));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }        

        public void insertHHInventoryAuditCR(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int auditid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            SqlConnection mdfConn2 = new SqlConnection(MDF_CONNECTION);
            SqlConnection mdfConn3 = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();
            mdfConn2.Open();
            mdfConn3.Open();


            //sqlite connection and statement
            string liteStmt = "insert into inventory_audit_cr (inventory_audit_id,location_id,mfg_catalog_id,qty,bulk_item,serial_number,shelf_life_expiration_date,current_volume,action_complete,username,device_type, ui, um,cosal, count, manufacture_date, atmosphere_control_number) " +
                              "VALUES (@inventory_audit_id,@location_id,@mfg_catalog_id,@qty,@bulk_item,@serial_number,@shelf_life_expiration_date,@current_volume,@action_complete,@username,@device_type, @ui, @um, @cosal, @count, @manDate, @atmosphere_control_number)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select * from inventory_audit_cr " +
                          "where action_complete > " +
                          "(select MAX(action_complete) from location_cr where inventory_audit_id = @audit and location_done = 0) " +
                          "and inventory_audit_id = @audit";
  
            string stmt2 = "select n.ui from niin_catalog n, mfg_catalog m " +
                          "where m.mfg_catalog_id = @catid " + 
                          "AND n.niin_catalog_id = m.niin_catalog_id";


            string stmt3 = "select n.um from niin_catalog n, mfg_catalog m " +
                            "where m.mfg_catalog_id = @catid " +
                            "AND n.niin_catalog_id = m.niin_catalog_id";

            SqlCommand uiCmd = new SqlCommand(stmt2, mdfConn2);
            SqlCommand umCmd = new SqlCommand(stmt3, mdfConn3);

            //SqlCommand cmd = new SqlCommand("select * from inventory_audit_cr where inventory_audit_id = @auditid", mdfConn);
            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            cmd.Parameters.AddWithValue("audit", auditid);
            SqlDataReader rdr = cmd.ExecuteReader();
           

            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int inventory_audit_id = Convert.ToInt32(rdr["inventory_audit_id"]);
                int location_id = Convert.ToInt32(rdr["location_id"]);
                int? mfg_catalog_id = null;
                try
                {
                    mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}

                int? qty = null;
                try
                {
                    qty = Convert.ToInt32(rdr["qty"]);
                }
                catch {}

                bool bulk_item = Convert.ToBoolean(rdr["bulk_item"]);


                string serial_number = null;
                try
                {
                    serial_number = rdr["serial_number"].ToString();
                }
                catch {}

                DateTime? shelf_life_expiration_date = null;
                try
                {
                    shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}

                string current_volume = null;
                try
                {
                    current_volume = rdr["current_volume"].ToString();
                }
                catch {}

                DateTime? action_complete = null;
                try
                {
                    action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch {}


                string username = null;
                try
                {
                    username = rdr["username"].ToString();
                }
                catch {}


                string ui = null;
                try
                {
                    ui = rdr["alternate_ui"].ToString();
                }
                catch {}                


                //check the ui value.  if it is null insert the value from niin_catalog
                if (ui == null || ui.Equals(""))
                {
                    uiCmd.Parameters.Clear();
                    uiCmd.Parameters.AddWithValue("@catid", mfg_catalog_id);
                    ui = Convert.ToString(uiCmd.ExecuteScalar());

                }

                string alternateUm = null;

                try
                {
                    alternateUm = Convert.ToString(rdr["alternate_um"]);
                }
                catch {}

                if (alternateUm == null || alternateUm.Equals(""))
                {
                    umCmd.Parameters.Clear();
                    umCmd.Parameters.AddWithValue("catid", mfg_catalog_id);
                    try
                    {
                        alternateUm = Convert.ToString(umCmd.ExecuteScalar());
                    }
                    catch {}
                }


                string cosal = "";

                try
                {
                    cosal = rdr["cosal"].ToString();
                }
                catch {}

                DateTime? manDate = null;
                try
                {
                    manDate = Convert.ToDateTime(rdr["manufacturer_date"]);
                }
                catch {}

                int? count = null;
                try
                {
                    count = Convert.ToInt32(rdr["count"]);
                }
                catch {}

                string atm = null;
                try
                {
                    atm = Convert.ToString(rdr["atm"]);
                }
                catch {}

                string device_type = rdr["device_type"].ToString();
               // int parent_id = Convert.ToInt32(rdr["parent_id"]);
              //  string device_id = rdr["device_id"].ToString();


                liteCmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(inventory_audit_id));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@qty", dbNull(qty));
                liteCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
                liteCmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                liteCmd.Parameters.AddWithValue("@current_volume", dbNull(current_volume));
                liteCmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                liteCmd.Parameters.AddWithValue("@username", dbNull(username));
                liteCmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@um", dbNull(alternateUm));
                liteCmd.Parameters.AddWithValue("@count", dbNull(count));
                liteCmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
                liteCmd.Parameters.AddWithValue("@manDate", dbNull(manDate));
               // liteCmd.Parameters.AddWithValue("@parent_id", parent_id);
               // liteCmd.Parameters.AddWithValue("@device_id", device_id);

                liteCmd.Parameters.AddWithValue("@atmosphere_control_number", dbNull(atm));
                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
            mdfConn2.Close();
            mdfConn3.Close();


        }


        public void insertHHDecantingNiinCatalogByTask(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int taskid)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into niin_catalog (niin_catalog_id,fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,shelf_life_code_id,shelf_life_action_code_id, remarks,storage_type_id,cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,allowance_qty,created,ship_id) " +
                              "VALUES (@niin_catalog_id,@fsc,@niin,@ui,@um,@usage_category_id,@description,@smcc_id,@specs,@shelf_life_code_id,@shelf_life_action_code_id, @remarks,@storage_type_id,@cog_id,@spmig,@nehc_rpt,@catalog_group_id,@catalog_serial_number,@allowance_qty,@created,@ship_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from niin_catalog n where n.niin_catalog_id in(select niin_catalog_id from inventory_in_decant_task i INNER JOIN mfg_catalog m ON (i.mfg_catalog_id=m.mfg_catalog_id) where decanting_task_id = @decanting)", mdfConn);
            cmd.Parameters.AddWithValue("@decanting", taskid);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"].ToString());
                int fsc = Convert.ToInt32(rdr["fsc"]);
                string niin = Convert.ToString(rdr["niin"]);
                string ui = Convert.ToString(rdr["ui"]);
                string um = Convert.ToString(rdr["um"]);
                int usage_category_id = Convert.ToInt32(rdr["usage_category_id"]);
                string description = Convert.ToString(rdr["description"]);
                int smcc_id = Convert.ToInt32(rdr["smcc_id"]);
                string specs = Convert.ToString(rdr["specs"]);
                int shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);
                int shelf_life_action_code_id = Convert.ToInt32(rdr["shelf_life_action_code_id"]);
                string remarks = Convert.ToString(rdr["remarks"]);
                int storage_type_id = Convert.ToInt32(rdr["storage_type_id"]);
                int cog_id = Convert.ToInt32(rdr["cog_id"]);
                string spmig = rdr["spmig"].ToString();
                string nehc_rpt = rdr["nehc_rpt"].ToString();
                int catalog_group_id = Convert.ToInt32(rdr["catalog_group_id"]);
                string catalog_serial_number = rdr["catalog_serial_number"].ToString();
                int allowance_qty = Convert.ToInt32(rdr["allowance_qty"]);

                DateTime created = DateTime.Now;
                if (rdr["created"].ToString() != "")
                {
                    created = Convert.ToDateTime(rdr["created"]);
                }
                int ship_id = Convert.ToInt32(rdr["ship_id"]);

                liteCmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);
                liteCmd.Parameters.AddWithValue("@fsc", fsc);
                liteCmd.Parameters.AddWithValue("@niin", niin);
                liteCmd.Parameters.AddWithValue("@ui", ui);
                liteCmd.Parameters.AddWithValue("@um", um);
                liteCmd.Parameters.AddWithValue("@usage_category_id", usage_category_id);
                liteCmd.Parameters.AddWithValue("@description", description);
                liteCmd.Parameters.AddWithValue("@smcc_id", smcc_id);
                liteCmd.Parameters.AddWithValue("@specs", specs);
                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                liteCmd.Parameters.AddWithValue("@shelf_life_action_code_id", shelf_life_action_code_id);
                liteCmd.Parameters.AddWithValue("@remarks", remarks);
                liteCmd.Parameters.AddWithValue("@storage_type_id", storage_type_id);
                liteCmd.Parameters.AddWithValue("@cog_id", cog_id);
                liteCmd.Parameters.AddWithValue("@spmig", spmig);
                liteCmd.Parameters.AddWithValue("@nehc_rpt", nehc_rpt);
                liteCmd.Parameters.AddWithValue("@catalog_group_id", catalog_group_id);
                liteCmd.Parameters.AddWithValue("@catalog_serial_number", catalog_serial_number);
                liteCmd.Parameters.AddWithValue("@allowance_qty", allowance_qty);
                liteCmd.Parameters.AddWithValue("@created", created);
                liteCmd.Parameters.AddWithValue("@ship_id", ship_id);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
        }


        public void insertHHInventoryInDecantTaskByTask(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int taskid)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            List<int> invIdList = new List<int>();
            //sqlite connection and statement
            string liteStmt = "insert into inventory_in_decant_task(inventory_in_decant_task_id, shelf_life_expiration_date, manufacturer_date, location_id,"
                +" qty, mfg_catalog_id, ui, um, contract_number, batch_number, lot_number, "
                +"decant_container_type, decant_container_volume, decanting_task_id, inventory_id, cosal, hcc, atmosphere_control_number) VALUES "
                + "(@inventory_in_decant_task_id,@shelf_life_expiration_date, @manufacturer_date, @location_id,"
                + " @qty, @mfg_catalog_id, @ui, @um, @contract_number, @batch_number, @lot_number, "
                + "@decant_container_type, @decant_container_volume, @decanting_task_id, @inventory_id, @cosal, @hcc, @atmosphere_control_number)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string selectStmt = "select v.hcc, i.*, m.*, u.category as usage_category from inventory_in_decant_task i INNER JOIN vMfgCatNiinCat m ON(i.mfg_catalog_id=m.mfg_catalog_id)"
               + " INNER JOIN usage_category u ON (m.usage_category_id=u.usage_category_id) "
               + " join v_top_hazard_items v on v.inventory_id = i.inventory_id "
               + " where decanting_task_id = @decanting_task_id ORDER BY m.niin_catalog_id, m.mfg_catalog_id, i.shelf_life_expiration_date, i.alternate_ui, i.alternate_um";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@decanting_task_id", taskid);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                string ui = rdr["ui"].ToString();
                string um = rdr["um"].ToString();

                if(rdr["alternate_ui"]!=DBNull.Value)
                        ui = rdr["alternate_ui"].ToString();
                if (rdr["alternate_um"] != DBNull.Value)
                        um = rdr["alternate_um"].ToString();

                liteCmd.Parameters.AddWithValue("@inventory_in_decant_task_id", dbNull(rdr["inventory_in_decant_task_id"]));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(rdr["shelf_life_expiration_date"]));
                liteCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(rdr["manufacturer_date"]));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(rdr["location_id"]));
                liteCmd.Parameters.AddWithValue("@qty", dbNull(rdr["qty"]));
                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(rdr["mfg_catalog_id"]));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@um", dbNull(um));
                liteCmd.Parameters.AddWithValue("@contract_number", dbNull(rdr["contract_number"]));
                liteCmd.Parameters.AddWithValue("@batch_number", dbNull(rdr["batch_number"]));
                liteCmd.Parameters.AddWithValue("@lot_number", dbNull(rdr["lot_number"]));
                liteCmd.Parameters.AddWithValue("@decant_container_type", dbNull(rdr["decant_container_type"]));
                liteCmd.Parameters.AddWithValue("@decant_container_volume", dbNull(rdr["decant_container_volume"]));
                liteCmd.Parameters.AddWithValue("@decanting_task_id", dbNull(rdr["decanting_task_id"]));
                liteCmd.Parameters.AddWithValue("@inventory_id", dbNull(rdr["inventory_id"]));
                liteCmd.Parameters.AddWithValue("@cosal", dbNull(rdr["cosal"]));
                liteCmd.Parameters.AddWithValue("@hcc", dbNull(rdr["hcc"]));
                liteCmd.Parameters.AddWithValue("@atmosphere_control_number", dbNull(rdr["atm"]));
                int invId = Convert.ToInt32(rdr["inventory_id"]);


                //do not add if the inventoryId has already been found
                if (!invIdList.Contains(invId))
                {
                    invIdList.Add(invId);
                    liteCmd.ExecuteNonQuery();

                }

                

                

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
        }


        public void insertHHOffloadList(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into offload_list(offload_list_id, archived_id,shelf_life_expiration_date,location_id,serial_number, " +
                               "bulk_item,mfg_catalog_id,manufacturer_date,expected_qty_offload) " +
                              "VALUES (@offload_list_id,@archived_id,@shelf_life_expiration_date,@location_id,@serial_number, " +
                              "@bulk_item,@mfg_catalog_id,@manufacturer_date,@offload_qty)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from offload_list where archived_id is null", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int? archived_id = null;

                try
                {
                    archived_id = Convert.ToInt32(rdr["archived_id"].ToString());
                }
                catch {}

                string serial_number = null;

                try
                {
                    serial_number = Convert.ToString(rdr["serial_number"].ToString());
                }
                catch {}

                DateTime? manufacturer_date = null;

                try
                {
                    manufacturer_date = Convert.ToDateTime(rdr["manufacturer_date"].ToString());
                }
                catch {}

                DateTime? shelf_life_expiration_date = null;

                try
                {
                    shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"].ToString());
                }
                catch {}

                int offload_list_id = Convert.ToInt32(rdr["offload_list_id"].ToString());
                int location_id = Convert.ToInt32(rdr["location_id"].ToString());

                bool? bulk_item = null;
                try
                {
                  bulk_item = Convert.ToBoolean(rdr["bulk_item"]);
                }
                catch {}
                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"].ToString());
                int offload_qty = Convert.ToInt32(rdr["offload_qty"].ToString());


                liteCmd.Parameters.AddWithValue("@archived_id", dbNull(archived_id));
                liteCmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                liteCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                liteCmd.Parameters.AddWithValue("@offload_list_id", dbNull(offload_list_id));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@offload_qty", dbNull(offload_qty));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHGarbageOffloadList(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into garbage_offload_list(garbage_offload_id, garbage_item_id,expected_qty_offload,archived_id) " +
                              "VALUES (@garbage_offload_id,@garbage_item_id,@qty,@archived_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from garbage_offload_list where archived_id is null", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int garbage_offload_id = Convert.ToInt32(rdr["garbage_offload_id"].ToString());

                int? garbage_item_id = null;
                try
                {
                    garbage_item_id = Convert.ToInt32(rdr["garbage_item_id"]);
                }
                catch {}

                int qty = Convert.ToInt32(rdr["qty"]);
                int? archived_id = null;

                try
                {
                    archived_id = Convert.ToInt32(rdr["archived_id"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@garbage_offload_id", dbNull(garbage_offload_id));
                liteCmd.Parameters.AddWithValue("@garbage_item_id", dbNull(garbage_item_id));
                liteCmd.Parameters.AddWithValue("@qty", dbNull(qty));
                liteCmd.Parameters.AddWithValue("@archived_id", dbNull(archived_id));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHGarbageItems(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select * from garbage_items g where g.garbage_item_id in (select garbage_item_id from garbage_offload_list l where l.archived_id is null)";

            //sqlite connection and statement
            string liteStmt = "insert into garbage_items (garbage_item_id, description) " +
                              "VALUES (@garbage_item_id,@description)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand(stmt, mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int garbage_item_id = Convert.ToInt32(rdr["garbage_item_id"].ToString());
                string description = Convert.ToString(rdr["description"]);

                liteCmd.Parameters.AddWithValue("@garbage_item_id", garbage_item_id);
                liteCmd.Parameters.AddWithValue("@description", description);

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHNiinCatalog(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into niin_catalog (niin_catalog_id,fsc,niin,ui,um,usage_category_id,description,smcc_id, " +
                              "specs,shelf_life_code_id,shelf_life_action_code_id, remarks,storage_type_id,cog_id,spmig,nehc_rpt, " +
                              "catalog_group_id,catalog_serial_number,allowance_qty,created,ship_id) " +
                              "VALUES (@niin_catalog_id,@fsc,@niin,@ui,@um,@usage_category_id,@description,@smcc_id,@specs,@shelf_life_code_id, " + 
                              "@shelf_life_action_code_id, @remarks,@storage_type_id,@cog_id,@spmig,@nehc_rpt,@catalog_group_id,@catalog_serial_number,@allowance_qty,@created,@ship_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from niin_catalog n where n.niin_catalog_id in(select m.niin_catalog_id from mfg_catalog m)",mdfConn);           
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"].ToString());
                int? fsc = null;
                try
                {
                   fsc = Convert.ToInt32(rdr["fsc"]);
                }
                catch {}

                string niin = null;
                try
                {
                  niin = Convert.ToString(rdr["niin"]);
                }
                catch {}

                string ui = null;
                try
                {
                   ui = Convert.ToString(rdr["ui"]);
                }
                catch {}

                string um = null;
                try
                {
                   um = Convert.ToString(rdr["um"]);
                }
                catch {}

                int? usage_category_id = null;
                try
                {
                  usage_category_id = Convert.ToInt32(rdr["usage_category_id"]);
                }
                catch {}

                string description = null;
                try
                {
                  description = Convert.ToString(rdr["description"]);
                }
                catch {}

                int? smcc_id = null;
                try
                {
                  smcc_id = Convert.ToInt32(rdr["smcc_id"]);
                }
                catch {}

                string specs = null;

                try
                {
                  specs = Convert.ToString(rdr["specs"]);
                }
                catch {}

                int? shelf_life_code_id = null;

                try
                {
                  shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);
                }
                catch {}

                int? shelf_life_action_code_id = null;
                try
                {
                  shelf_life_action_code_id = Convert.ToInt32(rdr["shelf_life_action_code_id"]);
                }
                catch {}

                string remarks = null;
                try
                {
                  remarks = Convert.ToString(rdr["remarks"]);
                }
                catch {}

                int? storage_type_id = null;
                try
                {
                   storage_type_id = Convert.ToInt32(rdr["storage_type_id"]);
                }
                catch {}

                int? cog_id = null;
                try
                {
                  cog_id = Convert.ToInt32(rdr["cog_id"]);
                }
                catch {}

                string spmig = null;
                try
                {
                  spmig = rdr["spmig"].ToString();
                }
                catch {}
                string nehc_rpt = null;

                try
                {
                  nehc_rpt = rdr["nehc_rpt"].ToString();
                }
                catch {}

                int? catalog_group_id = null;
                try
                {
                  catalog_group_id = Convert.ToInt32(rdr["catalog_group_id"]);
                }
                catch {}

                string catalog_serial_number = null;
                try
                {
                  catalog_serial_number = rdr["catalog_serial_number"].ToString();
                }
                catch {}

                int? allowance_qty = null;
                try
                {
                  allowance_qty = Convert.ToInt32(rdr["allowance_qty"]);
                }
                catch {}
                //string ati = null;
                //try
                //{
                //  ati = Convert.ToString(rdr["cosal"]);
                //}
                //catch (Exception ex)
                //{
                //}

                DateTime created = DateTime.Now;
                try
                {
                    created = Convert.ToDateTime(rdr["created"]);
                }
                catch {}

                int? ship_id = null;
                try
                {
                 ship_id = Convert.ToInt32(rdr["ship_id"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                liteCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                liteCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@um", dbNull(um));
                liteCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                liteCmd.Parameters.AddWithValue("@description", dbNull(description));
                liteCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                liteCmd.Parameters.AddWithValue("@specs", dbNull(specs));
                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                liteCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                liteCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));
                liteCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                liteCmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
                liteCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                liteCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));
                liteCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                liteCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                liteCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                liteCmd.Parameters.AddWithValue("@created", dbNull(created));
                liteCmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));                

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHNiinCatalogForDecanting(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int taskid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into niin_catalog (niin_catalog_id,fsc,niin,ui,um,usage_category_id,description,smcc_id, " +
                              "specs,shelf_life_code_id,shelf_life_action_code_id, remarks,storage_type_id,cog_id,spmig,nehc_rpt, " +
                              "catalog_group_id,catalog_serial_number,allowance_qty,created,ship_id) " +
                              "VALUES (@niin_catalog_id,@fsc,@niin,@ui,@um,@usage_category_id,@description,@smcc_id,@specs,@shelf_life_code_id, " +
                              "@shelf_life_action_code_id, @remarks,@storage_type_id,@cog_id,@spmig,@nehc_rpt,@catalog_group_id,@catalog_serial_number,@allowance_qty,@created,@ship_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from niin_catalog n where n.niin_catalog_id in " +
                "(select m.niin_catalog_id from mfg_catalog m where m.mfg_catalog_id in(select i.mfg_catalog_id from Inventory i))", mdfConn);
            //cmd.Parameters.AddWithValue("@taskId", taskid);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();

                int niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"].ToString());
                int? fsc = null;
                try
                {
                    fsc = Convert.ToInt32(rdr["fsc"]);
                }
                catch {}

                string niin = null;
                try
                {
                    niin = Convert.ToString(rdr["niin"]);
                }
                catch {}

                string ui = null;
                try
                {
                    ui = Convert.ToString(rdr["ui"]);
                }
                catch {}

                string um = null;
                try
                {
                    um = Convert.ToString(rdr["um"]);
                }
                catch {}

                int? usage_category_id = null;
                try
                {
                    usage_category_id = Convert.ToInt32(rdr["usage_category_id"]);
                }
                catch {}

                string description = null;
                try
                {
                    description = Convert.ToString(rdr["description"]);
                }
                catch {}

                int? smcc_id = null;
                try
                {
                    smcc_id = Convert.ToInt32(rdr["smcc_id"]);
                }
                catch {}

                string specs = null;

                try
                {
                    specs = Convert.ToString(rdr["specs"]);
                }
                catch {}

                int? shelf_life_code_id = null;

                try
                {
                    shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);
                }
                catch {}

                int? shelf_life_action_code_id = null;
                try
                {
                    shelf_life_action_code_id = Convert.ToInt32(rdr["shelf_life_action_code_id"]);
                }
                catch {}

                string remarks = null;
                try
                {
                    remarks = Convert.ToString(rdr["remarks"]);
                }
                catch {}

                int? storage_type_id = null;
                try
                {
                    storage_type_id = Convert.ToInt32(rdr["storage_type_id"]);
                }
                catch {}

                int? cog_id = null;
                try
                {
                    cog_id = Convert.ToInt32(rdr["cog_id"]);
                }
                catch {}

                string spmig = null;
                try
                {
                    spmig = rdr["spmig"].ToString();
                }
                catch {}
                string nehc_rpt = null;

                try
                {
                    nehc_rpt = rdr["nehc_rpt"].ToString();
                }
                catch {}

                int? catalog_group_id = null;
                try
                {
                    catalog_group_id = Convert.ToInt32(rdr["catalog_group_id"]);
                }
                catch {}

                string catalog_serial_number = null;
                try
                {
                    catalog_serial_number = rdr["catalog_serial_number"].ToString();
                }
                catch {}

                int? allowance_qty = null;
                try
                {
                    allowance_qty = Convert.ToInt32(rdr["allowance_qty"]);
                }
                catch {}
                //string ati = null;
                //try
                //{
                //  ati = Convert.ToString(rdr["cosal"]);
                //}
                //catch (Exception ex)
                //{
                //}

                DateTime created = DateTime.Now;
                try
                {
                    created = Convert.ToDateTime(rdr["created"]);
                }
                catch {}

                int? ship_id = null;
                try
                {
                    ship_id = Convert.ToInt32(rdr["ship_id"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                liteCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                liteCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@um", dbNull(um));
                liteCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                liteCmd.Parameters.AddWithValue("@description", dbNull(description));
                liteCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                liteCmd.Parameters.AddWithValue("@specs", dbNull(specs));
                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                liteCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                liteCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));
                liteCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                liteCmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
                liteCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                liteCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));
                liteCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                liteCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                liteCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                liteCmd.Parameters.AddWithValue("@created", dbNull(created));
                liteCmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));                

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHMfgCatalog(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            SqlConnection con = new SqlConnection(MDF_CONNECTION);

            con.Open();
            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into mfg_catalog (mfg_catalog_id,cage,manufacturer,niin_catalog_id) " +
                              "VALUES (@mfg_catalog_id,@cage,@manufacturer,@niin_catalog_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from mfg_catalog", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            //string tmpStmt = "select hazard_id from vHazardousItems v where v.mfg_catalog_id = @mfg_catalog_id";
            //SqlCommand cmdTemp = new SqlCommand(tmpStmt, con);


            while (rdr.Read())
            {                
                cmd.Parameters.Clear();
                //cmdTemp.Parameters.Clear();
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();


                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                string cage = null;
                try
                {
                    cage = Convert.ToString(rdr["cage"]);
                }
                catch {}

                string manufacturer = null;
                try
                {
                    manufacturer = Convert.ToString(rdr["manufacturer"]);
                }
                catch {}

                int? niin_catalog_id = null;
                try
                {
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                liteCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
            con.Close();


        }

        public void insertHHMfgCatalogForDecanting(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int taskid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            SqlConnection con = new SqlConnection(MDF_CONNECTION);

            con.Open();
            mdfConn.Open();


            //sqlite connection and statement
            string liteStmt = "insert into mfg_catalog (mfg_catalog_id,cage,manufacturer,niin_catalog_id) " +
                              "VALUES (@mfg_catalog_id,@cage,@manufacturer,@niin_catalog_id)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;


            SqlCommand cmd = new SqlCommand("select * from mfg_catalog m where m.mfg_catalog_id in (select i.mfg_catalog_id from Inventory i)", mdfConn);
            //cmd.Parameters.AddWithValue("@taskId", taskid);
            SqlDataReader rdr = cmd.ExecuteReader();

            //string tmpStmt = "select hazard_id from vHazardousItems v where v.mfg_catalog_id = @mfg_catalog_id";
            //SqlCommand cmdTemp = new SqlCommand(tmpStmt, con);


            while (rdr.Read())
            {
                cmd.Parameters.Clear();
                //cmdTemp.Parameters.Clear();
                //clear parameters for each new insert
                liteCmd.Parameters.Clear();


                int mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                string cage = null;
                try
                {
                    cage = Convert.ToString(rdr["cage"]);
                }
                catch {}

                string manufacturer = null;
                try
                {
                    manufacturer = Convert.ToString(rdr["manufacturer"]);
                }
                catch {}

                int? niin_catalog_id = null;
                try
                {
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}



                liteCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                liteCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                liteCmd.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
            con.Close();


        }


        public bool getDBTypeFromDecantConfig(string dbLocation)
        {

            SQLiteConnection con = new SQLiteConnection("Data Source =" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select decant from config", con);

            object o = cmd.ExecuteScalar();

            bool val = Convert.ToBoolean(o);

            con.Close();

            return val;

        }
        [CoverageExclude]
        public bool getDBTypeFromReceivingConfig(string dbLocation)
        {

            SQLiteConnection con = new SQLiteConnection("Data Source =" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select receive from config", con);

            object o = cmd.ExecuteScalar();

            bool val = Convert.ToBoolean(o);

            con.Close();

            return val;
        }

        public bool getDBTypeFromOffloadConfig(string dbLocation)
        {

            SQLiteConnection con = new SQLiteConnection("Data Source =" + dbLocation);
            con.Open();

            SQLiteCommand cmd = new SQLiteCommand("select offload from config", con);

            object o = cmd.ExecuteScalar();

            bool val = Convert.ToBoolean(o);

            con.Close();

            return val;
        }


        protected string buildFileHash(string filePath)
        {
            //string filePath = uploadHash[guid].ToString();

            //FileStream file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hash;
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096))
            {
                hash = md5.ComputeHash(fs);
                fs.Close();
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }


        [CoverageExclude]
        public List<int> importDecantClipboard(string databaseLocation)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();

            List<int> insertedInvIds = new List<int>();

            try
            {
                insertedInvIds = importDecantInventory(mdfConn, tran, databaseLocation);

            }
            catch (Exception ex)
            {

                mdfConn.Close();
                throw new Exception("Error importing Decanting clipboard: " + ex.Message);

            }

            try
            {
                insertDecantingDb(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing Decanting clipboard: " + ex.Message);


            }

            

            //commit all the work
            tran.Commit();

            //close the connection
            mdfConn.Close();

            return insertedInvIds;

        }

        public void importOffloadClipboard(string databaseLocation)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();

            
            try
            {
                importOffloadCR(mdfConn, tran, databaseLocation);                
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing offload_cr: " + ex.Message);
            }

           
            try
            {
                importGarbageOffloadCR(mdfConn, tran, databaseLocation);                
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing garbage_cr: " + ex.Message);
            }


            //commit all the work
            tran.Commit();

            //close the connection
            mdfConn.Close();

        }


        public void importInsurvTagCr(SqlConnection con, SqlTransaction tran, string dblocation)
        {


            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dblocation);
            liteCon.Open();

            SQLiteCommand liteCmd = new SQLiteCommand("select * from insurv_atm_cr", liteCon);
            SQLiteDataReader liteRdr = liteCmd.ExecuteReader();


            string stmt = "insert into atm_tag_audit (a,b,mfg_catalog_id,location_id,shelf_life_expiration_date, qty, action_complete) " +
                "VALUES(@a,@b,@mfg_catalog_id,@location_id,@shelf_life_expiration_date, @qty, @action_complete)";

            SqlCommand cmd = new SqlCommand(stmt, con);           
            cmd.Transaction = tran;          

            //loop through and insert
            while (liteRdr.Read())
            {
                cmd.Parameters.Clear();

                string a = liteRdr["a"].ToString();
                string b = liteRdr["b"].ToString();
                int mfgCatId = Convert.ToInt32(liteRdr["mfg_catalog_id"]);
                int locationId = Convert.ToInt32(liteRdr["location_id"]);
                if (liteRdr["shelf_life_expiration_date"] != DBNull.Value)
                {
                    DateTime shelfExDate = Convert.ToDateTime(liteRdr["shelf_life_expiration_date"]);
                    cmd.Parameters.AddWithValue("shelf_life_expiration_date", dbNull(shelfExDate));
                }
                else
                {
                    cmd.Parameters.AddWithValue("shelf_life_expiration_date", dbNull(null));
                }

                int qty = Convert.ToInt32(liteRdr["qty"]);
                DateTime actionComplete = Convert.ToDateTime(liteRdr["action_complete"]);

                cmd.Parameters.AddWithValue("a", dbNull(a));
                cmd.Parameters.AddWithValue("b", dbNull(b));
                cmd.Parameters.AddWithValue("mfg_catalog_id", dbNull(mfgCatId));
                cmd.Parameters.AddWithValue("location_id", dbNull(locationId));
                
                cmd.Parameters.AddWithValue("qty", dbNull(qty));
                cmd.Parameters.AddWithValue("action_complete", dbNull(actionComplete));

                cmd.ExecuteNonQuery();

            }


            liteCon.Close();
            cmd.Dispose();

        }


        public void importInsurvBromineCr(SqlConnection con, SqlTransaction tran, string dblocation)
        {


            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dblocation);
            liteCon.Open();

            SQLiteCommand liteCmd = new SQLiteCommand("select * from insurv_bromine_cr", liteCon);
            SQLiteDataReader liteRdr = liteCmd.ExecuteReader();


            string stmt = "insert into bromine_audit (a,b,c,d,mfg_catalog_id,location_id,shelf_life_expiration_date, qty, action_complete) " +
                "VALUES(@a,@b,@c,@d,@mfg_catalog_id,@location_id,@shelf_life_expiration_date, @qty, @action_complete)";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Transaction = tran;

            //loop through and insert
            while (liteRdr.Read())
            {
                cmd.Parameters.Clear();

                string a = liteRdr["a"].ToString();
                string b = liteRdr["b"].ToString();
                string c = liteRdr["c"].ToString();
                string d = liteRdr["d"].ToString();
                int mfgCatId = Convert.ToInt32(liteRdr["mfg_catalog_id"]);
                int locationId = Convert.ToInt32(liteRdr["location_id"]);
                //ah
                DateTime? shelfExDate = null;
                try
                {
                     shelfExDate = Convert.ToDateTime(liteRdr["shelf_life_expiration_date"]);
                }
                catch {}

                int? qty = null;
                try
                {
                   qty = Convert.ToInt32(liteRdr["qty"]);
                }
                catch {}
                DateTime actionComplete = Convert.ToDateTime(liteRdr["action_complete"]);


                cmd.Parameters.AddWithValue("a", dbNull(a));
                cmd.Parameters.AddWithValue("b", dbNull(b));
                cmd.Parameters.AddWithValue("c", dbNull(c));
                cmd.Parameters.AddWithValue("d", dbNull(d));
                cmd.Parameters.AddWithValue("mfg_catalog_id", dbNull(mfgCatId));
                cmd.Parameters.AddWithValue("location_id", dbNull(locationId));
                cmd.Parameters.AddWithValue("shelf_life_expiration_date", dbNull(shelfExDate));
                cmd.Parameters.AddWithValue("qty", dbNull(qty));
                cmd.Parameters.AddWithValue("action_complete", dbNull(actionComplete));


                cmd.ExecuteNonQuery();

            }


            liteCon.Close();
            cmd.Dispose();

        }

        public void importInsurvCalciumCr(SqlConnection con, SqlTransaction tran, string dblocation)
        {


            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dblocation);
            liteCon.Open();

            SQLiteCommand liteCmd = new SQLiteCommand("select * from insurv_calcium_cr", liteCon);
            SQLiteDataReader liteRdr = liteCmd.ExecuteReader();


            string stmt = "insert into calcium_hypochlorite_audit (a,b,c,d,e,f,g,h,i,j,mfg_catalog_id,location_id,shelf_life_expiration_date, qty, action_complete) " +
                "VALUES(@a,@b,@c,@d,@e,@f,@g,@h,@i,@j,@mfg_catalog_id,@location_id,@shelf_life_expiration_date, @qty, @action_complete)";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Transaction = tran;

            //loop through and insert
            while (liteRdr.Read())
            {
                cmd.Parameters.Clear();

                string a = liteRdr["a"].ToString();
                string b = liteRdr["b"].ToString();
                string c = liteRdr["c"].ToString();
                string d = liteRdr["d"].ToString();
                string e = liteRdr["e"].ToString();
                string f = liteRdr["f"].ToString();
                string g = liteRdr["g"].ToString();
                string h = liteRdr["h"].ToString();
                string i = liteRdr["i"].ToString();
                string j = liteRdr["j"].ToString();
                
                int mfgCatId = Convert.ToInt32(liteRdr["mfg_catalog_id"]);
                int locationId = Convert.ToInt32(liteRdr["location_id"]);
                DateTime? shelfExDate = null;
                try
                {
                   shelfExDate = Convert.ToDateTime(liteRdr["shelf_life_expiration_date"]);
                }
                catch {}
                int qty = Convert.ToInt32(liteRdr["qty"]);
                DateTime actionComplete = Convert.ToDateTime(liteRdr["action_complete"]);

                cmd.Parameters.AddWithValue("@a", dbNull(a));
                cmd.Parameters.AddWithValue("@b", dbNull(b));
                cmd.Parameters.AddWithValue("@c", dbNull(c));
                cmd.Parameters.AddWithValue("@d", dbNull(d));
                cmd.Parameters.AddWithValue("@e", dbNull(e));
                cmd.Parameters.AddWithValue("@f", dbNull(f));
                cmd.Parameters.AddWithValue("@g", dbNull(g));
                cmd.Parameters.AddWithValue("@h", dbNull(h));
                cmd.Parameters.AddWithValue("@i", dbNull(i));
                cmd.Parameters.AddWithValue("@j", dbNull(j));
                cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfgCatId));
                cmd.Parameters.AddWithValue("@location_id", dbNull(locationId));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelfExDate));
                cmd.Parameters.AddWithValue("@qty", dbNull(qty));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(actionComplete));


                cmd.ExecuteNonQuery();

            }


            liteCon.Close();
            cmd.Dispose();

        }



        public void importInsurvLocationCr(SqlConnection con, SqlTransaction tran, string dblocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dblocation);
            liteCon.Open();

            SQLiteCommand liteCmd = new SQLiteCommand("select * from insurv_location_cr", liteCon);
            SQLiteDataReader liteRdr = liteCmd.ExecuteReader();


            SqlConnection con2 = new SqlConnection(MDF_CONNECTION);
            con2.Open();
            SqlCommand cmd2 = new SqlCommand("select count(*) from insurv_location_cr where location_id = @location_id AND surv_name = @name", con);
            cmd2.Transaction = tran;

            string stmt = "insert into insurv_location_cr (location_id, location_done, action_complete, surv_name) " +
                "VALUES(@location_id, @location_done, @action_complete, @surv_name)";

            string insertStmt = "update insurv_location_cr set location_id=@location_id, location_done=@location_done, action_complete=@action_complete, surv_name=@surv_name"
                    +" WHERE location_id=@location_id AND surv_name = @surv_name";
           

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.Transaction = tran;

            //loop through and insert
            while (liteRdr.Read())
            {
                cmd.Parameters.Clear();
                cmd2.Parameters.Clear();
                
                int locationId = Convert.ToInt32(liteRdr["location_id"]);
                bool locDone = Convert.ToBoolean(liteRdr["location_done"]);
                string survName = Convert.ToString(liteRdr["surv_name"]);
                DateTime actionComplete = Convert.ToDateTime(liteRdr["action_complete"]);

                cmd2.Parameters.AddWithValue("@location_id", locationId);
                cmd2.Parameters.AddWithValue("@name", survName);

                int count = Convert.ToInt32(cmd2.ExecuteScalar());

                
                if (count == 0)
                {
                    cmd.CommandText = stmt;                  
                }
                else if (count > 0)
                {
                    cmd.CommandText = insertStmt;
                    
                }

                cmd.Parameters.AddWithValue("location_id", dbNull(locationId));
                cmd.Parameters.AddWithValue("location_done", dbNull(locDone));
                cmd.Parameters.AddWithValue("surv_name", dbNull(survName));
                cmd.Parameters.AddWithValue("action_complete", dbNull(actionComplete));

                cmd.ExecuteNonQuery();

            }


            liteCon.Close();
            cmd.Dispose();

            con2.Close();

        }

        [CoverageExclude]
        public void importInsurvClipboard(string databaseLocation)
        {
            
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();
            
            try
            {
                importInsurvTagCr(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing atm_audit: " + ex.Message);
            }

            try
            {
                importInsurvBromineCr(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing bromine_audit: " + ex.Message);
            }

            try
            {
                importInsurvCalciumCr(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing calcium_hypochlorite_audit: " + ex.Message);
            }


            try
            {
                importInsurvLocationCr(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing insurv_location_cr: " + ex.Message);
            }



            //commit all the work
            tran.Commit();

            //close the connection
            mdfConn.Close();

        }

        public List<int> getRecurringListByDateRange(DateTime start, DateTime end)
        {

           // List<AuditInfo> auditList = new List<AuditInfo>();
            List<AuditInfo> recurringList = new List<AuditInfo>();

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();
                          
            
            string stmt = "select * from recurring_audits r where r.deleted = 0 ";
            //AND r.initially_scheduled_on >= @start AND r.initially_scheduled_on <= @end
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@start", start);
            cmd.Parameters.AddWithValue("@end", end);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                AuditInfo recurringInfo = new AuditInfo();

                DateTime initiallyScheduledOn = Convert.ToDateTime(rdr["initially_scheduled_on"]);
                int recurringId = Convert.ToInt32(rdr["recurring_audit_id"]);
                string recurring = Convert.ToString(rdr["recurring"]);

                recurringInfo.AuditID = recurringId;
                recurringInfo.ScheduledTime = initiallyScheduledOn;
                recurringInfo.RecurringType = recurring;

                recurringList.Add(recurringInfo);
            }

            //get a list of the inventory audit scheduled dates which have a recurring audit.. check the dates and do not create a db twice

                        
            List<AuditInfo> recurringFoundList = new List<AuditInfo>();

            foreach (AuditInfo a in recurringList)
            {
                bool recurFoundInRange = false;

                DateTime recurringDate = a.ScheduledTime;

                    if (a.RecurringType.ToLower().Equals("weekly"))
                    {
                        recurringDate = recurringDate.AddDays(7);

                        while (!(recurringDate >= start && recurringDate <= end)&& recurringDate <= end)
                        {
                            recurringDate = recurringDate.AddDays(7);
                        }

                        if (recurringDate <= end)
                        {
                            recurFoundInRange = true;
                        }

                    }
                    else if (a.RecurringType.ToLower().Equals("monthly"))
                    {

                        recurringDate = recurringDate.AddMonths(1);

                        while (!(recurringDate >= start && recurringDate <= end) && recurringDate <= end)
                        {
                            recurringDate = recurringDate.AddMonths(1);
                        }

                        if (recurringDate <= end)
                        {
                            recurFoundInRange = true;
                        }
                    }


                    if (recurFoundInRange)
                    {
                        

                        //add to list to know which audits will need to be created.. 
                        a.ScheduledTime = recurringDate;

                        recurringFoundList.Add(a);

                        //compare to recurring_in_audits to ensure an audit doesn't currently exist on this day
                        
                    }
                
            }

            rdr.Close();
            cmd.Dispose();

            stmt = "select count(*) from inventory_audits a where a.scheduled_on = @date " + 
                "AND a.inventory_audit_id in (select r.inventory_audit_id from audits_in_recurring r where r.recurring_id = @recurring_id)";

           // stmt = "select count(*) from inventory_audits a where " +
             //   "a.inventory_audit_id in (select r.inventory_audit_id from audits_in_recurring r where r.recurring_id = @recurring_id)";

            cmd.CommandText = stmt;

            List<int> returnList = new List<int>();

            foreach (AuditInfo a in recurringFoundList)
            {
                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@date", a.ScheduledTime);
                cmd.Parameters.AddWithValue("recurring_id", a.AuditID);

                object o = cmd.ExecuteScalar();

                int count = Convert.ToInt32(o);

                if (count < 1)
                {
                    returnList.Add(a.AuditID);
                }

            }

            con.Close();

            return returnList;

        }


        public Dictionary<int, DateTime> getDateHashByRecurringAndRange(DateTime start, DateTime end, int recurId)
        {

            Dictionary<int, DateTime> dateHash = new Dictionary<int, DateTime>();


            List<AuditInfo> recurringList = new List<AuditInfo>();

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select * from recurring_audits r where r.deleted = 0";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@start", start);
            cmd.Parameters.AddWithValue("@end", end);
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                AuditInfo recurringInfo = new AuditInfo();

                DateTime initiallyScheduledOn = Convert.ToDateTime(rdr["initially_scheduled_on"]);
                int recurringId = Convert.ToInt32(rdr["recurring_audit_id"]);
                string recurring = Convert.ToString(rdr["recurring"]);

                recurringInfo.AuditID = recurringId;
                recurringInfo.ScheduledTime = initiallyScheduledOn;
                recurringInfo.RecurringType = recurring;

                recurringList.Add(recurringInfo);
            }

            //get a list of the inventory audit scheduled dates which have a recurring audit.. check the dates and do not create a db twice


            List<AuditInfo> recurringFoundList = new List<AuditInfo>();

            foreach (AuditInfo a in recurringList)
            {
                bool recurFoundInRange = false;

                DateTime recurringDate = a.ScheduledTime;

                if (a.RecurringType.ToLower().Equals("weekly"))
                {
                    recurringDate = recurringDate.AddDays(7);

                    while (!(recurringDate >= start && recurringDate <= end) && recurringDate <= end)
                    {
                        recurringDate = recurringDate.AddDays(7);
                    }

                    if (recurringDate <= end)
                    {
                        recurFoundInRange = true;
                    }

                }
                else if (a.RecurringType.ToLower().Equals("monthly"))
                {

                    recurringDate = recurringDate.AddMonths(1);

                    while (!(recurringDate >= start && recurringDate <= end) && recurringDate <= end)
                    {
                        recurringDate = recurringDate.AddMonths(1);
                    }

                    if (recurringDate <= end)
                    {
                        recurFoundInRange = true;
                    }
                }


                if (recurFoundInRange)
                {


                    //add to list to know which audits will need to be created.. 
                    a.ScheduledTime = recurringDate;

                    recurringFoundList.Add(a);

                    //compare to recurring_in_audits to ensure an audit doesn't currently exist on this day

                }

            }

            rdr.Close();
            cmd.Dispose();

            stmt = "select count(*) from inventory_audits a where a.scheduled_on = @date " +
                "AND a.inventory_audit_id in (select r.inventory_audit_id from audits_in_recurring r where r.recurring_id = @recurring_id)";
            cmd.CommandText = stmt;

            List<int> returnList = new List<int>();

            foreach (AuditInfo a in recurringFoundList)
            {
                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@date", a.ScheduledTime);
                cmd.Parameters.AddWithValue("recurring_id", a.AuditID);

                object o = cmd.ExecuteScalar();

                int count = Convert.ToInt32(o);

                if (count < 1)
                {
                    dateHash.Add(a.AuditID, a.ScheduledTime);
                    returnList.Add(a.AuditID);
                }

            }

            con.Close();

            return dateHash;

        }


        public List<int> getCurrentAuditIds(DateTime start, DateTime end)
        {

            List<int> auditList = new List<int>();

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select i.inventory_audit_id from inventory_audits i " +
                          "where i.deleted = 0 " +
                          "AND i.scheduled_on >= @start " +
                          "AND i.scheduled_on <= @end ";

            SqlCommand cmd = new SqlCommand(stmt, con);

            cmd.Parameters.AddWithValue("@end", end);
            cmd.Parameters.AddWithValue("@start", start);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                auditList.Add(Convert.ToInt32(rdr["inventory_audit_id"]));
            }

            con.Close();

            return auditList;

        }

        public List<int> getCurrentDecantingIds(DateTime start, DateTime end)
        {
            List<int> decantingList = new List<int>();

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select decanting_task_id from decanting_tasks t " +
                                            "where t.date <= @end AND t.date >= @start " +
                                            "AND t.active = 1", con);

            cmd.Parameters.AddWithValue("@end", end);
            cmd.Parameters.AddWithValue("@start", start);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                decantingList.Add(Convert.ToInt32(rdr["decanting_task_id"]));
            }

            con.Close();

            return decantingList;
        }
        [CoverageExclude]
        public void importClipboard(string databaseLocation)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();


            //insert the inventory_audit_cr from the handheld which was uploaded
            try
            {
                importInventoryAuditCRAndVolume(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing inventory_audit_cr: " + ex.Message);
            }

            //insert the location_cr from the handheld which was uploaded
            try
            {
                importLocationCR(mdfConn, tran, databaseLocation);
            }
            catch (Exception ex)
            {
                mdfConn.Close();
                throw new Exception("Error importing location_cr: " + ex.Message);
            }

            //commit all the work
            tran.Commit();

            //close the connection
            mdfConn.Close();

        }



        public void importGarbageOffloadCR(SqlConnection mdfConn, SqlTransaction tran, string databaseLocation)
        {

            string insertStmt = "insert into garbage_offload_cr (garbage_offload_id,offload_qty,expected_offload_qty,action_complete,username,device_type) " +
                               "VALUES (@garbage_offload_id, @offload_qty,@expected_offload_qty,@action_complete,@username,@device_type)";

            //create new command
            SqlCommand cmd = new SqlCommand(insertStmt, mdfConn);

            //set the transaction
            cmd.Transaction = tran;

            //connect to the database the user uploaded into the system
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();

            //select all records
            SQLiteCommand liteCmd = new SQLiteCommand("select * from garbage_offload_cr where from_server is null", liteCon);
            
            SQLiteDataReader rdr = liteCmd.ExecuteReader();

            //loop through the results
            while (rdr.Read())
            {

                //clear parameters for each new insert
                cmd.Parameters.Clear();

                int garbage_offload_id = Convert.ToInt32(rdr["garbage_offload_id"]);
                int? offload_qty = null;
                try
                {
                    offload_qty = Convert.ToInt32(rdr["qty_offloaded"]);
                }
                catch {}

                int? expected_offload_qty = null;
                try
                {
                    expected_offload_qty = Convert.ToInt32(rdr["expected_qty_offload"]);
                }
                catch {}

                DateTime? action_complete = null;
                try
                {
                   action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch {}

                string username = null;
                try
                {
                 username = Convert.ToString(rdr["username"]);
                }
                catch 
                {                    
                }

                string device_type = null;
                try
                {
                device_type = rdr["device_type"].ToString();
                }
                catch {}

                cmd.Parameters.AddWithValue("@garbage_offload_id", dbNull(garbage_offload_id));
                cmd.Parameters.AddWithValue("@offload_qty", dbNull(offload_qty));
                cmd.Parameters.AddWithValue("@expected_offload_qty", dbNull(expected_offload_qty));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@username", dbNull(username));
                cmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
               

                cmd.ExecuteNonQuery();


            }

            liteCmd.Dispose();
            liteCon.Close();


        }


        public DataTable getServerGarbageCR()
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from garbage_offload_cr", con);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;

        }

        public DataTable getServerOffloadCR()
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from offload_cr", con);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;

        }


        public DataTable getHHGarbageCRData(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + dbLocation);
            liteCon.Open();


            SQLiteCommand cmd = new SQLiteCommand("select * from garbage_offload_cr", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            liteCon.Close();

            return dt;
        }

        public DataTable getHHOffloadCRData(string dbLocation)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source = " + dbLocation);
            liteCon.Open();         


            SQLiteCommand cmd = new SQLiteCommand("select * from offload_cr", liteCon);

            DataTable dt = new DataTable();

            new SQLiteDataAdapter(cmd).Fill(dt);

            liteCon.Close();

            return dt;
        }


        public void insertDecantingDb(SqlConnection con, SqlTransaction tran, string dblocation)
        {

            string md5_hash = buildFileHash(dblocation);
            string checkStmt = "select count(*) from decanting_upload WHERE md5_hash=@md5_hash";

            SqlCommand cmdCheck = new SqlCommand(checkStmt, con);
            cmdCheck.Transaction = tran;
            cmdCheck.Parameters.AddWithValue("@md5_hash", md5_hash);

            object o = cmdCheck.ExecuteScalar();
            int count = Convert.ToInt32(o);

            if (count != 0)//Database already uploaded
            {
                throw new Exception("Database has already been uploaded.");
            }

            FileStream fs = new FileStream(dblocation, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];

            // read in the file stream to the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            // close the file stream
            fs.Close();

            string selectStmt = "insert into decanting_upload (uploaded_file, date_uploaded, md5_hash) VALUES (@uploaded_file,@date, @md5_hash)";

            DateTime date = DateTime.Now;

            SqlCommand cmd = new SqlCommand(selectStmt, con);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@uploaded_file", fileData);
            cmd.Parameters.AddWithValue("@date", date);            
            cmd.Parameters.AddWithValue("@md5_hash", md5_hash);            

            cmd.ExecuteNonQuery();            

        }

        public List<int> importDecantInventory(SqlConnection con, SqlTransaction tran, string dblocation)
        {

            List<int> invIds = new List<int>();

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + dblocation);
            liteCon.Open();

            SQLiteCommand liteCmd = new SQLiteCommand("select * from decanted_inventory where location_id is not null", liteCon);
            SQLiteDataReader liteRdr = liteCmd.ExecuteReader();


            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.Transaction = tran;

            SqlCommand cmd2 = new SqlCommand();
            cmd2.Connection = con;
            cmd2.Transaction = tran;


            SqlCommand cmd3 = new SqlCommand();
            cmd3.Connection = con;
            cmd3.Transaction = tran;
            //**removed serialization.  insert decanted inventory

            //loop through and insert
            while (liteRdr.Read())
            {
                cmd.Parameters.Clear();
                cmd2.Parameters.Clear();
                cmd3.Parameters.Clear();

                string exists = "select count(*) from decanted_inventory where inventory_in_decant_task_id=@inventory_in_decant_task_id AND action_complete=@action_complete";
                cmd3.CommandText = exists;
                cmd3.Parameters.AddWithValue("@inventory_in_decant_task_id", dbNull(liteRdr["inventory_in_decant_task_id"]));
                cmd3.Parameters.AddWithValue("@action_complete", dbNull(liteRdr["action_complete"]));
                object o = cmd3.ExecuteScalar();

                int countExists = Convert.ToInt32(o);

                //Don't upload duplicate records!
                if(countExists==0){

                string stmt = "insert into decanted_inventory (inventory_in_decant_task_id, location_id, qty, action_complete) " +
                "VALUES(@inventory_in_decant_task_id, @location_id, @qty, @action_complete)";
                cmd.CommandText = stmt;
                cmd.Parameters.AddWithValue("@qty", dbNull(liteRdr["qty"]));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(liteRdr["action_complete"]));
                try
                {
                    cmd.Parameters.AddWithValue("@inventory_in_decant_task_id", dbNull(Convert.ToInt32(liteRdr["inventory_in_decant_task_id"].ToString())));
                }
                catch 
                {
                    cmd.Parameters.AddWithValue("@inventory_in_decant_task_id", dbNull(null));
                }
                cmd.Parameters.AddWithValue("@location_id", dbNull(Convert.ToInt32(liteRdr["location_id"].ToString())));
                //cmd.Parameters.AddWithValue("@atm", dbNull(liteRdr["atmosphere_control_number"].ToString()));       
                cmd.ExecuteNonQuery();

                if (liteRdr["inventory_in_decant_task_id"] != DBNull.Value)
                {
                    DataTable dt = findInventoryInDecantTask(Int32.Parse(liteRdr["inventory_in_decant_task_id"].ToString()));
                    DataRow invTask = dt.Rows[0];

                    string invStmt = "insert into inventory (shelf_life_expiration_date, location_id, qty, serial_number, "
                        + "bulk_item, mfg_catalog_id, manufacturer_date, ati, alternate_ui, batch_number, lot_number, "
                        + "contract_number, alternate_um, cosal, atm) VALUES " +
                        "(@shelf_life_expiration_date, @location_id, @qty, @serial_number, "
                        + "@bulk_item, @mfg_catalog_id, @manufacturer_date, @ati, @alternate_ui, @batch_number, @lot_number, "
                        + "@contract_number, @alternate_um, @cosal, @atm); SELECT SCOPE_IDENTITY()";

                    cmd2.Transaction = tran;
                    cmd2.CommandText = invStmt;

                    cmd2.Parameters.AddWithValue("@location_id", liteRdr["location_id"]);
                    cmd2.Parameters.AddWithValue("@qty", liteRdr["qty"]);

                    cmd2.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(invTask["shelf_life_expiration_date"]));
                    cmd2.Parameters.AddWithValue("@serial_number", dbNull(invTask["serial_number"]));
                    cmd2.Parameters.AddWithValue("@bulk_item", dbNull(true));
                    cmd2.Parameters.AddWithValue("@mfg_catalog_id", dbNull(invTask["mfg_catalog_id"]));
                    cmd2.Parameters.AddWithValue("@manufacturer_date", dbNull(invTask["manufacturer_date"]));
                    cmd2.Parameters.AddWithValue("@ati", dbNull(invTask["ati"]));
                    cmd2.Parameters.AddWithValue("@alternate_ui", dbNull(invTask["decant_container_type"]));
                    cmd2.Parameters.AddWithValue("@batch_number", dbNull(invTask["batch_number"]));
                    cmd2.Parameters.AddWithValue("@lot_number", dbNull(invTask["lot_number"]));
                    cmd2.Parameters.AddWithValue("@contract_number", dbNull(invTask["contract_number"]));
                    cmd2.Parameters.AddWithValue("@alternate_um", dbNull(invTask["decant_container_volume"]));
                    cmd2.Parameters.AddWithValue("@cosal", dbNull(invTask["cosal"]));
                    cmd2.Parameters.AddWithValue("@atm", dbNull(invTask["atm"]));
                    

                     o = cmd2.ExecuteScalar();

                    int inventory_id = Convert.ToInt32(o);

                    invIds.Add(inventory_id);

                }//end exists already
                }//end
            }


            liteCon.Close();
            cmd.Dispose();

            return invIds;

        }

        public void importOffloadCR(SqlConnection mdfConn, SqlTransaction tran, string databaseLocation)
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();


            string insertStmt = "insert into offload_cr (offload_list_id,mfg_catalog_id,qty_offloaded,action_complete,username,device_type,manufacturer_date,shelf_life_expiration_date,expected_qty_offloaded,serial_number, bulk_item) " +
                               "VALUES (@offload_list_id,@mfg_catalog_id,@qty_offloaded,@action_complete,@username,@device_type,@manufacturer_date,@shelf_life_expiration_date,@expected_qty_offload,@serial_number,@bulk_item)";

            //create new command
            SqlCommand cmd = new SqlCommand(insertStmt, mdfConn);            

            string stmt2 = "select count(*) from offload_list where offload_list_id = @offloadid";
            SqlCommand cmd2 = new SqlCommand(stmt2, con);


            //set the transaction
            cmd.Transaction = tran;

            //connect to the database the user uploaded into the system
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();

            //select all records
            SQLiteCommand liteCmd = new SQLiteCommand("select * from offload_cr where from_server is null", liteCon);
            
            SQLiteDataReader rdr = liteCmd.ExecuteReader();

            //loop through the results
            while (rdr.Read())
            {

                //clear parameters for each new insert
                cmd.Parameters.Clear();

                int offload_list_id = Convert.ToInt32(rdr["offload_list_id"]);


                //check offloadid.  if it does not exist, do not throw exception.
                cmd2.Parameters.Clear();
                cmd2.Parameters.AddWithValue("@offloadid", offload_list_id);
                int count = Convert.ToInt32(cmd2.ExecuteScalar());


                int? mfg_catalog_id = null;
                try
                {
                    mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}
                int? qty_offloaded = null;
                try
                {
                    qty_offloaded = Convert.ToInt32(rdr["qty_offloaded"]);
                }
                catch {}
                int? expected_qty_offload = null;
                try
                {
                    expected_qty_offload = Convert.ToInt32(rdr["expected_qty_offload"]);
                }
                catch {}

                bool? bulk_item = null;
                try
                {
                  bulk_item = Convert.ToBoolean(rdr["bulk_item"]);
                }
                catch {}

                string serial_number = null;
                try
                {
                  serial_number = rdr["serial_number"].ToString();
                }
                catch {}

                DateTime? shelf_life_expiration_date = null;
                try
                {
                 shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}

                DateTime? manufacturer_date = null;
                try
                {
                 manufacturer_date = Convert.ToDateTime(rdr["manufacturer_date"]);
                }
                catch {}

                DateTime? action_complete = null;
                try
                {
                 action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch (Exception)
                {
                }

                string username = null;
                try
                {
                 username = rdr["username"].ToString();
                }
                catch {}

                string device_type = null;
                try
                {
                  device_type = rdr["device_type"].ToString();
                }
                catch {}

                cmd.Parameters.AddWithValue("@offload_list_id",dbNull(offload_list_id));                
                cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
                cmd.Parameters.AddWithValue("@qty_offloaded", dbNull(qty_offloaded));
                cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
                cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));               
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@username", dbNull(username));
                cmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
                cmd.Parameters.AddWithValue("@expected_qty_offload", dbNull(expected_qty_offload));


                if (count > 0)
                {
                    cmd.ExecuteNonQuery();
                }


            }


            con.Close();
            liteCmd.Dispose();
            liteCon.Close();


        }


        private void importInventoryAuditCRAndVolume(SqlConnection mdfConn, SqlTransaction tran, string databaseLocation)
        {

            //get locationids where the location has been marked as done
            string locStr = "select distinct location_id from location_cr where location_done = @done";
            SQLiteConnection con = new SQLiteConnection("Data Source=" + databaseLocation);
            con.Open();

            List<int> doneLocationList = new List<int>();

            SQLiteCommand locCmd = new SQLiteCommand(locStr, con);
            locCmd.Parameters.AddWithValue("@done", true);
            SQLiteDataReader r = locCmd.ExecuteReader();

            while (r.Read())
            {
                doneLocationList.Add(Convert.ToInt32(r["location_id"]));
            }

            con.Close();
            locCmd.Dispose();
            con.Dispose();

            //list for the new change records added on the handheld
            List<int> newCrIdsList = new List<int>();

            string insertStmt = "insert into inventory_audit_cr (inventory_audit_id,location_id,mfg_catalog_id,qty,bulk_item,serial_number,shelf_life_expiration_date,current_volume,action_complete,username,device_type,parent_id,device_id, alternate_ui, server_synctime, count, alternate_um, cosal, manufacturer_date, contract_number, atm) " +
                               "VALUES (@inventory_audit_id,@location_id,@mfg_catalog_id,@qty,@bulk_item,@serial_number,@shelf_life_expiration_date,@current_volume,@action_complete,@username,@device_type, @parent_id,@device_id, @ui, @server_synctime, @count, @um, @cosal,@manufacturer_date, @contract, @atm); SELECT SCOPE_IDENTITY()";
            
            //create new command
            SqlCommand cmd = new SqlCommand(insertStmt, mdfConn);

            //command for inserting volumes_in_inventory_audit
            string vInsertStmt = "insert into volumes_in_inventory_audit (inventory_audit_cr_id, volume_id) VALUES (@inventory_audit_cr_id, @volume_id)";
           // SqlConnection mdfConn2 = new SqlConnection(MDF_CONNECTION);
           // mdfConn2.Open();
            SqlCommand vCmd = new SqlCommand(vInsertStmt, mdfConn);
            vCmd.Transaction = tran;


            //set the transaction
            cmd.Transaction = tran;

            //server sync time
            DateTime syncTime = DateTime.Now;

            //connect to the database the user uploaded into the system
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            SQLiteConnection liteCon2 = new SQLiteConnection("Data Source=" + databaseLocation);
            SQLiteConnection liteCon3 = new SQLiteConnection("Data Source=" + databaseLocation);
            SQLiteConnection liteCon4 = new SQLiteConnection("Data Source=" + databaseLocation);


            liteCon2.Open();
            liteCon.Open();
            liteCon3.Open();
            liteCon4.Open();


            //first we need to get the inventory_audit_cr_id to from the new_audit_change_records table - 
            //this is how we know which change records were added on the handheld versus what was sent from the server on the initial download

            //get the ids in a List
            string newSelectStmt = "select inventory_audit_cr_id from new_audit_change_records";

            SQLiteCommand newLiteCmd = new SQLiteCommand(newSelectStmt, liteCon);
            SQLiteDataReader liteRdr = newLiteCmd.ExecuteReader();

            while (liteRdr.Read())
            {
                //get id
                int auditCrId = Convert.ToInt32(liteRdr["inventory_audit_cr_id"]);

                //add id to list
                newCrIdsList.Add(auditCrId);


            }
            //close reader
            liteRdr.Close();
            liteRdr.Dispose();

            //dispose of the command
            newLiteCmd.Dispose();

            List<int> parentList = new List<int>();

            //create list of parentids
            string parentStmt = "select parent_id from inventory_audit_cr where parent_id is not null";
            newLiteCmd = new SQLiteCommand(parentStmt, liteCon);

            SQLiteDataReader pRdr = newLiteCmd.ExecuteReader();

            while (pRdr.Read())
            {
                parentList.Add(Convert.ToInt32(pRdr["parent_id"]));
            }

            //close reader
            pRdr.Close();
            pRdr.Dispose();

            //dispose of the command
            newLiteCmd.Dispose();


            //check to see if any new change_records have been found
            if (newCrIdsList.Count < 1)
            {
                liteCon.Close();
                liteCon.Dispose();
                return;

            }

            int count = 1;
            string ids = "";

            foreach (int i in newCrIdsList)
            {

                if (count != newCrIdsList.Count)
                {

                    ids += i + ",";

                }

                //last iteration
                else
                {

                    ids += i;
                }

                count++;

            }


            //compare the ui value in the niin_catalog table to the alternate ui in the cr table..  if they're different add...else null
            string compareStmt = "select n.ui from niin_catalog n, mfg_catalog m " +
                                 "where m.mfg_catalog_id = @catid " +
                                 "AND m.niin_catalog_id = n.niin_catalog_id";

            string umCompareStmt = "select n.um from niin_catalog n, mfg_catalog m " +
                                    "where m.mfg_catalog_id = @catid " +
                                    "AND m.niin_catalog_id = n.niin_catalog_id";

            SQLiteCommand compareCmd = new SQLiteCommand(compareStmt, liteCon2);
            SQLiteCommand compareUmCmd = new SQLiteCommand(umCompareStmt, liteCon3);

            SQLiteCommand liteVCmd = new SQLiteCommand("select * from volumes_in_inventory_audit where inventory_audit_cr_id = @inventory_audit_cr_id",liteCon4);


            //select all records from inventory_audit_cr where the id is in the newCrIdsList (created above)
            SQLiteCommand liteCmd = new SQLiteCommand("select * from inventory_audit_cr where inventory_audit_cr_id in(" + ids + ")", liteCon);

            SQLiteDataReader rdr = liteCmd.ExecuteReader();

            //loop through the results
            while (rdr.Read())
            {

                //clear parameters for each new insert
                cmd.Parameters.Clear();

                int inventory_audit_id = Convert.ToInt32(rdr["inventory_audit_id"]);
                int location_id = Convert.ToInt32(rdr["location_id"]);
                int inventory_audit_cr_id = Convert.ToInt32(rdr["inventory_audit_cr_id"]);


                int? mfg_catalog_id = null;
                try
                {
                 mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}
                int qty = Convert.ToInt32(rdr["qty"]);
                bool bulk_item = Convert.ToBoolean(rdr["bulk_item"]);

                string serial_number = null;
                try
                {
                 serial_number = Convert.ToString(rdr["msds_serial_no"]);
                 serial_number = serial_number.ToUpper();
                }
                catch (Exception)
                {
                }

                string contract_number = null;
                try
                {
                    contract_number = Convert.ToString(rdr["contract_number"]);
                    contract_number = contract_number.ToUpper();
                }
                catch (Exception)
                {
                }

                DateTime? shelf_life_expiration_date = null;
                try
                {
                 shelf_life_expiration_date = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}

                DateTime? action_complete = null;
                try
                {
                 action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch {}

                string username = null;
                try
                {
                  username = Convert.ToString(rdr["username"]);
                }
                catch {}


                int? parent_id = null;
                try
                {
                  parent_id = Convert.ToInt32(rdr["parent_id"]);
                }
                catch {}

                string device_id = null;
                try
                {
                 device_id = rdr["device_id"].ToString();
                }
                catch {}
                
                

                string device_type = Convert.ToString(rdr["device_type"]);

                string current_volume = null;

                try
                {
                    current_volume = Convert.ToString(rdr["current_volume"]);
                }
                catch 
                {                    
                }

                string ui = null;

                try
                {
                    ui = Convert.ToString(rdr["ui"]);
                }
                catch {}

                int? auditCount = null;

                try
                {
                    auditCount = Convert.ToInt32(rdr["count"]);
                }
                catch {}

                string um = null;

                try
                {
                    um = Convert.ToString(rdr["um"]);
                }
                catch {}

                string cosal = null;

                try
                {
                    cosal = Convert.ToString(rdr["cosal"]);
                }
                catch {}

                DateTime? manDate = null;
                try
                {
                    manDate = Convert.ToDateTime(rdr["manufacture_date"]);
                }
                catch
                { 
                
                }

                compareUmCmd.Parameters.Clear();
                compareUmCmd.Parameters.AddWithValue("@catid", mfg_catalog_id);

                string foundUm = Convert.ToString(compareUmCmd.ExecuteScalar());

                if (um.ToUpper().Equals(foundUm.ToUpper()))
                {
                    foundUm = null;
                }
                else
                {
                    foundUm = um;
                    foundUm = foundUm.ToUpper();
                }

                compareCmd.Parameters.Clear();
                compareCmd.Parameters.AddWithValue("@catid", mfg_catalog_id);

                string foundUi = Convert.ToString(compareCmd.ExecuteScalar());

                if (ui.ToUpper().Equals(foundUi.ToUpper()))
                {
                    foundUi = null;
                }
                else
                {
                    foundUi = ui;
                    foundUi = foundUi.ToUpper();
                }


                string atm = null;
                try
                {
                    atm = Convert.ToString(rdr["atmosphere_control_number"]);
                }
                catch {}


                cmd.Parameters.AddWithValue("@inventory_audit_id",dbNull(inventory_audit_id));
                cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                cmd.Parameters.AddWithValue("@mfg_catalog_id",dbNull( mfg_catalog_id));
                cmd.Parameters.AddWithValue("@qty", dbNull(qty));
                cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
                cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
                cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                cmd.Parameters.AddWithValue("@current_volume", dbNull(current_volume));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@username", dbNull(username));
                cmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
                cmd.Parameters.AddWithValue("@parent_id", dbNull(parent_id));
                cmd.Parameters.AddWithValue("@device_id", dbNull(device_id));
                cmd.Parameters.AddWithValue("@ui", dbNull(foundUi));
                cmd.Parameters.AddWithValue("@count", dbNull(auditCount));
                cmd.Parameters.AddWithValue("@um", dbNull(foundUm));
                cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manDate));
                cmd.Parameters.AddWithValue("@contract", dbNull(contract_number));
                cmd.Parameters.AddWithValue("@atm", dbNull(atm));
                


                //if the location is marked as done, set the server_synctime
                if (doneLocationList.Contains(location_id))
                {
                    cmd.Parameters.AddWithValue("@server_synctime", dbNull(syncTime));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@server_synctime", dbNull(null));
                }

                if (!parentList.Contains(inventory_audit_cr_id))
                {

                    //clear
                    liteVCmd.Parameters.Clear();
                    vCmd.Parameters.Clear();

                     //get the id .. insert volumes_in_inventory_audit with the new assigned id                
                     object o = cmd.ExecuteScalar();
                     int newCRId = Convert.ToInt32(o);


                     liteVCmd.Parameters.AddWithValue("inventory_audit_cr_id", inventory_audit_cr_id);

                     SQLiteDataReader vRdr = liteVCmd.ExecuteReader();


                     //loop through and insert volumes_in_inventory_audit with the new crId
                     while (vRdr.Read())
                     {
                         vCmd.Parameters.Clear();

                         vCmd.Parameters.AddWithValue("@inventory_audit_cr_id", newCRId);
                         vCmd.Parameters.AddWithValue("@volume_id", Convert.ToInt32(vRdr["volume_id"]));

                         vCmd.ExecuteNonQuery();

                     }
                 

                    //close
                    vRdr.Close();
                    vRdr.Dispose();
                }


            }

            liteCmd.Dispose();
            liteCon.Close();
            liteCon2.Close();
            liteCon3.Close();
            liteCon4.Close();

            // Make sure the connections get disposed - Jim is seeing lock problems
            liteCon.Dispose();
            liteCon2.Dispose();
            liteCon3.Dispose();
            liteCon4.Dispose();
        }

        private void importLocationCR(SqlConnection mdfConn, SqlTransaction tran, string databaseLocation)
        {

            //list for the new change records added on the handheld
            List<int> newCrIdsList = new List<int>();

            string insertStmt = "insert into location_cr (location_id,location_done,action_complete, inventory_audit_id, count) VALUES (@location_id,@location_done,@action_complete,@inventory_audit_id, @count)";

            //create new command
            SqlCommand cmd = new SqlCommand(insertStmt, mdfConn);

            //set the transaction
            cmd.Transaction = tran;

            //connect to the database the user uploaded into the system
            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();



            //first we need to get the location_cr_id to from the new_location_change_records table - 
            //this is how we know which change records were added on the handheld versus what was sent from the server on the initial download

            //get the ids in a List
            string newSelectStmt = "select location_cr_id from new_location_change_records";

            SQLiteCommand newLiteCmd = new SQLiteCommand(newSelectStmt, liteCon);
            SQLiteDataReader liteRdr = newLiteCmd.ExecuteReader();

            while (liteRdr.Read())
            {
                //get id
                int locationCrId = Convert.ToInt32(liteRdr["location_cr_id"]);

                //add id to list
                newCrIdsList.Add(locationCrId);


            }
            //close reader
            liteRdr.Close();
            liteRdr.Dispose();

            //dispose of the command
            newLiteCmd.Dispose();

            //check to see if any new change_records have been found
            if (newCrIdsList.Count < 1)
            {
                liteCon.Close();
                liteCon.Dispose();
                return;

            }

            int count = 1;
            string ids = "";

            foreach (int i in newCrIdsList)
            {

                if (count != newCrIdsList.Count)
                {

                    ids += i + ",";

                }

                //last iteration
                else
                {

                    ids += i;
                }

                count++;
            }

            SQLiteCommand lCmd = new SQLiteCommand("select inventory_audit_id from config", liteCon);
            object o = lCmd.ExecuteScalar();
            int auditid = Convert.ToInt32(o);

           SQLiteCommand liteCmd = new SQLiteCommand("select * from location_cr where location_cr_id in(" + ids + ")", liteCon);

            SQLiteDataReader rdr = liteCmd.ExecuteReader();

            //loop through the results
            while (rdr.Read())
            {
                //clear parameters for each new insert
                cmd.Parameters.Clear();

                int location_id = Convert.ToInt32(rdr["location_id"]);
                bool location_done = Convert.ToBoolean(rdr["location_done"]);

                DateTime? action_complete = null;
                try
                {
                    action_complete = Convert.ToDateTime(rdr["action_complete"]);
                }
                catch {}

                int? auditCount = null;

                try
                {
                    auditCount = Convert.ToInt32(rdr["count"]);
                }
                catch {}

                cmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(auditid));
                cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                cmd.Parameters.AddWithValue("@location_done", dbNull(location_done));
                cmd.Parameters.AddWithValue("@action_complete", dbNull(action_complete));
                cmd.Parameters.AddWithValue("@count", dbNull(auditCount));

                cmd.ExecuteNonQuery();
            }

            cmd.Dispose();
            liteCmd.Dispose();
            liteCon.Close();
            liteCon.Dispose();
        }

        [CoverageExclude]
        private void insertHandheldData(string databaseLocation, int auditid)
        {

            SQLiteConnection liteCon = new SQLiteConnection("Data Source=" + databaseLocation);
            liteCon.Open();
            SQLiteTransaction tran = liteCon.BeginTransaction();


            //insert workcenter
            try
            {
                insertHHWorkCenters(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into WorkCenter : " + ex.Message);               
            }

            //insert locations - only the locations associated with the selected audit
            try
            {
                insertHHLocationsByAudit(databaseLocation, tran, liteCon, auditid);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into Locations : " + ex.Message);
            }


            //insert locations_cr
            //only the locations associated with the selected audit
            try
            {
                insertHHLocationCR(databaseLocation, tran, liteCon, auditid);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into Locations_cr : " + ex.Message);
            }

            //insert inventory_audit_cr
            //only the change records associated with the selected audit
            try
            {
                insertHHInventoryAuditCR(databaseLocation, tran, liteCon, auditid);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into inventory_audit_cr : " + ex.Message);
            }

            //insert config
            try
            {
                insertHHConfigForAudit(databaseLocation, tran, liteCon, auditid);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into inventory_audit_cr : " + ex.Message);
            }

            //insert niin_catalog
            try
            {
                insertHHNiinCatalog(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into niin_catalog : " + ex.Message);
            }

            //insert mfg_catalog
            try
            {
                insertHHMfgCatalog(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into mfg_catalog : " + ex.Message);
            }


            //insert inventory - only the inventory in the locations
            try
            {
                insertHHInventoryForAudit(databaseLocation, tran, liteCon, auditid);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into Inventory : " + ex.Message);
            }

            
            try
            {
                insertHHShelfLifeCodeForAudit(databaseLocation, tran, liteCon);

            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into Shelf_life_code : " + ex.Message);

            }


            try
            {
                insertHHVolumeForAudit(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into Volumes : " + ex.Message);
            }


            try
            {
                insertHHAtmosphereControl(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into atmosphere_control : " + ex.Message);
            }


            try
            {
                insertHHMsds(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into msds : " + ex.Message);
            }

            //////////////HAZARD STUFF
            try
            {
                insertHHHazardWarningsForReceiving(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazard warnings : " + ex.Message);
            }

            //insert hazards
            try
            {
                insertHHHazardsForReceiving(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazards : " + ex.Message);
            }

            try
            {

                insertHHDecantingHazardousHCC(databaseLocation, tran, liteCon);
            }
            catch (Exception ex)
            {
                liteCon.Close();
                throw new Exception("Error Inserting into hazardous_hcc : " + ex.Message);
            }
            ////////////END HAZARD STUFF

            tran.Commit();
            liteCon.Close();

        }


        public void insertHHVolumeForAudit(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select * from volumes", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            //sqlite connection and statement
            string liteStmt = "insert into volumes (volume_id, volume_description, volume_value) VALUES(@volume_id, @volume_description, @volume_value)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();
                                                
                int volumeId = Convert.ToInt32(rdr["volume_id"]);


                string volDesc = null;
                try
                {
                    volDesc = Convert.ToString(rdr["volume_description"]);
                }
                catch {}

                double? volVal = null;
                try
                {
                    volVal = Convert.ToDouble(rdr["volume_value"]);
                }
                catch {}
              
                liteCmd.Parameters.AddWithValue("@volume_id", dbNull(volumeId));
                liteCmd.Parameters.AddWithValue("@volume_description", dbNull(volDesc));
                liteCmd.Parameters.AddWithValue("@volume_value", dbNull(volVal));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHAtmosphereControl(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select * from atmosphere_control", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            //sqlite connection and statement
            string liteStmt = "insert into atmosphere_control (atmosphere_control_id, atmosphere_control_number, cage,niin,msds_serial_number, shelf_life_expiration_date) VALUES(@atmosphere_control_id, @atmosphere_control_number, @cage,@niin,@msds_serial_number, @shelf_life_expiration_date)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();

                int atmosphereControlId = Convert.ToInt32(rdr["atmosphere_control_id"]);


                string atmosphere_control_number = null;
                try
                {
                    atmosphere_control_number = Convert.ToString(rdr["atmosphere_control_number"]);
                }
                catch {}

                string cage = "";
                try
                {
                    cage = Convert.ToString(rdr["cage"]);
                }
                catch {}


                string niin = "";
                try
                {
                    niin = Convert.ToString(rdr["niin"]);
                }
                catch {}

                string msds_serial_number = "";
                try
                {
                    msds_serial_number = Convert.ToString(rdr["msds_serial_number"]);
                }
                catch {}

                DateTime? shelf_life = null;
                try
                {
                    shelf_life = Convert.ToDateTime(rdr["shelf_life"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@atmosphere_control_id", dbNull(atmosphereControlId));
                liteCmd.Parameters.AddWithValue("@atmosphere_control_number", dbNull(atmosphere_control_number));
                liteCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                liteCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                liteCmd.Parameters.AddWithValue("@msds_serial_number", dbNull(msds_serial_number));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHMsds(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select msds_id, msdsserno, cage, manufacturer, partno, fsc, niin, hcc from msds join hcc on msds.hcc_id = hcc.hcc_id", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            //sqlite connection and statement
            string liteStmt = "insert into msds (msdsserno, cage, manufacturer, partno, fsc, niin, hcc) VALUES(@msdsserno, @cage, @manufacturer, @partno, @fsc, @niin, @hcc)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();

                
                string msdsserno = null;
                try
                {
                    msdsserno = Convert.ToString(rdr["msdsserno"]);
                }
                catch {}

                string cage = "";
                try
                {
                    cage = Convert.ToString(rdr["cage"]);
                }
                catch {}


                string manufacturer = "";
                try
                {
                    manufacturer = Convert.ToString(rdr["manufacturer"]);
                }
                catch {}


                string partno = "";
                try
                {
                    partno = Convert.ToString(rdr["partno"]);
                }
                catch {}


                string fsc = "";
                try
                {
                    fsc = Convert.ToString(rdr["fsc"]);
                }
                catch {}

                string niin = "";
                try
                {
                    niin = Convert.ToString(rdr["niin"]);
                }
                catch {}

                string hcc = "";
                try
                {
                    hcc = Convert.ToString(rdr["hcc"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@msdsserno", dbNull(msdsserno));
                liteCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                liteCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                liteCmd.Parameters.AddWithValue("@partno", dbNull(partno));
                liteCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                liteCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                liteCmd.Parameters.AddWithValue("@hcc", dbNull(hcc));

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }

        public void insertHHShelfLifeCodeForAudit(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select * from shelf_life_code", mdfConn);
            SqlDataReader rdr = cmd.ExecuteReader();

            //sqlite connection and statement
            string liteStmt = "insert into shelf_life_code (shelf_life_code_id,slc,time_in_months, description, type) VALUES(@shelf_life_code_id,@slc,@time_in_months, @description, @type)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            while (rdr.Read())
            {
                liteCmd.Parameters.Clear();

                int shelf_life_code_id = Convert.ToInt32(rdr["shelf_life_code_id"]);

                string slc = null;
                try
                {
                    slc = Convert.ToString(rdr["slc"]);
                }
                catch {}

                int? time_in_months = null;
                try
                {
                  time_in_months = Convert.ToInt32(rdr["time_in_months"]);
                }
                catch {}

                string description = null;
                try
                {
                  description = Convert.ToString(rdr["description"]);
                }
                catch {}

                string type = null;
                try
                {
                  type = Convert.ToString(rdr["type"]);
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                liteCmd.Parameters.AddWithValue("@slc", dbNull(slc));
                liteCmd.Parameters.AddWithValue("@time_in_months", dbNull(time_in_months));
                liteCmd.Parameters.AddWithValue("@description", dbNull(description));
                liteCmd.Parameters.AddWithValue("@type", dbNull(type));

                liteCmd.ExecuteNonQuery();

            }
          
            //close
            liteCmd.Dispose();
            mdfConn.Close();


        }


        public void insertHHInventoryForAudit(string databaseLocation, SQLiteTransaction tran, SQLiteConnection liteCon, int auditid)
        {

            

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //first, get the list of locations associated with the selected audit
            List<int> locationList = new List<int>();
            SqlCommand cmd = new SqlCommand("select location_id from locations_in_audit where inventory_audit_id = @auditid", mdfConn);
            cmd.Parameters.AddWithValue("auditid", auditid);
            SqlDataReader rdr = cmd.ExecuteReader();

            //loop through results and add them to the list
            while (rdr.Read())
            {
                locationList.Add(Convert.ToInt32(rdr["location_id"]));
            }

            //close reader            
            rdr.Close();
            cmd.Dispose();


            //check to make sure at least one location is associated with this audit
            if (locationList.Count < 1)
            {
                //close connection
                mdfConn.Close();

                throw new Exception("No locations have been added to this audit");

            }

            string locStr = "";
            int count = 1;

            foreach (int i in locationList)
            {

                if (count != locationList.Count)
                {

                    locStr += i + ",";
                }
                else
                {

                    locStr += i;

                }

                count++;
            }



            //sqlite connection and statement
            string liteStmt = "insert into inventory (inventory_id, niin_cat_id,mfg_cat_id,location_id,workcenter_id,qty,ui,shelf_life_expiration_date, um, cosal, msds_serial_no,contract, atmosphere_control_number) " +
                              "VALUES (@inventory_id,@niin_cat_id,@mfg_cat_id,@location_id,@workcenter_id,@qty,@ui,@shelf_life_expiration_date, @um, @cosal,@msds_serial_no,@contract, @atmosphere_control_number)";
            SQLiteCommand liteCmd = new SQLiteCommand(liteStmt, liteCon);
            liteCmd.Transaction = tran;

            string stmt = "select i.alternate_ui as alternate_ui, i.mfg_catalog_id,  i.shelf_life_expiration_date, i.alternate_um, n.um, " +
                            "i.qty, i.location_id, i.inventory_id, n.niin_catalog_id, l.workcenter_id, n.ui as ui, i.cosal, i.serial_number, i.contract_number, i.atm " +

                            "from inventory i, niin_catalog n, mfg_catalog m, locations l " +

                            "where l.location_id in(" + locStr + ") " +
                            "AND l.location_id = i.location_id " +
                            "AND i.mfg_catalog_id = m.mfg_catalog_id " +
                            "AND n.niin_catalog_id = m.niin_catalog_id";


           cmd = new SqlCommand(stmt, mdfConn);
           rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                cmd.Parameters.Clear();                             
                liteCmd.Parameters.Clear();



                int? mfg_catalog_id = null;

                try
                {
                    mfg_catalog_id = Convert.ToInt32(rdr["mfg_catalog_id"]);
                }
                catch {}

                DateTime? shelfLifeExpirationDate = null;

                try
                {
                    shelfLifeExpirationDate = Convert.ToDateTime(rdr["shelf_life_expiration_date"]);
                }
                catch {}


                int? location_id = null;

                try
                {

                    location_id = Convert.ToInt32(rdr["location_id"]);
                }
                catch {}

                int? qty = null;

                try
                {
                    qty = Convert.ToInt32(rdr["qty"]);
                }
                catch {}

                int? niin_catalog_id = null;

                try
                {
                    niin_catalog_id = Convert.ToInt32(rdr["niin_catalog_id"]);
                }
                catch {}

                int? inventoryId = null;

                try
                {
                    inventoryId = Convert.ToInt32(rdr["inventory_id"]);
                }
                catch {}

                int? workcenterId = null;

                try
                {
                    workcenterId = Convert.ToInt32(rdr["workcenter_id"]);
                }
                catch {}
                string alternateUi = null;

                try
                {
                    alternateUi = Convert.ToString(rdr["alternate_ui"]);
                }
                catch {}


                string ui = null;

                try
                {
                    ui = Convert.ToString(rdr["ui"]);
                }
                catch {}

                if (alternateUi != null && !alternateUi.Equals(""))
                {
                    ui = alternateUi;
                                        
                }

                //make the UI value uppercase before sending to the handheld
                if (ui != null)
                {
                    ui = ui.ToUpper();
                }


                string alternateUm = null;

                try
                {
                    alternateUm = Convert.ToString(rdr["alternate_um"]);
                }
                catch {}


                string um = null;

                try
                {
                    um = Convert.ToString(rdr["um"]);
                }
                catch {}

                if (alternateUm != null && !alternateUm.Equals(""))
                {
                    um = alternateUm;
                }

                if (um != null)
                {
                    um = um.ToUpper();
                }


                string cosal = null;

                try
                {
                    cosal = Convert.ToString(rdr["cosal"]);
                    cosal = cosal.ToUpper();
                }
                catch {}

                string serialNumber = null;

                try
                {
                    serialNumber = Convert.ToString(rdr["serial_number"]);
                    serialNumber = serialNumber.ToUpper();
                }
                catch {}

                string contract = null;

                try
                {
                    contract = Convert.ToString(rdr["contract_number"]);
                    contract = contract.ToUpper();
                }
                catch {}


                string atm = null;

                try
                {
                    atm = Convert.ToString(rdr["atm"]);
                    
                }
                catch {}

                liteCmd.Parameters.AddWithValue("@mfg_cat_id", dbNull(mfg_catalog_id));
                liteCmd.Parameters.AddWithValue("@inventory_id", dbNull(inventoryId));
                liteCmd.Parameters.AddWithValue("@niin_cat_id", dbNull(niin_catalog_id));
                liteCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
                liteCmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenterId));
                //liteCmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternateUi));
                liteCmd.Parameters.AddWithValue("@qty", dbNull(qty));                
                liteCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                liteCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelfLifeExpirationDate));
                liteCmd.Parameters.AddWithValue("@um", dbNull(um));
                liteCmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
                liteCmd.Parameters.AddWithValue("@msds_serial_no", dbNull(serialNumber));
                liteCmd.Parameters.AddWithValue("@contract", dbNull(contract));
                liteCmd.Parameters.AddWithValue("@atmosphere_control_number", dbNull(atm));
               

                liteCmd.ExecuteNonQuery();

            }

            //close
            liteCmd.Dispose();
            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
         
        }

        public Hashtable createClipboard(int auditid, string dbLocation)
        {

            Hashtable table = new Hashtable();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();
            SqlTransaction tran = mdfConn.BeginTransaction();
            

            //create tempFile to copy the template to - 
            string tempFileLocation = Path.GetTempFileName();

            //copy template
            File.Copy(dbLocation, tempFileLocation, true);

            try
            {
                insertHandheldData(tempFileLocation, auditid);
            }
            catch (Exception ex)
            {
                File.Delete(tempFileLocation);
                throw new Exception(ex.Message);
            }
            //database has the data inserted at this point.  save the handheld database to the server database - pass to the user to download

            FileStream fs = new FileStream(tempFileLocation, FileMode.Open, FileAccess.Read);
            byte[] fileData = new byte[fs.Length];

            // read in the file stream to the byte array
            fs.Read(fileData, 0, Convert.ToInt32(fs.Length));
            // close the file stream
            fs.Close();

            string selectStmt = "insert into clipboard (name, deleted, archived, clip_file, inventory_audit_id) VALUES (@name,@deleted,@archived,@clip,@audit);  SELECT SCOPE_IDENTITY()";

            string descripton = getAuditDescription(auditid);

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@name", "clip_" + descripton);
            cmd.Parameters.AddWithValue("@deleted", 0);
            cmd.Parameters.AddWithValue("@archived", 0);
            cmd.Parameters.AddWithValue("@clip", fileData);
            cmd.Parameters.AddWithValue("@audit", auditid);

            //cmd.ExecuteNonQuery();

            object o = cmd.ExecuteScalar();

            int clipid = Convert.ToInt32(o);

            tran.Commit();

            mdfConn.Close();

            //delete the temporary file - it is no longer needed
            File.Delete(tempFileLocation);

            table.Add("file", fileData);
            table.Add("clipid", clipid);

            return table;

        }

        [CoverageExclude]
        public DataTable getFilteredAvailableLocationsFromSelected(DataTable selectedTable, int auditid, string filterValue, string workCenter, bool useAuditInfo)
        {

            //build string for select stmt

            string str = "";
            int count = 1;

            foreach (DataRow row in selectedTable.Rows)
            {

                if (count != selectedTable.Rows.Count)
                {
                    str += row[0].ToString() + ",";
                }

                //end 
                else
                {

                    str += row[0].ToString();
                }




                count++;


            }


            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //select locations where their id does not match the ID from the selected DataTable (parameter)
            string selectStmt = "";

           // string filterStmt = " WHERE  nc." + whereColumn + " like '%' + @filter + '%' ";

            if (!str.Equals(""))
            {

                selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                   "FROM locations l, workcenter w " +
                                  "where l.workcenter_id = w.workcenter_id " +
                                  "AND l.location_id not in(" + str + ") " +
                                  "AND l.name like '%' + @filter + '%' " +
                                  "AND w.description like '%' + @workFilter + '%'";

            }
            else
            {

                selectStmt = "select l.location_id, w.workcenter_id,  w.description as Workcenter, l.name as Location " +
                                  "FROM locations l, workcenter w " +
                                 "where l.workcenter_id = w.workcenter_id " +                                 
                                 "AND l.name like '%' + @filter + '%' " +
                                 "AND w.description like '%' + @workFilter + '%'";



            }

            //string selectStmt = "select distinct l.location_id, w.workcenter_id, w.description as Workcenter, l.name as Location " +
            //                    "from workcenter w, locations l, inventory_audits i, locations_in_audit a " +
            //                    "where a.inventory_audit_id = i.inventory_audit_id " +
            //                    "AND l.workcenter_id = w.workcenter_id " +
            //                    "AND a.location_id = l.location_id " + 
            //                    "AND l.location_id not in(" + str + ")";



            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@filter", filterValue);
            cmd.Parameters.AddWithValue("@workFilter", workCenter);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }



        public DataTable getLocationListFromRecurringAudit(int auditid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //string selectStmt = "select l.location_id w.workcenter_id, l.name, w.description  from locations l, workcenter w " +
            //                   "where l.workcenter_id = w.workcenter_id " + 
            //                   "AND i.inventory_audit;

            string selectStmt = "select l.location_id, w.workcenter_id, w.description as Workcenter, l.name as Location " +
                                "from workcenter w, locations l, recurring_audits i, locations_in_recurring_audits a " +
                                "where a.recurring_audit_id = i.recurring_audit_id " +
                                "AND l.workcenter_id = w.workcenter_id " +
                                "AND a.location_id = l.location_id " +
                                "AND i.recurring_audit_id = @recurring_id";
                                
            


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@recurring_id", auditid);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }

        public DataTable getLocationListFromAudit(int auditid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            //string selectStmt = "select l.location_id w.workcenter_id, l.name, w.description  from locations l, workcenter w " +
            //                   "where l.workcenter_id = w.workcenter_id " + 
            //                   "AND i.inventory_audit;

            string selectStmt = "select l.location_id, w.workcenter_id, w.description as Workcenter, l.name as Location " +
                                "from workcenter w, locations l, inventory_audits i, locations_in_audit a " +
                                "where a.inventory_audit_id = i.inventory_audit_id " +
                                "AND l.workcenter_id = w.workcenter_id " +
                                "AND a.location_id = l.location_id " +
                                "AND i.inventory_audit_id = @auditid";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@auditid", auditid);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }

        
        public List<int> getInventoryAuditID(DateTime selectedTime)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            List<int> auditList = new List<int>();

            mdfConn.Open();

            string selectStmt = "select inventory_audit_id from inventory_audits where scheduled_on = @date AND deleted = @flag";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@date", selectedTime);
            cmd.Parameters.AddWithValue("@flag", false);

            SqlDataReader rdr = cmd.ExecuteReader();            

            while (rdr.Read())
            {

                int id = Int32.Parse(rdr["inventory_audit_id"].ToString());
                auditList.Add(id);

            }
            

            cmd.Dispose();
            mdfConn.Close();


            return auditList;

        }


        public List<int> getRecurringIdsByDate(DateTime selectedTime)
        {

            DayOfWeek selectedDay = selectedTime.DayOfWeek;
            int day = selectedTime.Day;

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            List<int> recurringList = new List<int>();

            mdfConn.Open();

            string selectStmt = "select * from recurring_audits where deleted = @flag";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@flag", false);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {

                DateTime recurringInitialDate = (DateTime)rdr["initially_scheduled_on"];
                string recur = rdr["recurring"].ToString();

                //check to make sure the selected_date is AFTER the scheduled_on date - we do not want to show recurring audits for dates prior to when the recurring audit was created
                if (selectedTime >= recurringInitialDate)
                {

                    //this audit is weekly
                    if (recur.ToLower().Equals("weekly"))
                    {
                        DayOfWeek recurringDay = recurringInitialDate.DayOfWeek;

                        //this day falls on the same day as this recurring audit..we'll add to the list to show to the user.
                        if (recurringDay == selectedDay)
                        {
                            //List<int> auditidList = getAuditIdFromRecurringId(Convert.ToInt32(rdr["recurring_audit_id"].ToString()));
                            recurringList.Add(Convert.ToInt32(rdr["recurring_audit_id"]));
                        }

                    }
                    else if (recur.ToLower().Equals("monthly"))
                    {

                        //the days fall on the same day for the month as the recurring audit..  add this to the list
                        if (recurringInitialDate.Day == day)
                        {                           
                            //List<int> auditidList = getAuditIdFromRecurringId(Convert.ToInt32(rdr["recurring_audit_id"].ToString()));
                            recurringList.Add(Convert.ToInt32(rdr["recurring_audit_id"]));                          
                        }

                    }

                }

            }

            cmd.Dispose();
            mdfConn.Close();

            return recurringList;

        }


        public List<string> getStatusList()
        {

            List<string> statusList = new List<string>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select description from ship_statuses";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                statusList.Add(rdr["description"].ToString());
            }


            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


            return statusList;

        }

        public List<string> getWorkCenterList()
        {

            List<string> workList = new List<string>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select description from workcenter";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {                             
                workList.Add(rdr["description"].ToString());
            }


            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


            return workList;

        }


        public DataTable getAuditInfoCollection(List<int> auditList)
        {


            //build string for select stmt

            string str = "";
            int count = 1;

            foreach (int i in auditList)
            {

                if (count != auditList.Count)
                {
                    str += i + ",";
                }

                //end 
                else
                {

                    str += i;
                }

                count++;
            }


            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            //string selectStmt = "select l.name as Location, i.scheduled_on as Scheduled_Date, i.description as Audit_Description, " +  
            //                    "s.description as Ship_Status, w.description as Workcenter_Descripton  from ship_statuses s, inventory_audits i, " + 
            //                    "locations l, workcenter w, locations_in_audit a " + 
            //                    "where l.workcenter_id = w.workcenter_id " +
            //                    "AND a.location_id = l.location_id " +
            //                    "AND i.inventory_audit_id = a.inventory_audit_id " +
            //                    "AND s.status_id = i.scheduled_status_id " +
            //                    "AND i.scheduled_on = @selectedDate";


            string selectStmt = "select 0 as recurring_audit_id, i.inventory_audit_id, i.description as Audit, i.scheduled_on as Scheduled_Date, s.description as Status " +
                                "from ship_statuses s,inventory_audits i " + 
                                "where s.status_id = i.ship_status_id " + 
                                "AND i.inventory_audit_id in(" + str + ")";
            
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

           // cmd.Parameters.AddWithValue("@selectedDate", selectedDate);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            mdfConn.Close();

            return dt;

        }



        public DataTable getRecurringCollectionFromRecurringIDs(List<int> recurringList)
        {


            //build string for select stmt

            string str = "";
            int count = 1;

            foreach (int i in recurringList)
            {

                if (count != recurringList.Count)
                {
                    str += i + ",";
                }

                //end 
                else
                {

                    str += i;
                }

                count++;
            }


            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

           string selectStmt =
                "select 0 as inventory_audit_id, r.recurring_audit_id, " +
          "r.description as Audit, " +
          "r.initially_scheduled_on as Scheduled_Date, " +
          "s.description as Status " +
          "from ship_statuses s,recurring_audits r " +
          "where s.status_id = r.ship_status " +
          "AND r.deleted = 0 " +
          "AND r.recurring_audit_id in(" + str + ")";

            
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            // cmd.Parameters.AddWithValue("@selectedDate", selectedDate);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            mdfConn.Close();

            return dt;

        }



        public string getTaskDescription(int taskid)
        {

            string desc = "";

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select i.description from decanting_tasks i where i.decanting_task_id = @taskid";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@taskid", taskid);


            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                desc = rdr["description"].ToString();

                break;
            }


            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            return desc;

        }

        public DD1348Item getDD1348DataForGarbageId(int garbage_offload_id)
        {

            DD1348Item itemData = null;

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select o.ui, o.garbage_offload_id, o.offload_detail_id, o.garbage_item_id, o.qty, o.archived_id, o.description, o.offload_qty "
                + " from v_garbage_list_top o where o.garbage_offload_id = @garbage_offload_id";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@garbage_offload_id", garbage_offload_id);


            SqlDataReader rdr = cmd.ExecuteReader();

            string offload_detail_id = null;
            while (rdr.Read())
            {

                string description = rdr["description"].ToString();
               
                string OffloadQuantity = rdr["offload_qty"].ToString();
                if (OffloadQuantity.Equals(""))
                    OffloadQuantity = rdr["qty"].ToString();                             

                offload_detail_id = rdr["offload_detail_id"].ToString();

                itemData = new DD1348Item();
                itemData.UnitIss = rdr["ui"].ToString();
                itemData.ItemNomen = description;
                itemData.Quant = OffloadQuantity;
                itemData.FRGHT_CLASS_NOM = description;
                itemData.ITEM_NOM = description;

            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            if (offload_detail_id != "" && offload_detail_id != null)
            {
                DataTable dt = findOffloadDetails(Int32.Parse(offload_detail_id));
                DataRow row = dt.Rows[0];
                itemData.DOC_IDENT = "" + row["DOC_IDENT"].ToString();
                itemData.RI_FROM = "" + row["RI_FROM"].ToString();
                itemData.M_S = "" + row["M_S"].ToString();
                itemData.SER = "" + row["SER"].ToString();
                itemData.SUPP_ADDRESS = "" + row["SUPP_ADDRESS"].ToString();
                itemData.SIG = "" + row["SIG"].ToString();
                itemData.FUND = "" + row["FUND"].ToString();
                itemData.DISTRIBUTION = "" + row["DISTRIBUTION"].ToString();
                itemData.PROJECT = "" + row["PROJECT"].ToString();
                itemData.PRI = "" + row["PRI"].ToString();
                itemData.REQ_DEL_DATE = "" + row["REQ_DEL_DATE"].ToString();
                itemData.RI = "" + row["RI"].ToString();
                itemData.O_P = "" + row["O_P"].ToString();
                itemData.COND = "" + row["COND"].ToString();
                itemData.MGT = "" + row["MGT"].ToString();
                itemData.UNIT_DOLLARS = "" + row["UNIT_DOLLARS"].ToString();

                itemData.UNIT_CTS = "" + row["UNIT_CTS"].ToString();
                itemData.TOTAL_DOLLARS = "" + row["TOTAL_DOLLARS"].ToString();
                itemData.TOTAL_CTS = "" + row["TOTAL_CTS"].ToString();
                itemData.SHIP_FROM = "" + row["SHIP_FROM"].ToString();
                itemData.SHIP_TO = "" + row["SHIP_TO"].ToString();
                itemData.MARK_FOR = "" + row["MARK_FOR"].ToString();
                itemData.DOC_DATE = "" + row["DOC_DATE"].ToString();
                itemData.NMFC = "" + row["NMFC"].ToString();
                itemData.TYPE_CARGO = "" + row["TYPE_CARGO"].ToString();
                itemData.PS = "" + row["PS"].ToString();
                itemData.UP = "" + row["UP"].ToString();
                itemData.UNIT_WEIGHT = "" + row["UNIT_WEIGHT"].ToString();
                itemData.UNIT_CUBE = "" + row["UNIT_CUBE"].ToString();
                itemData.UFC = "" + row["UFC"].ToString();
                itemData.SL = "" + row["SL"].ToString();

                itemData.FRGHT_CLASS_NOM = "" + row["FRGHT_CLASS_NOM"].ToString();
                itemData.ITEM_NOM = "" + row["ITEM_NOM"].ToString();
                itemData.HCC_MSG = "" + row["HCC_MSG"].ToString();
                itemData.DMIL = "" + row["DMIL"].ToString();
                itemData.JON = "" + row["JON"].ToString();
                itemData.HCC = "" + row["HCC"].ToString();
                itemData.CIIC = "" + row["CIIC"].ToString();
                itemData.TY_CARGO_MSG = "" + row["TY_CARGO_MSG"].ToString();
                itemData.MSDS = "" + row["MSDS"].ToString();
                itemData.DOCUMENT_NUMBER = "" + row["DOCUMENT_NUMBER"].ToString();
                itemData.RICUIQTY = "" + row["RICUIQTY"].ToString();
                itemData.PCN = "" + row["PCN"].ToString();

                try { itemData.date_value = DateTime.Parse(row["date_value"].ToString());
                itemData.serial_number = Convert.ToInt32(row["serial_number"]);
                itemData.iteration = Convert.ToInt32(row["iteration"]);
                }
                catch { }
            }

            return itemData;

        }

        public DD1348Item getDD1348DataForMSDSId(int msds_id)
        {

            DD1348Item itemData = null;

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select m.msdsserno, m.hcc_id, h.HCC, z.hazard_name, m.PRODUCT_IDENTITY"
                + " "
                + "  from msds m  "
               
                + "  LEFT JOIN HCC h ON (h.hcc_id=m.hcc_id) "
                + "  LEFT JOIN hazardous_hcc zh ON (zh.hcc=h.hcc) "
                 + "  LEFT JOIN hazards z ON (z.hazard_id=zh.hazard_id) "

                + " where m.msds_id = @msds_id";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);


            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {

               
                string HCC = rdr["hcc"].ToString();
                string hazard_name = rdr["hazard_name"].ToString();
                string product_identity = rdr["PRODUCT_IDENTITY"].ToString();

                string MSDSSERNO = rdr["msdsserno"].ToString();
               

                itemData = new DD1348Item();

                itemData.HCC = HCC;
                itemData.MSDS = MSDSSERNO;
                itemData.TY_CARGO_MSG = hazard_name;
                itemData.HCC_MSG = product_identity;

            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            return itemData;

        }

        public DD1348Item getDD1348DataForOffloadId(int offload_list_id)
        {

            DD1348Item itemData = null;

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select o.contract_number, o.offload_list_id,o.manufacturer_date, o.shelf_life_expiration_date as expiration_date, o.offload_detail_id,  o.alternate_ui, o.description AS nomenclature, o.fsc, o.niin, o.cage, "
                + " o.offload_qty, o.qty_offloaded, o.ui, s.smcc, l.description as slc, a.description as slac"
                +"  from v_offload_list_top o  LEFT JOIN smcc s ON (s.smcc_id=o.smcc_id) "
                
                 + "  LEFT JOIN shelf_life_code l ON (l.shelf_life_code_id=o.shelf_life_code_id) "
                  + "  LEFT JOIN shelf_life_action_code a ON (a.shelf_life_action_code_id=o.shelf_life_action_code_id) "
                +" where o.offload_list_id = @offload_list_id";

            //Only get msds based on msdsserno
            string msdsStmt = "select o.offload_list_id, m.msdsserno, m.hcc_id, h.HCC, m.PRODUCT_IDENTITY "
              
               + "  from v_offload_list_top o INNER JOIN V_msds_contractor m ON (o.serial_number=m.MSDSSERNO)  "

               + "  LEFT JOIN HCC h ON (h.hcc_id=m.hcc_id) "
               
               + " where o.offload_list_id = @offload_list_id";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@offload_list_id", offload_list_id);

            SqlCommand cmdMSDS = new SqlCommand(msdsStmt, mdfConn);
            cmdMSDS.Parameters.AddWithValue("@offload_list_id", offload_list_id);

            DataTable dtMsds = new DataTable();

            new SqlDataAdapter(cmdMSDS).Fill(dtMsds);


            SqlDataReader rdr = cmd.ExecuteReader();

            string offload_detail_id = null;
            while (rdr.Read())
            {
               
                string Nomenclature = rdr["nomenclature"].ToString();
                string FSC = rdr["fsc"].ToString();
                string NIIN = rdr["niin"].ToString();

                
                string CAGE = rdr["cage"].ToString();



                string HCC = "";
                string hazard_name = "";
                string product_identity = "";
                string MSDSSERNO = "";
                if (dtMsds.Rows.Count == 1)
                {
                
                    HCC = dtMsds.Rows[0]["HCC"].ToString();
                   // hazard_name = dtMsds.Rows[0]["hazard_name"].ToString();
                    product_identity = dtMsds.Rows[0]["PRODUCT_IDENTITY"].ToString();
                    MSDSSERNO = dtMsds.Rows[0]["msdsserno"].ToString();
                }


                string OffloadQuantity = rdr["qty_offloaded"].ToString();
                if(OffloadQuantity.Equals(""))
                    OffloadQuantity = rdr["offload_qty"].ToString();

                string UI = rdr["ui"].ToString();
                string ALT_UI = rdr["alternate_ui"].ToString();

                if (ALT_UI != "")
                    UI = ALT_UI;

                string SMCC = rdr["smcc"].ToString();
                string SLCDescription = rdr["slc"].ToString();
                string SLACDescription = rdr["slac"].ToString();

                offload_detail_id = rdr["offload_detail_id"].ToString();

               itemData = new DD1348Item(Nomenclature, FSC, NIIN, CAGE, MSDSSERNO, OffloadQuantity, UI, SMCC, SLCDescription, SLACDescription);

               itemData.HCC = HCC;
               itemData.MSDS = MSDSSERNO;
               itemData.TY_CARGO_MSG = hazard_name;
               itemData.HCC_MSG = product_identity;


               itemData.FRGHT_CLASS_NOM = Nomenclature;
               itemData.ITEM_NOM = Nomenclature;

               itemData.manufacturer_date = ""+rdr["manufacturer_date"];
               itemData.expiration_date = "" + rdr["expiration_date"];


            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            if (offload_detail_id != "" && offload_detail_id != null)
            {
                DataTable dt = findOffloadDetails(Int32.Parse(offload_detail_id));
                DataRow row = dt.Rows[0];
                itemData.DOC_IDENT = "" + row["DOC_IDENT"].ToString();
                itemData.RI_FROM = "" + row["RI_FROM"].ToString();
                itemData.M_S = "" + row["M_S"].ToString();
                itemData.SER = "" + row["SER"].ToString();
                itemData.SUPP_ADDRESS = "" + row["SUPP_ADDRESS"].ToString();
                itemData.SIG = "" + row["SIG"].ToString();
                itemData.FUND = "" + row["FUND"].ToString();
                itemData.DISTRIBUTION = "" + row["DISTRIBUTION"].ToString();
                itemData.PROJECT = "" + row["PROJECT"].ToString();
                itemData.PRI = "" + row["PRI"].ToString();
                itemData.REQ_DEL_DATE = "" + row["REQ_DEL_DATE"].ToString();
                itemData.RI = "" + row["RI"].ToString();
                itemData.O_P = "" + row["O_P"].ToString();
                itemData.COND = "" + row["COND"].ToString();
                itemData.MGT = "" + row["MGT"].ToString();
                itemData.UNIT_DOLLARS = "" + row["UNIT_DOLLARS"].ToString();

                itemData.UNIT_CTS = "" + row["UNIT_CTS"].ToString();
                itemData.TOTAL_DOLLARS = "" + row["TOTAL_DOLLARS"].ToString();
                itemData.TOTAL_CTS = "" + row["TOTAL_CTS"].ToString();
                itemData.SHIP_FROM = "" + row["SHIP_FROM"].ToString();
                itemData.SHIP_TO = "" + row["SHIP_TO"].ToString();
                itemData.MARK_FOR = "" + row["MARK_FOR"].ToString();
                itemData.DOC_DATE = "" + row["DOC_DATE"].ToString();
                itemData.NMFC = "" + row["NMFC"].ToString();
                itemData.TYPE_CARGO = "" + row["TYPE_CARGO"].ToString();
                itemData.PS = "" + row["PS"].ToString();
                itemData.UP = "" + row["UP"].ToString();
                itemData.UNIT_WEIGHT = "" + row["UNIT_WEIGHT"].ToString();
                itemData.UNIT_CUBE = "" + row["UNIT_CUBE"].ToString();
                itemData.UFC = "" + row["UFC"].ToString();
                itemData.SL = "" + row["SL"].ToString();

                itemData.FRGHT_CLASS_NOM = "" + row["FRGHT_CLASS_NOM"].ToString();
                itemData.ITEM_NOM = "" + row["ITEM_NOM"].ToString();
                itemData.HCC_MSG = "" + row["HCC_MSG"].ToString();
                itemData.DMIL = "" + row["DMIL"].ToString();
                itemData.JON = "" + row["JON"].ToString();
                itemData.HCC = "" + row["HCC"].ToString();
                itemData.CIIC = "" + row["CIIC"].ToString();
                itemData.TY_CARGO_MSG = "" + row["TY_CARGO_MSG"].ToString();
                itemData.MSDS = "" + row["MSDS"].ToString();
                itemData.DOCUMENT_NUMBER = "" + row["DOCUMENT_NUMBER"].ToString();
                itemData.RICUIQTY = "" + row["RICUIQTY"].ToString();
                itemData.PCN = "" + row["PCN"].ToString();

                try
                {
                    itemData.date_value = DateTime.Parse(row["date_value"].ToString());
                    itemData.serial_number = Convert.ToInt32(row["serial_number"]);
                    itemData.iteration = Convert.ToInt32(row["iteration"]);
                }
                catch { }
            }

            return itemData;

        }

        public List<DD1348Item> getDD1348DatasForOffloadList()
        {

            List<DD1348Item> itemDataList = new List<DD1348Item>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();
           

            string selectStmt = "select d.*,o.contract_number, o.offload_list_id,o.manufacturer_date, o.shelf_life_expiration_date as expiration_date, o.offload_detail_id,  o.alternate_ui, o.description AS nomenclature, o.fsc, o.niin, o.cage, "
               + " o.offload_qty, o.qty_offloaded, o.ui, s.smcc, l.description as slc, a.description as slac"
               + "  from v_offload_list_top o  LEFT JOIN smcc s ON (s.smcc_id=o.smcc_id) "

                + "  LEFT JOIN offload_details d ON (d.offload_detail_id=o.offload_detail_id) "

                + "  LEFT JOIN shelf_life_code l ON (l.shelf_life_code_id=o.shelf_life_code_id) "
                 + "  LEFT JOIN shelf_life_action_code a ON (a.shelf_life_action_code_id=o.shelf_life_action_code_id) "
               + " WHERE (o.deleted IS NULL OR o.deleted=0) AND o.archived_id IS NULL";

            //Compare msds ONLY on msdsserno
            string msdsStmt = "select o.offload_list_id, m.msdsserno, m.hcc_id, h.HCC, m.PRODUCT_IDENTITY "

               + "  from v_offload_list_top o INNER JOIN V_msds_contractor m ON (o.serial_number=m.MSDSSERNO)  "

               + "  LEFT JOIN HCC h ON (h.hcc_id=m.hcc_id) "

               + " where o.offload_list_id = @offload_list_id";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            

            SqlCommand cmdMSDS = new SqlCommand(msdsStmt, conn);
           
           
            SqlDataReader rdr = cmd.ExecuteReader();
           

            while (rdr.Read())
            {
                DD1348Item itemData = null;
                string offload_list_id = rdr["offload_list_id"].ToString();

                cmdMSDS.Parameters.Clear();
                cmdMSDS.Parameters.AddWithValue("@offload_list_id", offload_list_id);
                DataTable dtMsds = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(cmdMSDS);
                adapter.Fill(dtMsds);


                    string Nomenclature = rdr["nomenclature"].ToString();
                    string FSC = rdr["fsc"].ToString();
                    string NIIN = rdr["niin"].ToString();

                    string CAGE = rdr["cage"].ToString();

                    string HCC = "";
                    string hazard_name = "";
                    string product_identity = "";
                    string MSDSSERNO = "";
                    if (dtMsds.Rows.Count == 1)
                    {

                        HCC = dtMsds.Rows[0]["HCC"].ToString();
                        // hazard_name = dtMsds.Rows[0]["hazard_name"].ToString();
                        product_identity = dtMsds.Rows[0]["PRODUCT_IDENTITY"].ToString();
                        MSDSSERNO = dtMsds.Rows[0]["msdsserno"].ToString();
                    }

                    string OffloadQuantity = rdr["qty_offloaded"].ToString();
                    if (OffloadQuantity.Equals(""))
                        OffloadQuantity = rdr["offload_qty"].ToString();

                    string UI = rdr["ui"].ToString();
                    string ALT_UI = rdr["alternate_ui"].ToString();

                    if (ALT_UI != "")
                        UI = ALT_UI;

                    string SMCC = rdr["smcc"].ToString();
                    string SLCDescription = rdr["slc"].ToString();
                    string SLACDescription = rdr["slac"].ToString();

                    itemData = new DD1348Item(Nomenclature, FSC, NIIN, CAGE, MSDSSERNO, OffloadQuantity, UI, SMCC, SLCDescription, SLACDescription);

                    itemData.HCC = HCC;
                    itemData.MSDS = MSDSSERNO;
                    itemData.TY_CARGO_MSG = hazard_name;
                    itemData.HCC_MSG = product_identity;
                    itemData.FRGHT_CLASS_NOM = Nomenclature;
                    itemData.ITEM_NOM = Nomenclature;
                    itemData.manufacturer_date = "" + rdr["manufacturer_date"];
                    itemData.expiration_date = "" + rdr["expiration_date"];
                    

                    if (rdr["offload_detail_id"] != DBNull.Value)
                    {
                        itemData.DOC_IDENT = "" + rdr["DOC_IDENT"].ToString();
                        itemData.RI_FROM = "" + rdr["RI_FROM"].ToString();
                        itemData.M_S = "" + rdr["M_S"].ToString();
                        itemData.SER = "" + rdr["SER"].ToString();
                        itemData.SUPP_ADDRESS = "" + rdr["SUPP_ADDRESS"].ToString();
                        itemData.SIG = "" + rdr["SIG"].ToString();
                        itemData.FUND = "" + rdr["FUND"].ToString();
                        itemData.DISTRIBUTION = "" + rdr["DISTRIBUTION"].ToString();
                        itemData.PROJECT = "" + rdr["PROJECT"].ToString();
                        itemData.PRI = "" + rdr["PRI"].ToString();
                        itemData.REQ_DEL_DATE = "" + rdr["REQ_DEL_DATE"].ToString();
                        itemData.RI = "" + rdr["RI"].ToString();
                        itemData.O_P = "" + rdr["O_P"].ToString();
                        itemData.COND = "" + rdr["COND"].ToString();
                        itemData.MGT = "" + rdr["MGT"].ToString();
                        itemData.UNIT_DOLLARS = "" + rdr["UNIT_DOLLARS"].ToString();

                        itemData.UNIT_CTS = "" + rdr["UNIT_CTS"].ToString();
                        itemData.TOTAL_DOLLARS = "" + rdr["TOTAL_DOLLARS"].ToString();
                        itemData.TOTAL_CTS = "" + rdr["TOTAL_CTS"].ToString();
                        itemData.SHIP_FROM = "" + rdr["SHIP_FROM"].ToString();
                        itemData.SHIP_TO = "" + rdr["SHIP_TO"].ToString();
                        itemData.MARK_FOR = "" + rdr["MARK_FOR"].ToString();
                        itemData.DOC_DATE = "" + rdr["DOC_DATE"].ToString();
                        itemData.NMFC = "" + rdr["NMFC"].ToString();
                        itemData.TYPE_CARGO = "" + rdr["TYPE_CARGO"].ToString();
                        itemData.PS = "" + rdr["PS"].ToString();
                        itemData.UP = "" + rdr["UP"].ToString();
                        itemData.UNIT_WEIGHT = "" + rdr["UNIT_WEIGHT"].ToString();
                        itemData.UNIT_CUBE = "" + rdr["UNIT_CUBE"].ToString();
                        itemData.UFC = "" + rdr["UFC"].ToString();
                        itemData.SL = "" + rdr["SL"].ToString();

                        itemData.FRGHT_CLASS_NOM = "" + rdr["FRGHT_CLASS_NOM"].ToString();
                        itemData.ITEM_NOM = "" + rdr["ITEM_NOM"].ToString();
                        itemData.HCC_MSG = "" + rdr["HCC_MSG"].ToString();
                        itemData.DMIL = "" + rdr["DMIL"].ToString();
                        itemData.JON = "" + rdr["JON"].ToString();
                        itemData.HCC = "" + rdr["HCC"].ToString();
                        itemData.CIIC = "" + rdr["CIIC"].ToString();
                        itemData.TY_CARGO_MSG = "" + rdr["TY_CARGO_MSG"].ToString();
                        itemData.MSDS = "" + rdr["MSDS"].ToString();
                        itemData.DOCUMENT_NUMBER = "" + rdr["DOCUMENT_NUMBER"].ToString();
                        itemData.RICUIQTY = "" + rdr["RICUIQTY"].ToString();
                        itemData.PCN = "" + rdr["PCN"].ToString();

                        try
                        {
                            itemData.date_value = DateTime.Parse(rdr["date_value"].ToString());
                            itemData.serial_number = Convert.ToInt32(rdr["serial_number"]);
                            itemData.iteration = Convert.ToInt32(rdr["iteration"]);
                        }
                        catch { }
                    }
                
                
                itemDataList.Add(itemData);

                    
               
            }

            //garbage item list DD1348s
            selectStmt = "select o.ui, o.garbage_offload_id, o.offload_detail_id, o.garbage_item_id, o.qty, o.archived_id, o.description, o.offload_qty "
               + " from v_garbage_list_top o where (o.deleted IS NULL OR o.deleted=0) AND o.archived_id IS NULL";
            
            cmd = new SqlCommand(selectStmt, mdfConn);
            rdr.Close();
            rdr = cmd.ExecuteReader();

            string offload_detail_id = null;
            while (rdr.Read())
            {
                DD1348Item itemData = null;
                
                string description = rdr["description"].ToString();

                string OffloadQuantity = rdr["offload_qty"].ToString();
                if (OffloadQuantity.Equals(""))
                    OffloadQuantity = rdr["qty"].ToString();

                offload_detail_id = rdr["offload_detail_id"].ToString();

                itemData = new DD1348Item();
                itemData.UnitIss = rdr["ui"].ToString();
                itemData.ItemNomen = description;
                itemData.Quant = OffloadQuantity;
                itemData.FRGHT_CLASS_NOM = description;
                itemData.ITEM_NOM = description;

                if (offload_detail_id != "")
                {
                    DataTable dt = findOffloadDetails(Int32.Parse(offload_detail_id));
                    DataRow row = dt.Rows[0];
                    itemData.DOC_IDENT = "" + row["DOC_IDENT"].ToString();
                    itemData.RI_FROM = "" + row["RI_FROM"].ToString();
                    itemData.M_S = "" + row["M_S"].ToString();
                    itemData.SER = "" + row["SER"].ToString();
                    itemData.SUPP_ADDRESS = "" + row["SUPP_ADDRESS"].ToString();
                    itemData.SIG = "" + row["SIG"].ToString();
                    itemData.FUND = "" + row["FUND"].ToString();
                    itemData.DISTRIBUTION = "" + row["DISTRIBUTION"].ToString();
                    itemData.PROJECT = "" + row["PROJECT"].ToString();
                    itemData.PRI = "" + row["PRI"].ToString();
                    itemData.REQ_DEL_DATE = "" + row["REQ_DEL_DATE"].ToString();
                    itemData.RI = "" + row["RI"].ToString();
                    itemData.O_P = "" + row["O_P"].ToString();
                    itemData.COND = "" + row["COND"].ToString();
                    itemData.MGT = "" + row["MGT"].ToString();
                    itemData.UNIT_DOLLARS = "" + row["UNIT_DOLLARS"].ToString();

                    itemData.UNIT_CTS = "" + row["UNIT_CTS"].ToString();
                    itemData.TOTAL_DOLLARS = "" + row["TOTAL_DOLLARS"].ToString();
                    itemData.TOTAL_CTS = "" + row["TOTAL_CTS"].ToString();
                    itemData.SHIP_FROM = "" + row["SHIP_FROM"].ToString();
                    itemData.SHIP_TO = "" + row["SHIP_TO"].ToString();
                    itemData.MARK_FOR = "" + row["MARK_FOR"].ToString();
                    itemData.DOC_DATE = "" + row["DOC_DATE"].ToString();
                    itemData.NMFC = "" + row["NMFC"].ToString();
                    itemData.TYPE_CARGO = "" + row["TYPE_CARGO"].ToString();
                    itemData.PS = "" + row["PS"].ToString();
                    itemData.UP = "" + row["UP"].ToString();
                    itemData.UNIT_WEIGHT = "" + row["UNIT_WEIGHT"].ToString();
                    itemData.UNIT_CUBE = "" + row["UNIT_CUBE"].ToString();
                    itemData.UFC = "" + row["UFC"].ToString();
                    itemData.SL = "" + row["SL"].ToString();

                    itemData.FRGHT_CLASS_NOM = "" + row["FRGHT_CLASS_NOM"].ToString();
                    itemData.ITEM_NOM = "" + row["ITEM_NOM"].ToString();
                    itemData.HCC_MSG = "" + row["HCC_MSG"].ToString();
                    itemData.DMIL = "" + row["DMIL"].ToString();
                    itemData.JON = "" + row["JON"].ToString();
                    itemData.HCC = "" + row["HCC"].ToString();
                    itemData.CIIC = "" + row["CIIC"].ToString();
                    itemData.TY_CARGO_MSG = "" + row["TY_CARGO_MSG"].ToString();
                    itemData.MSDS = "" + row["MSDS"].ToString();
                    itemData.DOCUMENT_NUMBER = "" + row["DOCUMENT_NUMBER"].ToString();
                    itemData.RICUIQTY = "" + row["RICUIQTY"].ToString();
                    itemData.PCN = "" + row["PCN"].ToString();

                    try
                    {
                        itemData.date_value = DateTime.Parse(rdr["date_value"].ToString());
                        itemData.serial_number = Convert.ToInt32(rdr["serial_number"]);
                        itemData.iteration = Convert.ToInt32(rdr["iteration"]);
                    }
                    catch { }
                }

                itemDataList.Add(itemData);
            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();
            conn.Close();

            return itemDataList;
        }

        public string getAuditDescription(int auditId)
        {

            string desc = "";

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select i.description from inventory_audits i where i.inventory_audit_id = @auditid";
                                

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@auditid", auditId);


            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                desc = rdr["description"].ToString();

                break;
            }


            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            return desc;

        }

        public string getShipStatusFromAudit(int auditId) {
            string statusDesc = "";
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();

            string selectStmt = "SELECT ss.description FROM ship_statuses ss " +
	            "JOIN inventory_audits ia ON ia.ship_status_id = ss.status_id " +
                "WHERE ia.inventory_audit_id = @auditid";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@auditid", auditId);

            statusDesc = (string) cmd.ExecuteScalar();
            cmd.Dispose();
            mdfConn.Close();

            return statusDesc;
        }

        public string getShipStatusFromRecurringAudit(int auditId) {
            string statusDesc = "";
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();

            string selectStmt = "SELECT ss.description FROM ship_statuses ss " +
                "JOIN recurring_audits ra ON ra.ship_status = ss.status_id " +
                "WHERE ra.recurring_audit_id = @auditid";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@auditid", auditId);

            statusDesc = (string)cmd.ExecuteScalar();
            cmd.Dispose();
            mdfConn.Close();

            return statusDesc;
        }

        public string getIntervalFromRecurringAudit(int auditId) {
            string recurInterval = "";
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);
            mdfConn.Open();

            string selectStmt = "SELECT recurring FROM recurring_audits " +
                "WHERE recurring_audit_id = @auditid";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@auditid", auditId);

            recurInterval = (string)cmd.ExecuteScalar();
            cmd.Dispose();
            mdfConn.Close();

            return recurInterval;
        }

        public DataTable getRecurringAuditInfoCollectionById(int recurringid)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select l.location_id, w.description as Workcenter, l.name as Location " +
                                "from workcenter w, locations l, recurring_audits i, locations_in_recurring_audits a " +
                                "where a.recurring_audit_id = i.recurring_audit_id " +
                                "AND l.workcenter_id = w.workcenter_id " +
                                "AND a.location_id = l.location_id " +
                                "AND i.recurring_audit_id = @recurring_id";
                                

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@recurring_id", recurringid);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }

         


        public AuditInfo getRecurringInfoByRecurringId(int recurringid)
        {

            AuditInfo info = new AuditInfo();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select a.description, s.description as status from recurring_audits a, ship_statuses s " +
                                    "where s.status_id = a.ship_status " +
                                    "AND a.recurring_audit_id = @recurring_id";


            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@recurring_id", recurringid);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {

                info.AuditDescription = Convert.ToString(rdr["description"]);
                info.Status = Convert.ToString(rdr["status"]);
                break;

            }
            
           // cmd.Dispose();

            mdfConn.Close();

            return info;

        }


        public DataTable getAuditInfoCollectionById(int auditId)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select l.location_id, w.description as Workcenter, l.name as Location " +
                                "from workcenter w, locations l, inventory_audits i, locations_in_audit a " +
                                "where a.inventory_audit_id = i.inventory_audit_id " +
                                "AND l.workcenter_id = w.workcenter_id " +
                                "AND a.location_id = l.location_id " +
                                "AND i.inventory_audit_id = @auditid " +
                                "AND i.deleted = 0 ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            cmd.Parameters.AddWithValue("@auditid", auditId);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
           
            mdfConn.Close();

            return dt;

        }

        public List<AuditInfo> getAuditInformation()
        {
            List<AuditInfo> auditList = new List<AuditInfo>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select * from inventory_audits where deleted = @flag", mdfConn);
            cmd.Parameters.AddWithValue("@flag", false);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {

                DateTime scheduled = (DateTime) rdr["scheduled_on"];
                string auditDescription = rdr["description"].ToString();
                int auditId = Int32.Parse(rdr["inventory_audit_id"].ToString());
               

                AuditInfo info = new AuditInfo();
                info.ScheduledTime = scheduled;
                info.AuditDescription = auditDescription;
                info.AuditID = auditId;
               

                auditList.Add(info);

            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            return auditList;

        }


        public List<AuditInfo> getRecurringAuditInformation()
        {
            List<AuditInfo> auditList = new List<AuditInfo>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("select * from recurring_audits where deleted = @flag", mdfConn);
            cmd.Parameters.AddWithValue("@flag", false);

            SqlDataReader rdr = cmd.ExecuteReader();



            while (rdr.Read())
            {

                DateTime scheduled = (DateTime)rdr["initially_scheduled_on"];
                string auditDescription = rdr["description"].ToString();
               // int auditId = Int32.Parse(rdr["inventory_audit_id"].ToString());
                string type = rdr["recurring"].ToString();

                AuditInfo info = new AuditInfo();
                info.ScheduledTime = scheduled;
                info.AuditDescription = auditDescription;
               // info.AuditID = auditId;
                info.RecurringType = type;

                auditList.Add(info);

            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            return auditList;

        }


        public DataTable getSMCLCollection()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, nc.usage_category_id, u.category as usage_category," +
           " nc.description, nc.smcc_id, sm.smcc as smcc,"
           + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, " +
           "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
           "nc.remarks, nc.storage_type_id, st.type as storage_type, " +
           "nc.cog_id, nc.spmig, nc.nehc_rpt, nc.catalog_group_id, nc.cog_id, cc.cog as cog," +
           "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id" +

           "  from NIIN_Catalog nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
           + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
           + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
           + "LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
           + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
           + " LEFT JOIN cog_codes cc ON (nc.cog_id=cc.cog_id)" +

           " ";                       

            DataTable dt = new DataTable();

            new SqlDataAdapter(selectStmt, conn).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getNewSMCLEntries()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, nc.usage_category_id, u.category as usage_category, u.description as usage_description," +
           " nc.description, nc.smcc_id, sm.smcc as smcc,"
           + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, " +
           "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
           "nc.remarks, nc.storage_type_id, st.type as storage_type, " +
           "nc.cog_id, nc.spmig, nc.nehc_rpt, nc.catalog_group_id, nc.cog_id, cc.cog as cog," +
           "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id, cna.COSAL, hcc.hcc " +

           "  from NIIN_Catalog nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
           + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
           + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
           + " LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
           + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
           + " LEFT JOIN cog_codes cc ON (nc.cog_id=cc.cog_id)"
           + " LEFT JOIN cosal_niin_allowances cna ON (nc.niin=cna.NIIN)" 
           + " LEFT JOIN msds on (msds.NIIN=cna.NIIN)"
           + " LEFT JOIN hcc on (hcc.hcc_id = msds.hcc_id)"
           + " WHERE msds.manually_entered=1";

            DataTable dt = new DataTable();

            new SqlDataAdapter(selectStmt, conn).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getINSURVAuditNIINs(string surv_name)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.*, u.category as usage_category, u.description as usage_description, i.insurv_id " +

           "  from NIIN_Catalog nc LEFT JOIN insurv_niin i ON (nc.niin_catalog_id=i.niin_catalog_id) " +

           " LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id) "
           +" WHERE i.surv_name=@surv_name";

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@surv_name", surv_name);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmdRead).Fill(dt);

            conn.Close();

            return dt;

        }

        public int getTotalInventoryQty(int niin_catalog_id)
        {

            int count = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "SELECT SUM(qty) from inventory where mfg_catalog_id IN (SELECT mfg_catalog_id from mfg_catalog where niin_catalog_id=@niin_catalog_id)";

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);

            string s = cmdRead.ExecuteScalar().ToString();

            if(!s.Equals(""))
            count = Int32.Parse(s);
            
            conn.Close();

            return count;
        }

        //For HashMaps
        public Dictionary<string, int> getMap(String selectStmt)
        {
            Dictionary<string, int> map = new Dictionary<string, int>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                if (!map.ContainsKey("" + reader.GetValue(0)))
                    map.Add("" + reader.GetValue(0), Int32.Parse(""+reader.GetValue(1)));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return map;

        }

        public Dictionary<string, int> getNIINCatalogMap()
        { 
            string selectStmt = "Select NIIN, niin_catalog_id from NIIN_Catalog";                        
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getUsageCategoryMap()
        {
            string selectStmt = "Select category, usage_category_id from usage_category";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSmccMap()
        {
            string selectStmt = "Select smcc, smcc_id from smcc";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlcMap()
        {
            string selectStmt = "Select slc, shelf_life_code_id from shelf_life_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlacMap()
        {
            string selectStmt = "Select slac, shelf_life_action_code_id from shelf_life_action_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getStorageTypeMap()
        {
            string selectStmt = "Select type, storage_type_id from storage_type";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getCogMap()
        {
            string selectStmt = "Select cog, cog_id from cog_codes";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getCatalogGroupMap()
        {
            string selectStmt = "Select group_name, catalog_group_id from catalog_groups";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSFRniinMap(string sfr_type)
        {
            string selectStmt = "Select n.niin, s.sfr_id from sfr s "+
            " INNER JOIN niin_catalog n ON (n.niin_catalog_id=s.niin_catalog_id) WHERE "
            + " s.action_complete = "
            +"(select MAX(r.action_complete) from sfr r WHERE r.niin_catalog_id=s.niin_catalog_id AND s.sfr_type='" + sfr_type + "')"
            +" AND "
            + " s.sfr_type='"+sfr_type+"' AND completed_flag IS NULL";

            return getMap(selectStmt);
        }

        [CoverageExclude]
        public void insertInventoryReceivingCR(int receiving_id, int qty, int location_id, string username, string shelf_life_expiration_date, string manufacturer_date, int shelf_life_code_id, bool bulk_item, int mfg_catalog_id, int expected_qty,
            string batch_number, string lot_number, string contract_number)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into receiving_cr (" +

            "receiving_id, qty, location_id, action_complete, username, shelf_life_expiration_date, manufacturer_date, bulk_item, mfg_catalog_id, expected_qty, batch_number, lot_number, contract_number"

            + ")"
            + "VALUES(" +
             "@receiving_id, @qty, @location_id, @action_complete, @username, @shelf_life_expiration_date, @manufacturer_date, @bulk_item, @mfg_catalog_id, @expected_qty, @batch_number, @lot_number, @contract_number"

            + ")";

            string invStmt = "insert into inventory (" +

            "shelf_life_expiration_date, manufacturer_date, location_id, qty, serial_number,"
            + "bulk_item, mfg_catalog_id, batch_number, lot_number, contract_number"

            + ")"
            + "VALUES(" +
             "@shelf_life_expiration_date, @manufacturer_date, @location_id, @qty, @serial_number,"
            + "@bulk_item, @mfg_catalog_id, @batch_number, @lot_number, @contract_number"
            + ")";

            SqlCommand invCmd = new SqlCommand(invStmt, conn);
            invCmd.Transaction = tran;

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            int slc_time_in_months = 0;

            SqlCommand selectCmd = new SqlCommand("Select time_in_months from shelf_life_code where shelf_life_code_id=@shelf_life_code_id", conn);
            selectCmd.Transaction = tran;
            selectCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
            object o = selectCmd.ExecuteScalar();
            if (o != null)
            {    
                if(!o.ToString().Equals(""))
                    slc_time_in_months = Int32.Parse(o.ToString());
            }

            //insert receiving cr
            insertCmd.Parameters.AddWithValue("@receiving_id", dbNull(receiving_id));
            insertCmd.Parameters.AddWithValue("@qty", dbNull(qty));
            insertCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            insertCmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            insertCmd.Parameters.AddWithValue("@username", dbNull(username));
            insertCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));           
            insertCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            insertCmd.Parameters.AddWithValue("@expected_qty", dbNull(expected_qty));

            insertCmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            insertCmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            insertCmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));

            if (manufacturer_date != null)
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);
                insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));
                invCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));
                    invCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                {
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                    invCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }

            }
            else
            {

                insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));
                invCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));

                if (shelf_life_expiration_date != null)
                {
                    DateTime slcDate = DateTime.Parse(shelf_life_expiration_date);
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));
                    invCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));
                }
                else
                {
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                    invCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }

            }

            insertCmd.ExecuteNonQuery();
            insertCmd.Parameters.Clear();

            //insert inventory            
            invCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            invCmd.Parameters.AddWithValue("@qty", dbNull(qty));
            invCmd.Parameters.AddWithValue("@serial_number", dbNull(null));
            invCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
            invCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));

            invCmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            invCmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            invCmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));

            invCmd.ExecuteNonQuery();
            invCmd.Parameters.Clear();

            tran.Commit();

            conn.Close();

        }

        public int insertApprovedIncompatibles(int inventory_id1, int inventory_id2, int location_id, string username, string explanation)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into approved_incompatibles (" +

            "inventory_id1, inventory_id2, location_id, username, action_complete, explanation, invalidated_flag"

            + ")"
            + "VALUES(" +
              "@inventory_id1, @inventory_id2, @location_id, @username, @action_complete, @explanation, @invalidated_flag"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@inventory_id1", dbNull(inventory_id1));
            insertCmd.Parameters.AddWithValue("@inventory_id2", dbNull(inventory_id2));
            insertCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            insertCmd.Parameters.AddWithValue("@username", dbNull(username));
            insertCmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            insertCmd.Parameters.AddWithValue("@explanation", dbNull(explanation));
            insertCmd.Parameters.AddWithValue("@invalidated_flag", dbNull(0));

            object o = insertCmd.ExecuteScalar();
            int item_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();

            tran.Commit();

            conn.Close();

            return item_id;

        }

        public int insertGarbageItem(string description)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into garbage_items (" +

            "description"

            + ")"
            + "VALUES(" +
              "@description"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@description", dbNull(description));           

            object o = insertCmd.ExecuteScalar();
            int garbage_item_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();

            tran.Commit();

            conn.Close();

            return garbage_item_id;

        }

        public int insertGarbageOffload(int garbage_item_id, int qty, string ui)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into garbage_offload_list (" +

            "garbage_item_id, qty, ui"

            + ")"
            + "VALUES(" +
              "@garbage_item_id, @qty, @ui"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@garbage_item_id", dbNull(garbage_item_id));
            insertCmd.Parameters.AddWithValue("@qty", dbNull(qty));
            insertCmd.Parameters.AddWithValue("@ui", dbNull(ui)); 

            object o = insertCmd.ExecuteScalar();
            int offload_list_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();


            tran.Commit();

            conn.Close();

            return offload_list_id;

        }

        public int insertShipTo(string uic, string organization, string plate_title, string city, string state, string zip)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into ship_to (" +

            "uic, organization, plate_title, city, state, zip"

            + ")"
            + "VALUES(" +
             "@uic, @organization, @plate_title, @city, @state, @zip"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@uic", dbNull(uic));
            insertCmd.Parameters.AddWithValue("@organization", dbNull(organization));
            insertCmd.Parameters.AddWithValue("@plate_title", dbNull(plate_title));
            insertCmd.Parameters.AddWithValue("@city", dbNull(city));
            insertCmd.Parameters.AddWithValue("@state", dbNull(state));
            insertCmd.Parameters.AddWithValue("@zip", dbNull(zip));
     
            object o = insertCmd.ExecuteScalar();
            int ship_to_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();

            tran.Commit();

            conn.Close();

            return ship_to_id;

        }

        public int insertOffload(int inventory_id, string atm, int updated_qty, string shelf_life_expiration_date, int location_id, string serial_number, bool bulk_item, int mfg_catalog_id, string manufacturer_date, int offload_qty, string alternate_ui, int offload_reason_id, List<VolumeOffloadItem> volumes,
            string batch_number, string lot_number, string contract_number, string alternate_um, string cosal)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into offload_list (" +

            "shelf_life_expiration_date, atm, location_id, serial_number, bulk_item, mfg_catalog_id, manufacturer_date, offload_qty, alternate_ui, offload_reason_id, batch_number, lot_number, contract_number, alternate_um, cosal"

            + ")"
            + "VALUES(" +
              "@shelf_life_expiration_date, @atm, @location_id, @serial_number, @bulk_item, @mfg_catalog_id, @manufacturer_date, @offload_qty, @alternate_ui, @offload_reason_id, @batch_number, @lot_number, @contract_number, @alternate_um, @cosal"

            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
            insertCmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            insertCmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
            insertCmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
            insertCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
            insertCmd.Parameters.AddWithValue("@offload_qty", dbNull(offload_qty));
            insertCmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));
            insertCmd.Parameters.AddWithValue("@offload_reason_id", dbNull(offload_reason_id));

            insertCmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            insertCmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            insertCmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));

            insertCmd.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));
            insertCmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
            insertCmd.Parameters.AddWithValue("@atm", dbNull(atm));

            object o = insertCmd.ExecuteScalar();
            int offload_list_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();


            insertStmt = "insert into volumes_in_offload (offload_list_id, volume_id) VALUES (@offload_list_id, @volume_id)";

            SqlCommand insertCmd2 = new SqlCommand(insertStmt, conn);
            insertCmd2.Transaction = tran;

            foreach (VolumeOffloadItem item in volumes)
            {
                //insert rows
                insertCmd2.Parameters.AddWithValue("@offload_list_id", dbNull(offload_list_id));
                insertCmd2.Parameters.AddWithValue("@volume_id", dbNull(item.volume));

                insertCmd2.ExecuteNonQuery();
                insertCmd2.Parameters.Clear();
            }

           
            string updatetStmt = "update inventory set qty=@qty WHERE inventory_id=@inventory_id";
            
             if(updated_qty==0)
                 updatetStmt = "delete from inventory WHERE inventory_id=@inventory_id";

            SqlCommand updateCmd = new SqlCommand(updatetStmt, conn);
            updateCmd.Transaction = tran;

                //update inventory
            updateCmd.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));
            if (updated_qty != 0)
               updateCmd.Parameters.AddWithValue("@qty", dbNull(updated_qty));

            updateCmd.ExecuteNonQuery();
            updateCmd.Parameters.Clear();
           

            tran.Commit();

            conn.Close();

            return offload_list_id;

        }


        public int insertOffloadCR(string shelf_life_expiration_date, int mfg_catalog_id, string manufacturer_date, int offload_qty, int expected_qty_offloaded, string device_type, int offload_list_id, string username)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into offload_cr (" +

            "shelf_life_expiration_date, mfg_catalog_id, manufacturer_date, qty_offloaded, expected_qty_offloaded, device_type, offload_list_id, action_complete, username"

            + ")"
            + "VALUES(" +
              "@shelf_life_expiration_date, @mfg_catalog_id, @manufacturer_date, @qty_offloaded, @expected_qty_offloaded, @device_type, @offload_list_id, @action_complete, @username"

            + "); SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));            
            insertCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
            insertCmd.Parameters.AddWithValue("@qty_offloaded", dbNull(offload_qty));
            insertCmd.Parameters.AddWithValue("@expected_qty_offloaded", dbNull(expected_qty_offloaded));
            insertCmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
            insertCmd.Parameters.AddWithValue("@offload_list_id", dbNull(offload_list_id));
            insertCmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            insertCmd.Parameters.AddWithValue("@username", dbNull(username));

            object o = insertCmd.ExecuteScalar();
            int offload_cr_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();



            tran.Commit();

            conn.Close();

            return offload_cr_id;
        }



        public int insertGarbageOffloadCR(int offload_qty, int expected_offload_qty, string device_type, int garbage_offload_id, string username)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into garbage_offload_cr (" +

            "offload_qty, expected_offload_qty, device_type, garbage_offload_id, username, action_complete"

            + ")"
            + "VALUES(" +
              "@offload_qty, @expected_offload_qty, @device_type, @garbage_offload_id, @username, @action_complete"

            + "); SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@offload_qty", dbNull(offload_qty));
            insertCmd.Parameters.AddWithValue("@expected_offload_qty", dbNull(expected_offload_qty));
            insertCmd.Parameters.AddWithValue("@device_type", dbNull(device_type));
            insertCmd.Parameters.AddWithValue("@garbage_offload_id", dbFKeyNull(garbage_offload_id));
            insertCmd.Parameters.AddWithValue("@username", dbNull(username));
            insertCmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));

            object o = insertCmd.ExecuteScalar();
            int garbage_offload_cr_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();



            tran.Commit();

            conn.Close();

            return garbage_offload_cr_id;
        }

        [CoverageExclude]
        public int insertReceiving(int mfg_catalog_id, int expected_qty, string manufacturer_date, string shelf_life_expiration_date, int slc_time_in_months,
            string batch_number, string lot_number, string contract_number)
        {
            int receiving_id = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into receiving_list (" +

            "mfg_catalog_id, expected_qty, manufacturer_date, shelf_life_expiration_date, batch_number, lot_number, contract_number"

            + ")"
            + "VALUES(" +
             "@mfg_catalog_id, @expected_qty, @manufacturer_date, @shelf_life_expiration_date, @batch_number, @lot_number, @contract_number"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            insertCmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            insertCmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));

            insertCmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            insertCmd.Parameters.AddWithValue("@expected_qty", dbNull(expected_qty));

            if (manufacturer_date != null)
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);
                insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));

            }
            else
            {

                insertCmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));

                if (shelf_life_expiration_date != null)
                {
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
                }
                else
                    insertCmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));

            }

            object o = insertCmd.ExecuteScalar();
            receiving_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();


            tran.Commit();

            conn.Close();

            return receiving_id;

        }

        //See if the UPDATE sfr request matches the SMCL entry
        public bool compareSMCLUpdatedSFR(SqlConnection conn, SqlTransaction tran, int sfr_id, Dictionary<string, string> smclValueMap)
        {
            bool match = true;

            const string selectstmt = "select * from sfr WHERE sfr_id=@sfr_id";

            SqlCommand cmd = new SqlCommand(selectstmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@sfr_id", sfr_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                foreach (String column_name in smclValueMap.Keys)
                {
                    string sfr_value = rdr[column_name].ToString();
                    string smcl_value = smclValueMap[column_name].ToString();

                    bool equals = false;

                    if (smcl_value.Equals(sfr_value))
                        equals = true;
                    if ((smcl_value == null || smcl_value.Equals("")) && (sfr_value == null || sfr_value.Equals("")))
                        equals = true;

                    if (!equals)
                        match = false;

                }//end for each column to compare

            }//end while read

            rdr.Close();
            cmd.Dispose();

            return match;
        }

        //See if the NIIN to delete has any inventory attached to it
        public bool niinHasInventory(SqlConnection conn, SqlTransaction tran, string niin)
        {
            bool match = false;

            string selectstmt = "select inventory_id from inventory i INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id) WHERE niin=@niin";

            SqlCommand cmd = new SqlCommand(selectstmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@niin", niin);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
               match = true;
            }//end while read

            rdr.Close();
            cmd.Dispose();

            return match;
        }

        //delete deleted niin_catalog smcl entry
        public void deleteNiinCatalogSMCL(SqlConnection conn, SqlTransaction tran, string niin)
        {
           
            string selectstmt = "DELETE FROM niin_catalog WHERE niin=@niin";

            SqlCommand cmd = new SqlCommand(selectstmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@niin", niin);

            SqlDataReader rdr = null;
            try{
                rdr = cmd.ExecuteReader();
            }
            catch
            {
                //In case of error on delete, simply flag as dropped_in_error
                flagNiinCatalogDroppedInError(conn, tran, niin);
            }
            finally
            {

                if(rdr!=null)
                {
                    rdr.Close();
                }
                    cmd.Dispose();
            }

        }

        //delete deleted niin_catalog smcl entry
        public void deleteAllNiinCatalogNotInSMCL(SqlConnection conn, SqlTransaction tran, List<String> droppedNiins)
        {
            try {

                string where = "";

                if (droppedNiins.Count != 0) {
                    int count = 0;
                    where = "AND niin IN ( ";
                    foreach (string niin in droppedNiins) {
                        count++;
                        if (count != droppedNiins.Count) {
                            where += "'" + niin + "', ";
                        }
                        else {
                            where += "'" + niin + "') ";
                        }
                    }

                    //Only delete records where manually_entered IS NULL AND Where there are no outstanding ADD SFRs.
                    // Get the records we want to keep
                    string innerSelect = "SELECT niin_catalog_id from sfr WHERE sfr_type='ADD' and completed_flag IS NULL";
                    string selectstmt = "SELECT niin FROM niin_catalog WHERE manually_entered IS NULL " +
                        "AND niin_catalog_id NOT IN (" + innerSelect + ") " + where;

                    SqlCommand cmd = new SqlCommand(selectstmt, conn);
                    cmd.Transaction = tran;

                    string deletestmt = "DELETE FROM niin_catalog WHERE niin=@niin";

                    SqlCommand cmdDel = new SqlCommand(deletestmt, conn);
                    cmdDel.Transaction = tran;

                    SqlDataReader rdr = cmd.ExecuteReader();

                    List<string> niinToDelete = new List<string>();
                    while (rdr.Read()) {
                        string niin = "";

                        niin = rdr["niin"].ToString();

                        niinToDelete.Add(niin);
                    }//end while

                    if (rdr != null) {
                        rdr.Close();
                    }
                    cmd.Dispose();

                    List<string> niinToFlagDroppedInError = new List<string>();
                    foreach (String niin in niinToDelete) {
                        try {
                            cmdDel.Parameters.AddWithValue("@niin", niin);

                            cmdDel.ExecuteNonQuery();

                        }
                        catch {
                            //In case of error on delete, simply flag as dropped_in_error
                            niinToFlagDroppedInError.Add(niin);
                        }
                        cmdDel.Parameters.Clear();
                    }

                    cmdDel.Dispose();

                    foreach (String niin in niinToFlagDroppedInError) {

                        //In case of error on delete, simply flag as dropped_in_error
                        flagNiinCatalogDroppedInError(conn, tran, niin);

                    }

                }//end if none found
            }
            catch (Exception ex) {
                Debug.WriteLine("ERROR  - deleteAllNiinCatalogNotInSMCL \n\t" + ex.Message);
            }
        }

        //flag niin_catalog dropped_in_error
        public void flagNiinCatalogDroppedInError(SqlConnection conn, SqlTransaction tran, string niin)
        {
           
            string selectstmt = "UPDATE niin_catalog SET dropped_in_error=@dropped_in_error WHERE niin=@niin";

            SqlCommand cmd = new SqlCommand(selectstmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@dropped_in_error", true);
            cmd.Parameters.AddWithValue("@niin", niin);

            SqlDataReader rdr = cmd.ExecuteReader();

            rdr.Close();
            cmd.Dispose();

        }

        private void confirmSMCLUploadFormat(string[] headers)
        {
           
            List<String> missingCols = new List<String>(); 
       
            string[] columns = { "ID", "NIIN", "FSC", "UI", "UM", "USAGE_CATEGORY", 
                                   "ITEM_NAME" , "MILSPEC", "SMCC", "TYPE_OF_STORAGE",
                                   "COG", "SLC", "SLAC", "REMARKS", 
                                   "SPMIG", "TABLE_NAME", "SERIAL_NUMBER", "CAGE", "MANUFACTURER"};

            foreach (String column in columns)
            {
                bool contains = false;
                foreach (String header in headers)
                {
                    if (header.ToUpper().Equals(column))
                        contains = true;
                }
                if (!contains)
                {
                    missingCols.Add(column);
                }
                }

            // See if we have any missing columns
            if (missingCols.Count > 0) {
                String sb = "";
                foreach (String column in missingCols) {
                    sb += column + ", ";
            }

                if (sb.ToString().EndsWith(", "))
                    sb = sb.Substring(0, sb.Length - 2);

                throw new Exception("Excel File is missing the following columns: " + sb);
            
        }
        }

        public bool isGreaterUsageCategoryThan(string uc1, string uc2)
        {
            int id_1 = 0;
            int id_2 = 0;

            if (uc1.Equals("X"))
                id_1 = 1;
            else if (uc1.Equals("X/X"))
                id_1 = 2;
            else if (uc1.Equals("X/L"))
                id_1 = 3;
            else if (uc1.Equals("R/X"))
                id_1 = 4;
            else if (uc1.Equals("L/X"))
                id_1 = 5;
            else if (uc1.Equals("R"))
                id_1 = 6;
            else if (uc1.Equals("R/L"))
                id_1 = 7;
            else if (uc1.Equals("L/R"))
                id_1 = 8;
            else if (uc1.Equals("L"))
                id_1 = 9;
            else if (uc1.Equals("L/L"))
                id_1 = 10;
            else if (uc1.Equals("N/L"))
                id_1 = 11;
            else if (uc1.Equals("N"))
                id_1 = 12;
            else 
                id_1 = 13;

            if (uc2.Equals("X"))
                id_2 = 1;
            else if (uc2.Equals("X/X"))
                id_2 = 2;
            else if (uc1.Equals("X/L"))
                id_2 = 3;
            else if (uc1.Equals("R/X"))
                id_2 = 4;
            else if (uc1.Equals("L/X"))
                id_2 = 5;
            else if (uc1.Equals("R"))
                id_2 = 6;
            else if (uc1.Equals("R/L"))
                id_2 = 7;
            else if (uc1.Equals("L/R"))
                id_2 = 8;
            else if (uc1.Equals("L"))
                id_2 = 9;
            else if (uc1.Equals("L/L"))
                id_2 = 10;
            else if (uc1.Equals("N/L"))
                id_2 = 11;
            else if (uc1.Equals("N"))
                id_2 = 12;
            else 
                id_2 = 13;

            return id_1 < id_2;
        }

        public string updateSMCLFromList(List<string[]> itemList, String fileName)
        {
            string successMessage = "";

            int countGood = 0;
            int countBlank = 0;
            int countDuplicate = 0;

            Dictionary<string, string> niinCageMap = new Dictionary<string, string>();

            // *** Existing ata
            // Get the current niin catalog records (NIIN, niin_catalog_id)

            Dictionary<string, int> niinMap = getNIINCatalogMap();
            // Get the support data records
            Dictionary<string, int> usageCategoryMap = getUsageCategoryMap();
            Dictionary<string, int> smccMap = getSmccMap();
            Dictionary<string, int> slcMap = getSlcMap();
            Dictionary<string, int> slacMap = getSlacMap();
            Dictionary<string, int> storageTypeMap = getStorageTypeMap();
            Dictionary<string, int> cogMap = getCogMap();
            Dictionary<string, int> catalogGroupMap = getCatalogGroupMap();

            //NIIN, SFR_ID - If a niin matches this map, flag the sfr as complete
            Dictionary<string, int> addedSFRMap = getSFRniinMap("ADD");
            //NIIN, SFR_ID - If a niin matches this map, compare the fields of the sfr and the smcl. If they match, flag completed
            Dictionary<string, int> updatedSFRMap = getSFRniinMap("UPDATE");
            //NIIN, SFR_ID - If any niins in this map do not appear in the SMCLMASTER, flag completed.
            Dictionary<string, int> deletedSFRMap = getSFRniinMap("DELETE");
            
            // *** Generated from SMCL
            //List of found niins
            List<string> foundNiins = new List<string>();
            //List of sfr to flag completed
            List<int> completedSFR = new List<int>();

            //HANDLE DUPES - lowest UC id wins
            Dictionary<string, string> dupeNiinUCMap = new Dictionary<string, string>();
            //HANDLE DUPES - highest ID wins
            Dictionary<string, int> dupeNiinSmclIDMap = new Dictionary<string, int>();

            string insertStmt = "insert into NIIN_CATALOG (" +

            "fsc, niin, ui, um, usage_category_id, description, smcc_id, specs," +
            "shelf_life_code_id, shelf_life_action_code_id, remarks," +
            "storage_type_id, cog_id, spmig, nehc_rpt," +
            "catalog_group_id, catalog_serial_number, allowance_qty, ship_id, " +
            "created, manufacturer, cage, msds_num)" +
            "VALUES(" +
             "@fsc, @niin, @ui, @um, @usage_category_id, @description, @smcc_id, @specs," +
            "@shelf_life_code_id, @shelf_life_action_code_id, @remarks," +
            "@storage_type_id, @cog_id, @spmig, @nehc_rpt," +
            "@catalog_group_id, @catalog_serial_number, @allowance_qty, @ship_id, " +
            "@created, @manufacturer, @cage, @msds_num)";

            string updateStmt = "UPDATE NIIN_CATALOG SET " +


            "fsc=@fsc, ui=@ui, um=@um, usage_category_id=@usage_category_id, description=@description, " +
            "smcc_id=@smcc_id, specs=@specs, shelf_life_code_id=@shelf_life_code_id, " +
            "shelf_life_action_code_id=@shelf_life_action_code_id, remarks=@remarks," +
            "storage_type_id=@storage_type_id, cog_id=@cog_id, spmig=@spmig, nehc_rpt=@nehc_rpt," +
            "catalog_group_id=@catalog_group_id, catalog_serial_number=@catalog_serial_number, " +
            "allowance_qty=@allowance_qty, ship_id=@ship_id, created=@created, " +
            "manufacturer=@manufacturer, cage=@cage, msds_num=@msds_num, manually_entered=null " +
            " WHERE niin=@niin";


            //CONFIRM header format before doing anything else
            // confirmSMCLUploadFormat throws an exception that is rolled up if the header fields aren't correct
            string[] headers = itemList[0];
            confirmSMCLUploadFormat(headers); 

            //// Create the database objects
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;
            SqlCommand updateCmd = new SqlCommand(updateStmt, conn);
            updateCmd.Transaction = tran;

            // Start the list processing
            foreach (string[] row in itemList)
            {
                int fsc = 0;
                string niin = "";
                string ui = "";
                string um = "";
                int usage_category_id = 0;
                string description = "";
                int smcc_id = 0;
                string specs = "";
                int shelf_life_code_id = 0;
                int shelf_life_action_code_id = 0;
                string remarks = "";
                int storage_type_id = 0;
                int cog_id = 0;
                string spmig = "";
                string nehc_rpt = "";
                int catalog_group_id = 0;
                string catalog_serial_number = "";
                int allowance_qty = 0;
                int ship_id = 1; //TEMP TODO - this should be carried as a global value

                string manufacturer = "";
                string cage = "";
                string msds_num = "";

                // Added for niin numeric check
                int niinNum = 0;

                int smcl_master_id = 0;

                string usage_category = "";

                bool overwrite = false; //set to true if niin already in smcl

                if (itemList[0] != row)//1st row is headers - also eliminates duplicate records 
                {

                    // Populate the field values from the list

                    for (int i = 0; i < row.Length; i++)
                    {
                        if (headers[i].ToUpper().Equals("FSC"))
                        {
                            try
                            {
                                fsc = Int32.Parse(row[i]);
                            }
                            catch { 
                            }
                        }
                        else if (headers[i].ToUpper().Equals("ID"))
                        {
                            try
                            {
                                smcl_master_id = Int32.Parse(row[i]);
                            }
                            catch
                            {
                            }
                        }
                        else if (headers[i].ToUpper().Equals("NIIN"))
                        {
                            niin = row[i];
                            if (int.TryParse(niin, out niinNum)) {
                                // NIIN is numeric - make sure it is formatted to 9 chars
                                niin = String.Format("{0:000000000}", niinNum);
                            }

                            if (niinMap.ContainsKey(niin))
                                overwrite = true;
                            else
                            {
                                niinMap.Add(niin, 0); //Add, so that duplicates overwrite. The int value doesn't matter.
                            }
                            
                        }
                        else if (headers[i].ToUpper().Equals("UI"))
                        {
                            ui = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("CAGE"))
                        {
                            cage = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("MANUFACTURER"))
                        {
                            manufacturer = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("MSDS_NUM"))
                        {
                            msds_num = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("UM"))
                        {
                            um = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("USAGE_CATEGORY"))
                        {
                            if(usageCategoryMap.ContainsKey(row[i]))
                            usage_category_id = usageCategoryMap[row[i]];

                            usage_category = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("ITEM_NAME"))
                        {
                            description = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("MILSPEC"))
                        {
                            specs = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("SMCC"))
                        {
                            if (smccMap.ContainsKey(row[i]))
                            smcc_id = smccMap[row[i]];
                        }
                        else if (headers[i].ToUpper().Equals("TYPE_OF_STORAGE"))
                        {
                            if (storageTypeMap.ContainsKey(row[i]))
                            storage_type_id = storageTypeMap[row[i]];
                        }
                        else if (headers[i].ToUpper().Equals("COG"))
                        {
                            if (cogMap.ContainsKey(row[i]))
                            cog_id = cogMap[row[i]];
                        }
                        else if (headers[i].ToUpper().Equals("SLC"))
                        {
                            if (slcMap.ContainsKey(row[i]))
                            shelf_life_code_id = slcMap[row[i]];
                        }
                        else if (headers[i].ToUpper().Equals("SLAC"))
                        {
                            if (slacMap.ContainsKey(row[i]))
                            shelf_life_action_code_id = slacMap[row[i]];
                        }
                        else if (headers[i].ToUpper().Equals("REMARKS"))
                        {
                            remarks = row[i];
                        }
                        else if (headers[i].ToUpper().Equals("SPMIG"))
                        {
                            spmig = row[i];
                        }                        
                        else if (headers[i].ToUpper().Equals("TABLE_NAME"))
                        {
                            if (catalogGroupMap.ContainsKey(row[i]))
                                catalog_group_id = catalogGroupMap[row[i]];
                        }
                        else if (headers[i].ToUpper().Equals("SERIAL_NUMBER"))
                        {
                            catalog_serial_number = row[i];
                        }

                    }//end fields


                    //Here check against a hashmap. Is this the lastest version of this niin?
                    bool isDupeToIgnore = false;
                    if (dupeNiinUCMap.ContainsKey(niin))
                    {
                        //Compare Usage Categories
                        string oldUc = dupeNiinUCMap[niin];
                        string currUc = usage_category;

                        if (isGreaterUsageCategoryThan(oldUc, currUc))
                            isDupeToIgnore = true;
                        else if (oldUc.Equals(currUc))
                        {
                            //equal. Compare SMCL ID
                            if (dupeNiinSmclIDMap.ContainsKey(niin))
                            {
                                int oldID = dupeNiinSmclIDMap[niin];
                                int currID = smcl_master_id;

                                if (oldID > currID)
                                    isDupeToIgnore = true;
                                else
                                    dupeNiinSmclIDMap[niin] = currID;
                            }//end compare SMCL ID
                           
                        }
                        else
                        {
                            dupeNiinUCMap[niin] = currUc;
                        }
                    }
                    else if(niin!=null && !niin.Equals(""))
                    {
                        dupeNiinUCMap.Add(niin, usage_category);
                        dupeNiinSmclIDMap.Add(niin, smcl_master_id);
                    } //End compare Usage Categories.


                    //This block just keeps counts of good/blank/duplicate records
                    try 
                    {
                        if (niin != null && !niin.Equals(""))
                        {
                            niinCageMap.Add(niin + cage, niin + cage);
                            countGood++;
                        }
                        else
                            countBlank++;
                    }
                    catch
                    {
                        countDuplicate++;
                    }//end counts


                    // Process if this isn't a dupe
                    if (!isDupeToIgnore)
                    {
                        // UPDATE if overwrite
                        if (overwrite)
                        {
                            //update rows
                            updateCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                            updateCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                            updateCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                            updateCmd.Parameters.AddWithValue("@um", dbNull(um));
                            updateCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                            updateCmd.Parameters.AddWithValue("@description", dbNull(description));
                            updateCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                            updateCmd.Parameters.AddWithValue("@specs", dbNull(specs));

                            updateCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                            updateCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                            updateCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));

                            updateCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                            updateCmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
                            updateCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                            updateCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));

                            updateCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                            updateCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                            updateCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                            updateCmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));
                            updateCmd.Parameters.AddWithValue("@created", dbNull(DateTime.Now));

                            updateCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                            updateCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                            updateCmd.Parameters.AddWithValue("@msds_num", dbNull(msds_num));

                            try
                            {
                                updateCmd.ExecuteNonQuery();
                                updateCmd.Parameters.Clear();
                                ////end update rows
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error Updating NIIN(" + niin + "), DESC(" + description + "), MFG(" + manufacturer + "). "
                                    + ex.Message);
                            }

                        }
                        else //NEW NIIN, INSERT
                        {
                            //insert rows
                            insertCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                            insertCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                            insertCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                            insertCmd.Parameters.AddWithValue("@um", dbNull(um));
                            insertCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                            insertCmd.Parameters.AddWithValue("@description", dbNull(description));
                            insertCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                            insertCmd.Parameters.AddWithValue("@specs", dbNull(specs));

                            insertCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                            insertCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                            insertCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));

                            insertCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                            insertCmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
                            insertCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                            insertCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));

                            insertCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                            insertCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                            insertCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                            insertCmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));
                            insertCmd.Parameters.AddWithValue("@created", dbNull(DateTime.Now));

                            insertCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                            insertCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                            insertCmd.Parameters.AddWithValue("@msds_num", dbNull(msds_num));

                            try
                            {
                                if (niin != null && !niin.Equals(""))
                                    insertCmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error Inserting NIIN(" + niin + "), DESC(" + description + "), MFG(" + manufacturer + "). "
                                  + ex.Message);
                            }
                            insertCmd.Parameters.Clear();
                            //end insert rows
                        }

                        //SFR CHECK
                        if (addedSFRMap.ContainsKey(niin))
                        {
                            completedSFR.Add(addedSFRMap[niin]);
                        }
                        if (updatedSFRMap.ContainsKey(niin))
                        {
                            //COMPARE FIELDS FIRST
                            Dictionary<string, string> smclValueMap = new Dictionary<string, string>();
                            smclValueMap.Add("fsc", fsc.ToString());
                            smclValueMap.Add("ui", ui);
                            smclValueMap.Add("um", um);
                            smclValueMap.Add("usage_category_id", usage_category_id.ToString());
                            smclValueMap.Add("description", description);
                            smclValueMap.Add("smcc_id", smcc_id.ToString());
                            smclValueMap.Add("specs", specs);
                            smclValueMap.Add("shelf_life_code_id", shelf_life_code_id.ToString());
                            smclValueMap.Add("shelf_life_action_code_id", shelf_life_action_code_id.ToString());
                            smclValueMap.Add("remarks", remarks);
                           //smclValueMap.Add("storage_type_id", storage_type_id.ToString());
                            smclValueMap.Add("cog_id", cog_id.ToString());
                            smclValueMap.Add("spmig", spmig);
                            smclValueMap.Add("nehc_rpt", nehc_rpt);
                            smclValueMap.Add("catalog_group_id", catalog_group_id.ToString());
                            smclValueMap.Add("catalog_serial_number", catalog_serial_number);
                            //smclValueMap.Add("manufacturer", manufacturer);
                            //smclValueMap.Add("cage", cage);
                            //smclValueMap.Add("msds_num", msds_num);

                            int sfr_id = updatedSFRMap[niin];
                            bool match = false;
                            try
                            {
                                match = compareSMCLUpdatedSFR(conn, tran, sfr_id, smclValueMap);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error Occurred Comparing NIIN(" + niin + "), DESC(" + description + "), MFG(" + manufacturer + ") to existing UPDATE SFR requests. "
                                    + ex.Message);
                            }

                            if (match)
                            {
                                completedSFR.Add(updatedSFRMap[niin]);
                            }//end if
                        }

                        //Found Niin
                        if (niin != null && !niin.Equals(""))
                        {
                            foundNiins.Add(niin);
                        }

                    }//End isDupeCheck
                }//end row
            }

            //Figure deleted sfr's
            foreach (String niin in deletedSFRMap.Keys)
            {
                if (!foundNiins.Contains(niin))
                {                   
                    try
                    {
                    //DOES NIIN HAVE INVENTORY?
                    bool hasInventory = niinHasInventory(conn, tran, niin);

                    if (!hasInventory)
                    {
                        deleteNiinCatalogSMCL(conn, tran, niin);
                    }
                    else
                    {
                        flagNiinCatalogDroppedInError(conn, tran, niin);
                    }

                        completedSFR.Add(deletedSFRMap[niin]);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error Occurred Deleting NIIN(" + niin + ") for existing DELETE SFR request. "
                            + ex.Message);
                    }
                }
            }

            //UPDATE ANY COMPLETED SFR
            String sfrStmt = "UPDATE SFR SET completed_flag=1 WHERE sfr_id=@sfr_id";
            SqlCommand sfrCmd = new SqlCommand(sfrStmt, conn);
            sfrCmd.Transaction = tran;
            foreach (int sfr_id in completedSFR)
            {
                sfrCmd.Parameters.AddWithValue("@sfr_id", dbNull(sfr_id));
                sfrCmd.ExecuteNonQuery();
                sfrCmd.Parameters.Clear();
            }

            //Delete all other niins from the system that were not in this SMCL update.
            try
            {
                // Go through the ninMap and remove any foundNiins
                List<string> droppedNiins = new List<string>(niinMap.Keys);

                foreach (String niin in foundNiins) {
                    if (niinMap.ContainsKey(niin)) {
                        // Add the niin to the droppedNiins list
                        droppedNiins.Remove(niin);
                    }
                }

                deleteAllNiinCatalogNotInSMCL(conn, tran, droppedNiins);
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occurred Deleting NIINs that did not exist in SMCL update file. "
                    + ex.Message);
            }

            tran.Commit();

            conn.Close();

            // Add the record to the uploads table
            updateFileTable(DatabaseManager.FILE_TABLES.FileUpload,
                fileName, DatabaseManager.FILE_TYPES.SMCL.ToString(),
                DateTime.Now, "", countGood, countBlank, countDuplicate);

            successMessage = countGood + " SMCL records Inserted/Updated.\n"
                + countBlank + " blank and " + countDuplicate + " duplicate records ignored.\n" 
                + completedSFR.Count + " SFR records were marked completed. ";

            return successMessage;
            
        }//end update SMCL

        public int insertInventory(string manufacturer_date, string shelf_life_expiration_date, int shelf_life_code_id, int location_id, int qty, string serial_number,
            bool bulk_item, int mfg_catalog_id, string batch_number, string lot_number, string contract_number, string cosal, string atm)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //calculate shelf_life_expiration_date from manufacturer_date and SLC
            int slc_time_in_months = 0;
            if (shelf_life_code_id != 0)
            {
                SqlCommand selectCmd = new SqlCommand("Select time_in_months from shelf_life_code where shelf_life_code_id=@shelf_life_code_id", conn);
                selectCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                object o = selectCmd.ExecuteScalar();
                if (o != null)
                {
                    if (!o.ToString().Equals(""))
                        slc_time_in_months = Int32.Parse(o.ToString());
                }
            }

            string insertStmt = "insert into inventory (" +
            
            "shelf_life_expiration_date, manufacturer_date,  location_id, qty, serial_number,"
            +"bulk_item, mfg_catalog_id, batch_number, lot_number, contract_number, cosal, atm"
            
            +")"
            + "VALUES("+
             "@shelf_life_expiration_date, @manufacturer_date,  @location_id, @qty, @serial_number,"
            + "@bulk_item, @mfg_catalog_id, @batch_number, @lot_number, @contract_number, @cosal, @atm"
            + "); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            if (manufacturer_date != null && !manufacturer_date.Equals(""))
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }

            }
            else
            {

                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));

                if (shelf_life_expiration_date != null && !shelf_life_expiration_date.Equals(""))
                {
                    DateTime slcDate = DateTime.Parse(shelf_life_expiration_date);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));
                   
                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                   
                }

            }
            cmd.Parameters.AddWithValue("@location_id", dbFKeyNull(location_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(qty));
            cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
            cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbFKeyNull(mfg_catalog_id));
            cmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            cmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            cmd.Parameters.AddWithValue("@contract_number", dbNull(truncate(50, contract_number)));
            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
            cmd.Parameters.AddWithValue("@atm", dbNull(atm));

            object i = cmd.ExecuteScalar();
            int inventory_id = Convert.ToInt32(i);
            cmd.Parameters.Clear();

            conn.Close();

            return inventory_id;
        }//end

        private string truncate(int maxlength, string value)
        {

            if (value != null && value.Length > maxlength)
            {

                value = value.Substring(0, maxlength-1);

            }

            return value;
        }
 
        public void insertMSDS(MsdsRecord msds, bool manually_entered)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
             conn.Open(); 
            SqlTransaction tran = conn.BeginTransaction();


            string stmtMSDS = "INSERT INTO msds (file_name, manually_entered, " + 
                "ARTICLE_IND, CAGE, MANUFACTURER, DESCRIPTION, EMERGENCY_TEL, END_COMP_IND, " +
                "END_ITEM_IND, FSC, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, " +
                "NIIN, PARTNO, PRODUCT_IDENTITY, PRODUCT_IND, PRODUCT_LANGUAGE, PRODUCT_LOAD_DATE, " +
                "PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, MSDSSERNO, MSDS_FILE," +
                "hcc_id, PROPRIETARY_IND, PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, " +
                "RADIOACTIVE_IND, SERVICE_AGENCY, TRADE_NAME, TRADE_SECRET_IND, created) " +
                "VALUES(@file_name, @manually_entered, @ARTICLE_IND, @CAGE, @MANUFACTURER, @DESCRIPTION, @EMERGENCY_TEL, @END_COMP_IND, " +
                "@END_ITEM_IND, @FSC, @KIT_IND, @KIT_PART_IND, @MANUFACTURER_MSDS_NO, @MIXTURE_IND, " +
                "@NIIN, @PARTNO, @PRODUCT_IDENTITY, @PRODUCT_IND, @PRODUCT_LANGUAGE, @PRODUCT_LOAD_DATE, " +
                "@PRODUCT_RECORD_STATUS, @PRODUCT_REVISION_NO, @MSDSSERNO, @MSDS_FILE," +
                "@hcc_id, @PROPRIETARY_IND, @PUBLISHED_IND, @PURCHASED_PROD_IND, @PURE_IND, " +
                "@RADIOACTIVE_IND, @SERVICE_AGENCY, @TRADE_NAME, @TRADE_SECRET_IND, getDate()); SELECT SCOPE_IDENTITY() ";


            string stmtPhysChemical = "INSERT INTO msds_phys_chemical (msds_id, VAPOR_PRESS, " +
                "VAPOR_DENS, SPECIFIC_GRAV, VOC_POUNDS_GALLON, VOC_GRAMS_LITER, " +
                "PH, VISCOSITY, EVAP_RATE_REF, SOL_IN_WATER, APP_ODOR, PERCENT_VOL_VOLUME, " +
                "AUTOIGNITION_TEMP, CARCINOGEN_IND, EPA_ACUTE, EPA_CHRONIC, EPA_FIRE, " +
                "EPA_PRESSURE, EPA_REACTIVITY, FLASH_PT_TEMP, NEUT_AGENT, " +
                "NFPA_FLAMMABILITY, NFPA_HEALTH, NFPA_REACTIVITY, NFPA_SPECIAL, " +
                "OSHA_CARCINOGENS, OSHA_COMB_LIQUID, OSHA_COMP_GAS, OSHA_CORROSIVE, " +
                "OSHA_EXPLOSIVE, OSHA_FLAMMABLE, OSHA_HIGH_TOXIC, OSHA_IRRITANT, OSHA_ORG_PEROX, " +
                "OSHA_OTHERLONGTERM, OSHA_OXIDIZER, OSHA_PYRO, OSHA_SENSITIZER, OSHA_TOXIC, " +
                "OSHA_UNST_REACT, OTHER_SHORT_TERM, PHYS_STATE_CODE, VOL_ORG_COMP_WT, " +
                "OSHA_WATER_REACTIVE) " +
                "VALUES (@msds_id, @VAPOR_PRESS, @VAPOR_DENS, @SPECIFIC_GRAV, @VOC_POUNDS_GALLON, @VOC_GRAMS_LITER, " +
                "@PH, @VISCOSITY, @EVAP_RATE_REF, @SOL_IN_WATER, @APP_ODOR, @PERCENT_VOL_VOLUME, @AUTOIGNITION_TEMP, @CARCINOGEN_IND, @EPA_ACUTE, " +
                "@EPA_CHRONIC, @EPA_FIRE, @EPA_PRESSURE, @EPA_REACTIVITY, @FLASH_PT_TEMP, @NEUT_AGENT, @NFPA_FLAMMABILITY, @NFPA_HEALTH, @NFPA_REACTIVITY, @NFPA_SPECIAL, " +
                "@OSHA_CARCINOGENS, @OSHA_COMB_LIQUID, @OSHA_COMP_GAS, @OSHA_CORROSIVE, @OSHA_EXPLOSIVE, @OSHA_FLAMMABLE, @OSHA_HIGH_TOXIC, @OSHA_IRRITANT, @OSHA_ORG_PEROX, " +
                "@OSHA_OTHERLONGTERM, @OSHA_OXIDIZER, @OSHA_PYRO, @OSHA_SENSITIZER, @OSHA_TOXIC, @OSHA_UNST_REACT, @OTHER_SHORT_TERM, @PHYS_STATE_CODE, @VOL_ORG_COMP_WT, " +
                "@OSHA_WATER_REACTIVE); ";


            string stmtIngredients = "INSERT INTO msds_ingredients (msds_id, CAS, RTECS_NUM, RTECS_CODE, INGREDIENT_NAME, PRCNT, OSHA_PEL, OSHA_STEL, ACGIH_TLV, " +
                                                "ACGIH_STEL, EPA_REPORT_QTY, DOT_REPORT_QTY, PRCNT_VOL_VALUE, PRCNT_VOL_WEIGHT, CHEM_MFG_COMP_NAME, ODS_IND, OTHER_REC_LIMITS) " +
                                                "VALUES (@msds_id, @CAS, @RTECS_NUM, @RTECS_CODE, @INGREDIENT_NAME, @PRCNT, @OSHA_PEL, @OSHA_STEL, @ACGIH_TLV, " +
                                                "@ACGIH_STEL, @EPA_REPORT_QTY, @DOT_REPORT_QTY, @PRCNT_VOL_VALUE, @PRCNT_VOL_WEIGHT, @CHEM_MFG_COMP_NAME, @ODS_IND, @OTHER_REC_LIMITS); ";



            string msds_Contractor_Info_InsertCmd = "INSERT INTO msds_contractor_info (CT_NUMBER, CT_CAGE, CT_CITY, CT_COMPANY_NAME, CT_COUNTRY, CT_PO_BOX, CT_PHONE, " +
                                                    "PURCHASE_ORDER_NO, CT_STATE, msds_id) " +
                                                    "VALUES (@CT_NUMBER, @CT_CAGE, @CT_CITY, @CT_COMPANY_NAME, @CT_COUNTRY, @CT_PO_BOX, @CT_PHONE, " +
                                                    "@PURCHASE_ORDER_NO, @CT_STATE, @msds_id); ";


            string msds_Radiological_Info_InsertCmd = "INSERT INTO msds_radiological_info (NRC_LP_NUM, OPERATOR, RAD_AMOUNT_MICRO, RAD_FORM, RAD_CAS, RAD_NAME, RAD_SYMBOL, REP_NSN, SEALED, msds_id) " +
                                                                 "VALUES (@NRC_LP_NUM, @OPERATOR, @RAD_AMOUNT_MICRO, @RAD_FORM, @RAD_CAS, @RAD_NAME, @RAD_SYMBOL, @REP_NSN, @SEALED, @msds_id); ";


            string msds_Transportation_InsertCmd = "INSERT INTO msds_transportation (AF_MMAC_CODE, CERTIFICATE_COE, COMPETENT_CAA, DOD_ID_CODE, DOT_EXEMPTION_NO, DOT_RQ_IND, EX_NO, FLASH_PT_TEMP, " +
                                                   "HIGH_EXPLOSIVE_WT, LTD_QTY_IND, MAGNETIC_IND, MAGNETISM, MARINE_POLLUTANT_IND, NET_EXP_QTY_DIST, NET_EXP_WEIGHT, NET_PROPELLANT_WT, " +
                                                   "NOS_TECHNICAL_SHIPPING_NAME, TRANSPORTATION_ADDITIONAL_DATA, msds_id) " +
                                                   "VALUES (@AF_MMAC_CODE, @CERTIFICATE_COE, @COMPETENT_CAA, @DOD_ID_CODE, @DOT_EXEMPTION_NO, @DOT_RQ_IND, @EX_NO, @FLASH_PT_TEMP, " +
                                                   "@HIGH_EXPLOSIVE_WT, @LTD_QTY_IND, @MAGNETIC_IND, @MAGNETISM, @MARINE_POLLUTANT_IND, @NET_EXP_QTY_DIST, @NET_EXP_WEIGHT, @NET_PROPELLANT_WT, " +
                                                   "@NOS_TECHNICAL_SHIPPING_NAME, @TRANSPORTATION_ADDITIONAL_DATA, @msds_id); ";

            string msds_DOT_PSN_InsertCmd = "INSERT INTO msds_dot_psn (DOT_HAZARD_CLASS_DIV, DOT_HAZARD_LABEL, DOT_MAX_CARGO, DOT_MAX_PASSENGER, DOT_PACK_BULK, DOT_PACK_EXCEPTIONS, " +
                                            "DOT_PACK_NONBULK, DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER, DOT_PSN_CODE, DOT_SPECIAL_PROVISION, DOT_SYMBOLS, DOT_UN_ID_NUMBER, " +
                                            "DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW, DOT_PACK_GROUP, msds_id) " +
                                            "VALUES (@DOT_HAZARD_CLASS_DIV, @DOT_HAZARD_LABEL, @DOT_MAX_CARGO, @DOT_MAX_PASSENGER, @DOT_PACK_BULK, @DOT_PACK_EXCEPTIONS, " +
                                            "@DOT_PACK_NONBULK, @DOT_PROP_SHIP_NAME, @DOT_PROP_SHIP_MODIFIER, @DOT_PSN_CODE, @DOT_SPECIAL_PROVISION, @DOT_SYMBOLS, @DOT_UN_ID_NUMBER, " +
                                            "@DOT_WATER_OTHER_REQ, @DOT_WATER_VESSEL_STOW, @DOT_PACK_GROUP, @msds_id); ";


            string msds_AFJM_PSN_InsertCmd = "INSERT INTO msds_afjm_psn (AFJM_HAZARD_CLASS, AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP, AFJM_PROP_SHIP_NAME, AFJM_PROP_SHIP_MODIFIER, " +
                                            "AFJM_PSN_CODE, AFJM_SPECIAL_PROV, AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS, AFJM_UN_ID_NUMBER, msds_id) " +
                                            "VALUES (@AFJM_HAZARD_CLASS, @AFJM_PACK_PARAGRAPH, @AFJM_PACK_GROUP, @AFJM_PROP_SHIP_NAME, @AFJM_PROP_SHIP_MODIFIER, " +
                                            "@AFJM_PSN_CODE, @AFJM_SPECIAL_PROV, @AFJM_SUBSIDIARY_RISK, @AFJM_SYMBOLS, @AFJM_UN_ID_NUMBER, @msds_id); ";


            string msds_IATA_PSN_InsertCmd = "INSERT INTO msds_iata_psn (IATA_CARGO_PACKING, IATA_HAZARD_CLASS, IATA_HAZARD_LABEL, IATA_PACK_GROUP, IATA_PASS_AIR_PACK_LMT_INSTR, " +
                                            "IATA_PASS_AIR_PACK_LMT_PER_PKG, IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME, IATA_PROP_SHIP_MODIFIER, IATA_CARGO_PACK_MAX_QTY, " +
                                            "IATA_PSN_CODE, IATA_PASS_AIR_MAX_QTY, IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK, IATA_UN_ID_NUMBER, msds_id) " +
                                            "VALUES (@IATA_CARGO_PACKING, @IATA_HAZARD_CLASS, @IATA_HAZARD_LABEL, @IATA_PACK_GROUP, @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                                            "@IATA_PASS_AIR_PACK_LMT_PER_PKG, @IATA_PASS_AIR_PACK_NOTE, @IATA_PROP_SHIP_NAME, @IATA_PROP_SHIP_MODIFIER, @IATA_CARGO_PACK_MAX_QTY, " +
                                            "@IATA_PSN_CODE, @IATA_PASS_AIR_MAX_QTY, @IATA_SPECIAL_PROV, @IATA_SUBSIDIARY_RISK, @IATA_UN_ID_NUMBER, @msds_id); ";


            string msds_IMO_PSN_InsertCmd = "INSERT INTO msds_imo_psn (IMO_EMS_NO, IMO_HAZARD_CLASS, IMO_IBC_INSTR, IMO_LIMITED_QTY, IMO_PACK_GROUP, IMO_PACK_INSTRUCTIONS, IMO_PACK_PROVISIONS, " +
                                           "IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER, IMO_PSN_CODE, IMO_SPECIAL_PROV, IMO_STOW_SEGR, IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO, IMO_TANK_INSTR_PROV, " +
                                           "IMO_TANK_INSTR_UN, IMO_UN_NUMBER, IMO_IBC_PROVISIONS, msds_id) " +
                                           "VALUES (@IMO_EMS_NO, @IMO_HAZARD_CLASS, @IMO_IBC_INSTR, @IMO_LIMITED_QTY, @IMO_PACK_GROUP, @IMO_PACK_INSTRUCTIONS, @IMO_PACK_PROVISIONS, " +
                                           "@IMO_PROP_SHIP_NAME, @IMO_PROP_SHIP_MODIFIER, @IMO_PSN_CODE, @IMO_SPECIAL_PROV, @IMO_STOW_SEGR, @IMO_SUBSIDIARY_RISK, @IMO_TANK_INSTR_IMO, @IMO_TANK_INSTR_PROV, " +
                                           "@IMO_TANK_INSTR_UN, @IMO_UN_NUMBER, @IMO_IBC_PROVISIONS, @msds_id); ";




            string stmtItemDescription  = "INSERT INTO msds_item_description (msds_id, ITEM_MANAGER, ITEM_NAME, SPECIFICATION_NUMBER, TYPE_GRADE_CLASS, UNIT_OF_ISSUE, " +
                                                     "QUANTITATIVE_EXPRESSION, UI_CONTAINER_QTY, TYPE_OF_CONTAINER, BATCH_NUMBER, LOT_NUMBER, LOG_FLIS_NIIN_VER, LOG_FSC, NET_UNIT_WEIGHT, " +
                                                     "SHELF_LIFE_CODE, SPECIAL_EMP_CODE, UN_NA_NUMBER, UPC_GTIN) " +
                                                     "VALUES (@msds_id, @ITEM_MANAGER, @ITEM_NAME, @SPECIFICATION_NUMBER, @TYPE_GRADE_CLASS, @UNIT_OF_ISSUE, " +
                                                     "@QUANTITATIVE_EXPRESSION, @UI_CONTAINER_QTY, @TYPE_OF_CONTAINER, @BATCH_NUMBER, @LOT_NUMBER, @LOG_FLIS_NIIN_VER, @LOG_FSC, @NET_UNIT_WEIGHT, " +
                                                     "@SHELF_LIFE_CODE, @SPECIAL_EMP_CODE, @UN_NA_NUMBER, @UPC_GTIN); ";


            string msds_Label_Info_InsertCmd = "INSERT INTO msds_label_info (COMPANY_CAGE_RP, COMPANY_NAME_RP, LABEL_EMERG_PHONE, LABEL_ITEM_NAME, LABEL_PROC_YEAR, LABEL_PROD_IDENT, " +
                                           "LABEL_PROD_SERIALNO, LABEL_SIGNAL_WORD, LABEL_STOCK_NO, SPECIFIC_HAZARDS, msds_id) " +
                                           "VALUES (@COMPANY_CAGE_RP, @COMPANY_NAME_RP, @LABEL_EMERG_PHONE, @LABEL_ITEM_NAME, @LABEL_PROC_YEAR, @LABEL_PROD_IDENT, " +
                                           "@LABEL_PROD_SERIALNO, @LABEL_SIGNAL_WORD, @LABEL_STOCK_NO, @SPECIFIC_HAZARDS, @msds_id); ";

            string msds_Disposal_InsertCmd = "INSERT INTO msds_disposal (DISPOSAL_ADD_INFO, EPA_HAZ_WASTE_CODE, EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME, msds_id) " +
                                             "VALUES (@DISPOSAL_ADD_INFO, @EPA_HAZ_WASTE_CODE, @EPA_HAZ_WASTE_IND, @EPA_HAZ_WASTE_NAME, @msds_id); ";

            string msds_Document_Types_InsertCmd = "INSERT INTO msds_document_types (MANUFACTURER_LABEL, manufacturer_label_filename, MSDS_TRANSLATED, NESHAP_COMP_CERT, OTHER_DOCS, PRODUCT_SHEET, " +
                                                    "TRANSPORTATION_CERT, msds_translated_filename, neshap_comp_filename, other_docs_filename, product_sheet_filename, " +
                                                    "transportation_cert_filename, msds_id) " +
                                                    "VALUES (@MANUFACTURER_LABEL, @manufacturer_label_filename, @MSDS_TRANSLATED, @NESHAP_COMP_CERT, @OTHER_DOCS, @PRODUCT_SHEET, " +
                                                    "@TRANSPORTATION_CERT, @msds_translated_filename, @neshap_comp_filename, @other_docs_filename, @product_sheet_filename, " +
                                                    "@transportation_cert_filename, @msds_id); ";



            SqlCommand cmd_msds = new SqlCommand(stmtMSDS, conn);
            cmd_msds.Transaction = tran;
            
            SqlCommand cmd_ing = new SqlCommand(stmtIngredients, conn);
            cmd_ing.Transaction = tran;
            SqlCommand cmd_desc = new SqlCommand(stmtItemDescription, conn);
            cmd_desc.Transaction = tran;
            SqlCommand cmd_phys = new SqlCommand(stmtPhysChemical, conn);
            cmd_phys.Transaction = tran;
            SqlCommand cmd_contract = new SqlCommand(msds_Contractor_Info_InsertCmd, conn);
            cmd_contract.Transaction = tran;
            SqlCommand cmd_radio = new SqlCommand(msds_Radiological_Info_InsertCmd, conn);
            cmd_radio.Transaction = tran;
            SqlCommand cmd_transport = new SqlCommand(msds_Transportation_InsertCmd, conn);
            cmd_transport.Transaction = tran;
            SqlCommand cmd_dot = new SqlCommand(msds_DOT_PSN_InsertCmd, conn);
            cmd_dot.Transaction = tran;
            SqlCommand cmd_afjm = new SqlCommand(msds_AFJM_PSN_InsertCmd, conn);
            cmd_afjm.Transaction = tran;
            SqlCommand cmd_iata = new SqlCommand(msds_IATA_PSN_InsertCmd, conn);
            cmd_iata.Transaction = tran;
            SqlCommand cmd_imo = new SqlCommand(msds_IMO_PSN_InsertCmd, conn);
            cmd_imo.Transaction = tran;
            SqlCommand cmd_label = new SqlCommand(msds_Label_Info_InsertCmd, conn);
            cmd_label.Transaction = tran;
            SqlCommand cmd_disposal = new SqlCommand(msds_Disposal_InsertCmd, conn);
            cmd_disposal.Transaction = tran;
            SqlCommand cmd_docs = new SqlCommand(msds_Document_Types_InsertCmd, conn);
            cmd_docs.Transaction = tran;

            int msds_id = 0;

            //insert msds
            cmd_msds.Parameters.AddWithValue("@MSDSSERNO", dbNull(msds.MSDSSERNO));
            cmd_msds.Parameters.AddWithValue("@CAGE", dbNull(msds.CAGE));            
            cmd_msds.Parameters.AddWithValue("@PARTNO", dbNull(msds.PARTNO));
            cmd_msds.Parameters.AddWithValue("@FSC", dbNull(msds.FSC));
            cmd_msds.Parameters.AddWithValue("@NIIN", dbNull(msds.NIIN));
            cmd_msds.Parameters.AddWithValue("@hcc_id", dbNull(msds.hcc_id));
            cmd_msds.Parameters.AddWithValue("@MANUFACTURER", dbNull(msds.MANUFACTURER));

            if (msds.msds_file != null)
                cmd_msds.Parameters.AddWithValue("@msds_file", dbNull(msds.msds_file));
            else
            {

                SqlParameter p = new SqlParameter("@msds_file", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_msds.Parameters.Add(p);
            }
           
            cmd_msds.Parameters.AddWithValue("@file_name", dbNull(msds.file_name));
            cmd_msds.Parameters.AddWithValue("@manually_entered", dbNull(manually_entered));

            cmd_msds.Parameters.AddWithValue("@ARTICLE_IND", dbNull(msds.ARTICLE_IND));
            cmd_msds.Parameters.AddWithValue("@DESCRIPTION", dbNull(msds.DESCRIPTION));
            cmd_msds.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(msds.EMERGENCY_TEL));
            cmd_msds.Parameters.AddWithValue("@END_COMP_IND", dbNull(msds.END_COMP_IND));
            cmd_msds.Parameters.AddWithValue("@END_ITEM_IND", dbNull(msds.END_ITEM_IND));
            cmd_msds.Parameters.AddWithValue("@KIT_IND", dbNull(msds.KIT_IND));
            cmd_msds.Parameters.AddWithValue("@KIT_PART_IND", dbNull(msds.KIT_PART_IND));
            cmd_msds.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(msds.MANUFACTURER_MSDS_NO));
            cmd_msds.Parameters.AddWithValue("@MIXTURE_IND", dbNull(msds.MIXTURE_IND));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(msds.PRODUCT_IDENTITY));            
            cmd_msds.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(msds.PRODUCT_LOAD_DATE));            
            cmd_msds.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(msds.PRODUCT_RECORD_STATUS));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(msds.PRODUCT_REVISION_NO));
            cmd_msds.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(msds.PROPRIETARY_IND));
            cmd_msds.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(msds.PUBLISHED_IND));
            cmd_msds.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(msds.PURCHASED_PROD_IND));
            cmd_msds.Parameters.AddWithValue("@PURE_IND", dbNull(msds.PURE_IND));
            cmd_msds.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(msds.RADIOACTIVE_IND));
            cmd_msds.Parameters.AddWithValue("@SERVICE_AGENCY", dbNull(msds.SERVICE_AGENCY));
            cmd_msds.Parameters.AddWithValue("@TRADE_NAME", dbNull(msds.TRADE_NAME));
            cmd_msds.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(msds.TRADE_SECRET_IND));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_IND", dbNull(msds.PRODUCT_IND));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(msds.PRODUCT_LANGUAGE));

            try {
                object o = cmd_msds.ExecuteScalar();
                msds_id = Convert.ToInt32(o);
                cmd_msds.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS \n\t" + ex.Message);  
            }


            //insert phys chemical            
            cmd_phys.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(msds.VAPOR_PRESS));
            cmd_phys.Parameters.AddWithValue("@VAPOR_DENS", dbNull(msds.VAPOR_DENS));
            cmd_phys.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(msds.SPECIFIC_GRAV));
            cmd_phys.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(msds.VOC_POUNDS_GALLON));
            cmd_phys.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(msds.VOC_GRAMS_LITER));
            cmd_phys.Parameters.AddWithValue("@PH", dbNull(msds.PH));
            cmd_phys.Parameters.AddWithValue("@VISCOSITY", dbNull(msds.VISCOSITY));
            cmd_phys.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(msds.EVAP_RATE_REF));
            cmd_phys.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(msds.SOL_IN_WATER));
            cmd_phys.Parameters.AddWithValue("@APP_ODOR", dbNull(msds.APP_ODOR));
            cmd_phys.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(msds.PERCENT_VOL_VOLUME));
            cmd_phys.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(msds.AUTOIGNITION_TEMP));
            cmd_phys.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(msds.CARCINOGEN_IND));
            cmd_phys.Parameters.AddWithValue("@EPA_ACUTE", dbNull(msds.EPA_ACUTE));
            cmd_phys.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(msds.EPA_CHRONIC));
            cmd_phys.Parameters.AddWithValue("@EPA_FIRE", dbNull(msds.EPA_FIRE));
            cmd_phys.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(msds.EPA_PRESSURE));
            cmd_phys.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(msds.EPA_REACTIVITY));
            cmd_phys.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(msds.FLASH_PT_TEMP));
            cmd_phys.Parameters.AddWithValue("@NEUT_AGENT", dbNull(msds.NEUT_AGENT));
            cmd_phys.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(msds.NFPA_FLAMMABILITY));
            cmd_phys.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(msds.NFPA_HEALTH));
            cmd_phys.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(msds.NFPA_REACTIVITY));
            cmd_phys.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(msds.NFPA_SPECIAL));
            cmd_phys.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(msds.OSHA_CARCINOGENS));
            cmd_phys.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(msds.OSHA_COMB_LIQUID));
            cmd_phys.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(msds.OSHA_COMP_GAS));
            cmd_phys.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(msds.OSHA_CORROSIVE));
            cmd_phys.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(msds.OSHA_EXPLOSIVE));
            cmd_phys.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(msds.OSHA_FLAMMABLE));
            cmd_phys.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(msds.OSHA_HIGH_TOXIC));
            cmd_phys.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(msds.OSHA_IRRITANT));
            cmd_phys.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(msds.OSHA_ORG_PEROX));
            cmd_phys.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(msds.OSHA_OTHERLONGTERM));
            cmd_phys.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(msds.OSHA_OXIDIZER));
            cmd_phys.Parameters.AddWithValue("@OSHA_PYRO", dbNull(msds.OSHA_PYRO));
            cmd_phys.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(msds.OSHA_SENSITIZER));
            cmd_phys.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(msds.OSHA_TOXIC));
            cmd_phys.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(msds.OSHA_UNST_REACT));
            cmd_phys.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(msds.OTHER_SHORT_TERM));
            cmd_phys.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(msds.PHYS_STATE_CODE));
            cmd_phys.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(msds.VOL_ORG_COMP_WT));
            cmd_phys.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(msds.OSHA_WATER_REACTIVE));
            
            cmd_phys.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_phys.ExecuteNonQuery();
                cmd_phys.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_phys \n\t" + ex.Message);
            }

            //insert ingredients
            foreach (MsdsIngredients ingredient in msds.ingredientsList)
            {
                
                cmd_ing.Parameters.AddWithValue("@CAS", dbNull(ingredient.CAS));
                cmd_ing.Parameters.AddWithValue("@RTECS_NUM", dbNull(ingredient.RTECS_NUM));
                cmd_ing.Parameters.AddWithValue("@RTECS_CODE", dbNull(ingredient.RTECS_CODE));
                cmd_ing.Parameters.AddWithValue("@INGREDIENT_NAME", dbNull(ingredient.INGREDIENT_NAME));
                cmd_ing.Parameters.AddWithValue("@PRCNT", dbNull(ingredient.PRCNT));               
                cmd_ing.Parameters.AddWithValue("@OSHA_PEL", dbNull(ingredient.OSHA_PEL));
                cmd_ing.Parameters.AddWithValue("@OSHA_STEL", dbNull(ingredient.OSHA_STEL));
                cmd_ing.Parameters.AddWithValue("@ACGIH_TLV", dbNull(ingredient.ACGIH_TLV));
                cmd_ing.Parameters.AddWithValue("@ACGIH_STEL", dbNull(ingredient.ACGIH_STEL));
                cmd_ing.Parameters.AddWithValue("@EPA_REPORT_QTY", dbNull(ingredient.EPA_REPORT_QTY));
                cmd_ing.Parameters.AddWithValue("@DOT_REPORT_QTY", dbNull(ingredient.DOT_REPORT_QTY));
                cmd_ing.Parameters.AddWithValue("@PRCNT_VOL_VALUE", dbNull(ingredient.PRCNT_VOL_VALUE));
                cmd_ing.Parameters.AddWithValue("@PRCNT_VOL_WEIGHT", dbNull(ingredient.PRCNT_VOL_WEIGHT));
                cmd_ing.Parameters.AddWithValue("@CHEM_MFG_COMP_NAME", dbNull(ingredient.CHEM_MFG_COMP_NAME));
                cmd_ing.Parameters.AddWithValue("@ODS_IND", dbNull(ingredient.ODS_IND));
                cmd_ing.Parameters.AddWithValue("@OTHER_REC_LIMITS", dbNull(ingredient.OTHER_REC_LIMITS));
                cmd_ing.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

                try {
                    cmd_ing.ExecuteNonQuery();
                    cmd_ing.Parameters.Clear();
                }
                catch (Exception ex) {
                    Debug.WriteLine("***** Error in insertMSDS_ing \n\t" + ex.Message);
                }
            }


            //insert contracts
            foreach (MsdsContractors contract in msds.contractorsList)
            {

                cmd_contract.Parameters.AddWithValue("@CT_NUMBER", dbNull(contract.CT_NUMBER));
                cmd_contract.Parameters.AddWithValue("@CT_CAGE", dbNull(contract.CT_CAGE));
                cmd_contract.Parameters.AddWithValue("@CT_CITY", dbNull(contract.CT_CITY));
                cmd_contract.Parameters.AddWithValue("@CT_COMPANY_NAME", dbNull(contract.CT_COMPANY_NAME));
                cmd_contract.Parameters.AddWithValue("@CT_COUNTRY", dbNull(contract.CT_COUNTRY));
                cmd_contract.Parameters.AddWithValue("@CT_PO_BOX", dbNull(contract.CT_PO_BOX));
                cmd_contract.Parameters.AddWithValue("@CT_PHONE", dbNull(contract.CT_PHONE));
                cmd_contract.Parameters.AddWithValue("@PURCHASE_ORDER_NO", dbNull(contract.PURCHASE_ORDER_NO));
                cmd_contract.Parameters.AddWithValue("@CT_STATE", dbNull(contract.CT_STATE));
                cmd_contract.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

                try {
                    cmd_contract.ExecuteNonQuery();
                    cmd_contract.Parameters.Clear();
                }
                catch (Exception ex) {
                    Debug.WriteLine("***** Error in insertMSDS_contract \n\t" + ex.Message);
                }
            }


            //insert radiological
            cmd_radio.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(msds.NRC_LP_NUM));
            cmd_radio.Parameters.AddWithValue("@OPERATOR", dbNull(msds.OPERATOR));
            cmd_radio.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(msds.RAD_AMOUNT_MICRO));
            cmd_radio.Parameters.AddWithValue("@RAD_FORM", dbNull(msds.RAD_FORM));
            cmd_radio.Parameters.AddWithValue("@RAD_CAS", dbNull(msds.RAD_CAS));
            cmd_radio.Parameters.AddWithValue("@RAD_NAME", dbNull(msds.RAD_NAME));
            cmd_radio.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(msds.RAD_SYMBOL));
            cmd_radio.Parameters.AddWithValue("@REP_NSN", dbNull(msds.REP_NSN));
            cmd_radio.Parameters.AddWithValue("@SEALED", dbNull(msds.SEALED));
           
            cmd_radio.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_radio.ExecuteNonQuery();
                cmd_radio.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_radio \n\t" + ex.Message);
            }

            //insert transport
            cmd_transport.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(msds.AF_MMAC_CODE));
            cmd_transport.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(msds.CERTIFICATE_COE));
            cmd_transport.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(msds.COMPETENT_CAA));
            cmd_transport.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(msds.DOD_ID_CODE));
            cmd_transport.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(msds.DOT_EXEMPTION_NO));
            cmd_transport.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(msds.DOT_RQ_IND));
            cmd_transport.Parameters.AddWithValue("@EX_NO", dbNull(msds.EX_NO));
            cmd_transport.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(msds.FLASH_PT_TEMP));
            cmd_transport.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(msds.HIGH_EXPLOSIVE_WT));
            cmd_transport.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(msds.LTD_QTY_IND));
            cmd_transport.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(msds.MAGNETIC_IND));
            cmd_transport.Parameters.AddWithValue("@MAGNETISM", dbNull(msds.MAGNETISM));
            cmd_transport.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(msds.MARINE_POLLUTANT_IND));
            cmd_transport.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(msds.NET_EXP_QTY_DIST));
            cmd_transport.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(msds.NET_EXP_WEIGHT));
            cmd_transport.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(msds.NET_PROPELLANT_WT));
            cmd_transport.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(msds.NOS_TECHNICAL_SHIPPING_NAME));
            cmd_transport.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(msds.TRANSPORTATION_ADDITIONAL_DATA));
            
            cmd_transport.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_transport.ExecuteNonQuery();
                cmd_transport.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_transport \n\t" + ex.Message);
            }

            //insert dot psn
            cmd_dot.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(msds.DOT_HAZARD_CLASS_DIV));
            cmd_dot.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(msds.DOT_HAZARD_LABEL));
            cmd_dot.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(msds.DOT_MAX_CARGO));
            cmd_dot.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(msds.DOT_MAX_PASSENGER));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(msds.DOT_PACK_BULK));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(msds.DOT_PACK_EXCEPTIONS));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(msds.DOT_PACK_NONBULK));
            cmd_dot.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(msds.DOT_PROP_SHIP_NAME));
            cmd_dot.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(msds.DOT_PROP_SHIP_MODIFIER));
            cmd_dot.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(msds.DOT_PSN_CODE));
            cmd_dot.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(msds.DOT_SPECIAL_PROVISION));
            cmd_dot.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(msds.DOT_SYMBOLS));
            cmd_dot.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(msds.DOT_UN_ID_NUMBER));
            cmd_dot.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(msds.DOT_WATER_OTHER_REQ));
            cmd_dot.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(msds.DOT_WATER_VESSEL_STOW));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(msds.DOT_PACK_GROUP));
            
            cmd_dot.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_dot.ExecuteNonQuery();
                cmd_dot.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_dot \n\t" + ex.Message);
            }


            //insert afjm psn
            cmd_afjm.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(msds.AFJM_HAZARD_CLASS));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(msds.AFJM_PACK_PARAGRAPH));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(msds.AFJM_PACK_GROUP));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(msds.AFJM_PROP_SHIP_NAME));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(msds.AFJM_PROP_SHIP_MODIFIER));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(msds.AFJM_PSN_CODE));
            cmd_afjm.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(msds.AFJM_SPECIAL_PROV));
            cmd_afjm.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(msds.AFJM_SUBSIDIARY_RISK));
            cmd_afjm.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(msds.AFJM_SYMBOLS));
            cmd_afjm.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(msds.AFJM_UN_ID_NUMBER));
           
            cmd_afjm.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_afjm.ExecuteNonQuery();
                cmd_afjm.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_afjm \n\t" + ex.Message);
            }


            //insert iata psn
            cmd_iata.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(msds.IATA_CARGO_PACKING));
            cmd_iata.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(msds.IATA_HAZARD_CLASS));
            cmd_iata.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(msds.IATA_HAZARD_LABEL));
            cmd_iata.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(msds.IATA_PACK_GROUP));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(msds.IATA_PASS_AIR_PACK_LMT_INSTR));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(msds.IATA_PASS_AIR_PACK_LMT_PER_PKG));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(msds.IATA_PASS_AIR_PACK_NOTE));
            cmd_iata.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(msds.IATA_PROP_SHIP_NAME));
            cmd_iata.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(msds.IATA_PROP_SHIP_MODIFIER));
            cmd_iata.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(msds.IATA_CARGO_PACK_MAX_QTY));
            cmd_iata.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(msds.IATA_PSN_CODE));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(msds.IATA_PASS_AIR_MAX_QTY));
            cmd_iata.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(msds.IATA_SPECIAL_PROV));
            cmd_iata.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(msds.IATA_SUBSIDIARY_RISK));
            cmd_iata.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(msds.IATA_UN_ID_NUMBER));
            
            cmd_iata.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_iata.ExecuteNonQuery();
                cmd_iata.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_iata \n\t" + ex.Message);
            }

            //insert imo psn
            cmd_imo.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(msds.IMO_EMS_NO));
            cmd_imo.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(msds.IMO_HAZARD_CLASS));
            cmd_imo.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(msds.IMO_IBC_INSTR));
            cmd_imo.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(msds.IMO_LIMITED_QTY));
            cmd_imo.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(msds.IMO_PACK_GROUP));
            cmd_imo.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(msds.IMO_PACK_INSTRUCTIONS));
            cmd_imo.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(msds.IMO_PACK_PROVISIONS));
            cmd_imo.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(msds.IMO_PROP_SHIP_NAME));
            cmd_imo.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(msds.IMO_PROP_SHIP_MODIFIER));
            cmd_imo.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(msds.IMO_PSN_CODE));
            cmd_imo.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(msds.IMO_SPECIAL_PROV));
            cmd_imo.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(msds.IMO_STOW_SEGR));
            cmd_imo.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(msds.IMO_SUBSIDIARY_RISK));
            cmd_imo.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(msds.IMO_TANK_INSTR_IMO));
            cmd_imo.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(msds.IMO_TANK_INSTR_PROV));
            cmd_imo.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(msds.IMO_TANK_INSTR_UN));
            cmd_imo.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(msds.IMO_UN_NUMBER));
            cmd_imo.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(msds.IMO_IBC_PROVISIONS));
           
            cmd_imo.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_imo.ExecuteNonQuery();
                cmd_imo.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_imo \n\t" + ex.Message);
            }


            //insert item description
            cmd_desc.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(msds.ITEM_MANAGER));
            cmd_desc.Parameters.AddWithValue("@ITEM_NAME", dbNull(msds.ITEM_NAME));
            cmd_desc.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(msds.SPECIFICATION_NUMBER));
            cmd_desc.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(msds.TYPE_GRADE_CLASS));
            cmd_desc.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(msds.UNIT_OF_ISSUE));
            cmd_desc.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(msds.QUANTITATIVE_EXPRESSION));
            cmd_desc.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(msds.UI_CONTAINER_QTY));
            cmd_desc.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(msds.TYPE_OF_CONTAINER));
            cmd_desc.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(msds.BATCH_NUMBER));
            cmd_desc.Parameters.AddWithValue("@LOT_NUMBER", dbNull(msds.LOT_NUMBER));
            cmd_desc.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(msds.LOG_FLIS_NIIN_VER));
            cmd_desc.Parameters.AddWithValue("@LOG_FSC", dbNull(msds.LOG_FSC));
            cmd_desc.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(msds.NET_UNIT_WEIGHT));
            cmd_desc.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(msds.SHELF_LIFE_CODE));
            cmd_desc.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(msds.SPECIAL_EMP_CODE));
            cmd_desc.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(msds.UN_NA_NUMBER));
            cmd_desc.Parameters.AddWithValue("@UPC_GTIN", dbNull(msds.UPC_GTIN));
            
            cmd_desc.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_desc.ExecuteNonQuery();
                cmd_desc.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_desc \n\t" + ex.Message);
            }

            //insert label info
            cmd_label.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(msds.COMPANY_CAGE_RP));
            cmd_label.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(msds.COMPANY_NAME_RP));
            cmd_label.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(msds.LABEL_EMERG_PHONE));
            cmd_label.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(msds.LABEL_ITEM_NAME));
            cmd_label.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(msds.LABEL_PROC_YEAR));
            cmd_label.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(msds.LABEL_PROD_IDENT));
            cmd_label.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(msds.LABEL_PROD_SERIALNO));
            cmd_label.Parameters.AddWithValue("@LABEL_SIGNAL_WORD", dbNull(msds.LABEL_SIGNAL_WORD));
            cmd_label.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(msds.LABEL_STOCK_NO));
            cmd_label.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(msds.SPECIFIC_HAZARDS));
           
            cmd_label.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_label.ExecuteNonQuery();
                cmd_label.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_label \n\t" + ex.Message);
            }


            //insert disposal
            cmd_disposal.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(msds.DISPOSAL_ADD_INFO));
            cmd_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(msds.EPA_HAZ_WASTE_CODE));
            cmd_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(msds.EPA_HAZ_WASTE_IND));
            cmd_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(msds.EPA_HAZ_WASTE_NAME));

            cmd_disposal.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_disposal.ExecuteNonQuery();
                cmd_disposal.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_disposal \n\t" + ex.Message);
            }

            //insert doc types
            if (msds.MANUFACTURER_LABEL != null)
                cmd_docs.Parameters.AddWithValue("@MANUFACTURER_LABEL", dbNull(msds.MANUFACTURER_LABEL));
            else
            {

                SqlParameter p = new SqlParameter("@MANUFACTURER_LABEL", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.MSDS_TRANSLATED != null)
                cmd_docs.Parameters.AddWithValue("@MSDS_TRANSLATED", dbNull(msds.MSDS_TRANSLATED));
            else
            {

                SqlParameter p = new SqlParameter("@MSDS_TRANSLATED", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.NESHAP_COMP_CERT != null)
                cmd_docs.Parameters.AddWithValue("@NESHAP_COMP_CERT", dbNull(msds.NESHAP_COMP_CERT));
            else
            {

                SqlParameter p = new SqlParameter("@NESHAP_COMP_CERT", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.OTHER_DOCS != null)
                cmd_docs.Parameters.AddWithValue("@OTHER_DOCS", dbNull(msds.OTHER_DOCS));
            else
            {

                SqlParameter p = new SqlParameter("@OTHER_DOCS", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
             if (msds.PRODUCT_SHEET != null)
                 cmd_docs.Parameters.AddWithValue("@PRODUCT_SHEET", dbNull(msds.PRODUCT_SHEET));
            else
            {

                SqlParameter p = new SqlParameter("@PRODUCT_SHEET", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.TRANSPORTATION_CERT != null)
                cmd_docs.Parameters.AddWithValue("@TRANSPORTATION_CERT", dbNull(msds.TRANSPORTATION_CERT));
            else
            {

                SqlParameter p = new SqlParameter("@TRANSPORTATION_CERT", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            cmd_docs.Parameters.AddWithValue("@msds_translated_filename", dbNull(msds.msds_translated_filename));
            cmd_docs.Parameters.AddWithValue("@neshap_comp_filename", dbNull(msds.neshap_comp_filename));
            cmd_docs.Parameters.AddWithValue("@other_docs_filename", dbNull(msds.other_docs_filename));
            cmd_docs.Parameters.AddWithValue("@product_sheet_filename", dbNull(msds.product_sheet_filename));
            cmd_docs.Parameters.AddWithValue("@transportation_cert_filename", dbNull(msds.transportation_cert_filename));
            cmd_docs.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(msds.manufacturer_label_filename));
            
            cmd_docs.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_docs.ExecuteNonQuery();
                cmd_docs.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_docs \n\t" + ex.Message);
            }
 

            tran.Commit();
            conn.Close();
        }//end MSDS


        public DateTime? calculateShelfLifeExpirationDate(string manufacturer_date, string shelf_life_expiration_date, int shelf_life_code_id)
        {
            DateTime? shelf_life = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //calculate shelf_life_expiration_date from manufacturer_date and SLC
            int slc_time_in_months = 0;
            if (shelf_life_code_id != 0)
            {
                SqlCommand selectCmd = new SqlCommand("Select time_in_months from shelf_life_code where shelf_life_code_id=@shelf_life_code_id", conn);
                selectCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                object o = selectCmd.ExecuteScalar();
                if (o != null)
                {
                    if (!o.ToString().Equals(""))
                        slc_time_in_months = Int32.Parse(o.ToString());
                }
            }

            if (manufacturer_date != null && !manufacturer_date.Equals(""))
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);                

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    shelf_life = slcDate;

                }
            }
            else
            {
                
                if (shelf_life_expiration_date != null && !shelf_life_expiration_date.Equals(""))
                {
                    DateTime slcDate = DateTime.Parse(shelf_life_expiration_date);
                    shelf_life = slcDate;
                }

            }

            conn.Close();

            return shelf_life;
        }


         public void updateInventory(int inventory_id, string manufacturer_date, 
            string shelf_life_expiration_date, int shelf_life_code_id, 
            int location_id, int qty, string serial_number,
            bool bulk_item, int mfg_catalog_id, string batch_number, string lot_number, string contract_number, 
            string cosal, string atm)
        {
            updateInventory(inventory_id, manufacturer_date, 
            shelf_life_expiration_date, shelf_life_code_id, 
            location_id, qty, serial_number,
            bulk_item, mfg_catalog_id, batch_number, lot_number, contract_number, 
            cosal, atm, null);
        }

        public void updateInventory(int inventory_id, string manufacturer_date, 
            string shelf_life_expiration_date, int shelf_life_code_id, 
            int location_id, int qty, string serial_number,
            bool bulk_item, int mfg_catalog_id, string batch_number, string lot_number, string contract_number, 
            string cosal, string atm, string comments)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            //calculate shelf_life_expiration_date from manufacturer_date and SLC
            int slc_time_in_months = 0;
            if (shelf_life_code_id != 0)
            {
                SqlCommand selectCmd = new SqlCommand("Select time_in_months from shelf_life_code where shelf_life_code_id=@shelf_life_code_id", conn);
                selectCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                object o = selectCmd.ExecuteScalar();
                if (o != null)
                {
                    if (!o.ToString().Equals(""))
                        slc_time_in_months = Int32.Parse(o.ToString());
                }
            }

            string insertStmt = "UPDATE inventory SET " +
            "shelf_life_expiration_date=@shelf_life_expiration_date, " +
            "manufacturer_date=@manufacturer_date, location_id=@location_id, " +
            "qty=@qty, serial_number=@serial_number," +
            "bulk_item=@bulk_item, mfg_catalog_id=@mfg_catalog_id, " +
            "batch_number=@batch_number, lot_number=@lot_number, contract_number=@contract_number, " +
            "cosal=@cosal, atm=@atm, comments=@comments " +
            " WHERE inventory_id=@inventory_id";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            if (manufacturer_date != null && !manufacturer_date.Equals(""))
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));

                if (shelf_life_expiration_date != null && !shelf_life_expiration_date.Equals(""))
                {
                    DateTime slcDate = DateTime.Parse(shelf_life_expiration_date);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }
            }
            cmd.Parameters.AddWithValue("@location_id", dbFKeyNull(location_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(qty));
            cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
            cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
            cmd.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbFKeyNull(mfg_catalog_id));

            cmd.Parameters.AddWithValue("@batch_number", dbNull(batch_number));
            cmd.Parameters.AddWithValue("@lot_number", dbNull(lot_number));
            cmd.Parameters.AddWithValue("@contract_number", dbNull(truncate(50, contract_number)));

            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
            cmd.Parameters.AddWithValue("@atm", dbNull(atm));

            cmd.Parameters.AddWithValue("@comments", dbNull(comments));

            cmd.ExecuteNonQuery();

            conn.Close();
        }//end 

        public int findGarbageItem(string description)
        {

            int garbage_item_id = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "Select garbage_item_id from garbage_items where description=@description";

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@description", dbNull(description));
            
            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                garbage_item_id = Int32.Parse(reader.GetValue(0).ToString());
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return garbage_item_id;
        }

        public int findMfgCatalog(string cage, int niin_catalog_id)
        {

            int mfg_catalog_id = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "Select mfg_catalog_id from mfg_catalog where cage=@cage AND niin_catalog_id=@niin_catalog_id";

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@cage", dbNull(cage));
            cmdRead.Parameters.AddWithValue("@niin_catalog_id", dbNull(niin_catalog_id));
            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {

               mfg_catalog_id = Int32.Parse(reader.GetValue(0).ToString());
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return mfg_catalog_id;
        }

        public int insertMfgCatalog(string cage, string manufacturer, int niin_catalog_id, string product_identity)
        {
            
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = "insert into mfg_catalog (" +

            "cage, manufacturer, niin_catalog_id, product_identity"

            + ")"
            + "VALUES(" +
            "@cage, @manufacturer, @niin_catalog_id, @product_identity"
            + "); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@cage", dbNull(cage));
            cmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
            cmd.Parameters.AddWithValue("@niin_catalog_id", dbFKeyNull(niin_catalog_id));
            cmd.Parameters.AddWithValue("@product_identity", dbNull(product_identity));

            object o = cmd.ExecuteScalar();
            int mfg_catalog_id = Convert.ToInt32(o);

            conn.Close();

            return mfg_catalog_id;
        }//end 

        public void updateMfgCatalog(string manufacturer, int mfg_catalog_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = "UPDATE mfg_catalog " +

            "set manufacturer=@manufacturer WHERE"

            + " mfg_catalog_id=@mfg_catalog_id ";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbFKeyNull(mfg_catalog_id));

            cmd.ExecuteNonQuery();

            conn.Close();

        }//end 

        public int insertNiinCatalog(int fsc, string niin, string ui, string um,
            int usage_category_id, string description, int smcc_id, string specs,
            int shelf_life_code_id, int shelf_life_action_code_id, string remarks,
            int storage_type_id, int cog_id, string spmig, string nehc_rpt,
            int catalog_group_id, string catalog_serial_number, int allowance_qty, int ship_id, bool manually_entered)
        {  
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            int niin_catalog_id;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
           
            string insertStmt = "insert into NIIN_CATALOG (" +
            
            "fsc, niin, ui, um, usage_category_id, description, smcc_id, specs,"+
            "shelf_life_code_id, shelf_life_action_code_id, remarks,"+
            "storage_type_id, cog_id, spmig, nehc_rpt,"+
            "catalog_group_id, catalog_serial_number, allowance_qty, ship_id, created, manually_entered"
            
            +")"
            + "VALUES("+
             "@fsc, @niin, @ui, @um, @usage_category_id, @description, @smcc_id, @specs," +
            "@shelf_life_code_id, @shelf_life_action_code_id, @remarks," +
            "@storage_type_id, @cog_id, @spmig, @nehc_rpt," +
            "@catalog_group_id, @catalog_serial_number, @allowance_qty, @ship_id, @created, @manually_entered"
            + ");SELECT scope_identity()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;
            cmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
            cmd.Parameters.AddWithValue("@niin", dbNull(niin));
            cmd.Parameters.AddWithValue("@ui", dbNull(ui));
            cmd.Parameters.AddWithValue("@um", dbNull(um));
            cmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
            cmd.Parameters.AddWithValue("@description", dbNull(description));
            cmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
            cmd.Parameters.AddWithValue("@specs", dbNull(specs));

            cmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
            cmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
            cmd.Parameters.AddWithValue("@remarks", dbNull(remarks));

            cmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
            cmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
            cmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
            cmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));

            cmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
            cmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
            cmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
            cmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));
            cmd.Parameters.AddWithValue("@created", dbNull(DateTime.Now));

            cmd.Parameters.AddWithValue("@manually_entered", dbNull(manually_entered));

            //cmd.Parameters.AddWithValue("@atc", dbNull(atc));
            //cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));

            object o = cmd.ExecuteScalar();
            niin_catalog_id = Convert.ToInt32(o);
            tran.Commit();
            conn.Close();
            return niin_catalog_id;
        }//end order


        public DataTable getOffloadTotalVolumes()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_offload_volumes";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;


        }

        public DataTable getNextOffloadDetailSerialIteration()
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select MAX(serial_number)as serial_number, iteration from offload_details where iteration=(SELECT MAX(iteration) from offload_details) group by iteration";
            SqlCommand cmd = new SqlCommand(stmt, con);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            con.Close();

            return dt;


        }

        public int getExistingOffloadListId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 offload_list_id from offload_list";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public int getExistingGarbageItemId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 garbage_item_id from garbage_items";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public int getExistingGarbageOffloadListId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 garbage_offload_id from garbage_offload_list";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        //Get inventory that is without an exact matching change record
        public DataTable getDiscrepanciesInventoryByLocationAndAudit(int location_id, int inventory_audit_id, string filterColumn, string filterText)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            // Replaced the old query calling nested views with this one 
            // Query uses a join within a select + filter
            // The join performs the work of getting the audit and unaudited records
            // The outer select allows the results of the join to be sorted and 
            // filtered as a whole
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM (");   // Outer select - allows filtering of results
            sb.Append("	SELECT ");  // Select to get the results from the audit_cr table
            sb.Append("	(CASE ");
            sb.Append("		WHEN (inv.qty - ia.qty) > 0 THEN 'LOSS' ");
            sb.Append("		WHEN (inv.qty - ia.qty) < 0 THEN 'GAIN' ");
            sb.Append("		ELSE 'MATCH' ");
            sb.Append("	END) AS details, "); 
            sb.Append(" ia.action_complete, ia.qty AS counted_qty, ia.inventory_audit_cr_id, ");
            sb.Append("	inv.location_id, inv.alternate_ui, inv.alternate_um, inv.batch_number, inv.contract_number, ");
            sb.Append("	inv.cosal, inv.qty AS expected_qty, inv.inventory_id, inv.lot_number, inv.mfg_catalog_id, inv.serial_number, ");
            sb.Append("	inv.shelf_life_expiration_date, inv.atm, ");
            sb.Append("	mc.cage, mc.manufacturer, mc.niin_catalog_id, mc.product_identity, ");
            sb.Append("	nc.description, nc.niin, nc.shelf_life_code_id, nc.ui, nc.um, nc.manually_entered, ");
            sb.Append("	(inv.qty - ia.qty) AS qty_difference ");
            sb.Append(" FROM inventory inv ");
            sb.Append(" JOIN inventory_audit_cr ia ON ia.atm = inv.atm AND ia.inventory_audit_id = @inventory_audit_id ");
            sb.Append(" JOIN mfg_catalog mc ON mc.mfg_catalog_id = inv.mfg_catalog_id ");
            sb.Append(" JOIN niin_catalog nc on nc.niin_catalog_id = mc.niin_catalog_id ");
            sb.Append(" WHERE inv.location_id = @location_id ");
            sb.Append(" UNION ");
            sb.Append(" SELECT 'LOSS' AS details, null, 0 AS counted_qty, 0, ");    // Select to get the missing inventory items
            sb.Append("	inv.location_id, inv.alternate_ui, inv.alternate_um, inv.batch_number, inv.contract_number, ");
            sb.Append("	inv.cosal, inv.qty AS expected_qty, inv.inventory_id, inv.lot_number, inv.mfg_catalog_id, inv.serial_number, ");
            sb.Append("	inv.shelf_life_expiration_date, inv.atm, ");
            sb.Append("	mc.cage, mc.manufacturer, mc.niin_catalog_id, mc.product_identity, ");
            sb.Append("	nc.description, nc.niin, nc.shelf_life_code_id, nc.ui, nc.um, nc.manually_entered, ");
            sb.Append("	(-inv.qty) AS qty_difference ");
            sb.Append(" FROM inventory inv ");
            sb.Append(" JOIN mfg_catalog mc ON mc.mfg_catalog_id = inv.mfg_catalog_id ");
            sb.Append(" JOIN niin_catalog nc on nc.niin_catalog_id = mc.niin_catalog_id ");
            sb.Append(" WHERE inv.location_id = @location_id ");
            sb.Append("	AND inv.atm NOT IN ");
            sb.Append("	(SELECT atm FROM inventory_audit_cr ");
            sb.Append("		WHERE inventory_audit_id = @inventory_audit_id AND location_id = @location_id) ");
            sb.Append(") AS t ");   // End of outer select

            // Add the filter
            if (!filterText.Equals("")) {
                sb.Append(" WHERE " + filterColumn + " like '%' + @filter + '%' ");
            }

            // Order by
            sb.Append("ORDER BY niin, manufacturer, details");

            SqlCommand cmd = new SqlCommand(sb.ToString(), conn);
            //SqlCommand cmd = new SqlCommand(selectStmt, conn);

            cmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(inventory_audit_id));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;


        }


        //Get inventory that is without an exact matching change record
        public DataTable getWorkcenterAuditCorrectness()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string search = "";


            string selectStmt = "select v.*, m.*, cr.action_complete, cr.count, cr.server_synctime from v_inventory_location_discrepancies v inner join vMfgCatNiinCat m " +
" ON (v.mfg_catalog_id=m.mfg_catalog_id) " +
" left join inventory_audit_cr cr " +
" ON (v.inventory_audit_cr_id = cr.inventory_audit_cr_id) " +
" WHERE " +
" (  (details='MATCH') " +
" OR " +
" (details='LOSS') " +
" OR " +
" (details='GAIN' AND v.counted_qty!=0 )  ) "

//Make sure count is NULL (for inventory not in audit) OR count is the latest (for audited inventory)
+ " AND (v.count is null OR " +
" v.count =   (SELECT     max_count   FROM    dbo.v_max_count_audit_done_location AS a "
+ "  WHERE      (location_id = cr.location_id) AND (cr.inventory_audit_id = inventory_audit_id))) "

////Ignore any records with their id in the parent_id field
+ " AND (v.inventory_audit_cr_id is NULL or (v.inventory_audit_cr_id NOT IN (SELECT     parent_id " +
            "                FROM         inventory_audit_cr " +
                         "   WHERE      (parent_id IS NOT NULL)))) "

                         + " " + search + " "

+ " ORDER BY m.niin_catalog_id ASC, v.mfg_catalog_id ASC, v.shelf_life_expiration_date ASC, v.alternate_ui ASC, v.alternate_um ASC, v.details DESC, v.inventory_audit_cr_id ASC, v.inventory_id ASC; ";


            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

           

            Dictionary<int, int> locationCorrectCount = new Dictionary<int, int>();
            Dictionary<int, int> locationWrongCount = new Dictionary<int, int>();

            //Filter the dataTable records and remove duplicates.
            List<int> invCrList = new List<int>();
            List<int> inventoryIdList = new List<int>();

            foreach (DataRow row in dt.Rows)
            {
                int location_id = Int32.Parse(row["location_id"].ToString());
                int correct = 0;
                int wrong = 0;

                if (locationCorrectCount.ContainsKey(location_id))
                    correct = locationCorrectCount[location_id];
                else
                    locationCorrectCount.Add(location_id, 0);

                if (locationWrongCount.ContainsKey(location_id))
                    wrong = locationWrongCount[location_id];
                else
                    locationWrongCount.Add(location_id, 0);

                int qty_difference = Int32.Parse(row["qty_difference"].ToString());

                //////
                int? inventory_audit_cr_id = null;
                int? inventory_id = null;

                try
                {
                    inventory_audit_cr_id = Int32.Parse(row["inventory_audit_cr_id"].ToString());
                }
                catch { }
                try
                {
                    inventory_id = Int32.Parse(row["inventory_id"].ToString());
                }
                catch { }

                //We don't want duplicates of either id in the table. If either match, delete that row.
                if ((inventory_audit_cr_id != null && invCrList.Contains(inventory_audit_cr_id.Value))
                    || (inventory_id != null && inventoryIdList.Contains(inventory_id.Value)))
                {
                    row.Delete();
                }
                else
                {
                    if (inventory_audit_cr_id != null)
                        invCrList.Add(inventory_audit_cr_id.Value);

                    if (inventory_id != null)
                        inventoryIdList.Add(inventory_id.Value);

                    ///////
                    if (qty_difference == 0)
                    {
                        locationCorrectCount[location_id]++;
                    }
                    else
                    {
                        locationWrongCount[location_id]++;
                    }

                }

            }
            //end remove duplicates

            //Get list of all workcenters
             SqlCommand cmd2 = new SqlCommand("select *, 100 as percent_correct, '100' as total from vLocationWorkcenters where name<>'NEWLY ISSUED' order by workcenter_id, name", conn);

            DataTable workcenterTable = new DataTable();

            new SqlDataAdapter(cmd2).Fill(workcenterTable);

            Dictionary<int, double> workcenterMap = new Dictionary<int, double>();

            //for each workcenter/locations
            foreach (DataRow r in workcenterTable.Rows)
            {
                int workcenter_id = Int32.Parse(r["workcenter_id"].ToString());

                int location_id = Int32.Parse(r["location_id"].ToString());

                double correct = 0;

                if (locationCorrectCount.ContainsKey(location_id))
                    correct = locationCorrectCount[location_id];

                double wrong = 0;

                if (locationWrongCount.ContainsKey(location_id))
                    wrong = locationWrongCount[location_id];

                double total = correct + wrong;

                double percent_correct = 100;

                if (wrong != 0 && total != 0)
                {
                    percent_correct = (correct / total) * 100.0;
                }

                r["percent_correct"] = Math.Round(percent_correct, 3);


                double workcenter_percent = -1;

                if (workcenterMap.ContainsKey(workcenter_id))
                    workcenter_percent = workcenterMap[workcenter_id];

                if (workcenter_percent == -1)
                {
                    workcenter_percent = percent_correct;
                    workcenterMap.Add(workcenter_id, workcenter_percent);
                }
                else
                {
                    workcenter_percent = (workcenter_percent + percent_correct) / 2.0;
                    workcenterMap.Remove(workcenter_id);
                    workcenterMap.Add(workcenter_id, workcenter_percent);
                }

            }//end for each location

            int last_workcenter_id = 0;
            //for each workcenter/locations
            foreach (DataRow r in workcenterTable.Rows)
            {
                int workcenter_id = Int32.Parse(r["workcenter_id"].ToString());

                r["total"] = Math.Round(workcenterMap[workcenter_id], 3) + " %";

                if (last_workcenter_id == workcenter_id)
                {
                    r["total"] = "";
                }
                last_workcenter_id = workcenter_id;

            }//end for each location


             conn.Close();
             return workcenterTable;


        }


        public DataTable getFilteredInventoryAvailable(String whereColumn, String filterValue, String location, String workcenter, List<int> inventoryList, bool workcenterUser, string username)
        {

            string str = "";
            int count = 1;

            foreach (int i in inventoryList)
            {

                if (count != inventoryList.Count)
                {

                    str += i + ",";
                }
                else
                {

                    str += i;
                }

                count++;
            }


            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "";
            if (workcenterUser)
            {
               selectStmt = "select i.*, m.*, l.name, l.description as workcenter " +
                "from inventory i INNER JOIN vMfgCatNiinCat m " +
                "ON(i.mfg_catalog_id=m.mfg_catalog_id) " +
                "INNER JOIN vLocationWorkcenters l ON (i.location_id=l.location_id) " +
                "where l.workcenter_id in (select u.workcenter_id from authorized_users u where u.username = @username AND u.deleted = @deleted)";
            }
            else
            {

               selectStmt = "select i.*, m.*, l.name, l.description as workcenter from inventory i INNER JOIN vMfgCatNiinCat m ON(i.mfg_catalog_id=m.mfg_catalog_id) " +
               "  INNER JOIN vLocationWorkcenters l ON (i.location_id=l.location_id) ";
            }
            string whereStmt = "";

            bool addFilter = false;
            bool addLocation = false;
            bool addWorkcenter = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                if (whereColumn.Equals("ATM"))
                {
                    filterStmt = " AND atm = @filter";
                   // filterStmt = " AND m.niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                   // "AND m.cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter)" ;
                }
                else
                {
                    filterStmt = " AND  m." + whereColumn + " like '%' + @filter + '%' ";
                }

                    
              
                addFilter = true;
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

                // locationStmt = " i.location_id = @location";
                locationStmt = " l.name = @location";

                if (addFilter)
                    locationStmt = " AND " + locationStmt;
                else
                    locationStmt = " AND " + locationStmt;

                addLocation = true;

            }
            string workcenterStmt = "";
            if (!workcenter.Equals(""))
            {

                workcenterStmt = " l.workcenter_id = @workcenter";

                if (addLocation)
                    workcenterStmt = " AND " + workcenterStmt;
                else if (addFilter && !addLocation)
                    workcenterStmt = " AND " + workcenterStmt;
                else
                    workcenterStmt = " AND " + workcenterStmt;

                addWorkcenter = true;

            }

            whereStmt += (filterStmt + locationStmt + workcenterStmt);

            selectStmt += whereStmt;            

            if (whereStmt.Equals(""))
            {
                if (inventoryList.Count > 0 && !workcenterUser)
                {
                    selectStmt += " WHERE inventory_id not in(" + str + ")";
                }
                else if (inventoryList.Count > 0 && workcenterUser)
                {
                    selectStmt += " AND inventory_id not in(" + str + ")";
                }

            }
            else
            {
                if (inventoryList.Count > 0)
                {
                    selectStmt += " AND inventory_id not in(" + str + ")";
                }
            }

            selectStmt += " ORDER BY m.niin_catalog_id, m.mfg_catalog_id, i.shelf_life_expiration_date, i.alternate_ui, i.alternate_um, l.description, l.name";


            SqlCommand cmd = new SqlCommand(selectStmt, conn);            
            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location; //Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workcenter);
            if (workcenterUser)
            {
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@deleted", false);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getFilteredSMCLCollection(String whereColumn, String filterValue, String usageCategory, String catalogGroup)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, nc.usage_category_id, u.description as usage_category, u.category as category," +
            " nc.description, nc.smcc_id, sm.smcc as smcc,"
            + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, " +
            "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
            "nc.remarks, nc.storage_type_id, st.type as storage_type, " +
            "nc.cog_id, nc.spmig, nc.nehc_rpt, nc.catalog_group_id, nc.cog_id, cc.cog as cog," +
            "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id, nc.manually_entered, nc.dropped_in_error, nc.cage, nc.manufacturer, nc.msds_num " +

            "  from NIIN_Catalog nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
            + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
            + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
            + "LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
            + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
            + " LEFT JOIN cog_codes cc ON (nc.cog_id=cc.cog_id) WHERE niin_catalog_id > 0";
            
            string whereStmt = "";
                        
            bool addFilter = false;
            bool addUsageCategory = false;
            bool addCatalogGroup = false;

            string filterStmt ="";
            if (!filterValue.Equals(""))
            {
                if (!whereColumn.Equals("CAGE"))
                {

                    if (whereColumn.Equals("ATM"))
                    {
                        filterStmt = " AND nc.niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                                "AND nc.niin_catalog_id in (select mfg.niin_catalog_id from mfg_catalog mfg where mfg.cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter)) ";
                    }
                    else
                    {

                        filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                    }
                }
                else
                {
                    filterStmt = " AND nc.niin_catalog_id IN " +
                    "(SELECT mfg.niin_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            string usageStmt = "";
            if (!usageCategory.Equals(""))
            {
                
                usageStmt = " nc.usage_category_id = @usageCategory";

                if (addFilter)
                    usageStmt = " AND " + usageStmt;
                else
                    usageStmt = " AND " + usageStmt;

                addUsageCategory = true;

            }
            string groupStmt = "";
            if (!catalogGroup.Equals(""))
            {
                
                groupStmt = " nc.catalog_group_id = @catalogGroup";

                if (addUsageCategory)
                    groupStmt = " AND " + groupStmt;
                else if (addFilter && !addUsageCategory)
                    groupStmt = " AND " + groupStmt;
                else
                    groupStmt = " AND " + groupStmt;

                addCatalogGroup = true;

            }

            string orderStmt = " ORDER BY nc.niin";

            whereStmt = filterStmt + usageStmt + groupStmt;

            selectStmt += whereStmt;
            selectStmt += orderStmt;

           SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if(addFilter)
            cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addUsageCategory)
                cmd.Parameters.Add("@usageCategory", SqlDbType.Int).Value = Int32.Parse(usageCategory);
            if (addCatalogGroup)
                cmd.Parameters.Add("@catalogGroup", SqlDbType.Int).Value = Int32.Parse(catalogGroup);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getFilteredAllowanceQtyCollection(String whereColumn, String filterValue, string usageCategory, string catalogGroup)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.cosal, nc.at, nc.is_decanted_inventory, nc.qty, nc.qty_difference, nc.alternate_ui, nc.alternate_um, nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, nc.usage_category_id, u.description as usage_category, u.category as category," +
            " nc.description, nc.smcc_id, sm.smcc as smcc,"
            + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, " +
            "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
            "nc.remarks, nc.storage_type_id, st.type as storage_type, " +
            "nc.cog_id, nc.spmig, nc.nehc_rpt, nc.catalog_group_id, nc.cog_id, cc.cog as cog," +
            "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id" +

            "  from v_niin_allowance_qty nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
            + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
            + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
            + "LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
            + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
            + " LEFT JOIN cog_codes cc ON (nc.cog_id=cc.cog_id) WHERE nc.qty_difference > 0 ";

            string whereStmt = "";

            bool addFilter = false;
            bool addUsageCategory = false;
            bool addCatalogGroup = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {
                if (!whereColumn.Equals("CAGE"))
                {
                    filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                }
                else
                {
                    filterStmt = " AND nc.niin_catalog_id IN " +
                    "(SELECT mfg.niin_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            string usageStmt = "";
            if (!usageCategory.Equals(""))
            {

                usageStmt = " nc.usage_category_id = @usageCategory";

                if (addFilter)
                    usageStmt = " AND " + usageStmt;
                else
                    usageStmt = " AND " + usageStmt;

                addUsageCategory = true;

            }
            string groupStmt = "";
            if (!catalogGroup.Equals(""))
            {

                groupStmt = " nc.catalog_group_id = @catalogGroup";

                if (addUsageCategory)
                    groupStmt = " AND " + groupStmt;
                else if (addFilter && !addUsageCategory)
                    groupStmt = " AND " + groupStmt;
                else
                    groupStmt = " AND " + groupStmt;

                addCatalogGroup = true;

            }

            whereStmt = filterStmt + usageStmt + groupStmt + " ORDER BY niin_catalog_id, alternate_ui, is_decanted_inventory";

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addUsageCategory)
                cmd.Parameters.Add("@usageCategory", SqlDbType.Int).Value = Int32.Parse(usageCategory);
            if (addCatalogGroup)
                cmd.Parameters.Add("@catalogGroup", SqlDbType.Int).Value = Int32.Parse(catalogGroup);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public List<ListItem> getStorageTypeList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select type, storage_type_id, description from storage_type";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getSmccList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select smcc, smcc_id, description from smcc";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getSlacList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select slac, shelf_life_action_code_id, description from shelf_life_action_code";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getSlcList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select slc, shelf_life_code_id, description from shelf_life_code";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getCogList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select cog, cog_id, description from cog_codes";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getUsageCategoryList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select category, usage_category_id, description from usage_category";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getUsageCategoryListU(bool includeDescriptionInDropdown)
        {
            //string selectStmt = "Select category, usage_category_id, description from usage_category";
            string selectStmt = "Select category, usage_category_id, description from usage_category where category = 'U'";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getLocationWorkcenterList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select l.name,  l.location_id,  w.description from locations l, workcenter w WHERE l.workcenter_id=w.workcenter_id order by w.description";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getHccList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select hcc, hcc_id, description from hcc order by hcc";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getCatalogGroupList()
        {
            string selectStmt = "Select group_name, catalog_group_id from catalog_groups ORDER BY group_name";
            return getListItemList(selectStmt, false);

        }
        public List<ListItem> getShipStatusesList()
        {
            string selectStmt = "Select description, status_id from ship_statuses";
            return getListItemList(selectStmt, false);

        }
        public List<ListItem> getOffloadReasonList()
        {
            string selectStmt = "Select reason_name, offload_reason_id from offload_reasons";
            return getListItemList(selectStmt, false);

        }

        public List<ListItem> getCageManufacturerList(int niin_catalog_id)
        {
            string selectStmt = "Select CAGE, MANUFACTURER,  mfg_catalog_id, PRODUCT_IDENTITY from mfg_catalog WHERE niin_catalog_id=@niin_catalog_id ORDER BY CAGE";
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                string prodId = ""+reader.GetValue(3);
                if (!prodId.Equals(""))
                    prodId = " | " + prodId + "";
                
                  list.Add(new ListItem("" + reader.GetValue(0) + "   (" + reader.GetValue(1) + ")"+prodId, "" + reader.GetValue(2)));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return list;
        }

        public List<ListItem> getShipToList()
        {
            string selectStmt = "Select ship_to_id, uic, organization, plate_title, city, state, zip from ship_to";
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
           
            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {

                list.Add(new ListItem("" + reader.GetValue(1) + "   (" + reader.GetValue(2) + ", " + reader.GetValue(4) + ", " + reader.GetValue(5) + ")", "" + reader.GetValue(0)));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return list;
        }

       public List<ListItem> getAuthorizedUserListForDropdown(int workcenter_id)
        {
            string selectStmt = "Select lastname + ', ' + firstname as username, authorized_user_id from authorized_users where workcenter_id="+workcenter_id + " AND deleted=0 ORDER BY lastname";
            return getListItemList(selectStmt, false);

        }

        public List<ListItem> getWorkcenterListByUserRole(bool includeDescription, string username)
        {
            string selectStmt = "select distinct w.wid, w.workcenter_id, w.description from workcenter w " +
                 "where w.workcenter_id in (select u.workcenter_id from authorized_users u where u.username = @username AND u.deleted = @deleted)";
                
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

            cmdRead.Parameters.AddWithValue("@username", username);
            cmdRead.Parameters.AddWithValue("@deleted", false);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                //ListItem(Label (code) + (description), Value (item id))
                if (!includeDescription)
                    list.Add(new ListItem("" + reader.GetValue(0), "" + reader.GetValue(1)));
                else
                    list.Add(new ListItem("" + reader.GetValue(0) + "   (" + reader.GetValue(2) + ")", "" + reader.GetValue(1)));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return list;


        }

        public List<ListItem> getWorkcenterList(bool includeDescription)
        {
            string selectStmt = "Select wid, workcenter_id, description from workcenter";
            return getListItemList(selectStmt, includeDescription);

        }
        public List<ListItem> getWorkcenterListNoSS01(bool includeDescription)
        {
            string selectStmt = "Select wid, workcenter_id, description from workcenter WHERE wid<>'SS01'";
            return getListItemList(selectStmt, includeDescription);

        }

        //For DropDownLists Items, etc.
        public List<ListItem> getListItemList(String selectStmt, bool includeDescription)
        {
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

               SqlDataReader reader = cmdRead.ExecuteReader();
               while (reader.Read())
               {              
                   //ListItem(Label (code) + (description), Value (item id))
                   if(!includeDescription)
                   list.Add(new ListItem(""+reader.GetValue(0), ""+reader.GetValue(1)));
                   else
                       list.Add(new ListItem("" + reader.GetValue(0) + "   (" + reader.GetValue(2)+")", "" + reader.GetValue(1)));
               }

              reader.Dispose();
              reader.Close();

            conn.Close();

            return list;

        }

        public DataTable getAuthorizedUserCollection(string workcenter, bool hideAdmin)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string hide = null;

            if (hideAdmin)
                hide = " AND role <> 'ADMIN' "; //show only SUPPLY OFFICER

            string selectStmt = "select l.*, w.WID, w.description as Workcenter "
                + " from  v_authorized_user_roles l  LEFT JOIN workcenter w ON (l.workcenter_id=w.workcenter_id)  "
                + "  WHERE 1=1 " + hide;


            string whereStmt = "";

            //bool addFilter = false;
            bool addWorkcenter = false;


            string filterStmt = "";
            
            string wcStmt = "";
            if (!workcenter.Equals(""))
            {

                wcStmt = " l.workcenter_id = @workcenter_id";

                //if (addFilter)
                    wcStmt = " AND " + wcStmt;
                //else
                //    wcStmt = " WHERE " + wcStmt;

                addWorkcenter = true;

            }

            whereStmt = filterStmt + wcStmt;

            selectStmt += whereStmt + " ORDER BY l.lastname ";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            
            if (addWorkcenter)
                cmd.Parameters.AddWithValue("@workcenter_id", workcenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public int findNewlyIssuedLocationForWorkcenter(int workcenter_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select location_id from locations WHERE name='NEWLY ISSUED' AND workcenter_id=@workcenter_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            cmd.Parameters.AddWithValue("@workcenter_id", workcenter_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            int location_id = 0;

            if (dt.Rows.Count > 0)
            {
                location_id = Int32.Parse(dt.Rows[0]["location_id"].ToString());
            }

            return location_id;

        }


        public DataTable getInsurvLocationCollectionForAtmTags(string filterValue, string workcenter, string location_done)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select l.name, l.location_id, w.*, cr.action_complete, cr.surv_name, cr.location_done, cr.insurv_location_cr_id " + 
                                "from locations l inner join " +
                                "workcenter w ON (l.workcenter_id=w.workcenter_id)left join " +
                                "insurv_location_cr cr ON (cr.location_id=l.location_id and cr.surv_name='TAGS')   WHERE l.name<>'NEWLY ISSUED' " +
                                "AND l.location_id in " +
                                "(select i.location_id from inventory i where i.mfg_catalog_id in " +
                                "(select m.mfg_catalog_id from mfg_catalog m where m.niin_catalog_id in " +
                                "(select nn.niin_catalog_id from niin_catalog nn where nn.usage_category_id in " +
                                "(select u.usage_category_id from usage_category u where u.category in ('L','R','X'))))) ";
                               
            
            string whereStmt = "";

            bool addFilter = false;
            bool addWorkcenter = false;
            bool addDoneStatus = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                filterStmt = " AND l.name like '%' + @filter + '%' ";

                addFilter = true;
            }
            string wcStmt = "";
            if (!workcenter.Equals(""))
            {

                wcStmt = " l.workcenter_id = @workcenter_id";

                if (addFilter)
                    wcStmt = " AND " + wcStmt;
                else
                    wcStmt = " AND " + wcStmt;

                addWorkcenter = true;

            }
            string doneStmt = "";
            if (!location_done.Equals(""))
            {

                doneStmt = " AND cr.location_done = @location_done";

                if (location_done.Equals("DONE"))
                {
                    location_done = "1";
                    addDoneStatus = true;
                }
                else
                    doneStmt = " AND cr.location_done IS NULL";

            }


            whereStmt = filterStmt + wcStmt + doneStmt;

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
         

            if (addFilter)
                cmd.Parameters.AddWithValue("@filter", dbNull(filterValue));
            if (addWorkcenter)
                cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter));
            if (addDoneStatus)
                cmd.Parameters.AddWithValue("@location_done", dbNull(location_done));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getInsurvLocationCollection(string filterValue, string workcenter, string surv_name, string location_done)
        {            

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select l.name, l.location_id, w.*, cr.action_complete, cr.surv_name, cr.location_done, cr.insurv_location_cr_id "+
" from locations l inner join "+
" workcenter w ON (l.workcenter_id=w.workcenter_id)left join "+
" insurv_location_cr cr ON (cr.location_id=l.location_id AND cr.surv_name=@surv_name3)   WHERE l.name<>'NEWLY ISSUED' AND (cr.surv_name=@surv_name OR cr.surv_name IS NULL) "
//only show locations that HAVE 'BROMINE', etc.
+ "  AND  l.location_id IN (Select distinct location_id from "+
" inventory i inner join mfg_catalog m ON (m.mfg_catalog_id=i.mfg_catalog_id) "+
" inner join insurv_niin s ON (s.niin_catalog_id=m.niin_catalog_id) WHERE s.surv_name=@surv_name2) "
;


            string whereStmt = "";

            bool addFilter = false;
            bool addWorkcenter = false;
            bool addDoneStatus = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                filterStmt = " AND l.name like '%' + @filter + '%' ";

                addFilter = true;
            }
            string wcStmt = "";
            if (!workcenter.Equals(""))
            {

                wcStmt = " l.workcenter_id = @workcenter_id";

                if (addFilter)
                    wcStmt = " AND " + wcStmt;
                else
                    wcStmt = " AND " + wcStmt;

                addWorkcenter = true;

            }
            string doneStmt = "";
            if (!location_done.Equals(""))
            {

                doneStmt = " AND cr.location_done = @location_done";

                if (location_done.Equals("DONE"))
                {
                    location_done = "1";
                    addDoneStatus = true;
                }
                else
                    doneStmt = " AND cr.location_done IS NULL";

            }
            

            whereStmt = filterStmt + wcStmt + doneStmt;

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            cmd.Parameters.AddWithValue("@surv_name", dbNull(surv_name));
            cmd.Parameters.AddWithValue("@surv_name2", dbNull(surv_name));
            cmd.Parameters.AddWithValue("@surv_name3", dbNull(surv_name));

            if (addFilter)
                cmd.Parameters.AddWithValue("@filter",  dbNull(filterValue));
            if (addWorkcenter)
                cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter));
            if (addDoneStatus)
                cmd.Parameters.AddWithValue("@location_done", dbNull(location_done));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getLocationCollection(string filterValue, string workcenter)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            
            string selectStmt = "select l.location_id, l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter "
                + " from  locations l  LEFT JOIN workcenter w ON (l.workcenter_id=w.workcenter_id)  "
                + "  WHERE l.name<>'NEWLY ISSUED'";

           
            string whereStmt = "";

            bool addFilter = false;
            bool addWorkcenter = false;
            

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                filterStmt = " AND l.name like '%' + @filter + '%' ";
                
                addFilter = true;
            }
            string wcStmt = "";
            if (!workcenter.Equals(""))
            {

                wcStmt = " l.workcenter_id = @workcenter_id";

                if (addFilter)
                    wcStmt = " AND " + wcStmt;
                else
                    wcStmt = " AND " + wcStmt;

                addWorkcenter = true;

            }            

            whereStmt = filterStmt + wcStmt;

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if(addFilter)
                cmd.Parameters.AddWithValue("@filter", filterValue);
            if(addWorkcenter)
                cmd.Parameters.AddWithValue("@workcenter_id", workcenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public LocationRecord getLocationRecord(int location_id) {
            LocationRecord location = new LocationRecord();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "SELECT loc.*, wc.WID, wc.Description from Locations loc " +
                "JOIN Workcenter wc ON wc.workcenter_id = loc.workcenter_id " + 
                "WHERE location_id = @location_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("location_id", location_id);

            try {
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read()) {
                    location.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                    location.Location_Name = rdr["name"].ToString();
                    location.WID = rdr["WID"].ToString();
                    location.Workcenter = rdr["description"].ToString();
                    location.WorkCenter_ID = Convert.ToInt32(rdr["workcenter_id"].ToString());
                }
            }
            catch (Exception ex) {
                Debug.WriteLine("ERROR - getLocationRecord\n\t" + ex.Message); 
            }
            finally {
                conn.Close();
            }

            return location;
        }

        public void updateLocationRecord(int location_id, String locationName,
            int workcenter_id) {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string updateStmt = "UPDATE locations " +
                "SET [name]=@locationName, workcenter_id=@workcenter_id " +
                "WHERE location_id=@location_id";

            SqlCommand cmd = new SqlCommand(updateStmt, conn);

            cmd.Parameters.AddWithValue("@locationName", dbNull(locationName));
            cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));
            cmd.Parameters.AddWithValue("@location_id", dbFKeyNull(location_id));

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public DataTable getArchivedList()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from archived ORDER BY date_archived desc";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getGarbageItems()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from garbage_items WHERE deleted_flag IS NULL " ;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getCosalNiinAllowances(string whereColumn, string filterValue)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                filterStmt = " WHERE "+whereColumn+" like '%' + @filter + '%' ";
                
            }
            string selectStmt = "select * from cosal_niin_allowances " + filterStmt + " order by niin, cosal ";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (!filterValue.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", dbNull(filterValue));
            }
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getGarbageOffloadCrList()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from garbage_offload_cr";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getGarbageTopViewList(string filterColumn, string filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                where = " AND  " + filterColumn + " like '%' + @filter + '%' ";
            }

            string selectStmt = "select * from v_garbage_list_top WHERE (deleted is null OR deleted=0 ) AND archived_id IS NULL " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getArchivedGarbageTopViewList(string filterColumn, string filterText, int archived_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                where = " AND  " + filterColumn + " like '%' + @filter + '%' ";
            }

            string selectStmt = "select * from v_garbage_list_top WHERE archived_id=@archived_id " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@archived_id", archived_id);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getOffloadTopViewList(string filterColumn, string filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                if (filterColumn.Equals("ATM"))
                {
                    where = " AND niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                            "AND cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter) ";
                }
                else
                {

                    where = " AND  " + filterColumn + " like '%' + @filter + '%' ";
                }

                
            }

            string selectStmt = "select * from v_offload_list_top WHERE (deleted is null OR deleted=0 ) AND archived_id IS NULL " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getArchivedOffloadTopViewList(string filterColumn, string filterText, int archived_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                where = " AND  " + filterColumn + " like '%' + @filter + '%' ";
            }

            string selectStmt = "select * from v_offload_list_top WHERE archived_id=@archived_id " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@archived_id", archived_id);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getOffloadList(string filterColumn, string filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                where = " WHERE  " + filterColumn + " like '%' + @filter + '%' ";
            }

            string selectStmt = "select * from v_offload " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }



        public DataTable getGarbageItemList(string filterColumn, string filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                where = " WHERE  " + filterColumn + " like '%' + @filter + '%' ";
            }

            string selectStmt = "select * from v_garbage_items " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }
        [CoverageExclude]
        public DataTable getReceivingList(string filterColumn, string filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (!filterText.Equals(""))
            {
                where = " WHERE  " + filterColumn + " like '%' + @filter + '%' ";                            
            }

            string selectStmt = "select * from v_receiving " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }
                       
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getMFGCatalogCollection(string filterColumn, string filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if (filterColumn.Equals("Description", StringComparison.OrdinalIgnoreCase))
            {
                filterColumn = "n." + filterColumn;
            }

            if (!filterText.Equals(""))
            {
                where = " WHERE  " + filterColumn + " like '%' + @filter + '%' ";
            }                       

            string selectStmt = "select slc.time_in_months,n.ui, n.niin, n.description, n.shelf_life_code_id, m.*, slc.slc from mfg_catalog m "
                +"INNER JOIN niin_catalog n ON (m.niin_catalog_id=n.niin_catalog_id)"
                +" LEFT JOIN   shelf_life_code slc ON (n.shelf_life_code_id=slc.shelf_life_code_id)   " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (!filterText.Equals(""))
            {
                cmd.Parameters.AddWithValue("@filter", filterText);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getInventoryForSMCLCollection(int niin_catalog_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.atm, i.manufacturer_date, i.cosal, i.at, i.contract_number, " +
                "i.batch_number, i.lot_number, i.inventory_id,i.alternate_ui,i.alternate_um, " +
                "i.qty, i.serial_number, i.bulk_item, i.mfg_catalog_id, i.shelf_life_expiration_date, i.location_id, " +
                "l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter, " +
                "m.cage, m.manufacturer, m.PRODUCT_IDENTITY, m.niin_catalog_id, m.hazard_id" +
                " from inventory i LEFT JOIN locations l ON (i.location_id=l.location_id) " +
                "LEFT JOIN workcenter w ON (l.workcenter_id=w.workcenter_id), mfg_catalog m " +
                " WHERE m.niin_catalog_id = @niin_catalog_id AND i.mfg_catalog_id=m.mfg_catalog_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            cmd.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getMSDSforSMCL(string NIIN, bool filter)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
        


            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, " +
                "PRODUCT_IDENTITY, PARTNO, FSC, NIIN, hcc_id, file_name " +
                "from msds WHERE NIIN=@NIIN ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("NIIN", dbNull(NIIN));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            if (filter) {
                // Go through table and remove duplicate records
                // key is composed of NIIN + MSDSSERNO + CAGE
                String previousKey = "";
                String currentKey = "";
                List<DataRow> rowsToRemove = new List<DataRow>();
                foreach (DataRow row in dt.Rows) {
                    currentKey = row.ItemArray[7].ToString() + row.ItemArray[1].ToString()
                        + row.ItemArray[2].ToString();
                    if (currentKey.ToUpper().Equals(previousKey.ToUpper())) {
                        rowsToRemove.Add(row);
                    }
                    else {
                        previousKey = currentKey;
                    }
                }

                // Remove the duplicate rows
                foreach (var dr in rowsToRemove) {
                    dt.Rows.Remove(dr);
                }

            }

            return dt;

        }
//TODO - ATM
        public DataTable getATMforNiinATM(string niin, string atmosphere_control_number)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string selectStmt = "select * from atmosphere_control WHERE niin=@niin"
                +" AND atmosphere_control_number=@atmosphere_control_number";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("niin", dbNull(niin));
            //cmd.Parameters.AddWithValue("cage", dbNull(cage));
            cmd.Parameters.AddWithValue("atmosphere_control_number", dbNull(atmosphere_control_number));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }
//ATM
        public DataTable getATMforNiinCageExpDateMsds(string niin, string cage, DateTime? shelf_life, string msds_serial_number)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string where = "";

            if(shelf_life!=null)
                where+= " AND shelf_life=@shelf_life ";
            else
                where += " AND shelf_life IS NULL ";

            if (msds_serial_number != null)
                where += " AND msds_serial_number=@msds_serial_number ";
            else
                where += " AND msds_serial_number IS NULL ";


            string selectStmt = "select * from atmosphere_control WHERE niin=@niin"
                + " AND cage=@cage  " + where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("niin", dbNull(niin));
            cmd.Parameters.AddWithValue("cage", dbNull(cage));

            if (shelf_life != null)
            cmd.Parameters.AddWithValue("shelf_life", dbNull(shelf_life));

            if (msds_serial_number != null)
            cmd.Parameters.AddWithValue("msds_serial_number", dbNull(msds_serial_number));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

 public ATMPrintInfo getAtmInfoByAtm(string atmosphere_control_number)
        {
            ATMPrintInfo atmInfo = null;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from atmosphere_control WHERE "
                + " atmosphere_control_number=@atmosphere_control_number";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            
            cmd.Parameters.AddWithValue("atmosphere_control_number", dbNull(atmosphere_control_number));

            SqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                atmInfo = new ATMPrintInfo();
                atmInfo.ControlNumber = int.Parse(atmosphere_control_number);
                atmInfo.Cage = Convert.ToString(rdr["cage"]);
                atmInfo.Msds = Convert.ToString(rdr["msds_serial_number"]);
                atmInfo.Niin = Convert.ToString(rdr["niin"]);
                try
                {
                    DateTime dt = (DateTime)rdr["shelf_life"];
                    atmInfo.Date = dt.ToShortDateString();
                }
                catch {}
                
            }

            conn.Close();

            return atmInfo;

        }

//ATM
 public void updateAtmosphereControl(string atmosphere_control_number, string cage, string msds_serial_number, DateTime? shelf_life)
 {

     SqlConnection conn = new SqlConnection(MDF_CONNECTION);

     conn.Open();

     string selectStmt = "UPDATE atmosphere_control set cage=@cage,"
         + " msds_serial_number=@msds_serial_number, shelf_life=@shelf_life"
         + " WHERE atmosphere_control_number=@atmosphere_control_number";

     SqlCommand cmd = new SqlCommand(selectStmt, conn);
     cmd.Parameters.AddWithValue("msds_serial_number", dbNull(msds_serial_number));
     cmd.Parameters.AddWithValue("shelf_life", dbNull(shelf_life));
     cmd.Parameters.AddWithValue("cage", dbNull(cage));
     cmd.Parameters.AddWithValue("atmosphere_control_number", dbNull(atmosphere_control_number));

     cmd.ExecuteNonQuery();

     conn.Close();

 }

 public void updateAtmosphereControl(string atmosphere_control_number, string cage, string niin, string msds_serial_number, DateTime? shelf_life)
 {

     SqlConnection conn = new SqlConnection(MDF_CONNECTION);

     conn.Open();

     string selectStmt = "UPDATE atmosphere_control set cage=@cage,"
         + " msds_serial_number=@msds_serial_number, shelf_life=@shelf_life, niin=@niin"
         + " WHERE atmosphere_control_number=@atmosphere_control_number";

     SqlCommand cmd = new SqlCommand(selectStmt, conn);
     cmd.Parameters.AddWithValue("msds_serial_number", dbNull(msds_serial_number));
     cmd.Parameters.AddWithValue("shelf_life", dbNull(shelf_life));
     cmd.Parameters.AddWithValue("cage", dbNull(cage));
     cmd.Parameters.AddWithValue("niin", dbNull(niin));
     cmd.Parameters.AddWithValue("atmosphere_control_number", dbNull(atmosphere_control_number));

     cmd.ExecuteNonQuery();

     conn.Close();

 }


        public void updateInventoryForATM(int inventory_id, string msds_serial_number, DateTime? shelf_life, string atm)
        {
            
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "UPDATE inventory set atm=@atm, serial_number=@msds_serial_number, shelf_life_expiration_date=@shelf_life"
                + " WHERE inventory_id=@inventory_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_serial_number", dbNull(msds_serial_number));
            cmd.Parameters.AddWithValue("shelf_life", dbNull(shelf_life));
            cmd.Parameters.AddWithValue("atm", dbNull(atm));
            cmd.Parameters.AddWithValue("inventory_id", dbNull(inventory_id));

            cmd.ExecuteNonQuery();

            conn.Close();

        }
//TODO - ATM
        public void updateInventoryCrForATM(int inventory_audit_cr_id, string msds_serial_number, DateTime? shelf_life, string atm)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "UPDATE inventory_audit_cr set atm=@atm, serial_number=@msds_serial_number, shelf_life_expiration_date=@shelf_life"
                + " WHERE inventory_audit_cr_id=@inventory_audit_cr_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_serial_number", dbNull(msds_serial_number));
            cmd.Parameters.AddWithValue("shelf_life", dbNull(shelf_life));
            cmd.Parameters.AddWithValue("atm", dbNull(atm));
            cmd.Parameters.AddWithValue("inventory_audit_cr_id", dbNull(inventory_audit_cr_id));

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public DataTable getMSDSforNiinCageContractSerno(string NIIN, string CAGE, string CT_NUMBER, string MSDSSERNO, bool filter)
        {
            string where = " WHERE NIIN=@NIIN ";

            if (MSDSSERNO != null && !MSDSSERNO.Equals(""))
                where += " AND MSDSSERNO LIKE @MSDSSERNO ";
            else
            {
                if (CAGE != null && !CAGE.Equals(""))
                    where += " AND CAGE LIKE @CAGE ";

                if (CT_NUMBER != null && !CT_NUMBER.Equals(""))
                    where += " AND CT_NUMBER LIKE @CT_NUMBER ";

            }

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, CT_NUMBER, PRODUCT_IDENTITY, PARTNO, FSC, NIIN, hcc_id, file_name"
                +" from v_msds_contractor " + where + " ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("NIIN", dbNull(NIIN));

            if (MSDSSERNO != null && !MSDSSERNO.Equals(""))
                cmd.Parameters.AddWithValue("MSDSSERNO", dbNull(MSDSSERNO+"%"));
            else
            {
                if (CAGE != null && !CAGE.Equals(""))
                    cmd.Parameters.AddWithValue("CAGE", dbNull(CAGE + "%"));

                if (CT_NUMBER != null && !CT_NUMBER.Equals(""))
                    cmd.Parameters.AddWithValue("CT_NUMBER", dbNull(CT_NUMBER + "%"));

            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            if (filter) {

            // Go through table and remove duplicate records
            // key is composed of NIIN + MSDSSERNO + CAGE + CT_NUMBER
            String previousKey = "";
            String currentKey = "";
            List<DataRow> rowsToRemove = new List<DataRow>();
            foreach (DataRow row in dt.Rows) {
                currentKey = row.ItemArray[8].ToString() + row.ItemArray[1].ToString()
                    + row.ItemArray[2].ToString() + row.ItemArray[4].ToString();
                if (currentKey.ToUpper().Equals(previousKey.ToUpper())) {
                    rowsToRemove.Add(row);
                }
                else {
                    previousKey = currentKey;
                }
            }

            // Remove the duplicate rows
            foreach (var dr in rowsToRemove) {
                dt.Rows.Remove(dr);
            }
            }

            return dt;

        }


        public DataTable getMSDSforSerialNumber(string msdsserno, bool filter)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, " +
                "PRODUCT_IDENTITY, PARTNO, FSC, NIIN, hcc_id, file_name " +
                "from msds WHERE MSDSSERNO=@MSDSSERNO ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("MSDSSERNO", dbNull(msdsserno));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            if (filter) {
                // Go through table and remove duplicate records
                // key is composed of NIIN + MSDSSERNO + CAGE
                String previousKey = "";
                String currentKey = "";
                List<DataRow> rowsToRemove = new List<DataRow>();
                foreach (DataRow row in dt.Rows) {
                    currentKey = row.ItemArray[7].ToString() + row.ItemArray[1].ToString()
                        + row.ItemArray[2].ToString();
                    if (currentKey.ToUpper().Equals(previousKey.ToUpper())) {
                        rowsToRemove.Add(row);
                    }
                    else {
                        previousKey = currentKey;
                    }
                }

                // Remove the duplicate rows
                foreach (var dr in rowsToRemove) {
                    dt.Rows.Remove(dr);
                }

            }

            conn.Close();

            return dt;

        }

        public DataTable getMSDSList(String whereColumn, String filterValue, 
                String usageCategory, String catalogGroup, String manuallyEnteredOnly) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "SELECT * FROM vMSDSList vm WHERE vm.msds_id in " +
                    "(SELECT MAX(msds_id) FROM msds WHERE MSDSSERNO = vm.MSDSSERNO AND CAGE = vm.CAGE) "; 

            bool addFilter = false;
            bool addUsageCategory = false;
            bool addCatalogGroup = false;

            string filterStmt = "";
            if (!filterValue.Equals("")) {
                if (whereColumn.Equals("SPECS")) {
                    filterStmt = " AND vm.niin IN (SELECT niin FROM niin_catalog " +
                        "WHERE specs LIKE '%' + @filter + '%') ";
                }
                else {

                    filterStmt = " AND  vm." + whereColumn + " LIKE '%' + @filter + '%' ";
                }
                addFilter = true;
            }
            string usageStmt = "";
            if (!usageCategory.Equals("")) {

                usageStmt = "AND vm.niin in (SELECT niin FROM niin_catalog " +
                        "WHERE  usage_category_id = @usageCategory) ";

                addUsageCategory = true;
            }
            string groupStmt = "";
            if (!catalogGroup.Equals("")) {

                groupStmt = "AND vm.niin in (SELECT niin FROM niin_catalog " +
                        "WHERE catalog_group_id = @catalogGroup) ";

                addCatalogGroup = true;
            }
            string manuallyEnteredOnlyStmt = "";
            if (!manuallyEnteredOnly.Equals("")) {

                usageStmt = "AND vm.manually_entered = 1 ";
            }

            selectStmt += filterStmt + usageStmt + groupStmt + manuallyEnteredOnlyStmt;

            selectStmt += "ORDER BY msdsserno, CAGE, msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addUsageCategory)
                cmd.Parameters.Add("@usageCategory", SqlDbType.Int).Value = Int32.Parse(usageCategory);
            if (addCatalogGroup)
                cmd.Parameters.Add("@catalogGroup", SqlDbType.Int).Value = Int32.Parse(catalogGroup);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public bool isAuthorizedForWorkcenter(string username, int workcenter_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from authorized_users WHERE deleted=0 AND username=@username AND workcenter_id=@workcenter_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("username", dbNull(username));
            cmd.Parameters.AddWithValue("workcenter_id", dbNull(workcenter_id));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt.Rows.Count > 0;
        }


        public DataTable doesMSDSExistforMSDSERNO(string msdsserno)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string selectStmt = "select manually_entered from msds WHERE MSDSSERNO=@msdsserno";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msdsserno", dbNull(msdsserno));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public string getSlcByNiinId(int niinCatId)
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select s.slc from shelf_life_code s where s.shelf_life_code_id = (select n.shelf_life_code_id from niin_catalog n where n.niin_catalog_id = @niinCatId)";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@niinCatId", niinCatId);

            string slc = null;

            try
            {
                slc = Convert.ToString(cmd.ExecuteScalar());
            }
            catch
            {}

            con.Close();

            return slc;
        }

        public DataTable getFilteredInventoryCollectionForINSURVAtmTag(String whereColumn, String filterValue, String location)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.contract_number, i.batch_number, i.lot_number, i.alternate_ui, i.alternate_um, i.inventory_id, i.qty, i.serial_number, i.bulk_item, i.mfg_catalog_id, i.manufacturer_date, i.shelf_life_expiration_date, i.location_id, l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter, mfg.cage, mfg.manufacturer, mfg.PRODUCT_IDENTITY, mfg.niin_catalog_id, mfg.hazard_id, nc.ui, nc.um, nc.spmig, nc.description, nc.niin, nc.shelf_life_code_id " +

            " from inventory i, "
            + "locations l, "
            + "mfg_catalog mfg, "
            + "niin_catalog nc, "
            + "workcenter w";        

            string whereStmt = " where i.location_id = l.location_id AND i.mfg_catalog_id = mfg.mfg_catalog_id AND mfg.niin_catalog_id = nc.niin_catalog_id"
                + " and l.workcenter_id = w.workcenter_id AND nc.niin_catalog_id in(select nn.niin_catalog_id from niin_catalog nn where nn.usage_category_id in(select u.usage_category_id from usage_category u where u.category in ('L','R','X')))";

            bool addFilter = false;
            bool addLocation = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {
                if (whereColumn.Equals("ATM"))
                {
                    filterStmt = " AND nc.niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                            "AND mfg.cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter) ";
                }
               
                else if (!whereColumn.Equals("CAGE") && !whereColumn.Equals("MANUFACTURER"))
                {
                    filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                }
                else
                {
                    filterStmt = " AND mfg.mfg_catalog_id IN " +
                    "(SELECT mfg.mfg_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

                locationStmt = " i.location_id = @location";
                //locationStmt = " l.name = @location";

                if (addFilter)
                    locationStmt = " AND " + locationStmt;
                else
                    locationStmt = " AND " + locationStmt;

                addLocation = true;
            }

            whereStmt += (filterStmt + locationStmt);

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = Int32.Parse(location);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }


        public DataTable getFilteredInventoryCollectionForINSURVAudit(String whereColumn, String filterValue, String location, string surv_name)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.contract_number, i.batch_number, i.lot_number, i.alternate_ui, i.alternate_um, i.inventory_id, i.qty, i.serial_number, i.bulk_item, i.mfg_catalog_id, i.manufacturer_date, i.shelf_life_expiration_date, i.location_id, l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter, mfg.cage, mfg.manufacturer, mfg.PRODUCT_IDENTITY, mfg.niin_catalog_id, mfg.hazard_id, nc.ui, nc.um, nc.spmig, nc.description, nc.niin, nc.shelf_life_code_id " +

            " from inventory i, "
            + "locations l, "
            + "mfg_catalog mfg, "
            + "niin_catalog nc, "
            + "workcenter w, " 
            +"insurv_niin n";

            string whereStmt = " where i.location_id = l.location_id AND i.mfg_catalog_id = mfg.mfg_catalog_id AND mfg.niin_catalog_id = nc.niin_catalog_id"
                + " and l.workcenter_id = w.workcenter_id AND n.niin_catalog_id = nc.niin_catalog_id AND n.surv_name = @name";

            bool addFilter = false;
            bool addLocation = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {
                if (!whereColumn.Equals("CAGE") && !whereColumn.Equals("MANUFACTURER"))
                {
                    filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                }
                else
                {
                    filterStmt = " AND mfg.mfg_catalog_id IN " +
                    "(SELECT mfg.mfg_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

                 locationStmt = " i.location_id = @location";
                //locationStmt = " l.name = @location";

                if (addFilter)
                    locationStmt = " AND " + locationStmt;
                else
                    locationStmt = " AND " + locationStmt;

                addLocation = true;

            }
            //string workcenterStmt = "";
            //if (!workcenter.Equals(""))
            //{

            //    workcenterStmt = " l.workcenter_id = @workcenter";

            //    if (addLocation)
            //        workcenterStmt = " AND " + workcenterStmt;
            //    else if (addFilter && !addLocation)
            //        workcenterStmt = " AND " + workcenterStmt;
            //    else
            //        workcenterStmt = " AND " + workcenterStmt;

            //    addWorkcenter = true;

            //}

            whereStmt += (filterStmt + locationStmt );

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = Int32.Parse(location);
            //if (addWorkcenter)
            //    cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workcenter);

            DataTable dt = new DataTable();

            cmd.Parameters.AddWithValue("@name", surv_name);

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }



        public DataTable getFilteredInventoryCollection(String whereColumn, String filterValue, 
                String location, String workcenter) {
            return getFilteredInventoryCollection(whereColumn, filterValue, location, workcenter, null);
        }
 
        public DataTable getFilteredInventoryCollection(String whereColumn, String filterValue, 
                String location, String workcenter, String cosal) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                   @"SELECT   
                           a.allowance_qty, 
                           i.AT, 
                           i.atm, 
                           i.cosal, 
                           i.comments,
                           i.contract_number,                             
                           i.batch_number, 
                           i.lot_number, 
                           i.alternate_ui, 
                           i.alternate_um, 
                           i.inventory_id, 
                           i.qty, 
                           i.serial_number, 
                           i.bulk_item, 
                           i.mfg_catalog_id, 
                           i.manufacturer_date, 
                           i.shelf_life_expiration_date, 
                           i.location_id, 
                           l.name                                         AS location_name, 
                           l.workcenter_id, 
                           w.wid, 
                           w.DESCRIPTION                                  AS workcenter, 
                           mfg.cage, 
                           mfg.manufacturer, 
                           mfg.product_identity, 
                           mfg.niin_catalog_id, 
                           mfg.hazard_id, 
                           nc.ui, 
                           nc.um, 
                           nc.spmig, 
                           nc.DESCRIPTION, 
                           nc.niin, 
                           nc.shelf_life_code_id, 
                           nc.manually_entered, 
                           nc.dropped_in_error, 
                           hcc.hcc, 
                           msds.product_identity                          AS msds_product_identity, 
                           msds.msds_id,
                           nc.usage_category_id, 
                           u.DESCRIPTION                                  AS usage_category, 
                           (SELECT MAX(action_complete) 
                            FROM   inventory_audit_cr 
                            WHERE  location_id = i.location_id 
                                   AND mfg_catalog_id = i.mfg_catalog_id) AS count_date,
                           smcc.smcc
                    FROM   
                           inventory i 
                           INNER JOIN locations l 
                             ON( i.location_id = l.location_id ) 
                           INNER JOIN mfg_catalog mfg 
                             ON ( i.mfg_catalog_id = mfg.mfg_catalog_id ) 
                           INNER JOIN niin_catalog nc 
                             ON ( mfg.niin_catalog_id = nc.niin_catalog_id ) 
                           INNER JOIN workcenter w 
                             ON ( l.workcenter_id = w.workcenter_id ) 
                           LEFT JOIN cosal_niin_allowances a 
                             ON ( i.cosal = a.cosal 
                                  AND nc.niin = a.niin ) 
                           LEFT JOIN msds 
                             ON ( i.serial_number = msds.msdsserno ) 
                           LEFT JOIN hcc 
                             ON ( msds.hcc_id = hcc.hcc_id ) 
                           LEFT JOIN usage_category u 
                             ON ( u.usage_category_id = nc.usage_category_id )
                           LEFT JOIN smcc
                             ON ( smcc.smcc_id = nc.smcc_id )
                    ";

            string whereStmt = " where  i.inventory_id=i.inventory_id "
                + " AND (msds.msds_id = (select max(msds_id) from msds where msds.MSDSSERNO = i.serial_number) OR msds.msds_id IS NULL)"
                + "  ";

            // Filter by COSAL
            if (cosal != null)
                whereStmt += " AND a.cosal = @cosal ";

            bool addFilter = false;
            bool addLocation = false;
            bool addWorkcenter = false;

            string filterStmt = "";
            if (filterValue != null && filterValue.Length > 0)
            {
                if (whereColumn.Equals("ATM"))
                {
                    filterStmt = " and i.atm = @filter";

                    //filterStmt = " AND mfg.cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                     //             "AND nc.niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter)";
                }
                else if (whereColumn.Equals("COSAL"))
                {
                    filterStmt = " AND  i." + whereColumn + " like '%' + @filter + '%' ";
                }
                else if (whereColumn.Equals("inventory_id"))
                {
                    filterStmt = " AND i.inventory_id = @filter ";
                }
                else if (!whereColumn.Equals("CAGE") && !whereColumn.Equals("MANUFACTURER"))
                {
                    filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                }                
                else
                {
                    filterStmt = " AND mfg.mfg_catalog_id IN " +
                    "(SELECT mfg.mfg_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            string locationStmt = "";
            if (location != null && location.Length > 0)
            {

               // locationStmt = " i.location_id = @location";
                locationStmt = " l.name = @location";

                if (addFilter)
                    locationStmt = " AND " + locationStmt;
                else
                    locationStmt = " AND " + locationStmt;

                addLocation = true;

            }
            string workcenterStmt = "";
            if (workcenter != null && workcenter.Length > 0)
            {

                workcenterStmt = " l.workcenter_id = @workcenter";

                if (addLocation)
                    workcenterStmt = " AND " + workcenterStmt;
                else if (addFilter && !addLocation)
                    workcenterStmt = " AND " + workcenterStmt;
                else
                    workcenterStmt = " AND " + workcenterStmt;

                addWorkcenter = true;

            }

            whereStmt += (filterStmt + locationStmt + workcenterStmt);

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (cosal != null)
                cmd.Parameters.Add("@cosal", SqlDbType.NVarChar).Value = cosal;

            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location; //Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workcenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        //All inventory only in SS01
        public DataTable getFilteredInventoryCollectionForIssue(String whereColumn, String filterValue)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.cosal, i.atm, i.batch_number, i.lot_number, i.contract_number, i.alternate_ui,i.alternate_um, i.inventory_id, i.qty, i.serial_number, i.bulk_item, i.mfg_catalog_id, i.manufacturer_date, i.shelf_life_expiration_date, i.location_id, l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter, mfg.cage, mfg.manufacturer, mfg.PRODUCT_IDENTITY, mfg.niin_catalog_id, mfg.hazard_id, nc.ui, nc.um, nc.spmig, nc.description, nc.niin, nc.shelf_life_code_id, nc.manually_entered, nc.dropped_in_error " +

            " from inventory i, "
            + "locations l, "
            + "mfg_catalog mfg, "
            + "niin_catalog nc, "
            + "workcenter w";

            string whereStmt = " where i.location_id = l.location_id AND i.mfg_catalog_id = mfg.mfg_catalog_id AND mfg.niin_catalog_id = nc.niin_catalog_id"
                + " and l.workcenter_id = w.workcenter_id AND w.wid = 'SS01' ";

            bool addFilter = false;            

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                if (whereColumn.Equals("ATM"))
                {
                    filterStmt = " AND i.atm = @filter";

                    //filterStmt = " AND nc.niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                    //             "AND mfg.cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter) ";
                }

                else if (!whereColumn.Equals("CAGE") && !whereColumn.Equals("MANUFACTURER"))
                {
                    filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                }
                else
                {
                    filterStmt = " AND mfg.mfg_catalog_id IN " +
                    "(SELECT mfg.mfg_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            
            whereStmt += filterStmt ;

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;           

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getFilteredIssuedCollection(String whereColumn, String filterValue, String workcenter)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.cosal, i.batch_number, i.atm, i.lot_number, i.contract_number, i.alternate_ui,i.alternate_um, i.date_issued, i.issue_id, i.issued_qty, i.mfg_catalog_id, i.manufacturer_date, i.shelf_life_expiration_date, i.workcenter_id, i.authorized_user_id, u.lastname + ', ' + u.firstname as username, w.WID, w.description as Workcenter, mfg.cage, mfg.manufacturer, mfg.PRODUCT_IDENTITY, mfg.niin_catalog_id, mfg.hazard_id, nc.ui,nc.um,  nc.spmig, nc.description, nc.niin, nc.shelf_life_code_id, nc.manually_entered, nc.dropped_in_error " +
            //string selectStmt = "select i.cosal, i.atm, i.batch_number, i.lot_number, i.contract_number, i.alternate_ui,i.alternate_um, i.date_issued, i.issue_id, i.issued_qty, i.mfg_catalog_id, i.manufacturer_date, i.shelf_life_expiration_date, i.workcenter_id, i.authorized_user_id, u.lastname + ', ' + u.firstname as username, w.WID, w.description as Workcenter, mfg.cage, mfg.manufacturer, mfg.PRODUCT_IDENTITY, mfg.niin_catalog_id, mfg.hazard_id, nc.ui,nc.um,  nc.spmig, nc.description, nc.niin, nc.shelf_life_code_id " +

            " from issues i, "
            + "authorized_users u,"
            + "mfg_catalog mfg, "
            + "niin_catalog nc, "
            + "workcenter w";

            string whereStmt = " where i.authorized_user_id=u.authorized_user_id AND  i.mfg_catalog_id = mfg.mfg_catalog_id AND mfg.niin_catalog_id = nc.niin_catalog_id"
                + " and i.workcenter_id = w.workcenter_id";

            bool addFilter = false;
            bool addLocation = false;
            bool addWorkcenter = false;

            string filterStmt = "";
            if (!filterValue.Equals(""))
            {

                if (whereColumn.Equals("ATM"))
                {

                    filterStmt = " and i.atm = @filter";
                  //  filterStmt = " AND nc.niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                   //              "AND mfg.cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter) ";
                }               

                else if (!whereColumn.Equals("CAGE") && !whereColumn.Equals("MANUFACTURER"))
                {
                    filterStmt = " AND  nc." + whereColumn + " like '%' + @filter + '%' ";
                }
                else
                {
                    filterStmt = " AND mfg.mfg_catalog_id IN " +
                    "(SELECT mfg.mfg_catalog_id from mfg_catalog mfg WHERE mfg." + whereColumn + " like '%' + @filter + '%' )";
                }
                addFilter = true;
            }
            string locationStmt = "";
           
            string workcenterStmt = "";
            if (!workcenter.Equals(""))
            {

                workcenterStmt = " AND i.workcenter_id = @workcenter";
                addWorkcenter = true;

            }

            whereStmt += (filterStmt + locationStmt + workcenterStmt);

            selectStmt += whereStmt + "  ORDER BY i.date_issued DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workcenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getSMCLforInventoryCollection(int niin_catalog_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, nc.usage_category_id, u.description as usage_category, u.category as category," +
            " nc.description, nc.smcc_id, sm.smcc as smcc,"
            + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, shc.description as shelf_life_code_description, " +
            "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
            "nc.remarks, nc.storage_type_id, st.type as storage_type, " +
            "nc.cog_id, nc.spmig, nc.nehc_rpt, nc.catalog_group_id, nc.cog_id, cc.cog as cog," +
            "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id," +
            "sfr.sfr_id as sfr" +

            "  from NIIN_Catalog nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
            + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
            + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
            + "LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
            + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
            + " LEFT JOIN cog_codes cc ON (nc.cog_id=cc.cog_id)"
            + " LEFT JOIN sfr ON (sfr.niin_catalog_id=nc.niin_catalog_id)" +

            " WHERE nc.niin_catalog_id = @niin_catalog_id";    

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getUIUM(int niin_catalog_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from niin_catalog WHERE niin_catalog_id=@niin_catalog_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

       



        public DataTable getSMCLforSFR(int niin_catalog_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, nc.usage_category_id, u.description as usage_category, u.category as category," +
            " nc.description, nc.smcc_id, sm.smcc as smcc,"
            + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, " +
            "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
            "nc.remarks, nc.storage_type_id, st.type as storage_type, " +
            "nc.cog_id, nc.spmig, nc.nehc_rpt, nc.catalog_group_id, nc.cog_id, cc.cog as cog," +
            "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id, " +
            "nc.cage, nc.manufacturer" +

            "  from NIIN_Catalog nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
            + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
            + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
            + " LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
            + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
            + " LEFT JOIN cog_codes cc ON (nc.cog_id=cc.cog_id)" +            

            " WHERE nc.niin_catalog_id = @niin_catalog_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }



        public List<KeyValuePair<string, byte[]>> getManuallyEnteredMSDSfiles()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            List<KeyValuePair<string, byte[]>> uploadedFiles = new List<KeyValuePair<string, byte[]>>();

            string selectStmt = "select file_name, msds_file"
                + " from msds WHERE manually_entered=1 AND file_name IS NOT NULL";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmd.ExecuteReader();
            int i = 0;//for unique file names
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "msds_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select msds_translated_filename, MSDS_TRANSLATED "
               + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND msds_translated_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "translated_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select neshap_comp_filename, NESHAP_COMP_CERT "
               + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND neshap_comp_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "neshap_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select other_docs_filename, OTHER_DOCS "
              + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND other_docs_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "other_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select product_sheet_filename, PRODUCT_SHEET "
             + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND product_sheet_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string key = i + "prod_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select transportation_cert_filename, TRANSPORTATION_CERT "
            + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND transportation_cert_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "trans_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select manufacturer_label_filename, MANUFACTURER_LABEL "
            + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND manufacturer_label_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "label_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            conn.Close();

            return uploadedFiles;
        }

        //For csv export
        public DataTable getManuallyEnteredMSDS()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select "

                + "msds.msds_id, ingredients_id, contractor_id, MSDSSERNO as [ Product Serial Number], CAGE as [CAGE (Responsible Party)], MANUFACTURER as [Company Name (Responsible Party)], PARTNO as [PART #], FSC, NIIN, hcc.HCC, "
                + "ARTICLE_IND as [Article Ind.], msds.DESCRIPTION as [Description], EMERGENCY_TEL as [Emergency Response Phone # (Responsible Party)], END_COMP_IND as [End Item Component Ind.], "
                + "END_ITEM_IND as [End Item Ind.], KIT_IND as [Kit Ind.], KIT_PART_IND as [Kit Part Ind.], MANUFACTURER_MSDS_NO as [Manufacturer MSDS #], "
                + "MIXTURE_IND as [Mixture Ind], PRODUCT_IDENTITY as [Product Identity], CONVERT(varchar, product_load_date, 101) as [Product Load Date], PRODUCT_RECORD_STATUS as [Product Record Status],"
                + "PRODUCT_REVISION_NO as [Product Revision #], PROPRIETARY_IND as [Proprietary Indicator], PUBLISHED_IND as [Published Ind], "
                + "PURCHASED_PROD_IND as [Purchased Product Ind], PURE_IND as [Pure Ind], RADIOACTIVE_IND as [Radioactive Ind], SERVICE_AGENCY as [Service/Agency], TRADE_NAME as [Trade Name],"
                + " TRADE_SECRET_IND as [Trade Secret Ind], PRODUCT_IND as [Product Ind.], PRODUCT_LANGUAGE as [Product Language], "
                 + " " 
                + "VAPOR_PRESS as [Vapor Pressure], VAPOR_DENS as [Vapor Density], SPECIFIC_GRAV as [Specific Gravity], VOC_POUNDS_GALLON as [Volatile Organic Compound (lb./g)], VOC_GRAMS_LITER as [Volatile Organic Compound (gm/L)], "
                + "PH as [pH], VISCOSITY as [Viscosity], EVAP_RATE_REF as [Evaporation Rate], SOL_IN_WATER as [Solubility In Water], APP_ODOR as [Appearance Odor Text], PERCENT_VOL_VOLUME as [% Volatiles By Volume],"
                + " AUTOIGNITION_TEMP as [Autoignition Temp. (C)], CARCINOGEN_IND as [Carcinogen Ind.], EPA_ACUTE as [EPA - Acute], "
                + "EPA_CHRONIC as [EPA - Chronic], EPA_FIRE as [EPA - Fire], EPA_PRESSURE as [EPA - Pressure], EPA_REACTIVITY as [EPA - Reactivity], msds_PHYS_CHEMICAL.FLASH_PT_TEMP as [Flash Pt. Temp. (C)], NEUT_AGENT as [Neutralizing Agent Text],"
                + " NFPA_FLAMMABILITY as [NFPA - Flammability], NFPA_HEALTH as [NFPA - Health], NFPA_REACTIVITY as [NFPA - Reactivity], NFPA_SPECIAL as [NFPA - Special], "
                + "OSHA_CARCINOGENS as [OSHA - Carcinogens], OSHA_COMB_LIQUID as [OSHA - Combustion Liquid], OSHA_COMP_GAS as [OSHA - Compressed Gas], OSHA_CORROSIVE as [OSHA - Corrosive], OSHA_EXPLOSIVE as [OSHA - Explosive],"
                + " OSHA_FLAMMABLE as [OSHA - Flammable], OSHA_HIGH_TOXIC as [OSHA - Highly Toxic], OSHA_IRRITANT as [OSHA - Irritant], OSHA_ORG_PEROX as [OSHA - Organic Peroxide], "
                + "OSHA_OTHERLONGTERM as [OSHA - Other/Long Term], OSHA_OXIDIZER as [OSHA - Oxidizer], OSHA_PYRO as [OSHA - Pyrophoric], OSHA_SENSITIZER as [OSHA - Sensitizer], OSHA_TOXIC as [OSHA - Toxic],"
                + " OSHA_UNST_REACT as [OSHA - Unstable Reactive],OSHA_WATER_REACTIVE as [OSHA - Water Reactive], OTHER_SHORT_TERM as [Other/Short Term], PHYS_STATE_CODE as [Physical State Code], VOL_ORG_COMP_WT as [Volatile Organic Compound (wt %)], "
                + " "
                + "CAS as [CAS Number], RTECS_NUM as [RTECS #], RTECS_CODE as [RTECS Code], INGREDIENT_NAME as [Component Ingredient Name], PRCNT as [% Text Value], OSHA_PEL as [OSHA PEL], OSHA_STEL as [OSHA STEL], ACGIH_TLV as [ACGIH TLV], "
                + "ACGIH_STEL as [ACGIH STEL], EPA_REPORT_QTY as [EPA RQ], DOT_REPORT_QTY as [DOT RQ], PRCNT_VOL_VALUE as [% Volume Value], PRCNT_VOL_WEIGHT as [% Weight Value], CHEM_MFG_COMP_NAME as [Chemical Manufacturer Company Name], ODS_IND as [ODS Ind], OTHER_REC_LIMITS as [Other Recorded Limits], "
                + " "
                + "CT_NUMBER as [Contract Number], CT_CAGE as [Contractor CAGE], CT_CITY as [Contractor City], CT_COMPANY_NAME as [Contractor Company Name], CT_COUNTRY as [Contractor Country], CT_PO_BOX as [Contractor P.O. Box], CT_PHONE as [Contractor Telephone Number], "
                + "PURCHASE_ORDER_NO as [Purchase Order Number], CT_STATE as [Contractor State], " 
                + " "
                + "NRC_LP_NUM as [NRC License/ Permit Number], OPERATOR as [Operator], RAD_AMOUNT_MICRO as [Radioactive Amount (Microcuries)], RAD_FORM as [Radioactive Form], RAD_CAS as [Radioisotope CAS],"
                + " RAD_NAME as [Radioisotope Name], RAD_SYMBOL as [Radioisotope Symbol], REP_NSN as [Replacement NSN], SEALED as [Sealed],"
                + " "
                + "AF_MMAC_CODE as [AF MMAC Code], CERTIFICATE_COE as [Certificate of Equivalency (COE)], "
                + "COMPETENT_CAA as [Competent Authority Approval (CAA)], DOD_ID_CODE as [Department of Defense ID Code], DOT_EXEMPTION_NO as [DOT Exemption #],"
                + " DOT_RQ_IND as [DOT RQ Ind.], EX_NO as [EX #], msds_transportation.FLASH_PT_TEMP as [Flash Pt. Temp. ( C )], "
                + "HIGH_EXPLOSIVE_WT as [High Explosive Weight], LTD_QTY_IND as [Ltd Qty Ind.], MAGNETIC_IND as [Magnetic Ind], MAGNETISM as [Magnetism (Magnetic Strength)], MARINE_POLLUTANT_IND as [Marine Pollutant Ind.], NET_EXP_QTY_DIST as [Net Explosive Qty. Distance Weight], "
                + "NET_EXP_WEIGHT as [Net Explosive Weight (kg)], NET_PROPELLANT_WT as [Net Propellant Weight (kg)], "
                + "NOS_TECHNICAL_SHIPPING_NAME as [NOS Technical Shipping Name], TRANSPORTATION_ADDITIONAL_DATA as [Transportation Additional Data],"
                + " "
                + "DOT_HAZARD_CLASS_DIV as [DOT Hazard Class/Div], DOT_HAZARD_LABEL as [DOT Hazard Label],"
                + " DOT_MAX_CARGO as [DOT Max. Quantity: Cargo Aircraft Only], DOT_MAX_PASSENGER as [DOT Max. Quantity: Passenger Aircraft/Rail], DOT_PACK_BULK as [DOT Packaging (Bulk)], DOT_PACK_EXCEPTIONS as [DOT Packaging (Exceptions)], "
                + "DOT_PACK_NONBULK as [DOT Packaging (Non Bulk)], DOT_PROP_SHIP_NAME as [DOT Proper Shipping Name], DOT_PROP_SHIP_MODIFIER as [DOT Proper Shipping Name Modifier], DOT_PSN_CODE as [DOT PSN Code],"
                + " DOT_SPECIAL_PROVISION as [DOT Special Provision], DOT_SYMBOLS as [DOT Symbols], DOT_UN_ID_NUMBER as [DOT UN ID Number], "
                + "DOT_WATER_OTHER_REQ as [DOT Water Shipment: Other Requirements], DOT_WATER_VESSEL_STOW as [DOT Water Shipment: Vessel Stowage], DOT_PACK_GROUP as [DOT Packing Group],"
                + " "
                + "AFJM_HAZARD_CLASS as [AFJM Hazard Class/Div], AFJM_PACK_PARAGRAPH as [AFJM Packaging Paragraph], AFJM_PACK_GROUP as [AFJM Packing Group], AFJM_PROP_SHIP_NAME as [AFJM Proper Shipping Name], AFJM_PROP_SHIP_MODIFIER as [AFJM Proper Shipping Name Modifier], "
                + "AFJM_PSN_CODE as [AFJM PSN Code], AFJM_SPECIAL_PROV as [AFJM Special Provisions], AFJM_SUBSIDIARY_RISK as [AFJM Subsidiary Risk], AFJM_SYMBOLS as [AFJM Symbols], AFJM_UN_ID_NUMBER as [AFJM UN ID Number], "
                + " "
                + "IATA_CARGO_PACKING as [IATA Cargo Packing: Note Cargo Aircraft Packing Instructions], IATA_HAZARD_CLASS as [IATA Hazard Class/Div], IATA_HAZARD_LABEL as [IATA Hazard Label], IATA_PACK_GROUP as [IATA Packing Group], IATA_PASS_AIR_PACK_LMT_INSTR as [IATA Passenger Air Packing: Lmt. Qty Pkg. Instr], "
                + "IATA_PASS_AIR_PACK_LMT_PER_PKG as [IATA Passenger Air Packing: Lmt. Qty. Max Qty. per Pkg], IATA_PASS_AIR_PACK_NOTE as [IATA Passenger Air Packing: Note Passenger Air Packing Instr], IATA_PROP_SHIP_NAME as [IATA Proper Shipping Name], IATA_PROP_SHIP_MODIFIER as [IATA Proper Shipping Name Modifier], IATA_CARGO_PACK_MAX_QTY as [IATA Cargo Packing: Max. Quantity], "
                + "IATA_PSN_CODE as [IATA PSN Code], IATA_PASS_AIR_MAX_QTY as [IATA Passenger Air Packing: Max. Quantity], IATA_SPECIAL_PROV as [IATA Special Provisions], IATA_SUBSIDIARY_RISK as [IATA Subsidiary Risk], IATA_UN_ID_NUMBER as [IATA UN ID Number], "
                + " "
                + "IMO_EMS_NO as [IMO EMS Number], IMO_HAZARD_CLASS as [IMO Hazard Class/Div], IMO_IBC_INSTR as [IMO IBC Instructions], IMO_LIMITED_QTY as [IMO Limited Quantity], IMO_PACK_GROUP as [IMO Packing Group], IMO_PACK_INSTRUCTIONS as [IMO Packing Instructions], IMO_PACK_PROVISIONS as [IMO Packing Provisions], "
                + "IMO_PROP_SHIP_NAME as [IMO Proper Shipping Name], IMO_PROP_SHIP_MODIFIER as [IMO Proper Shipping Name Modifier], IMO_PSN_CODE as [IMO PSN Code], IMO_SPECIAL_PROV as [IMO Special Provisions],"
                + " IMO_STOW_SEGR as [IMO Stowage and Segregation], IMO_SUBSIDIARY_RISK as [IMO Subsidiary Risk Label], IMO_TANK_INSTR_IMO as [IMO Tank Instructions, IMO], IMO_TANK_INSTR_PROV as [IMO Tank Instructions, Provisions], "
                + "IMO_TANK_INSTR_UN as [IMO Tank Instructions, UN], IMO_UN_NUMBER as [IMO UN Number], IMO_IBC_PROVISIONS as [IMO IBC Provisions],"
                + " "
                + "ITEM_MANAGER as [Item Manager], ITEM_NAME as [Item Name], SPECIFICATION_NUMBER as [Specification #], TYPE_GRADE_CLASS as [Specification Type /Grade/Class], UNIT_OF_ISSUE as [Unit Of Issue], "
                + "QUANTITATIVE_EXPRESSION as [Quantitative Expression Exp], UI_CONTAINER_QTY as [Unit of Issue Container Quantity], TYPE_OF_CONTAINER as [Type Of Container], "
                + "BATCH_NUMBER as [Batch Number], LOT_NUMBER as [Lot Number], LOG_FLIS_NIIN_VER as [Logistics FLIS NIIN Verified], LOG_FSC as [Logistics FSC], NET_UNIT_WEIGHT as [Net Unit Weight], "
                + "SHELF_LIFE_CODE as [Shelf Life Code], SPECIAL_EMP_CODE as [Special Emphasis Code], UN_NA_NUMBER as [UN/NA Number], UPC_GTIN as [UPC/GTIN], "
                + " "
                + "COMPANY_CAGE_RP as [Company CAGE (Responsible Party)], COMPANY_NAME_RP as [Company Name (Responsible Party)], LABEL_EMERG_PHONE as [Label Emergency Telephone Number#],"
                + " LABEL_ITEM_NAME as [Label Item Name], LABEL_PROC_YEAR as [Label Procurement Year], LABEL_PROD_IDENT as [Label Product Identity], "
                + "LABEL_PROD_SERIALNO as [Label Product Serial Number], LABEL_SIGNAL_WORD as [Label Signal Word], LABEL_STOCK_NO as [Label Stock Number], SPECIFIC_HAZARDS as [Specific Hazards], "
                + " "
                + "DISPOSAL_ADD_INFO as [Disposal Additional Information], EPA_HAZ_WASTE_CODE as [EPA Hazardous Waste Code], EPA_HAZ_WASTE_IND as [EPA Hazardous Waste Ind], EPA_HAZ_WASTE_NAME as [EPA Hazardous Waste Name], "
                + " "
                + "manufacturer_label_filename as [MANUFACTURER'S LABEL],  file_name as [MATERIAL SAFETY DATA SHEET], msds_translated_filename as [MSDS, TRANSLATED], "
                + "neshap_comp_filename as [NESHAP COMPLIANCE CERTIFICATE], other_docs_filename as [OTHER DOCUMENTS], product_sheet_filename as [PRODUCT SHEET], "
                + "transportation_cert_filename as [TRANSPORTATION CERTIFICATE]  " 
                + " "
                
                + " from msds LEFT JOIN hcc ON (msds.hcc_id=hcc.hcc_id) "               

                  + " LEFT JOIN msds_INGREDIENTS   ON (msds.msds_id=msds_INGREDIENTS.msds_id)"

                  + " LEFT JOIN msds_contractor_info    ON (msds.msds_id=msds_contractor_info.msds_id)"

                  + " LEFT JOIN msds_ITEM_DESCRIPTION    ON (msds.msds_id=msds_ITEM_DESCRIPTION.msds_id)"

                   + " LEFT JOIN msds_PHYS_CHEMICAL     ON (msds.msds_id=msds_PHYS_CHEMICAL.msds_id)"


                 + " LEFT JOIN msds_afjm_psn     ON (msds.msds_id=msds_afjm_psn.msds_id)"
                 
                 + " LEFT JOIN msds_disposal     ON (msds.msds_id=msds_disposal.msds_id)"
                 + " LEFT JOIN msds_document_types     ON (msds.msds_id=msds_document_types.msds_id)"
                 + " LEFT JOIN msds_dot_psn     ON (msds.msds_id=msds_dot_psn.msds_id)"
                 + " LEFT JOIN msds_iata_psn     ON (msds.msds_id=msds_iata_psn.msds_id)"
                 + " LEFT JOIN msds_imo_psn     ON (msds.msds_id=msds_imo_psn.msds_id)"
                 + " LEFT JOIN msds_label_info     ON (msds.msds_id=msds_label_info.msds_id)"
                 + " LEFT JOIN msds_radiological_info     ON (msds.msds_id=msds_radiological_info.msds_id)"
                 + " LEFT JOIN msds_transportation     ON (msds.msds_id=msds_transportation.msds_id)"
                  

                + " WHERE manually_entered=1 AND CAGE<>'1' ORDER BY msds.msds_id";


            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public int findLocationByName(String name)
        {
            int location_id = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select location_id from locations WHERE name=@name";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("name", name);

            object o = cmd.ExecuteScalar();
            if (o != null)
                location_id = Int32.Parse(""+o);

            conn.Close();

            return location_id;
        }

        public Object[] getHazardListsForInventoryId(int inventory_id)
        {
            Object[] hazards = new Object[2];

            List<int> hazardList = new List<int>();
            List<string> hccList = new List<string>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            //string selectStmt = "select hazard_id from mfg_catalog WHERE mfg_catalog_id=@mfg_catalog_id";

            //SqlCommand cmd = new SqlCommand(selectStmt, conn);
            //cmd.Parameters.AddWithValue("mfg_catalog_id", dbNull(mfg_catalog_id));

            //object o = cmd.ExecuteScalar();
            //try
            //{
            //    hazard_id = Int32.Parse("" + o);
            //}
            //catch { }

            

            //if (hazard_id == 0)
            //{

               string selectStmt = "select hazard_id, hcc from V_Top_Hazard_Items WHERE inventory_id=@inventory_id";

                SqlCommand cmd2 = new SqlCommand(selectStmt, conn);
                cmd2.Parameters.AddWithValue("inventory_id", dbNull(inventory_id));
                
                DataTable dt = new DataTable();

                new SqlDataAdapter(cmd2).Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    hazardList.Add(Convert.ToInt32(r["hazard_id"]));
                    hccList.Add(Convert.ToString(r["hcc"]));
                }

                if (hazardList.Count == 0)
                {
                    hazardList.Add(0);
                    hccList.Add("");
                }

            //}

            conn.Close();

            hazards[0] = hazardList;
            hazards[1] = hccList;

            return hazards;
        }

        public Object[] getHazardListsForMSDSERNO(string msdsserno)
        {
            Object[] hazards = new Object[2];

            List<int> hazardList = new List<int>();
            List<string> hccList = new List<string>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select hazard_id, hcc from V_MSDS_HCC_Hazards WHERE msdsserno=@msdsserno";

            SqlCommand cmd2 = new SqlCommand(selectStmt, conn);
            cmd2.Parameters.AddWithValue("msdsserno", dbNull(msdsserno));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd2).Fill(dt);

            foreach (DataRow r in dt.Rows)
            {
                hazardList.Add(Convert.ToInt32(r["hazard_id"]));
                hccList.Add(Convert.ToString(r["hcc"]));
            }

            if (hazardList.Count == 0)
            {
                hazardList.Add(0);
                hccList.Add("");
            }

            conn.Close();

            hazards[0] = hazardList;
            hazards[1] = hccList;

            return hazards;
        }

        public DataTable findInventory(int inventory_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from inventory WHERE inventory_id=@inventory_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("inventory_id", inventory_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }



        public DataTable findInventoryInDecantTask(int inventory_in_decant_task_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select t.*, i.serial_number from inventory_in_decant_task t LEFT JOIN inventory i ON (t.inventory_id=i.inventory_id) WHERE inventory_in_decant_task_id=@inventory_in_decant_task_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("inventory_in_decant_task_id", inventory_in_decant_task_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findSingleInventoryWithAllFields(int inventory_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select  i.*, m.*, l.name, l.description as workcenter  from inventory i INNER JOIN vMfgCatNiinCat m ON(i.mfg_catalog_id=m.mfg_catalog_id) " +
           "  INNER JOIN vLocationWorkcenters l ON (i.location_id=l.location_id)  WHERE inventory_id=@inventory_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("inventory_id", inventory_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

         public DataTable findInvAuditCr(int inventory_audit_cr_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from inventory_audit_cr WHERE inventory_audit_cr_id=@inventory_audit_cr_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("inventory_audit_cr_id", inventory_audit_cr_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }


        public DataTable findInsurvLocationCr(int insurv_location_cr_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from insurv_location_cr WHERE insurv_location_cr_id=@insurv_location_cr_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("insurv_location_cr_id", insurv_location_cr_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findBromineAudit(int bromine_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from bromine_audit WHERE bromine_id=@bromine_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("bromine_id", bromine_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public void executeDatabaseBackup(string fileName) {
            executeDatabaseBackup(null, fileName);
        }
        public void executeDatabaseBackup(string folder, string fileName)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            if (folder == null) {
                folder = DB_BACKUP_DIRECTORY;
            }

            if (!folder.EndsWith("\\") && !folder.EndsWith("/"))
                folder += "\\";
            
            conn.Open();

            string selectStmt = "BACKUP database " + conn.Database + " TO DISK = @filePath;";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@filePath", folder + fileName);
            cmd.CommandTimeout = 0;

            cmd.ExecuteNonQuery();

            conn.Close();

        }


        public DataTable findCalciumAudit(int calcium_hypochlorite_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from calcium_hypochlorite_audit WHERE " +
                "calcium_hypochlorite_id=@calcium_hypochlorite_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("calcium_hypochlorite_id", calcium_hypochlorite_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findTagAudit(int tagId)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from atm_tag_audit WHERE atm_id=@id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", tagId);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findIssue(int issue_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from issues WHERE issue_id=@issue_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("issue_id", issue_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getBromineAuditCounts()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_bromine_audit_counts";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);           

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getCalciumAuditCounts()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_calcium_audit_counts";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getTagsAuditCounts()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_atm_tags_audit_counts";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public int getExistingLocationId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 location_id from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public int getExistingInventoryId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 inventory_id from inventory";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public int getExistingMfgCatalogId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 mfg_catalog_id from mfg_catalog";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public int getExistingInventoryAuditId()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 inventory_audit_id from inventory_audits";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public int getExistingNiinIdWithNoInventory()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 niin_catalog_id from niin_catalog WHERE niin_catalog_id NOT IN (SELECT niin_catalog_id from inventory i inner join mfg_catalog m on(i.mfg_catalog_id=m.mfg_catalog_id)" 
              +") AND niin_catalog_id IN (select niin_catalog_id from mfg_catalog)";
            SqlCommand cmd = new SqlCommand(stmt, con);

            int value = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return value;

        }

        public int? getNiinCatalogIdForNIIN(string NIIN)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 niin_catalog_id from niin_catalog where NIIN=@NIIN";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@NIIN", NIIN);

            int? value = null;

            object o = cmd.ExecuteScalar();

            if (o != null)
                value = Convert.ToInt32(o);

            con.Close();

            return value;

        }

        public int getWorkcenterIdForLocationId(int location_id)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select workcenter_id from locations WHERE location_id=@location_id";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("location_id", dbNull(location_id));

            int locationId = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return locationId;

        }

        public bool alreadyExistsMFGCatalog(int niin_catalog_id, string CAGE)//, string MANUFACTURER, string product_identity)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 mfg_catalog_id from mfg_catalog where niin_catalog_id=@niin_catalog_id AND CAGE=@CAGE "; //AND MANUFACTURER=@MANUFACTURER AND product_identity=@product_identity";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);
            cmd.Parameters.AddWithValue("@CAGE", CAGE);
            //cmd.Parameters.AddWithValue("@MANUFACTURER", MANUFACTURER);
            //cmd.Parameters.AddWithValue("@product_identity", product_identity);

            bool exists = false;

            object o = cmd.ExecuteScalar();

            if (o != null)
                exists = true;

            con.Close();

            return exists;

        }

        public int? getHazardIdForHCC(string HCC)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select hazard_id from hazardous_hcc where hcc=@hcc";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@hcc", HCC);

            int? value = null;

            object o = cmd.ExecuteScalar();
            
            if(o!=null)
                value = Convert.ToInt32(o);

            con.Close();

            return value;

        }

        public int? getHazardIdForHCC(int hcc_id)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select z.hazard_id from hazardous_hcc z LEFT JOIN hcc h ON (z.hcc=h.hcc) WHERE h.hcc_id=@hcc_id";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@hcc_id", hcc_id);

            int? value = null;

            object o = cmd.ExecuteScalar();

            if (o != null)
                value = Convert.ToInt32(o);

            con.Close();

            return value;

        }

        public int getExistingMfgForNiin(int niin_catalog_id)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 mfg_catalog_id from mfg_catalog WHERE niin_catalog_id=@niin_catalog_id";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            int value = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return value;

        }

        public string getExistingNiinWithNoInventory()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 niin from niin_catalog WHERE niin_catalog_id NOT IN (SELECT niin_catalog_id from inventory i inner join mfg_catalog m on(i.mfg_catalog_id=m.mfg_catalog_id)"
              + ") AND niin_catalog_id IN (select niin_catalog_id from mfg_catalog)"; SqlCommand cmd = new SqlCommand(stmt, con);

            string value = ""+cmd.ExecuteScalar();

            con.Close();

            return value;

        }

        public string getExistingLocationName()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 name from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            string location = Convert.ToString(cmd.ExecuteScalar());

            con.Close();

            return location;

        }

        public string getExistingMSDSSERNO()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 MSDSSERNO from V_MSDS_HCC_HAZARDS";
            SqlCommand cmd = new SqlCommand(stmt, con);

            string location = Convert.ToString(cmd.ExecuteScalar());

            con.Close();

            return location;

        }

        public DataTable findNiinCatalog(int niin_catalog_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from niin_catalog WHERE niin_catalog_id=@niin_catalog_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("niin_catalog_id", niin_catalog_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        [CoverageExclude]
        public DataTable findReceiving(int receiving_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from receiving_list WHERE receiving_id=@receiving_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("receiving_id", receiving_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findShipTo(int ship_to_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select uic, organization, plate_title, city, state, zip from ship_to where ship_to_id=@ship_to_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("ship_to_id", ship_to_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findOffloadDetails(int offload_detail_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from offload_details WHERE offload_detail_id=@offload_detail_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("offload_detail_id", offload_detail_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findMSDS(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, NIIN, msds.hcc_id, msds_file, file_name, HCC, "
                +" article_ind, msds.description, emergency_tel, end_comp_ind, end_item_ind, kit_ind, kit_part_ind, manufacturer_msds_no,"
                + "mixture_ind, product_identity, CONVERT(varchar, product_load_date, 101) AS product_load_date, product_record_status, product_revision_no, proprietary_ind, published_ind, purchased_prod_ind,"
                + "pure_ind, radioactive_ind, service_agency, trade_name, trade_secret_ind, product_ind, product_language, "
                + "COALESCE(created, product_load_date) AS last_update, "
                + "(hcc.hcc + ' - ' + hcc.description) AS HCC_Description "
                + " from msds LEFT JOIN hcc ON (msds.hcc_id=hcc.hcc_id) WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDScontractorInfo(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_contractor_info WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSradiologicalInfo(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_radiological_info WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDStransportation(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_transportation WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSdotPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_dot_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSingredients(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_ingredients WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSitemDescription(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_item_description WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSphysChemical(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_phys_chemical WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSafjmPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_afjm_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSiataPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_iata_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSimoPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_imo_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSlabelInfo(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_label_info WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSdisposal(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_disposal WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSdocTypes(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_document_types WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }


        private object dbNull(object o)
        {
            if (o != null && !o.Equals(""))
            {
                return o;
            }
            else
            {
                return DBNull.Value;
            }
        }

        //Taking into account unit tests where we
        //need to pass in a null foreign key relationship id (represented by 0)
        private object dbFKeyNull(int o)
        {
            if (o != 0)
            {
                return o;
            }
            else
            {
                return DBNull.Value;
            }
        }


        //Show all locations for a audit. Done and Not Done
        public DataTable getInventoryAuditLocationSelectionCollection(int audit_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from locations_in_audit a LEFT JOIN vLocationWorkcenters l ON (a.location_id=l.location_id) " +


 " WHERE a.inventory_audit_id=@inventory_audit_id ORDER BY name";

//            string selectStmt = "select * from locations_in_audit a LEFT JOIN vLocationWorkcenters l ON (a.location_id=l.location_id) LEFT JOIN"

// +" location_cr AS cc ON (l.location_id=cc.location_id)"
//+" WHERE (location_cr_id IN"
//                          +" (SELECT     location_cr_id"
//                           +" FROM          dbo.location_cr AS x"
//                           +" WHERE      (action_complete ="+
//                                                       " (SELECT     MAX(action_complete) AS Expr1"+
//                                                       "  FROM          dbo.location_cr"
//                                                        + " WHERE      (location_id = x.location_id  AND inventory_audit_id = @inventory_audit_id)))))" +


// " AND a.inventory_audit_id=@inventory_audit_id AND a.inventory_audit_id = cc.inventory_audit_id ORDER BY name";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("inventory_audit_id", audit_id);
           

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        
        
        public DataTable getInventoryAuditLocationWithDiscrepanciesCollection(int audit_id, int count, string username, bool isWorkcenterUser)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string mainSelect = "";


            if (isWorkcenterUser)
            {
                mainSelect = "select v.location_id as location_id, name, description, wid, location_done, l.count from vLocationWorkCenters v" +
                                    " left outer join vlimited_location_by_count l on l.location_id = v.location_id and l.count = @count and inventory_audit_id = @inventory_audit_id " +
                                    " where (l.count = @count or l.count is null) and v.location_id IN (select location_id from locations_in_audit where inventory_audit_id = @inventory_audit_id) " +
                                    "AND v.location_id in (select l.location_id from locations l where l.workcenter_id in(select u.workcenter_id from authorized_users u where u.username=@username AND u.deleted = @deleted))";

            }
            else
            {
                mainSelect = "select v.location_id as location_id, name, description, wid, location_done, l.count from vLocationWorkCenters v" +
                                   " left outer join vlimited_location_by_count l on l.location_id = v.location_id and l.count = @count and inventory_audit_id = @inventory_audit_id " +
                                   " where (l.count = @count or l.count is null) and v.location_id IN (select location_id from locations_in_audit where inventory_audit_id = @inventory_audit_id)";
                                   
            }

            if (count == 2)
            {
                mainSelect += " and v.location_id not in (select location_id from vDoneLocationsWithoutDiscrepancies where count = 1)";
            }
            if (count == 3)
            {           
                mainSelect += " and v.location_id not in (select location_id from vDoneLocationsWithoutDiscrepancies where count = 2 or count = 1)";
            }

            SqlCommand Cmd = new SqlCommand(mainSelect, conn);
            Cmd.Parameters.AddWithValue("inventory_audit_id", audit_id);
            Cmd.Parameters.AddWithValue("count", count);
            if (isWorkcenterUser)
            {
                Cmd.Parameters.AddWithValue("@username", username);
                Cmd.Parameters.AddWithValue("@deleted", false);
            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(Cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getFilteredAtm(string filterColumn, string filterText)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt = "select * from atmosphere_control";
            
            String filterStmt = "";
            if (!filterText.Equals(""))
            {
                if (filterColumn.Equals("ATM"))
                {
                    filterStmt = " where atmosphere_control_number like '%' + @filter + '%' ";
                    selectStmt = selectStmt + filterStmt;
                }
                else if (filterColumn.Equals("MSDS Serial Number"))
                {
                    filterStmt = " where msds_serial_number like '%' + @filter + '%' ";
                    selectStmt = selectStmt + filterStmt;
                }
                else
                {
                    filterStmt = " where " + filterColumn + " like '%' + @filter + '%' ";
                    selectStmt = selectStmt + filterStmt;

                }

            }
            

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@filter", filterText);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable getFilteredAvailableInventoryAuditCollection(String filterColumn, String filterText)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vMfgCatNiinCat WHERE mfg_catalog_id IS NOT NULL AND mfg_catalog_id > 0 AND niin_catalog_id > 0 ";
            String filterStmt = "";
            if (!filterText.Equals(""))
            {
                if (filterColumn.Equals("ATM"))
                {
                    filterStmt = " AND niin = (select a.niin from atmosphere_control a where a.atmosphere_control_number = @filter) " +
                                 "AND cage = (select a.cage from atmosphere_control a where a.atmosphere_control_number = @filter) ";
                    selectStmt = selectStmt + filterStmt;
                }
                else
                {
                    filterStmt = " AND " + filterColumn + " like '%' + @filter + '%' ";
                    selectStmt = selectStmt + filterStmt;

                }
                 
            }

            SqlCommand cmd = new SqlCommand(selectStmt + " ORDER BY niin_catalog_id, cage", conn);
            cmd.Parameters.AddWithValue("filter", filterText);
            

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getAuditedInventoryAuditCollection(int location_id, int audit_id, int count)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            //string stmt = "SELECT inventory_audit_cr_id, inventory_audit_id, location_id, mfg_catalog_id, qty, bulk_item, " +
            //    "serial_number, shelf_life_expiration_date, current_volume, action_complete, username, device_type " +
            //    "FROM inventory_audit_cr AS cc " +
            //    "WHERE  (inventory_audit_cr_id IN " +
            //    "(SELECT inventory_audit_cr_id " +
            //    "FROM inventory_audit_cr AS x " +
            //    "WHERE (action_complete = (SELECT MAX(action_complete) AS Expr1 " +
            //    "FROM inventory_audit_cr " +
            //    "WHERE (location_id = x.location_id " +
            //    "AND inventory_audit_id = @auditid " +
            //    "AND mfg_catalog_id = @mfg)))))";

            //string selectStmt = "select * from vInventoryAudit WHERE location_id = @location_id AND inventory_audit_id = @audit_id";

            string selectStmt = "select v.*, m.*, cr.manufacturer_date,cr.contract_number, cr.atm from vInventoryAudit v "
                +" INNER JOIN inventory_audit_cr cr ON (v.inventory_audit_cr_id=cr.inventory_audit_cr_id) "
                +"INNER JOIN vMfgCatNiinCat m on v.mfg_catalog_id = m.mfg_catalog_id "
                +" WHERE v.location_id = @location_id AND v.inventory_audit_id = @audit_id "
                +"and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                +"where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)";
            


            //string selectStmt = "select * from vInventoryAudit";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("location_id", location_id);
            cmd.Parameters.AddWithValue("audit_id", audit_id);
            cmd.Parameters.AddWithValue("count", count);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public void deleteLocation(int location_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from locations where location_id=@location_id ";
            
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@location_id", location_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteAuthorizedUser(int authorized_user_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from authorized_users where authorized_user_id=@authorized_user_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@authorized_user_id", authorized_user_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteAdminUser(int user_role_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from user_roles where user_role_id=@user_role_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@user_role_id", user_role_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteCosalNiinAllowance(int cosal_niin_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from cosal_niin_allowances where cosal_niin_id=@cosal_niin_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@cosal_niin_id", cosal_niin_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteInsurvNiin(int insurv_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from insurv_niin where insurv_id=@insurv_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@insurv_id", insurv_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteInsurvLocationChangeRecord(int insurv_location_cr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from insurv_location_cr where insurv_location_cr_id=@insurv_location_cr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@insurv_location_cr_id", insurv_location_cr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteOffload(int offload_list_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from offload_list where offload_list_id=@offload_list_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@offload_list_id", offload_list_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteOffloadDetail(int offload_detail_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from offload_details where offload_detail_id=@offload_detail_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@offload_detail_id", offload_detail_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteIssue(int issue_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from issues where issue_id=@issue_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@issue_id", issue_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteArchived(int archived_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from archived where archived_id=@archived_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@archived_id", archived_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteInventory(int inventory_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from inventory where inventory_id=@inventory_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteApprovedIncompatible(int approved_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from approved_incompatibles where approved_id=@approved_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@approved_id", approved_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public int deleteApprovedIncompatible(int inventory_id1, int inventory_id2,
                int location_id) {
            int result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "DELETE FROM approved_incompatibles "
                + "WHERE location_id = @location_id AND "
                + "((inventory_id1 =  @inventory_id1 and inventory_id2 =  @inventory_id2) "
                + "OR (inventory_id2 =  @inventory_id1 and inventory_id1 =  @inventory_id2))";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_id1", inventory_id1);
            cmd.Parameters.AddWithValue("@inventory_id2", inventory_id2);
            cmd.Parameters.AddWithValue("@location_id", location_id);

            result = Convert.ToInt32(cmd.ExecuteScalar());

            conn.Close();

            return result;
        }

        public void deleteDecantingTask(int decanting_task_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from decanting_tasks where decanting_task_id=@decanting_task_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@decanting_task_id", decanting_task_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteInventoryAuditCr(int inventory_audit_cr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from inventory_audit_cr where inventory_audit_cr_id=@inventory_audit_cr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@inventory_audit_cr_id", inventory_audit_cr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        [CoverageExclude]
        public void deleteLocationCr(int location_cr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from location_cr where location_cr_id=@location_cr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@location_cr_id", location_cr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }



        public void deleteInventoryAuditAndRecurringAudits(int inventory_audit_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();
            List<int> recurringAuditIds = new List<int>();

            string recurringAuditSelectStmt = "SELECT * from audits_in_recurring where inventory_audit_id = @inventory_audit_id";

            SqlCommand selectRecurringCmd = new SqlCommand(recurringAuditSelectStmt, mdfConn);
            selectRecurringCmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);

            SqlDataReader tmpRdr = selectRecurringCmd.ExecuteReader();
            while (tmpRdr.Read())
            {
                int recurringId = Convert.ToInt32(tmpRdr["recurring_id"]);
                recurringAuditIds.Add(recurringId);
            }

            tmpRdr.Close();

            string recurringIdStmt = "";
            for (int i = 0; i < recurringAuditIds.Count; i++)
            {
                recurringIdStmt = recurringIdStmt + recurringAuditIds[i];
                if (i != recurringAuditIds.Count - 1)
                {
                    recurringIdStmt = recurringIdStmt + ", ";
                }
            }

            if (recurringIdStmt.Equals(""))
            {
                recurringIdStmt = "0";
            }




            string updateRecurringAuditStmt = "Update recurring_audits set deleted = 1 where recurring_audit_id IN (" + recurringIdStmt + ");";

            SqlCommand updateRecurringCmd = new SqlCommand(updateRecurringAuditStmt, mdfConn);
            updateRecurringCmd.ExecuteNonQuery();

            

            string auditUpdateStmt = "UPDATE inventory_audits SET deleted = 1 where inventory_audit_id=@inventory_audit_id ";

            SqlCommand updateCmd = new SqlCommand(auditUpdateStmt, mdfConn);
            updateCmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);

            updateCmd.ExecuteNonQuery();

            mdfConn.Close();
        }


        public void deleteMfgCatalog(int mfg_catalog_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from mfg_catalog where mfg_catalog_id=@mfg_catalog_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@mfg_catalog_id", mfg_catalog_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteSfr(int sfr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from sfr where sfr_id=@sfr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@sfr_id", sfr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteShipTo(int ship_to_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from ship_to where ship_to_id=@ship_to_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@ship_to_id", ship_to_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteGarbageOffload(int garbage_offload_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from garbage_offload_list where garbage_offload_id=@garbage_offload_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@garbage_offload_id", garbage_offload_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteGarbageItem(int garbage_item_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "UPDATE garbage_items SET deleted_flag=1 where garbage_item_id=@garbage_item_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@garbage_item_id", garbage_item_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void flagDeletedOffload(int offload_list_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "UPDATE offload_list SET deleted=1 where offload_list_id=@offload_list_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@offload_list_id", offload_list_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }
        public void flagDeletedGarbageOffload(int garbage_offload_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "UPDATE garbage_offload_list SET deleted=1 where garbage_offload_id=@garbage_offload_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@garbage_offload_id", garbage_offload_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        //only for unit test use
        public void deleteGarbageItemCompletely(int garbage_item_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from garbage_items where garbage_item_id=@garbage_item_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@garbage_item_id", garbage_item_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        [CoverageExclude]
        public void deleteReceiving(int receiving_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from receiving_list where receiving_id=@receiving_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@receiving_id", receiving_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }


        public void deleteOffloadCr(int offload_cr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from offload_cr where offload_cr_id=@offload_cr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@offload_cr_id", offload_cr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteGarbageOffloadCr(int garbage_offload_cr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from garbage_offload_cr where garbage_offload_cr_id=@garbage_offload_cr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@garbage_offload_cr_id", garbage_offload_cr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public int insertInventoryAuditCr(List<VolumeOffloadItem> volumes, int inventory_audit_id, int location_id, int mfg_catalog_id, int qty, bool bulk_item, String serial_number, string shelf_life_expiration_date, string manufacturer_date, int shelf_life_code_id, string ui, int count, string um, string cosal, string contract_number, string atm)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
           

            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();
           

            //calculate shelf_life_expiration_date from manufacturer_date and SLC
            int slc_time_in_months = 0;
            if (shelf_life_code_id != 0)
            {
                SqlCommand selectCmd = new SqlCommand("Select time_in_months from shelf_life_code where shelf_life_code_id=@shelf_life_code_id", conn);
                selectCmd.Transaction = tran;
                selectCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                object o = selectCmd.ExecuteScalar();
                if (o != null)
                {
                    if (!o.ToString().Equals(""))
                        slc_time_in_months = Int32.Parse(o.ToString());
                }
            }

            string insertStmt = "insert into inventory_audit_cr (inventory_audit_id, location_id, mfg_catalog_id, qty, bulk_item, serial_number, shelf_life_expiration_date, current_volume, action_complete, username, device_type, alternate_ui, manufacturer_date, count, alternate_um, cosal, contract_number, atm) "
            + "VALUES (@inventory_audit_id, @location_id, @mfg_catalog_id, @qty, @bulk_item, @serial_number, @shelf_life_expiration_date, @current_volume, @action_complete, @username, @device_type,@alternate_ui, @manufacturer_date, @count, @alternate_um, @cosal, @contract_number, @atm); SELECT scope_identity()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;

            if (manufacturer_date != null && !manufacturer_date.Equals(""))
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }

            }
            else
            {

                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));

                if (shelf_life_expiration_date != null && !shelf_life_expiration_date.Equals(""))
                {
                    DateTime slcDate = DateTime.Parse(shelf_life_expiration_date);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));

                }

            }

            cmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(inventory_audit_id));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(qty));
            cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
            cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
            
            cmd.Parameters.AddWithValue("@current_volume", dbNull(null));
            cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            cmd.Parameters.AddWithValue("@username", dbNull(null));
            cmd.Parameters.AddWithValue("@device_type", dbNull(null));
            cmd.Parameters.AddWithValue("@alternate_ui", dbNull(ui));
            cmd.Parameters.AddWithValue("@count", dbNull(count));
            cmd.Parameters.AddWithValue("@alternate_um", dbNull(um));

            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
            cmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));
            cmd.Parameters.AddWithValue("@atm", dbNull(atm));
            object ee = cmd.ExecuteScalar();
            int inventory_audit_cr_id = Convert.ToInt32(ee);




            insertStmt = "insert into volumes_in_inventory_audit (inventory_audit_cr_id, volume_id) VALUES (@inventory_audit_cr_id, @volume_id)";

            SqlCommand insertCmd2 = new SqlCommand(insertStmt, conn);
            insertCmd2.Transaction = tran;

            foreach (VolumeOffloadItem item in volumes)
            {
                //insert rows
                insertCmd2.Parameters.AddWithValue("@inventory_audit_cr_id", dbNull(inventory_audit_cr_id));
                insertCmd2.Parameters.AddWithValue("@volume_id", dbNull(item.volume));

                insertCmd2.ExecuteNonQuery();
                insertCmd2.Parameters.Clear();
            }


            tran.Commit();



            conn.Close();

            return inventory_audit_cr_id;

        }



        public void updateInventoryAuditCr(List<VolumeOffloadItem> volumes, int inventory_audit_cr_id, int inventory_audit_id, int location_id, int mfg_catalog_id, int qty, bool bulk_item, String serial_number, string shelf_life_expiration_date, string manufacturer_date, int shelf_life_code_id, string ui, int count, string um, string cosal, string contract_number, string atm)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            

            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            //calculate shelf_life_expiration_date from manufacturer_date and SLC
            int slc_time_in_months = 0;
            if (shelf_life_code_id != 0)
            {
                SqlCommand selectCmd = new SqlCommand("Select time_in_months from shelf_life_code where shelf_life_code_id=@shelf_life_code_id", conn);
                selectCmd.Transaction = tran;
                selectCmd.Parameters.AddWithValue("@shelf_life_code_id", shelf_life_code_id);
                object o = selectCmd.ExecuteScalar();
                if (o != null)
                {
                    if (!o.ToString().Equals(""))
                        slc_time_in_months = Int32.Parse(o.ToString());
                }
            }


            string insertStmt = "insert into inventory_audit_cr (inventory_audit_id, location_id, mfg_catalog_id, qty, bulk_item, serial_number, shelf_life_expiration_date, current_volume, action_complete, username, device_type, parent_id, alternate_ui, alternate_um, count, manufacturer_date, cosal, contract_number, atm) "
            + "VALUES (@inventory_audit_id, @location_id, @mfg_catalog_id, @qty, @bulk_item, @serial_number, @shelf_life_expiration_date, @current_volume, @action_complete, @username, @device_type, @parent_id,@alternate_ui, @alternate_um, @count, @manufacturer_date, @cosal, @contract_number, @atm); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;

            if (manufacturer_date != null && !manufacturer_date.Equals(""))
            {
                DateTime mfgDate = DateTime.Parse(manufacturer_date);
                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(mfgDate));

                if (slc_time_in_months != 0)
                {
                    DateTime slcDate = mfgDate.AddMonths(slc_time_in_months);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));
                }

            }
            else
            {

                cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));

                if (shelf_life_expiration_date != null && !shelf_life_expiration_date.Equals(""))
                {
                    DateTime slcDate = DateTime.Parse(shelf_life_expiration_date);
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(slcDate));

                }
                else
                {
                    cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(null));

                }

            }


            cmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(inventory_audit_id));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(qty));
            cmd.Parameters.AddWithValue("@bulk_item", dbNull(bulk_item));
            cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
            
            cmd.Parameters.AddWithValue("@current_volume", dbNull(null));
            cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            cmd.Parameters.AddWithValue("@username", dbNull(null));
            cmd.Parameters.AddWithValue("@device_type", dbNull(null));
            cmd.Parameters.AddWithValue("@parent_id", dbNull(inventory_audit_cr_id));
            cmd.Parameters.AddWithValue("@alternate_ui", dbNull(ui));
            cmd.Parameters.AddWithValue("@alternate_um", dbNull(um));
            cmd.Parameters.AddWithValue("@count", dbNull(count));

            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));

            cmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));

            cmd.Parameters.AddWithValue("@atm", dbNull(atm));

            object cr = cmd.ExecuteScalar();
            int newInvCrId = Convert.ToInt32(cr);




            insertStmt = "insert into volumes_in_inventory_audit (inventory_audit_cr_id, volume_id) VALUES (@inventory_audit_cr_id, @volume_id)";

            SqlCommand insertCmd2 = new SqlCommand(insertStmt, conn);
            insertCmd2.Transaction = tran;

            foreach (VolumeOffloadItem item in volumes)
            {
                //insert rows
                insertCmd2.Parameters.AddWithValue("@inventory_audit_cr_id", dbNull(newInvCrId));
                insertCmd2.Parameters.AddWithValue("@volume_id", dbNull(item.volume));

                insertCmd2.ExecuteNonQuery();
                insertCmd2.Parameters.Clear();
            }


            tran.Commit();

            conn.Close();

        }


        public void acceptInventoryChangeRecordCount(int inventory_id, int counted_qty,  int inventory_audit_cr_id, 
           string  acceptance_reason,  string acceptance_note)
        {

             SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string updateStmt = "UPDATE inventory set qty= @qty "
               + "  WHERE inventory_id=@inventory_id ";

            SqlCommand cmd = new SqlCommand(updateStmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(counted_qty));
            cmd.ExecuteNonQuery();

        
            string insertStmt = "UPDATE inventory_audit_cr set acceptance_reason= @acceptance_reason "
                + " , acceptance_note=@acceptance_note WHERE inventory_audit_cr_id=@inventory_audit_cr_id ";

            SqlCommand cmd2 = new SqlCommand(insertStmt, conn);
            cmd2.Transaction = tran;

            cmd2.Parameters.AddWithValue("@inventory_audit_cr_id", dbNull(inventory_audit_cr_id));
            cmd2.Parameters.AddWithValue("@acceptance_reason", dbNull(acceptance_reason));
            cmd2.Parameters.AddWithValue("@acceptance_note", dbNull(acceptance_note));
           
            cmd2.ExecuteNonQuery();

            if (counted_qty == 0)
            {
                string selectStmt = "DELETE from inventory where inventory_id=@inventory_id ";

                SqlCommand cmd3 = new SqlCommand(selectStmt, conn);
                cmd3.Parameters.AddWithValue("@inventory_id", inventory_id);

                cmd3.Transaction = tran;

                cmd3.ExecuteNonQuery();
            }

            tran.Commit();
            
            conn.Close();

        }

        public int acceptInventoryGain(DateTime? manufacturer_date, DateTime? shelf_life_expiration_date,
                int location_id, int counted_qty, int mfg_catalog_id, string alternate_ui, string alternate_um,
                int inventory_audit_cr_id, string acceptance_reason, string acceptance_note, string cosal, string serial_number, string contract_number, string atm)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            int inventory_id = 0;

            string insertStmt = "insert into inventory (" +

            "shelf_life_expiration_date, manufacturer_date,  location_id, qty, serial_number,"
            + "bulk_item, mfg_catalog_id, batch_number, lot_number, contract_number, alternate_ui, alternate_um, cosal, atm"

            + ")"
            + "VALUES(" +
             "@shelf_life_expiration_date, @manufacturer_date,  @location_id, @qty, @serial_number,"
            + "@bulk_item, @mfg_catalog_id, @batch_number, @lot_number, @contract_number, @alternate_ui, @alternate_um, @cosal, @atm"
            + "); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
            cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(counted_qty));
            cmd.Parameters.AddWithValue("@serial_number", dbNull(serial_number));
            cmd.Parameters.AddWithValue("@bulk_item", dbNull(true));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            cmd.Parameters.AddWithValue("@batch_number", dbNull(null));
            cmd.Parameters.AddWithValue("@lot_number", dbNull(null));
            cmd.Parameters.AddWithValue("@contract_number", dbNull(contract_number));
            cmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));
            cmd.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));
            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));
            cmd.Parameters.AddWithValue("@atm", dbNull(atm));

            object i = cmd.ExecuteScalar();
            inventory_id = Convert.ToInt32(i);
            cmd.Parameters.Clear();

            string updateStmt = "UPDATE inventory_audit_cr set acceptance_reason= @acceptance_reason "
                + " , acceptance_note=@acceptance_note WHERE inventory_audit_cr_id=@inventory_audit_cr_id ";

            SqlCommand cmd2 = new SqlCommand(updateStmt, conn);
            cmd2.Transaction = tran;

            cmd2.Parameters.AddWithValue("@inventory_audit_cr_id", dbNull(inventory_audit_cr_id));
            cmd2.Parameters.AddWithValue("@acceptance_reason", dbNull(acceptance_reason));
            cmd2.Parameters.AddWithValue("@acceptance_note", dbNull(acceptance_note));
            cmd2.ExecuteNonQuery();

            tran.Commit();

            conn.Close();

            return inventory_id;

        }

        public int acceptInventoryLoss(int inventory_id, int inventory_audit_id, int location_id, int mfg_catalog_id, 
                DateTime? shelf_life_expiration_date, DateTime? manufacturer_date, string alternate_ui, string alternate_um,
               string  acceptance_reason, string acceptance_note, int count,  DateTime? server_synctime, string cosal)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            int inventory_audit_cr_id = 0;

            string insertStmt = "insert into inventory_audit_cr (inventory_audit_id, location_id, mfg_catalog_id, qty, "+
          "  bulk_item, serial_number, shelf_life_expiration_date, current_volume, action_complete, username, device_type, alternate_ui, alternate_um, "
          +" manufacturer_date, count, server_synctime, acceptance_reason, acceptance_note, cosal) "
           + "VALUES (@inventory_audit_id, @location_id, @mfg_catalog_id, @qty, @bulk_item, @serial_number, "
           +"@shelf_life_expiration_date, @current_volume, @action_complete, @username, @device_type,@alternate_ui, @alternate_um, "
           + " @manufacturer_date, @count, @server_synctime,  @acceptance_reason, @acceptance_note, @cosal); SELECT scope_identity()";
            
            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(inventory_audit_id));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
            cmd.Parameters.AddWithValue("@qty", dbNull(0));
            cmd.Parameters.AddWithValue("@bulk_item", dbNull(true));
            cmd.Parameters.AddWithValue("@serial_number", dbNull(null));
            cmd.Parameters.AddWithValue("@current_volume", dbNull(null));
            cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            cmd.Parameters.AddWithValue("@username", dbNull(null));
            cmd.Parameters.AddWithValue("@device_type", dbNull(null));
            cmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));
            cmd.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));
            cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(manufacturer_date));
            cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
            cmd.Parameters.AddWithValue("@count", dbNull(count));
            cmd.Parameters.AddWithValue("@server_synctime", dbNull(server_synctime));
            cmd.Parameters.AddWithValue("@acceptance_reason", dbNull(acceptance_reason));
            cmd.Parameters.AddWithValue("@acceptance_note", dbNull(acceptance_note));
            cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));

            object i = cmd.ExecuteScalar();
            inventory_audit_cr_id = Convert.ToInt32(i);
            cmd.Parameters.Clear();

            string selectStmt = "DELETE from inventory where inventory_id=@inventory_id ";

            SqlCommand cmd2 = new SqlCommand(selectStmt, conn);
            cmd2.Parameters.AddWithValue("@inventory_id", inventory_id);

            cmd2.Transaction = tran;

            cmd2.ExecuteNonQuery();

            tran.Commit();

            conn.Close();

            return inventory_audit_cr_id;
        }

       public int insertInventoryAuditCrForDiscrepancy(int inventory_audit_id, int location_id, 
           DateTime? server_synctime, int count, int mfg_catalog_id, DateTime? shelf_life_expiration_date,
           string alternate_ui, string alternate_um, int counted_qty, bool override_flag, string cosal)
       {
           SqlConnection conn = new SqlConnection(MDF_CONNECTION);
           conn.Open();

           SqlTransaction tran = conn.BeginTransaction();

           int inventory_audit_cr_id = 0;

           string insertStmt = "insert into inventory_audit_cr (inventory_audit_id, location_id, mfg_catalog_id, qty, " +
         "  bulk_item, serial_number, shelf_life_expiration_date, current_volume, action_complete, username, device_type, alternate_ui, alternate_um, "
         + " manufacturer_date, count, server_synctime, acceptance_reason, acceptance_note, override_flag, cosal) "
          + "VALUES (@inventory_audit_id, @location_id, @mfg_catalog_id, @qty, @bulk_item, @serial_number, "
          + "@shelf_life_expiration_date, @current_volume, @action_complete, @username, @device_type,@alternate_ui, @alternate_um, "
          + " @manufacturer_date, @count, @server_synctime,  @acceptance_reason, @acceptance_note, @override_flag, @cosal); SELECT scope_identity()";

           SqlCommand cmd = new SqlCommand(insertStmt, conn);
           cmd.Transaction = tran;

           cmd.Parameters.AddWithValue("@inventory_audit_id", dbNull(inventory_audit_id));
           cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
           cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
           cmd.Parameters.AddWithValue("@qty", dbNull(counted_qty));
           cmd.Parameters.AddWithValue("@bulk_item", dbNull(true));
           cmd.Parameters.AddWithValue("@serial_number", dbNull(null));
           cmd.Parameters.AddWithValue("@current_volume", dbNull(null));
           cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
           cmd.Parameters.AddWithValue("@username", dbNull(null));
           cmd.Parameters.AddWithValue("@device_type", dbNull(null));
           cmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));
           cmd.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));
           cmd.Parameters.AddWithValue("@manufacturer_date", dbNull(null));
           cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));
           cmd.Parameters.AddWithValue("@count", dbNull(count));
           cmd.Parameters.AddWithValue("@server_synctime", dbNull(server_synctime));
           cmd.Parameters.AddWithValue("@acceptance_reason", dbNull(null));
           cmd.Parameters.AddWithValue("@acceptance_note", dbNull(null));

           cmd.Parameters.AddWithValue("@override_flag", dbNull(override_flag));
           cmd.Parameters.AddWithValue("@cosal", dbNull(cosal));

           object i = cmd.ExecuteScalar();
           inventory_audit_cr_id = Convert.ToInt32(i);
           cmd.Parameters.Clear();
                     

           tran.Commit();

           conn.Close();

           return inventory_audit_cr_id;
       }

       public void updateInventoryAuditCrForDiscrepancy(int inventory_audit_cr_id, int mfg_catalog_id, DateTime? shelf_life_expiration_date, 
           string alternate_ui, string alternate_um, int counted_qty, bool override_flag)
       {

           SqlConnection conn = new SqlConnection(MDF_CONNECTION);
           conn.Open();

           SqlTransaction tran = conn.BeginTransaction();
          

           string insertStmt = "update inventory_audit_cr set mfg_catalog_id= @mfg_catalog_id, shelf_life_expiration_date= @shelf_life_expiration_date," +
               "alternate_ui=@alternate_ui , alternate_um=@alternate_um , qty =@qty ," +
               " override_flag=@override_flag  WHERE inventory_audit_cr_id=@inventory_audit_cr_id";

           SqlCommand cmd = new SqlCommand(insertStmt, conn);
           cmd.Transaction = tran;
         
           cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));
           cmd.Parameters.AddWithValue("@qty", dbNull(counted_qty));         
           cmd.Parameters.AddWithValue("@alternate_ui", dbNull(alternate_ui));
           cmd.Parameters.AddWithValue("@alternate_um", dbNull(alternate_um));
           cmd.Parameters.AddWithValue("@shelf_life_expiration_date", dbNull(shelf_life_expiration_date));         

           cmd.Parameters.AddWithValue("@override_flag", dbNull(override_flag));

           cmd.Parameters.AddWithValue("@inventory_audit_cr_id", dbNull(inventory_audit_cr_id));

           cmd.ExecuteNonQuery();

           tran.Commit();

           conn.Close();

       }

        public int insertLocationCr(int location_id, int location_done, int audit_id, int count)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            if (location_done == 1)
            {
                location_done = 0;
            }
            else if (location_done == 0)
            {
                location_done = 1;
            }
            conn.Open();

            string insertStmt = "insert into location_cr (location_id, location_done, action_complete, inventory_audit_id, count) "
            + "VALUES (@location_id, @location_done, @action_complete, @audit_id, @count); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);
            
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@location_done", dbNull(location_done));
            cmd.Parameters.AddWithValue("@action_complete", dbNull(DateTime.Now));
            cmd.Parameters.AddWithValue("@audit_id", dbNull(audit_id));
            cmd.Parameters.AddWithValue("@count", dbNull(count));


            object i = cmd.ExecuteScalar();
            int location_cr_id = Convert.ToInt32(i);

            conn.Close();

            return location_cr_id;

        }


        public void updateInventoryAuditCrSyncTime(int location_id, int audit_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            List<int> invAuditCrIds = new List<int>();
            string selectStmt = "select * from vInventoryAudit WHERE location_id = @location_id AND inventory_audit_id = @audit_id and action_complete > (select max(action_complete) from location_cr where location_id = @location_id and inventory_audit_id = @audit_id)";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("location_id", location_id);
            cmd.Parameters.AddWithValue("audit_id", audit_id);

            SqlDataReader tmpRdr = cmd.ExecuteReader();
            while (tmpRdr.Read())
            {
                int locationCrId = Convert.ToInt32(tmpRdr["inventory_audit_cr_id"]);
                invAuditCrIds.Add(locationCrId);
            }

            tmpRdr.Close();

            string ids = "";
            for (int i = 0; i < invAuditCrIds.Count; i++)
            {
                ids = ids + invAuditCrIds[i];
                if(i != invAuditCrIds.Count - 1)
                {
                    ids = ids + ", ";
                }
            }
            if (ids.Equals(""))
            {
                ids = "0"; 
            }
            string updateStmt = "Update inventory_audit_cr set server_synctime = '" + DateTime.Now + "' WHERE inventory_audit_cr_id IN (" + ids + ")";
            SqlCommand updateCmd = new SqlCommand(updateStmt, conn);

            updateCmd.ExecuteNonQuery();

            conn.Close();
        }

        public int archiveOffloadLists()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "INSERT into archived (date_archived) VALUES (@date_archived); SELECT SCOPE_IDENTITY()";
            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            insertCmd.Parameters.AddWithValue("@date_archived", dbNull(DateTime.Now));

            object o = insertCmd.ExecuteScalar();
            int archived_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();

            string updateOffload = "UPDATE offload_list set archived_id=@archived_id WHERE archived_id IS NULL";

            string updateGarbage = "UPDATE garbage_offload_list set archived_id=@archived_id WHERE archived_id IS NULL";

            SqlCommand updateOffloadCmd = new SqlCommand(updateOffload, conn);
            updateOffloadCmd.Transaction = tran;

            SqlCommand updateGarbageCmd = new SqlCommand(updateGarbage, conn);
            updateGarbageCmd.Transaction = tran;

            updateOffloadCmd.Parameters.AddWithValue("@archived_id", dbNull(archived_id));
            updateOffloadCmd.ExecuteNonQuery();

            updateGarbageCmd.Parameters.AddWithValue("@archived_id", dbNull(archived_id));
            updateGarbageCmd.ExecuteNonQuery();

            tran.Commit();

            conn.Close();

            return archived_id;

        }

        public void unarchive(int archived_id)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string updateOffload = "UPDATE offload_list set archived_id=NULL WHERE archived_id=@archived_id";

            string updateGarbage = "UPDATE garbage_offload_list set archived_id=NULL WHERE archived_id=@archived_id";

            SqlCommand updateOffloadCmd = new SqlCommand(updateOffload, conn);
            updateOffloadCmd.Transaction = tran;

            SqlCommand updateGarbageCmd = new SqlCommand(updateGarbage, conn);
            updateGarbageCmd.Transaction = tran;

            updateOffloadCmd.Parameters.AddWithValue("@archived_id", dbNull(archived_id));
            updateOffloadCmd.ExecuteNonQuery();

            updateGarbageCmd.Parameters.AddWithValue("@archived_id", dbNull(archived_id));
            updateGarbageCmd.ExecuteNonQuery();

            tran.Commit();

            conn.Close();

        }

        public DataTable getManuallyAddedSMCLforSFR()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from sfr where sfr_type = 'ADD'";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getWrongLocationsList(String location, String workCenter, List<string> invisibleHazardsList, bool hidePotHaz)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_wrong_locations_unapproved ";


            bool addLocation = false;
            bool addWorkcenter = false;

            string whereStmt = "";
            string locationStmt = "";
            if (!location.Equals(""))
            {

                //locationStmt = " location_id = @location";
                locationStmt = " name = @location"; //autocomplete

                locationStmt = " WHERE " + locationStmt;

                addLocation = true;

            }
            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";

                if (addLocation)
                    workCenterStmt = " AND " + workCenterStmt;
                else
                    workCenterStmt = " WHERE " + workCenterStmt;

                addWorkcenter = true;

            }

            string hazardStmt = "";
            if (invisibleHazardsList.Count > 0)
            {

                if (addLocation || addWorkcenter)
                    hazardStmt = " AND (hazard_name not in ('";
                else
                    hazardStmt = " WHERE (hazard_name not in ('";

                string hazardList = "";
                string hazardList2 = "";
                int count = 0;
                int count2 = 0;
                foreach (string s in invisibleHazardsList)
                {
                    if (count == invisibleHazardsList.Count - 1)
                    {
                        hazardList += s + "') OR hazard_name2 not in ('";
                    }
                    else
                    {
                        hazardList += s + "', '";
                    }
                    count++;
                }
                foreach (string s in invisibleHazardsList)
                {
                    if (count2 == invisibleHazardsList.Count - 1)
                    {
                        hazardList2 += s + "'))";
                    }
                    else
                    {
                        hazardList2 += s + "', '";
                    }
                    count2++;
                }

                hazardStmt += hazardList + hazardList2;

            }
            else
            {
                if (addLocation || addWorkcenter)
                    hazardStmt = " AND (hazard_name not in ('') AND hazard_name2 not in (''))";
                else
                    hazardStmt = " WHERE (hazard_name not in ('') AND hazard_name2 not in (''))";
            }


            string potHazStmt = "";
            if (hidePotHaz)
            {
                potHazStmt = " AND hazard_name != 'Potential Hazards Unknown' AND hazard_name2 != 'Potential Hazards Unknown'";



            }


            whereStmt += (locationStmt + workCenterStmt + hazardStmt + potHazStmt);
            whereStmt += " ORDER BY warning_level, inventory_id, inventory_id2, hcc desc, hcc2 desc;";

            selectStmt += whereStmt;

            System.Diagnostics.Debug.WriteLine("\n\n" + selectStmt);

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            List<DataRow> rowsToRemove = new List<DataRow>();
            Hashtable htFound = new Hashtable();

            //FILTER OUT SPECIAL CASE
            foreach(DataRow row in dt.Rows)
            {
                System.Diagnostics.Debug.WriteLine(
                    row["niin"].ToString() + " - id " + row["inventory_id"].ToString() +
                    " - hcc " + row["hcc"].ToString() + " = " +
                    row["niin2"].ToString() + " - id " + row["inventory_id2"].ToString() +
                    " - hcc2 " + row["hcc2"].ToString());
 
                string hcc1 = Convert.ToString(row["hcc"]);
                string hcc2 = Convert.ToString(row["hcc2"]);
                string key = row["inventory_id"] + "_" + row["inventory_id2"];
                string keyrev = row["inventory_id2"] + "_" + row["inventory_id"];

                bool ignore = false;

                if (hcc1.Equals("V2") && hcc2.Equals("V3"))
                    ignore = true;
                else if (hcc1.Equals(hcc2) && hcc1 != null && hcc2 != null && !hcc1.Equals("") && !hcc2.Equals(""))
                     //If HCC=HCC they are compatible
                    ignore = true;
                else if (hcc1.Equals("V3") && hcc2.Equals("V2"))
                    ignore = true;
                else {
                    // See if this is a duplicate
                    if (htFound.ContainsKey(key) || htFound.ContainsKey(keyrev)) {
                        ignore = true;
                    }
                    else {
                        htFound[row["inventory_id"] + "_" + row["inventory_id2"]] = "";
                    }
                }

                if (ignore)
                {
                    if (!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }
              

            }//end for each

            //Remove rows
            foreach (DataRow row in rowsToRemove)
            {
                dt.Rows.Remove(row);
            }//end

            return dt;
        }

        public DataTable getWrongLocationsListForPDF(String location, String workCenter, List<string> invisibleHazardsList, bool hidePotHaz)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select warning_level, atm as ATM, niin, description as 'Item Name', "
                + "SMCC, HCC, hazard_name as Hazard, atm2 as ATM, niin2 as NIIN, description2 as 'Item Name', "
                + "smcc2 as SMCC, HCC2 as HCC1,  hazard_name2 as Hazard, name as 'Location', wid as 'Workcenter', "
                + "inventory_id, inventory_id2 "
                + "from v_wrong_locations_unapproved ";

            bool addLocation = false;
            bool addWorkcenter = false;

            string whereStmt = "";
            string locationStmt = "";
            if (!location.Equals("")) {

                //locationStmt = " location_id = @location";
                locationStmt = " name = @location"; //autocomplete

                locationStmt = " WHERE " + locationStmt;

                addLocation = true;
            }
            string workCenterStmt = "";
            if (!workCenter.Equals("")) {

                workCenterStmt = " workcenter_id = @workcenter";

                if (addLocation)
                    workCenterStmt = " AND " + workCenterStmt;
                else
                    workCenterStmt = " WHERE " + workCenterStmt;

                addWorkcenter = true;
            }

            string hazardStmt = "";
            if (invisibleHazardsList.Count > 0) {
                if (addLocation || addWorkcenter)
                    hazardStmt = " AND (hazard_name not in ('";
                else
                    hazardStmt = " WHERE (hazard_name not in ('";

                string hazardList = "";
                string hazardList2 = "";
                int count = 0;
                int count2 = 0;
                foreach (string s in invisibleHazardsList) {
                    if (count == invisibleHazardsList.Count - 1) {
                        hazardList += s + "') OR hazard_name2 not in ('";
                    }
                    else {
                        hazardList += s + "', '";
                    }
                    count++;
                }
                foreach (string s in invisibleHazardsList) {
                    if (count2 == invisibleHazardsList.Count - 1) {
                        hazardList2 += s + "'))";
                    }
                    else {
                        hazardList2 += s + "', '";
                    }
                    count2++;
                }

                hazardStmt += hazardList + hazardList2;
            }
            else {
                if (addLocation || addWorkcenter)
                    hazardStmt = " AND (hazard_name not in ('') AND hazard_name2 not in (''))";
                else
                    hazardStmt = " WHERE (hazard_name not in ('') AND hazard_name2 not in (''))";
            }

            string potHazStmt = "";
            if (hidePotHaz) {
                potHazStmt = " AND hazard_name != 'Potential Hazards Unknown' AND hazard_name2 != 'Potential Hazards Unknown'";
            }

            whereStmt += (locationStmt + workCenterStmt + hazardStmt + potHazStmt);

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            List<DataRow> rowsToRemove = new List<DataRow>();
            Hashtable htFound = new Hashtable();

            //FILTER OUT SPECIAL CASE
            foreach (DataRow row in dt.Rows) {
                string hcc1 = Convert.ToString(row["hcc"]);
                string hcc2 = Convert.ToString(row["hcc1"]);
                string key = row["inventory_id2"] + "_" + row["inventory_id"];

                bool ignore = false;

                if (hcc1.Equals("V2") && hcc2.Equals("V3"))
                    ignore = true;
                else if (hcc1.Equals(hcc2) && hcc1 != null && hcc2 != null && !hcc1.Equals("") && !hcc2.Equals("")) //If HCC=HCC they are compatible
                    ignore = true;
                else if (hcc1.Equals("V3") && hcc2.Equals("V2"))
                    ignore = true;
                else {
                    // See if this is a duplicate
                    if (htFound.ContainsKey(key)) {
                        ignore = true;
                    }
                    else {
                        htFound[row["inventory_id"] + "_" + row["inventory_id2"]] = "";
                    }
                }

                if (ignore) {
                    if (!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }
            }//end for each

            //Remove rows
            foreach (DataRow row in rowsToRemove) {
                dt.Rows.Remove(row);
            }//end

            // Remove the inventory ID columns
            dt.Columns.Remove(dt.Columns["inventory_id"]);
            dt.Columns.Remove(dt.Columns["inventory_id2"]);

            return dt;
        }


        public DataTable getApprovedWrongLocationsList(String location, String workCenter)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_wrong_locations_approved";


            bool addLocation = false;
            bool addWorkcenter = false;

            string whereStmt = "";
            string locationStmt = "";
            if (!location.Equals(""))
            {

                //locationStmt = " location_id = @location";
                locationStmt = " name = @location"; //autocomplete

                locationStmt = " WHERE " + locationStmt;

                addLocation = true;

            }
            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";

                if (addLocation)
                    workCenterStmt = " AND " + workCenterStmt;
                else
                    workCenterStmt = " WHERE " + workCenterStmt;

                addWorkcenter = true;

            }

            whereStmt += (locationStmt + workCenterStmt);

            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            // Dedup the table
            List<DataRow> rowsToRemove = new List<DataRow>();
            Hashtable htFound = new Hashtable();

            //FILTER OUT SPECIAL CASE
            foreach (DataRow row in dt.Rows) {
                string key = row["inventory_id2"] + "_" + row["inventory_id"];

                // See if this is a duplicate
                if (htFound.ContainsKey(key)) {
                    if (!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }
                else {
                    htFound[row["inventory_id"] + "_" + row["inventory_id2"]] = "";
                }
            }

            //Remove rows
            foreach (DataRow row in rowsToRemove) {
                dt.Rows.Remove(row);
            }

            conn.Close();                       

            return dt;
        }


        public List<MSSLDataDiscrepancy> getMsslDiscrepancy(String location, String field, String value)
        {

            List<MSSLDataDiscrepancy> dataList = new List<MSSLDataDiscrepancy>();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT * FROM ");
            sb.AppendLine("  (SELECT (v.niin + v.[name] + v.cosal) AS hashkey, ");
            sb.AppendLine("     v.cosal     AS cosal, ");
            sb.AppendLine("     v.niin, ");
            sb.AppendLine("     nc.[description], ");
            sb.AppendLine("     nc.ui, ");
            sb.AppendLine("     v.[name]    AS location, ");
            sb.AppendLine("     0           AS mssl_qty, ");
            sb.AppendLine("     SUM(v.qty)  AS inventory_qty, ");
            sb.AppendLine("     SUM(v.qty)  AS qty_difference, ");
            sb.AppendLine("     2           AS sortkey ");
            sb.AppendLine("  FROM vinventory v ");
            sb.AppendLine("  JOIN niin_catalog nc ON nc.niin = v.niin ");
            sb.AppendLine("    WHERE (v.niin + v.[name] + v.cosal) NOT IN ");
            sb.AppendLine("     (SELECT DISTINCT (niin + location + ati) ");
            sb.AppendLine("        FROM   mssl_inventory ");
            sb.AppendLine("        WHERE location IS NOT NULL) ");
            sb.AppendLine("  GROUP BY (v.niin + v.[name] + v.cosal), ");
            sb.AppendLine("      v.cosal, ");
            sb.AppendLine("      v.niin, ");
            sb.AppendLine("      v.[name], ");
            sb.AppendLine("      nc.[description], ");
            sb.AppendLine("      nc.ui ");
            sb.AppendLine("  UNION ");
            sb.AppendLine("  SELECT (m.niin + m.location + m.ati) AS hashkey, ");
            sb.AppendLine("     m.ati       AS cosal, ");
            sb.AppendLine("     m.niin, ");
            sb.AppendLine("     nc.[description], ");
            sb.AppendLine("     nc.ui, ");
            sb.AppendLine("     m.location  AS location, ");
            sb.AppendLine("     SUM(m.qty)  AS mssl_qty, ");
            sb.AppendLine("     0           AS inventory_qty, ");
            sb.AppendLine("     SUM(m.qty)  AS qty_difference, ");
            sb.AppendLine("     1           AS sortkey ");
            sb.AppendLine("  FROM mssl_inventory m ");
            sb.AppendLine("     JOIN niin_catalog nc ");
            sb.AppendLine("     ON nc.niin = m.niin ");
            sb.AppendLine("  WHERE m.location IS NOT NULL ");
            sb.AppendLine("     AND (m.niin + m.location + m.ati) NOT IN ");
            sb.AppendLine("      (SELECT DISTINCT (niin + [name] + cosal) ");
            sb.AppendLine("      FROM   vinventory) ");
            sb.AppendLine("  GROUP BY (m.niin + m.location + m.ati), ");
            sb.AppendLine("      m.ati, ");
            sb.AppendLine("      m.niin, ");
            sb.AppendLine("      m.location, ");
            sb.AppendLine("      nc.[description], ");
            sb.AppendLine("      nc.ui ");
            sb.AppendLine("  UNION ");
            sb.AppendLine("  SELECT (v.niin + v.[name] + v.cosal) AS hashkey, ");
            sb.AppendLine("     v.cosal     AS cosal, ");
            sb.AppendLine("     v.niin, ");
            sb.AppendLine("     nc.[description], ");
            sb.AppendLine("     nc.ui, ");
            sb.AppendLine("     v.[name]    AS location, ");
            sb.AppendLine("     SUM(m.qty)  AS mssl_qty, ");
            sb.AppendLine("     SUM(v.qty)  AS inventory_qty, ");
            sb.AppendLine("     ( SUM(v.qty) - SUM(m.qty) ) AS qty_difference, ");
            sb.AppendLine("     3           AS sortkey ");
            sb.AppendLine("  FROM vinventory v, ");
            sb.AppendLine("     mssl_inventory m ");
            sb.AppendLine("     JOIN niin_catalog nc ");
            sb.AppendLine("     ON nc.niin = m.niin ");
            sb.AppendLine("  WHERE (v.niin + v.[name] + v.cosal) = (m.niin + m.location + m.ati) ");
            sb.AppendLine("  GROUP BY (v.niin + v.[name] + v.cosal), ");
            sb.AppendLine("      v.cosal, ");
            sb.AppendLine("      v.niin, ");
            sb.AppendLine("      v.[name], ");
            sb.AppendLine("      nc.[description], ");
            sb.AppendLine("      nc.ui ");
            sb.AppendLine(") AS q ");
            sb.AppendLine("WHERE (( q.qty_difference != 0 AND q.sortkey = 3 ) OR q.sortkey < 3 ) ");

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);            

            conn.Open();

            Dictionary<string, string> dicDesc = new Dictionary<string, string>();
            Dictionary<string, string> dicUi = new Dictionary<string, string>();

            if (!location.Equals("")) {
                //user wants to filter the table by location
                // Add the location filter
                sb.AppendLine("AND q.location like '%' + @location + '%' ");
            }

            if (!field.Equals("")) {
                //user wants to filter the table by desc or niin
                if (field.Equals("NIIN")) {
                    //niin
                    sb.AppendLine("AND q.niin like '%' + @value + '%' ");
                }
                else if (field.Equals("Description")) {

                    //description
                    sb.AppendLine("AND q.description like '%' + @value + '%' ");

                }

            }

            // Add the Order By
            sb.AppendLine("ORDER BY q.sortkey, q.hashkey");

            SqlCommand cmd = new SqlCommand(sb.ToString(), conn);

            if (!location.Equals("")) {
                //location parameter only
                cmd.Parameters.AddWithValue("@location", location);                
            }

            if (!field.Equals("")) {
                //niin/desc only               
                cmd.Parameters.AddWithValue("@value", value);
            }


            SqlDataReader rdr = cmd.ExecuteReader();
           
            while (rdr.Read()) {

                MSSLDataDiscrepancy d = new MSSLDataDiscrepancy();

                d.cosal =  HttpUtility.HtmlEncode(rdr["cosal"].ToString());

                string niin = rdr["NIIN"].ToString();
                string loc = rdr["LOCATION"].ToString();
                string desc = rdr["DESCRIPTION"].ToString();
                string ui = rdr["UI"].ToString();

                int nTemp = 0;
                int.TryParse(rdr["MSSL_QTY"].ToString(), out nTemp);
                int msslqty = nTemp;

                nTemp = 0;
                int.TryParse(rdr["INVENTORY_QTY"].ToString(), out nTemp);
                int invqty = nTemp;

                nTemp = 0;
                int.TryParse(rdr["QTY_DIFFERENCE"].ToString(), out nTemp);
                int qtyDif = nTemp;

                nTemp = 0;
                int.TryParse(rdr["sortkey"].ToString(), out nTemp);
                int sortKey = nTemp;

                switch (sortKey) {
                    case 1:
                        d.DiffType = "Missing HILT Location";
                        break;
                    case 2:
                        d.DiffType = "Missing MSSL Location";
                        break;
                    case 3:
                        d.DiffType = "QTY Difference";
                        break;

                }

                
                d.Location =  HttpUtility.HtmlEncode(loc);
                d.Niin =  HttpUtility.HtmlEncode(niin);

                string desc = dicDesc[niin].ToString();
                string ui = dicUi[niin].ToString();

              

                d.Description =  HttpUtility.HtmlEncode(desc);
                d.Ui =  HttpUtility.HtmlEncode(ui);
                
                //if (msslqty == null && invqty == null) {
                //    d.MsslQty = 0;
                //    d.InvQty = 0;
                //    d.Difference = 0;

                //}
                //else if (msslqty == null) {
                //    d.MsslQty = 0;
                //    d.Difference = (int)invqty;
                //    d.InvQty = (int)invqty;
                //}
                //else if (invqty == null) {
                //    d.InvQty = 0;
                //    d.Difference = (int)msslqty;
                //    d.MsslQty = (int)msslqty;
                //}
                //else {
                //    d.InvQty = (int)invqty;
                //    d.MsslQty = (int)msslqty;
                //    d.Difference = (int)qtyDif;
                //}


                //if (d.Difference != 0) {
                //}

                d.InvQty = invqty;
                d.MsslQty = msslqty;
                d.Difference = qtyDif;

                    dataList.Add(d);
                }


            
           
            conn.Close();

            return dataList;
        }


        public DataTable getLimResProList(String location, String workCenter, String usageCategory)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory";
            string whereStmt = "";
            bool addLocation = false;
            bool addWorkcenter = false;

            if (usageCategory.Equals("Restricted"))
            {
                whereStmt = " WHERE usage_description = 'Restricted'";
            }
            else if (usageCategory.Equals("Prohibited"))
            {
                whereStmt = " WHERE usage_description = 'Prohibited'";
            }
            else if (usageCategory.Equals("Limited"))
            {
                whereStmt = " WHERE usage_description = 'Limited'";
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

               // locationStmt = " location_id = @location";
                locationStmt = " name = @location";

                if(!usageCategory.Equals(""))
                locationStmt = " AND " + locationStmt;
                else if(usageCategory.Equals(""))
                locationStmt = " WHERE " + locationStmt;

                addLocation = true;

            }
            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";
                if(usageCategory.Equals("") && location.Equals(""))
                    workCenterStmt = " WHERE " + workCenterStmt;
                else 
                    workCenterStmt = " AND " + workCenterStmt;

                addWorkcenter = true;

            }                                       
            
            whereStmt += (locationStmt + workCenterStmt);

            if (location.Equals("") && workCenter.Equals("") && usageCategory.Equals(""))
                whereStmt = " WHERE usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Limited'";
            else if ((!location.Equals("") || !workCenter.Equals("")) && usageCategory.Equals(""))
                whereStmt = whereStmt + " AND (usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Limited')";
            whereStmt += "ORDER BY wid, name";
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }



        public DataTable getAtmosphereControlLogReport(String location, String workCenter, String usageCategory, string sortBy, bool pdfVersion)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory ";
            
            if(pdfVersion)
              selectStmt = "select ATM, NIIN, description as [Description], CAGE, Manufacturer, product_identity as [Product Identity], qty as [QTY], name as [Location], wid as [Workcenter], usage_description as [Usage Category] from vUsageCatInventory ";
            
            string whereStmt = "";
            bool addLocation = false;
            bool addWorkcenter = false;

            if (usageCategory.Equals("Restricted"))
            {
                whereStmt = " WHERE usage_description = 'Restricted'";
            }
            else if (usageCategory.Equals("Prohibited"))
            {
                whereStmt = " WHERE usage_description = 'Prohibited'";
            }
            else if (usageCategory.Equals("Limited"))
            {
                whereStmt = " WHERE usage_description = 'Limited'";
            }
            else if (usageCategory.Equals("Permitted"))
            {
                whereStmt = " WHERE usage_description = 'Permitted, no restrictions'";
            }
            else if (usageCategory.Equals("Not yet evaluated"))
            {
                whereStmt = " WHERE usage_description = 'Not yet evaluated'";
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

                // locationStmt = " location_id = @location";
                locationStmt = " name = @location";

                if (!usageCategory.Equals(""))
                    locationStmt = " AND " + locationStmt;
                else if (usageCategory.Equals(""))
                    locationStmt = " WHERE " + locationStmt;

                addLocation = true;

            }
            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";
                if (usageCategory.Equals("") && location.Equals(""))
                    workCenterStmt = " WHERE " + workCenterStmt;
                else
                    workCenterStmt = " AND " + workCenterStmt;

                addWorkcenter = true;

            }

            whereStmt += (locationStmt + workCenterStmt);

            if (location.Equals("") && workCenter.Equals("") && usageCategory.Equals(""))
                whereStmt = " WHERE usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Limited' OR usage_description = 'Permitted, no restrictions' OR usage_description = 'Not yet evaluated'";
            else if ((!location.Equals("") || !workCenter.Equals("")) && usageCategory.Equals(""))
                whereStmt = whereStmt + " AND (usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Limited' OR usage_description = 'Permitted, no restrictions' OR usage_description = 'Not yet evaluated')";

            string sort = " wid, name, niin ";
            if (sortBy != null)
                sort = sortBy;
            
            whereStmt += " ORDER BY "+sort;
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }




        public DataTable getLimResProListForReport(String location, String workCenter, String usageCategory)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select atm, NIIN, description as [Item Name], CAGE, Manufacturer, product_identity as [Product Identity], qty as [QTY], name as [Location], wid as [Workcenter], usage_description as [Usage Category], ' ' as [Signature] from vUsageCatInventory";
            string whereStmt = "";
            bool addLocation = false;
            bool addWorkcenter = false;

            if (usageCategory.Equals("Restricted"))
            {
                whereStmt = " WHERE usage_description = 'Restricted'";
            }
            else if (usageCategory.Equals("Prohibited"))
            {
                whereStmt = " WHERE usage_description = 'Prohibited'";
            }
            else if (usageCategory.Equals("Limited"))
            {
                whereStmt = " WHERE usage_description = 'Limited'";
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

                // locationStmt = " location_id = @location";
                locationStmt = " name = @location";

                if (!usageCategory.Equals(""))
                    locationStmt = " AND " + locationStmt;
                else if (usageCategory.Equals(""))
                    locationStmt = " WHERE " + locationStmt;

                addLocation = true;

            }
            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";
                if (usageCategory.Equals("") && location.Equals(""))
                    workCenterStmt = " WHERE " + workCenterStmt;
                else
                    workCenterStmt = " AND " + workCenterStmt;

                addWorkcenter = true;

            }

            whereStmt += (locationStmt + workCenterStmt);

            if (location.Equals("") && workCenter.Equals("") && usageCategory.Equals(""))
                whereStmt = " WHERE usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Prohibited'";
            else if ((!location.Equals("") || !workCenter.Equals("")) && usageCategory.Equals(""))
                whereStmt = whereStmt + " AND (usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Prohibited')";
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public bool locationHasInventory(int location_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from inventory where location_id = @loc";
            
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("loc", location_id);

            SqlDataReader reader = cmd.ExecuteReader();

            bool retVal = false;
            if (reader.HasRows)
            {
                retVal = true;
            }

            reader.Close();
            conn.Close();

            return retVal;
        }


        [CoverageExclude]
        public int createAuditForTest()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("insert into inventory_audits (scheduled_on, created, description, ship_status_id, deleted) VALUES ('2011-2-3 00:00:00', '" + DateTime.Now + "', 'TESTING', 1, 0); SELECT SCOPE_IDENTITY()", mdfConn);

            object o = cmd.ExecuteScalar();

            int auditid = Convert.ToInt32(o);

            cmd.Dispose();
            mdfConn.Close();

            return auditid;
        }


        [CoverageExclude]
        public int createRecurringAuditForTest(int inventory_audit_id)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand recurringCmd = new SqlCommand("insert into recurring_audits (description, initially_scheduled_on, ship_status, recurring, deleted) VALUES ('TESTING', '2010-10-18 00:00:00', 1, 'Weekly', 0); SELECT SCOPE_IDENTITY()", mdfConn);            

            object o = recurringCmd.ExecuteScalar();

            int recurringAuditid = Convert.ToInt32(o);

            SqlCommand cmd = new SqlCommand("insert into audits_in_recurring (inventory_audit_id, recurring_id) VALUES (" + inventory_audit_id + ", " + recurringAuditid + "); SELECT SCOPE_IDENTITY()", mdfConn);

            cmd.Dispose();
            mdfConn.Close();

            return recurringAuditid;
        }

        [CoverageExclude]
        public void removeAuditForTest(int inventory_audit_id)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "DELETE from inventory_audits where inventory_audit_id=@inventory_audit_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);

            cmd.ExecuteNonQuery();

            mdfConn.Close();
        }


        [CoverageExclude]
        public void removeRecurringAuditForTest(int recurring_audit_id, int inventory_audit_id)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string deleteAuditsInRecurringStmt = "DELETE from audits_in_recurring where inventory_audit_id=@inventory_audit_id AND recurring_id=@recurring_id";

            SqlCommand cmd = new SqlCommand(deleteAuditsInRecurringStmt, mdfConn);
            cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);
            cmd.Parameters.AddWithValue("@recurring_id", recurring_audit_id);

            cmd.ExecuteNonQuery();

            string deleteRecurringAuditStmt = "DELETE from recurring_audits where recurring_audit_id=@recurring_audit_id";

            SqlCommand cmd2 = new SqlCommand(deleteRecurringAuditStmt, mdfConn);
            cmd2.Parameters.AddWithValue("@recurring_audit_id", recurring_audit_id);

            cmd.ExecuteNonQuery();

            mdfConn.Close();
        }

        [CoverageExclude]
        public int createWorkcenterForTest()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlCommand cmd = new SqlCommand("insert into workcenter (description, wid) VALUES ('TESTING', 'TE01'); SELECT SCOPE_IDENTITY()", mdfConn);

            object o = cmd.ExecuteScalar();

            int wcid = Convert.ToInt32(o);

            cmd.Dispose();
            mdfConn.Close();

            return wcid;
        }

        [CoverageExclude]
        public void removeWorkcenterForTest(int workcenter_id)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "DELETE from workcenter where workcenter_id=@workcenter_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@workcenter_id", workcenter_id);

            cmd.ExecuteNonQuery();

            mdfConn.Close();
        }


        [CoverageExclude]
        public int getIfInventoryAuditDeletedById(int inventory_audit_id)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            string selectStmt = "Select deleted from inventory_audits where inventory_audit_id=@inventory_audit_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@inventory_audit_id", inventory_audit_id);


            object o = cmd.ExecuteScalar();

            int deleted = Convert.ToInt32(o);

            mdfConn.Close();

            return deleted;
        }


        [CoverageExclude]
        public int getIfRecurringAuditDeletedById(int recurring_audit_id)
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();


            string selectStmt = "Select deleted from recurring_audits where recurring_audit_id=@recurring_audit_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@recurring_audit_id", recurring_audit_id);


            object o = cmd.ExecuteScalar();

            int deleted = Convert.ToInt32(o);

            mdfConn.Close();

            return deleted;
        }



        public DataTable getOffloadReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_offload_list_top v, locations l WHERE v.location_id = l.location_id AND archived_id IS NULL";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }



        public DataTable getUsageCatReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getFeedbackExcelReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from feedback";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getIncompatiblesReport()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();


            string[] selectStmt = 
                    new string[] { @"select COUNT(inventory_id) as Total
	                                                from inventory",

	                                @"select 
	                                    COUNT(inventory_id) as Good
                                    from 
	                                    inventory 
                                    where 
	                                    inventory_id not in (select inventory_id from v_wrong_locations)",
    
                                    @"select 
	                                    COUNT(inventory_id) as Cleared
                                    from
	                                    v_wrong_locations_approved",
	
                                    @"select 
	                                    COUNT(distinct inventory_id) as Incompatible
                                    from 
	                                    v_wrong_locations_unapproved"};

            SqlCommand cmd;
            DataTable dt = new DataTable();

            foreach (string s in selectStmt)
            {
                cmd = new SqlCommand(s, conn);
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                sda.Fill(dt);
            }
            
            conn.Close();
            return dt;

        }

        public DataTable getUsageCategoryExcelReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getHccExcelReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"select 
	                cage, niin, smcc, hcc, serial_number, manufacturer, COSAL
                from
	                vUsageCatInventory
                where
	                hcc = '' or hcc is null";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getInventoryForShelfLifeReport(string startDate, string niin, string toDate, string location, string workcenter)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.atm, i.cosal, i.inventory_id, i.qty, i.mfg_catalog_id, i.shelf_life_expiration_date, i.location_id, l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter, m.cage, m.manufacturer, m.PRODUCT_IDENTITY, m.niin_catalog_id, n.niin, n.description"
                + " from inventory i LEFT JOIN locations l ON (i.location_id=l.location_id) LEFT JOIN workcenter w ON (l.workcenter_id=w.workcenter_id), mfg_catalog m, niin_catalog n "
                + " WHERE i.mfg_catalog_id=m.mfg_catalog_id AND m.niin_catalog_id = n.niin_catalog_id";

            string whereStmt = " AND shelf_life_expiration_date is NOT NULL ";
            bool addStart = false;
            bool addTo = false;
            if (!startDate.Equals(""))
            {
                whereStmt += "AND shelf_life_expiration_date > @start_date ";
                addStart = true;
            }
            if (!toDate.Equals(""))
            {
                whereStmt += "AND shelf_life_expiration_date < @to_date ";
                addTo = true;
            }
            bool addLocation = false;
            bool addWorkcenter = false;
            bool addNiin = false;
            
            string locationStmt = "";
            if (!location.Equals(""))
            {
                
                locationStmt = " AND name = @location ";                

                addLocation = true;

            }
            string workcenterStmt = "";
            if (!workcenter.Equals(""))
            {

                workcenterStmt = " AND l.workcenter_id = @workcenter ";

                addWorkcenter = true;

            }

            string niinStmt = "";
            if (!niin.Equals(""))
            {

                niinStmt = " AND niin like '%' + @niin + '%' ";

                addNiin = true;

            }

            whereStmt += (locationStmt + workcenterStmt + niinStmt);

            
            whereStmt += " ORDER BY shelf_life_expiration_date, wid, location_name ";
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workcenter);
            if (addStart)
                cmd.Parameters.Add("@start_date", SqlDbType.DateTime).Value = startDate;
            if (addTo)
                cmd.Parameters.Add("@to_date", SqlDbType.DateTime).Value = toDate;
            if (addNiin)
                cmd.Parameters.Add("@niin", SqlDbType.NVarChar).Value = niin;


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public string getUserRole(string userName)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select role from user_roles where username = @username";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@username", userName);
            List<string> roles = new List<string>();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string s = reader.GetValue(0).ToString();
                roles.Add(s);
            }
            conn.Close();
            if (roles.Contains("ADMIN"))            
                return "ADMIN";            
            else if (roles.Contains("SUPPLY OFFICER"))
                return "SUPPLY OFFICER";
            else if (roles.Contains("SUPPLY USER"))
                return "SUPPLY USER";
            else if (roles.Contains("WORKCENTER USER"))
                return "WORKCENTER USER";
            else
                return "BASE USER";
            

        }

        public string getSLCBySLCId(int slc_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select slc from shelf_life_code where shelf_life_code_id = @id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", slc_id);

            SqlDataReader reader = cmd.ExecuteReader();

            string slc = "";
            while (reader.Read())
            {
                slc = reader.GetString(0);
            }
            conn.Close();

            return slc;
        }


        public String getMSDSSerNoForID(int msds_id) {
            String msdsSerNo = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select MSDSSERNO from msds where msds_id = @msdsid";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msdsid", msds_id);
            msdsSerNo = (String) cmd.ExecuteScalar();

            conn.Close();

            return msdsSerNo;
        }

        /// <summary>
        /// Returns the latest version of the MSDS for the specified MSDS serial number
        /// </summary>
        /// <param name="msdsserno">the MSDS serial number</param>
        /// <returns>msdsRecord object</returns>
        ///
        public MsdsRecord getMSDSRecordforSerialNumber(string msdsserno)
        {
            return getMSDSRecordforSerialNumber(msdsserno, "");
        }

        /// <summary>
        /// Returns the latest version of the MSDS depending on whether the MSDS was
        /// manually loaded or imported from the HMIRS data
        /// </summary>
        /// <param name="msdsserno">the MSDS serial number</param>
        /// <param name="manually_entered">"" = ignore manually_entered field
        /// "Y" = extract from manually entered records
        /// "N" = extract from the original MSDS record</param>
        /// <returns>msdsRecord object</returns>
        ///
        public MsdsRecord getMSDSRecordforSerialNumber(string msdsserno, string manually_entered)
        {
            MsdsRecord msdsItem = null;
            int msds_id = 0;

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            // Get the basic MSDS data and single record table items
            StringBuilder sb = new StringBuilder();
            sb.Append("select m.*, h.hcc ");
            sb.Append(" from msds m "
                + "  LEFT JOIN HCC h ON (h.hcc_id=m.hcc_id) "
                + " where m.msds_id = ");


            // Determine which version of the MSDS to get:
            // manually_entered = "" - get the latest version ignoring manually_entered flag
            // manually_entered = "Y" - get the latest manually entered version
            // manually_entered = "N" - get the original msds record
            if ("Y".Equals(manually_entered.ToUpper())) {
                sb.Append(" (select max(msds_id) from msds where msdsserno = @msdsserno "
                    + "and manually_entered = 1)");
            }
            else if ("N".Equals(manually_entered.ToUpper()))
            {
                sb.Append(" (select max(msds_id) from msds where msdsserno = @msdsserno "
                    + "and manually_entered = 0)");
            }
            else {
                sb.Append(" (select max(msds_id) from msds where msdsserno = @msdsserno)");
            }

            SqlCommand cmd = new SqlCommand(sb.ToString(), mdfConn);
            cmd.Parameters.AddWithValue("msdsserno", msdsserno);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                msdsItem = new MsdsRecord();

                // Basic data
                msds_id = Convert.ToInt32(rdr["msds_id"].ToString());
                msdsItem.MSDSSERNO = msdsserno;
                msdsItem.FSC = rdr["FSC"].ToString();
                msdsItem.NIIN = rdr["NIIN"].ToString();
                msdsItem.CAGE = rdr["CAGE"].ToString();
                msdsItem.MANUFACTURER = rdr["MANUFACTURER"].ToString();
                msdsItem.PARTNO = rdr["PARTNO"].ToString();
                int hcc_id = -1;
                int.TryParse(rdr["hcc_id"].ToString(), out hcc_id);
                msdsItem.hcc_id = hcc_id;
                msdsItem.HCC = rdr["HCC"].ToString();
                msdsItem.ARTICLE_IND = rdr["ARTICLE_IND"].ToString();
                msdsItem.DESCRIPTION = rdr["DESCRIPTION"].ToString();
                msdsItem.EMERGENCY_TEL = rdr["EMERGENCY_TEL"].ToString();
                msdsItem.END_COMP_IND = rdr["END_COMP_IND"].ToString();
                msdsItem.END_ITEM_IND = rdr["END_ITEM_IND"].ToString();
                msdsItem.KIT_IND = rdr["KIT_IND"].ToString();
                msdsItem.KIT_PART_IND = rdr["KIT_PART_IND"].ToString();
                msdsItem.MANUFACTURER_MSDS_NO = rdr["MANUFACTURER_MSDS_NO"].ToString();
                msdsItem.MIXTURE_IND = rdr["MIXTURE_IND"].ToString();
                msdsItem.PRODUCT_IDENTITY = rdr["PRODUCT_IDENTITY"].ToString();
                msdsItem.PRODUCT_IND = rdr["PRODUCT_IND"].ToString();
                msdsItem.PRODUCT_LANGUAGE = rdr["PRODUCT_LANGUAGE"].ToString();
                msdsItem.PRODUCT_LOAD_DATE = String.Format("{0:MM/dd/yyyy}",rdr["PRODUCT_LOAD_DATE"]);
                msdsItem.PRODUCT_RECORD_STATUS = rdr["PRODUCT_RECORD_STATUS"].ToString();
                msdsItem.PRODUCT_REVISION_NO = rdr["PRODUCT_REVISION_NO"].ToString();
                msdsItem.PROPRIETARY_IND = rdr["PROPRIETARY_IND"].ToString();
                msdsItem.PUBLISHED_IND = rdr["PUBLISHED_IND"].ToString();
                msdsItem.PURCHASED_PROD_IND = rdr["PURCHASED_PROD_IND"].ToString();
                msdsItem.PURE_IND = rdr["PURE_IND"].ToString();
                msdsItem.RADIOACTIVE_IND = rdr["RADIOACTIVE_IND"].ToString();
                msdsItem.SERVICE_AGENCY = rdr["SERVICE_AGENCY"].ToString();
                msdsItem.TRADE_NAME = rdr["TRADE_NAME"].ToString();
                msdsItem.TRADE_SECRET_IND = rdr["TRADE_SECRET_IND"].ToString();
                // MSDS file
                if (rdr["MSDS_FILE"] != DBNull.Value)
                    msdsItem.msds_file = (byte[])rdr["MSDS_FILE"];
                msdsItem.file_name = rdr["file_name"].ToString();
                DateTime dtExtract;
                DateTime.TryParse(rdr["created"].ToString(), out dtExtract);
                msdsItem.CreatedDate = dtExtract;
                msdsItem.CreatedString = msdsItem.CreatedDate.ToShortDateString();

                Debug.WriteLine("CreatedDate = " + msdsItem.CreatedString);
            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            // Get the details from the other msds tables
            // Radiological_Info
            DataTable dtTemp = findMSDSradiologicalInfo(msds_id);
            if (dtTemp.Rows.Count > 0)
            {
                msdsItem.NRC_LP_NUM = dtTemp.Rows[0]["NRC_LP_NUM"].ToString();
                msdsItem.OPERATOR = dtTemp.Rows[0]["OPERATOR"].ToString();
                msdsItem.RAD_AMOUNT_MICRO = dtTemp.Rows[0]["RAD_AMOUNT_MICRO"].ToString();
                msdsItem.RAD_FORM = dtTemp.Rows[0]["RAD_FORM"].ToString();
                msdsItem.RAD_CAS = dtTemp.Rows[0]["RAD_CAS"].ToString();
                msdsItem.RAD_NAME = dtTemp.Rows[0]["RAD_NAME"].ToString();
                msdsItem.RAD_SYMBOL = dtTemp.Rows[0]["RAD_SYMBOL"].ToString();
                msdsItem.REP_NSN = dtTemp.Rows[0]["REP_NSN"].ToString();
                msdsItem.SEALED = dtTemp.Rows[0]["SEALED"].ToString();
                // Transportation
                dtTemp = findMSDStransportation(msds_id);
                msdsItem.AF_MMAC_CODE = dtTemp.Rows[0]["AF_MMAC_CODE"].ToString();
                msdsItem.CERTIFICATE_COE = dtTemp.Rows[0]["CERTIFICATE_COE"].ToString();
                msdsItem.COMPETENT_CAA = dtTemp.Rows[0]["COMPETENT_CAA"].ToString();
                msdsItem.DOD_ID_CODE = dtTemp.Rows[0]["DOD_ID_CODE"].ToString();
                msdsItem.DOT_EXEMPTION_NO = dtTemp.Rows[0]["DOT_EXEMPTION_NO"].ToString();
                msdsItem.DOT_RQ_IND = dtTemp.Rows[0]["DOT_RQ_IND"].ToString();
                msdsItem.EX_NO = dtTemp.Rows[0]["EX_NO"].ToString();
                msdsItem.LTD_QTY_IND = dtTemp.Rows[0]["LTD_QTY_IND"].ToString();
                msdsItem.MAGNETIC_IND = dtTemp.Rows[0]["MAGNETIC_IND"].ToString();
                msdsItem.MAGNETISM = dtTemp.Rows[0]["MAGNETISM"].ToString();
                msdsItem.MARINE_POLLUTANT_IND = dtTemp.Rows[0]["MARINE_POLLUTANT_IND"].ToString();
                msdsItem.NET_EXP_WEIGHT = dtTemp.Rows[0]["NET_EXP_WEIGHT"].ToString();
                msdsItem.NET_PROPELLANT_WT = dtTemp.Rows[0]["NET_PROPELLANT_WT"].ToString();
                msdsItem.NOS_TECHNICAL_SHIPPING_NAME = dtTemp.Rows[0]["NOS_TECHNICAL_SHIPPING_NAME"].ToString();
                msdsItem.TRANSPORTATION_ADDITIONAL_DATA = dtTemp.Rows[0]["TRANSPORTATION_ADDITIONAL_DATA"].ToString();
                msdsItem.FLASH_POINT_TEMP = dtTemp.Rows[0]["FLASH_PT_TEMP"].ToString();
                if (dtTemp.Rows[0]["HIGH_EXPLOSIVE_WT"].ToString().Length != 0)
                    msdsItem.HIGH_EXPLOSIVE_WT = Convert.ToInt32(dtTemp.Rows[0]["HIGH_EXPLOSIVE_WT"].ToString());
                if (dtTemp.Rows[0]["NET_EXP_QTY_DIST"].ToString().Length != 0)
                    msdsItem.NET_EXP_QTY_DIST = Convert.ToInt32(dtTemp.Rows[0]["NET_EXP_QTY_DIST"].ToString());
                // Physical_Chemical
                dtTemp = findMSDSphysChemical(msds_id);
                msdsItem.APP_ODOR = dtTemp.Rows[0]["APP_ODOR"].ToString();
                msdsItem.VAPOR_PRESS = dtTemp.Rows[0]["VAPOR_PRESS"].ToString();
                msdsItem.VAPOR_DENS = dtTemp.Rows[0]["VAPOR_DENS"].ToString();
                msdsItem.SPECIFIC_GRAV = dtTemp.Rows[0]["SPECIFIC_GRAV"].ToString();
                msdsItem.VOC_POUNDS_GALLON = dtTemp.Rows[0]["VOC_POUNDS_GALLON"].ToString();
                msdsItem.VOC_GRAMS_LITER = dtTemp.Rows[0]["VOC_GRAMS_LITER"].ToString();
                msdsItem.PH = dtTemp.Rows[0]["PH"].ToString();
                msdsItem.VISCOSITY = dtTemp.Rows[0]["VISCOSITY"].ToString();
                msdsItem.EVAP_RATE_REF = dtTemp.Rows[0]["EVAP_RATE_REF"].ToString();
                msdsItem.SOL_IN_WATER = dtTemp.Rows[0]["SOL_IN_WATER"].ToString();
                msdsItem.PERCENT_VOL_VOLUME = dtTemp.Rows[0]["PERCENT_VOL_VOLUME"].ToString();
                msdsItem.AUTOIGNITION_TEMP = dtTemp.Rows[0]["AUTOIGNITION_TEMP"].ToString();
                msdsItem.CARCINOGEN_IND = dtTemp.Rows[0]["CARCINOGEN_IND"].ToString();
                msdsItem.EPA_ACUTE = dtTemp.Rows[0]["EPA_ACUTE"].ToString();
                msdsItem.EPA_CHRONIC = dtTemp.Rows[0]["EPA_CHRONIC"].ToString();
                msdsItem.EPA_FIRE = dtTemp.Rows[0]["EPA_FIRE"].ToString();
                msdsItem.EPA_PRESSURE = dtTemp.Rows[0]["EPA_PRESSURE"].ToString();
                msdsItem.EPA_REACTIVITY = dtTemp.Rows[0]["EPA_REACTIVITY"].ToString();
                msdsItem.NEUT_AGENT = dtTemp.Rows[0]["NEUT_AGENT"].ToString();
                msdsItem.NFPA_FLAMMABILITY = dtTemp.Rows[0]["NFPA_FLAMMABILITY"].ToString();
                msdsItem.NFPA_HEALTH = dtTemp.Rows[0]["NFPA_HEALTH"].ToString();
                msdsItem.NFPA_REACTIVITY = dtTemp.Rows[0]["NFPA_REACTIVITY"].ToString();
                msdsItem.NFPA_SPECIAL = dtTemp.Rows[0]["NFPA_SPECIAL"].ToString();
                msdsItem.OSHA_CARCINOGENS = dtTemp.Rows[0]["OSHA_CARCINOGENS"].ToString();
                msdsItem.OSHA_COMB_LIQUID = dtTemp.Rows[0]["OSHA_COMB_LIQUID"].ToString();
                msdsItem.OSHA_COMP_GAS = dtTemp.Rows[0]["OSHA_COMP_GAS"].ToString();
                msdsItem.OSHA_CORROSIVE = dtTemp.Rows[0]["OSHA_CORROSIVE"].ToString();
                msdsItem.OSHA_EXPLOSIVE = dtTemp.Rows[0]["OSHA_EXPLOSIVE"].ToString();
                msdsItem.OSHA_FLAMMABLE = dtTemp.Rows[0]["OSHA_FLAMMABLE"].ToString();
                msdsItem.OSHA_HIGH_TOXIC = dtTemp.Rows[0]["OSHA_HIGH_TOXIC"].ToString();
                msdsItem.OSHA_IRRITANT = dtTemp.Rows[0]["OSHA_IRRITANT"].ToString();
                msdsItem.OSHA_ORG_PEROX = dtTemp.Rows[0]["OSHA_ORG_PEROX"].ToString();
                msdsItem.OSHA_OTHERLONGTERM = dtTemp.Rows[0]["OSHA_OTHERLONGTERM"].ToString();
                msdsItem.OSHA_OXIDIZER = dtTemp.Rows[0]["OSHA_OXIDIZER"].ToString();
                msdsItem.OSHA_PYRO = dtTemp.Rows[0]["OSHA_PYRO"].ToString();
                msdsItem.OSHA_SENSITIZER = dtTemp.Rows[0]["OSHA_SENSITIZER"].ToString();
                msdsItem.OSHA_TOXIC = dtTemp.Rows[0]["OSHA_TOXIC"].ToString();
                msdsItem.OSHA_UNST_REACT = dtTemp.Rows[0]["OSHA_UNST_REACT"].ToString();
                msdsItem.OTHER_SHORT_TERM = dtTemp.Rows[0]["OTHER_SHORT_TERM"].ToString();
                msdsItem.PHYS_STATE_CODE = dtTemp.Rows[0]["PHYS_STATE_CODE"].ToString();
                msdsItem.VOL_ORG_COMP_WT = dtTemp.Rows[0]["VOL_ORG_COMP_WT"].ToString();
                msdsItem.OSHA_WATER_REACTIVE = dtTemp.Rows[0]["OSHA_WATER_REACTIVE"].ToString();
                if (dtTemp.Rows[0]["FLASH_PT_TEMP"].ToString().Length != 0)
                    msdsItem.FLASH_PT_TEMP = Convert.ToInt32(Math.Round(Convert.ToDouble(dtTemp.Rows[0]["FLASH_PT_TEMP"].ToString())));
                // DOT_PSN 
                dtTemp = findMSDSdotPSN(msds_id);
                msdsItem.DOT_HAZARD_CLASS_DIV = dtTemp.Rows[0]["DOT_HAZARD_CLASS_DIV"].ToString();
                msdsItem.DOT_HAZARD_LABEL = dtTemp.Rows[0]["DOT_HAZARD_LABEL"].ToString();
                msdsItem.DOT_MAX_CARGO = dtTemp.Rows[0]["DOT_MAX_CARGO"].ToString();
                msdsItem.DOT_MAX_PASSENGER = dtTemp.Rows[0]["DOT_MAX_PASSENGER"].ToString();
                msdsItem.DOT_PACK_BULK = dtTemp.Rows[0]["DOT_PACK_BULK"].ToString();
                msdsItem.DOT_PACK_EXCEPTIONS = dtTemp.Rows[0]["DOT_PACK_EXCEPTIONS"].ToString();
                msdsItem.DOT_PACK_NONBULK = dtTemp.Rows[0]["DOT_PACK_NONBULK"].ToString();
                msdsItem.DOT_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["DOT_PROP_SHIP_MODIFIER"].ToString();
                msdsItem.DOT_PROP_SHIP_NAME = dtTemp.Rows[0]["DOT_PROP_SHIP_NAME"].ToString();
                msdsItem.DOT_PSN_CODE = dtTemp.Rows[0]["DOT_PSN_CODE"].ToString();
                msdsItem.DOT_SPECIAL_PROVISION = dtTemp.Rows[0]["DOT_SPECIAL_PROVISION"].ToString();
                msdsItem.DOT_SYMBOLS = dtTemp.Rows[0]["DOT_SYMBOLS"].ToString();
                msdsItem.DOT_UN_ID_NUMBER = dtTemp.Rows[0]["DOT_UN_ID_NUMBER"].ToString();
                msdsItem.DOT_WATER_OTHER_REQ = dtTemp.Rows[0]["DOT_WATER_OTHER_REQ"].ToString();
                msdsItem.DOT_WATER_VESSEL_STOW = dtTemp.Rows[0]["DOT_WATER_VESSEL_STOW"].ToString();
                msdsItem.DOT_PACK_GROUP = dtTemp.Rows[0]["DOT_PACK_GROUP"].ToString();
                // AFJM_PSN
                dtTemp = findMSDSafjmPSN(msds_id);
                if (dtTemp.Rows.Count > 0)
                {
                    msdsItem.AFJM_HAZARD_CLASS = dtTemp.Rows[0]["AFJM_HAZARD_CLASS"].ToString();
                    msdsItem.AFJM_PACK_GROUP = dtTemp.Rows[0]["AFJM_PACK_GROUP"].ToString();
                    msdsItem.AFJM_PACK_PARAGRAPH = dtTemp.Rows[0]["AFJM_PACK_PARAGRAPH"].ToString();
                    msdsItem.AFJM_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["AFJM_PROP_SHIP_MODIFIER"].ToString();
                    msdsItem.AFJM_PROP_SHIP_NAME = dtTemp.Rows[0]["AFJM_PROP_SHIP_NAME"].ToString();
                    msdsItem.AFJM_PSN_CODE = dtTemp.Rows[0]["AFJM_PSN_CODE"].ToString();
                    msdsItem.AFJM_SPECIAL_PROV = dtTemp.Rows[0]["AFJM_SPECIAL_PROV"].ToString();
                    msdsItem.AFJM_SUBSIDIARY_RISK = dtTemp.Rows[0]["AFJM_SUBSIDIARY_RISK"].ToString();
                    msdsItem.AFJM_SYMBOLS = dtTemp.Rows[0]["AFJM_SYMBOLS"].ToString();
                    msdsItem.AFJM_UN_ID_NUMBER = dtTemp.Rows[0]["AFJM_UN_ID_NUMBER"].ToString();
                }
                // IATR_PSN
                dtTemp = findMSDSiataPSN(msds_id);
                msdsItem.IATA_CARGO_PACKING = dtTemp.Rows[0]["IATA_CARGO_PACKING"].ToString();
                msdsItem.IATA_HAZARD_CLASS = dtTemp.Rows[0]["IATA_HAZARD_CLASS"].ToString();
                msdsItem.IATA_HAZARD_LABEL = dtTemp.Rows[0]["IATA_HAZARD_LABEL"].ToString();
                msdsItem.IATA_PACK_GROUP = dtTemp.Rows[0]["IATA_PACK_GROUP"].ToString();
                msdsItem.IATA_PASS_AIR_MAX_QTY = dtTemp.Rows[0]["IATA_PASS_AIR_MAX_QTY"].ToString();
                msdsItem.IATA_PASS_AIR_PACK_LMT_INSTR = dtTemp.Rows[0]["IATA_PASS_AIR_PACK_LMT_PER_PKG"].ToString();
                msdsItem.IATA_PASS_AIR_PACK_LMT_PER_PKG = dtTemp.Rows[0]["IATA_PASS_AIR_PACK_LMT_PER_PKG"].ToString();
                msdsItem.IATA_PASS_AIR_PACK_NOTE = dtTemp.Rows[0]["IATA_PASS_AIR_PACK_NOTE"].ToString();
                msdsItem.IATA_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["IATA_PROP_SHIP_MODIFIER"].ToString();
                msdsItem.IATA_PROP_SHIP_NAME = dtTemp.Rows[0]["IATA_PROP_SHIP_NAME"].ToString();
                msdsItem.IATA_CARGO_PACK_MAX_QTY = dtTemp.Rows[0]["IATA_CARGO_PACK_MAX_QTY"].ToString();
                msdsItem.IATA_PSN_CODE = dtTemp.Rows[0]["IATA_PSN_CODE"].ToString();
                msdsItem.IATA_SPECIAL_PROV = dtTemp.Rows[0]["IATA_SPECIAL_PROV"].ToString();
                msdsItem.IATA_SUBSIDIARY_RISK = dtTemp.Rows[0]["IATA_SUBSIDIARY_RISK"].ToString();
                msdsItem.IATA_UN_ID_NUMBER = dtTemp.Rows[0]["IATA_UN_ID_NUMBER"].ToString();
                // IMO_PSN
                dtTemp = findMSDSimoPSN(msds_id);
                msdsItem.IMO_EMS_NO = dtTemp.Rows[0]["IMO_EMS_NO"].ToString();
                msdsItem.IMO_HAZARD_CLASS = dtTemp.Rows[0]["IMO_HAZARD_CLASS"].ToString();
                msdsItem.IMO_IBC_INSTR = dtTemp.Rows[0]["IMO_IBC_INSTR"].ToString();
                msdsItem.IMO_LIMITED_QTY = dtTemp.Rows[0]["IMO_LIMITED_QTY"].ToString();
                msdsItem.IMO_PACK_GROUP = dtTemp.Rows[0]["IMO_PACK_GROUP"].ToString();
                msdsItem.IMO_PACK_INSTRUCTIONS = dtTemp.Rows[0]["IMO_PACK_INSTRUCTIONS"].ToString();
                msdsItem.IMO_PACK_PROVISIONS = dtTemp.Rows[0]["IMO_PACK_PROVISIONS"].ToString();
                msdsItem.IMO_PROP_SHIP_NAME = dtTemp.Rows[0]["IMO_PROP_SHIP_NAME"].ToString();
                msdsItem.IMO_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["IMO_PROP_SHIP_MODIFIER"].ToString();
                msdsItem.IMO_PSN_CODE = dtTemp.Rows[0]["IMO_PSN_CODE"].ToString();
                msdsItem.IMO_SPECIAL_PROV = dtTemp.Rows[0]["IMO_SPECIAL_PROV"].ToString();
                msdsItem.IMO_STOW_SEGR = dtTemp.Rows[0]["IMO_STOW_SEGR"].ToString();
                msdsItem.IMO_SUBSIDIARY_RISK = dtTemp.Rows[0]["IMO_SUBSIDIARY_RISK"].ToString();
                msdsItem.IMO_TANK_INSTR_IMO = dtTemp.Rows[0]["IMO_TANK_INSTR_IMO"].ToString();
                msdsItem.IMO_TANK_INSTR_PROV = dtTemp.Rows[0]["IMO_TANK_INSTR_PROV"].ToString();
                msdsItem.IMO_TANK_INSTR_UN = dtTemp.Rows[0]["IMO_TANK_INSTR_UN"].ToString();
                msdsItem.IMO_UN_NUMBER = dtTemp.Rows[0]["IMO_UN_NUMBER"].ToString();
                msdsItem.IMO_IBC_PROVISIONS = dtTemp.Rows[0]["IMO_IBC_PROVISIONS"].ToString();
                // Item_Description
                dtTemp = findMSDSitemDescription(msds_id);
                msdsItem.ITEM_MANAGER = dtTemp.Rows[0]["ITEM_MANAGER"].ToString();
                msdsItem.ITEM_NAME = dtTemp.Rows[0]["ITEM_NAME"].ToString();
                msdsItem.SPECIFICATION_NUMBER = dtTemp.Rows[0]["SPECIFICATION_NUMBER"].ToString();
                msdsItem.TYPE_GRADE_CLASS = dtTemp.Rows[0]["TYPE_GRADE_CLASS"].ToString();
                msdsItem.UNIT_OF_ISSUE = dtTemp.Rows[0]["UNIT_OF_ISSUE"].ToString();
                msdsItem.QUANTITATIVE_EXPRESSION = dtTemp.Rows[0]["QUANTITATIVE_EXPRESSION"].ToString();
                msdsItem.UI_CONTAINER_QTY = dtTemp.Rows[0]["UI_CONTAINER_QTY"].ToString();
                msdsItem.BATCH_NUMBER = dtTemp.Rows[0]["BATCH_NUMBER"].ToString();
                msdsItem.LOT_NUMBER = dtTemp.Rows[0]["LOT_NUMBER"].ToString();
                msdsItem.LOG_FLIS_NIIN_VER = dtTemp.Rows[0]["LOG_FLIS_NIIN_VER"].ToString();
                msdsItem.NET_UNIT_WEIGHT = dtTemp.Rows[0]["NET_UNIT_WEIGHT"].ToString();
                msdsItem.TYPE_OF_CONTAINER = dtTemp.Rows[0]["TYPE_OF_CONTAINER"].ToString();
                msdsItem.SHELF_LIFE_CODE = dtTemp.Rows[0]["SHELF_LIFE_CODE"].ToString();
                msdsItem.SPECIAL_EMP_CODE = dtTemp.Rows[0]["SPECIAL_EMP_CODE"].ToString();
                msdsItem.UN_NA_NUMBER = dtTemp.Rows[0]["UN_NA_NUMBER"].ToString();
                msdsItem.UPC_GTIN = dtTemp.Rows[0]["UPC_GTIN"].ToString();
                if (dtTemp.Rows[0]["LOG_FSC"].ToString().Length != 0)
                    msdsItem.LOG_FSC = Convert.ToInt32(dtTemp.Rows[0]["LOG_FSC"].ToString());
                // Label_Info
                dtTemp = findMSDSlabelInfo(msds_id);
                msdsItem.COMPANY_CAGE_RP = dtTemp.Rows[0]["COMPANY_CAGE_RP"].ToString();
                msdsItem.COMPANY_NAME_RP = dtTemp.Rows[0]["COMPANY_NAME_RP"].ToString();
                msdsItem.LABEL_EMERG_PHONE = dtTemp.Rows[0]["LABEL_EMERG_PHONE"].ToString();
                msdsItem.LABEL_ITEM_NAME = dtTemp.Rows[0]["LABEL_ITEM_NAME"].ToString();
                msdsItem.LABEL_PROC_YEAR = dtTemp.Rows[0]["LABEL_PROC_YEAR"].ToString();
                msdsItem.LABEL_PROD_IDENT = dtTemp.Rows[0]["LABEL_PROD_IDENT"].ToString();
                msdsItem.LABEL_PROD_SERIALNO = dtTemp.Rows[0]["LABEL_PROD_SERIALNO"].ToString();
                msdsItem.LABEL_SIGNAL_WORD = dtTemp.Rows[0]["LABEL_SIGNAL_WORD"].ToString();
                msdsItem.LABEL_STOCK_NO = dtTemp.Rows[0]["LABEL_STOCK_NO"].ToString();
                msdsItem.SPECIFIC_HAZARDS = dtTemp.Rows[0]["SPECIFIC_HAZARDS"].ToString();
                // DocuemntTypes
                dtTemp = findMSDSdocTypes(msds_id);
                if (dtTemp.Rows[0]["MSDS_TRANSLATED"] != DBNull.Value)
                    msdsItem.MSDS_TRANSLATED = (byte[])dtTemp.Rows[0]["MSDS_TRANSLATED"];
                if (dtTemp.Rows[0]["NESHAP_COMP_CERT"] != DBNull.Value)
                    msdsItem.NESHAP_COMP_CERT = (byte[])dtTemp.Rows[0]["NESHAP_COMP_CERT"];
                if (dtTemp.Rows[0]["OTHER_DOCS"] != DBNull.Value)
                    msdsItem.OTHER_DOCS = (byte[])dtTemp.Rows[0]["OTHER_DOCS"];
                if (dtTemp.Rows[0]["PRODUCT_SHEET"] != DBNull.Value)
                    msdsItem.PRODUCT_SHEET = (byte[])dtTemp.Rows[0]["PRODUCT_SHEET"];
                if (dtTemp.Rows[0]["TRANSPORTATION_CERT"] != DBNull.Value)
                    msdsItem.TRANSPORTATION_CERT = (byte[])dtTemp.Rows[0]["TRANSPORTATION_CERT"];
                msdsItem.msds_translated_filename = dtTemp.Rows[0]["msds_translated_filename"].ToString();
                msdsItem.neshap_comp_filename = dtTemp.Rows[0]["neshap_comp_filename"].ToString();
                msdsItem.other_docs_filename = dtTemp.Rows[0]["other_docs_filename"].ToString();
                msdsItem.product_sheet_filename = dtTemp.Rows[0]["product_sheet_filename"].ToString();
                msdsItem.transportation_cert_filename = dtTemp.Rows[0]["transportation_cert_filename"].ToString();
                msdsItem.manufacturer_label_filename = dtTemp.Rows[0]["manufacturer_label_filename"].ToString();
                // Disposal
                dtTemp = findMSDSdisposal(msds_id);
                msdsItem.DISPOSAL_ADD_INFO = dtTemp.Rows[0]["DISPOSAL_ADD_INFO"].ToString();
                msdsItem.EPA_HAZ_WASTE_CODE = dtTemp.Rows[0]["EPA_HAZ_WASTE_CODE"].ToString();
                msdsItem.EPA_HAZ_WASTE_IND = dtTemp.Rows[0]["EPA_HAZ_WASTE_IND"].ToString();
                msdsItem.EPA_HAZ_WASTE_NAME = dtTemp.Rows[0]["EPA_HAZ_WASTE_NAME"].ToString();
            }

            // Now for the lists
            // Ingredients
            dtTemp = findMSDSingredients(msds_id);
            foreach (DataRow row in dtTemp.Rows)
            {
                // Get the ingredient object 
                MsdsIngredients ingredient = buildMsdsIngredient(row);
                msdsItem.ingredientsList.Add(ingredient);
            }
            // Contractors
            dtTemp = findMSDScontractorInfo(msds_id);
            foreach (DataRow row in dtTemp.Rows)
            {
                // Get the contractor_info object 
                MsdsContractors contractor = buildMsdsContractor(row);
                msdsItem.contractorsList.Add(contractor);
            }

            return msdsItem;
        }

        private MsdsIngredients buildMsdsIngredient(DataRow row)
        {
            MsdsIngredients item = new MsdsIngredients();

            item.CAS = row["CAS"].ToString();
            item.RTECS_NUM = row["RTECS_NUM"].ToString();
            item.RTECS_CODE = row["RTECS_CODE"].ToString();
            item.INGREDIENT_NAME = row["INGREDIENT_NAME"].ToString();
            item.PRCNT = row["PRCNT"].ToString();
            item.OSHA_PEL = row["OSHA_PEL"].ToString();
            item.OSHA_STEL = row["OSHA_STEL"].ToString();
            item.ACGIH_TLV = row["ACGIH_TLV"].ToString();
            item.ACGIH_STEL = row["ACGIH_STEL"].ToString();
            item.EPA_REPORT_QTY = row["EPA_REPORT_QTY"].ToString();
            item.DOT_REPORT_QTY = row["DOT_REPORT_QTY"].ToString();
            item.PRCNT_VOL_VALUE = row["PRCNT_VOL_VALUE"].ToString();
            item.PRCNT_VOL_WEIGHT = row["PRCNT_VOL_WEIGHT"].ToString();
            item.CHEM_MFG_COMP_NAME = row["CHEM_MFG_COMP_NAME"].ToString();
            item.ODS_IND = row["ODS_IND"].ToString();
            item.OTHER_REC_LIMITS = row["OTHER_REC_LIMITS"].ToString();

            return item;
        }

        private MsdsContractors buildMsdsContractor(DataRow row)
        {
            MsdsContractors item = new MsdsContractors();

            item.CT_NUMBER = row["CT_NUMBER"].ToString();
            item.CT_CAGE = row["CT_CAGE"].ToString();
            item.CT_CITY = row["CT_CITY"].ToString();
            item.CT_COMPANY_NAME = row["CT_COMPANY_NAME"].ToString();
            item.CT_COUNTRY = row["CT_COUNTRY"].ToString();
            item.CT_PO_BOX = row["CT_PO_BOX"].ToString();
            item.CT_PHONE = row["CT_PHONE"].ToString();
            item.PURCHASE_ORDER_NO = row["PURCHASE_ORDER_NO"].ToString();
            item.CT_STATE = row["CT_STATE"].ToString();

            return item;
        }

        public void checkForUpdates() {
            DatabaseUpdater databaseUpdater = new DatabaseUpdater();
            SqlConnection connection = null;

            try {
                if (MDF_CONNECTION != null) {
                    Debug.WriteLine("ConnectionString = \"{0}\"",
                        MDF_CONNECTION);

                    connection = new SqlConnection(MDF_CONNECTION);

                    databaseUpdater.update(connection);
                }
                else
                    Debug.WriteLine("No HAZMAT ConnectionString");
            }
            catch (Exception ex) {
                Debug.WriteLine("Error in checkForUpdates - " + ex.Message);
            }
            finally {
                connection.Close();
            }

            // Make sure we have backup directory
            checkForBackupFolder();
        }

        private void checkForBackupFolder() {
            try {
                // See if the backup folder exists
                if (!Directory.Exists (DB_BACKUP_DIRECTORY)) {
                    // Create the folder
                    Directory.CreateDirectory (DB_BACKUP_DIRECTORY);
                    Debug.WriteLine("Created backup directory - " + DB_BACKUP_DIRECTORY);
                }
            }
            catch (Exception ex) {
                Debug.WriteLine("Error creating backup directory - " + ex.Message);
            }
        }

        public DataTable getMSDSWithChangedHCC(string filterColumn, string filterText) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            StringBuilder selectStmt = new StringBuilder();
            selectStmt.Append("select m1.msds_id AS m1_msds_id, m1.NIIN AS m1_NIIN, ");
            selectStmt.Append("  m1.MSDSSERNO AS m1_MSDSSERNO, m1.CAGE AS m1_CAGE, ");
            selectStmt.Append("  m1.MANUFACTURER AS m1_Manufacturer, m1.PARTNO AS m1_PartNo, ");
            selectStmt.Append("  (CASE m1.manually_entered WHEN 0 THEN 'Y' ELSE '' END) AS m1_HMIRS, ");
            selectStmt.Append("  COALESCE(m1.DESCRIPTION, m1.PRODUCT_IDENTITY) AS m1_Description, ");
            selectStmt.Append("  CONVERT(varchar, COALESCE(m1.created, m1.product_load_date), 101) AS m1_Load_Date, ");
            selectStmt.Append("  hc1.hcc AS m1_HCC, hc1.description AS m1_HCC_Description, ");
            selectStmt.Append("  m2.msds_id AS m2_msds_id, m2.NIIN AS m2_NIIN, ");
            selectStmt.Append("  m2.MSDSSERNO AS m2_MSDSSERNO, m2.CAGE AS m2_CAGE, ");
            selectStmt.Append("  m2.MANUFACTURER AS m2_Manufacturer, m2.PARTNO AS m2_PartNo, ");
            selectStmt.Append("  (CASE m2.manually_entered WHEN 0 THEN 'Y' ELSE '' END) AS m2_HMIRS, ");
            selectStmt.Append("  COALESCE(m2.DESCRIPTION, m2.PRODUCT_IDENTITY) AS m2_Description, ");
            selectStmt.Append("  CONVERT(varchar, COALESCE(m2.created, m2.product_load_date), 101) AS m2_Load_Date, ");
            selectStmt.Append(  "hc2.hcc AS m2_HCC, hc2.description AS m2_HCC_Description ");
            selectStmt.Append("from msds m1 ");
            selectStmt.Append("  left outer join hcc hc1 on hc1.hcc_id = m1.hcc_id ");
            selectStmt.Append("  join msds m2 on m2.MSDSSERNO = m1.MSDSSERNO ");
            selectStmt.Append("  left outer join hcc hc2 on hc2.hcc_id = m2.hcc_id ");
            selectStmt.Append("  where ISNULL(m2.hcc_id, 0) != ISNULL(m1.hcc_id, 0) ");
            selectStmt.Append("  and m1.msds_id = (SELECT MIN(msds_id) FROM msds where MSDSSERNO = m1.MSDSSERNO) ");
            selectStmt.Append("  and m2.msds_id = (SELECT MAX(msds_id) FROM msds where MSDSSERNO = m1.MSDSSERNO) ");

            // See if we are doing any filtering
            if (!filterText.Equals("")) {
                if (filterColumn.Equals("MSDSSERNO")) {
                    selectStmt.Append("  AND m1.MSDSSERNO LIKE '%' + @filter + '%' ");
                }
                else if (filterColumn.Equals("HCC")) {
                    selectStmt.Append("  AND (hc1.hcc LIKE '%' + @filter + '%' " +
                        "OR hc2.hcc LIKE '%' + @filter + '%') ");
                }
            }

            selectStmt.Append("ORDER BY m1.MSDSSERNO");

            // Run the command
            SqlCommand cmd = new SqlCommand(selectStmt.ToString(), conn);
            cmd.Parameters.AddWithValue("@filter", filterText);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public String getLastSMCLUploadDate() {
            String uploadDateFormatted = null;

            List<FileLoadInfo> fileInfo = getFileTableInfo(FILE_TABLES.FileUpload,
                FILE_TYPES.SMCL.ToString(), true);

            if (fileInfo.Count > 0) {
                uploadDateFormatted = fileInfo[0].FileName + " - Load Date: "
                    + String.Format("{0:MM/dd/yyyy HHmm}", fileInfo[0].ProcessDate);
            }
            else {
                uploadDateFormatted = "";
            }

            return uploadDateFormatted;
        }

        #region File Tables
        public bool updateFileTable(FILE_TABLES fileTable, String fileName, String fileType,
            DateTime processedDate, String username,
            int records_added) {

            return updateFileTable(fileTable, fileName, fileType,
                processedDate, username, 
                records_added, 0, 0);
        }
        public bool updateFileTable(FILE_TABLES fileTable, String fileName, String fileType,
            DateTime processedDate, String username,
            int records_added, int records_changed, int records_deleted) {
            bool result = false;
            string sqlStmt = "INSERT INTO file_uploads (" +
                "file_name, file_type, upload_date, uploaded_by, " +
                "records_added, records_changed, records_deleted) " +
                "VALUES(@file_name, @file_type,@process_date, @processed_by, " +
                "@records_added, @records_changed, @records_deleted)";

            if (fileTable == FILE_TABLES.FileDownLoad)
                sqlStmt = "INSERT INTO file_downloads (" +
                    "file_name, file_type, download_date, downloaded_by, " +
                    "records_downloaded) " +
                    "VALUES(@file_name, @file_type, @process_date, @processed_by, " +
                    "@records_added)";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@file_name", fileName);
                insertCmd.Parameters.AddWithValue("@file_type", fileType);
                insertCmd.Parameters.AddWithValue("@process_date", processedDate);
                insertCmd.Parameters.AddWithValue("@processed_by", username);
                insertCmd.Parameters.AddWithValue("@records_added", records_added);
                insertCmd.Parameters.AddWithValue("@records_changed", records_changed);
                insertCmd.Parameters.AddWithValue("@records_deleted", records_deleted);

                insertCmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex) {
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public List<FileLoadInfo> getFileTableInfo(FILE_TABLES fileTable, String fileType, bool latest) {
            List<FileLoadInfo> fileInfo = new List<FileLoadInfo>();

            string sqlStmt = "SELECT file_name, file_type, upload_date AS process_date, " +
                "uploaded_by AS processed_by, " +
                "records_added, records_changed, records_deleted FROM file_uploads " +
                "WHERE file_type = '" + fileType + "'";

            if (fileTable == FILE_TABLES.FileDownLoad)
                sqlStmt = "SELECT file_name, file_type, download_date AS process_date, " +
                "downloaded_by AS processed_by, " +
                "records_downloaded AS records_added, 0 AS records_changed, 0 AS records_deleted FROM file_downloads " +
                "WHERE file_type = '" + fileType + "'";

            if (latest)
                sqlStmt += " ORDER BY process_date DESC";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);
            SqlDataReader rdr = cmd.ExecuteReader();

            if (latest) {
                // Read just the latest record
                if (rdr.Read()) {
                    FileLoadInfo info = new FileLoadInfo();
                    info.FileName = Convert.ToString(rdr["file_name"]);
                    info.FileType = Convert.ToString(rdr["file_type"]);
                    info.ProcessDate = Convert.ToDateTime(rdr["process_date"]);
                    info.ProcessedBy = Convert.ToString(rdr["processed_by"]);
                    info.RecordsAdded = Convert.ToInt32(rdr["records_added"]);
                    info.RecordsChanged = Convert.ToInt32(rdr["records_changed"]);
                    info.RecordsDeleted = Convert.ToInt32(rdr["records_deleted"]);

                    fileInfo.Add(info);
                }
            }
            else {
                while (rdr.Read()) {
                    FileLoadInfo info = new FileLoadInfo();
                    info.FileName = Convert.ToString(rdr["file_name"]);
                    info.FileType = Convert.ToString(rdr["file_type"]);
                    info.ProcessDate = Convert.ToDateTime(rdr["process_date"]);
                    info.ProcessedBy = Convert.ToString(rdr["processed_by"]);
                    info.RecordsAdded = Convert.ToInt32(rdr["records_added"]);
                    info.RecordsChanged = Convert.ToInt32(rdr["records_changed"]);
                    info.RecordsDeleted = Convert.ToInt32(rdr["records_deleted"]);

                    fileInfo.Add(info);
                }
            }

            conn.Close();

            return fileInfo;
        }
        #endregion
    }
}
