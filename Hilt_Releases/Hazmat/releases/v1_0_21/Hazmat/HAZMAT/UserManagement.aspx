﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="HAZMAT.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .style1 {
            width: 43px;
        }
    </style>
    <script type="text/javascript" src="/resources/js/UserAdmin.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-header">
        <h1>User Administration
        </h1>
    </div>

    <hr id="line" />
    <table id="userTable" class="table" width="100%">
        <thead>
            <th>User Name</th>
            <th>Role</th>
            <th>Workcenter Name</th>
            <th>Delete</th>
        </thead>
    </table>
    <hr />

</asp:Content>

