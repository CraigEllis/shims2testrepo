﻿using System;
using System.Collections.Generic;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace HAZMAT
{
    public class GenerateLocationLabel
    {


 public string templateFilePath { get; set; }

 public GenerateLocationLabel(string templateFilePath)
        {
            this.templateFilePath = templateFilePath;

        }


        public string generateLabel(List<string> locationList)
        {
           
            String file = templateFilePath;

            List<string> filePathList = new List<string>();

            PdfReader reader = new PdfReader(file);

            String completedFilePath = Path.GetTempFileName();

            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        completedFilePath, FileMode.Create));
            AcroFields fields = stamper.AcroFields;


            //get page dimensions
            PdfContentByte pdfContentByte = stamper.GetOverContent(1);
            Rectangle rec = pdfContentByte.PdfDocument.PageSize;



            //add to list
            filePathList.Add(completedFilePath);

            int count = 0;

            foreach(string s in locationList)
            {
                count++;

                //set location name on label
                setLabelData(fields, s);

                //create barcode
                setBarcode(60f, 22, stamper, s);


                stamper.FormFlattening = true;
                stamper.Close();


                completedFilePath = Path.GetTempFileName();

                reader = new PdfReader(file);

                stamper = new PdfStamper(reader, new FileStream(
                            completedFilePath, FileMode.Create));
                fields = stamper.AcroFields;

                if (count != locationList.Count)
                {
                    filePathList.Add(completedFilePath);
                }


            }


            string combinedFile = "";

            //combine each individual pdf file created (if more than 6 labels were created) and return the file path
            if (filePathList.Count > 1)
            {
                combinedFile = Path.GetTempFileName();
                MergeFiles(combinedFile, filePathList, rec);

            }
            else
            {
                //set the completed file path to the file path saved (this file path should already be named correctly..)
                combinedFile = filePathList[0].ToString();


            }

            return combinedFile;
        }



        public void MergeFiles(string destinationFile, List<string> pathList, Rectangle rec)
        {

            rec = new Rectangle(140, 70);

            try
            {
                int f = 0;
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(pathList[f]);
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                Console.WriteLine("There are " + n + " pages in the original file.");
                // step 1: creation of a document-object
                Document document = new Document(rec, 10, 10, 42, 35);
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));
                // step 3: we open the document
                document.Open();
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;
                int rotation;
                // step 4: we add content
                while (f < pathList.Count)
                {
                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(rec);
                        document.NewPage();
                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, rec.Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                        Console.WriteLine("Processed page " + i);
                    }
                    f++;
                    if (f < pathList.Count)
                    {
                        reader = new PdfReader(pathList[f]);
                        // we retrieve the total number of pages
                        n = reader.NumberOfPages;
                        Console.WriteLine("There are " + n + " pages in the original file.");
                    }
                }
                // step 5: we close the document
                document.Close();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }

        }



        public void setBarcode(float x, float y, PdfStamper stamper, string s)
        {
            PdfContentByte pdfContentByte = stamper.GetOverContent(1);
            
            Barcode128 code = new Barcode128();
            code.Code = s.ToUpper();
            code.GenerateChecksum = false;
            
           Image image = code.CreateImageWithBarcode(pdfContentByte, BaseColor.BLACK, BaseColor.BLACK);
                   
            image.SetAbsolutePosition(11, 22);           
             image.ScaleAbsoluteWidth(120f);
             image.ScaleAbsoluteHeight(45f);
            pdfContentByte.AddImage(image);

        }


        public void setLabelData(AcroFields fields, string s)
        {

        }





    }


}