﻿using System.Configuration;

namespace HAZMAT
{
    public class Configuration
    {

        public static string ConnectionInfo
        {
            [CoverageExclude]
            get
            {

                return ConfigurationManager.ConnectionStrings["HAZMAT"].ConnectionString;

            }
        }
    }
}
