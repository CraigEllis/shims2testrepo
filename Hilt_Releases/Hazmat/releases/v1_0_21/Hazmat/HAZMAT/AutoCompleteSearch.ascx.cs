﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;

namespace HAZMAT
{
    [CoverageExclude]
    public partial class AutoCompleteSearch : System.Web.UI.UserControl, ICallbackEventHandler
    {
        #region "Properties"
        [CoverageExclude]
        private string strCallbackResult = string.Empty;
        [CoverageExclude]
        private string objCallbackScriptBlock = string.Empty;
        [CoverageExclude]
        private List<string> lstAutoFill = new List<string>();

        private int intMatchKeyCount;
        /// <summary>
        /// Perform search after this many characters have been entered
        /// </summary>
        /// <value>Integer >= 0</value>
        
        public int MatchKeyCount
        {
            [CoverageExclude]
            get { return intMatchKeyCount; }
            [CoverageExclude]
            set { intMatchKeyCount = value; }
        }

        private SearchType stSearchType;
        /// <summary>
        /// Search style is one of 3 types.
        /// - StartsWith matches any terms starting with the search phrase
        /// - Contains matches any terms containing the search phrase
        /// - EndsWith matches any terms ending with the search phrase
        /// </summary>
        /// <value>SearchStyle.Blah</value>
        
        public SearchType SearchStyle
        {
            [CoverageExclude]
            get { return stSearchType; }
            [CoverageExclude]
            set { stSearchType = value; }
        }
      
        private bool blnIsCaseSensitive;
        /// <summary>
        /// Signifies whether search is case-sensitive or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is case-sensitive; otherwise, <c>false; Default: false</c>.
        /// </value>
         
        public bool IsCaseSensitive
        {
            [CoverageExclude]
            get { return blnIsCaseSensitive; }
            [CoverageExclude]
            set { blnIsCaseSensitive = value; }
        }
         
        private DataTable dtAutoFillData;
        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value><c>DataTable</c> containing all Search Results</value>
        
        public DataTable DataSource
        {
            [CoverageExclude]
            get { return dtAutoFillData; }
            [CoverageExclude]
            set { dtAutoFillData = value; }
        }
         
        private string strDataValueColumn;
        /// <summary>
        /// Gets or sets the data source column to be used as the value submitted.
        /// </summary>
        /// <value>String name of the value column in the DataSource</value>
         
        public string DataValueColumn
        {
            [CoverageExclude]
            get { return strDataValueColumn; }
            [CoverageExclude]
            set { strDataValueColumn = value; }
        }
         
        private string strDataTextColumn;
        /// <summary>
        /// Gets or sets the text to be displayed in the option.
        /// </summary>
        /// <value>String name of the text column in the DataSource</value>
     
        public string DataTextColumn
        {
            [CoverageExclude]
            get { return strDataTextColumn; }
            [CoverageExclude]
            set { strDataTextColumn = value; }
        }
         
        private int intListCount;
        /// <summary>
        /// Gets or sets the number of results returned
        /// </summary>
        /// <value>Integer >= 1; Default: 10</value>
   
        public int ListCount
        {
            [CoverageExclude]
            get { return intListCount; }
            [CoverageExclude]
            set { intListCount = value; }
        }
      
        private int intCacheMinutes;
        /// <summary>
        /// Gets or sets the number of minutes to hold the object in cache.
        /// </summary>
        /// <value>Integer >= 0; Default: 60</value>
   
        public int CacheMinutes
        {
            [CoverageExclude]
            get { return intCacheMinutes; }
            [CoverageExclude]
            set { intCacheMinutes = value; }
        }
        
        private bool blnDisableCache;
        /// <summary>
        /// Gets or sets a indicating whether or not to cache data.
        /// </summary>
        /// <value><c>false</c> to cache data; <c>true</c> to reload data with each call. Default: true</value>
   
        public bool DisableCache
        {
            [CoverageExclude]
            get { return blnDisableCache; }
            [CoverageExclude]
            set { blnDisableCache = value; }
        }
       
        private bool blnAutoPostBack;
        /// <summary>
        /// Gets or sets a value indicating whether submit the form when a selection is made.
        /// </summary>
        /// <value><c>true</c> if [enable auto choose]; otherwise, <c>false</c>.</value>
        
        public bool AutoPostBack
        {
            [CoverageExclude]
            get { return blnAutoPostBack; }
            [CoverageExclude]
            set { blnAutoPostBack = value; }
        }
        
        private string strSelectedValue;
        /// <summary>
        /// Gets the value of the QueryBox.
        /// </summary>
        /// <value>String of the selected value.</value>
       
        public string SelectedValue
        {
            [CoverageExclude]
            get { return strSelectedValue; }
        }

        private string strAutoCompleteWrapperCssClass;
        /// <summary>
        /// Gets or sets the Container CSS class for the entire control.
        /// </summary>
        /// <value>CSS class name.</value>
      
        public string AutoCompleteWrapperCssClass
        {
            [CoverageExclude]
            get { return strAutoCompleteWrapperCssClass; }
            [CoverageExclude]
            set { strAutoCompleteWrapperCssClass = value; }
        }

        private string strListCSSClass;
        /// <summary>
        /// Gets or sets the List Container CSS class.
        /// </summary>
        /// <value>CSS class name.</value>

        public string ListCSSClass
        {
            [CoverageExclude]
            get { return strListCSSClass; }
            [CoverageExclude]
            set { strListCSSClass = value; }
        }

        private string strListOptionCSSClass;
        /// <summary>
        /// Gets or sets the normal list option CSS class.
        /// </summary>
        /// <value>CSS class name.</value>
   
        public string ListOptionCSSClass
        {
            [CoverageExclude]
            get { return strListOptionCSSClass; }
            [CoverageExclude]
            set { strListOptionCSSClass = value; }
        }

        private string strListOptionHighlightedCSSClass;
        /// <summary>
        /// Gets or sets the highlighted list option CSS class.
        /// </summary>
        /// <value>CSS class name.</value>
   
        public string ListOptionHighlightedCSSClass
        {
            [CoverageExclude]
            get { return strListOptionHighlightedCSSClass; }
            [CoverageExclude]
            set { strListOptionHighlightedCSSClass = value; }
        }

        private string strQueryBoxCSSClass;
        /// <summary>
        /// Gets or sets the query textbox CSS class.
        /// </summary>
        /// <value>CSS class name.</value>
       
        public string QueryBoxCSSClass
        {
            [CoverageExclude]
            get { return strQueryBoxCSSClass; }
            [CoverageExclude]
            set { strQueryBoxCSSClass = value; }
        }

        private string strButtonCSSClass;
        /// <summary>
        /// Gets or sets the submit button CSS class.
        /// </summary>
        /// <value>CSS class name.</value>
      
        public string ButtonCSSClass
        {
            [CoverageExclude]
            get { return strButtonCSSClass; }
            [CoverageExclude]
            set { strButtonCSSClass = value; }
        }

        private string strButtonText;
        /// <summary>
        /// Gets or sets the submit button text.
        /// </summary>
        /// <value>String</value>
      
        public string ButtonText
        {
            [CoverageExclude]
            get
            {
                if (strButtonText == null)
                {
                    strButtonText = "Search";
                }
                return strButtonText;
            }
            [CoverageExclude]
            set { strButtonText = value; }
        }

        /// <summary>
        /// Gets the callback script block.
        /// </summary>
        /// <value>The callback script block.</value>

        public string CallbackScriptBlock
        {
            [CoverageExclude]
            get { return objCallbackScriptBlock; }
        }

        /// <summary>
        /// Returns the results of a callback event that targets a control.
        /// </summary>
        /// <returns>The result of the callback.</returns>
        [CoverageExclude]
        public string GetCallbackResult()
        {

            return strCallbackResult;
        }

        //Added
        
        public string Text
        {
            [CoverageExclude]
            get
            {               
                return txtQuery.Text;
            }
            [CoverageExclude]
            set { txtQuery.Text = value; }
        }


        #endregion

        /// <summary>
        /// Speficies result string segment to match query string against
        /// </summary>
         [CoverageExclude]
        public enum SearchType
        {
            StartsWith,
            Contains,
            EndsWith
        }


        #region "Methods"
         [CoverageExclude]
        public void Page_Load(object sender, EventArgs e)
        {
            // Set style Defaults to page elements
            txtQuery.Attributes.Add("onkeyup", "GetAutoComplete();");
            txtQuery.CssClass = QueryBoxCSSClass;
            buttonSearch.CssClass = ButtonCSSClass;
            buttonSearch.Text = ButtonText;

            // create client side script block
            objCallbackScriptBlock = Page.ClientScript.GetCallbackEventReference(this, "strQuery", "fillAutoComplete", string.Empty, "onError", true);
        }

        /// <summary>
        /// Performs the comparison and fill a new Result list with matches.
        /// Returns a string of the results separated by a pipe (|)
        /// </summary>
        /// <param name="QueryText">The query text.</param>
         [CoverageExclude]
        public void RaiseCallbackEvent(string QueryText)
        {
            lstAutoFill = loadList();

            List<string> lstResults;

            lstResults = lstAutoFill.FindAll(delegate(string strDelegate) { return SearchList(strDelegate, QueryText, stSearchType, blnIsCaseSensitive); });

            if (lstResults.Count > intListCount)
            {
                lstResults = lstResults.GetRange(0, intListCount);
            }

            strCallbackResult = string.Join("|", lstResults.ToArray());
        }

        /// <summary>
        /// Compares Query Text with individual result
        /// </summary>
        /// <param name="strDelegate">Text being searched</param>
        /// <param name="strQueryText">Text to search for</param>
        /// <param name="stType">SearchType of the comparison to be made</param>
        /// <param name="blnCaseSens">Specifies if search is case-sensitive</param>
        /// <returns><c>true</c> for match, <c>false</c> otherwise</returns>
         [CoverageExclude]
        private static bool SearchList(string strDelegate, string strQueryText, SearchType stType, bool blnCaseSens)
        {
            if (strQueryText == string.Empty)
            {
                return false;
            }

            if (!blnCaseSens)
            {
                strQueryText = strQueryText.ToLowerInvariant();
                strDelegate = strDelegate.ToLowerInvariant();
            }

            switch (stType)
            {
                case SearchType.Contains:
                    return strDelegate.Contains(strQueryText);
                case SearchType.EndsWith:
                    return strDelegate.EndsWith(strQueryText);
                default:
                case SearchType.StartsWith:
                    return strDelegate.StartsWith(strQueryText);
            }
        }

         [CoverageExclude]
        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            // set the value of the control
            strSelectedValue = txtQuery.Text;

            // bubble up event
            onResultSelected(e);

        }

        /// <summary>
        /// Fills the auto complete list with all results.
        /// </summary>
        /// <returns>
        /// Generic String List
        /// </returns>
         [CoverageExclude]
        private List<string> loadList()
        {
            if (blnDisableCache || Cache["AutoCompleteList"] == null)
            {
                lstAutoFill.Clear();
                foreach (DataRow dr in dtAutoFillData.Rows)
                {
                    lstAutoFill.Add(dr[strDataTextColumn].ToString());
                }

                lstAutoFill.Sort();
            }
            else
            {
                lstAutoFill = (List<string>)Cache["AutoCompleteList"];
            }

            if (blnDisableCache == false)
            {
                Cache.Insert("AutoCompleteList", lstAutoFill, null, DateTime.Now.AddMinutes(10), TimeSpan.Zero);
            }

            return lstAutoFill;
        }
        #endregion

        #region "Events"
         [CoverageExclude]
        public class ResultSelectionCommandEventArgs
        {
            private string strResult;
            public string Value
            {
                get { return strResult; }
            }

            public ResultSelectionCommandEventArgs(string Result)
            {
                strResult = Result;
            }
        }

        public event EventHandler ResultSelected;
          [CoverageExclude]
        protected virtual void onResultSelected(EventArgs e)
        {
            if (ResultSelected != null)
            {
                ResultSelected(this, e);
            }
        }

        #endregion
    }
}