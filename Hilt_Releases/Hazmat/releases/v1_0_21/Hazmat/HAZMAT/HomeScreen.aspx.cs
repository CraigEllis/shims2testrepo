﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAZMAT
{
    public partial class HomeScreen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string current_ship_name = "" + Session["current_ship_name"];
            ShipName.InnerHtml = current_ship_name;
        }
    }
}