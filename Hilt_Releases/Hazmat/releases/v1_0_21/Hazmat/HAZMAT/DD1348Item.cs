﻿using System;

namespace HAZMAT
{
    public class DD1348Item
    {

            public string TPDollars {get; set;}
            public string TPCents {get; set;}
                   
             public string ItemNomen {get; set;}
          
             public string NSNa {get; set;}
             public string NSNb {get; set;}
             public string DocIden {get; set;}
             public string RIFrom {get; set;}
         
             public string UnitIss{get; set;}
             public string Quant{get; set;}

             public string UPDollars { get; set; }
             public string UPCents {get; set;}
                   
             public string SMCC { get; set; }
             public string SLCDescription { get; set; }
             public string SLACDescription { get; set; }


        //Details
             public string DOC_IDENT { get; set; }
             public string RI_FROM { get; set; }
             public string M_S { get; set; }
             public string SER { get; set; }
             public string SUPP_ADDRESS { get; set; }
             public string SIG { get; set; }
             public string FUND { get; set; }
             public string DISTRIBUTION { get; set; }
             public string PROJECT { get; set; }
             public string PRI { get; set; }
             public string REQ_DEL_DATE { get; set; }
             public string RI { get; set; }
             public string O_P { get; set; }
             public string COND { get; set; }
             public string MGT { get; set; }
             public string UNIT_DOLLARS { get; set; }
             public string UNIT_CTS { get; set; }
             public string TOTAL_DOLLARS { get; set; }
             public string TOTAL_CTS { get; set; }
             public string SHIP_FROM { get; set; }
             public string SHIP_TO { get; set; }
             public string MARK_FOR { get; set; }

             public string DOC_DATE { get; set; }
             public string NMFC { get; set; }
             public string TYPE_CARGO { get; set; }
             public string PS { get; set; }
             public string UP { get; set; }
             public string UNIT_WEIGHT { get; set; }
             public string UNIT_CUBE { get; set; }
             public string UFC { get; set; }
             public string SL { get; set; }
             public string FRGHT_CLASS_NOM { get; set; }
             public string ITEM_NOM { get; set; }
             public string HCC_MSG { get; set; }
             public string DMIL { get; set; }
             public string JON { get; set; }
             public string HCC { get; set; }
             public string CIIC { get; set; }
             public string TY_CARGO_MSG { get; set; }
             public string MSDS { get; set; }

             public string DOCUMENT_NUMBER { get; set; } //barcode
             public string RICUIQTY { get; set; } //barcode
             public string PCN { get; set; } //barcode

             public string manufacturer_date { get; set; } //MFRDT
             public string expiration_date { get; set; } //EXPDT

             public DateTime? date_value { get; set; } //for julian date in document number
             public int serial_number { get; set; }//serial 1-99
             public int iteration { get; set; } //which 1-99 set we are on.
             public string UIC { get; set; } //UIC
        
        public DD1348Item(string Nomenclature, string FSC, string NIIN, string CAGE, string MSDSSERNO, string OffloadQuantity, string UI, string SMCC, string SLCDescription, string SLACDescription)
        {
            ItemNomen = Nomenclature;
            NSNa = FSC;
            NSNb = NIIN;
            Quant = OffloadQuantity;
            UnitIss = UI;
            this.SMCC = SMCC;
            this.SLACDescription = SLACDescription;
            this.SLCDescription = SLCDescription;

            TPCents = "";
            TPDollars = "";
            DocIden = "";
            RIFrom = "";
            UPCents = "";
        }

        public DD1348Item()
        {

        }
    }
}