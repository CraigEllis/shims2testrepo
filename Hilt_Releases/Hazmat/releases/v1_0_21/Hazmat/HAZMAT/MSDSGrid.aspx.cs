﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;

namespace HAZMAT
{
    [CoverageExclude]
    public partial class MSDSGrid : Page
    {
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (!isAdmin() && !isSupplyOfficer() && !isSupplyUser())
                {
                    btnNewMSDS.Visible = false;
                }
                else
                {
                    btnNewMSDS.Visible = false;
                }

                if (isSupplyOfficer() || isSupplyUser() || isAdmin())
                {
                    int colcount = GridView1.Columns.Count;
                    GridView1.Columns[colcount - 2].Visible = true;
                }
                else
                {
                    int colcount = GridView1.Columns.Count;
                    GridView1.Columns[colcount - 2].Visible = false;
                }

            }

            loadSMCLGrid();

            //populate usage category drop down list
            if (DropDownListUsageCategory.Items.Count == 0)
            {
                List<ListItem> usageCategories = new DatabaseManager().getUsageCategoryList(true);
                foreach (ListItem item in usageCategories)
                {
                    DropDownListUsageCategory.Items.Add(item);
                }
            }

            //populate inventory/catalog group drop down list
            if (DropDownListCatalogGroup.Items.Count == 0)
            {
                List<ListItem> usageCategories = new DatabaseManager().getCatalogGroupList();
                foreach (ListItem item in usageCategories)
                {
                    DropDownListCatalogGroup.Items.Add(item);
                }
            }

        }


        private void loadSMCLGrid()
        {
            String filterColumn = "";
            String filterText = "";
            if (chkbox_filter.Checked)
            {
                filterColumn = DropDownListSearch.SelectedValue;
                filterText = TxtBoxSearch.Text;
            }
            String usageCategory = "";
            if (chkbox_usageCategory.Checked)
            {
                usageCategory = DropDownListUsageCategory.SelectedValue;
            }
            String catalogGroup = "";
            if (chkbox_catalogGroup.Checked)
            {
                catalogGroup = DropDownListCatalogGroup.SelectedValue;
            }
            String manuallyEnteredOnly = "";
            if (chkbox_ManuallyEntered.Checked) {
                manuallyEnteredOnly = "Y";
            }
            GridView1.DataSource = new DatabaseManager().getMSDSList(
                    filterColumn, filterText, usageCategory, catalogGroup, manuallyEnteredOnly).DefaultView;
            GridView1.DataBind();
        }

        private void loadMSDSPage(bool canEdit)
        {
            int msds_id = 0;

            if (canEdit) {
                if (GridView1.SelectedIndex != -1 && GridView1.SelectedDataKey != null)
                    msds_id = Int32.Parse(GridView1.SelectedDataKey["msds_id"].ToString());

                Response.Redirect("MSDS.aspx?msdsid=" + msds_id + "&edit=Y");
            }
            else {
                if (GridView1.SelectedIndex != -1 && GridView1.SelectedDataKey != null)
                    msds_id = Int32.Parse(GridView1.SelectedDataKey["msds_id"].ToString());

                ClientScript.RegisterStartupScript(Page.GetType(), "",
                        "window.open('MSDS.aspx?msdsid=" + msds_id + 
                        "&edit=N','Graph','height=600,width=800, scrollbars=yes');", true);
            }
        }

        [CoverageExclude]
        public string getSelectedUi()
        {
            if (GridView1.SelectedIndex != -1 && GridView1.SelectedDataKey != null)
                return GridView1.SelectedDataKey["ui"].ToString();
            return "";
        }
        [CoverageExclude]
        public string getSelectedUm()
        {
            if (GridView1.SelectedIndex != -1 && GridView1.SelectedDataKey != null)
                return GridView1.SelectedDataKey["um"].ToString();
            return "";
        }

        [CoverageExclude]
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;            
            GridView1.DataBind();

            resetSMCLselections();
        }
        [CoverageExclude]
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = GridView1.DataSource as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

                GridView1.DataSource = dataView;
                GridView1.DataBind();
            }
        }       

        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }
        [CoverageExclude]
        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridView1.SelectedIndex = e.NewSelectedIndex;
        }
        [CoverageExclude]
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Edit1")
            {
                GridView1.SelectedIndex = Convert.ToInt16(e.CommandArgument);
                loadMSDSPage(true);
            }


            //Update/Delete SMCL select

            //MSDS select
            if (e.CommandName == "ViewMSDS")
            {
                GridView1.SelectedIndex = Convert.ToInt16(e.CommandArgument);
                loadMSDSPage(false);
            }

        }
        public String INV_ID
        {
            get
            {
                return INVIDparam.Text;
            }
        }



        [CoverageExclude]
        protected void btn_filter_Click(object sender, EventArgs e)
        {
            resetSMCLselections();            
        }

        private void resetSMCLselections()
        {
            //Clear selected rows on SMCL and Inventory
            //Because SMCL selection is cleared, clear out the Inventory table
            //And disable the Add Inventory button
            GridView1.SelectedIndex = -1;
            GridView1.DataBind();
        }


        protected void btnFeedback_Click(object sender, ImageClickEventArgs e)
        {

            //save current page to the session for feedback
            Session.Add("feedbackPage", GetCurrentPageName());

            ClientScript.RegisterStartupScript(Page.GetType(), "",
                "window.open('FEEDBACK.aspx','Feedback','height=400,width=590, scrollbars=no');", true);
        }

        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        protected void btnHelp_Click(object sender, ImageClickEventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "",
                "window.open('Help.html','Help','height=750,width=790, scrollbars=yes');", true);
        } 


        public bool isAdmin()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isAdmin(username);
        }

        public bool isSupplyOfficer()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isSupplyOfficer(username);
        }


        public bool isSupplyUser()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isSupplyUser(username);
        }


        public bool isWorkCenterUser()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isWorkCenterUser(username);
        }

        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridViewRow grow = e.Row;
                DataRowView row = (DataRowView)grow.DataItem;
                DataRow data = row.Row;
                if (data["manually_entered"] != DBNull.Value)
                {
                   int manually_entered = Convert.ToInt32(data["manually_entered"]);
                   if (manually_entered == 1)
                   {
                       e.Row.BackColor = ColorTranslator.FromHtml("#FF9933");
                   }
                }
            }

        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                object value = DBNull.Value;
                if (GridView1.SelectedIndex != -1 && GridView1.SelectedDataKey != null)
                {
                   value = GridView1.SelectedDataKey["manually_entered"];                   
                }
                if (value != DBNull.Value)
                {
                    int manually_entered = Convert.ToInt32(value);
                    if (manually_entered == 1)
                    {
                        e.Row.BackColor = ColorTranslator.FromHtml("#FF9933");
                    }
                }
            }
        }


        [CoverageExclude]
        protected void btnSFRNewSMCL_Click(object sender, EventArgs e) {
            Response.Redirect("MSDS.aspx");
        }

        [CoverageExclude]
        protected void btnNewMSDS_Click(object sender, EventArgs e)
        {
            Response.Redirect("MSDS.aspx");
        }
    }
}
