﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAZMAT
{
   /* public partial class ShelfLifeReport : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadShelfLifeGridView();
            if (DropDownListWorkcenter.Items.Count == 0)
            {
                List<ListItem> workcenters = new DatabaseManager().getWorkcenterList(true);
                foreach (ListItem item in workcenters)
                {
                    DropDownListWorkcenter.Items.Add(item);
                }
            }

            if (!Page.IsPostBack)
            {
                new Reports().populateReportsDropdown(DropDownListReport, "Shelf Life"); 
            }
        }


        private void loadShelfLifeGridView()
        {
            string startDate = Request.Form[txtStartDate.ClientID.Replace("_", "$")];
            string toDate = Request.Form[txtToDate.ClientID.Replace("_", "$")];
            string niin = "";
            if (chkbox_Niin.Checked)
            {
                niin = txtNiin.Text;
            }
            String location = "";
            if (chkbox_location.Checked)
            {
                
                location = txtLocation.Text;
            }
            String workCenter = "";
            if (chkbox_workcenter.Checked)
            {
                workCenter = DropDownListWorkcenter.SelectedValue;
            }
            shelfLifeGridView.DataSource = new DatabaseManager().getInventoryForShelfLifeReport(startDate, niin, toDate, location, workCenter).DefaultView;
            shelfLifeGridView.DataBind();
        }


        protected void btnFeedback_Click(object sender, ImageClickEventArgs e)
        {

            //save current page to the session for feedback
            Session.Add("feedbackPage", GetCurrentPageName());

            ClientScript.RegisterStartupScript(Page.GetType(), "",
"window.open('FEEDBACK.aspx','Feedback','height=400,width=590, scrollbars=no');", true);


        }

        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        protected void btnHelp_Click(object sender, ImageClickEventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "",
           "window.open('Help.html','Help','height=750,width=790, scrollbars=yes');", true);
        }


        [CoverageExclude]
        protected void btnExcelReport_Click(object sender, EventArgs e)
        {

            ExcelReports report = new ExcelReports();
            string reportPath = Request.PhysicalApplicationPath + "resources/Reports.xls";
            string filename = report.exportReports(reportPath);


            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=ExcelReports.xls");
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(filename);
            Response.Flush();
            Response.End();

        }


        protected void btnChooseReport_Click(object sender, EventArgs e)
          {

              String selectedValue = DropDownListReport.SelectedValue;


              if (!selectedValue.Equals("Feedback"))
              {
                  string url = new Reports().getReportUrl(selectedValue);
                  Response.Redirect(url);
              }
              else if (selectedValue.Equals("Feedback"))
              {


                  //get feedback info
                  List<FeedbackInfo> infoList = new DatabaseManager().getFeedbackInfo();


                  //call class or method to create tab delimited report
                  byte[] file = new DatabaseManager().createFeedbackReport(infoList);

                  //allow user to download report
                  Response.Clear();
                  Response.AddHeader("Content-Disposition", "attachment; filename=" + "FeedbackReport.xls");
                  Response.AddHeader("Content-Length", file.Length.ToString());
                  Response.ContentType = "application/octet-stream";
                  Response.BinaryWrite(file);
                  Response.Flush();
                  Response.End();

              }


          }


        [CoverageExclude]
        protected void btnDownloadExcel_Click(object sender, EventArgs e)
        {


            string startDate = txtStartDate.Text;
            string toDate = txtToDate.Text;
            string niin = "";
            if (chkbox_Niin.Checked)
            {
                niin = txtNiin.Text;
            }
            String location = "";
            if (chkbox_location.Checked)
            {

                location = txtLocation.Text;
            }
            String workCenter = "";
            if (chkbox_workcenter.Checked)
            {
                workCenter = DropDownListWorkcenter.SelectedValue;
            }

            ExcelReports report = new ExcelReports();
            string reportPath = Request.PhysicalApplicationPath + "resources/ShelfLifeReport.xls";
            string filename = report.exportShelfLifeReport(reportPath, startDate, niin, toDate, location, workCenter);


            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=ShelfLifeReport.xls");
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(filename);
            Response.Flush();
            Response.End();

        }


        [CoverageExclude]
        protected void shelfLifeGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            shelfLifeGridView.PageIndex = e.NewPageIndex;
            shelfLifeGridView.SelectedIndex = -1;
            shelfLifeGridView.DataBind();
        }


        public void populateReportsDropdown(DropDownList dd, string selectedReport)
        {


            dd.Items.Add(new ListItem("Incompatible Stowage", "Incompatibilities in same Location"));

            dd.Items.Add(new ListItem("Atmosphere Control Log", "Atmosphere Control Log"));

            dd.Items.Add(new ListItem("Quantities Over Allowance", "Quanties over allowance"));

            dd.Items.Add(new ListItem("MSSL/HILT Discrepancy", "MSSL/HILT Discrepancy"));

            dd.Items.Add(new ListItem("Current Total Volume Onboard", "InventoryAuditVolume"));

            dd.Items.Add(new ListItem("Total Volume Offloaded", "OffloadVolume"));

            dd.Items.Add(new ListItem("Shelf Life", "Shelf Life"));


            if (isAdmin())
            {
                dd.Items.Add(new ListItem("Feedback", "Feedback"));
            }



            if (selectedReport.Equals("Incompatibilities in same Location"))
            {
                dd.SelectedIndex = 0;
            }
            else if (selectedReport.Equals("Atmosphere Control Log"))
            {
                dd.SelectedIndex = 1;
            }
            else if (selectedReport.Equals("Quanties over allowance"))
            {
                dd.SelectedIndex = 2;
            }
            else if (selectedReport.Equals("MSSL/HILT Discrepancy"))
            {
                dd.SelectedIndex = 3;
            }
            else if (selectedReport.Equals("InventoryAuditVolume"))
            {
                dd.SelectedIndex = 4;
            }
            else if (selectedReport.Equals("OffloadVolume"))
            {
                dd.SelectedIndex = 5;
            }
            else if (selectedReport.Equals("Shelf Life"))
            {
                dd.SelectedIndex = 6;
            }
            else if (selectedReport.Equals("Feedback"))
            {
                dd.SelectedIndex = 7;
            }

        }


        public string getReportUrl(string selectedReport)
        {
            string url = "";

            if (selectedReport.Equals("Incompatibilities in same Location"))
            {
                url = "Reports.aspx";
            }
            else if (selectedReport.Equals("Atmosphere Control Log"))
            {
                url = "UsageCategoryReport.aspx";
            }
            else if (selectedReport.Equals("Quanties over allowance"))
            {
                url = "AllowanceReport.aspx";

            }
            else if (selectedReport.Equals("MSSL/HILT Discrepancy"))
            {
                url = "MSSLReport.aspx";

            }
            else if (selectedReport.Equals("InventoryAuditVolume"))
            {
                url = "AuditVolumesReport.aspx";

            }
            else if (selectedReport.Equals("OffloadVolume"))
            {
                url = "OffloadVolumesReport.aspx";

            }
            else if (selectedReport.Equals("Shelf Life"))
            {
                url = "ShelfLifeReport.aspx";
            }

            return url;
        }


        [CoverageExclude]
        public bool isAdmin()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isAdmin(username);
        }



        protected void btnLoadReport_Click(object sender, EventArgs e)
        {
            loadShelfLifeGridView();
        }
    }*/
}