﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddSMCL.aspx.cs" Inherits="HAZMAT.AddSMCL" %>

<script runat="server">
    void AddSMCL_NIINValidate(object source, ServerValidateEventArgs args) {
        int? niin_catalog_id = new HAZMAT.DatabaseManager().getNiinCatalogIdForNIIN(args.Value);

        if (niin_catalog_id.HasValue) {
            args.IsValid = false;
        }
    }
    
    void AddSMCL_MSDSValidate(object source, ServerValidateEventArgs args) {
        int? msds_id = new HAZMAT.DatabaseManager().GetMsdsIdforSerialNumber(args.Value);

        if (!msds_id.HasValue) {
            args.IsValid = false;
        }
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(
        function () {
            $('.queryBox').css('width', '130px');
            $('.autoCompleteWrapper').css('width', '135px');
            $('.list').css('width', '135px');
            $('.listOption').css('width', '135px');
        });
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>
            Add New SMCL Item
        </h1>
    </div>

    <p class="help-block">
        Items marked with an asterisk * are used in the SMCL Feedback Report (SFR)
    </p>

    <br />

    <div class="row">
        <div class="col-md-5">
            <div class="form-horizontal">

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * FSC
                    </label>
                    <div class="col-sm-10">
		                <asp:textbox id="txt_fsc" maxlength="4" runat="server" CssClass="form-control"></asp:textbox>
		                <asp:requiredfieldvalidator CssClass="alert alert-danger" controltovalidate="txt_fsc" id="RequiredFieldValidator2" runat="server" errormessage="Enter FSC. "></asp:requiredfieldvalidator>
		                <asp:rangevalidator CssClass="alert alert-danger" controltovalidate="txt_fsc" minimumvalue="0" maximumvalue="9999" type="Integer" id="Rangevalidator1" runat="server" errormessage="Enter digits only."/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * NIIN
                    </label>
                    <div class="col-sm-10">
		                <asp:textbox id="txt_niin" maxlength="9" runat="server" CssClass="form-control"></asp:textbox>
		                <asp:requiredfieldvalidator CssClass="alert alert-danger" controltovalidate="txt_niin" id="RequiredFieldValidator1" runat="server" errormessage="Enter NIIN. "></asp:requiredfieldvalidator>
		                <asp:customvalidator CssClass="alert alert-danger" controltovalidate="txt_niin" id="val_txt_niin" runat="server" errormessage="The NIIN already exists." onservervalidate="AddSMCL_NIINValidate"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        MSDS_Number
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="msdsNumTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                        <asp:textbox id="TextBox1" runat="server" visible="false"></asp:textbox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * UI
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_ui" maxlength="50" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        UM
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_um" maxlength="50" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Usage Category
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_usagecategory" runat="server" CssClass="form-control">
		                </asp:dropdownlist>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        *Nomenclature (Description)
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_desc" maxlength="1024" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        SMCC
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_smcc" runat="server" CssClass="form-control">
		                </asp:dropdownlist>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        SPECS
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_specs" maxlength="50" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        HCC
                    </label>
                    <div class="col-sm-10">
                        <asp:dropdownlist id="dd_hcc" runat="server" CssClass="form-control"/>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Shelf Life Code
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_slc" runat="server" CssClass="form-control"></asp:dropdownlist>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Shelf Life Action Code
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_slac" runat="server" CssClass="form-control">
		                </asp:dropdownlist>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Storage Type
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_storagetype" runat="server" CssClass="form-control">
		                </asp:dropdownlist>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        COG
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_cog" runat="server" CssClass="form-control">
		                </asp:dropdownlist>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        SPMIG
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_spmig" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        NEHC_RPT
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_nehc" maxlength="10" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Inventory Group
                    </label>
                    <div class="col-sm-10">
		                <asp:dropdownlist id="dd_group" runat="server" CssClass="form-control">
		                </asp:dropdownlist>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Serial Number
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_serial" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Remarks
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="txt_remarks" runat="server" height="100px" CssClass="form-control" textmode="MultiLine"></asp:textbox>
                    </div>
                </div>
            </div>

            <h3><asp:label id="Label1" runat="server" skinid="LabelTableHeader" text=" Manufacturer Information" font-bold="True"></asp:label></h3>
            <hr />

            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * Manufacturer
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="manufacturerTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * Cage
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="cageTxt" maxlength="5" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * Part Number
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="partNoTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * POC Name
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocNameTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        * POC Telephone Number
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocTelephoneTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        POC Address
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocAddressTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        POC City
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocCityTxt" maxlength="255" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        POC State
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocStateTxt" maxlength="20" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        POC Zip
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocZipTxt" maxlength="10" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        POC Email
                    </label>
                    <div class="col-sm-10">
                        <asp:textbox id="pocEmailTxt" maxlength="90" runat="server" CssClass="form-control"></asp:textbox>
                    </div>
                </div>
            </div>

            <h3><asp:label id="Label3" runat="server" skinid="LabelTableHeader" text=" Technical Data" font-bold="True"></asp:label></h3>
            <hr />

            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * System/Equipment/Material Use (including typical/average/maximum ambient and surface temperatures where material will be used)
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="systemEquipmentMaterialTxt" maxlength="255" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator CssClass="alert alert-danger"  id="SystemEquipmentRegexValidator" controltovalidate="systemEquipmentMaterialTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Method of Application
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="methodOfApplicationTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator id="RegularExpressionValidator1" CssClass="alert alert-danger"  controltovalidate="methodOfApplicationTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Proposed Usage
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="proposedUsageTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator id="RegularExpressionValidator2" CssClass="alert alert-danger" controltovalidate="proposedUsageTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Negative Impact of Not Having Material Available
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="negativeImpactTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator CssClass="alert alert-danger" id="RegularExpressionValidator3" controltovalidate="negativeImpactTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Special Training Requirements
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="specialTrainingTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator CssClass="alert alert-danger"  id="RegularExpressionValidator4" controltovalidate="specialTrainingTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Precautions (including local/general ventilation, personal protection equipment, including respiratory protection to be used)
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="precautionsTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator id="RegularExpressionValidator5" CssClass="alert alert-danger"  controltovalidate="precautionsTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Properties (i.e. corrosivity; reactivity; toxicity, etc.)
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="propertiesTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator CssClass="alert alert-danger"  id="RegularExpressionValidator6" controltovalidate="propertiesTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Advantages of Using This Material Over Materials Used in the Past
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="advantagesTxt" maxlength="1024" CssClass="form-control" textmode="MultiLine" height="100px" runat="server"></asp:textbox>
		                <asp:regularexpressionvalidator CssClass="alert alert-danger" id="RegularExpressionValidator7" controltovalidate="advantagesTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>
	
                <div class="form-group">
                    <label class="col-sm-6 control-label">
                        * Comments
                    </label>
                    <div class="col-sm-6">
		                <asp:textbox id="CommentsTxt" runat="server" height="100px" CssClass="form-control" textmode="MultiLine"></asp:textbox>
		                <asp:regularexpressionvalidator CssClass="alert alert-danger" id="RegularExpressionValidator8" controltovalidate="CommentsTxt" display="Dynamic" validationexpression="^([\S\s]{0,1024})$" errormessage="Must be 1024 characters or less" runat="server"></asp:regularexpressionvalidator>
                    </div>
                </div>
	
	
            </div>

            <asp:button id="btn_save" runat="server" onclick="btn_save_Click" text="Save" CssClass="btn btn-info"/>
            <asp:button validationgroup="NONE" id="btn_save0" runat="server" text="Return to SMCL" postbackurl="~/SMCL.aspx" CssClass="btn btn-default" />
            <div class="clearfix"></div>
            <br/>
            <asp:label id="lbl_message" runat="server" forecolor="Green" visible="False"></asp:label>

        </div>
    </div>

</asp:Content>
