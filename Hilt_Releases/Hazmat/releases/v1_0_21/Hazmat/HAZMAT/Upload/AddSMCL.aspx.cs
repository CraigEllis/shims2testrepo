﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;

namespace HAZMAT
{
    public partial class AddSMCL : Page
    {
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (!isAdmin() && !isSupplyOfficer() && !isSupplyUser())
                {
                    Page.Form.Visible = false;
                }
                else
                {
                    Page.Form.Visible = true;
                }

            }

            //populate drop down list
            if (dd_storagetype.Items.Count == 0)
            {
                List<ListItem> list = new DatabaseManager().getStorageTypeList(true);
                dd_storagetype.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in list)
                {
                    dd_storagetype.Items.Add(item);
                }
            }
            //populate drop down list
            if (dd_smcc.Items.Count == 0)
            {
                List<ListItem> list = new DatabaseManager().getSmccList(true);
                dd_smcc.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in list)
                {
                    dd_smcc.Items.Add(item);
                }
            }
            //populate drop down list
            if (dd_slac.Items.Count == 0)
            {
                List<ListItem> list = new DatabaseManager().getSlacList(true);
                dd_slac.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in list)
                {
                    dd_slac.Items.Add(item);
                }
            }
            //populate drop down list
            if (dd_slc.Items.Count == 0)
            {
                List<ListItem> list = new DatabaseManager().getSlcList(true);
                dd_slc.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in list)
                {
                    dd_slc.Items.Add(item);
                }
            }

            //populate cog drop down list
            if (dd_cog.Items.Count == 0)
            {
                List<ListItem> list = new DatabaseManager().getCogList(false);
                dd_cog.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in list)
                {
                    dd_cog.Items.Add(item);
                }
            }

            //populate usage category drop down list
            if (dd_usagecategory.Items.Count == 0)
            {
                List<ListItem> usageCategories = new DatabaseManager().getUsageCategoryListU(true);
                dd_usagecategory.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in usageCategories)
                {
                    dd_usagecategory.Items.Add(item);

                    //Default to U (Not yet evaluated)
                    if (item.Text.StartsWith("U"))
                        dd_usagecategory.SelectedValue = item.Value;
                }

                //dd_usagecategory.Enabled = false;               
            }

            //populate inventory/catalog group drop down list
            if (dd_group.Items.Count == 0)
            {
                List<ListItem> usageCategories = new DatabaseManager().getCatalogGroupList();
                dd_group.Items.Add(new ListItem("None", "0"));
                foreach (ListItem item in usageCategories)
                {
                    dd_group.Items.Add(item);
                }
            }

            //populate HCC drop down list
            if (dd_hcc.Items.Count == 0) {
                List<ListItem> usageCategories = new DatabaseManager().getHccList(true);
                foreach (ListItem item in usageCategories) {
                    dd_hcc.Items.Add(item);
                }
            }
        }

        public bool isAdmin()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isAdmin(username);
        }

        public bool isSupplyOfficer()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isSupplyOfficer(username);
        }


        public bool isSupplyUser()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isSupplyUser(username);
        }


        public bool isWorkCenterUser()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isWorkCenterUser(username);
        }
    }
}