﻿using System;
using Thor.Units;

namespace HAZMAT
{
    public class UnitConverter
    {

        //access this to use uc's methods directly
        public IUnitConverter uc { get; set; }

        public UnitConverter(string xmlPath)
        {
            //initialize the unit converter
            uc = Thor.Units.InterfaceFactory.CreateUnitConverter();

            //grab the xml file with the defined units of measure
            uc.LoadUnitsFile(xmlPath);

        }

        public void setXml(string xmlPath)
        {
            uc.LoadUnitsFile(xmlPath);
        }

        private string[] split(string input)
        {
            string[] dash = input.Split('-');

            if (dash.Length > 1)
                return dash;

            string[] times = input.Split('X');

            if (times.Length > 1)
                return times;

            string[] t2 = input.Split('x');

            if (t2.Length > 1)
                return t2;

            return null;
        }

        //Example: convertToUnit("16 OZ", 3.25, "GL") - 3.25 bottles that are 16 OZ. How many GL is that?
        public String convertToUnit(string txtInput, double multiplier, string txtUnitTo)
        {
            try
            {
                //Split txtInput ex 12-24OZ, 6X6.3LBS
                string[] inputs = split(txtInput);

                foreach (string s in inputs)
                {
                    Console.WriteLine("split:" + s);
                }
                if (inputs != null && inputs.Length == 2)
                {
                    //first split is the multiplier
                    double mult = Double.Parse(inputs[0]);
                    multiplier = multiplier * mult;

                    //second split is the txtInput
                    txtInput = inputs[1];
                }
            }
            catch { }

            double val, outval;
            string in_unit;
            UnitResult res = UnitResult.NoError;

            string txtOutput = "";

            res = uc.ParseUnitString(txtInput, out val, out in_unit);

            if (res == UnitResult.BadUnit)
            {
                txtOutput = "Bad input unit.";
                throw new Exception(txtOutput);
            }
            else if (res == UnitResult.BadValue)
            {
                txtOutput = "Bad input value.";
                throw new Exception(txtOutput);
            }

            IUnitEntry out_unit = uc.GetUnitBySymbol(txtUnitTo);

            if (out_unit == null)
            {
                txtOutput = "Bad output unit.";
                throw new Exception(txtOutput);
            }

            if (!uc.CompatibleUnits(in_unit, txtUnitTo))
            {
                txtOutput = "Units are of different types.";
                throw new Exception(txtOutput);
            }

            res = uc.ConvertUnits(val, in_unit, txtUnitTo, out outval);

            //multiplier - ex I have 3.50 containers that are 16 oz. 3.50 is the multiplier.
            outval = outval * multiplier;

            txtOutput = outval.ToString() + " " + out_unit.DefaultSymbol;

            return txtOutput;
        }

        //Example: convertToUnit("16 OZ", 3.25) - 3.25 bottles that are 16 OZ. How many total OZ is that?
        public String convertToTotal(string txtInput, double multiplier)
        {
            try
            {
                //Split txtInput ex 12-24OZ, 6X6.3LBS
                string[] inputs = split(txtInput);

                foreach (string s in inputs)
                {
                    Console.WriteLine("split:" + s);
                }
                if (inputs != null && inputs.Length == 2)
                {
                    //first split is the multiplier
                    double mult = Double.Parse(inputs[0]);
                    multiplier = multiplier * mult;

                    //second split is the txtInput
                    txtInput = inputs[1];
                }
            }
            catch { }

            double val, outval;
            string in_unit;
            UnitResult res = UnitResult.NoError;

            string txtOutput = "";

            res = uc.ParseUnitString(txtInput, out val, out in_unit);

            if (res == UnitResult.BadUnit)
            {
                txtOutput = "Bad input unit.";
                throw new Exception(txtOutput);
            }
            else if (res == UnitResult.BadValue)
            {
                txtOutput = "Bad input value.";
                throw new Exception(txtOutput);
            }

            res = uc.ConvertUnits(val, in_unit, in_unit, out outval);

            //multiplier - ex I have 3.50 containers that are 16 oz. 3.50 is the multiplier.
            outval = outval * multiplier;

            txtOutput = outval.ToString() + " " + in_unit;

            return txtOutput;
        }
    }
}