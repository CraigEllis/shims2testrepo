﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MSDSHCCReport.aspx.cs" Inherits="HAZMAT.MSDSHCCReport" %>
<%@ Register Src="~/AutoCompleteSearch.ascx"  TagName="AutoCompleteSearch"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
        .style1
        {
            width: 43px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br /><br />
<table style="width: 430px;"><tr><td align="left" class="style1">
    <asp:Image ID="Image1" runat="server" ImageUrl="images/hazard_icon.png"  /></td><td>
    <asp:Label  ID="Label1" runat="server" Text="Reports"
     SkinID="LabelPageHeader"></asp:Label></td>
     <td align="right">
                <asp:ImageButton ID="btnFeedback" runat="server" ImageUrl="images/feedback.gif" 
                    ToolTip="Click to provide feedback" onclick="btnFeedback_Click" />
                    <asp:ImageButton ID="btnHelp" runat="server" ImageUrl="images/help.gif" 
                    ToolTip="Click to access manuals and training material" onclick="btnHelp_Click" />
            </td>
     </tr></table>

     <asp:Button runat="server" ID="btnExcelReport" OnClick="btnExcelReport_Click" Text="Download Dashboard Excel Reports" /> 
        
        <br /><br />

    Choose Report:<asp:DropDownList ID="DropDownListReport" runat="server">
    </asp:DropDownList>
    <asp:Button runat="server" ID="btnChooseReport" OnClick="btnChooseReport_Click" Text="Submit" />
   <br /> <br />

   Filter: <asp:CheckBox ID="popupCkBox" runat="server" />
&nbsp;<asp:DropDownList ID="popupDropDown" runat="server">
            <asp:ListItem  Value="MSDSSERNO">MSDS Serial Number</asp:ListItem>
            <asp:ListItem Value="HCC">HCC</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="popupTxtSearch" runat="server" MaxLength="12" ></asp:TextBox>
         <asp:Button ID="btn_popupApplyFilter" runat="server" Text="Apply Filter" />

         <br /><br />

    <br />
    
    <br />
   
    <table width="95%">
        <tr>
            <td style="width: 63%" valign="top">
            <div class="tableHeaderDivReport" style="width:95%;">
    <asp:Label CssClass="floatLeft" ID="Label2" runat="server" SkinID="LabelTableHeaderReport" Text="MSDS's with changed HCC's" 
               Font-Bold="True"></asp:Label>
               <asp:Button CssClass="floatRight" ID="btnExportChangedHCCs" runat="server" 
                    Text="Download Changed HCC's Excel Report" onclick="btnExportChangedHCCs_Click" />

               <br />
               <br />
                <asp:GridView CssClass="gridWidth" DataKeyNames="m1_MSDSSERNO, m1_HMIRS, m1_HCC, m1_HCC_Description, m2_HMIRS, m2_HCC, m2_HCC_Description, m2_Load_Date"
                    SkinID="GridViewReport" ID="gridView1" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" OnPageIndexChanging="WrongLocationGridView_PageIndexChanging"
                    ShowFooter="True"
                    EmptyDataText=" No data to display">
                    <Columns>
                     <asp:BoundField Visible="true" DataField="m1_MSDSSERNO" HeaderText="MSDS Serial Number" SortExpression="m1_MSDSSERNO" />
                        <asp:BoundField Visible="true" DataField="m1_HMIRS" HeaderText="HMIRS" SortExpression="m1_HMIRS" />
                        <asp:BoundField Visible="true" DataField="m1_HCC" HeaderText="HCC" SortExpression="m1_HCC" />
                        <asp:BoundField Visible="true" DataField="m1_HCC_Description" HeaderText="Description" SortExpression="m1_HCC_Description" />
                        <asp:BoundField Visible="true" DataField="m2_HMIRS" HeaderText="HMIRS" SortExpression="m2_HMIRS" />
                        <asp:BoundField Visible="true" DataField="m2_HCC" HeaderText="HCC" SortExpression="m2_HCC" />
                        <asp:BoundField Visible="true" DataField="m2_HCC_Description" HeaderText="Description" SortExpression="m2_HCC_Description" />
                        <asp:BoundField Visible="true" DataField="m2_Load_Date" HeaderText="Changed Date" SortExpression="m2_Load_Date" />
                        
                       
                    </Columns>
                    
                </asp:GridView>  
                </div>              
            </td>
            
        </tr>
    </table>
    <br />
    <br />


</asp:Content>

