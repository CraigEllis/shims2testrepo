﻿using System; 
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
//using Common.Logging;
using System.IO;
using System.Data.SQLite;
using System.Collections;
using System.Diagnostics;
using System.Security.Cryptography;
using Microsoft.SqlServer.Server;
using HAZMAT.Api.Models;
using System.Data.OleDb;

namespace HAZMAT
{
    public class DatabaseManager
    {
//        private static ILog logger = LogManager.GetCurrentClassLogger();

        private static string MDF_CONNECTION = null;

        public enum FILE_TABLES {
            FileUpload,
            FileDownLoad
        };

        public enum FILE_TYPES {
            MSDS,
            MSSL,
            SMCL,
            INC
        };

        public DatabaseManager()
        {
            if (MDF_CONNECTION == null)
            {
                MDF_CONNECTION = Configuration.ConnectionInfo;
            }

            //if (DB_BACKUP_DIRECTORY == null) {
            //    DB_BACKUP_DIRECTORY = Configuration.HILT_DataFolder + "\\Backups";

            //    // Make sure the folder exists
            //    if (!System.IO.Directory.Exists(DB_BACKUP_DIRECTORY)) {
            //        System.IO.Directory.CreateDirectory(DB_BACKUP_DIRECTORY);
            //    }
            //}
        }

        public DatabaseManager(string connectionString)
        {
            if (MDF_CONNECTION == null)
            {
                  MDF_CONNECTION = connectionString;
            }
        }

        public List<string> getLocationNameList()
        {

            List<string> locationList = new List<string>();

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from locations  WHERE name<>'NEWLY ISSUED'", con);
            SqlDataReader rdr = cmd.ExecuteReader();


            while (rdr.Read())
            {

                locationList.Add(rdr["name"].ToString());

            }

            con.Close();

            return locationList;
        }


        public bool isAdmin(string username)
        {
            return isUserInRole(username, "ADMIN");
        }

        public bool isSupplyOfficer(string username)
        {
            return isUserInRole(username, "SUPPLY OFFICER");
        }

        public bool isSupplyUser(string username)
        {
            return isUserInRole(username, "SUPPLY USER");
        }

        public bool isWorkCenterUser(string username)
        {
            return isUserInRole(username, "WORKCENTER USER");
        }

        public bool isPlainUser(string username)
        {
            return isUserInRole(username, "USER");
        }

        public bool isUserInRole(string username, string role)
        {
            bool isUserInRole = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "select * FROM user_roles WHERE username=@username AND role=@role";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@username", dbNull(username));
            cmd.Parameters.AddWithValue("@role", dbNull(role));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();
            conn.Close();

            if (dt.Rows.Count > 0)
                isUserInRole = true;

            return isUserInRole;

        }

        public String getCurrentShipName()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            String shipName = null;

            mdfConn.Open();

            string selectStmt = "select s.name from ships s where s.current_ship = 1";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            
            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                shipName = rdr["name"].ToString();
            }

            cmd.Dispose();
            mdfConn.Close();

            return shipName;

        }

        public DataTable getShip()
        { 

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select * from Ships";

            SqlCommand selectCmd = new SqlCommand(selectStmt, mdfConn);

            SqlDataReader rdr = selectCmd.ExecuteReader();
            string shipName = "";
            while (rdr.Read())
            {
                shipName = rdr["name"].ToString();                
            }
            rdr.Close();
            string stmt = "select * from ships where name = @name";

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

            cmd.Parameters.AddWithValue("@name", dbNull(shipName));
             
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);            

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }

        public DataTable getCurrentShip()
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select * from ships where current_ship = 1";
               
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            
            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getAllWorkcenters()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select distinct w.workcenter_id,w.wid " +
                          "from workcenter w, offload_list o, locations l " +
                          "where w.workcenter_id = l.workcenter_id " +
                          "AND o.location_id = l.location_id " +
                          "AND o.archived_id is null";
      
            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getIncompatibilitiesInventoryList(List<string> hccList, List<int>hazardList, int location_id)
        {
            int hazard_id = hazardList[0];
            int hazard_id2 = hazardList[0];

            if (hazardList.Count == 2)
                hazard_id2 = hazardList[1];

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string stmt = "select v.*, h.warning_level, i.*, m.* from vHazardousItems v " +
                          " INNER JOIN hazard_warnings h ON ((h.hazard_id_1=v.hazard_id) AND (h.hazard_id_2 = @hazard_id OR h.hazard_id_2 = @hazard_id2))" +
                          " INNER JOIN inventory i ON (v.inventory_id=i.inventory_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          +" WHERE v.location_id = @location_id " +
                          " " +
                          " UNION ALL select v.*, 'U', i.*, m.* from v_Top_Hazard_Items v"+
                          " INNER JOIN inventory i ON (v.inventory_id=i.inventory_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE v.location_id = @location_id AND v.hazard_id=0";

            if (hazard_id == 0)
            {
                //If 0 show all items in that location with their hazards.
                stmt = "select v.*, 'U' as warning_level, i.*, m.* from v_Top_Hazard_Items v" +
                          " INNER JOIN inventory i ON (v.inventory_id=i.inventory_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE v.location_id = @location_id";
            }

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);

           
           
            cmd.Parameters.AddWithValue("@hazard_id", dbNull(hazard_id));
            cmd.Parameters.AddWithValue("@hazard_id2", dbNull(hazard_id2));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));

            DataTable dt = new DataTable();           

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            List<string> usedInvIds = new List<string>();
            List<DataRow> rowsToRemove = new List<DataRow>();
            //remove duplicates
            foreach (DataRow r in dt.Rows)
            {
                string inventory_id = r["inventory_id"].ToString();
                if (usedInvIds.Contains(inventory_id))
                    rowsToRemove.Add(r);
                else
                    usedInvIds.Add(inventory_id);
            }

            
            //FILTER OUT SPECIAL CASE
            foreach (DataRow row in dt.Rows)
            {
                string hcc1 = hccList[0];
                string hcc2 = Convert.ToString(row["hcc"]);              

                bool ignore = false;

                if (hcc1.Equals("V2") && hcc2.Equals("V3"))
                    ignore = true;
                else if (hcc1.Equals(hcc2) && hcc1 != null && hcc2 != null && !hcc1.Equals("") && !hcc2.Equals("")) //If HCC=HCC they are compatible
                    ignore = true;   
                else if (hcc1.Equals("V3") && hcc2.Equals("V2"))
                    ignore = true;

                if (ignore)
                {
                    if (!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }


            }//end for each
            //Remove rows
            foreach (DataRow row in rowsToRemove)
            {
                dt.Rows.Remove(row);
            }//end

            return dt;

        }



        public DataTable getIncompatibilitiesInventoryListInvAuditCr(List<string> hccList, List<int> hazardList, int location_id, int audit_id, int count, int? inventory_audit_cr_id)
        {
            int hazard_id = hazardList[0];
            int hazard_id2 = hazardList[0];

            if (hazardList.Count == 2)
                hazard_id2 = hazardList[1];

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string ignoreIfParentId = " i.inventory_audit_cr_id NOT IN (select x.parent_id from inventory_audit_cr x WHERE x.parent_id IS NOT NULL) ";
           
            //This method is grabbing hazardous items from the correct count, location, AND audit.
            string stmt = "select v.*, h.warning_level, i.*, m.* from vHazardousItemsInvAuditCr v " +
                          " INNER JOIN hazard_warnings h ON ((h.hazard_id_1=v.hazard_id) AND (h.hazard_id_2 = @hazard_id OR h.hazard_id_2 = @hazard_id2))" +
                          " INNER JOIN inventory_audit_cr i ON (v.inventory_audit_cr_id=i.inventory_audit_cr_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE " + ignoreIfParentId + " AND v.location_id = @location_id " +
                          " AND v.inventory_audit_id = @audit_id "
                + "and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                + "where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)" +
                          " " +
                          " UNION ALL select v.*, 'U', i.*, m.* from v_Top_Hazard_ItemsInvAuditCr v" +
                          " INNER JOIN inventory_audit_cr i ON (v.inventory_audit_cr_id=i.inventory_audit_cr_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE " + ignoreIfParentId + " AND v.location_id = @location_id AND v.hazard_id=0"
                          + " AND v.inventory_audit_id = @audit_id "
                + "and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                + "where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)";

            if (hazard_id == 0)
            {
                //If 0 show all items in that location with their hazards.
                stmt = "select v.*, 'U' as warning_level, i.*, m.* from v_Top_Hazard_ItemsInvAuditCr v" +
                          " INNER JOIN inventory_audit_cr i ON (v.inventory_audit_cr_id=i.inventory_audit_cr_id) INNER JOIN vMfgCatNiinCat m ON (i.mfg_catalog_id=m.mfg_catalog_id)"
                          + " WHERE " + ignoreIfParentId + " AND v.location_id = @location_id"
                          + " AND v.inventory_audit_id = @audit_id "
                + "and v.count = @count and v.action_complete > (select max(action_complete) from location_cr "
                + "where location_done = 0 and location_id = @location_id and inventory_audit_id = @audit_id and count = @count)";
            }

            SqlCommand cmd = new SqlCommand(stmt, mdfConn);



            cmd.Parameters.AddWithValue("@hazard_id", dbNull(hazard_id));
            cmd.Parameters.AddWithValue("@hazard_id2", dbNull(hazard_id2));
            cmd.Parameters.AddWithValue("@location_id", dbNull(location_id));
            cmd.Parameters.AddWithValue("@audit_id", audit_id);
            cmd.Parameters.AddWithValue("@count", count);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            List<string> usedInvIds = new List<string>();
            List<DataRow> rowsToRemove = new List<DataRow>();

            //This stmt must be included in the case of an InvAuditCr update so that an item does not show up as incompatible with itself.
            if(inventory_audit_cr_id!=null)
               usedInvIds.Add(inventory_audit_cr_id.ToString());

            //remove duplicates
            foreach (DataRow r in dt.Rows)
            {
                string cr_id = r["inventory_audit_cr_id"].ToString();
                if (usedInvIds.Contains(cr_id))
                    rowsToRemove.Add(r);
                else
                    usedInvIds.Add(cr_id);
            }

            //FILTER OUT SPECIAL CASE
            foreach (DataRow row in dt.Rows)
            {
                string hcc1 = hccList[0];
                string hcc2 = Convert.ToString(row["hcc"]);

                bool ignore = false;

                if (hcc1.Equals("V2") && hcc2.Equals("V3"))
                    ignore = true;
                else if (hcc1.Equals(hcc2) && hcc1!=null && hcc2!=null && !hcc1.Equals("") && !hcc2.Equals("")) //If HCC=HCC they are compatible
                    ignore = true;               
                else if (hcc1.Equals("V3") && hcc2.Equals("V2"))
                    ignore = true;

                if (ignore)
                {
                    if(!rowsToRemove.Contains(row))
                        rowsToRemove.Add(row);
                }


            }//end for each
            //Remove rows
            foreach (DataRow row in rowsToRemove)
            {
                dt.Rows.Remove(row);
            }//end

            return dt;

        }

        public DataTable getAllLocations()
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select distinct l.location_id,l.name,l.workcenter_id from locations l, offload_list o where l.location_id = o.location_id AND o.archived_id is null";
      
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getDistinctLocationListAll() //includes NEWLY ISSUED locations
        {
            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select DISTINCT l.name as Location " +
                                 "FROM locations l ";
            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public DataTable getLocationList()
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select l.location_id, w.workcenter_id,  w.wid as Workcenter, l.name as Location " +
                                 "FROM locations l, workcenter w " +
                                "where l.workcenter_id = w.workcenter_id AND l.name<>'NEWLY ISSUED'";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            DataTable dt = new DataTable();


            new SqlDataAdapter(cmd).Fill(dt);

            cmd.Dispose();

            mdfConn.Close();

            return dt;

        }


        public void updateShip(string name, string uic, string hull_type, string hull_number, string address, string pocName, string pocTelephone, string pocEmail)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "UPDATE ships set name=@name, uic=@uic, hull_type=@hull_type,"
                + " hull_number=@hull_number, address=@address, poc_Name=@pocName, poc_Telephone=@pocTelephone, poc_Email=@pocEmail ";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@name", dbNull(name));
            cmd.Parameters.AddWithValue("@uic", dbNull(uic));
            cmd.Parameters.AddWithValue("@hull_type", dbNull(hull_type));
            cmd.Parameters.AddWithValue("@hull_number", dbNull(hull_number));
            cmd.Parameters.AddWithValue("@address", dbNull(address));
            cmd.Parameters.AddWithValue("@pocName", dbNull(pocName));
            cmd.Parameters.AddWithValue("@pocTelephone", dbNull(pocTelephone));
            cmd.Parameters.AddWithValue("@pocEmail", dbNull(pocEmail));

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public int insertLocation(string name, int workcenter_id)
        {
            
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string insertStmt = "insert into locations (name,workcenter_id) VALUES (@name,@workcenter_id);  SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@name", dbNull(name));
            cmd.Parameters.AddWithValue("@workcenter_id", dbNull(workcenter_id));

            object o = cmd.ExecuteScalar();
            int location_id = Convert.ToInt32(o);

            conn.Close();

            return location_id;

        }

        public void insertWorkcenter(string wid, string description)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string insertStmt = "insert into workcenter (wid) VALUES (@wid); SELECT SCOPE_IDENTITY()";

            SqlCommand cmd = new SqlCommand(insertStmt, conn);

            cmd.Parameters.AddWithValue("@wid", dbNull(wid));

            object o = cmd.ExecuteScalar();
            int workcenter_id = Convert.ToInt32(o);

            conn.Close();

        }

        public List<string> getWorkCenterList()
        {

            List<string> workList = new List<string>();

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            string selectStmt = "select wid from workcenter";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                workList.Add(rdr["wid"].ToString());
            }


            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();


            return workList;

        }

        public int getTotalInventoryQty(int inventory_onhand_id)
        {
            int count = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select inventory_detail_id from inventory_onhand where inventory_onhand_id = @onhand_id";

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@onhand_id", inventory_onhand_id);

            string s = cmdRead.ExecuteScalar().ToString();

            string getQuantity = "select sum(qty) from inventory_onhand where inventory_detail_id = " + s;
            cmdRead = new SqlCommand(getQuantity, conn);
            s = cmdRead.ExecuteScalar().ToString();
            if (!s.Equals(""))
                count = Int32.Parse(s);

            conn.Close();

            return count;
        }

        public int getTotalInventoryQty(string niin)
        {
            if (niin == null)
            {
                return 0;
            }
            int count = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "SELECT SUM(qty) from inventory_onhand oh inner join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id where niin = @niin";

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@niin", niin);

            string s = cmdRead.ExecuteScalar().ToString();

            if(!s.Equals(""))
            count = Int32.Parse(s);
            
            conn.Close();

            return count;
        }

        //For HashMaps
        public Dictionary<string, int> getMap(String selectStmt)
        {
            Dictionary<string, int> map = new Dictionary<string, int>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                if (!map.ContainsKey("" + reader.GetValue(0)))
                    map.Add("" + reader.GetValue(0), Int32.Parse(""+reader.GetValue(1)));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return map;

        }

        public Dictionary<string, int> getHccMap()
        {
            string selectStmt = "Select hcc, hcc_id from hcc";
            return getMap(selectStmt);
        }

        public Dictionary<string, int> getUsageCategoryMap()
        {
            string selectStmt = "Select category, usage_category_id from usage_category";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getUiMap()
        {
            string selectStmt = "Select abbreviation, ui_id from ui";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSmccMap()
        {
            string selectStmt = "Select smcc, smcc_id from smcc";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlcMap()
        {
            string selectStmt = "Select slc, shelf_life_code_id from shelf_life_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlacMap()
        {
            string selectStmt = "Select slac, shelf_life_action_code_id from shelf_life_action_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getStorageTypeMap()
        {
            string selectStmt = "Select type, storage_type_id from storage_type";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getCogMap()
        {
            string selectStmt = "Select cog, cog_id from cog_codes";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getCatalogGroupMap()
        {
            string selectStmt = "Select group_name, catalog_group_id from catalog_groups";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSFRniinMap(int sfr_type)
        {
            string selectStmt = "Select s.niin, s.sfr_id from sfr s WHERE " 
            + " s.sfr_type='"+sfr_type+"' AND completed_flag = 0";

            return getMap(selectStmt);
        }

        public int insertShipTo(string uic, string organization, string plate_title, string city, string state, string zip)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string insertStmt = "insert into ship_to (" +

            "uic, organization, plate_title, city, state, zip"

            + ")"
            + "VALUES(" +
             "@uic, @organization, @plate_title, @city, @state, @zip"
            + ");  SELECT SCOPE_IDENTITY()";

            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //insert rows
            insertCmd.Parameters.AddWithValue("@uic", dbNull(uic));
            insertCmd.Parameters.AddWithValue("@organization", dbNull(organization));
            insertCmd.Parameters.AddWithValue("@plate_title", dbNull(plate_title));
            insertCmd.Parameters.AddWithValue("@city", dbNull(city));
            insertCmd.Parameters.AddWithValue("@state", dbNull(state));
            insertCmd.Parameters.AddWithValue("@zip", dbNull(zip));

            object o = insertCmd.ExecuteScalar();
            int ship_to_id = Convert.ToInt32(o);
            insertCmd.Parameters.Clear();

            tran.Commit();

            conn.Close();

            return ship_to_id;

        }

        //See if the UPDATE sfr request matches the SMCL entry
        public bool compareSMCLUpdatedSFR(SqlConnection conn, SqlTransaction tran, int sfr_id, Dictionary<string, string> smclValueMap)
        {
            bool match = true;

            const string selectstmt = "select * from sfr WHERE sfr_id=@sfr_id";

            SqlCommand cmd = new SqlCommand(selectstmt, conn);
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@sfr_id", sfr_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                foreach (String column_name in smclValueMap.Keys)
                {
                    string sfr_value = rdr[column_name].ToString();
                    string smcl_value = smclValueMap[column_name].ToString();

                    bool equals = false;

                    if (smcl_value.Equals(sfr_value))
                        equals = true;
                    if ((smcl_value == null || smcl_value.Equals("")) 
                            && (sfr_value == null || sfr_value.Equals("")))
                        equals = true;

                    if (!equals)
                        match = false;

                }//end for each column to compare

            }//end while read

            rdr.Close();
            cmd.Dispose();

            return match;
        }

        private bool confirmSMCLUploadFormat(string[] headers)
        {
           
            List<String> missingCols = new List<String>(); 
       
            string[] columns = { "ID", "NIIN", "FSC", "UI", "UM", "USAGE_CATEGORY", 
                                   "ITEM_NAME" , "MILSPEC", "SMCC", "TYPE_OF_STORAGE",
                                   "COG", "SLC", "SLAC", "REMARKS", 
                                   "SPMIG", "TABLE_NAME", "SERIAL_NUMBER", "CAGE", "MANUFACTURER"};

            foreach (String column in columns)
            {
                bool contains = false;
                foreach (String header in headers)
                {
                    if (header.ToUpper().Equals(column))
                        contains = true;
                }
                if (!contains)
                {
                    missingCols.Add(column);
                }
                }

            // See if we have any missing columns
            if (missingCols.Count > 0) {
                return false;
        }
            return true;
        }

        public bool isGreaterUsageCategoryThan(string uc1, string uc2)
        {
            int id_1 = 0;
            int id_2 = 0;

            if (uc1.Equals("X"))
                id_1 = 1;
            else if (uc1.Equals("X/X"))
                id_1 = 2;
            else if (uc1.Equals("X/L"))
                id_1 = 3;
            else if (uc1.Equals("R/X"))
                id_1 = 4;
            else if (uc1.Equals("L/X"))
                id_1 = 5;
            else if (uc1.Equals("R"))
                id_1 = 6;
            else if (uc1.Equals("R/L"))
                id_1 = 7;
            else if (uc1.Equals("L/R"))
                id_1 = 8;
            else if (uc1.Equals("L"))
                id_1 = 9;
            else if (uc1.Equals("L/L"))
                id_1 = 10;
            else if (uc1.Equals("N/L"))
                id_1 = 11;
            else if (uc1.Equals("N"))
                id_1 = 12;
            else 
                id_1 = 13;

            if (uc2.Equals("X"))
                id_2 = 1;
            else if (uc2.Equals("X/X"))
                id_2 = 2;
            else if (uc1.Equals("X/L"))
                id_2 = 3;
            else if (uc1.Equals("R/X"))
                id_2 = 4;
            else if (uc1.Equals("L/X"))
                id_2 = 5;
            else if (uc1.Equals("R"))
                id_2 = 6;
            else if (uc1.Equals("R/L"))
                id_2 = 7;
            else if (uc1.Equals("L/R"))
                id_2 = 8;
            else if (uc1.Equals("L"))
                id_2 = 9;
            else if (uc1.Equals("L/L"))
                id_2 = 10;
            else if (uc1.Equals("N/L"))
                id_2 = 11;
            else if (uc1.Equals("N"))
                id_2 = 12;
            else 
                id_2 = 13;

            return id_1 < id_2;
        }

        public string updateIncompatibles(string fileName)
        {
            Dictionary<string, int> hccs = new Dictionary<string, int>();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            var connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 12.0 Xml;HDR=YES;'";
            var connection = new OleDbConnection(connectionString);
            connection.Open();
            using (connection)
            {
                    // Get all rows from the Sheet
                    OleDbCommand cmd = new OleDbCommand(@"SELECT * FROM [Incompatibles$]", connection);

                    DataTable dt = new DataTable();
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(dt);
                    foreach (DataRow row in dt.Rows)
                    {
                        //get any new hcc's
                        var insert = @"if (NOT exists (select * from hcc where hcc = @hcc)) begin insert into hcc (hcc, description) values (@hcc, @description) end;";
                        SqlCommand sqlCmd = new SqlCommand(insert, conn);
                        sqlCmd.Parameters.AddWithValue("@hcc", row[0]);
                        sqlCmd.Parameters.AddWithValue("@description", row[1]);
                        sqlCmd.ExecuteNonQuery();
                    }
                    hccs = getHccMap();
                    //delete old relationships
                    SqlCommand sqlCommand = new SqlCommand("delete from hazard_warnings", conn);
                    sqlCommand.ExecuteNonQuery();
                    foreach (DataRow row in dt.Rows)
                    {
                        int hcc_id_1 = 0;
                        int hcc_id_2 = 0;
                        var insert = @"insert into hazard_warnings (hcc_id_1, hcc_id_2, warning_level) values (@hcc1, @hcc2, @warning_level);";
                        SqlCommand sql = new SqlCommand(insert, conn);
                        if (hccs.ContainsKey(row[0].ToString()))
                                        hcc_id_1 = hccs[row[0].ToString()];
                        if (hccs.ContainsKey(row[2].ToString()))
                            hcc_id_2 = hccs[row[2].ToString()];
                        sql.Parameters.AddWithValue("@hcc1", hcc_id_1);
                        sql.Parameters.AddWithValue("@hcc2", hcc_id_2);
                        sql.Parameters.AddWithValue(@"warning_level", row[3].ToString());
                        sql.ExecuteNonQuery();
                    }

                    // Add the record to the uploads table
                    updateFileTable(DatabaseManager.FILE_TABLES.FileUpload,
                        fileName, DatabaseManager.FILE_TYPES.INC.ToString(),
                        DateTime.Now, "", 0, 0, 0);
            }

            return "Success!";
        }

        public string updateSMCL(List<string[]> itemList, string fileName)
        {
            Dictionary<string, int> usageCategoryMap = getUsageCategoryMap();
            Dictionary<string, int> smccMap = getSmccMap();
            Dictionary<string, int> slcMap = getSlcMap();
            Dictionary<string, int> slacMap = getSlacMap();
            Dictionary<string, int> storageTypeMap = getStorageTypeMap();
            Dictionary<string, int> catalogGroupMap = getCatalogGroupMap();
            Dictionary<string, int> uiMap = getUiMap();

            string insertStmt = "if NOT exists (select * from niin_catalog where niin = @niin) insert into niin_catalog (" +
        "fsc, niin, ui, um, usage_category_id, description, smcc_id, specs," +
        "shelf_life_code_id, shelf_life_action_code_id, remarks," +
        "storage_type_id, cog, spmig, nehc_rpt," +
        "catalog_group_id, catalog_serial_number, allowance_qty, " +
        "created, manufacturer, cage, msds_num)" +
        "VALUES(" +
         "@fsc, @niin, @ui, @um, @usage_category_id, @description, @smcc_id, @specs," +
        "@shelf_life_code_id, @shelf_life_action_code_id, @remarks," +
        "@storage_type_id, @cog, @spmig, @nehc_rpt," +
        "@catalog_group_id, @catalog_serial_number, @allowance_qty, " +
        "@created, @manufacturer, @cage, @msds_num)";


            //CONFIRM header format before doing anything else
            // confirmSMCLUploadFormat throws an exception that is rolled up if the header fields aren't correct
            string[] headers = itemList[0];
            if (!confirmSMCLUploadFormat(headers))
            {
                return "Selected file does not contain the expected SMCL data.";
            }

            //// Create the database objects
            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION))
            {
                conn.Open();


                using (SqlTransaction tran = conn.BeginTransaction())
                {
                    //delete original SMCL
                    string deleteStmt = "delete from niin_catalog";
                    SqlCommand deleteCmd = new SqlCommand(deleteStmt, conn);
                    deleteCmd.Transaction = tran;
                    deleteCmd.ExecuteNonQuery();

                    SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                    insertCmd.Transaction = tran;

                    // Start the list processing
                    foreach (string[] row in itemList)
                    {
                        int fsc = 0;
                        string niin = "";
                        int ui_id = 0;
                        string um = "";
                        int usage_category_id = 0;
                        string description = "";
                        int smcc_id = 0;
                        string specs = "";
                        int shelf_life_code_id = 0;
                        int shelf_life_action_code_id = 0;
                        string remarks = "";
                        int storage_type_id = 0;
                        string cog = "";
                        string spmig = "";
                        string nehc_rpt = "";
                        int catalog_group_id = 0;
                        string catalog_serial_number = "";
                        int allowance_qty = 0;

                        string manufacturer = "";
                        string cage = "";
                        string msds_num = "";

                        // Added for niin numeric check
                        int niinNum = 0;

                        int smcl_master_id = 0;

                        string usage_category = "";

                        if (itemList[0] != row)//1st row is headers - also eliminates duplicate records 
                        {

                            // Populate the field values from the list

                            for (int i = 0; i < row.Length; i++)
                            {
                                if (headers[i].ToUpper().Equals("FSC"))
                                {
                                    try
                                    {
                                        fsc = Int32.Parse(row[i]);
                                    }
                                    catch
                                    {
                                    }
                                }
                                else if (headers[i].ToUpper().Equals("ID"))
                                {
                                    try
                                    {
                                        smcl_master_id = Int32.Parse(row[i]);
                                    }
                                    catch
                                    {
                                    }
                                }
                                else if (headers[i].ToUpper().Equals("NIIN"))
                                {
                                    niin = row[i];
                                    if (int.TryParse(niin, out niinNum))
                                    {
                                        // NIIN is numeric - make sure it is formatted to 9 chars
                                        niin = String.Format("{0:000000000}", niinNum);
                                    }
                                }
                                else if (headers[i].ToUpper().Equals("UI"))
                                {
                                    if (uiMap.ContainsKey(row[i]))
                                        ui_id = uiMap[row[i]];
                                }
                                else if (headers[i].ToUpper().Equals("CAGE"))
                                {
                                    cage = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("MANUFACTURER"))
                                {
                                    manufacturer = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("MSDS_NUM"))
                                {
                                    msds_num = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("UM"))
                                {
                                    um = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("USAGE_CATEGORY"))
                                {
                                    if (usageCategoryMap.ContainsKey(row[i]))
                                        usage_category_id = usageCategoryMap[row[i]];

                                    usage_category = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("ITEM_NAME"))
                                {
                                    if (row[i] == "Listerine")
                                    {
                                        niin = "111111111";
                                    }
                                    description = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("MILSPEC"))
                                {
                                    specs = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("SMCC"))
                                {
                                    if (smccMap.ContainsKey(row[i]))
                                        smcc_id = smccMap[row[i]];
                                }
                                else if (headers[i].ToUpper().Equals("TYPE_OF_STORAGE"))
                                {
                                    if (storageTypeMap.ContainsKey(row[i]))
                                        storage_type_id = storageTypeMap[row[i]];
                                }
                                else if (headers[i].ToUpper().Equals("COG"))
                                {
                                    cog = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("SLC"))
                                {
                                    if (slcMap.ContainsKey(row[i]))
                                        shelf_life_code_id = slcMap[row[i]];
                                }
                                else if (headers[i].ToUpper().Equals("SLAC"))
                                {
                                    if (slacMap.ContainsKey(row[i]))
                                        shelf_life_action_code_id = slacMap[row[i]];
                                }
                                else if (headers[i].ToUpper().Equals("REMARKS"))
                                {
                                    remarks = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("SPMIG"))
                                {
                                    spmig = row[i];
                                }
                                else if (headers[i].ToUpper().Equals("TABLE_NAME"))
                                {
                                    if (catalogGroupMap.ContainsKey(row[i]))
                                        catalog_group_id = catalogGroupMap[row[i]];
                                }
                                else if (headers[i].ToUpper().Equals("SERIAL_NUMBER"))
                                {
                                    catalog_serial_number = row[i];
                                }

                            }//end fields

                                    //insert rows
                                    insertCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                                    insertCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                                    insertCmd.Parameters.AddWithValue("@ui", dbNull(ui_id));
                                    insertCmd.Parameters.AddWithValue("@um", dbNull(um));
                                    insertCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                                    insertCmd.Parameters.AddWithValue("@description", dbNull(description));
                                    insertCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                                    insertCmd.Parameters.AddWithValue("@specs", dbNull(specs));

                                    insertCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                                    insertCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                                    insertCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));

                                    insertCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                                    insertCmd.Parameters.AddWithValue("@cog", dbNull(cog));
                                    insertCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                                    insertCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));

                                    insertCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                                    insertCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                                    insertCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                                    insertCmd.Parameters.AddWithValue("@created", dbNull(DateTime.Now));

                                    insertCmd.Parameters.AddWithValue("@manufacturer", dbNull(manufacturer));
                                    insertCmd.Parameters.AddWithValue("@cage", dbNull(cage));
                                    insertCmd.Parameters.AddWithValue("@msds_num", dbNull(msds_num));

                                    try
                                    {
                                        if (niin != null && !niin.Equals(""))
                                            insertCmd.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception("Error Inserting NIIN(" + niin
                                            + "), DESC(" + description + "), MFG(" + manufacturer + "). "
                                          + ex.Message);
                                    }
                                    insertCmd.Parameters.Clear();
                                    //end insert rows
                        }//end row
                    }

                    //update the sfrs as appropriate
                    string sfrSql = @"update sfr 
                                        set sfr.completed_flag = 1 where (sfr.niin in (select niin from niin_catalog) and sfr.sfr_type = 0);
                                      update sfr
                                        set sfr.completed_flag = 1 where (sfr.niin not in (select niin from niin_catalog) and sfr.sfr_type = 1);";


                    SqlCommand sfrCommand = new SqlCommand(sfrSql, conn);
                    sfrCommand.Transaction = tran;
                    sfrCommand.ExecuteNonQuery();


                    tran.Commit();
                }

                conn.Close();

                // Add the record to the uploads table
                updateFileTable(DatabaseManager.FILE_TABLES.FileUpload,
                    fileName, DatabaseManager.FILE_TYPES.SMCL.ToString(),
                    DateTime.Now, "", 0, 0, 0);
            }
            return "SMCL data successfully updated.";

        }

 
        public void insertMSDS(MsdsRecord msds, bool manually_entered)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
             conn.Open(); 
            SqlTransaction tran = conn.BeginTransaction();


            string stmtMSDS = "INSERT INTO msds (file_name, manually_entered, " + 
                "ARTICLE_IND, CAGE, MANUFACTURER, DESCRIPTION, EMERGENCY_TEL, END_COMP_IND, " +
                "END_ITEM_IND, FSC, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, " +
                "NIIN, PARTNO, PRODUCT_IDENTITY, PRODUCT_IND, PRODUCT_LANGUAGE, PRODUCT_LOAD_DATE, " +
                "PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, MSDSSERNO, MSDS_FILE," +
                "hcc_id, PROPRIETARY_IND, PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, " +
                "RADIOACTIVE_IND, SERVICE_AGENCY, TRADE_NAME, TRADE_SECRET_IND, created) " +
                "VALUES(@file_name, @manually_entered, @ARTICLE_IND, @CAGE, @MANUFACTURER, @DESCRIPTION, @EMERGENCY_TEL, @END_COMP_IND, " +
                "@END_ITEM_IND, @FSC, @KIT_IND, @KIT_PART_IND, @MANUFACTURER_MSDS_NO, @MIXTURE_IND, " +
                "@NIIN, @PARTNO, @PRODUCT_IDENTITY, @PRODUCT_IND, @PRODUCT_LANGUAGE, @PRODUCT_LOAD_DATE, " +
                "@PRODUCT_RECORD_STATUS, @PRODUCT_REVISION_NO, @MSDSSERNO, @MSDS_FILE," +
                "@hcc_id, @PROPRIETARY_IND, @PUBLISHED_IND, @PURCHASED_PROD_IND, @PURE_IND, " +
                "@RADIOACTIVE_IND, @SERVICE_AGENCY, @TRADE_NAME, @TRADE_SECRET_IND, getDate()); SELECT SCOPE_IDENTITY() ";


            string stmtPhysChemical = "INSERT INTO msds_phys_chemical (msds_id, VAPOR_PRESS, " +
                "VAPOR_DENS, SPECIFIC_GRAV, VOC_POUNDS_GALLON, VOC_GRAMS_LITER, " +
                "PH, VISCOSITY, EVAP_RATE_REF, SOL_IN_WATER, APP_ODOR, PERCENT_VOL_VOLUME, " +
                "AUTOIGNITION_TEMP, CARCINOGEN_IND, EPA_ACUTE, EPA_CHRONIC, EPA_FIRE, " +
                "EPA_PRESSURE, EPA_REACTIVITY, FLASH_PT_TEMP, NEUT_AGENT, " +
                "NFPA_FLAMMABILITY, NFPA_HEALTH, NFPA_REACTIVITY, NFPA_SPECIAL, " +
                "OSHA_CARCINOGENS, OSHA_COMB_LIQUID, OSHA_COMP_GAS, OSHA_CORROSIVE, " +
                "OSHA_EXPLOSIVE, OSHA_FLAMMABLE, OSHA_HIGH_TOXIC, OSHA_IRRITANT, OSHA_ORG_PEROX, " +
                "OSHA_OTHERLONGTERM, OSHA_OXIDIZER, OSHA_PYRO, OSHA_SENSITIZER, OSHA_TOXIC, " +
                "OSHA_UNST_REACT, OTHER_SHORT_TERM, PHYS_STATE_CODE, VOL_ORG_COMP_WT, " +
                "OSHA_WATER_REACTIVE) " +
                "VALUES (@msds_id, @VAPOR_PRESS, @VAPOR_DENS, @SPECIFIC_GRAV, @VOC_POUNDS_GALLON, @VOC_GRAMS_LITER, " +
                "@PH, @VISCOSITY, @EVAP_RATE_REF, @SOL_IN_WATER, @APP_ODOR, @PERCENT_VOL_VOLUME, @AUTOIGNITION_TEMP, @CARCINOGEN_IND, @EPA_ACUTE, " +
                "@EPA_CHRONIC, @EPA_FIRE, @EPA_PRESSURE, @EPA_REACTIVITY, @FLASH_PT_TEMP, @NEUT_AGENT, @NFPA_FLAMMABILITY, @NFPA_HEALTH, @NFPA_REACTIVITY, @NFPA_SPECIAL, " +
                "@OSHA_CARCINOGENS, @OSHA_COMB_LIQUID, @OSHA_COMP_GAS, @OSHA_CORROSIVE, @OSHA_EXPLOSIVE, @OSHA_FLAMMABLE, @OSHA_HIGH_TOXIC, @OSHA_IRRITANT, @OSHA_ORG_PEROX, " +
                "@OSHA_OTHERLONGTERM, @OSHA_OXIDIZER, @OSHA_PYRO, @OSHA_SENSITIZER, @OSHA_TOXIC, @OSHA_UNST_REACT, @OTHER_SHORT_TERM, @PHYS_STATE_CODE, @VOL_ORG_COMP_WT, " +
                "@OSHA_WATER_REACTIVE); ";


            string stmtIngredients = "INSERT INTO msds_ingredients (msds_id, CAS, RTECS_NUM, RTECS_CODE, INGREDIENT_NAME, PRCNT, OSHA_PEL, OSHA_STEL, ACGIH_TLV, " +
                                                "ACGIH_STEL, EPA_REPORT_QTY, DOT_REPORT_QTY, PRCNT_VOL_VALUE, PRCNT_VOL_WEIGHT, CHEM_MFG_COMP_NAME, ODS_IND, OTHER_REC_LIMITS) " +
                                                "VALUES (@msds_id, @CAS, @RTECS_NUM, @RTECS_CODE, @INGREDIENT_NAME, @PRCNT, @OSHA_PEL, @OSHA_STEL, @ACGIH_TLV, " +
                                                "@ACGIH_STEL, @EPA_REPORT_QTY, @DOT_REPORT_QTY, @PRCNT_VOL_VALUE, @PRCNT_VOL_WEIGHT, @CHEM_MFG_COMP_NAME, @ODS_IND, @OTHER_REC_LIMITS); ";



            string msds_Contractor_Info_InsertCmd = "INSERT INTO msds_contractor_info (CT_NUMBER, CT_CAGE, CT_CITY, CT_COMPANY_NAME, CT_COUNTRY, CT_PO_BOX, CT_PHONE, " +
                                                    "PURCHASE_ORDER_NO, CT_STATE, msds_id) " +
                                                    "VALUES (@CT_NUMBER, @CT_CAGE, @CT_CITY, @CT_COMPANY_NAME, @CT_COUNTRY, @CT_PO_BOX, @CT_PHONE, " +
                                                    "@PURCHASE_ORDER_NO, @CT_STATE, @msds_id); ";


            string msds_Radiological_Info_InsertCmd = "INSERT INTO msds_radiological_info (NRC_LP_NUM, OPERATOR, RAD_AMOUNT_MICRO, RAD_FORM, RAD_CAS, RAD_NAME, RAD_SYMBOL, REP_NSN, SEALED, msds_id) " +
                                                                 "VALUES (@NRC_LP_NUM, @OPERATOR, @RAD_AMOUNT_MICRO, @RAD_FORM, @RAD_CAS, @RAD_NAME, @RAD_SYMBOL, @REP_NSN, @SEALED, @msds_id); ";


            string msds_Transportation_InsertCmd = "INSERT INTO msds_transportation (AF_MMAC_CODE, CERTIFICATE_COE, COMPETENT_CAA, DOD_ID_CODE, DOT_EXEMPTION_NO, DOT_RQ_IND, EX_NO, FLASH_PT_TEMP, " +
                                                   "HIGH_EXPLOSIVE_WT, LTD_QTY_IND, MAGNETIC_IND, MAGNETISM, MARINE_POLLUTANT_IND, NET_EXP_QTY_DIST, NET_EXP_WEIGHT, NET_PROPELLANT_WT, " +
                                                   "NOS_TECHNICAL_SHIPPING_NAME, TRANSPORTATION_ADDITIONAL_DATA, msds_id) " +
                                                   "VALUES (@AF_MMAC_CODE, @CERTIFICATE_COE, @COMPETENT_CAA, @DOD_ID_CODE, @DOT_EXEMPTION_NO, @DOT_RQ_IND, @EX_NO, @FLASH_PT_TEMP, " +
                                                   "@HIGH_EXPLOSIVE_WT, @LTD_QTY_IND, @MAGNETIC_IND, @MAGNETISM, @MARINE_POLLUTANT_IND, @NET_EXP_QTY_DIST, @NET_EXP_WEIGHT, @NET_PROPELLANT_WT, " +
                                                   "@NOS_TECHNICAL_SHIPPING_NAME, @TRANSPORTATION_ADDITIONAL_DATA, @msds_id); ";

            string msds_DOT_PSN_InsertCmd = "INSERT INTO msds_dot_psn (DOT_HAZARD_CLASS_DIV, DOT_HAZARD_LABEL, DOT_MAX_CARGO, DOT_MAX_PASSENGER, DOT_PACK_BULK, DOT_PACK_EXCEPTIONS, " +
                                            "DOT_PACK_NONBULK, DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER, DOT_PSN_CODE, DOT_SPECIAL_PROVISION, DOT_SYMBOLS, DOT_UN_ID_NUMBER, " +
                                            "DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW, DOT_PACK_GROUP, msds_id) " +
                                            "VALUES (@DOT_HAZARD_CLASS_DIV, @DOT_HAZARD_LABEL, @DOT_MAX_CARGO, @DOT_MAX_PASSENGER, @DOT_PACK_BULK, @DOT_PACK_EXCEPTIONS, " +
                                            "@DOT_PACK_NONBULK, @DOT_PROP_SHIP_NAME, @DOT_PROP_SHIP_MODIFIER, @DOT_PSN_CODE, @DOT_SPECIAL_PROVISION, @DOT_SYMBOLS, @DOT_UN_ID_NUMBER, " +
                                            "@DOT_WATER_OTHER_REQ, @DOT_WATER_VESSEL_STOW, @DOT_PACK_GROUP, @msds_id); ";


            string msds_AFJM_PSN_InsertCmd = "INSERT INTO msds_afjm_psn (AFJM_HAZARD_CLASS, AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP, AFJM_PROP_SHIP_NAME, AFJM_PROP_SHIP_MODIFIER, " +
                                            "AFJM_PSN_CODE, AFJM_SPECIAL_PROV, AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS, AFJM_UN_ID_NUMBER, msds_id) " +
                                            "VALUES (@AFJM_HAZARD_CLASS, @AFJM_PACK_PARAGRAPH, @AFJM_PACK_GROUP, @AFJM_PROP_SHIP_NAME, @AFJM_PROP_SHIP_MODIFIER, " +
                                            "@AFJM_PSN_CODE, @AFJM_SPECIAL_PROV, @AFJM_SUBSIDIARY_RISK, @AFJM_SYMBOLS, @AFJM_UN_ID_NUMBER, @msds_id); ";


            string msds_IATA_PSN_InsertCmd = "INSERT INTO msds_iata_psn (IATA_CARGO_PACKING, IATA_HAZARD_CLASS, IATA_HAZARD_LABEL, IATA_PACK_GROUP, IATA_PASS_AIR_PACK_LMT_INSTR, " +
                                            "IATA_PASS_AIR_PACK_LMT_PER_PKG, IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME, IATA_PROP_SHIP_MODIFIER, IATA_CARGO_PACK_MAX_QTY, " +
                                            "IATA_PSN_CODE, IATA_PASS_AIR_MAX_QTY, IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK, IATA_UN_ID_NUMBER, msds_id) " +
                                            "VALUES (@IATA_CARGO_PACKING, @IATA_HAZARD_CLASS, @IATA_HAZARD_LABEL, @IATA_PACK_GROUP, @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                                            "@IATA_PASS_AIR_PACK_LMT_PER_PKG, @IATA_PASS_AIR_PACK_NOTE, @IATA_PROP_SHIP_NAME, @IATA_PROP_SHIP_MODIFIER, @IATA_CARGO_PACK_MAX_QTY, " +
                                            "@IATA_PSN_CODE, @IATA_PASS_AIR_MAX_QTY, @IATA_SPECIAL_PROV, @IATA_SUBSIDIARY_RISK, @IATA_UN_ID_NUMBER, @msds_id); ";


            string msds_IMO_PSN_InsertCmd = "INSERT INTO msds_imo_psn (IMO_EMS_NO, IMO_HAZARD_CLASS, IMO_IBC_INSTR, IMO_LIMITED_QTY, IMO_PACK_GROUP, IMO_PACK_INSTRUCTIONS, IMO_PACK_PROVISIONS, " +
                                           "IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER, IMO_PSN_CODE, IMO_SPECIAL_PROV, IMO_STOW_SEGR, IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO, IMO_TANK_INSTR_PROV, " +
                                           "IMO_TANK_INSTR_UN, IMO_UN_NUMBER, IMO_IBC_PROVISIONS, msds_id) " +
                                           "VALUES (@IMO_EMS_NO, @IMO_HAZARD_CLASS, @IMO_IBC_INSTR, @IMO_LIMITED_QTY, @IMO_PACK_GROUP, @IMO_PACK_INSTRUCTIONS, @IMO_PACK_PROVISIONS, " +
                                           "@IMO_PROP_SHIP_NAME, @IMO_PROP_SHIP_MODIFIER, @IMO_PSN_CODE, @IMO_SPECIAL_PROV, @IMO_STOW_SEGR, @IMO_SUBSIDIARY_RISK, @IMO_TANK_INSTR_IMO, @IMO_TANK_INSTR_PROV, " +
                                           "@IMO_TANK_INSTR_UN, @IMO_UN_NUMBER, @IMO_IBC_PROVISIONS, @msds_id); ";




            string stmtItemDescription  = "INSERT INTO msds_item_description (msds_id, ITEM_MANAGER, ITEM_NAME, SPECIFICATION_NUMBER, TYPE_GRADE_CLASS, UNIT_OF_ISSUE, " +
                                                     "QUANTITATIVE_EXPRESSION, UI_CONTAINER_QTY, TYPE_OF_CONTAINER, BATCH_NUMBER, LOT_NUMBER, LOG_FLIS_NIIN_VER, LOG_FSC, NET_UNIT_WEIGHT, " +
                                                     "SHELF_LIFE_CODE, SPECIAL_EMP_CODE, UN_NA_NUMBER, UPC_GTIN) " +
                                                     "VALUES (@msds_id, @ITEM_MANAGER, @ITEM_NAME, @SPECIFICATION_NUMBER, @TYPE_GRADE_CLASS, @UNIT_OF_ISSUE, " +
                                                     "@QUANTITATIVE_EXPRESSION, @UI_CONTAINER_QTY, @TYPE_OF_CONTAINER, @BATCH_NUMBER, @LOT_NUMBER, @LOG_FLIS_NIIN_VER, @LOG_FSC, @NET_UNIT_WEIGHT, " +
                                                     "@SHELF_LIFE_CODE, @SPECIAL_EMP_CODE, @UN_NA_NUMBER, @UPC_GTIN); ";


            string msds_Label_Info_InsertCmd = "INSERT INTO msds_label_info (COMPANY_CAGE_RP, COMPANY_NAME_RP, LABEL_EMERG_PHONE, LABEL_ITEM_NAME, LABEL_PROC_YEAR, LABEL_PROD_IDENT, " +
                                           "LABEL_PROD_SERIALNO, LABEL_SIGNAL_WORD, LABEL_STOCK_NO, SPECIFIC_HAZARDS, msds_id) " +
                                           "VALUES (@COMPANY_CAGE_RP, @COMPANY_NAME_RP, @LABEL_EMERG_PHONE, @LABEL_ITEM_NAME, @LABEL_PROC_YEAR, @LABEL_PROD_IDENT, " +
                                           "@LABEL_PROD_SERIALNO, @LABEL_SIGNAL_WORD, @LABEL_STOCK_NO, @SPECIFIC_HAZARDS, @msds_id); ";

            string msds_Disposal_InsertCmd = "INSERT INTO msds_disposal (DISPOSAL_ADD_INFO, EPA_HAZ_WASTE_CODE, EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME, msds_id) " +
                                             "VALUES (@DISPOSAL_ADD_INFO, @EPA_HAZ_WASTE_CODE, @EPA_HAZ_WASTE_IND, @EPA_HAZ_WASTE_NAME, @msds_id); ";

            string msds_Document_Types_InsertCmd = "INSERT INTO msds_document_types (MANUFACTURER_LABEL, manufacturer_label_filename, MSDS_TRANSLATED, NESHAP_COMP_CERT, OTHER_DOCS, PRODUCT_SHEET, " +
                                                    "TRANSPORTATION_CERT, msds_translated_filename, neshap_comp_filename, other_docs_filename, product_sheet_filename, " +
                                                    "transportation_cert_filename, msds_id) " +
                                                    "VALUES (@MANUFACTURER_LABEL, @manufacturer_label_filename, @MSDS_TRANSLATED, @NESHAP_COMP_CERT, @OTHER_DOCS, @PRODUCT_SHEET, " +
                                                    "@TRANSPORTATION_CERT, @msds_translated_filename, @neshap_comp_filename, @other_docs_filename, @product_sheet_filename, " +
                                                    "@transportation_cert_filename, @msds_id); ";



            SqlCommand cmd_msds = new SqlCommand(stmtMSDS, conn);
            cmd_msds.Transaction = tran;
            
            SqlCommand cmd_ing = new SqlCommand(stmtIngredients, conn);
            cmd_ing.Transaction = tran;
            SqlCommand cmd_desc = new SqlCommand(stmtItemDescription, conn);
            cmd_desc.Transaction = tran;
            SqlCommand cmd_phys = new SqlCommand(stmtPhysChemical, conn);
            cmd_phys.Transaction = tran;
            SqlCommand cmd_contract = new SqlCommand(msds_Contractor_Info_InsertCmd, conn);
            cmd_contract.Transaction = tran;
            SqlCommand cmd_radio = new SqlCommand(msds_Radiological_Info_InsertCmd, conn);
            cmd_radio.Transaction = tran;
            SqlCommand cmd_transport = new SqlCommand(msds_Transportation_InsertCmd, conn);
            cmd_transport.Transaction = tran;
            SqlCommand cmd_dot = new SqlCommand(msds_DOT_PSN_InsertCmd, conn);
            cmd_dot.Transaction = tran;
            SqlCommand cmd_afjm = new SqlCommand(msds_AFJM_PSN_InsertCmd, conn);
            cmd_afjm.Transaction = tran;
            SqlCommand cmd_iata = new SqlCommand(msds_IATA_PSN_InsertCmd, conn);
            cmd_iata.Transaction = tran;
            SqlCommand cmd_imo = new SqlCommand(msds_IMO_PSN_InsertCmd, conn);
            cmd_imo.Transaction = tran;
            SqlCommand cmd_label = new SqlCommand(msds_Label_Info_InsertCmd, conn);
            cmd_label.Transaction = tran;
            SqlCommand cmd_disposal = new SqlCommand(msds_Disposal_InsertCmd, conn);
            cmd_disposal.Transaction = tran;
            SqlCommand cmd_docs = new SqlCommand(msds_Document_Types_InsertCmd, conn);
            cmd_docs.Transaction = tran;

            int msds_id = 0;

            //insert msds
            cmd_msds.Parameters.AddWithValue("@MSDSSERNO", dbNull(msds.MSDSSERNO));
            cmd_msds.Parameters.AddWithValue("@CAGE", dbNull(msds.CAGE));            
            cmd_msds.Parameters.AddWithValue("@PARTNO", dbNull(msds.PARTNO));
            cmd_msds.Parameters.AddWithValue("@FSC", dbNull(msds.FSC));
            cmd_msds.Parameters.AddWithValue("@NIIN", dbNull(msds.NIIN));
            cmd_msds.Parameters.AddWithValue("@hcc_id", dbNull(msds.hcc_id));
            cmd_msds.Parameters.AddWithValue("@MANUFACTURER", dbNull(msds.MANUFACTURER));

            if (msds.msds_file != null)
                cmd_msds.Parameters.AddWithValue("@msds_file", dbNull(msds.msds_file));
            else
            {

                SqlParameter p = new SqlParameter("@msds_file", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_msds.Parameters.Add(p);
            }
           
            cmd_msds.Parameters.AddWithValue("@file_name", dbNull(msds.file_name));
            cmd_msds.Parameters.AddWithValue("@manually_entered", dbNull(manually_entered));

            cmd_msds.Parameters.AddWithValue("@ARTICLE_IND", dbNull(msds.ARTICLE_IND));
            cmd_msds.Parameters.AddWithValue("@DESCRIPTION", dbNull(msds.DESCRIPTION));
            cmd_msds.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(msds.EMERGENCY_TEL));
            cmd_msds.Parameters.AddWithValue("@END_COMP_IND", dbNull(msds.END_COMP_IND));
            cmd_msds.Parameters.AddWithValue("@END_ITEM_IND", dbNull(msds.END_ITEM_IND));
            cmd_msds.Parameters.AddWithValue("@KIT_IND", dbNull(msds.KIT_IND));
            cmd_msds.Parameters.AddWithValue("@KIT_PART_IND", dbNull(msds.KIT_PART_IND));
            cmd_msds.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(msds.MANUFACTURER_MSDS_NO));
            cmd_msds.Parameters.AddWithValue("@MIXTURE_IND", dbNull(msds.MIXTURE_IND));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(msds.PRODUCT_IDENTITY));            
            cmd_msds.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(msds.PRODUCT_LOAD_DATE));            
            cmd_msds.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(msds.PRODUCT_RECORD_STATUS));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(msds.PRODUCT_REVISION_NO));
            cmd_msds.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(msds.PROPRIETARY_IND));
            cmd_msds.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(msds.PUBLISHED_IND));
            cmd_msds.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(msds.PURCHASED_PROD_IND));
            cmd_msds.Parameters.AddWithValue("@PURE_IND", dbNull(msds.PURE_IND));
            cmd_msds.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(msds.RADIOACTIVE_IND));
            cmd_msds.Parameters.AddWithValue("@SERVICE_AGENCY", dbNull(msds.SERVICE_AGENCY));
            cmd_msds.Parameters.AddWithValue("@TRADE_NAME", dbNull(msds.TRADE_NAME));
            cmd_msds.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(msds.TRADE_SECRET_IND));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_IND", dbNull(msds.PRODUCT_IND));
            cmd_msds.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(msds.PRODUCT_LANGUAGE));

            try {
                object o = cmd_msds.ExecuteScalar();
                msds_id = Convert.ToInt32(o);
                cmd_msds.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS \n\t" + ex.Message);  
            }


            //insert phys chemical 
            // Make sure we set the temperatures to null if they are blank
            if (msds.AUTOIGNITION_TEMP.Length == 0)
                msds.AUTOIGNITION_TEMP = null;
            if (msds.FLASH_PT_TEMP.Length == 0)
                msds.FLASH_PT_TEMP = null;

            cmd_phys.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(msds.VAPOR_PRESS));
            cmd_phys.Parameters.AddWithValue("@VAPOR_DENS", dbNull(msds.VAPOR_DENS));
            cmd_phys.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(msds.SPECIFIC_GRAV));
            cmd_phys.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(msds.VOC_POUNDS_GALLON));
            cmd_phys.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(msds.VOC_GRAMS_LITER));
            cmd_phys.Parameters.AddWithValue("@PH", dbNull(msds.PH));
            cmd_phys.Parameters.AddWithValue("@VISCOSITY", dbNull(msds.VISCOSITY));
            cmd_phys.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(msds.EVAP_RATE_REF));
            cmd_phys.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(msds.SOL_IN_WATER));
            cmd_phys.Parameters.AddWithValue("@APP_ODOR", dbNull(msds.APP_ODOR));
            cmd_phys.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(msds.PERCENT_VOL_VOLUME));
            cmd_phys.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(msds.AUTOIGNITION_TEMP));
            cmd_phys.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(msds.CARCINOGEN_IND));
            cmd_phys.Parameters.AddWithValue("@EPA_ACUTE", dbNull(msds.EPA_ACUTE));
            cmd_phys.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(msds.EPA_CHRONIC));
            cmd_phys.Parameters.AddWithValue("@EPA_FIRE", dbNull(msds.EPA_FIRE));
            cmd_phys.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(msds.EPA_PRESSURE));
            cmd_phys.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(msds.EPA_REACTIVITY));
            cmd_phys.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(msds.FLASH_PT_TEMP));
            cmd_phys.Parameters.AddWithValue("@NEUT_AGENT", dbNull(msds.NEUT_AGENT));
            cmd_phys.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(msds.NFPA_FLAMMABILITY));
            cmd_phys.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(msds.NFPA_HEALTH));
            cmd_phys.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(msds.NFPA_REACTIVITY));
            cmd_phys.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(msds.NFPA_SPECIAL));
            cmd_phys.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(msds.OSHA_CARCINOGENS));
            cmd_phys.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(msds.OSHA_COMB_LIQUID));
            cmd_phys.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(msds.OSHA_COMP_GAS));
            cmd_phys.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(msds.OSHA_CORROSIVE));
            cmd_phys.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(msds.OSHA_EXPLOSIVE));
            cmd_phys.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(msds.OSHA_FLAMMABLE));
            cmd_phys.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(msds.OSHA_HIGH_TOXIC));
            cmd_phys.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(msds.OSHA_IRRITANT));
            cmd_phys.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(msds.OSHA_ORG_PEROX));
            cmd_phys.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(msds.OSHA_OTHERLONGTERM));
            cmd_phys.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(msds.OSHA_OXIDIZER));
            cmd_phys.Parameters.AddWithValue("@OSHA_PYRO", dbNull(msds.OSHA_PYRO));
            cmd_phys.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(msds.OSHA_SENSITIZER));
            cmd_phys.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(msds.OSHA_TOXIC));
            cmd_phys.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(msds.OSHA_UNST_REACT));
            cmd_phys.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(msds.OTHER_SHORT_TERM));
            cmd_phys.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(msds.PHYS_STATE_CODE));
            cmd_phys.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(msds.VOL_ORG_COMP_WT));
            cmd_phys.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(msds.OSHA_WATER_REACTIVE));
            
            cmd_phys.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_phys.ExecuteNonQuery();
                cmd_phys.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_phys \n\t" + ex.Message);
            }

            //insert ingredients
            foreach (MsdsIngredients ingredient in msds.ingredientsList)
            {
                
                cmd_ing.Parameters.AddWithValue("@CAS", dbNull(ingredient.CAS));
                cmd_ing.Parameters.AddWithValue("@RTECS_NUM", dbNull(ingredient.RTECS_NUM));
                cmd_ing.Parameters.AddWithValue("@RTECS_CODE", dbNull(ingredient.RTECS_CODE));
                cmd_ing.Parameters.AddWithValue("@INGREDIENT_NAME", dbNull(ingredient.INGREDIENT_NAME));
                cmd_ing.Parameters.AddWithValue("@PRCNT", dbNull(ingredient.PRCNT));               
                cmd_ing.Parameters.AddWithValue("@OSHA_PEL", dbNull(ingredient.OSHA_PEL));
                cmd_ing.Parameters.AddWithValue("@OSHA_STEL", dbNull(ingredient.OSHA_STEL));
                cmd_ing.Parameters.AddWithValue("@ACGIH_TLV", dbNull(ingredient.ACGIH_TLV));
                cmd_ing.Parameters.AddWithValue("@ACGIH_STEL", dbNull(ingredient.ACGIH_STEL));
                cmd_ing.Parameters.AddWithValue("@EPA_REPORT_QTY", dbNull(ingredient.EPA_REPORT_QTY));
                cmd_ing.Parameters.AddWithValue("@DOT_REPORT_QTY", dbNull(ingredient.DOT_REPORT_QTY));
                cmd_ing.Parameters.AddWithValue("@PRCNT_VOL_VALUE", dbNull(ingredient.PRCNT_VOL_VALUE));
                cmd_ing.Parameters.AddWithValue("@PRCNT_VOL_WEIGHT", dbNull(ingredient.PRCNT_VOL_WEIGHT));
                cmd_ing.Parameters.AddWithValue("@CHEM_MFG_COMP_NAME", dbNull(ingredient.CHEM_MFG_COMP_NAME));
                cmd_ing.Parameters.AddWithValue("@ODS_IND", dbNull(ingredient.ODS_IND));
                cmd_ing.Parameters.AddWithValue("@OTHER_REC_LIMITS", dbNull(ingredient.OTHER_REC_LIMITS));
                cmd_ing.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

                try {
                    cmd_ing.ExecuteNonQuery();
                    cmd_ing.Parameters.Clear();
                }
                catch (Exception ex) {
                    Debug.WriteLine("***** Error in insertMSDS_ing \n\t" + ex.Message);
                }
            }


            //insert contracts
            foreach (MsdsContractors contract in msds.contractorsList)
            {

                cmd_contract.Parameters.AddWithValue("@CT_NUMBER", dbNull(contract.CT_NUMBER));
                cmd_contract.Parameters.AddWithValue("@CT_CAGE", dbNull(contract.CT_CAGE));
                cmd_contract.Parameters.AddWithValue("@CT_CITY", dbNull(contract.CT_CITY));
                cmd_contract.Parameters.AddWithValue("@CT_COMPANY_NAME", dbNull(contract.CT_COMPANY_NAME));
                cmd_contract.Parameters.AddWithValue("@CT_COUNTRY", dbNull(contract.CT_COUNTRY));
                cmd_contract.Parameters.AddWithValue("@CT_PO_BOX", dbNull(contract.CT_PO_BOX));
                cmd_contract.Parameters.AddWithValue("@CT_PHONE", dbNull(contract.CT_PHONE));
                cmd_contract.Parameters.AddWithValue("@PURCHASE_ORDER_NO", dbNull(contract.PURCHASE_ORDER_NO));
                cmd_contract.Parameters.AddWithValue("@CT_STATE", dbNull(contract.CT_STATE));
                cmd_contract.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

                try {
                    cmd_contract.ExecuteNonQuery();
                    cmd_contract.Parameters.Clear();
                }
                catch (Exception ex) {
                    Debug.WriteLine("***** Error in insertMSDS_contract \n\t" + ex.Message);
                }
            }


            //insert radiological
            cmd_radio.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(msds.NRC_LP_NUM));
            cmd_radio.Parameters.AddWithValue("@OPERATOR", dbNull(msds.OPERATOR));
            cmd_radio.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(msds.RAD_AMOUNT_MICRO));
            cmd_radio.Parameters.AddWithValue("@RAD_FORM", dbNull(msds.RAD_FORM));
            cmd_radio.Parameters.AddWithValue("@RAD_CAS", dbNull(msds.RAD_CAS));
            cmd_radio.Parameters.AddWithValue("@RAD_NAME", dbNull(msds.RAD_NAME));
            cmd_radio.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(msds.RAD_SYMBOL));
            cmd_radio.Parameters.AddWithValue("@REP_NSN", dbNull(msds.REP_NSN));
            cmd_radio.Parameters.AddWithValue("@SEALED", dbNull(msds.SEALED));
           
            cmd_radio.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_radio.ExecuteNonQuery();
                cmd_radio.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_radio \n\t" + ex.Message);
            }

            //insert transport
            if (msds.TRAN_FLASH_PT_TEMP.Length == 0)
                msds.TRAN_FLASH_PT_TEMP = null;
            cmd_transport.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(msds.AF_MMAC_CODE));
            cmd_transport.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(msds.CERTIFICATE_COE));
            cmd_transport.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(msds.COMPETENT_CAA));
            cmd_transport.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(msds.DOD_ID_CODE));
            cmd_transport.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(msds.DOT_EXEMPTION_NO));
            cmd_transport.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(msds.DOT_RQ_IND));
            cmd_transport.Parameters.AddWithValue("@EX_NO", dbNull(msds.EX_NO));
            cmd_transport.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(msds.TRAN_FLASH_PT_TEMP));
            cmd_transport.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(msds.HIGH_EXPLOSIVE_WT));
            cmd_transport.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(msds.LTD_QTY_IND));
            cmd_transport.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(msds.MAGNETIC_IND));
            cmd_transport.Parameters.AddWithValue("@MAGNETISM", dbNull(msds.MAGNETISM));
            cmd_transport.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(msds.MARINE_POLLUTANT_IND));
            cmd_transport.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(msds.NET_EXP_QTY_DIST));
            cmd_transport.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(msds.NET_EXP_WEIGHT));
            cmd_transport.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(msds.NET_PROPELLANT_WT));
            cmd_transport.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(msds.NOS_TECHNICAL_SHIPPING_NAME));
            cmd_transport.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(msds.TRANSPORTATION_ADDITIONAL_DATA));
            
            cmd_transport.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_transport.ExecuteNonQuery();
                cmd_transport.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_transport \n\t" + ex.Message);
            }

            //insert dot psn
            cmd_dot.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(msds.DOT_HAZARD_CLASS_DIV));
            cmd_dot.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(msds.DOT_HAZARD_LABEL));
            cmd_dot.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(msds.DOT_MAX_CARGO));
            cmd_dot.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(msds.DOT_MAX_PASSENGER));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(msds.DOT_PACK_BULK));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(msds.DOT_PACK_EXCEPTIONS));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(msds.DOT_PACK_NONBULK));
            cmd_dot.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(msds.DOT_PROP_SHIP_NAME));
            cmd_dot.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(msds.DOT_PROP_SHIP_MODIFIER));
            cmd_dot.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(msds.DOT_PSN_CODE));
            cmd_dot.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(msds.DOT_SPECIAL_PROVISION));
            cmd_dot.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(msds.DOT_SYMBOLS));
            cmd_dot.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(msds.DOT_UN_ID_NUMBER));
            cmd_dot.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(msds.DOT_WATER_OTHER_REQ));
            cmd_dot.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(msds.DOT_WATER_VESSEL_STOW));
            cmd_dot.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(msds.DOT_PACK_GROUP));
            
            cmd_dot.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_dot.ExecuteNonQuery();
                cmd_dot.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_dot \n\t" + ex.Message);
            }


            //insert afjm psn
            cmd_afjm.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(msds.AFJM_HAZARD_CLASS));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(msds.AFJM_PACK_PARAGRAPH));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(msds.AFJM_PACK_GROUP));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(msds.AFJM_PROP_SHIP_NAME));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(msds.AFJM_PROP_SHIP_MODIFIER));
            cmd_afjm.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(msds.AFJM_PSN_CODE));
            cmd_afjm.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(msds.AFJM_SPECIAL_PROV));
            cmd_afjm.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(msds.AFJM_SUBSIDIARY_RISK));
            cmd_afjm.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(msds.AFJM_SYMBOLS));
            cmd_afjm.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(msds.AFJM_UN_ID_NUMBER));
           
            cmd_afjm.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_afjm.ExecuteNonQuery();
                cmd_afjm.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_afjm \n\t" + ex.Message);
            }


            //insert iata psn
            cmd_iata.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(msds.IATA_CARGO_PACKING));
            cmd_iata.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(msds.IATA_HAZARD_CLASS));
            cmd_iata.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(msds.IATA_HAZARD_LABEL));
            cmd_iata.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(msds.IATA_PACK_GROUP));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(msds.IATA_PASS_AIR_PACK_LMT_INSTR));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(msds.IATA_PASS_AIR_PACK_LMT_PER_PKG));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(msds.IATA_PASS_AIR_PACK_NOTE));
            cmd_iata.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(msds.IATA_PROP_SHIP_NAME));
            cmd_iata.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(msds.IATA_PROP_SHIP_MODIFIER));
            cmd_iata.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(msds.IATA_CARGO_PACK_MAX_QTY));
            cmd_iata.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(msds.IATA_PSN_CODE));
            cmd_iata.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(msds.IATA_PASS_AIR_MAX_QTY));
            cmd_iata.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(msds.IATA_SPECIAL_PROV));
            cmd_iata.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(msds.IATA_SUBSIDIARY_RISK));
            cmd_iata.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(msds.IATA_UN_ID_NUMBER));
            
            cmd_iata.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_iata.ExecuteNonQuery();
                cmd_iata.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_iata \n\t" + ex.Message);
            }

            //insert imo psn
            cmd_imo.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(msds.IMO_EMS_NO));
            cmd_imo.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(msds.IMO_HAZARD_CLASS));
            cmd_imo.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(msds.IMO_IBC_INSTR));
            cmd_imo.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(msds.IMO_LIMITED_QTY));
            cmd_imo.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(msds.IMO_PACK_GROUP));
            cmd_imo.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(msds.IMO_PACK_INSTRUCTIONS));
            cmd_imo.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(msds.IMO_PACK_PROVISIONS));
            cmd_imo.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(msds.IMO_PROP_SHIP_NAME));
            cmd_imo.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(msds.IMO_PROP_SHIP_MODIFIER));
            cmd_imo.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(msds.IMO_PSN_CODE));
            cmd_imo.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(msds.IMO_SPECIAL_PROV));
            cmd_imo.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(msds.IMO_STOW_SEGR));
            cmd_imo.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(msds.IMO_SUBSIDIARY_RISK));
            cmd_imo.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(msds.IMO_TANK_INSTR_IMO));
            cmd_imo.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(msds.IMO_TANK_INSTR_PROV));
            cmd_imo.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(msds.IMO_TANK_INSTR_UN));
            cmd_imo.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(msds.IMO_UN_NUMBER));
            cmd_imo.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(msds.IMO_IBC_PROVISIONS));
           
            cmd_imo.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_imo.ExecuteNonQuery();
                cmd_imo.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_imo \n\t" + ex.Message);
            }


            //insert item description
            cmd_desc.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(msds.ITEM_MANAGER));
            cmd_desc.Parameters.AddWithValue("@ITEM_NAME", dbNull(msds.ITEM_NAME));
            cmd_desc.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(msds.SPECIFICATION_NUMBER));
            cmd_desc.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(msds.TYPE_GRADE_CLASS));
            cmd_desc.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(msds.UNIT_OF_ISSUE));
            cmd_desc.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(msds.QUANTITATIVE_EXPRESSION));
            cmd_desc.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(msds.UI_CONTAINER_QTY));
            cmd_desc.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(msds.TYPE_OF_CONTAINER));
            cmd_desc.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(msds.BATCH_NUMBER));
            cmd_desc.Parameters.AddWithValue("@LOT_NUMBER", dbNull(msds.LOT_NUMBER));
            cmd_desc.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(msds.LOG_FLIS_NIIN_VER));
            cmd_desc.Parameters.AddWithValue("@LOG_FSC", dbNull(msds.LOG_FSC));
            cmd_desc.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(msds.NET_UNIT_WEIGHT));
            cmd_desc.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(msds.SHELF_LIFE_CODE));
            cmd_desc.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(msds.SPECIAL_EMP_CODE));
            cmd_desc.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(msds.UN_NA_NUMBER));
            cmd_desc.Parameters.AddWithValue("@UPC_GTIN", dbNull(msds.UPC_GTIN));
            
            cmd_desc.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_desc.ExecuteNonQuery();
                cmd_desc.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_desc \n\t" + ex.Message);
            }

            //insert label info
            cmd_label.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(msds.COMPANY_CAGE_RP));
            cmd_label.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(msds.COMPANY_NAME_RP));
            cmd_label.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(msds.LABEL_EMERG_PHONE));
            cmd_label.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(msds.LABEL_ITEM_NAME));
            cmd_label.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(msds.LABEL_PROC_YEAR));
            cmd_label.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(msds.LABEL_PROD_IDENT));
            cmd_label.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(msds.LABEL_PROD_SERIALNO));
            cmd_label.Parameters.AddWithValue("@LABEL_SIGNAL_WORD", dbNull(msds.LABEL_SIGNAL_WORD));
            cmd_label.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(msds.LABEL_STOCK_NO));
            cmd_label.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(msds.SPECIFIC_HAZARDS));
           
            cmd_label.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_label.ExecuteNonQuery();
                cmd_label.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_label \n\t" + ex.Message);
            }


            //insert disposal
            cmd_disposal.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(msds.DISPOSAL_ADD_INFO));
            cmd_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(msds.EPA_HAZ_WASTE_CODE));
            cmd_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(msds.EPA_HAZ_WASTE_IND));
            cmd_disposal.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(msds.EPA_HAZ_WASTE_NAME));

            cmd_disposal.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_disposal.ExecuteNonQuery();
                cmd_disposal.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_disposal \n\t" + ex.Message);
            }

            //insert doc types
            if (msds.MANUFACTURER_LABEL != null)
                cmd_docs.Parameters.AddWithValue("@MANUFACTURER_LABEL", dbNull(msds.MANUFACTURER_LABEL));
            else
            {

                SqlParameter p = new SqlParameter("@MANUFACTURER_LABEL", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.MSDS_TRANSLATED != null)
                cmd_docs.Parameters.AddWithValue("@MSDS_TRANSLATED", dbNull(msds.MSDS_TRANSLATED));
            else
            {

                SqlParameter p = new SqlParameter("@MSDS_TRANSLATED", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.NESHAP_COMP_CERT != null)
                cmd_docs.Parameters.AddWithValue("@NESHAP_COMP_CERT", dbNull(msds.NESHAP_COMP_CERT));
            else
            {

                SqlParameter p = new SqlParameter("@NESHAP_COMP_CERT", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.OTHER_DOCS != null)
                cmd_docs.Parameters.AddWithValue("@OTHER_DOCS", dbNull(msds.OTHER_DOCS));
            else
            {

                SqlParameter p = new SqlParameter("@OTHER_DOCS", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
             if (msds.PRODUCT_SHEET != null)
                 cmd_docs.Parameters.AddWithValue("@PRODUCT_SHEET", dbNull(msds.PRODUCT_SHEET));
            else
            {

                SqlParameter p = new SqlParameter("@PRODUCT_SHEET", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            if (msds.TRANSPORTATION_CERT != null)
                cmd_docs.Parameters.AddWithValue("@TRANSPORTATION_CERT", dbNull(msds.TRANSPORTATION_CERT));
            else
            {

                SqlParameter p = new SqlParameter("@TRANSPORTATION_CERT", SqlDbType.Image);
                p.Value = DBNull.Value;
                cmd_docs.Parameters.Add(p);
            }
            cmd_docs.Parameters.AddWithValue("@msds_translated_filename", dbNull(msds.msds_translated_filename));
            cmd_docs.Parameters.AddWithValue("@neshap_comp_filename", dbNull(msds.neshap_comp_filename));
            cmd_docs.Parameters.AddWithValue("@other_docs_filename", dbNull(msds.other_docs_filename));
            cmd_docs.Parameters.AddWithValue("@product_sheet_filename", dbNull(msds.product_sheet_filename));
            cmd_docs.Parameters.AddWithValue("@transportation_cert_filename", dbNull(msds.transportation_cert_filename));
            cmd_docs.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(msds.manufacturer_label_filename));
            
            cmd_docs.Parameters.AddWithValue("@msds_id", dbNull(msds_id));

            try {
                cmd_docs.ExecuteNonQuery();
                cmd_docs.Parameters.Clear();
            }
            catch (Exception ex) {
                Debug.WriteLine("***** Error in insertMSDS_docs \n\t" + ex.Message);
            }
 

            tran.Commit();
            conn.Close();
        }//end MSDS


        public AddInventoryModel AttemptToGetSMCLInformation(string niin)
        {
            bool itemFound = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            AddInventoryModel model = new AddInventoryModel(); //will use this to keep track of what data is coming from the SMCL and what is coming from the inventory details table
            conn.Open();

            //check SMCL information first, if something is in the SMCL it should overwrite whatever is in the inventory table
            string selectStmt = "select nc.fsc, nc.description, nc.ui, nc.remarks, nc.allowance_qty, nc.um, nc.spmig, nc.specs, nc.usage_category_id, nc.shelf_life_code_id, nc.shelf_life_action_code_id, nc.smcc_id from niin_catalog nc " +
                    " where nc.niin = @niin; ";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@niin", niin);
            DataTable smclData = new DataTable();
            new SqlDataAdapter(cmd).Fill(smclData);
            cmd = new SqlCommand(@"select MAX(hcc_id) from msds where niin = @niin", conn);
            cmd.Parameters.AddWithValue("@niin", niin);
            var result = cmd.ExecuteScalar().ToString();
            int hcc;
            Int32.TryParse(result, out hcc);
            if (smclData.Rows.Count != 0)
            {
                var currentRow = smclData.Rows[0];
                model.fsc = currentRow["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["fsc"]) : null;
                model.niin = niin;
                model.name = currentRow["description"] != DBNull.Value ? currentRow["description"].ToString() : null;
                model.ui = currentRow["ui"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["ui"]) : null;
                model.um = currentRow["um"] != DBNull.Value ? currentRow["um"].ToString() : null;
                model.spmig = currentRow["spmig"] != DBNull.Value ? currentRow["spmig"].ToString() : null;
                model.specs = currentRow["specs"] != DBNull.Value ? currentRow["specs"].ToString() : null;
                model.remarks = currentRow["remarks"] != DBNull.Value ? currentRow["remarks"].ToString() : null;
                model.usage_category_id = currentRow["usage_category_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["usage_category_id"]) : null;
                model.slc_id = currentRow["shelf_life_code_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["shelf_life_code_id"]) : null;
                model.slac_id = currentRow["shelf_life_action_code_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["shelf_life_action_code_id"]) : null;
                model.smcc_id = currentRow["smcc_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["smcc_id"]) : null;
                model.allowance_qty = currentRow["allowance_qty"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["allowance_qty"]) : 0;
                model.hcc_id = hcc != 0 ? (int?)hcc : null;
                model.notes = null;
            }
            else
            {
                //if the item's not in the SMCL, usage category is automatically "not evaluated"
                model.usage_category_id = 5;
                //check for current SFR 
                string selectSFR = "select * from sfr where niin = @niin";
                cmd = new SqlCommand(selectSFR, conn);
                cmd.Parameters.AddWithValue("@niin", niin);
                DataTable sfrData = new DataTable();
                new SqlDataAdapter(cmd).Fill(sfrData);
                if (sfrData.Rows.Count != 0)
                {
                    var currentRow = sfrData.Rows[0];
                    model.hasSfr = true;
                    model.fsc = currentRow["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["fsc"]) : null;
                    model.niin = niin;
                    model.ui = currentRow["ui_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["ui_id"]) : null;
                    model.name = currentRow["item_name"] != DBNull.Value ? currentRow["item_name"].ToString() : null;
                }
            }
            //we have the SMCL information handy, now we need to check the inventory table to see if any missing information has been filled in by a user but is not yet
            //incorporated into the SMCL. if this item is already in the inventory we will also have some rows to put into the grid on the add page.
            string selectInv = "select * from inventory_detail where niin = @niin";
            cmd = new SqlCommand(selectInv, conn);
            cmd.Parameters.AddWithValue("@niin", niin);
            DataTable invData = new DataTable();
            new SqlDataAdapter(cmd).Fill(invData);
            if (invData.Rows.Count != 0)
            {
                var currentRow = invData.Rows[0];
                model.fsc = (model.fsc == null && currentRow["fsc"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["fsc"]) : model.fsc;
                model.niin = niin;
                model.name = (model.name == null && currentRow["item_name"] != DBNull.Value) ? currentRow["item_name"].ToString() : model.name;
                model.ui = (model.ui == null && currentRow["ui_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["ui_id"]) : model.ui;
                model.um = (model.um == null && currentRow["um"] != DBNull.Value) ? currentRow["um"].ToString() : model.um;
                model.spmig = (model.spmig == null && currentRow["spmig"] != DBNull.Value) ? currentRow["spmig"].ToString() : model.spmig;
                model.specs = (model.specs == null && currentRow["specs"] != DBNull.Value) ? currentRow["specs"].ToString() : model.specs;
                model.usage_category_id = (model.usage_category_id == null && currentRow["usage_category_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["usage_category_id"]) : model.usage_category_id;
                model.slc_id = (model.slc_id == null && currentRow["slc_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["slc_id"]) : model.slc_id;
                model.slac_id = (model.slac_id == null && currentRow["slac_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["slac_id"]) : model.slac_id;
                model.hcc_id = (model.hcc_id == null && currentRow["hcc_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["hcc_id"]) : model.hcc_id;
                model.smcc_id = (model.smcc_id == null && currentRow["smcc_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["smcc_id"]) : model.smcc_id;
                model.notes = currentRow["notes"] != DBNull.Value ? currentRow["notes"].ToString() : null;
                model.inventory_detail_id = currentRow["inventory_detail_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["inventory_detail_id"]) : null;
            }
            conn.Close();
            return model;
        }

        public List<SFRRowModel> getSFRCollection()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select fsc, niin, cage, item_name, CONVERT(varchar(10), date_submitted, 110) as date_submitted, CONVERT(varchar(10), date_mailed, 110) as date_mailed, sfr_id from sfr where completed_flag = 0";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            List<SFRRowModel> data = new List<SFRRowModel>();
            foreach (DataRow row in dt.Rows)
            {
                var results = new SFRRowModel()
                {
                    sfr_id = (int?)Convert.ToInt32(row["sfr_id"]),
                    date_submitted = row["date_submitted"] != DBNull.Value ? Convert.ToDateTime(row["date_submitted"]).ToString("MM/dd/yyyy") : null,
                    date_mailed = row["date_mailed"] != DBNull.Value ? Convert.ToDateTime(row["date_mailed"]).ToString("MM/dd/yyyy") : null,
                    cage = row["cage"] != DBNull.Value ? row["cage"].ToString() : null,
                    item_name = row["item_name"] != DBNull.Value ? row["item_name"].ToString() : null,
                    fsc = row["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(row["fsc"]) : null,
                    niin = row["niin"] != DBNull.Value ? row["niin"].ToString() : null
                };
                data.Add(results);
            }
            conn.Close();

            return data;
        }

        public void AddSimpleSfr(SimpleSfr sfr)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            var ship = getCurrentShip();
            var ship_id = ship.Rows[0]["ship_id"];
            string stmt = "insert into sfr (ship_id, fsc, niin, username, sfr_type, inventory_detail_id, completed_flag, item_name, ui_id, date_submitted) values (@ship_id, @fsc, @niin, @username, 0, @inventory_detail_id, 0, @item_name, @ui_id, @date_submitted)";
            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@ship_id", (object)ship_id ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@fsc", (object)sfr.fsc ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@niin", (object)sfr.niin ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@username", (object)sfr.user ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@inventory_detail_id", (object)sfr.inventory_detail_id ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@item_name", (object)sfr.item_name ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@ui_id", (object)sfr.ui ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@date_submitted", (object)sfr.submitted_date ?? DBNull.Value);
            cmd.ExecuteNonQuery();
        }

        public string updateSFR(SFRModel sfr)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            var ship = getCurrentShip();
            var ship_id = ship.Rows[0]["ship_id"];
            if (sfr.sfr_id != null)
            {
                //just update the existing SFR, we are coming from the SFR details button
                string updateExistingSfr = @"update sfr set sfr_type = @sfr_type, date_submitted = @date_submitted, date_mailed = null, completed_flag = 0, ship_id = @ship_id,
                    username = @username, manufacturer = @manufacturer, cage = @cage, part_no = @part_no, mfg_city = @mfg_city, mfg_state = @mfg_state, mfg_address = @mfg_address, mfg_zip = @mfg_zip,
                    mfg_poc = @mfg_poc, mfg_phone = @mfg_phone, system_equipment_material = @system_equipment_material, method_of_application = @method_of_application, proposed_usage = @proposed_usage,
                    negative_impact = @negative_impact, special_training = @special_training, precautions = @precautions, properties = @properties, comments = @comments, advantages = @advantages,
                    inventory_detail_id = @inventory_detail_id, item_name = @item_name, fsc = @fsc, niin = @niin, specification_no = @specification_number, msds_attached = @msds_attached, ui_id = @ui_id
                    where sfr_id = @sfr_id";
                SqlCommand cmd = new SqlCommand(updateExistingSfr, conn);
                cmd.Parameters.AddWithValue("@sfr_id", (object)sfr.sfr_id ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@sfr_type", (object)sfr.sfr_type ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@date_submitted", (object)sfr.date_submitted ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@ship_id", (object)ship_id ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@username", (object)sfr.username ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@manufacturer", (object)sfr.manufacturer ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@cage", (object)sfr.cage ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@part_no", (object)sfr.part_no ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@mfg_city", (object)sfr.mfg_city ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@mfg_state", (object)sfr.mfg_state ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@mfg_address", (object)sfr.mfg_address ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@mfg_zip", (object)sfr.mfg_zip ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@mfg_poc", (object)sfr.mfg_poc ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@mfg_phone", (object)sfr.mfg_phone ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@system_equipment_material", (object)sfr.system_equipment_material ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@method_of_application", (object)sfr.method_of_application ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@proposed_usage", (object)sfr.proposed_usage ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@negative_impact", (object)sfr.negative_impact ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@special_training", (object)sfr.special_training ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@precautions", (object)sfr.precautions ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@properties", (object)sfr.properties ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@comments", (object)sfr.comments ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@advantages", (object)sfr.advantages ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@inventory_detail_id", (object)sfr.inventory_detail_id ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@item_name", (object)sfr.item_name ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@fsc", (object)sfr.fsc ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@niin", (object)sfr.niin ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@specification_number", (object)sfr.specification_number ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@msds_attached", (object)sfr.msds_attached ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@ui_id", (object)sfr.ui_id ?? DBNull.Value);
                cmd.Parameters.AddWithValue("@date_mailed", (object)sfr.date_mailed ?? DBNull.Value);
                cmd.ExecuteNonQuery();
                return "sfrUpdated";
            }
            else
            {
                //check that there's no other existing SFR with the same niin/cage
                string checkSFRs = "select count(sfr_id) from sfr where sfr.niin = @niin and sfr.completed_flag = 0";
                SqlCommand cmd = new SqlCommand(checkSFRs, conn);
                cmd.Parameters.AddWithValue("@niin", (object)sfr.niin ?? DBNull.Value);
                var result = cmd.ExecuteScalar();
                if ((int)result > 0)
                {
                    return "sfrPresent";
                }
                else
                {
                    //add a new SFR
                    string addSFR = @"insert into sfr values (@sfr_type, @date_submitted, @username, 0, @ship_id, @manufacturer, @cage, @part_no, @mfg_city, @mfg_address, @mfg_state, @mfg_zip, 
                                    @system_equipment_material, @method_of_application, @proposed_usage, @negative_impact, @special_training, @precautions, @properties, @comments, @advantages,
                                    @inventory_detail_id, @item_name, @fsc, @niin, @specification_no, @ui_id, @mfg_poc, @mfg_phone, @msds_attached, @date_mailed)";
                    cmd = new SqlCommand(addSFR, conn);
                    cmd.Parameters.AddWithValue("@sfr_id", (object)sfr.sfr_id ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@sfr_type", (object)sfr.sfr_type ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@date_submitted", (object)sfr.date_submitted ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@ship_id", (object)ship_id ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@username", (object)sfr.username ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@manufacturer", (object)sfr.manufacturer ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@cage", (object)sfr.cage ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@part_no", (object)sfr.part_no ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@mfg_city", (object)sfr.mfg_city ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@mfg_state", (object)sfr.mfg_state ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@mfg_address", (object)sfr.mfg_address ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@mfg_zip", (object)sfr.mfg_zip ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@mfg_poc", (object)sfr.mfg_poc ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@mfg_phone", (object)sfr.mfg_phone ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@system_equipment_material", (object)sfr.system_equipment_material ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@method_of_application", (object)sfr.method_of_application ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@proposed_usage", (object)sfr.proposed_usage ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@negative_impact", (object)sfr.negative_impact ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@special_training", (object)sfr.special_training ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@precautions", (object)sfr.precautions ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@properties", (object)sfr.properties ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@comments", (object)sfr.comments ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@advantages", (object)sfr.advantages ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@inventory_detail_id", (object)sfr.inventory_detail_id ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@item_name", (object)sfr.item_name ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@fsc", (object)sfr.fsc ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@niin", (object)sfr.niin ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@specification_no", (object)sfr.specification_number ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@msds_attached", (object)sfr.msds_attached ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@ui_id", (object)sfr.ui_id ?? DBNull.Value);
                    cmd.Parameters.AddWithValue("@date_mailed", (object)sfr.date_mailed ?? DBNull.Value);
                    cmd.ExecuteNonQuery();
                    return "sfrAdded";
                }
            }
        }

        public List<SFRRowModel> UpdateSFRStatus(SFRUpdateModel sfr)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "UPDATE sfr set date_mailed = @date_mailed where sfr_id = @id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@date_mailed", ((object)sfr.data.date_mailed) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", ((object)sfr.id) ?? DBNull.Value);
            cmd.ExecuteNonQuery();

            string getLastRow = "select fsc, niin, cage, item_name, CONVERT(varchar(10), date_submitted, 110) as date_submitted, CONVERT(varchar(10), date_mailed, 110) as date_mailed, sfr_id from sfr where sfr_id = @id";
            cmd = new SqlCommand(getLastRow, conn);
            cmd.Parameters.AddWithValue("@id", ((object)sfr.id) ?? DBNull.Value);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            List<SFRRowModel> data = new List<SFRRowModel>();
            foreach (var row in dt.Rows)
            {
                var results = new SFRRowModel()
                {
                    sfr_id = (int?)Convert.ToInt32(dt.Rows[0]["sfr_id"]),
                    date_submitted = dt.Rows[0]["date_submitted"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["date_submitted"]).ToString("MM/dd/yyyy") : null,
                    date_mailed = dt.Rows[0]["date_mailed"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["date_mailed"]).ToString("MM/dd/yyyy") : null,
                    cage = dt.Rows[0]["cage"] != DBNull.Value ? dt.Rows[0]["cage"].ToString() : null,
                    item_name = dt.Rows[0]["item_name"] != DBNull.Value ? dt.Rows[0]["item_name"].ToString() : null,
                    fsc = dt.Rows[0]["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(dt.Rows[0]["fsc"]) : null,
                    niin = dt.Rows[0]["niin"] != DBNull.Value ? dt.Rows[0]["niin"].ToString() : null
                };
                data.Add(results);
            }
            return data;
        }

        public SFRModel getSfrById(int sfr_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from sfr where sfr_id = @sfr_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@sfr_id", sfr_id);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            var results = new SFRModel()
            {
                sfr_id = (int?)Convert.ToInt32(dt.Rows[0]["sfr_id"]),
                cage = dt.Rows[0]["cage"] != DBNull.Value ? dt.Rows[0]["cage"].ToString() : null,
                sfr_type = dt.Rows[0]["sfr_type"] != DBNull.Value ? (int?)Convert.ToInt32(dt.Rows[0]["sfr_type"]) : null,
                item_name = dt.Rows[0]["item_name"] != DBNull.Value ? dt.Rows[0]["item_name"].ToString() : null,
                fsc = dt.Rows[0]["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(dt.Rows[0]["fsc"]) : null,
                niin = dt.Rows[0]["niin"] != DBNull.Value ? dt.Rows[0]["niin"].ToString() : null,
                manufacturer = dt.Rows[0]["manufacturer"] != DBNull.Value ? dt.Rows[0]["manufacturer"].ToString() : null,
                part_no = dt.Rows[0]["part_no"] != DBNull.Value ? dt.Rows[0]["part_no"].ToString() : null,
                mfg_city = dt.Rows[0]["mfg_city"] != DBNull.Value ? dt.Rows[0]["mfg_city"].ToString() : null,
                mfg_address = dt.Rows[0]["mfg_address"] != DBNull.Value ? dt.Rows[0]["mfg_address"].ToString() : null,
                mfg_state = dt.Rows[0]["mfg_state"] != DBNull.Value ? dt.Rows[0]["mfg_state"].ToString() : null,
                mfg_zip = dt.Rows[0]["mfg_zip"] != DBNull.Value ? dt.Rows[0]["mfg_zip"].ToString() : null,
                system_equipment_material = dt.Rows[0]["system_equipment_material"] != DBNull.Value ? dt.Rows[0]["system_equipment_material"].ToString() : null,
                method_of_application = dt.Rows[0]["method_of_application"] != DBNull.Value ? dt.Rows[0]["method_of_application"].ToString() : null,
                proposed_usage = dt.Rows[0]["proposed_usage"] != DBNull.Value ? dt.Rows[0]["proposed_usage"].ToString() : null,
                negative_impact = dt.Rows[0]["negative_impact"] != DBNull.Value ? dt.Rows[0]["negative_impact"].ToString() : null,
                special_training = dt.Rows[0]["special_training"] != DBNull.Value ? dt.Rows[0]["special_training"].ToString() : null,
                precautions = dt.Rows[0]["precautions"] != DBNull.Value ? dt.Rows[0]["precautions"].ToString() : null,
                properties = dt.Rows[0]["properties"] != DBNull.Value ? dt.Rows[0]["properties"].ToString() : null,
                comments = dt.Rows[0]["comments"] != DBNull.Value ? dt.Rows[0]["comments"].ToString() : null,
                advantages = dt.Rows[0]["advantages"] != DBNull.Value ? dt.Rows[0]["advantages"].ToString() : null,
                specification_number = dt.Rows[0]["specification_no"] != DBNull.Value ? dt.Rows[0]["specification_no"].ToString() : null,
                ui_id = dt.Rows[0]["ui_id"] != DBNull.Value ? (int?)Convert.ToInt32(dt.Rows[0]["ui_id"]) : null,
                mfg_phone = dt.Rows[0]["mfg_phone"] != DBNull.Value ? dt.Rows[0]["mfg_phone"].ToString() : null,
                mfg_poc = dt.Rows[0]["mfg_poc"] != DBNull.Value ? dt.Rows[0]["mfg_poc"].ToString() : null,
                msds_attached = dt.Rows[0]["msds_attached"] != DBNull.Value ? (bool)dt.Rows[0]["msds_attached"] : false,
            };
            return results;
        }

        public DataTable getFilteredSMCLCollection(string niin, string itemName, string specs, string tableName, string manufacturer, string acmCategory)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc, nc.niin, nc.ui, nc.um, "
            + "nc.usage_category_id, u.description as usage_category, u.category as category,"
            + " nc.description, nc.smcc_id, sm.smcc as smcc,"
            + " nc.nehc_rpt as nehc_rpt, nc.part_name as part_name, "
            + " nc.specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, "
            + "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, "
            + "nc.remarks, nc.storage_type_id, st.type as storage_type, "
            + "nc.spmig, nc.nehc_rpt, nc.catalog_group_id, "
            + "nc.catalog_serial_number, nc.allowance_qty, nc.created, nc.ship_id, nc.catalog_group_id, "
            + " nc.cage, nc.manufacturer, nc.msds_num "
            + "from NIIN_Catalog nc "
            + "LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id) "
            + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
            + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
            + "LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
            + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) "
            + "LEFT JOIN catalog_groups cg ON (cg.catalog_group_id=nc.catalog_group_id) ";
            
            string whereStmt = "WHERE 1=1";
            bool addNiin = false;
            bool addItemName = false;
            bool addSpecs = false;
            bool addTableName = false;
            bool addManufacturer = false;
            bool addACM = false;

            if (niin != null && niin != "")
            {
                whereStmt += " and nc.niin = @niin";
                addNiin = true;
            }

            if (itemName != null && itemName != "")
            {
                addItemName = true;
                whereStmt += " and nc.description = @itemName";
            }
            if (specs != null && specs != "")
            {
                addSpecs = true;
                whereStmt += " and nc.specs = @specs";

            }
            if (tableName != null && tableName != "")
            {
                addTableName = true;
                whereStmt += " and nc.catalog_group_id = @tableName";
            }
            if (manufacturer != null && manufacturer != "")
            {
                addManufacturer = true;
                whereStmt += " and nc.manufacturer = @manufacturer";
            }
            if (acmCategory != null && acmCategory != "")
            {
                addACM = true;
                whereStmt += " and nc.usage_category_id = @acmCategory";
            }
            selectStmt += whereStmt;
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addNiin)
                cmd.Parameters.Add("@niin", niin);
            if (addItemName)
                cmd.Parameters.Add("@itemName", itemName);
            if (addSpecs)
                cmd.Parameters.Add("@specs", specs);
            if (addTableName)
                cmd.Parameters.Add("@tableName", tableName);
            if (addManufacturer)
                cmd.Parameters.Add("@manufacturer", manufacturer);
            if (addACM)
                cmd.Parameters.Add("@acmCategory", acmCategory);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getStorageTypes()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select type, storage_type_id, description from storage_type";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public List<ListItem> getStorageTypeList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select type, storage_type_id, description from storage_type";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getSmccList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select smcc, smcc_id, description from smcc";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }

        public DataTable getSmccs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select smcc, smcc_id, description from smcc";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public List<ListItem> getSlacList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select slac, shelf_life_action_code_id, description from shelf_life_action_code";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }

        public DataTable getSlacs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select slac, shelf_life_action_code_id, description from shelf_life_action_code";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }


        public DataTable getAllHccs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select hcc, hcc_id, description from hcc";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable getAllUis()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select ui_id, abbreviation, description from ui";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }


        public List<ListItem> getSlcList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select slc, shelf_life_code_id, description from shelf_life_code";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }

        public DataTable getSlcs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select slc, shelf_life_code_id, description from shelf_life_code";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable GetUICs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select uic from ships";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public string GetCurrentUIC()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select uic from ships where current_ship = 1";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            var uic = cmd.ExecuteScalar();
            conn.Close();
            if (uic == null)
                return "NOT SET";
            else
                return uic.ToString();
        }

        public void AddShipLogo(string filename)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "update ships set ship_logo_filename = @filename where current_ship = 1";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue(@"filename", filename);
            var uic = cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void AddShipImage(string filename)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "update ships set ship_picture_filename = @filename where current_ship = 1";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@filename", filename);
            var uic = cmd.ExecuteNonQuery();
            conn.Close();
        }

        public DataTable GetCurrentShipData(string UIC)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select * from ships where uic = @uic";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@uic", UIC);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public int UpdateCurrentShipData(ShipAdministrationModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "update ships set current_ship = 0; update ships set address=@address, base=@base, POC_Email = @poc_email, POC_Name = @poc_name, POC_Title = @poc_title, POC_Telephone = @poc_telephone, act_code = @act, " +
                "city = @city, current_ship = 1, hull_number = @hull_number, hull_type= @subtype, sndl = @sndl, state = @state, zip = @zip where uic = @uic";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@address", (object)model.Address ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@base", (object)model.BASE ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@poc_email", (object)model.POCEmail ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@poc_name", (object)model.POCName ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@poc_title", (object)model.POCTitle ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@poc_telephone", (object)model.POCPhone ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@act", (object)model.ACTCode ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@city", (object)model.City ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@hull_number", (object)model.HullNumber ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@subtype", (object)model.SubType ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@sndl", (object)model.SNDL ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@state", (object)model.State ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@zip", (object)model.Zip ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@uic", (object)model.UIC ?? DBNull.Value);

            var result = cmd.ExecuteNonQuery();
            conn.Close();
            return result;
        }


        public List<ListItem> getCogList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select cog, cog_id, description from cog_codes";
            return getListItemList(selectStmt, includeDescriptionInDropdown, "cog");

        }

        public DataTable getCogs()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select cog, cog_id, description from cog_codes";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public List<ListItem> getUsageCategoryList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select category, usage_category_id, description from usage_category";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }

        public DataTable getUsageCategories()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select category, usage_category_id, description from usage_category";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }


        public DataTable getLocations()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select distinct location_id, name from locations";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable getLocationsForUser(string user)
        {
            //all locations in their workcenter
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt = "select l.location_id, l.name from user_roles v LEFT OUTER JOIN workcenter w on w.workcenter_id = v.workcenter_id INNER JOIN locations l on l.workcenter_id = w.workcenter_id " +
                "where v.username = @username";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@username", user);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable getWorkcenterForUser(string user)
        {
            //all locations in their workcenter
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt = "select v.workcenter_id, w.wid from user_roles v LEFT OUTER JOIN workcenter w on w.workcenter_id = v.workcenter_id " +
                "where v.username = @username";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@username", user);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable getWorkCenters()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select distinct workcenter_id, wid from workcenter";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public List<ListItem> getAllSPMIG()
        {
            string selectStmt = "select distinct spmig from niin_catalog";
            return getPlainList(selectStmt);

        }


        public List<ListItem> getInvSPMIG()
        {
            string selectStmt = "select spmig from inventory_detail where spmig is not null and spmig <> '' order by spmig";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getAllNIINs()
        {
            string selectStmt = "select niin from niin_catalog";
            return getPlainList(selectStmt);

        }

        public List<ListItem> getInvNIINs()
        {
            string selectStmt = "select niin from inventory_detail where niin is not null and niin <> '' order by niin";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getAllItemDescriptions()
        {
            string selectStmt = "select distinct description from niin_catalog where description is not null order by description";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getInvItemDescriptions()
        {
            string selectStmt = "select item_name from inventory_detail where item_name is not null and item_name <> '' order by item_name";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getAllManufacturers()
        {
            string selectStmt = "select distinct manufacturer from niin_catalog where manufacturer is not null order by manufacturer";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getFilteredCages(string niin)
        {
            string selectStmt = @"SELECT distinct cage FROM mfg_catalog where mfg_catalog.niin = @niin and cage is not null
                                    UNION
                                  SELECT distinct cage FROM inventory_onhand oh inner join inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id where i.niin = @niin and cage is not null; ";
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@niin", niin);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                list.Add(new ListItem(reader.GetValue(0).ToString()));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return list;

        }

        public List<ListItem> getFilteredManufacturers(string niin)
        {
            string selectStmt = @"SELECT distinct manufacturer FROM mfg_catalog where mfg_catalog.niin = @niin and manufacturer is not null
                                    UNION
                                  SELECT distinct manufacturer FROM inventory_onhand oh inner join inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id where i.niin = @niin and manufacturer is not null; ";
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            cmdRead.Parameters.AddWithValue("@niin", niin);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                list.Add(new ListItem(reader.GetValue(0).ToString()));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return list;

        }


        public DataTable getAllInventoryGroups()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select catalog_group_id, group_name from catalog_groups";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public List<ListItem> getAllSpecs()
        {
            string selectStmt = "select distinct specs from niin_catalog where specs is not null order by specs";
            return getPlainList(selectStmt);
        }


        public List<ListItem> getInvSpecs()
        {
            string selectStmt = "select specs from inventory_detail where specs is not null and specs <> '' order by specs";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getUsageCategoryListU(bool includeDescriptionInDropdown)
        {
            //string selectStmt = "Select category, usage_category_id, description from usage_category";
            string selectStmt = "Select category, usage_category_id, description from usage_category where category = 'U'";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getLocationWorkcenterList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select l.name,  l.location_id,  w.description from locations l, workcenter w WHERE l.workcenter_id=w.workcenter_id order by w.description";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getHccList(bool includeDescriptionInDropdown)
        {
            string selectStmt = "Select hcc, hcc_id, description from hcc order by hcc";
            return getListItemList(selectStmt, includeDescriptionInDropdown);

        }
        public List<ListItem> getCatalogGroupList()
        {
            string selectStmt = "Select group_name, catalog_group_id from catalog_groups ORDER BY group_name";
            return getListItemList(selectStmt, false);

        }

        public DataTable getCatalogGroups()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "Select group_name, catalog_group_id from catalog_groups ORDER BY group_name";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }


        public List<ListItem> getWorkcenterList(bool includeDescription)
        {
            string selectStmt = "Select wid, workcenter_id from workcenter";
            return getListItemList(selectStmt, includeDescription);

        }

        //For DropDownLists Items, etc.
        public List<ListItem> getListItemList(String selectStmt, bool includeDescription) {
            return getListItemList(selectStmt, includeDescription, null);
        }
        public List<ListItem> getListItemList(String selectStmt, bool includeDescription,
            string sortField)
        {
            // See if we want to sort the list
            if (sortField != null && sortField.Length > 0)
                selectStmt += " ORDER BY " + sortField;

            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

               SqlDataReader reader = cmdRead.ExecuteReader();
               while (reader.Read())
               {              
                   //ListItem(Label (code) + (description), Value (item id))
                   if(!includeDescription)
                   list.Add(new ListItem(""+reader.GetValue(0), ""+reader.GetValue(1)));
                   else
                       list.Add(new ListItem("" + reader.GetValue(0) + "   (" + reader.GetValue(2)+")", "" + reader.GetValue(1)));
               }

              reader.Dispose();
              reader.Close();

            conn.Close();

            return list;

        }

        public List<ListItem> getPlainList(String selectStmt)
        {
            List<ListItem> list = new List<ListItem>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                list.Add(new ListItem(reader.GetValue(0).ToString()));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return list;

        }

        public List<UserModel> GetUserList()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select * from user_roles left join workcenter on workcenter.workcenter_id = user_roles.workcenter_id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            List<UserModel> results = new List<UserModel>();
            foreach (DataRow dr in dt.Rows)
            {
                UserModel user = new UserModel()
                {
                    user_id = Convert.ToInt32(dr["user_role_id"]),
                    username = dr["username"] != DBNull.Value ? dr["username"].ToString() : null,
                    role = dr["role"] != DBNull.Value ? dr["role"].ToString() : null,
                    workcenter = new Workcenter()
                    {
                        workcenter_id = dr["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["workcenter_id"]) : null,
                        workcenter_name = dr["wid"] != DBNull.Value ? dr["wid"].ToString() : null
                    }
                };
                results.Add(user);
            }
            conn.Close();

            return results;
        }

        public List<LocationModel> GetLocationList()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select location_id, name as location_name, locations.workcenter_id, wid from locations inner join workcenter on workcenter.workcenter_id = locations.workcenter_id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            List<LocationModel> results = new List<LocationModel>();
            foreach (DataRow dr in dt.Rows)
            {
                LocationModel location = new LocationModel()
                {
                    location_id = Convert.ToInt32(dr["location_id"]),
                    location_name = dr["location_name"] != DBNull.Value ? dr["location_name"].ToString() : null,
                    workcenter = new Workcenter()
                    {
                        workcenter_id = dr["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["workcenter_id"]) : null,
                        workcenter_name = dr["wid"] != DBNull.Value ? dr["wid"].ToString() : null
                    }
                };
                results.Add(location);
            }
            conn.Close();

            return results;
        }

        public LocationModel UpdateLocation(LocationUpdateModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"update locations set name = @name, workcenter_id = @workcenter_id where location_id = @id; select * from locations 
                left join workcenter on workcenter.workcenter_id = locations.workcenter_id where location_id = @id;";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@name", (object)model.data.location_name ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@workcenter_id", (object)model.data.workcenter.workcenter_id ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", (object)model.id);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            var location = dt.Rows[0];
            LocationModel returnRow = new LocationModel()
            {
                location_id = Convert.ToInt32(location["location_id"]),
                location_name = location["name"] != DBNull.Value ? location["name"].ToString() : null,
                workcenter = new Workcenter()
                {
                    workcenter_id = location["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(location["workcenter_id"]) : null,
                    workcenter_name = location["wid"] != DBNull.Value ? location["wid"].ToString() : null
                }
            };
            return returnRow;
        }

        public LocationModel AddLocation(LocationUpdateModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "insert into locations values (@name, @workcenter_id); select * from locations left join workcenter on workcenter.workcenter_id = locations.workcenter_id where location_id = SCOPE_IDENTITY()";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@name", (object)model.data.location_name ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@workcenter_id", (object)model.data.workcenter.workcenter_id ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", (object)model.id);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            var location = dt.Rows[0];
            LocationModel returnRow = new LocationModel()
            {
                location_id = Convert.ToInt32(location["location_id"]),
                location_name = location["name"] != DBNull.Value ? location["name"].ToString() : null,
                workcenter = new Workcenter()
                {
                    workcenter_id = location["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(location["workcenter_id"]) : null,
                    workcenter_name = location["wid"] != DBNull.Value ? location["wid"].ToString() : null
                }
            };
            return returnRow;
        }


        public UserModel UpdateUser(UserUpdateModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"update user_roles set username = @username, role = @role, workcenter_id = @workcenter_id where user_role_id = @id; select * from user_roles 
                left join workcenter on workcenter.workcenter_id = user_roles.workcenter_id where user_role_id = @id;";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@username", (object)model.data.username ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@role", (object)model.data.role ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@workcenter_id", (object)model.data.workcenter.workcenter_id ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", (object)model.id);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            var user = dt.Rows[0];
            UserModel returnRow = new UserModel()
            {
                user_id = Convert.ToInt32(user["user_role_id"]),
                username = user["username"] != DBNull.Value ? user["username"].ToString() : null,
                role = user["role"] != DBNull.Value ? user["role"].ToString() : null,
                workcenter = new Workcenter()
                {
                    workcenter_id = user["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(user["workcenter_id"]) : null,
                    workcenter_name = user["wid"] != DBNull.Value ? user["wid"].ToString() : null
                }
            };
            return returnRow;
        }


        public UserModel AddUser(UserUpdateModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "insert into user_roles values (@username, @role, @workcenter_id); select * from user_roles left join workcenter on workcenter.workcenter_id = user_roles.workcenter_id where user_role_id = SCOPE_IDENTITY()";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@username", (object)model.data.username ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@role", (object)model.data.role ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@workcenter_id", (object)model.data.workcenter.workcenter_id ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", (object)model.id);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            var user = dt.Rows[0];
            UserModel returnRow = new UserModel()
            {
                user_id = Convert.ToInt32(user["user_role_id"]),
                username = user["username"] != DBNull.Value ? user["username"].ToString() : null,
                role = user["role"] != DBNull.Value ? user["role"].ToString() : null,
                workcenter = new Workcenter()
                {
                    workcenter_id = user["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(user["workcenter_id"]) : null,
                    workcenter_name = user["wid"] != DBNull.Value ? user["wid"].ToString() : null
                }
            };
            return returnRow;
        }

        public void DeleteUser(int user_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "delete from user_roles where user_role_id = @id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", user_id);
            cmd.ExecuteNonQuery();
        }

        public void DeleteLocation(int location_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "delete from locations where location_id = @id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", location_id);
            cmd.ExecuteNonQuery();
        }

        public DataTable getMSDSforSMCL(string NIIN, bool filter)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);



            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, " +
                "PRODUCT_IDENTITY, PARTNO, FSC, NIIN, hcc_id, file_name " +
                "from msds WHERE NIIN=@NIIN ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@NIIN", dbNull(NIIN));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            if (filter)
            {
                // Go through table and remove duplicate records
                // key is composed of NIIN + MSDSSERNO + CAGE
                String previousKey = "";
                String currentKey = "";
                List<DataRow> rowsToRemove = new List<DataRow>();
                foreach (DataRow row in dt.Rows)
                {
                    currentKey = row.ItemArray[7].ToString() + row.ItemArray[1].ToString()
                        + row.ItemArray[2].ToString();
                    if (currentKey.ToUpper().Equals(previousKey.ToUpper()))
                    {
                        rowsToRemove.Add(row);
                    }
                    else
                    {
                        previousKey = currentKey;
                    }
                }

                // Remove the duplicate rows
                foreach (var dr in rowsToRemove)
                {
                    dt.Rows.Remove(dr);
                }

            }

            return dt;

        }

        public DataTable getMSDSforNiinCageContractSerno(string NIIN, string CAGE, string CT_NUMBER, string MSDSSERNO, bool filter)
        {
            string where = " WHERE NIIN=@NIIN ";

            if (MSDSSERNO != null && !MSDSSERNO.Equals(""))
                where += " AND MSDSSERNO LIKE @MSDSSERNO ";
            else
            {
                if (CAGE != null && !CAGE.Equals(""))
                    where += " AND CAGE LIKE @CAGE ";

                if (CT_NUMBER != null && !CT_NUMBER.Equals(""))
                    where += " AND CT_NUMBER LIKE @CT_NUMBER ";

            }

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, CT_NUMBER, PRODUCT_IDENTITY, PARTNO, FSC, NIIN, hcc_id, file_name"
                +" from v_msds_contractor " + where + " ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@NIIN", dbNull(NIIN));

            if (MSDSSERNO != null && !MSDSSERNO.Equals(""))
                cmd.Parameters.AddWithValue("@MSDSSERNO", dbNull(MSDSSERNO + "%"));
            else
            {
                if (CAGE != null && !CAGE.Equals(""))
                    cmd.Parameters.AddWithValue("@CAGE", dbNull(CAGE + "%"));

                if (CT_NUMBER != null && !CT_NUMBER.Equals(""))
                    cmd.Parameters.AddWithValue("@CT_NUMBER", dbNull(CT_NUMBER + "%"));

            }

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            if (filter) {

            // Go through table and remove duplicate records
            // key is composed of NIIN + MSDSSERNO + CAGE + CT_NUMBER
            String previousKey = "";
            String currentKey = "";
            List<DataRow> rowsToRemove = new List<DataRow>();
            foreach (DataRow row in dt.Rows) {
                currentKey = row.ItemArray[8].ToString() + row.ItemArray[1].ToString()
                    + row.ItemArray[2].ToString() + row.ItemArray[4].ToString();
                if (currentKey.ToUpper().Equals(previousKey.ToUpper())) {
                    rowsToRemove.Add(row);
                }
                else {
                    previousKey = currentKey;
                }
            }

            // Remove the duplicate rows
            foreach (var dr in rowsToRemove) {
                dt.Rows.Remove(dr);
            }
            }

            return dt;

        }


        public DataTable getMSDSforSerialNumber(string msdsserno, bool filter)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, " +
                "PRODUCT_IDENTITY, PARTNO, FSC, NIIN, hcc_id, file_name " +
                "from msds WHERE MSDSSERNO=@MSDSSERNO ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@MSDSSERNO", dbNull(msdsserno));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            if (filter) {
                // Go through table and remove duplicate records
                // key is composed of NIIN + MSDSSERNO + CAGE
                String previousKey = "";
                String currentKey = "";
                List<DataRow> rowsToRemove = new List<DataRow>();
                foreach (DataRow row in dt.Rows) {
                    currentKey = row.ItemArray[7].ToString() + row.ItemArray[1].ToString()
                        + row.ItemArray[2].ToString();
                    if (currentKey.ToUpper().Equals(previousKey.ToUpper())) {
                        rowsToRemove.Add(row);
                    }
                    else {
                        previousKey = currentKey;
                    }
                }

                // Remove the duplicate rows
                foreach (var dr in rowsToRemove) {
                    dt.Rows.Remove(dr);
                }

            }

            conn.Close();

            return dt;

        }

        public DataTable getMSDSList(String whereColumn, String filterValue, 
                String usageCategory, String catalogGroup, String manuallyEnteredOnly) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "SELECT * FROM vMSDSList vm WHERE vm.msds_id in " +
                    "(SELECT MAX(msds_id) FROM msds WHERE MSDSSERNO = vm.MSDSSERNO AND CAGE = vm.CAGE) "; 

            bool addFilter = false;
            bool addUsageCategory = false;
            bool addCatalogGroup = false;

            string filterStmt = "";
            if (!filterValue.Equals("")) {
                if (whereColumn.Equals("SPECS")) {
                    filterStmt = " AND vm.niin IN (SELECT niin FROM niin_catalog " +
                        "WHERE specs LIKE '%' + @filter + '%') ";
                }
                else {

                    filterStmt = " AND  vm." + whereColumn + " LIKE '%' + @filter + '%' ";
                }
                addFilter = true;
            }
            string usageStmt = "";
            if (!usageCategory.Equals("")) {

                usageStmt = "AND vm.niin in (SELECT niin FROM niin_catalog " +
                        "WHERE  usage_category_id = @usageCategory) ";

                addUsageCategory = true;
            }
            string groupStmt = "";
            if (!catalogGroup.Equals("")) {

                groupStmt = "AND vm.niin in (SELECT niin FROM niin_catalog " +
                        "WHERE catalog_group_id = @catalogGroup) ";

                addCatalogGroup = true;
            }
            string manuallyEnteredOnlyStmt = "";
            if (!manuallyEnteredOnly.Equals("")) {

                usageStmt = "AND vm.manually_entered = 1 ";
            }

            selectStmt += filterStmt + usageStmt + groupStmt + manuallyEnteredOnlyStmt;

            selectStmt += "ORDER BY msdsserno, CAGE, msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            if (addFilter)
                cmd.Parameters.Add("@filter", SqlDbType.NVarChar).Value = filterValue;
            if (addUsageCategory)
                cmd.Parameters.Add("@usageCategory", SqlDbType.Int).Value = Int32.Parse(usageCategory);
            if (addCatalogGroup)
                cmd.Parameters.Add("@catalogGroup", SqlDbType.Int).Value = Int32.Parse(catalogGroup);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }


        public DataTable doesMSDSExistforMSDSERNO(string msdsserno)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string selectStmt = "select manually_entered from msds WHERE MSDSSERNO=@msdsserno";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msdsserno", dbNull(msdsserno));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public string getSlcByNiinId(int niinCatId)
        {

            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select s.slc from shelf_life_code s where s.shelf_life_code_id = (select n.shelf_life_code_id from niin_catalog n where n.niin_catalog_id = @niinCatId)";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@niinCatId", niinCatId);

            string slc = null;

            try
            {
                slc = Convert.ToString(cmd.ExecuteScalar());
            }
            catch
            {}

            con.Close();

            return slc;
        }

        public DataTable getAllInventory()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                   @"SELECT 
                                         i.fsc              AS FSC, 
                                        i.niin              AS NIIN,
                                   i.item_name              AS ITEM_NAME,
								m.manufacturer              AS MFG,
                                        m.cage              AS CAGE,
									 smcc.smcc				AS SMCC, 
                                    u.category              AS ACM_CAT,
									   hcc.hcc				AS HCC,
 CONVERT(varchar(10), oh.expiration_date, 110)              AS SHELF_LIFE_EXP_DATE,
                                        oh.qty              AS ON_HAND_QTY,
                                        l.name              AS LOCATION,
                                 w.description              AS WCENTER,
                          i.allowance_quantity              AS ALLOWANCE_QUANTITY, 
                             unit.abbreviation              AS UNIT_OF_ISSUE,
                                          i.um              AS UNIT_OF_MEASURE,
			  CONCAT(n.remarks,' - ', i.notes)				AS REMARKS,
									   slc.slc				AS SHELF_LIFE_CODE,
									 slac.slac				AS SHELF_LIFE_ACT_CODE,
 CONVERT(varchar(10), oh.inventoried_date, 110)			    AS INV_DATE,
     CONVERT(varchar(10), oh.onboard_date, 110)				AS ONBOARD_DATE

                    from inventory_onhand oh left join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id 
                                             left join mfg_catalog m on m.mfg_catalog_id = oh.mfg_catalog_id
                                             left join locations l on l.location_id = oh.location_id
                                             left join workcenter w on w.workcenter_id = l.workcenter_id
                                             left join usage_category u on u.usage_category_id = i.usage_category_id
                                             left join ui unit on i.ui_id = unit.ui_id 
											 left join smcc smcc on i.smcc_id = smcc.smcc_id
											 left join shelf_life_code slc on i.slc_id = slc.shelf_life_code_id
											 left join shelf_life_action_code slac on i.slac_id = slac.shelf_life_action_code_id
											 left join hcc hcc	on i.hcc_id = hcc.hcc_id
											 left join niin_catalog n on i.niin = n.niin ";

            string whereStmt = "where oh.inventory_detail_id is not null";

            selectStmt += whereStmt;
            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }



        public DataTable getFilteredInventoryCollection(string niin, string itemName, string location, string workcenter, string spmig, string specs, string inventoryId) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                   @"SELECT 
                     oh.inventory_detail_id, i.fsc, i.niin, u.category, i.item_name as item_name, i.spmig, CONVERT(varchar(10), oh.expiration_date, 110) as expiration_date, w.wid as workcenter,
                        l.name as location, l.workcenter_id as workcenter_id, oh.qty, i.allowance_quantity, oh.cage, unit.abbreviation as ui, i.um, oh.manufacturer, oh.inventory_onhand_id as onhand_id,
                        (select count(niin) from niin_catalog where niin = i.niin) as is_smcl
                    from inventory_onhand oh left join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id 
                                             left join locations l on l.location_id = oh.location_id
                                             left join workcenter w on w.workcenter_id = l.workcenter_id
                                             left join usage_category u on u.usage_category_id = i.usage_category_id
                                             left join ui unit on i.ui_id = unit.ui_id 
                                             left join niin_catalog on i.niin = niin_catalog.niin ";

            string whereStmt = "where oh.inventory_detail_id is not null";

            bool addNiin = false;
            bool addItemName = false;
            bool addSpecs = false;
            bool addLocation = false;
            bool addWorkCenter = false;
            bool addSPMIG = false;
            bool addId = false;

            if (niin != null && niin != "")
            {
                whereStmt += " and i.niin = @niin";
                addNiin = true;
            }

            if (itemName != null && itemName != "")
            {
                addItemName = true;
                whereStmt += " and i.item_name = @itemName";
            }
            if (specs != null && specs != "")
            {
                addSpecs = true;
                whereStmt += " and i.specs = @specs";

            }
            if (location != null && location != "")
            {
                addLocation = true;
                whereStmt += " and oh.location_id = @location";
            }
            if (workcenter != null && workcenter != "")
            {
                addWorkCenter = true;
                whereStmt += " and l.workcenter_id = @workcenter";
            }
            if (spmig != null && spmig != "")
            {
                addSPMIG = true;
                whereStmt += " and i.spmig = @spmig";
            }
            if (inventoryId != null && inventoryId != "")
            {
                addId = true;
                whereStmt += " and i.inventory_detail_id = @inventory_id";
            }
            selectStmt += whereStmt;
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addNiin)
                cmd.Parameters.AddWithValue("@niin", niin);
            if (addItemName)
                cmd.Parameters.AddWithValue("@itemName", itemName);
            if (addSpecs)
                cmd.Parameters.AddWithValue("@specs", specs);
            if (addWorkCenter)
                cmd.Parameters.AddWithValue("@workcenter", workcenter);
            if (addLocation)
                cmd.Parameters.AddWithValue("@location", location);
            if (addSPMIG)
                cmd.Parameters.AddWithValue("@spmig", spmig);
            if (addId)
                cmd.Parameters.AddWithValue("@inventory_id", inventoryId);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getSMCLByNiin(string niin)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select nc.niin_catalog_id, nc.fsc as fsc, nc.niin, nc.ui as ui, nc.um as um,  nc.usage_category_id, u.description as usage_category, u.category as category," +
            " nc.description as description, nc.smcc_id, sm.smcc as smcc, nc.NMCPHC as NMCPHC, nc.msds_num as msds_num, "
            + " nc.specs as specs, nc.shelf_life_code_id, shc.slc as shelf_life_code, " +
            "nc.shelf_life_action_code_id, shac.slac as shelf_life_action_code, " +
            "nc.remarks as remarks, nc.storage_type_id, st.type as storage_type, " +
            " nc.spmig as spmig,   " +
            "nc.cage as cage, nc.manufacturer as manufacturer"+

            " from NIIN_Catalog nc LEFT JOIN usage_category u ON (u.usage_category_id=nc.usage_category_id)"
            + " LEFT JOIN shelf_life_code shc ON (shc.shelf_life_code_id=nc.shelf_life_code_id) "
            + " LEFT JOIN shelf_life_action_code shac ON (shac.shelf_life_action_code_id=nc.shelf_life_action_code_id) "
            + " LEFT JOIN storage_type st ON (st.storage_type_id=nc.storage_type_id)"
            + " LEFT JOIN smcc sm ON (sm.smcc_id=nc.smcc_id) " +

            " WHERE nc.niin = @niin";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@niin", niin);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getInventoryDetailsForPrint(int inventory_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"select i.fsc, i.niin, i.item_name, i.um, i.spmig, i.specs, slac.slac, slac.description as slac_desc, slc.slc, slc.description as slc_desc,
                                    hcc.hcc, hcc.description as hcc_desc, smcc.smcc, smcc.description as smcc_desc, ui.abbreviation as ui_abbrev, ui.description as ui_desc,
                                    uc.category as usage_category, uc.description as usage_category_desc, i.notes, i.allowance_quantity
                                    from inventory_onhand oh
                                    left join inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id
                                    left join shelf_life_action_code slac on i.slac_id = slac.shelf_life_action_code_id
                                    left join shelf_life_code slc on i.slc_id = slc.shelf_life_code_id
                                    left join hcc on hcc.hcc_id = i.hcc_id
                                    left join smcc on smcc.smcc_id = i.smcc_id
                                    left join ui on ui.ui_id = i.ui_id
                                    left join usage_category uc on uc.usage_category_id = i.usage_category_id
                                    where oh.inventory_onhand_id = @inventory_id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public AddInventoryModel getInventoryDetailsForModal(int inventory_onhand_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            //check SMCL information first, if something is in the SMCL it should overwrite whatever is in the inventory table
            //TODO: Get the HCC from the MSDS information?
            string selectStmt = "select i.inventory_detail_id, nc.fsc, nc.niin, nc.ui, nc.description, nc.remarks, nc.allowance_qty, nc.um, nc.spmig, nc.specs, nc.usage_category_id, nc.shelf_life_code_id, nc.shelf_life_action_code_id, nc.smcc_id from inventory_onhand oh " +
               " left join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id left join niin_catalog nc on nc.niin = i.niin where oh.inventory_onhand_id = @onhand_id"; 
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@onhand_id", inventory_onhand_id);
            AddInventoryModel model = new AddInventoryModel();
            DataTable smclData = new DataTable();
            new SqlDataAdapter(cmd).Fill(smclData);
            if (smclData.Rows.Count != 0)
            {
                var currentRow = smclData.Rows[0];
                model.fsc = currentRow["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["fsc"]) : null;
                model.inventory_detail_id = currentRow["inventory_detail_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["inventory_detail_id"]) : null;
                model.niin = currentRow["niin"] != DBNull.Value ? currentRow["niin"].ToString() : null;
                model.name = currentRow["description"] != DBNull.Value ? currentRow["description"].ToString() : null;
                model.ui = currentRow["ui"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["ui"]) : null;
                model.um = currentRow["um"] != DBNull.Value ? currentRow["um"].ToString() : null;
                model.spmig = currentRow["spmig"] != DBNull.Value ? currentRow["spmig"].ToString() : null;
                model.specs = currentRow["specs"] != DBNull.Value ? currentRow["specs"].ToString() : null;
                model.remarks = currentRow["remarks"] != DBNull.Value ? currentRow["remarks"].ToString() : null;
                model.usage_category_id = currentRow["usage_category_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["usage_category_id"]) : null;
                model.slc_id = currentRow["shelf_life_code_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["shelf_life_code_id"]) : null;
                model.slac_id = currentRow["shelf_life_action_code_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["shelf_life_action_code_id"]) : null;
                model.hcc_id = null;
                model.smcc_id = currentRow["smcc_id"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["smcc_id"]) : null;
                model.allowance_qty = currentRow["allowance_qty"] != DBNull.Value ? (int?)Convert.ToInt32(currentRow["allowance_qty"]) : 0;
                model.notes = null;
            }
            //we have the SMCL information handy, now we need to check the inventory table to see if any missing information has been filled in by a user but is not yet
            //incorporated into the SMCL. 
            if (model.inventory_detail_id != null)
            {
                string selectInv = "select * from inventory_detail where inventory_detail_id = @inventory_detail_id";
                cmd = new SqlCommand(selectInv, conn);
                cmd.Parameters.AddWithValue("@inventory_detail_id", model.inventory_detail_id);
                DataTable invData = new DataTable();
                new SqlDataAdapter(cmd).Fill(invData);
                if (invData.Rows.Count != 0)
                {
                    var currentRow = invData.Rows[0];
                    model.fsc = (model.fsc == null && currentRow["fsc"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["fsc"]) : model.fsc;
                    model.niin = (model.niin == null && currentRow["niin"] != DBNull.Value) ? currentRow["niin"].ToString() : model.niin;
                    model.name = (model.name == null && currentRow["item_name"] != DBNull.Value) ? currentRow["item_name"].ToString() : model.name;
                    model.ui = (model.ui == null && currentRow["ui_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["ui_id"]) : model.ui;
                    model.um = (model.um == null && currentRow["um"] != DBNull.Value) ? currentRow["um"].ToString() : model.um;
                    model.spmig = (model.spmig == null && currentRow["spmig"] != DBNull.Value) ? currentRow["spmig"].ToString() : model.spmig;
                    model.specs = (model.specs == null && currentRow["specs"] != DBNull.Value) ? currentRow["specs"].ToString() : model.specs;
                    model.usage_category_id = (model.usage_category_id == null && currentRow["usage_category_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["usage_category_id"]) : model.usage_category_id;
                    model.slc_id = (model.slc_id == null && currentRow["slc_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["slc_id"]) : model.slc_id;
                    model.slac_id = (model.slac_id == null && currentRow["slac_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["slac_id"]) : model.slac_id;
                    model.hcc_id = (model.hcc_id == null && currentRow["hcc_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["hcc_id"]) : model.hcc_id;
                    model.smcc_id = (model.smcc_id == null && currentRow["smcc_id"] != DBNull.Value) ? (int?)Convert.ToInt32(currentRow["smcc_id"]) : model.smcc_id;
                    model.notes = currentRow["notes"] != DBNull.Value ? currentRow["notes"].ToString() : null;
                }
            }
            conn.Close();
            return model;
        }

        public List<KeyValuePair<string, byte[]>> getManuallyEnteredMSDSfiles()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            List<KeyValuePair<string, byte[]>> uploadedFiles = new List<KeyValuePair<string, byte[]>>();

            string selectStmt = "select file_name, msds_file"
                + " from msds WHERE manually_entered=1 AND file_name IS NOT NULL";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmd.ExecuteReader();
            int i = 0;//for unique file names
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "msds_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select msds_translated_filename, MSDS_TRANSLATED "
               + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND msds_translated_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "translated_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select neshap_comp_filename, NESHAP_COMP_CERT "
               + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND neshap_comp_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "neshap_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select other_docs_filename, OTHER_DOCS "
              + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND other_docs_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "other_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select product_sheet_filename, PRODUCT_SHEET "
             + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND product_sheet_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                string key = i + "prod_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select transportation_cert_filename, TRANSPORTATION_CERT "
            + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND transportation_cert_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "trans_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            selectStmt = "select manufacturer_label_filename, MANUFACTURER_LABEL "
            + " from msds_document_types d left join msds m ON (d.msds_id=m.msds_id) WHERE manually_entered=1 AND manufacturer_label_filename IS NOT NULL";

            cmd = new SqlCommand(selectStmt, conn);

            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                KeyValuePair<string, byte[]> kvp = new KeyValuePair<string, byte[]>();
                string key = i + "label_" + reader.GetValue(0);
                byte[] value = (byte[])reader.GetValue(1);
                kvp = new KeyValuePair<string, byte[]>(key, value);

                uploadedFiles.Add(kvp);
                i++;
            }
            reader.Close();

            conn.Close();

            return uploadedFiles;
        }

        //For csv export
        public DataTable getManuallyEnteredMSDS()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select "

                + "msds.msds_id, ingredients_id, contractor_id, MSDSSERNO as [ Product Serial Number], CAGE as [CAGE (Responsible Party)], MANUFACTURER as [Company Name (Responsible Party)], PARTNO as [PART #], FSC, NIIN, hcc.HCC, "
                + "ARTICLE_IND as [Article Ind.], msds.DESCRIPTION as [Description], EMERGENCY_TEL as [Emergency Response Phone # (Responsible Party)], END_COMP_IND as [End Item Component Ind.], "
                + "END_ITEM_IND as [End Item Ind.], KIT_IND as [Kit Ind.], KIT_PART_IND as [Kit Part Ind.], MANUFACTURER_MSDS_NO as [Manufacturer MSDS #], "
                + "MIXTURE_IND as [Mixture Ind], PRODUCT_IDENTITY as [Product Identity], CONVERT(varchar, product_load_date, 101) as [Product Load Date], PRODUCT_RECORD_STATUS as [Product Record Status],"
                + "PRODUCT_REVISION_NO as [Product Revision #], PROPRIETARY_IND as [Proprietary Indicator], PUBLISHED_IND as [Published Ind], "
                + "PURCHASED_PROD_IND as [Purchased Product Ind], PURE_IND as [Pure Ind], RADIOACTIVE_IND as [Radioactive Ind], SERVICE_AGENCY as [Service/Agency], TRADE_NAME as [Trade Name],"
                + " TRADE_SECRET_IND as [Trade Secret Ind], PRODUCT_IND as [Product Ind.], PRODUCT_LANGUAGE as [Product Language], "
                 + " " 
                + "VAPOR_PRESS as [Vapor Pressure], VAPOR_DENS as [Vapor Density], SPECIFIC_GRAV as [Specific Gravity], VOC_POUNDS_GALLON as [Volatile Organic Compound (lb./g)], VOC_GRAMS_LITER as [Volatile Organic Compound (gm/L)], "
                + "PH as [pH], VISCOSITY as [Viscosity], EVAP_RATE_REF as [Evaporation Rate], SOL_IN_WATER as [Solubility In Water], APP_ODOR as [Appearance Odor Text], PERCENT_VOL_VOLUME as [% Volatiles By Volume],"
                + " AUTOIGNITION_TEMP as [Autoignition Temp. (C)], CARCINOGEN_IND as [Carcinogen Ind.], EPA_ACUTE as [EPA - Acute], "
                + "EPA_CHRONIC as [EPA - Chronic], EPA_FIRE as [EPA - Fire], EPA_PRESSURE as [EPA - Pressure], EPA_REACTIVITY as [EPA - Reactivity], msds_PHYS_CHEMICAL.FLASH_PT_TEMP as [Flash Pt. Temp. (C)], NEUT_AGENT as [Neutralizing Agent Text],"
                + " NFPA_FLAMMABILITY as [NFPA - Flammability], NFPA_HEALTH as [NFPA - Health], NFPA_REACTIVITY as [NFPA - Reactivity], NFPA_SPECIAL as [NFPA - Special], "
                + "OSHA_CARCINOGENS as [OSHA - Carcinogens], OSHA_COMB_LIQUID as [OSHA - Combustion Liquid], OSHA_COMP_GAS as [OSHA - Compressed Gas], OSHA_CORROSIVE as [OSHA - Corrosive], OSHA_EXPLOSIVE as [OSHA - Explosive],"
                + " OSHA_FLAMMABLE as [OSHA - Flammable], OSHA_HIGH_TOXIC as [OSHA - Highly Toxic], OSHA_IRRITANT as [OSHA - Irritant], OSHA_ORG_PEROX as [OSHA - Organic Peroxide], "
                + "OSHA_OTHERLONGTERM as [OSHA - Other/Long Term], OSHA_OXIDIZER as [OSHA - Oxidizer], OSHA_PYRO as [OSHA - Pyrophoric], OSHA_SENSITIZER as [OSHA - Sensitizer], OSHA_TOXIC as [OSHA - Toxic],"
                + " OSHA_UNST_REACT as [OSHA - Unstable Reactive],OSHA_WATER_REACTIVE as [OSHA - Water Reactive], OTHER_SHORT_TERM as [Other/Short Term], PHYS_STATE_CODE as [Physical State Code], VOL_ORG_COMP_WT as [Volatile Organic Compound (wt %)], "
                + " "
                + "CAS as [CAS Number], RTECS_NUM as [RTECS #], RTECS_CODE as [RTECS Code], INGREDIENT_NAME as [Component Ingredient Name], PRCNT as [% Text Value], OSHA_PEL as [OSHA PEL], OSHA_STEL as [OSHA STEL], ACGIH_TLV as [ACGIH TLV], "
                + "ACGIH_STEL as [ACGIH STEL], EPA_REPORT_QTY as [EPA RQ], DOT_REPORT_QTY as [DOT RQ], PRCNT_VOL_VALUE as [% Volume Value], PRCNT_VOL_WEIGHT as [% Weight Value], CHEM_MFG_COMP_NAME as [Chemical Manufacturer Company Name], ODS_IND as [ODS Ind], OTHER_REC_LIMITS as [Other Recorded Limits], "
                + " "
                + "CT_NUMBER as [Contract Number], CT_CAGE as [Contractor CAGE], CT_CITY as [Contractor City], CT_COMPANY_NAME as [Contractor Company Name], CT_COUNTRY as [Contractor Country], CT_PO_BOX as [Contractor P.O. Box], CT_PHONE as [Contractor Telephone Number], "
                + "PURCHASE_ORDER_NO as [Purchase Order Number], CT_STATE as [Contractor State], " 
                + " "
                + "NRC_LP_NUM as [NRC License/ Permit Number], OPERATOR as [Operator], RAD_AMOUNT_MICRO as [Radioactive Amount (Microcuries)], RAD_FORM as [Radioactive Form], RAD_CAS as [Radioisotope CAS],"
                + " RAD_NAME as [Radioisotope Name], RAD_SYMBOL as [Radioisotope Symbol], REP_NSN as [Replacement NSN], SEALED as [Sealed],"
                + " "
                + "AF_MMAC_CODE as [AF MMAC Code], CERTIFICATE_COE as [Certificate of Equivalency (COE)], "
                + "COMPETENT_CAA as [Competent Authority Approval (CAA)], DOD_ID_CODE as [Department of Defense ID Code], DOT_EXEMPTION_NO as [DOT Exemption #],"
                + " DOT_RQ_IND as [DOT RQ Ind.], EX_NO as [EX #], msds_transportation.FLASH_PT_TEMP as [Flash Pt. Temp. ( C )], "
                + "HIGH_EXPLOSIVE_WT as [High Explosive Weight], LTD_QTY_IND as [Ltd Qty Ind.], MAGNETIC_IND as [Magnetic Ind], MAGNETISM as [Magnetism (Magnetic Strength)], MARINE_POLLUTANT_IND as [Marine Pollutant Ind.], NET_EXP_QTY_DIST as [Net Explosive Qty. Distance Weight], "
                + "NET_EXP_WEIGHT as [Net Explosive Weight (kg)], NET_PROPELLANT_WT as [Net Propellant Weight (kg)], "
                + "NOS_TECHNICAL_SHIPPING_NAME as [NOS Technical Shipping Name], TRANSPORTATION_ADDITIONAL_DATA as [Transportation Additional Data],"
                + " "
                + "DOT_HAZARD_CLASS_DIV as [DOT Hazard Class/Div], DOT_HAZARD_LABEL as [DOT Hazard Label],"
                + " DOT_MAX_CARGO as [DOT Max. Quantity: Cargo Aircraft Only], DOT_MAX_PASSENGER as [DOT Max. Quantity: Passenger Aircraft/Rail], DOT_PACK_BULK as [DOT Packaging (Bulk)], DOT_PACK_EXCEPTIONS as [DOT Packaging (Exceptions)], "
                + "DOT_PACK_NONBULK as [DOT Packaging (Non Bulk)], DOT_PROP_SHIP_NAME as [DOT Proper Shipping Name], DOT_PROP_SHIP_MODIFIER as [DOT Proper Shipping Name Modifier], DOT_PSN_CODE as [DOT PSN Code],"
                + " DOT_SPECIAL_PROVISION as [DOT Special Provision], DOT_SYMBOLS as [DOT Symbols], DOT_UN_ID_NUMBER as [DOT UN ID Number], "
                + "DOT_WATER_OTHER_REQ as [DOT Water Shipment: Other Requirements], DOT_WATER_VESSEL_STOW as [DOT Water Shipment: Vessel Stowage], DOT_PACK_GROUP as [DOT Packing Group],"
                + " "
                + "AFJM_HAZARD_CLASS as [AFJM Hazard Class/Div], AFJM_PACK_PARAGRAPH as [AFJM Packaging Paragraph], AFJM_PACK_GROUP as [AFJM Packing Group], AFJM_PROP_SHIP_NAME as [AFJM Proper Shipping Name], AFJM_PROP_SHIP_MODIFIER as [AFJM Proper Shipping Name Modifier], "
                + "AFJM_PSN_CODE as [AFJM PSN Code], AFJM_SPECIAL_PROV as [AFJM Special Provisions], AFJM_SUBSIDIARY_RISK as [AFJM Subsidiary Risk], AFJM_SYMBOLS as [AFJM Symbols], AFJM_UN_ID_NUMBER as [AFJM UN ID Number], "
                + " "
                + "IATA_CARGO_PACKING as [IATA Cargo Packing: Note Cargo Aircraft Packing Instructions], IATA_HAZARD_CLASS as [IATA Hazard Class/Div], IATA_HAZARD_LABEL as [IATA Hazard Label], IATA_PACK_GROUP as [IATA Packing Group], IATA_PASS_AIR_PACK_LMT_INSTR as [IATA Passenger Air Packing: Lmt. Qty Pkg. Instr], "
                + "IATA_PASS_AIR_PACK_LMT_PER_PKG as [IATA Passenger Air Packing: Lmt. Qty. Max Qty. per Pkg], IATA_PASS_AIR_PACK_NOTE as [IATA Passenger Air Packing: Note Passenger Air Packing Instr], IATA_PROP_SHIP_NAME as [IATA Proper Shipping Name], IATA_PROP_SHIP_MODIFIER as [IATA Proper Shipping Name Modifier], IATA_CARGO_PACK_MAX_QTY as [IATA Cargo Packing: Max. Quantity], "
                + "IATA_PSN_CODE as [IATA PSN Code], IATA_PASS_AIR_MAX_QTY as [IATA Passenger Air Packing: Max. Quantity], IATA_SPECIAL_PROV as [IATA Special Provisions], IATA_SUBSIDIARY_RISK as [IATA Subsidiary Risk], IATA_UN_ID_NUMBER as [IATA UN ID Number], "
                + " "
                + "IMO_EMS_NO as [IMO EMS Number], IMO_HAZARD_CLASS as [IMO Hazard Class/Div], IMO_IBC_INSTR as [IMO IBC Instructions], IMO_LIMITED_QTY as [IMO Limited Quantity], IMO_PACK_GROUP as [IMO Packing Group], IMO_PACK_INSTRUCTIONS as [IMO Packing Instructions], IMO_PACK_PROVISIONS as [IMO Packing Provisions], "
                + "IMO_PROP_SHIP_NAME as [IMO Proper Shipping Name], IMO_PROP_SHIP_MODIFIER as [IMO Proper Shipping Name Modifier], IMO_PSN_CODE as [IMO PSN Code], IMO_SPECIAL_PROV as [IMO Special Provisions],"
                + " IMO_STOW_SEGR as [IMO Stowage and Segregation], IMO_SUBSIDIARY_RISK as [IMO Subsidiary Risk Label], IMO_TANK_INSTR_IMO as [IMO Tank Instructions, IMO], IMO_TANK_INSTR_PROV as [IMO Tank Instructions, Provisions], "
                + "IMO_TANK_INSTR_UN as [IMO Tank Instructions, UN], IMO_UN_NUMBER as [IMO UN Number], IMO_IBC_PROVISIONS as [IMO IBC Provisions],"
                + " "
                + "ITEM_MANAGER as [Item Manager], ITEM_NAME as [Item Name], SPECIFICATION_NUMBER as [Specification #], TYPE_GRADE_CLASS as [Specification Type /Grade/Class], UNIT_OF_ISSUE as [Unit Of Issue], "
                + "QUANTITATIVE_EXPRESSION as [Quantitative Expression Exp], UI_CONTAINER_QTY as [Unit of Issue Container Quantity], TYPE_OF_CONTAINER as [Type Of Container], "
                + "BATCH_NUMBER as [Batch Number], LOT_NUMBER as [Lot Number], LOG_FLIS_NIIN_VER as [Logistics FLIS NIIN Verified], LOG_FSC as [Logistics FSC], NET_UNIT_WEIGHT as [Net Unit Weight], "
                + "SHELF_LIFE_CODE as [Shelf Life Code], SPECIAL_EMP_CODE as [Special Emphasis Code], UN_NA_NUMBER as [UN/NA Number], UPC_GTIN as [UPC/GTIN], "
                + " "
                + "COMPANY_CAGE_RP as [Company CAGE (Responsible Party)], COMPANY_NAME_RP as [Company Name (Responsible Party)], LABEL_EMERG_PHONE as [Label Emergency Telephone Number#],"
                + " LABEL_ITEM_NAME as [Label Item Name], LABEL_PROC_YEAR as [Label Procurement Year], LABEL_PROD_IDENT as [Label Product Identity], "
                + "LABEL_PROD_SERIALNO as [Label Product Serial Number], LABEL_SIGNAL_WORD as [Label Signal Word], LABEL_STOCK_NO as [Label Stock Number], SPECIFIC_HAZARDS as [Specific Hazards], "
                + " "
                + "DISPOSAL_ADD_INFO as [Disposal Additional Information], EPA_HAZ_WASTE_CODE as [EPA Hazardous Waste Code], EPA_HAZ_WASTE_IND as [EPA Hazardous Waste Ind], EPA_HAZ_WASTE_NAME as [EPA Hazardous Waste Name], "
                + " "
                + "manufacturer_label_filename as [MANUFACTURER'S LABEL],  file_name as [MATERIAL SAFETY DATA SHEET], msds_translated_filename as [MSDS, TRANSLATED], "
                + "neshap_comp_filename as [NESHAP COMPLIANCE CERTIFICATE], other_docs_filename as [OTHER DOCUMENTS], product_sheet_filename as [PRODUCT SHEET], "
                + "transportation_cert_filename as [TRANSPORTATION CERTIFICATE]  " 
                + " "
                
                + " from msds LEFT JOIN hcc ON (msds.hcc_id=hcc.hcc_id) "               

                  + " LEFT JOIN msds_INGREDIENTS   ON (msds.msds_id=msds_INGREDIENTS.msds_id)"

                  + " LEFT JOIN msds_contractor_info    ON (msds.msds_id=msds_contractor_info.msds_id)"

                  + " LEFT JOIN msds_ITEM_DESCRIPTION    ON (msds.msds_id=msds_ITEM_DESCRIPTION.msds_id)"

                   + " LEFT JOIN msds_PHYS_CHEMICAL     ON (msds.msds_id=msds_PHYS_CHEMICAL.msds_id)"


                 + " LEFT JOIN msds_afjm_psn     ON (msds.msds_id=msds_afjm_psn.msds_id)"
                 
                 + " LEFT JOIN msds_disposal     ON (msds.msds_id=msds_disposal.msds_id)"
                 + " LEFT JOIN msds_document_types     ON (msds.msds_id=msds_document_types.msds_id)"
                 + " LEFT JOIN msds_dot_psn     ON (msds.msds_id=msds_dot_psn.msds_id)"
                 + " LEFT JOIN msds_iata_psn     ON (msds.msds_id=msds_iata_psn.msds_id)"
                 + " LEFT JOIN msds_imo_psn     ON (msds.msds_id=msds_imo_psn.msds_id)"
                 + " LEFT JOIN msds_label_info     ON (msds.msds_id=msds_label_info.msds_id)"
                 + " LEFT JOIN msds_radiological_info     ON (msds.msds_id=msds_radiological_info.msds_id)"
                 + " LEFT JOIN msds_transportation     ON (msds.msds_id=msds_transportation.msds_id)"
                  

                + " WHERE manually_entered=1 AND CAGE<>'1' ORDER BY msds.msds_id";


            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public int findLocationByName(String name)
        {
            int location_id = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select location_id from locations WHERE name=@name";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@name", name);

            object o = cmd.ExecuteScalar();
            if (o != null)
                location_id = Int32.Parse(""+o);

            conn.Close();

            return location_id;
        }

        public Object[] getHazardListsForInventoryId(int inventory_id)
        {
            Object[] hazards = new Object[2];

            List<int> hazardList = new List<int>();
            List<string> hccList = new List<string>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            //string selectStmt = "select hazard_id from mfg_catalog WHERE mfg_catalog_id=@mfg_catalog_id";

            //SqlCommand cmd = new SqlCommand(selectStmt, conn);
            //cmd.Parameters.AddWithValue("@mfg_catalog_id", dbNull(mfg_catalog_id));

            //object o = cmd.ExecuteScalar();
            //try
            //{
            //    hazard_id = Int32.Parse("" + o);
            //}
            //catch { }

            

            //if (hazard_id == 0)
            //{

               string selectStmt = "select hazard_id, hcc from V_Top_Hazard_Items WHERE inventory_id=@inventory_id";

                SqlCommand cmd2 = new SqlCommand(selectStmt, conn);
                cmd2.Parameters.AddWithValue("@inventory_id", dbNull(inventory_id));
                
                DataTable dt = new DataTable();

                new SqlDataAdapter(cmd2).Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    hazardList.Add(Convert.ToInt32(r["hazard_id"]));
                    hccList.Add(Convert.ToString(r["hcc"]));
                }

                if (hazardList.Count == 0)
                {
                    hazardList.Add(0);
                    hccList.Add("");
                }

            //}

            conn.Close();

            hazards[0] = hazardList;
            hazards[1] = hccList;

            return hazards;
        }

        public Object[] getHazardListsForMSDSERNO(string msdsserno)
        {
            Object[] hazards = new Object[2];

            List<int> hazardList = new List<int>();
            List<string> hccList = new List<string>();

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select hazard_id, hcc from V_MSDS_HCC_Hazards WHERE msdsserno=@msdsserno";

            SqlCommand cmd2 = new SqlCommand(selectStmt, conn);
            cmd2.Parameters.AddWithValue("@msdsserno", dbNull(msdsserno));

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd2).Fill(dt);

            foreach (DataRow r in dt.Rows)
            {
                hazardList.Add(Convert.ToInt32(r["hazard_id"]));
                hccList.Add(Convert.ToString(r["hcc"]));
            }

            if (hazardList.Count == 0)
            {
                hazardList.Add(0);
                hccList.Add("");
            }

            conn.Close();

            hazards[0] = hazardList;
            hazards[1] = hccList;

            return hazards;
        }

        public void ExecuteDatabaseBackup()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            
            conn.Open();

            string selectStmt = "use msdb; exec dbo.sp_start_job @job_name = N'SHIMS Database Backup'";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            cmd.ExecuteNonQuery();

            conn.Close();

        }

        public int? GetMsdsIdforSerialNumber(string msdsSerNo) {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "SELECT MAX(msds_id) FROM msds WHERE MSDSSERNO=@msdsSerNo";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@msdsSerNo", msdsSerNo);

            object o = cmd.ExecuteScalar();

            int? value = null;
            int id = 0;

            int.TryParse(o.ToString(), out id);

            con.Close();

            if (id > 0)
                value = id;

            return value;
        }

        public bool alreadyExistsMFGCatalog(int niin_catalog_id, string CAGE)//, string MANUFACTURER, string product_identity)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 mfg_catalog_id from mfg_catalog where niin_catalog_id=@niin_catalog_id AND CAGE=@CAGE "; //AND MANUFACTURER=@MANUFACTURER AND product_identity=@product_identity";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);
            cmd.Parameters.AddWithValue("@CAGE", CAGE);
            //cmd.Parameters.AddWithValue("@MANUFACTURER", MANUFACTURER);
            //cmd.Parameters.AddWithValue("@product_identity", product_identity);

            bool exists = false;

            object o = cmd.ExecuteScalar();

            if (o != null)
                exists = true;

            con.Close();

            return exists;

        }

        public int? getHazardIdForHCC(string HCC)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select hazard_id from hazardous_hcc where hcc=@hcc";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@hcc", HCC);

            int? value = null;

            object o = cmd.ExecuteScalar();
            
            if(o!=null)
                value = Convert.ToInt32(o);

            con.Close();

            return value;

        }

        public int? getHazardIdForHCC(int hcc_id)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select z.hazard_id from hazardous_hcc z LEFT JOIN hcc h ON (z.hcc=h.hcc) WHERE h.hcc_id=@hcc_id";

            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@hcc_id", hcc_id);

            int? value = null;

            object o = cmd.ExecuteScalar();

            if (o != null)
                value = Convert.ToInt32(o);

            con.Close();

            return value;

        }

        public int getExistingMfgForNiin(int niin_catalog_id)
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 mfg_catalog_id from mfg_catalog WHERE niin_catalog_id=@niin_catalog_id";
            SqlCommand cmd = new SqlCommand(stmt, con);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);

            int value = Convert.ToInt32(cmd.ExecuteScalar());

            con.Close();

            return value;

        }

        public string getExistingNiinWithNoInventory()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 niin from niin_catalog WHERE niin_catalog_id NOT IN (SELECT niin_catalog_id from inventory i inner join mfg_catalog m on(i.mfg_catalog_id=m.mfg_catalog_id)"
              + ") AND niin_catalog_id IN (select niin_catalog_id from mfg_catalog)"; SqlCommand cmd = new SqlCommand(stmt, con);

            string value = ""+cmd.ExecuteScalar();

            con.Close();

            return value;

        }

        public string getExistingLocationName()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 name from locations";
            SqlCommand cmd = new SqlCommand(stmt, con);

            string location = Convert.ToString(cmd.ExecuteScalar());

            con.Close();

            return location;

        }

        public string getExistingMSDSSERNO()
        {
            SqlConnection con = new SqlConnection(MDF_CONNECTION);
            con.Open();

            string stmt = "select TOP 1 MSDSSERNO from V_MSDS_HCC_HAZARDS";
            SqlCommand cmd = new SqlCommand(stmt, con);

            string location = Convert.ToString(cmd.ExecuteScalar());

            con.Close();

            return location;

        }

        public DataTable findNiinCatalog(int niin_catalog_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from niin_catalog WHERE niin_catalog_id=@niin_catalog_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@niin_catalog_id", niin_catalog_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

    
        public DataTable findShipTo(int ship_to_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select uic, organization, plate_title, city, state, zip from ship_to where ship_to_id=@ship_to_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@ship_to_id", ship_to_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable findMSDS(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, NIIN, msds.hcc_id, msds_file, file_name, HCC, "
                +" article_ind, msds.description, emergency_tel, end_comp_ind, end_item_ind, kit_ind, kit_part_ind, manufacturer_msds_no,"
                + "mixture_ind, product_identity, CONVERT(varchar, product_load_date, 101) AS product_load_date, product_record_status, product_revision_no, proprietary_ind, published_ind, purchased_prod_ind,"
                + "pure_ind, radioactive_ind, service_agency, trade_name, trade_secret_ind, product_ind, product_language, "
                + "COALESCE(created, product_load_date) AS last_update, "
                + "(hcc.hcc + ' - ' + hcc.description) AS HCC_Description "
                + " from msds LEFT JOIN hcc ON (msds.hcc_id=hcc.hcc_id) WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDScontractorInfo(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_contractor_info WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSradiologicalInfo(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_radiological_info WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDStransportation(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_transportation WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSdotPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_dot_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSingredients(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_ingredients WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSitemDescription(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_item_description WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSphysChemical(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_phys_chemical WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSafjmPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_afjm_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSiataPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_iata_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSimoPSN(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_imo_psn WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSlabelInfo(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_label_info WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSdisposal(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_disposal WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        public DataTable findMSDSdocTypes(int msds_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from msds_document_types WHERE msds_id=@msds_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }


        private object dbNull(object o)
        {
            if (o != null && !o.Equals(""))
            {
                return o;
            }
            else
            {
                return DBNull.Value;
            }
        }

        //Taking into account unit tests where we
        //need to pass in a null foreign key relationship id (represented by 0)
        private object dbFKeyNull(int o)
        {
            if (o != 0)
            {
                return o;
            }
            else
            {
                return DBNull.Value;
            }
        }

        public void deleteInventory(int inventory_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = @"DECLARE @inventory_detail_id bigint; SET @inventory_detail_id = (select inventory_detail_id from inventory_onhand where inventory_onhand_id = @inventory_id);
                                   DELETE from inventory_onhand where inventory_onhand_id=@inventory_id; IF (select count(*) from inventory_onhand where inventory_detail_id = @inventory_detail_id) < 1
                                    BEGIN DELETE from inventory_detail where inventory_detail_id = @inventory_detail_id END";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteSfr(int sfr_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from sfr where sfr_id=@sfr_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@sfr_id", sfr_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public void deleteShipTo(int ship_to_id)
        {

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            SqlTransaction tran = mdfConn.BeginTransaction();

            string selectStmt = "DELETE from ship_to where ship_to_id=@ship_to_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, mdfConn);
            cmd.Parameters.AddWithValue("@ship_to_id", ship_to_id);

            cmd.Transaction = tran;

            cmd.ExecuteNonQuery();

            tran.Commit();

            mdfConn.Close();
        }

        public DataTable getAtmosphereControlLogReport(String location, String workCenter, String usageCategory, string sortBy, bool pdfVersion)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory ";
            
            if(pdfVersion)
              selectStmt = "select ATM, NIIN, description as [Description], CAGE, Manufacturer, product_identity as [Product Identity], qty as [QTY], name as [Location], wid as [Workcenter], usage_description as [Usage Category] from vUsageCatInventory ";
            
            string whereStmt = "";
            bool addLocation = false;
            bool addWorkcenter = false;

            if (usageCategory.Equals("Restricted"))
            {
                whereStmt = " WHERE usage_description = 'Restricted'";
            }
            else if (usageCategory.Equals("Prohibited"))
            {
                whereStmt = " WHERE usage_description = 'Prohibited'";
            }
            else if (usageCategory.Equals("Limited"))
            {
                whereStmt = " WHERE usage_description = 'Limited'";
            }
            else if (usageCategory.Equals("Permitted"))
            {
                whereStmt = " WHERE usage_description = 'Permitted, no restrictions'";
            }
            else if (usageCategory.Equals("Not yet evaluated"))
            {
                whereStmt = " WHERE usage_description = 'Not yet evaluated'";
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {
                locationStmt = " name = @location";

                if (!usageCategory.Equals(""))
                    locationStmt = " AND " + locationStmt;
                else if (usageCategory.Equals(""))
                    locationStmt = " WHERE " + locationStmt;

                addLocation = true;
            }

            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";
                if (usageCategory.Equals("") && location.Equals(""))
                    workCenterStmt = " WHERE " + workCenterStmt;
                else
                    workCenterStmt = " AND " + workCenterStmt;

                addWorkcenter = true;
            }

            whereStmt += (locationStmt + workCenterStmt);

            if (location.Equals("") && workCenter.Equals("") && usageCategory.Equals(""))
                whereStmt = " WHERE usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Limited' OR usage_description = 'Permitted, no restrictions' OR usage_description = 'Not yet evaluated'";
            else if ((!location.Equals("") || !workCenter.Equals("")) && usageCategory.Equals(""))
                whereStmt = whereStmt + " AND (usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Limited' OR usage_description = 'Permitted, no restrictions' OR usage_description = 'Not yet evaluated')";

            string sort = " wid, name, niin ";
            if (sortBy != null)
                sort = sortBy;
            
            whereStmt += " ORDER BY "+sort;
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getLimResProListForReport(String location, String workCenter, String usageCategory)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select atm, NIIN, description as [Item Name], CAGE, Manufacturer, product_identity as [Product Identity], qty as [QTY], name as [Location], wid as [Workcenter], usage_description as [Usage Category], ' ' as [Signature] from vUsageCatInventory";
            string whereStmt = "";
            bool addLocation = false;
            bool addWorkcenter = false;

            if (usageCategory.Equals("Restricted"))
            {
                whereStmt = " WHERE usage_description = 'Restricted'";
            }
            else if (usageCategory.Equals("Prohibited"))
            {
                whereStmt = " WHERE usage_description = 'Prohibited'";
            }
            else if (usageCategory.Equals("Limited"))
            {
                whereStmt = " WHERE usage_description = 'Limited'";
            }
            string locationStmt = "";
            if (!location.Equals(""))
            {

                // locationStmt = " location_id = @location";
                locationStmt = " name = @location";

                if (!usageCategory.Equals(""))
                    locationStmt = " AND " + locationStmt;
                else if (usageCategory.Equals(""))
                    locationStmt = " WHERE " + locationStmt;

                addLocation = true;

            }
            string workCenterStmt = "";
            if (!workCenter.Equals(""))
            {

                workCenterStmt = " workcenter_id = @workcenter";
                if (usageCategory.Equals("") && location.Equals(""))
                    workCenterStmt = " WHERE " + workCenterStmt;
                else
                    workCenterStmt = " AND " + workCenterStmt;

                addWorkcenter = true;

            }

            whereStmt += (locationStmt + workCenterStmt);

            if (location.Equals("") && workCenter.Equals("") && usageCategory.Equals(""))
                whereStmt = " WHERE usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Prohibited'";
            else if ((!location.Equals("") || !workCenter.Equals("")) && usageCategory.Equals(""))
                whereStmt = whereStmt + " AND (usage_description = 'Restricted' OR usage_description = 'Prohibited' OR usage_description = 'Prohibited')";
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workCenter);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public bool locationHasInventory(int location_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from inventory where location_id = @loc";
            
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@loc", location_id);

            SqlDataReader reader = cmd.ExecuteReader();

            bool retVal = false;
            if (reader.HasRows)
            {
                retVal = true;
            }

            reader.Close();
            conn.Close();

            return retVal;
        }

        public DataTable getOffloadReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from v_offload_list_top v, locations l WHERE v.location_id = l.location_id AND archived_id IS NULL";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public DataTable getUsageCatReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public string CheckIncompatibility(int inventory_id, int location_id)
        {
            //look up the hcc for this item in the msds table based on niin/cage
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string hcc = null;
            string selectHccFromInv = @"select i.hcc_id from inventory_onhand oh inner join inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id where oh.inventory_onhand_id = @inventory_id;";
            SqlCommand selectHccInvCommand = new SqlCommand(selectHccFromInv, conn);
            selectHccInvCommand.Parameters.AddWithValue("@inventory_id", inventory_id);
            hcc = selectHccInvCommand.ExecuteScalar().ToString();
            //select hcc for all items in the current location
            string selectAllHccs = @"select hcc_id from inventory_onhand oh inner join inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id where oh.location_id = @location_id";
            SqlCommand selectAllHccCommand = new SqlCommand(selectAllHccs, conn);
            selectAllHccCommand.Parameters.AddWithValue("@location_id", location_id);
            List<string> hccs = new List<string>();
            using (var reader = selectAllHccCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            hccs.Add(reader.GetInt64(0).ToString());
                        }
                    }
                }
            }
            foreach (string hcc_id in hccs)
            {
                string sql2 = @"select count(hazard_warning_id) from hazard_warnings where ((hcc_id_1 = @hcc_current and hcc_id_2 = @hcc_loop) or (hcc_id_1 = @hcc_loop and hcc_id_2 = @hcc_current)) and warning_level = 'X'";
                SqlCommand sql2Cmd = new SqlCommand(sql2, conn);
                sql2Cmd.Parameters.AddWithValue("@hcc_current", hcc);
                sql2Cmd.Parameters.AddWithValue("@hcc_loop", hcc_id);
                int prohibited = (int)sql2Cmd.ExecuteScalar();
                if (prohibited > 0)
                {
                    return "PROHIBITED";
                }
                string sql1 = @"select count(hazard_warning_id) from hazard_warnings where ((hcc_id_1 = @hcc_current and hcc_id_2 = @hcc_loop) or (hcc_id_1 = @hcc_loop and hcc_id_2 = @hcc_current)) and warning_level = 'O'";
                SqlCommand sql1Cmd = new SqlCommand(sql1, conn);
                sql1Cmd.Parameters.AddWithValue("@hcc_current", hcc);
                sql1Cmd.Parameters.AddWithValue("@hcc_loop", hcc_id);
                int restricted = (int)sql1Cmd.ExecuteScalar();
                if (restricted > 0)
                {
                    return "RESTRICTED";
                }
            }
            return "OK";
        }

        public DataTable getIncompatiblesReport()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();


            string[] selectStmt = 
                    new string[] { @"select COUNT(inventory_offload_id) as Total
	                                                from inventory_offload",
	                                @"select 
	                                    COUNT(inventory_offload_id) as Good
                                    from 
	                                    inventory_offload 
                                    where 
	                                    inventory_offload_id not in (select inventory_id from v_wrong_locations)",
    
                                    @"select 
	                                    COUNT(inventory_id) as Cleared
                                    from
	                                    v_wrong_locations_approved",
	
                                    @"select 
	                                    COUNT(distinct inventory_id) as Incompatible
                                    from 
	                                    v_wrong_locations_unapproved"};

            SqlCommand cmd;
            DataTable dt = new DataTable();

            foreach (string s in selectStmt)
            {
                cmd = new SqlCommand(s, conn);
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                sda.Fill(dt);
            }
            
            conn.Close();
            return dt;

        }

        public DataTable getUsageCategoryExcelReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select * from vUsageCatInventory";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }

        public DataTable getHccExcelReport()
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"select 
	                cage, niin, smcc, hcc, serial_number, manufacturer, COSAL
                from
	                vUsageCatInventory
                where
	                hcc = '' or hcc is null";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getInventoryForShelfLifeReport(string startDate, string niin, string toDate, string location, string workcenter)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select i.atm, i.cosal, i.inventory_id, i.qty, i.mfg_catalog_id, i.shelf_life_expiration_date, i.location_id, l.name as location_name, l.workcenter_id, w.WID, w.description as Workcenter, m.cage, m.manufacturer, m.PRODUCT_IDENTITY, m.niin_catalog_id, n.niin, n.description"
                + " from inventory i LEFT JOIN locations l ON (i.location_id=l.location_id) LEFT JOIN workcenter w ON (l.workcenter_id=w.workcenter_id), mfg_catalog m, niin_catalog n "
                + " WHERE i.mfg_catalog_id=m.mfg_catalog_id AND m.niin_catalog_id = n.niin_catalog_id";

            string whereStmt = " AND shelf_life_expiration_date is NOT NULL ";
            bool addStart = false;
            bool addTo = false;
            if (!String.IsNullOrEmpty(startDate))
            {
                whereStmt += "AND shelf_life_expiration_date > @start_date ";
                addStart = true;
            }
            if (!String.IsNullOrEmpty(toDate))
            {
                whereStmt += "AND shelf_life_expiration_date < @to_date ";
                addTo = true;
            }
            bool addLocation = false;
            bool addWorkcenter = false;
            bool addNiin = false;
            
            string locationStmt = "";
            if (!location.Equals(""))
            {
                
                locationStmt = " AND name = @location ";                

                addLocation = true;

            }
            string workcenterStmt = "";
            if (!workcenter.Equals(""))
            {

                workcenterStmt = " AND l.workcenter_id = @workcenter ";

                addWorkcenter = true;

            }

            string niinStmt = "";
            if (!niin.Equals(""))
            {

                niinStmt = " AND niin like '%' + @niin + '%' ";

                addNiin = true;

            }

            whereStmt += (locationStmt + workcenterStmt + niinStmt);

            
            whereStmt += " ORDER BY shelf_life_expiration_date, wid, location_name ";
            selectStmt += whereStmt;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (addLocation)
                cmd.Parameters.Add("@location", SqlDbType.NVarChar).Value = location;//Int32.Parse(location);
            if (addWorkcenter)
                cmd.Parameters.Add("@workcenter", SqlDbType.Int).Value = Int32.Parse(workcenter);
            if (addStart)
                cmd.Parameters.Add("@start_date", SqlDbType.DateTime).Value = startDate;
            if (addTo)
                cmd.Parameters.Add("@to_date", SqlDbType.DateTime).Value = toDate;
            if (addNiin)
                cmd.Parameters.Add("@niin", SqlDbType.NVarChar).Value = niin;


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;

        }


        public string getUserRole(string userName)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select role from user_roles where username = @username";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@username", userName);
            List<string> roles = new List<string>();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string s = reader.GetValue(0).ToString();
                roles.Add(s);
            }
            conn.Close();
            if (roles.Count > 0)
            {
                if (roles[0].Contains("1"))
                    return "LEVEL 1";
                else if (roles[0].Contains("2"))
                    return "LEVEL 2";
                else return "LEVEL 3";
            }
                //if there are no registered users, give admin access so someone can set up the roles
            else return "LEVEL 1";
        }

        public string getSLCBySLCId(int slc_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select slc from shelf_life_code where shelf_life_code_id = @id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", slc_id);

            SqlDataReader reader = cmd.ExecuteReader();

            string slc = "";
            while (reader.Read())
            {
                slc = reader.GetString(0);
            }
            conn.Close();

            return slc;
        }


        public String getMSDSSerNoForID(int msds_id) {
            String msdsSerNo = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "select MSDSSERNO from msds where msds_id = @msdsid";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@msdsid", msds_id);
            msdsSerNo = (String) cmd.ExecuteScalar();

            conn.Close();

            return msdsSerNo;
        }

        /// <summary>
        /// Returns the latest version of the MSDS for the specified MSDS serial number
        /// </summary>
        /// <param name="msdsserno">the MSDS serial number</param>
        /// <returns>msdsRecord object</returns>
        ///
        public MsdsRecord getMSDSRecordforSerialNumber(string msdsserno)
        {
            return getMSDSRecordforSerialNumber(msdsserno, "");
        }

        /// <summary>
        /// Returns the latest version of the MSDS depending on whether the MSDS was
        /// manually loaded or imported from the HMIRS data
        /// </summary>
        /// <param name="msdsserno">the MSDS serial number</param>
        /// <param name="manually_entered">"" = ignore manually_entered field
        /// "Y" = extract from manually entered records
        /// "N" = extract from the original MSDS record</param>
        /// <returns>msdsRecord object</returns>
        ///
        public MsdsRecord getMSDSRecordforSerialNumber(string msdsserno, string manually_entered)
        {
            MsdsRecord msdsItem = null;
            int msds_id = 0;

            SqlConnection mdfConn = new SqlConnection(MDF_CONNECTION);

            mdfConn.Open();

            // Get the basic MSDS data and single record table items
            StringBuilder sb = new StringBuilder();
            sb.Append("select m.*, h.hcc ");
            sb.Append(" from msds m "
                + "  LEFT JOIN HCC h ON (h.hcc_id=m.hcc_id) "
                + " where m.msds_id = ");


            // Determine which version of the MSDS to get:
            // manually_entered = "" - get the latest version ignoring manually_entered flag
            // manually_entered = "Y" - get the latest manually entered version
            // manually_entered = "N" - get the original msds record
            if ("Y".Equals(manually_entered.ToUpper())) {
                sb.Append(" (select max(msds_id) from msds where msdsserno = @msdsserno "
                    + "and manually_entered = 1)");
            }
            else if ("N".Equals(manually_entered.ToUpper()))
            {
                sb.Append(" (select max(msds_id) from msds where msdsserno = @msdsserno "
                    + "and manually_entered = 0)");
            }
            else {
                sb.Append(" (select max(msds_id) from msds where msdsserno = @msdsserno)");
            }

            SqlCommand cmd = new SqlCommand(sb.ToString(), mdfConn);
            cmd.Parameters.AddWithValue("@msdsserno", msdsserno);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                msdsItem = new MsdsRecord();

                // Basic data
                msds_id = Convert.ToInt32(rdr["msds_id"].ToString());
                msdsItem.MSDSSERNO = msdsserno;
                msdsItem.FSC = rdr["FSC"].ToString();
                msdsItem.NIIN = rdr["NIIN"].ToString();
                msdsItem.CAGE = rdr["CAGE"].ToString();
                msdsItem.MANUFACTURER = rdr["MANUFACTURER"].ToString();
                msdsItem.PARTNO = rdr["PARTNO"].ToString();
                int hcc_id = -1;
                int.TryParse(rdr["hcc_id"].ToString(), out hcc_id);
                msdsItem.hcc_id = hcc_id;
                msdsItem.HCC = rdr["HCC"].ToString();
                msdsItem.ARTICLE_IND = rdr["ARTICLE_IND"].ToString();
                msdsItem.DESCRIPTION = rdr["DESCRIPTION"].ToString();
                msdsItem.EMERGENCY_TEL = rdr["EMERGENCY_TEL"].ToString();
                msdsItem.END_COMP_IND = rdr["END_COMP_IND"].ToString();
                msdsItem.END_ITEM_IND = rdr["END_ITEM_IND"].ToString();
                msdsItem.KIT_IND = rdr["KIT_IND"].ToString();
                msdsItem.KIT_PART_IND = rdr["KIT_PART_IND"].ToString();
                msdsItem.MANUFACTURER_MSDS_NO = rdr["MANUFACTURER_MSDS_NO"].ToString();
                msdsItem.MIXTURE_IND = rdr["MIXTURE_IND"].ToString();
                msdsItem.PRODUCT_IDENTITY = rdr["PRODUCT_IDENTITY"].ToString();
                msdsItem.PRODUCT_IND = rdr["PRODUCT_IND"].ToString();
                msdsItem.PRODUCT_LANGUAGE = rdr["PRODUCT_LANGUAGE"].ToString();
                msdsItem.PRODUCT_LOAD_DATE = String.Format("{0:MM/dd/yyyy}",rdr["PRODUCT_LOAD_DATE"]);
                msdsItem.PRODUCT_RECORD_STATUS = rdr["PRODUCT_RECORD_STATUS"].ToString();
                msdsItem.PRODUCT_REVISION_NO = rdr["PRODUCT_REVISION_NO"].ToString();
                msdsItem.PROPRIETARY_IND = rdr["PROPRIETARY_IND"].ToString();
                msdsItem.PUBLISHED_IND = rdr["PUBLISHED_IND"].ToString();
                msdsItem.PURCHASED_PROD_IND = rdr["PURCHASED_PROD_IND"].ToString();
                msdsItem.PURE_IND = rdr["PURE_IND"].ToString();
                msdsItem.RADIOACTIVE_IND = rdr["RADIOACTIVE_IND"].ToString();
                msdsItem.SERVICE_AGENCY = rdr["SERVICE_AGENCY"].ToString();
                msdsItem.TRADE_NAME = rdr["TRADE_NAME"].ToString();
                msdsItem.TRADE_SECRET_IND = rdr["TRADE_SECRET_IND"].ToString();
                // MSDS file
                if (rdr["MSDS_FILE"] != DBNull.Value)
                    msdsItem.msds_file = (byte[])rdr["MSDS_FILE"];
                msdsItem.file_name = rdr["file_name"].ToString();
                DateTime dtExtract;
                DateTime.TryParse(rdr["created"].ToString(), out dtExtract);
                msdsItem.CreatedDate = dtExtract;
                msdsItem.CreatedString = msdsItem.CreatedDate.ToShortDateString();

                Debug.WriteLine("CreatedDate = " + msdsItem.CreatedString);
            }

            rdr.Close();
            cmd.Dispose();
            mdfConn.Close();

            // Get the details from the other msds tables
            // Radiological_Info
            DataTable dtTemp = findMSDSradiologicalInfo(msds_id);
            if (dtTemp.Rows.Count > 0)
            {
                msdsItem.NRC_LP_NUM = dtTemp.Rows[0]["NRC_LP_NUM"].ToString();
                msdsItem.OPERATOR = dtTemp.Rows[0]["OPERATOR"].ToString();
                msdsItem.RAD_AMOUNT_MICRO = dtTemp.Rows[0]["RAD_AMOUNT_MICRO"].ToString();
                msdsItem.RAD_FORM = dtTemp.Rows[0]["RAD_FORM"].ToString();
                msdsItem.RAD_CAS = dtTemp.Rows[0]["RAD_CAS"].ToString();
                msdsItem.RAD_NAME = dtTemp.Rows[0]["RAD_NAME"].ToString();
                msdsItem.RAD_SYMBOL = dtTemp.Rows[0]["RAD_SYMBOL"].ToString();
                msdsItem.REP_NSN = dtTemp.Rows[0]["REP_NSN"].ToString();
                msdsItem.SEALED = dtTemp.Rows[0]["SEALED"].ToString();
                // Transportation
                dtTemp = findMSDStransportation(msds_id);
                msdsItem.AF_MMAC_CODE = dtTemp.Rows[0]["AF_MMAC_CODE"].ToString();
                msdsItem.CERTIFICATE_COE = dtTemp.Rows[0]["CERTIFICATE_COE"].ToString();
                msdsItem.COMPETENT_CAA = dtTemp.Rows[0]["COMPETENT_CAA"].ToString();
                msdsItem.DOD_ID_CODE = dtTemp.Rows[0]["DOD_ID_CODE"].ToString();
                msdsItem.DOT_EXEMPTION_NO = dtTemp.Rows[0]["DOT_EXEMPTION_NO"].ToString();
                msdsItem.DOT_RQ_IND = dtTemp.Rows[0]["DOT_RQ_IND"].ToString();
                msdsItem.EX_NO = dtTemp.Rows[0]["EX_NO"].ToString();
                msdsItem.LTD_QTY_IND = dtTemp.Rows[0]["LTD_QTY_IND"].ToString();
                msdsItem.MAGNETIC_IND = dtTemp.Rows[0]["MAGNETIC_IND"].ToString();
                msdsItem.MAGNETISM = dtTemp.Rows[0]["MAGNETISM"].ToString();
                msdsItem.MARINE_POLLUTANT_IND = dtTemp.Rows[0]["MARINE_POLLUTANT_IND"].ToString();
                msdsItem.NET_EXP_WEIGHT = dtTemp.Rows[0]["NET_EXP_WEIGHT"].ToString();
                msdsItem.NET_PROPELLANT_WT = dtTemp.Rows[0]["NET_PROPELLANT_WT"].ToString();
                msdsItem.NOS_TECHNICAL_SHIPPING_NAME = dtTemp.Rows[0]["NOS_TECHNICAL_SHIPPING_NAME"].ToString();
                msdsItem.TRANSPORTATION_ADDITIONAL_DATA = dtTemp.Rows[0]["TRANSPORTATION_ADDITIONAL_DATA"].ToString();
                msdsItem.TRAN_FLASH_PT_TEMP = dtTemp.Rows[0]["FLASH_PT_TEMP"].ToString();
                if (dtTemp.Rows[0]["HIGH_EXPLOSIVE_WT"].ToString().Length != 0)
                    msdsItem.HIGH_EXPLOSIVE_WT = Convert.ToInt32(dtTemp.Rows[0]["HIGH_EXPLOSIVE_WT"].ToString());
                if (dtTemp.Rows[0]["NET_EXP_QTY_DIST"].ToString().Length != 0)
                    msdsItem.NET_EXP_QTY_DIST = Convert.ToInt32(dtTemp.Rows[0]["NET_EXP_QTY_DIST"].ToString());
                // Physical_Chemical
                dtTemp = findMSDSphysChemical(msds_id);
                msdsItem.APP_ODOR = dtTemp.Rows[0]["APP_ODOR"].ToString();
                msdsItem.VAPOR_PRESS = dtTemp.Rows[0]["VAPOR_PRESS"].ToString();
                msdsItem.VAPOR_DENS = dtTemp.Rows[0]["VAPOR_DENS"].ToString();
                msdsItem.SPECIFIC_GRAV = dtTemp.Rows[0]["SPECIFIC_GRAV"].ToString();
                msdsItem.VOC_POUNDS_GALLON = dtTemp.Rows[0]["VOC_POUNDS_GALLON"].ToString();
                msdsItem.VOC_GRAMS_LITER = dtTemp.Rows[0]["VOC_GRAMS_LITER"].ToString();
                msdsItem.PH = dtTemp.Rows[0]["PH"].ToString();
                msdsItem.VISCOSITY = dtTemp.Rows[0]["VISCOSITY"].ToString();
                msdsItem.EVAP_RATE_REF = dtTemp.Rows[0]["EVAP_RATE_REF"].ToString();
                msdsItem.SOL_IN_WATER = dtTemp.Rows[0]["SOL_IN_WATER"].ToString();
                msdsItem.PERCENT_VOL_VOLUME = dtTemp.Rows[0]["PERCENT_VOL_VOLUME"].ToString();
                msdsItem.AUTOIGNITION_TEMP = dtTemp.Rows[0]["AUTOIGNITION_TEMP"].ToString();
                msdsItem.CARCINOGEN_IND = dtTemp.Rows[0]["CARCINOGEN_IND"].ToString();
                msdsItem.EPA_ACUTE = dtTemp.Rows[0]["EPA_ACUTE"].ToString();
                msdsItem.EPA_CHRONIC = dtTemp.Rows[0]["EPA_CHRONIC"].ToString();
                msdsItem.EPA_FIRE = dtTemp.Rows[0]["EPA_FIRE"].ToString();
                msdsItem.EPA_PRESSURE = dtTemp.Rows[0]["EPA_PRESSURE"].ToString();
                msdsItem.EPA_REACTIVITY = dtTemp.Rows[0]["EPA_REACTIVITY"].ToString();
                msdsItem.NEUT_AGENT = dtTemp.Rows[0]["NEUT_AGENT"].ToString();
                msdsItem.NFPA_FLAMMABILITY = dtTemp.Rows[0]["NFPA_FLAMMABILITY"].ToString();
                msdsItem.NFPA_HEALTH = dtTemp.Rows[0]["NFPA_HEALTH"].ToString();
                msdsItem.NFPA_REACTIVITY = dtTemp.Rows[0]["NFPA_REACTIVITY"].ToString();
                msdsItem.NFPA_SPECIAL = dtTemp.Rows[0]["NFPA_SPECIAL"].ToString();
                msdsItem.OSHA_CARCINOGENS = dtTemp.Rows[0]["OSHA_CARCINOGENS"].ToString();
                msdsItem.OSHA_COMB_LIQUID = dtTemp.Rows[0]["OSHA_COMB_LIQUID"].ToString();
                msdsItem.OSHA_COMP_GAS = dtTemp.Rows[0]["OSHA_COMP_GAS"].ToString();
                msdsItem.OSHA_CORROSIVE = dtTemp.Rows[0]["OSHA_CORROSIVE"].ToString();
                msdsItem.OSHA_EXPLOSIVE = dtTemp.Rows[0]["OSHA_EXPLOSIVE"].ToString();
                msdsItem.OSHA_FLAMMABLE = dtTemp.Rows[0]["OSHA_FLAMMABLE"].ToString();
                msdsItem.OSHA_HIGH_TOXIC = dtTemp.Rows[0]["OSHA_HIGH_TOXIC"].ToString();
                msdsItem.OSHA_IRRITANT = dtTemp.Rows[0]["OSHA_IRRITANT"].ToString();
                msdsItem.OSHA_ORG_PEROX = dtTemp.Rows[0]["OSHA_ORG_PEROX"].ToString();
                msdsItem.OSHA_OTHERLONGTERM = dtTemp.Rows[0]["OSHA_OTHERLONGTERM"].ToString();
                msdsItem.OSHA_OXIDIZER = dtTemp.Rows[0]["OSHA_OXIDIZER"].ToString();
                msdsItem.OSHA_PYRO = dtTemp.Rows[0]["OSHA_PYRO"].ToString();
                msdsItem.OSHA_SENSITIZER = dtTemp.Rows[0]["OSHA_SENSITIZER"].ToString();
                msdsItem.OSHA_TOXIC = dtTemp.Rows[0]["OSHA_TOXIC"].ToString();
                msdsItem.OSHA_UNST_REACT = dtTemp.Rows[0]["OSHA_UNST_REACT"].ToString();
                msdsItem.OTHER_SHORT_TERM = dtTemp.Rows[0]["OTHER_SHORT_TERM"].ToString();
                msdsItem.PHYS_STATE_CODE = dtTemp.Rows[0]["PHYS_STATE_CODE"].ToString();
                msdsItem.VOL_ORG_COMP_WT = dtTemp.Rows[0]["VOL_ORG_COMP_WT"].ToString();
                msdsItem.OSHA_WATER_REACTIVE = dtTemp.Rows[0]["OSHA_WATER_REACTIVE"].ToString();
                msdsItem.FLASH_PT_TEMP = dtTemp.Rows[0]["FLASH_PT_TEMP"].ToString();
                // DOT_PSN 
                dtTemp = findMSDSdotPSN(msds_id);
                msdsItem.DOT_HAZARD_CLASS_DIV = dtTemp.Rows[0]["DOT_HAZARD_CLASS_DIV"].ToString();
                msdsItem.DOT_HAZARD_LABEL = dtTemp.Rows[0]["DOT_HAZARD_LABEL"].ToString();
                msdsItem.DOT_MAX_CARGO = dtTemp.Rows[0]["DOT_MAX_CARGO"].ToString();
                msdsItem.DOT_MAX_PASSENGER = dtTemp.Rows[0]["DOT_MAX_PASSENGER"].ToString();
                msdsItem.DOT_PACK_BULK = dtTemp.Rows[0]["DOT_PACK_BULK"].ToString();
                msdsItem.DOT_PACK_EXCEPTIONS = dtTemp.Rows[0]["DOT_PACK_EXCEPTIONS"].ToString();
                msdsItem.DOT_PACK_NONBULK = dtTemp.Rows[0]["DOT_PACK_NONBULK"].ToString();
                msdsItem.DOT_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["DOT_PROP_SHIP_MODIFIER"].ToString();
                msdsItem.DOT_PROP_SHIP_NAME = dtTemp.Rows[0]["DOT_PROP_SHIP_NAME"].ToString();
                msdsItem.DOT_PSN_CODE = dtTemp.Rows[0]["DOT_PSN_CODE"].ToString();
                msdsItem.DOT_SPECIAL_PROVISION = dtTemp.Rows[0]["DOT_SPECIAL_PROVISION"].ToString();
                msdsItem.DOT_SYMBOLS = dtTemp.Rows[0]["DOT_SYMBOLS"].ToString();
                msdsItem.DOT_UN_ID_NUMBER = dtTemp.Rows[0]["DOT_UN_ID_NUMBER"].ToString();
                msdsItem.DOT_WATER_OTHER_REQ = dtTemp.Rows[0]["DOT_WATER_OTHER_REQ"].ToString();
                msdsItem.DOT_WATER_VESSEL_STOW = dtTemp.Rows[0]["DOT_WATER_VESSEL_STOW"].ToString();
                msdsItem.DOT_PACK_GROUP = dtTemp.Rows[0]["DOT_PACK_GROUP"].ToString();
                // AFJM_PSN
                dtTemp = findMSDSafjmPSN(msds_id);
                if (dtTemp.Rows.Count > 0)
                {
                    msdsItem.AFJM_HAZARD_CLASS = dtTemp.Rows[0]["AFJM_HAZARD_CLASS"].ToString();
                    msdsItem.AFJM_PACK_GROUP = dtTemp.Rows[0]["AFJM_PACK_GROUP"].ToString();
                    msdsItem.AFJM_PACK_PARAGRAPH = dtTemp.Rows[0]["AFJM_PACK_PARAGRAPH"].ToString();
                    msdsItem.AFJM_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["AFJM_PROP_SHIP_MODIFIER"].ToString();
                    msdsItem.AFJM_PROP_SHIP_NAME = dtTemp.Rows[0]["AFJM_PROP_SHIP_NAME"].ToString();
                    msdsItem.AFJM_PSN_CODE = dtTemp.Rows[0]["AFJM_PSN_CODE"].ToString();
                    msdsItem.AFJM_SPECIAL_PROV = dtTemp.Rows[0]["AFJM_SPECIAL_PROV"].ToString();
                    msdsItem.AFJM_SUBSIDIARY_RISK = dtTemp.Rows[0]["AFJM_SUBSIDIARY_RISK"].ToString();
                    msdsItem.AFJM_SYMBOLS = dtTemp.Rows[0]["AFJM_SYMBOLS"].ToString();
                    msdsItem.AFJM_UN_ID_NUMBER = dtTemp.Rows[0]["AFJM_UN_ID_NUMBER"].ToString();
                }
                // IATR_PSN
                dtTemp = findMSDSiataPSN(msds_id);
                msdsItem.IATA_CARGO_PACKING = dtTemp.Rows[0]["IATA_CARGO_PACKING"].ToString();
                msdsItem.IATA_HAZARD_CLASS = dtTemp.Rows[0]["IATA_HAZARD_CLASS"].ToString();
                msdsItem.IATA_HAZARD_LABEL = dtTemp.Rows[0]["IATA_HAZARD_LABEL"].ToString();
                msdsItem.IATA_PACK_GROUP = dtTemp.Rows[0]["IATA_PACK_GROUP"].ToString();
                msdsItem.IATA_PASS_AIR_MAX_QTY = dtTemp.Rows[0]["IATA_PASS_AIR_MAX_QTY"].ToString();
                msdsItem.IATA_PASS_AIR_PACK_LMT_INSTR = dtTemp.Rows[0]["IATA_PASS_AIR_PACK_LMT_PER_PKG"].ToString();
                msdsItem.IATA_PASS_AIR_PACK_LMT_PER_PKG = dtTemp.Rows[0]["IATA_PASS_AIR_PACK_LMT_PER_PKG"].ToString();
                msdsItem.IATA_PASS_AIR_PACK_NOTE = dtTemp.Rows[0]["IATA_PASS_AIR_PACK_NOTE"].ToString();
                msdsItem.IATA_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["IATA_PROP_SHIP_MODIFIER"].ToString();
                msdsItem.IATA_PROP_SHIP_NAME = dtTemp.Rows[0]["IATA_PROP_SHIP_NAME"].ToString();
                msdsItem.IATA_CARGO_PACK_MAX_QTY = dtTemp.Rows[0]["IATA_CARGO_PACK_MAX_QTY"].ToString();
                msdsItem.IATA_PSN_CODE = dtTemp.Rows[0]["IATA_PSN_CODE"].ToString();
                msdsItem.IATA_SPECIAL_PROV = dtTemp.Rows[0]["IATA_SPECIAL_PROV"].ToString();
                msdsItem.IATA_SUBSIDIARY_RISK = dtTemp.Rows[0]["IATA_SUBSIDIARY_RISK"].ToString();
                msdsItem.IATA_UN_ID_NUMBER = dtTemp.Rows[0]["IATA_UN_ID_NUMBER"].ToString();
                // IMO_PSN
                dtTemp = findMSDSimoPSN(msds_id);
                msdsItem.IMO_EMS_NO = dtTemp.Rows[0]["IMO_EMS_NO"].ToString();
                msdsItem.IMO_HAZARD_CLASS = dtTemp.Rows[0]["IMO_HAZARD_CLASS"].ToString();
                msdsItem.IMO_IBC_INSTR = dtTemp.Rows[0]["IMO_IBC_INSTR"].ToString();
                msdsItem.IMO_LIMITED_QTY = dtTemp.Rows[0]["IMO_LIMITED_QTY"].ToString();
                msdsItem.IMO_PACK_GROUP = dtTemp.Rows[0]["IMO_PACK_GROUP"].ToString();
                msdsItem.IMO_PACK_INSTRUCTIONS = dtTemp.Rows[0]["IMO_PACK_INSTRUCTIONS"].ToString();
                msdsItem.IMO_PACK_PROVISIONS = dtTemp.Rows[0]["IMO_PACK_PROVISIONS"].ToString();
                msdsItem.IMO_PROP_SHIP_NAME = dtTemp.Rows[0]["IMO_PROP_SHIP_NAME"].ToString();
                msdsItem.IMO_PROP_SHIP_MODIFIER = dtTemp.Rows[0]["IMO_PROP_SHIP_MODIFIER"].ToString();
                msdsItem.IMO_PSN_CODE = dtTemp.Rows[0]["IMO_PSN_CODE"].ToString();
                msdsItem.IMO_SPECIAL_PROV = dtTemp.Rows[0]["IMO_SPECIAL_PROV"].ToString();
                msdsItem.IMO_STOW_SEGR = dtTemp.Rows[0]["IMO_STOW_SEGR"].ToString();
                msdsItem.IMO_SUBSIDIARY_RISK = dtTemp.Rows[0]["IMO_SUBSIDIARY_RISK"].ToString();
                msdsItem.IMO_TANK_INSTR_IMO = dtTemp.Rows[0]["IMO_TANK_INSTR_IMO"].ToString();
                msdsItem.IMO_TANK_INSTR_PROV = dtTemp.Rows[0]["IMO_TANK_INSTR_PROV"].ToString();
                msdsItem.IMO_TANK_INSTR_UN = dtTemp.Rows[0]["IMO_TANK_INSTR_UN"].ToString();
                msdsItem.IMO_UN_NUMBER = dtTemp.Rows[0]["IMO_UN_NUMBER"].ToString();
                msdsItem.IMO_IBC_PROVISIONS = dtTemp.Rows[0]["IMO_IBC_PROVISIONS"].ToString();
                // Item_Description
                dtTemp = findMSDSitemDescription(msds_id);
                msdsItem.ITEM_MANAGER = dtTemp.Rows[0]["ITEM_MANAGER"].ToString();
                msdsItem.ITEM_NAME = dtTemp.Rows[0]["ITEM_NAME"].ToString();
                msdsItem.SPECIFICATION_NUMBER = dtTemp.Rows[0]["SPECIFICATION_NUMBER"].ToString();
                msdsItem.TYPE_GRADE_CLASS = dtTemp.Rows[0]["TYPE_GRADE_CLASS"].ToString();
                msdsItem.UNIT_OF_ISSUE = dtTemp.Rows[0]["UNIT_OF_ISSUE"].ToString();
                msdsItem.QUANTITATIVE_EXPRESSION = dtTemp.Rows[0]["QUANTITATIVE_EXPRESSION"].ToString();
                msdsItem.UI_CONTAINER_QTY = dtTemp.Rows[0]["UI_CONTAINER_QTY"].ToString();
                msdsItem.BATCH_NUMBER = dtTemp.Rows[0]["BATCH_NUMBER"].ToString();
                msdsItem.LOT_NUMBER = dtTemp.Rows[0]["LOT_NUMBER"].ToString();
                msdsItem.LOG_FLIS_NIIN_VER = dtTemp.Rows[0]["LOG_FLIS_NIIN_VER"].ToString();
                msdsItem.NET_UNIT_WEIGHT = dtTemp.Rows[0]["NET_UNIT_WEIGHT"].ToString();
                msdsItem.TYPE_OF_CONTAINER = dtTemp.Rows[0]["TYPE_OF_CONTAINER"].ToString();
                msdsItem.SHELF_LIFE_CODE = dtTemp.Rows[0]["SHELF_LIFE_CODE"].ToString();
                msdsItem.SPECIAL_EMP_CODE = dtTemp.Rows[0]["SPECIAL_EMP_CODE"].ToString();
                msdsItem.UN_NA_NUMBER = dtTemp.Rows[0]["UN_NA_NUMBER"].ToString();
                msdsItem.UPC_GTIN = dtTemp.Rows[0]["UPC_GTIN"].ToString();
                if (dtTemp.Rows[0]["LOG_FSC"].ToString().Length != 0)
                    msdsItem.LOG_FSC = Convert.ToInt32(dtTemp.Rows[0]["LOG_FSC"].ToString());
                // Label_Info
                dtTemp = findMSDSlabelInfo(msds_id);
                msdsItem.COMPANY_CAGE_RP = dtTemp.Rows[0]["COMPANY_CAGE_RP"].ToString();
                msdsItem.COMPANY_NAME_RP = dtTemp.Rows[0]["COMPANY_NAME_RP"].ToString();
                msdsItem.LABEL_EMERG_PHONE = dtTemp.Rows[0]["LABEL_EMERG_PHONE"].ToString();
                msdsItem.LABEL_ITEM_NAME = dtTemp.Rows[0]["LABEL_ITEM_NAME"].ToString();
                msdsItem.LABEL_PROC_YEAR = dtTemp.Rows[0]["LABEL_PROC_YEAR"].ToString();
                msdsItem.LABEL_PROD_IDENT = dtTemp.Rows[0]["LABEL_PROD_IDENT"].ToString();
                msdsItem.LABEL_PROD_SERIALNO = dtTemp.Rows[0]["LABEL_PROD_SERIALNO"].ToString();
                msdsItem.LABEL_SIGNAL_WORD = dtTemp.Rows[0]["LABEL_SIGNAL_WORD"].ToString();
                msdsItem.LABEL_STOCK_NO = dtTemp.Rows[0]["LABEL_STOCK_NO"].ToString();
                msdsItem.SPECIFIC_HAZARDS = dtTemp.Rows[0]["SPECIFIC_HAZARDS"].ToString();
                // DocuemntTypes
                dtTemp = findMSDSdocTypes(msds_id);
                if (dtTemp.Rows[0]["MSDS_TRANSLATED"] != DBNull.Value)
                    msdsItem.MSDS_TRANSLATED = (byte[])dtTemp.Rows[0]["MSDS_TRANSLATED"];
                if (dtTemp.Rows[0]["NESHAP_COMP_CERT"] != DBNull.Value)
                    msdsItem.NESHAP_COMP_CERT = (byte[])dtTemp.Rows[0]["NESHAP_COMP_CERT"];
                if (dtTemp.Rows[0]["OTHER_DOCS"] != DBNull.Value)
                    msdsItem.OTHER_DOCS = (byte[])dtTemp.Rows[0]["OTHER_DOCS"];
                if (dtTemp.Rows[0]["PRODUCT_SHEET"] != DBNull.Value)
                    msdsItem.PRODUCT_SHEET = (byte[])dtTemp.Rows[0]["PRODUCT_SHEET"];
                if (dtTemp.Rows[0]["TRANSPORTATION_CERT"] != DBNull.Value)
                    msdsItem.TRANSPORTATION_CERT = (byte[])dtTemp.Rows[0]["TRANSPORTATION_CERT"];
                msdsItem.msds_translated_filename = dtTemp.Rows[0]["msds_translated_filename"].ToString();
                msdsItem.neshap_comp_filename = dtTemp.Rows[0]["neshap_comp_filename"].ToString();
                msdsItem.other_docs_filename = dtTemp.Rows[0]["other_docs_filename"].ToString();
                msdsItem.product_sheet_filename = dtTemp.Rows[0]["product_sheet_filename"].ToString();
                msdsItem.transportation_cert_filename = dtTemp.Rows[0]["transportation_cert_filename"].ToString();
                msdsItem.manufacturer_label_filename = dtTemp.Rows[0]["manufacturer_label_filename"].ToString();
                // Disposal
                dtTemp = findMSDSdisposal(msds_id);
                msdsItem.DISPOSAL_ADD_INFO = dtTemp.Rows[0]["DISPOSAL_ADD_INFO"].ToString();
                msdsItem.EPA_HAZ_WASTE_CODE = dtTemp.Rows[0]["EPA_HAZ_WASTE_CODE"].ToString();
                msdsItem.EPA_HAZ_WASTE_IND = dtTemp.Rows[0]["EPA_HAZ_WASTE_IND"].ToString();
                msdsItem.EPA_HAZ_WASTE_NAME = dtTemp.Rows[0]["EPA_HAZ_WASTE_NAME"].ToString();
            }

            // Now for the lists
            // Ingredients
            dtTemp = findMSDSingredients(msds_id);
            foreach (DataRow row in dtTemp.Rows)
            {
                // Get the ingredient object 
                MsdsIngredients ingredient = buildMsdsIngredient(row);
                msdsItem.ingredientsList.Add(ingredient);
            }
            // Contractors
            dtTemp = findMSDScontractorInfo(msds_id);
            foreach (DataRow row in dtTemp.Rows)
            {
                // Get the contractor_info object 
                MsdsContractors contractor = buildMsdsContractor(row);
                msdsItem.contractorsList.Add(contractor);
            }

            return msdsItem;
        }

        private MsdsIngredients buildMsdsIngredient(DataRow row)
        {
            MsdsIngredients item = new MsdsIngredients();

            item.CAS = row["CAS"].ToString();
            item.RTECS_NUM = row["RTECS_NUM"].ToString();
            item.RTECS_CODE = row["RTECS_CODE"].ToString();
            item.INGREDIENT_NAME = row["INGREDIENT_NAME"].ToString();
            item.PRCNT = row["PRCNT"].ToString();
            item.OSHA_PEL = row["OSHA_PEL"].ToString();
            item.OSHA_STEL = row["OSHA_STEL"].ToString();
            item.ACGIH_TLV = row["ACGIH_TLV"].ToString();
            item.ACGIH_STEL = row["ACGIH_STEL"].ToString();
            item.EPA_REPORT_QTY = row["EPA_REPORT_QTY"].ToString();
            item.DOT_REPORT_QTY = row["DOT_REPORT_QTY"].ToString();
            item.PRCNT_VOL_VALUE = row["PRCNT_VOL_VALUE"].ToString();
            item.PRCNT_VOL_WEIGHT = row["PRCNT_VOL_WEIGHT"].ToString();
            item.CHEM_MFG_COMP_NAME = row["CHEM_MFG_COMP_NAME"].ToString();
            item.ODS_IND = row["ODS_IND"].ToString();
            item.OTHER_REC_LIMITS = row["OTHER_REC_LIMITS"].ToString();

            return item;
        }

        private MsdsContractors buildMsdsContractor(DataRow row)
        {
            MsdsContractors item = new MsdsContractors();

            item.CT_NUMBER = row["CT_NUMBER"].ToString();
            item.CT_CAGE = row["CT_CAGE"].ToString();
            item.CT_CITY = row["CT_CITY"].ToString();
            item.CT_COMPANY_NAME = row["CT_COMPANY_NAME"].ToString();
            item.CT_COUNTRY = row["CT_COUNTRY"].ToString();
            item.CT_PO_BOX = row["CT_PO_BOX"].ToString();
            item.CT_PHONE = row["CT_PHONE"].ToString();
            item.PURCHASE_ORDER_NO = row["PURCHASE_ORDER_NO"].ToString();
            item.CT_STATE = row["CT_STATE"].ToString();

            return item;
        }

        public DataTable getMSDSWithChangedHCC(string filterColumn, string filterText) {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            StringBuilder selectStmt = new StringBuilder();
            selectStmt.Append("select m1.msds_id AS m1_msds_id, m1.NIIN AS m1_NIIN, ");
            selectStmt.Append("  m1.MSDSSERNO AS m1_MSDSSERNO, m1.CAGE AS m1_CAGE, ");
            selectStmt.Append("  m1.MANUFACTURER AS m1_Manufacturer, m1.PARTNO AS m1_PartNo, ");
            selectStmt.Append("  (CASE m1.manually_entered WHEN 0 THEN 'Y' ELSE '' END) AS m1_HMIRS, ");
            selectStmt.Append("  COALESCE(m1.DESCRIPTION, m1.PRODUCT_IDENTITY) AS m1_Description, ");
            selectStmt.Append("  CONVERT(varchar, COALESCE(m1.created, m1.product_load_date), 101) AS m1_Load_Date, ");
            selectStmt.Append("  hc1.hcc AS m1_HCC, hc1.description AS m1_HCC_Description, ");
            selectStmt.Append("  m2.msds_id AS m2_msds_id, m2.NIIN AS m2_NIIN, ");
            selectStmt.Append("  m2.MSDSSERNO AS m2_MSDSSERNO, m2.CAGE AS m2_CAGE, ");
            selectStmt.Append("  m2.MANUFACTURER AS m2_Manufacturer, m2.PARTNO AS m2_PartNo, ");
            selectStmt.Append("  (CASE m2.manually_entered WHEN 0 THEN 'Y' ELSE '' END) AS m2_HMIRS, ");
            selectStmt.Append("  COALESCE(m2.DESCRIPTION, m2.PRODUCT_IDENTITY) AS m2_Description, ");
            selectStmt.Append("  CONVERT(varchar, COALESCE(m2.created, m2.product_load_date), 101) AS m2_Load_Date, ");
            selectStmt.Append(  "hc2.hcc AS m2_HCC, hc2.description AS m2_HCC_Description ");
            selectStmt.Append("from msds m1 ");
            selectStmt.Append("  left outer join hcc hc1 on hc1.hcc_id = m1.hcc_id ");
            selectStmt.Append("  join msds m2 on m2.MSDSSERNO = m1.MSDSSERNO ");
            selectStmt.Append("  left outer join hcc hc2 on hc2.hcc_id = m2.hcc_id ");
            selectStmt.Append("  where ISNULL(m2.hcc_id, 0) != ISNULL(m1.hcc_id, 0) ");
            selectStmt.Append("  and m1.msds_id = (SELECT MIN(msds_id) FROM msds where MSDSSERNO = m1.MSDSSERNO) ");
            selectStmt.Append("  and m2.msds_id = (SELECT MAX(msds_id) FROM msds where MSDSSERNO = m1.MSDSSERNO) ");

            // See if we are doing any filtering
            if (!filterText.Equals("")) {
                if (filterColumn.Equals("MSDSSERNO")) {
                    selectStmt.Append("  AND m1.MSDSSERNO LIKE '%' + @filter + '%' ");
                }
                else if (filterColumn.Equals("HCC")) {
                    selectStmt.Append("  AND (hc1.hcc LIKE '%' + @filter + '%' " +
                        "OR hc2.hcc LIKE '%' + @filter + '%') ");
                }
            }

            selectStmt.Append("ORDER BY m1.MSDSSERNO");

            // Run the command
            SqlCommand cmd = new SqlCommand(selectStmt.ToString(), conn);
            cmd.Parameters.AddWithValue("@filter", filterText);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public String getLastSMCLUploadDate() {
            String uploadDateFormatted = null;

            List<FileLoadInfo> fileInfo = getFileTableInfo(FILE_TABLES.FileUpload,
                FILE_TYPES.SMCL.ToString(), true);

            if (fileInfo.Count > 0) {
                uploadDateFormatted = String.Format("{0:MM/dd/yyyy HH:mm}", fileInfo[0].ProcessDate);
            }
            else {
                uploadDateFormatted = "";
            }

            return uploadDateFormatted;
        }

        public String getLastIncUploadDate()
        {
            String uploadDateFormatted = null;

            List<FileLoadInfo> fileInfo = getFileTableInfo(FILE_TABLES.FileUpload,
                FILE_TYPES.INC.ToString(), true);

            if (fileInfo.Count > 0)
            {
                uploadDateFormatted = String.Format("{0:MM/dd/yyyy HH:mm}", fileInfo[0].ProcessDate);
            }
            else
            {
                uploadDateFormatted = "";
            }

            return uploadDateFormatted;
        }


        public DateTime? getLastSMCLUploadDateTime()
        {
            DateTime? uploadDateFormatted = null;

            List<FileLoadInfo> fileInfo = getFileTableInfo(FILE_TABLES.FileUpload,
                FILE_TYPES.SMCL.ToString(), true);

            if (fileInfo.Count > 0)
            {
                uploadDateFormatted = fileInfo[0].ProcessDate;
            }

            return uploadDateFormatted;
        }


        public bool CheckForExpiredSmcl()
        {
            var lastUpload = getLastSMCLUploadDateTime();
            if (lastUpload != null)
            {
                TimeSpan difference = DateTime.Now.Subtract((DateTime)lastUpload);
                if (difference.Days > 90)
                {
                    return true;
                }
            }
            return false;
        }

        public string getTipOfTheDay()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "SELECT TOP 1 TipText FROM TipOfTheDay ORDER BY NEWID()";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            string tip = cmd.ExecuteScalar().ToString();
            conn.Close();
            return tip;
        }

        public DataTable getInventoryItemForDeleteConfirm(int inventory_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"select 
	                mfg.cage as cage,
                    i.niin as niin,
                    oh.expiration_date as expiration_date,
                    oh.inventory_onhand_id as inventory_id,
                    l.name as location_name
                  from 
                      inventory_onhand oh 
                      inner join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id
                      left JOIN mfg_catalog mfg 
                             ON ( oh.mfg_catalog_id = mfg.mfg_catalog_id ) 
                      left JOIN niin_catalog n 
                             ON ( mfg.niin = n.niin_catalog_id ) 
                      left JOIN locations l on (oh.location_id = l.location_id) where oh.inventory_onhand_id = " + inventory_id;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public UpdateOnHandRowModel getUpdatedOnHandItem(int inventory_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"select 
                         i.inventory_onhand_id as inventory_id,
                         i.location_id as location_id, 
                         l.name as location_name,
                         w.wid as workcenter_name,
                         i.inventoried_date as inv_date,
                         i.onboard_date as onboard_date,
                         i.expiration_date as expiration_date,
                         i.qty,
                         l.workcenter_id as workcenter_id,
                         i.manufacturer as manufacturer,
                         i.cage as cage 
                  FROM inventory_onhand i 
                       LEFT OUTER JOIN locations l ON l.location_id = i.location_id 
                       LEFT OUTER JOIN workcenter w ON w.workcenter_id = l.workcenter_id

                 WHERE i.inventory_onhand_id = @inventory_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);

            DataTable dt = new DataTable();
            List<UpdateOnHandRowModel> data = new List<UpdateOnHandRowModel>();
            new SqlDataAdapter(cmd).Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                UpdateOnHandRowModel returnRow = new UpdateOnHandRowModel()
                {
                    id = Convert.ToInt32(dr["inventory_id"]),
                    location = new Location() { location_id = (dr["location_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["location_id"]) : null), location_name = dr["location_name"].ToString() },
                    cage = dr["cage"].ToString(),
                    manufacturer = dr["manufacturer"].ToString(),
                    qty = dr["qty"] != DBNull.Value ? (int?)Convert.ToInt32(dr["qty"]) : null,
                    workcenter = new Workcenter() { workcenter_id = (dr["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["workcenter_id"]) : null), workcenter_name = dr["workcenter_name"].ToString() },
                    inv_date = dr["inv_date"] != DBNull.Value ? Convert.ToDateTime(dr["inv_date"]).ToString("MM/dd/yyyy") : null,
                    onboard_date = dr["onboard_date"] != DBNull.Value ? Convert.ToDateTime(dr["onboard_date"]).ToString("MM/dd/yyyy") : null,
                    expiration_date = dr["expiration_date"] != DBNull.Value ? Convert.ToDateTime(dr["expiration_date"]).ToString("MM/dd/yyyy") : null
                };

                data.Add(returnRow);
            }

            conn.Close();

            return data[0];
        }

        public List<UpdateOnHandRowModel> getOnHandInfoByInventoryDetailId(int? inventory_detail_id, int? workcenter_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"select 
                         oh.inventory_onhand_id as inventory_id,
                         i.inventory_detail_id, 
                         oh.location_id as location_id, 
                         l.name as location_name,
                         w.wid as workcenter_name,
                         oh.inventoried_date as inv_date,
                         oh.onboard_date as onboard_date,
                         oh.expiration_date as expiration_date,
                         oh.qty,
                         l.workcenter_id as workcenter_id,
                         mfg.manufacturer as manufacturer,
                         mfg.cage as cage 
                  FROM inventory_onhand oh 
                       LEFT JOIN inventory_detail i ON i.inventory_detail_id = oh.inventory_detail_id
                       LEFT JOIN mfg_catalog mfg ON oh.mfg_catalog_id = mfg.mfg_catalog_id
                       LEFT JOIN niin_catalog nc ON mfg.niin = nc.niin 
                       LEFT JOIN locations l ON l.location_id = oh.location_id 
                       LEFT JOIN workcenter w ON w.workcenter_id = l.workcenter_id

                 WHERE i.inventory_detail_id = @inventory_detail_id";
            bool filterByWorkCenter = false;
            if (workcenter_id != null)
            {
                filterByWorkCenter = true;
                selectStmt += " and l.workcenter_id = @workcenter_id";
            }

            var cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_detail_id", inventory_detail_id);
            if (filterByWorkCenter)
            {
                cmd.Parameters.AddWithValue(@"workcenter_id", workcenter_id);
            }

            DataTable dt = new DataTable();
            List<UpdateOnHandRowModel> data = new List<UpdateOnHandRowModel>();
            new SqlDataAdapter(cmd).Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                UpdateOnHandRowModel returnRow = new UpdateOnHandRowModel()
                {
                    id = Convert.ToInt32(dr["inventory_id"]),
                    inventory_detail_id = Convert.ToInt32(dr["inventory_detail_id"]),
                    location = new Location() { location_id = (dr["location_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["location_id"]) : null), location_name = dr["location_name"].ToString() },
                    cage = dr["cage"].ToString(),
                    manufacturer = dr["manufacturer"].ToString(),
                    qty = dr["qty"] != DBNull.Value ? (int?)Convert.ToInt32(dr["qty"]) : null,
                    workcenter = new Workcenter() { workcenter_id = (dr["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["workcenter_id"]) : null), workcenter_name = dr["workcenter_name"].ToString() },
                    inv_date = dr["inv_date"] != DBNull.Value ? Convert.ToDateTime(dr["inv_date"]).ToString("MM/dd/yyyy") : null,
                    onboard_date = dr["onboard_date"] != DBNull.Value ? Convert.ToDateTime(dr["onboard_date"]).ToString("MM/dd/yyyy") : null,
                    expiration_date = dr["expiration_date"] != DBNull.Value ? Convert.ToDateTime(dr["expiration_date"]).ToString("MM/dd/yyyy") : null
                };

                data.Add(returnRow);
            }

            conn.Close();

            return data;
        }

        public List<UpdateOnHandRowModel> getInventoryDetailsForGrid(int? inventory_id, int? workcenter_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectNiin =
                @"select i.niin from inventory_onhand oh inner join inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id where oh.inventory_onhand_id = @inventory_id";
            SqlCommand cmd = new SqlCommand(selectNiin, conn);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);

            string niin = cmd.ExecuteScalar().ToString();

            string selectStmt =
                @"select 
                         oh.inventory_onhand_id as inventory_id,
                         i.inventory_detail_id, 
                         oh.location_id as location_id, 
                         l.name as location_name,
                         w.wid as workcenter_name,
                         oh.inventoried_date as inv_date,
                         oh.onboard_date as onboard_date,
                         oh.expiration_date as expiration_date,
                         oh.qty,
                         l.workcenter_id as workcenter_id,
                         oh.manufacturer as manufacturer,
                         oh.cage as cage 
                  FROM inventory_detail i 
                       left JOIN inventory_onhand oh ON i.inventory_detail_id = oh.inventory_detail_id
                       left JOIN locations l ON l.location_id = oh.location_id 
                       left JOIN workcenter w ON w.workcenter_id = l.workcenter_id

                 WHERE i.niin = @niin";
            bool filterByWorkCenter = false;
            if (workcenter_id != null)
            {
                filterByWorkCenter = true;
                selectStmt += " and l.workcenter_id = @workcenter_id";
            }

            cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@niin", niin);
            if (filterByWorkCenter)
            {
                cmd.Parameters.AddWithValue(@"workcenter_id", workcenter_id);
            }

            DataTable dt = new DataTable();
            List<UpdateOnHandRowModel> data = new List<UpdateOnHandRowModel>();
            new SqlDataAdapter(cmd).Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                UpdateOnHandRowModel returnRow = new UpdateOnHandRowModel()
                {
                    id = Convert.ToInt32(dr["inventory_id"]),
                    inventory_detail_id = Convert.ToInt32(dr["inventory_detail_id"]),
                    location = new Location() { location_id = (dr["location_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["location_id"]) : null), location_name = dr["location_name"].ToString() },
                    cage = dr["cage"].ToString(),
                    manufacturer = dr["manufacturer"].ToString(),
                    qty = dr["qty"] != DBNull.Value ? (int?)Convert.ToInt32(dr["qty"]) : null,
                    workcenter = new Workcenter() { workcenter_id = (dr["workcenter_id"] != DBNull.Value ? (int?)Convert.ToInt32(dr["workcenter_id"]) : null), workcenter_name = dr["workcenter_name"].ToString() },
                    inv_date = dr["inv_date"] != DBNull.Value ? Convert.ToDateTime(dr["inv_date"]).ToString("MM/dd/yyyy") : null,
                    onboard_date = dr["onboard_date"] != DBNull.Value ? Convert.ToDateTime(dr["onboard_date"]).ToString("MM/dd/yyyy") : null,
                    expiration_date = dr["expiration_date"] != DBNull.Value ? Convert.ToDateTime(dr["expiration_date"]).ToString("MM/dd/yyyy") : null
                };

                data.Add(returnRow);
            }

            conn.Close();

            return data;
        }

        public DataTable getInventoryDetailsForReport(int? inventory_id, int? workcenter_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectDetailId =
                @"select inventory_detail_id from inventory_onhand where inventory_onhand_id = @inventory_id";
            SqlCommand cmd = new SqlCommand(selectDetailId, conn);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);

            string inventory_detail_id = cmd.ExecuteScalar().ToString();

            string selectStmt =
                @"select 
                         l.name as Location,
                         mfg.cage as CAGE,
                         mfg.manufacturer as Manufacturer, 
                         oh.qty as Quantity,
                         w.wid as Workcenter,
                         oh.inventoried_date as [Inv Date],
                         oh.onboard_date as [Onboard Date],
                         oh.expiration_date as [Expiration Date]
                  FROM inventory_onhand oh 
                       LEFT JOIN inventory_detail i ON i.inventory_detail_id = oh.inventory_detail_id
                       LEFT JOIN mfg_catalog mfg ON oh.mfg_catalog_id = mfg.mfg_catalog_id
                       LEFT JOIN niin_catalog nc ON mfg.niin = nc.niin 
                       LEFT JOIN locations l ON l.location_id = oh.location_id 
                       LEFT JOIN workcenter w ON w.workcenter_id = l.workcenter_id

                 WHERE oh.inventory_detail_id = @inventory_detail_id";
            bool filterByWorkCenter = false;
            if (workcenter_id != null)
            {
                filterByWorkCenter = true;
                selectStmt += " and l.workcenter_id = @workcenter_id";
            }

            cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@inventory_detail_id", inventory_detail_id);
            if (filterByWorkCenter)
            {
                cmd.Parameters.AddWithValue(@"workcenter_id", workcenter_id);
            }

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public void UpdateInventoryOnHand(UpdateOnHandModel item)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            object cage = null;
            object mfg_catalog_id = null;
            if (item.data.manufacturer != null)
            {
                string getCage = "select distinct cage from mfg_catalog where manufacturer = @manufacturer  union select distinct cage from inventory_onhand where manufacturer = @manufacturer";
                SqlCommand cageCmd = new SqlCommand(getCage, conn);
                cageCmd.Parameters.AddWithValue("@manufacturer", item.data.manufacturer);
                cage = cageCmd.ExecuteScalar();

                string getMfgCatalogId = "select mfg_catalog_id from mfg_catalog where cage = @cage and niin = (select niin from inventory_detail where inventory_detail_id = @inventory_detail_id)";
                SqlCommand mfgCatalogCmd = new SqlCommand(getMfgCatalogId, conn);
                mfgCatalogCmd.Parameters.Add("@cage", cage);
                mfgCatalogCmd.Parameters.Add("@inventory_detail_id", item.data.inventory_detail_id);
                mfg_catalog_id = mfgCatalogCmd.ExecuteScalar();
            }

            string selectStmt = "UPDATE inventory_onhand set expiration_date = @expiration_date, location_id = @location, qty = @qty,  "
                + "inventoried_date = @inv_date, onboard_date = @onboard_date, cage = @cage, manufacturer = @manufacturer, mfg_catalog_id = @mfg_catalog_id WHERE inventory_onhand_id = @id";
                
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@expiration_date", ((object)item.data.expiration_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@location", ((object)item.data.location.location_id) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@qty", ((object)item.data.qty) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@inv_date", ((object)item.data.inv_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@onboard_date", ((object)item.data.onboard_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@manufacturer", ((object)item.data.manufacturer) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@cage", cage ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", item.id);
            cmd.Parameters.AddWithValue("@mfg_catalog_id", mfg_catalog_id ?? DBNull.Value);

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public int AddInventoryItem(UpdateOnHandModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            int result;
            string sql = "insert into inventory_onhand (expiration_date, inventory_detail_id, location_id, qty, manufacturer, cage, inventoried_date, onboard_date) values (@expiration_date, @inventory_detail_id, @location, @qty, @manufacturer, "
                + " @cage, @inv_date, @onboard_date); select scope_identity()";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@expiration_date", ((object)model.data.expiration_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@inventory_detail_id", ((object)model.data.inventory_detail_id) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@location", ((object)model.data.location.location_id) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@qty", ((object)model.data.qty) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@inv_date", ((object)model.data.inv_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@onboard_date", ((object)model.data.onboard_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@manufacturer", ((object)model.data.manufacturer) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@cage", ((object)model.data.cage) ?? DBNull.Value);
            result = Convert.ToInt32(cmd.ExecuteScalar());
            return result;
        }

        public void masterUpdate(MasterUpdateModel data)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "UPDATE inventory_onhand set expiration_date = @expiration_date, "
                + "inventoried_date = @inv_date, qty = @quantity WHERE inventory_onhand_id = @id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@quantity", ((object)data.data.quantity) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@inv_date", ((object)data.data.inv_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@expiration_date", ((object)data.data.shelf_life_date) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@id", data.id);

            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public List<MasterTableRowModel> getMasterUpdateData(int? inventory_id, string workcenterId, string locationId)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt = "SELECT oh.inventory_onhand_id, oh.expiration_date, l.name as location_name, i.niin, i.fsc, w.wid, oh.qty, oh.inventoried_date " +
                "FROM inventory_onhand oh LEFT JOIN inventory_detail i on oh.inventory_detail_id = i.inventory_detail_id "
                 + "LEFT JOIN locations l on oh.location_id = l.location_id "
                 + "LEFT JOIN workcenter w on w.workcenter_id = l.workcenter_id ";

            string where = "";
            bool hasWhere = false;
            bool filterByInventoryId = false;
            bool filterByWorkcenter = false;
            bool filterByLocation = false;

            if (inventory_id != null)
            {
                hasWhere = true;
                where += "WHERE oh.inventory_onhand_id = @id";
                filterByInventoryId = true;
            }
            else
            {
                if (workcenterId != null && workcenterId != "")
                {
                    hasWhere = true;
                    filterByWorkcenter = true;
                    where += "WHERE l.workcenter_id = @workcenter_id";
                }
                if (locationId != null && locationId != "")
                {
                    filterByLocation = true;
                    if (hasWhere == true)
                    {
                        where += " AND oh.location_id = @location_id";
                    }
                    else
                    {
                        hasWhere = true;
                        where += "WHERE oh.location_id = @location_id";
                    }
                }
            }
            selectStmt += where;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            if (filterByInventoryId)
            {
                cmd.Parameters.AddWithValue("@id", inventory_id);
            }
            if (filterByWorkcenter)
            {
                cmd.Parameters.AddWithValue("@workcenter_id", workcenterId);
            }
            if (filterByLocation)
            {
                cmd.Parameters.AddWithValue("@location_id", locationId);
            }

            List<MasterTableRowModel> data = new List<MasterTableRowModel>();
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                MasterTableRowModel returnRow = new MasterTableRowModel()
                {
                    id = Convert.ToInt32(dr["inventory_onhand_id"]),
                    quantity = dr["qty"] != DBNull.Value ? (int?)Convert.ToInt32(dr["qty"]) : null,
                    shelf_life_date = dr["expiration_date"] != DBNull.Value ? Convert.ToDateTime(dr["expiration_date"]).ToString("MM/dd/yyyy") : null,
                    inv_date = dr["inventoried_date"] != DBNull.Value ? Convert.ToDateTime(dr["inventoried_date"]).ToString("MM/dd/yyyy") : null,
                    fsc = dr["fsc"] != DBNull.Value ? (int?)Convert.ToInt32(dr["fsc"]) : null,
                    niin = dr["niin"].ToString(),
                    wid = dr["wid"].ToString(),
                    location_name = dr["location_name"].ToString()
                };
                data.Add(returnRow);
            }

            conn.Close();

            return data;
        }

        public int updateInvItem(UpdateInvItemModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt;
            if (model.id != null)
            {
                //existing item in inventory details table, no need to add a new row
                selectStmt = "update inventory_detail set notes = @notes, ui_id = @ui, um = @um, slc_id = @slc, slac_id = @slac, allowance_quantity = @allowance_qty, fsc = @fsc, " +
                    "item_name = @name, spmig = @spmig, specs = @specs, smcc_id = @smcc, hcc_id = @hcc, usage_category_id = @usage_category, niin = @niin " +
                "where inventory_detail.inventory_detail_id = @id";
            }
            else
            {
                //"real" new item - if it's in the SMCL the values have been prepopulated so this will essentially just copy the SMCL data into another table
                selectStmt = "insert into inventory_detail values (@ui, @um, @notes, @slc, @slac, @hcc, @allowance_qty, @name, @spmig, @specs, @usage_category, @fsc, @niin, @smcc); select SCOPE_IDENTITY(); ";
            }
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", ((object)model.id) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@niin", ((object)model.niin) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@fsc", ((object)model.fsc) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@name", ((object)model.name) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@spmig", ((object)model.spmig) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@specs", ((object)model.specs) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@smcc", ((object)model.smcc) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@notes", ((object)model.notes) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@ui", ((object)model.ui) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@um", ((object)model.um) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@slc", ((object)model.slc) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@slac", ((object)model.slac) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@hcc", ((object)model.hcc) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@usage_category", ((object)model.usage_category) ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@allowance_qty", ((object)model.qtyAllow) ?? DBNull.Value);
            int id = Convert.ToInt32(cmd.ExecuteScalar());
            conn.Close();
            return id;
        }

        #region Offload
        public DataTable getFilteredOffloadCollection()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                @"SELECT         
                           dd.document_number                             AS document_number,                      
                           o.offload_list_id                              AS offload_list_id, 
                           o.offload_qty                                  AS quantity, 
                           o.cage                                         AS cage, 
                           o.ui                                           AS ui, 
                           o.um                                           AS um,
                           o.description                                  AS description, 
                           o.niin                                         AS niin, 
                           o.fsc                                          AS fsc
                    FROM   
                           offload_list o 
                           LEFT JOIN dd_1348 dd
                             ON (o.offload_list_id = dd.offload_list_id)

                    WHERE
                           o.offload_list_id=o.offload_list_id AND deleted = 0 and (o.archived_id is null or o.archived_id = '')
                   ORDER BY 
                           offload_list_id DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();

            return dt;
        }

        public List<ListItem> getShipToUic()
        {
            string selectStmt = @"SELECT uic from ships where uic is not null and uic <> ' '";
            return getPlainList(selectStmt);
        }

        public String getShipToName(String uic)
        {
            String name = " ";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt = @"SELECT name from ships where uic= @uic and name is not null and name <> ' '";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@uic", uic);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read()) { name = reader.GetString(0); }
            }

            conn.Close();
            return name;
        }

        public ShipDetailModel getShipToDetails(string uic)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            ShipDetailModel details = new ShipDetailModel();
            conn.Open();

            string selectStmt = @"select uic, name, address, POC_Name, POC_Telephone, POC_Email from ships where uic=@uic";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@uic", uic);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    details.Uic = !reader.IsDBNull(0) ? reader.GetString(0) : string.Empty;
                    details.ShipName = !reader.IsDBNull(1) ? reader.GetString(1) : string.Empty;
                    details.Address = !reader.IsDBNull(2) ? reader.GetString(2) : string.Empty;
                    details.Poc = !reader.IsDBNull(3) ? reader.GetString(3) : string.Empty;
                    details.Telephone = !reader.IsDBNull(4) ? reader.GetString(4) : string.Empty;
                    details.Email = !reader.IsDBNull(5) ? reader.GetString(5) : string.Empty;
                }
            }
            conn.Close();
            return details;
        }

        public ShipDetailModel getShipFromDetails(string name)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            ShipDetailModel details = new ShipDetailModel();
            conn.Open();

            string selectStmt = @"select uic, name, address, POC_Name, POC_Telephone, POC_Email from ships where name=@name";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@name", name);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    details.Uic = !reader.IsDBNull(0) ? reader.GetString(0) : string.Empty;
                    details.ShipName = !reader.IsDBNull(1) ? reader.GetString(1) : string.Empty;
                    details.Address = !reader.IsDBNull(2) ? reader.GetString(2) : string.Empty;
                    details.Poc = !reader.IsDBNull(3) ? reader.GetString(3) : string.Empty;
                    details.Telephone = !reader.IsDBNull(4) ? reader.GetString(4) : string.Empty;
                    details.Email = !reader.IsDBNull(5) ? reader.GetString(5) : string.Empty;
                }
            }
            conn.Close();
            return details;
        }

        public DataTable getOffloadItemForDeleteConfirm(int offload_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                @"SELECT 
					o.niin as niin,
					o.offload_qty as quantity,
					o.offload_list_id as offload_list_id
				  FROM 
                    offload_list o
				  WHERE 
                    o.offload_list_id = @offload_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@offload_id", offload_id);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();

            return dt;
        }

        public void deleteOffloadItem(int offload_id)
        {
            SqlConnection conn= new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            string selectStmt = "UPDATE offload_list set deleted = 1 where offload_list_id= @offload_list_id ";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@offload_list_id", offload_id);
            cmd.Transaction = tran;
            cmd.ExecuteNonQuery();
            tran.Commit();
            conn.Close();
        }

        public String getCurrentShipUic(string name)
        {

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            String uic = " ";
            conn.Open();

            string selectStmt = "select uic from ships where name = @name";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@name", name);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read()) {uic = reader.GetString(0);}
            }
            conn.Close();
            return uic;
        }

        public List<long> getOffloadItemIds()
        {
            List<long> ids = new List<long>();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt = @"SELECT offload_list_id FROM offload_list WHERE deleted = 0 AND archived_id is null 
                                  ORDER BY offload_list_id DESC";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read()) { ids.Add(reader.GetInt64(0)); }
            }
            conn.Close();
            return ids;
        }

        //TODO: Hook in MSDS when info is available
        public DataTable getOffloadItemDetails(long id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                @"SELECT     
                           dd.offload_list_id                             AS OffloadId,
                           concat(o.fsc,'-',o.niin)                       AS NSN,
                           o.cage                                         AS CAGE,  
                           o.description                                  AS ItemNomen,
                           l.name                                         AS Location,                    
                           o.offload_qty                                  AS Quant, 
						   i.qty										  AS OnhandQuant,
                           o.offload_date                                 AS DocDate,
                           o.ui                                           AS UnitIss,
                           o.um                                           AS UM,
						   dd.doc_ident									  AS Dociden,
						   dd.ri_from									  AS RIFrom,
						   dd.m_and_s									  AS MandS,
						   dd.ser										  AS SER,
						   dd.suppaddress								  AS SupAdd,
						   dd.sig										  AS Sig,
						   dd.fund										  AS Fund,
						   dd.distribution								  AS Distribution,
						   dd.project									  AS Project,
						   dd.pri										  AS Pri,
						   dd.reqd_del_date								  AS RecdDelDate,
						   dd.adv										  AS Adv,
						   dd.ri										  AS Ri,
						   dd.op										  AS OP,
						   dd.mgt										  AS MGT,
						   dd.condition									  AS Cond,
						   dd.unit_price								  AS UnitPrice,
						   dd.total_price								  AS TotalPrice,
						   dd.mark_for									  AS MarkFor,
						   dd.nmfc										  AS NMFC,
						   dd.frt_rate									  AS FRTRate,
						   dd.cargo_type								  AS TypeCargo,
						   dd.ps										  AS PS,
						   dd.qty_recd									  AS QtyRecd,
						   dd.up										  AS UP,
						   dd.unit_weight								  AS UnitWeight,
						   dd.unit_cube									  AS UnitCube,
						   dd.ufc										  AS UFC,
						   dd.sl										  AS SI,
						   dd.item_name									  AS FreightClassNomen,
						   dd.ty_cont									  AS TYCount,
						   dd.no_cont									  AS NOCount,
						   dd.total_weight								  AS TotalWeight,
						   dd.total_cube								  AS TotalCube,
						   dd.recd_by									  AS RecdBy,
						   dd.date_recd									  AS DateRecd,
                           dd.document_number							  AS DocNum,
						   dd.memo_add_1								  AS RIC,
						   dd.memo_add_2								  AS AddData1,
						   o.add_data_4                                   AS AddData4
      
                    FROM   
                           offload_list o 
                           LEFT JOIN locations l 
                             ON( o.location_id = l.location_id ) 
                           LEFT JOIN mfg_catalog mfg 
                             ON ( o.mfg_catalog_id = mfg.mfg_catalog_id ) 
                           LEFT JOIN niin_catalog nc 
                             ON ( mfg.niin = nc.niin ) 
						   LEFT JOIN inventory_onhand i
						     ON (o.mfg_catalog_id = i.mfg_catalog_id)
                           LEFT JOIN usage_category u 
                             ON ( u.usage_category_id = o.usage_category_id )
                           LEFT JOIN shelf_life_code slc
                             ON ( slc.shelf_life_code_id = o.slc_id )
                           LEFT JOIN shelf_life_action_code slac
                             ON ( slac.shelf_life_action_code_id = o.slac_id )
                           LEFT JOIN smcc
                             ON ( smcc.smcc_id = o.smcc_id )
						   LEFT JOIN dd_1348 dd
                             ON ( dd.offload_list_id = o.offload_list_id )";
                           
            string whereStmt = "where o.deleted = 0 and o.archived_id IS null ";
            if (id != 0)
            {
                whereStmt = " where  o.offload_list_id=@id"
                            + " AND o.deleted = 0 AND o.archived_id IS NULL ";
            }
            selectStmt += whereStmt + "ORDER BY dd.offload_list_id desc";
            
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", id);

            DataTable details = new DataTable();
            new SqlDataAdapter(cmd).Fill(details);
            conn.Close();

            return details;
        }

        public int updateOffloadItem(long id, List<string> details)
        {
            SqlConnection conn= new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            string selectStmt = @"IF EXISTS(SELECT 'True' FROM dd_1348 WHERE offload_list_id = @offload_list_id)
                                    update dd_1348 set document_number = @docNumber,
                                                    doc_ident = @docIdent,
                                                    ri_from = @riFrom,
                                                    m_and_s = @mAndS, 
                                                    ser = @SER,
                                                    suppaddress = @suppaddress, 
                                                    sig = @SIG, 
                                                    fund = @fund, 
                                                    distribution = @dist, 
                                                    project = @project, 
                                                    pri = @PRI, 
                                                    reqd_del_date = @reqdDelDate,   
                                                    adv = @ADV, 
                                                    ri = @RI, 
                                                    op = @OP, 
                                                    mgt = @MGT, 
                                                    condition = @condition, 
                                                    unit_price = @unitPrice, 
                                                    total_price = @totalPrice, 
                                                    mark_for = @markFor, 
                                                    nmfc = @NMFC, 
                                                    frt_rate = @frtRate, 
                                                    cargo_type = @cargoType, 
                                                    ps = @PS, 
                                                    qty_recd = @qtyRecd, 
                                                    up = @UP,
                                                    unit_weight = @unitWeight,  
                                                    unit_cube = @unitCube, 
                                                    ufc = @UFC, 
                                                    sl = @SL, 
                                                    item_name = @itemName, 
                                                    ty_cont = @tyCont,
                                                    no_cont = @noCont,
                                                    total_weight = @totalWeight, 
                                                    total_cube = @totalCube, 
                                                    recd_by = @recdBy, 
                                                    date_recd = @dateRecd, 
                                                    memo_add_1 = @memoAdd1, 
                                                    memo_add_2 = @memoAdd2
                                    where offload_list_id = @offload_list_id
                                ELSE
                                    INSERT INTO dd_1348 VALUES(
                                                    @offload_list_id,
                                                    NULL,
                                                    @docNumber,
                                                    @docIdent,
                                                    @riFrom,
                                                    @mAndS, 
                                                    @SER,
                                                    @suppaddress, 
                                                    @SIG, 
                                                    @fund, 
                                                    @dist, 
                                                    @project, 
                                                    @PRI, 
                                                    @reqdDelDate,   
                                                    @ADV, 
                                                    @RI, 
                                                    @OP, 
                                                    @MGT, 
                                                    @condition, 
                                                    @unitPrice,
                                                    @totalPrice,
                                                    NULL,
                                                    NULL,
                                                    @markFor, 
                                                    @NMFC, 
                                                    @frtRate, 
                                                    @cargoType, 
                                                    @PS, 
                                                    @qtyRecd, 
                                                    @UP,
                                                    @unitWeight,  
                                                    @unitCube, 
                                                    @UFC, 
                                                    @SL, 
                                                    @itemName, 
                                                    @tyCont,
                                                    @noCont,
                                                    @totalWeight, 
                                                    @totalCube, 
                                                    @recdBy, 
                                                    @dateRecd, 
                                                    NULL,
                                                    @memoAdd1, 
                                                    @memoAdd2)

                          UPDATE offload_list set offload_qty = @offQuantity,
                                                  cage = @cage,
                                                  description = @description,
                                                  ui = @ui,
                                                  add_data_4 = @addData4
                                    where offload_list_id = @offload_list_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@offload_list_id", id);
            cmd.Parameters.AddWithValue("@docNumber", details[0]);
            cmd.Parameters.AddWithValue("@docIdent", details[1]);
            cmd.Parameters.AddWithValue("@riFrom", details[2]);
            cmd.Parameters.AddWithValue("@mAndS", details[3]);
            cmd.Parameters.AddWithValue("@SER", details[4]);
            cmd.Parameters.AddWithValue("@suppaddress", details[5]);
            cmd.Parameters.AddWithValue("@SIG", details[6]);
            cmd.Parameters.AddWithValue("@fund", details[7]);
            cmd.Parameters.AddWithValue("@dist", details[8]);
            cmd.Parameters.AddWithValue("@project", details[9]);
            cmd.Parameters.AddWithValue("@PRI", details[10]);
            cmd.Parameters.AddWithValue("@reqdDelDate", details[11]);
            cmd.Parameters.AddWithValue("@ADV", details[12]);
            cmd.Parameters.AddWithValue("@RI", details[13]);
            cmd.Parameters.AddWithValue("@OP", details[14]);
            cmd.Parameters.AddWithValue("@MGT", details[15]);
            cmd.Parameters.AddWithValue("@condition", details[16]);
            cmd.Parameters.AddWithValue("@unitPrice", details[17]);
            cmd.Parameters.AddWithValue("@totalPrice", details[18]);
            cmd.Parameters.AddWithValue("@markFor", details[19]);
            cmd.Parameters.AddWithValue("@NMFC", details[20]);
            cmd.Parameters.AddWithValue("@frtRate", details[21]);
            cmd.Parameters.AddWithValue("@cargoType", details[22]);
            cmd.Parameters.AddWithValue("@PS", details[23]);
            cmd.Parameters.AddWithValue("@qtyRecd", details[24]);
            cmd.Parameters.AddWithValue("@UP", details[25]);
            cmd.Parameters.AddWithValue("@unitWeight", details[26]);
            cmd.Parameters.AddWithValue("@unitCube", details[27]);
            cmd.Parameters.AddWithValue("@UFC", details[28]);
            cmd.Parameters.AddWithValue("@SL", details[29]);
            cmd.Parameters.AddWithValue("@itemName", details[30]);
            cmd.Parameters.AddWithValue("@tyCont", details[31]);
            cmd.Parameters.AddWithValue("@noCont", details[32]);
            cmd.Parameters.AddWithValue("@totalWeight", details[33]);
            cmd.Parameters.AddWithValue("@totalCube", details[34]);
            cmd.Parameters.AddWithValue("@recdBy", details[35]);
            cmd.Parameters.AddWithValue("@dateRecd", details[36]);
            cmd.Parameters.AddWithValue("@memoAdd1", details[37]);
            cmd.Parameters.AddWithValue("@memoAdd2", details[38]);
            cmd.Parameters.AddWithValue("@cage", details[39]);
            cmd.Parameters.AddWithValue("@description", details[40]);
            cmd.Parameters.AddWithValue("@ui", details[41]);
            cmd.Parameters.AddWithValue("@um", details[42]);
            cmd.Parameters.AddWithValue("@addData4", details[43]);
            cmd.Parameters.AddWithValue("@offQuantity", details[44]);
            cmd.Transaction = tran;
            cmd.ExecuteNonQuery();
            tran.Commit();
            conn.Close();

            return 1;
        }

        public DataTable getInventoryItemForOffloadConfirm(int inventory_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"select 
                    id.fsc as fsc,
                    id.niin as niin,
                    id.item_name as name,
                    i.qty as total,
                    i.inventory_onhand_id as id,
                    u.abbreviation as ui,
                    id.um as um,
	                i.cage as cage

                  from 
                      inventory_onhand i 
					  LEFT JOIN inventory_detail id 
                             ON ( id.inventory_detail_id = i.inventory_detail_id ) 
                      LEFT JOIN ui u
                             ON (id.ui_id = u.ui_id)
                  where i.inventory_onhand_id = " + inventory_id;

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();

            return dt;
        }

        public int AddOffloadItem(int inventory_id, int quantity)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            int result;
            string sql = @" DECLARE
                            @offloadId bigint;
                            BEGIN
                            INSERT INTO offload_list (shelf_life_expiration_date, location_id,
						                              mfg_catalog_id, offload_qty,
						                              deleted, offload_date, slc_id, 
						                              slac_id, hcc_id, usage_category_id, 
													  smcc_id, fsc, niin, cage, description, um)

                            SELECT io.expiration_date, io.location_id, io.mfg_catalog_id, @quantity,
	                                0, GETDATE(), id.slc_id, id.slac_id, id.hcc_id, id.usage_category_id,
	                               id.smcc_id, id.fsc, id.niin, 0, id.item_name, id.um

                            FROM   inventory_onhand io
	                               LEFT JOIN inventory_detail id
	                               ON (io.inventory_detail_id = id.inventory_detail_id)

                            WHERE  io.inventory_onhand_id = @inventory_id

                            SELECT @offloadId = scope_identity()

							UPDATE offload_list SET add_data_4 = concat('SLC: ', slc.slc, '-', 
							slc.description, '|SLAC: ', slac.slac, '-', slac.description, 
                            '|SMCC: ', smcc.smcc,'-',smcc.description, '|HCC: ', h.hcc,'-', h.description)
							FROM 
							offload_list o
							LEFT JOIN usage_category u 
								ON ( u.usage_category_id = o.usage_category_id )
                            LEFT JOIN shelf_life_code slc
                                ON ( slc.shelf_life_code_id = o.slc_id )
                            LEFT JOIN shelf_life_action_code slac
                                ON ( slac.shelf_life_action_code_id = o.slac_id )
                            LEFT JOIN smcc
                                ON ( smcc.smcc_id = o.smcc_id )	
                            LEFT JOIN hcc h
                                ON ( h.hcc_id = o.hcc_id )												 
                            WHERE o.offload_list_id = @offloadId
                            END	

                            UPDATE inventory_onhand SET qty = qty-@quantity WHERE inventory_onhand_id = @inventory_id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@quantity",quantity);
            cmd.Parameters.AddWithValue("@inventory_id", inventory_id);

            result = Convert.ToInt32(cmd.ExecuteScalar());
            return result;
        }

        public int AddUniqueOffloadItem(List<string> details)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            string selectStmt =
                @"INSERT INTO offload_list( offload_qty,  deleted, offload_date, fsc, niin, cage, description, ui, add_data_4)
                                       VALUES ( @quantity, 0, @offloadDate, @fsc, @niin, @cage, @desc, @ui, @addData4)

                  UPDATE dd_1348 SET document_number = @docNumber,
                                     doc_ident = @docIdent,
                                     ri_from = @riFrom,
                                     m_and_s = @mAndS, 
                                     ser = @SER,
                                     suppaddress = @suppaddress, 
                                     sig = @SIG, 
                                     fund = @fund, 
                                     distribution = @dist, 
                                     project = @project, 
                                     pri = @PRI, 
                                     reqd_del_date = @reqdDelDate,   
                                     adv = @ADV, 
                                     ri = @RI, 
                                     op = @OP, 
                                     mgt = @MGT, 
                                     condition = @condition, 
                                     unit_price = @unitPrice, 
                                     total_price = @totalPrice, 
                                     mark_for = @markFor, 
                                     nmfc = @NMFC, 
                                     frt_rate = @frtRate, 
                                     cargo_type = @cargoType, 
                                     ps = @PS, 
                                     qty_recd = @qtyRecd, 
                                     up = @UP,
                                     unit_weight = @unitWeight,  
                                     unit_cube = @unitCube, 
                                     ufc = @UFC, 
                                     sl = @SL, 
                                     item_name = @itemName, 
                                     ty_cont = @tyCont,
                                     no_cont = @noCont,
                                     total_weight = @totalWeight, 
                                     total_cube = @totalCube, 
                                     recd_by = @recdBy, 
                                     date_recd = @dateRecd, 
                                     memo_add_1 = @memoAdd1, 
                                     memo_add_2 = @memoAdd2
                  WHERE offload_list_id = IDENT_CURRENT('offload_list')";                                                                                  

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@fsc", details[0]);
            cmd.Parameters.AddWithValue("@niin", details[1]);
            cmd.Parameters.AddWithValue("@cage", details[2]);
            cmd.Parameters.AddWithValue("@desc", details[3]);
            cmd.Parameters.AddWithValue("@quantity", details[4]);
            cmd.Parameters.AddWithValue("@ui", details[5]);
            cmd.Parameters.AddWithValue("@addData4", details[6]);
            cmd.Parameters.AddWithValue("@um", details[7]);
            cmd.Parameters.AddWithValue("@docNumber", details[8]);
            cmd.Parameters.AddWithValue("@docIdent", details[9]);
            cmd.Parameters.AddWithValue("@riFrom", details[10]);
            cmd.Parameters.AddWithValue("@mAndS", details[11]);
            cmd.Parameters.AddWithValue("@SER", details[12]);
            cmd.Parameters.AddWithValue("@suppaddress", details[13]);
            cmd.Parameters.AddWithValue("@SIG", details[14]);
            cmd.Parameters.AddWithValue("@fund", details[15]);
            cmd.Parameters.AddWithValue("@dist", details[16]);
            cmd.Parameters.AddWithValue("@project", details[17]);
            cmd.Parameters.AddWithValue("@PRI", details[18]);
            cmd.Parameters.AddWithValue("@reqdDelDate", details[19]);
            cmd.Parameters.AddWithValue("@ADV", details[20]);
            cmd.Parameters.AddWithValue("@RI", details[21]);
            cmd.Parameters.AddWithValue("@OP", details[22]);
            cmd.Parameters.AddWithValue("@MGT", details[23]);
            cmd.Parameters.AddWithValue("@condition", details[24]);
            cmd.Parameters.AddWithValue("@unitPrice", details[25]);
            cmd.Parameters.AddWithValue("@totalPrice", details[26]);
            cmd.Parameters.AddWithValue("@markFor", details[27]);
            cmd.Parameters.AddWithValue("@NMFC", details[28]);
            cmd.Parameters.AddWithValue("@frtRate", details[29]);
            cmd.Parameters.AddWithValue("@cargoType", details[30]);
            cmd.Parameters.AddWithValue("@PS", details[31]);
            cmd.Parameters.AddWithValue("@qtyRecd", details[32]);
            cmd.Parameters.AddWithValue("@UP", details[33]);
            cmd.Parameters.AddWithValue("@unitWeight", details[34]);
            cmd.Parameters.AddWithValue("@unitCube", details[35]);
            cmd.Parameters.AddWithValue("@UFC", details[36]);
            cmd.Parameters.AddWithValue("@SL", details[37]);
            cmd.Parameters.AddWithValue("@itemName", details[38]);
            cmd.Parameters.AddWithValue("@tyCont", details[39]);
            cmd.Parameters.AddWithValue("@noCont", details[40]);
            cmd.Parameters.AddWithValue("@totalWeight", details[41]);
            cmd.Parameters.AddWithValue("@totalCube", details[42]);
            cmd.Parameters.AddWithValue("@recdBy", details[43]);
            cmd.Parameters.AddWithValue("@dateRecd", details[44]);
            cmd.Parameters.AddWithValue("@memoAdd1", details[45]);
            cmd.Parameters.AddWithValue("@memoAdd2", details[46]);
            cmd.Parameters.AddWithValue("@offloadDate", details[47]);
            cmd.Transaction = tran;
            cmd.ExecuteNonQuery();
            tran.Commit();
            conn.Close();

            return 1;
        }

        public string getSerial()
        {
            string serial = " ";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"select next value for dbo.Serial as serial";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if(reader.Read()) { serial = reader.GetInt64(0).ToString(); }
            }
            conn.Close();
            return serial;
        }
        #endregion

        #region OffloadArchived
        public DataTable getFilteredOffloadArchivedCollection()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                @"
                   SELECT
                           o.archived_id                                  AS archived_id,
                           dd.document_number                             AS document_number,
						   o.offload_qty                                  AS quantity,
                           CONVERT(varchar(10), o.offload_date, 110)                           AS offload_date,
					       o.cage                                        AS cage,
					       o.ui                                          AS ui,
						   o.um                                          AS um,
						   o.description                                 AS description,
						   o.niin                                        AS niin,
					       o.fsc                                         AS fsc
            		FROM 
                           archived a					   
						   INNER JOIN offload_list o 
                             ON(o.archived_id = a.archived_id)
						   INNER JOIN dd_1348 dd 
                             ON(o.offload_list_id = dd.offload_list_id)
				   WHERE 
                           deleted = 0 and o.archived_id is not null
                   ORDER BY 
                           offload_date DESC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();

            return dt;
        }

        public void AddOffloadArchivedItem()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            string sql = @"INSERT INTO archived (date_archived, offload_list_id)
                                SELECT GETDATE(), 
                                       o.offload_list_id 
                                  FROM offload_list o 
                             LEFT JOIN dd_1348 dd 
                                    ON (o.offload_list_id = dd.offload_list_id)
                                 WHERE dd.document_number <> ' '
                                   AND dd.document_number IS NOT NULL
                                   AND o.deleted = 0
                                   AND o.archived_id IS NULL; UPDATE offload_list SET archived_id = a.archived_id from offload_list as o inner join archived as a on a.offload_list_id = o.offload_list_id";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Transaction = tran;
            cmd.ExecuteNonQuery();
            tran.Commit();
            conn.Close();
        }

        public List<long> getOffloadArchivedIds()
        {
            List<long> ids = new List<long>();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt = @"SELECT archived_id FROM archived
                                  ORDER BY archived_id DESC";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read()) { ids.Add(reader.GetInt64(0)); }
            }
            conn.Close();
            return ids;            
        }

        public DataTable GetBlankSfrModal()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select hull_number, uic, POC_Name, POC_Telephone from ships where current_ship = 1";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            new SqlDataAdapter(cmd).Fill(dt);
            cmd.Dispose();
            return dt;
        }

        //TODO: Hook in MSDS when info is available
        public DataTable getOffloadArchivedItemDetails(long id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                @"SELECT     
                           o.archived_id                                  AS ArchivedId,
                           concat(o.fsc,'-',o.niin)                       AS NSN,
                           o.cage                                        AS CAGE,  
                           o.description                                 AS ItemNomen,
                           l.name                                         AS Location,                    
                           o.offload_qty                                  AS Quant, 
						   i.qty										  AS OnhandQuant,
                           o.offload_date                                 AS DocDate,
                           o.ui                                          AS UnitIss,
                           o.um                                          AS UM,
						   dd.doc_ident									  AS Dociden,
						   dd.ri_from									  AS RIFrom,
						   dd.m_and_s									  AS MandS,
						   dd.ser										  AS SER,
						   dd.suppaddress								  AS SupAdd,
						   dd.sig										  AS Sig,
						   dd.fund										  AS Fund,
						   dd.distribution								  AS Distribution,
						   dd.project									  AS Project,
						   dd.pri										  AS Pri,
						   dd.reqd_del_date								  AS RecdDelDate,
						   dd.adv										  AS Adv,
						   dd.ri										  AS Ri,
						   dd.op										  AS OP,
						   dd.mgt										  AS MGT,
						   dd.condition									  AS Cond,
						   dd.unit_price								  AS UnitPrice,
						   dd.total_price								  AS TotalPrice,
						   dd.mark_for									  AS MarkFor,
						   dd.nmfc										  AS NMFC,
						   dd.frt_rate									  AS FRTRate,
						   dd.cargo_type								  AS TypeCargo,
						   dd.ps										  AS PS,
						   dd.qty_recd									  AS QtyRecd,
						   dd.up										  AS UP,
						   dd.unit_weight								  AS UnitWeight,
						   dd.unit_cube									  AS UnitCube,
						   dd.ufc										  AS UFC,
						   dd.sl										  AS SI,
						   dd.item_name									  AS FreightClassNomen,
						   dd.ty_cont									  AS TYCount,
						   dd.no_cont									  AS NOCount,
						   dd.total_weight								  AS TotalWeight,
						   dd.total_cube								  AS TotalCube,
						   dd.recd_by									  AS RecdBy,
						   dd.date_recd									  AS DateRecd,
                           dd.document_number							  AS DocNum,
						   dd.memo_add_1								  AS RIC,
						   dd.memo_add_2								  AS AddData1,
						   o.add_data_4                                  AS AddData4    
                    FROM   
                           offload_list o 
                           LEFT JOIN locations l 
                             ON( o.location_id = l.location_id )                        
						   LEFT JOIN inventory_onhand i
						     ON (o.mfg_catalog_id = i.mfg_catalog_id)
						   LEFT JOIN dd_1348 dd
                             ON ( dd.offload_list_id = o.offload_list_id )";
            string whereStmt = "where deleted = 0 and o.archived_id IS NOT null  and o.archived_id <> ''";
            if (id != 0)
            {
                whereStmt = " where  o.archived_id=@id"
                            + " AND deleted = 0";
            }
            selectStmt += whereStmt + "ORDER BY o.archived_id desc";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", id);

            DataTable details = new DataTable();
            new SqlDataAdapter(cmd).Fill(details);
            conn.Close();

            return details;
        }
        #endregion

        #region Reports

        public DataTable getInventoryLocations()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"Select distinct i.location_id as id, 
                                                         l.name as name
                                               from locations l, 
                                             inventory_onhand i 
                                            where i.location_id = l.location_id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public string GetUiById(int? id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = "select description from ui where ui_id = @ui_id ";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@ui_id", (object)id ?? DBNull.Value);
            var value = cmd.ExecuteScalar();
            cmd.Dispose();
            conn.Close();
            if (value != null) return value.ToString(); else return "";
        }

        public DataTable getInventoryWorkcenters()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"select distinct w.wid as id from workcenter w 
                                  inner join locations l on l.workcenter_id = w.workcenter_id
                                  inner join inventory_onhand i on i.location_id = l.location_id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable getInventoryDetailsForReport(string whereStatement)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"SELECT 
                                         i.fsc              AS FSC, 
                                        i.niin              AS NIIN,
                                        m.cage              AS CAGE,
                                   i.item_name              AS ITEM_NAME,
                             unit.abbreviation              AS UNIT_OF_ISSUE,
                                          i.um              AS UNIT_OF_MEASURE,
                                        l.name              AS LOCATION,                                
                                        w.wid               AS WCENTER,
                                        oh.qty              AS ON_HAND_QTY,
 CONVERT(varchar(10), oh.expiration_date, 110)              AS SHELF_LIFE_EXP_DATE,
									 smcc.smcc				AS SMCC, 
                              smcc.description              AS SMCC_DESC,
									   hcc.hcc				AS HCC,
                               hcc.description              AS HCC_DESC,
							   slc.description				AS SHELF_LIFE_CODE,
								m.manufacturer              AS MFG,
			  CONCAT(n.remarks,' - ', i.notes)				AS REMARKS

                    from inventory_onhand oh left join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id 
                                             left join mfg_catalog m on m.mfg_catalog_id = oh.mfg_catalog_id
                                             left join locations l on l.location_id = oh.location_id
                                             left join workcenter w on w.workcenter_id = l.workcenter_id
                                             left join usage_category u on u.usage_category_id = i.usage_category_id
                                             left join ui unit on i.ui_id = unit.ui_id 
											 left join smcc smcc on i.smcc_id = smcc.smcc_id
											 left join shelf_life_code slc on i.slc_id = slc.shelf_life_code_id
											 left join shelf_life_action_code slac on i.slac_id = slac.shelf_life_action_code_id
											 left join hcc hcc	on i.hcc_id = hcc.hcc_id
											 left join niin_catalog n on i.niin = n.niin";

            selectStmt += whereStatement;
            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getAcmDetailsForReport(string whereStatement)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                @"SELECT 
                                    u.category              AS ACM,
                                         i.fsc              AS FSC, 
                                        i.niin              AS NIIN,
                                        m.cage              AS CAGE,
                                   i.item_name              AS ITEM_NAME,
                             unit.abbreviation              AS UNIT_OF_ISSUE,
                                        l.name              AS LOCATION,
                                 w.description              AS WORKCENTER,
                          i.allowance_quantity              AS ALLOWANCE_QTY,
                                        oh.qty              AS ON_HAND_QTY,
    CONVERT(varchar(10), oh.onboard_date, 110)              AS ONBOARD_DATE,
									 smcc.smcc				AS SMCC, 
                              smcc.description              AS SMCC_DESC,
									   hcc.hcc				AS HCC,
                               hcc.description              AS HCC_DESC,
							   slc.description				AS SHELF_LIFE_CODE,
								m.manufacturer              AS MFG,
			  CONCAT(n.remarks,' - ', i.notes)				AS REMARKS

                    from inventory_onhand oh left join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id 
                                             left join mfg_catalog m on m.mfg_catalog_id = oh.mfg_catalog_id
                                             left join locations l on l.location_id = oh.location_id
                                             left join workcenter w on w.workcenter_id = l.workcenter_id
                                             left join usage_category u on u.usage_category_id = i.usage_category_id
                                             left join ui unit on i.ui_id = unit.ui_id 
											 left join smcc smcc on i.smcc_id = smcc.smcc_id
											 left join shelf_life_code slc on i.slc_id = slc.shelf_life_code_id
											 left join shelf_life_action_code slac on i.slac_id = slac.shelf_life_action_code_id
											 left join hcc hcc	on i.hcc_id = hcc.hcc_id
											 left join niin_catalog n on i.niin = n.niin";

            selectStmt += whereStatement;
            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getExpirationDetailsForReport(string whereStatement)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                    @"SELECT 
                                         i.fsc              AS FSC, 
                                        i.niin              AS NIIN,
                                        m.cage              AS CAGE,
                                   i.item_name              AS ITEM_NAME,
                             unit.abbreviation              AS UNIT_OF_ISSUE,
							              i.um              AS UNIT_OF_MEASURE,
								 w.description              AS WORKCENTER,
                                        l.name              AS LOCATION,
                                        oh.qty              AS ON_HAND_QTY,
 CONVERT(varchar(10), oh.expiration_date, 110)              AS EXPIRATION_DATE,
									 slac.slac				AS SLAC,
									 smcc.smcc				AS SMCC, 
                              smcc.description              AS SMCC_DESC,
									   hcc.hcc				AS HCC,
                               hcc.description              AS HCC_DESC,
							   slc.description				AS SHELF_LIFE_CODE,
								m.manufacturer              AS MFG,
			  CONCAT(n.remarks,' - ', i.notes)				AS REMARKS

                    from inventory_onhand oh left join inventory_detail i on i.inventory_detail_id = oh.inventory_detail_id 
                                             left join mfg_catalog m on m.mfg_catalog_id = oh.mfg_catalog_id
                                             left join locations l on l.location_id = oh.location_id
                                             left join workcenter w on w.workcenter_id = l.workcenter_id
                                             left join usage_category u on u.usage_category_id = i.usage_category_id
                                             left join ui unit on i.ui_id = unit.ui_id 
											 left join smcc smcc on i.smcc_id = smcc.smcc_id
											 left join shelf_life_code slc on i.slc_id = slc.shelf_life_code_id
											 left join shelf_life_action_code slac on i.slac_id = slac.shelf_life_action_code_id
											 left join hcc hcc	on i.hcc_id = hcc.hcc_id
											 left join niin_catalog n on i.niin = n.niin";

            selectStmt += whereStatement;
            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public List<ListItem> getWorkcenters()
        {
            string selectStmt = "select workcenter_id from workcenter order by workcenter_id asc";
            return getPlainList(selectStmt);
        }

        public List<ListItem> getLocationIds()
        {
            string selectStmt = "select location_id from locations order by location_id asc";
            return getPlainList(selectStmt);
        }

        public DataTable getLocationsByWorkcenterId(long id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                    @"SELECT w.wid, l.name 
                        FROM workcenter w 
                   LEFT JOIN locations l 
                          ON (w.workcenter_id = l.workcenter_id)
                       WHERE w.workcenter_id = @w_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@w_id", id);
            

            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getWorkcenterByLocationId(long id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string selectStmt =
                    @"SELECT l.name, w.wid, w.workcenter_id 
                        FROM locations l 
                   LEFT JOIN workcenter w 
                          ON (w.workcenter_id = l.workcenter_id)
                       WHERE l.location_id = @l_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@l_id", id);


            DataTable dt = new DataTable();

            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public string getLocationName(string id)
        {
            string name = " ";
            long lid = Convert.ToInt64(id);

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"select name from locations where location_id = @id";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@id", lid);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read()){ name = !reader.IsDBNull(0) ? reader.GetString(0) : string.Empty; }
            }
            conn.Close();
            return name;        
        }

        public string getWorkcenterDescription(string wid)
        {
            string desc = " ";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            string selectStmt = @"select description from workcenter where wid = @wid";
            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@wid", wid);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read()) { desc = !reader.IsDBNull(0) ? reader.GetString(0) : string.Empty; }
            }
            conn.Close();
            return desc;
        }

        public DataTable getIncompatibleLocations(string whereStatment)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT l.location_id, count(l.location_id) AS locationCount 
                      FROM hcc h
                      INNER JOIN inventory_detail id 
	                        ON (id.hcc_id = h.hcc_id)
                      INNER JOIN inventory_onhand io
	                        ON (io.inventory_detail_id = id.inventory_detail_id)
                      INNER JOIN locations l
	                        ON (l.location_id = io.location_id)
                       LEFT JOIN workcenter w
                            ON (l.workcenter_id = w.workcenter_id)
                      WHERE io.qty > 0";                   

            selectStmt += whereStatment;
            selectStmt += @" GROUP BY l.location_id
                            HAVING count(l.location_id) >1";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getItemsWithHcc(long location)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT distinct id.inventory_detail_id
		                FROM inventory_detail id
		                LEFT JOIN inventory_onhand io
			                 ON(id.inventory_detail_id = io.inventory_detail_id)
		                LEFT JOIN locations l
			                 ON(io.location_id = l.location_id)
		               INNER JOIN hcc h
			                 ON(id.hcc_id = h.hcc_id)
		               WHERE io.qty > 0
		                 AND l.location_id = @l_id
		               ORDER BY id.inventory_detail_id ASC";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@l_id", location);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getNonHccLocations()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT DISTINCT l.location_id , l.name 
                        FROM locations l
                  INNER JOIN inventory_onhand oh
                          ON (oh.location_id = l.location_id)
                  INNER JOIN inventory_detail id
                          ON (oh.inventory_detail_id = id.inventory_detail_id)
                       WHERE id.hcc_id IS NULL
                         AND oh.qty > 0";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getItemsWithoutHcc(long location)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT id.niin, id.item_name, oh.qty
                        FROM inventory_detail id
                   LEFT JOIN inventory_onhand oh
                          ON (oh.inventory_detail_id = id.inventory_detail_id)
                       WHERE id.hcc_id is null
                         AND oh.location_id = @location
                         AND oh.qty > 0";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@location", location);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getIncompatiblesForReport(long itemId, long location)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT DISTINCT id1.niin, id1.item_name, io1.qty, h1.hcc, id2.niin, id2.item_name, io2.qty, h2.hcc, hw.warning_level
					   FROM inventory_detail id1
				  LEFT JOIN inventory_onhand io1
						 ON (id1.inventory_detail_id = io1.inventory_detail_id)
				  LEFT JOIN hcc h1
						 ON (h1.hcc_id = id1.hcc_id)
				  LEFT JOIN inventory_onhand io2
				         ON io2.location_id = io1.location_id
				  LEFT JOIN inventory_detail id2
				         ON id2.inventory_detail_id = io2.inventory_detail_id
				  LEFT JOIN hcc h2 
				         ON h2.hcc_id = id2.hcc_id
				  LEFT JOIN hazard_warnings hw
						 ON ((hw.hcc_id_1 = h1.hcc_id and hw.hcc_id_2 = h2.hcc_id) or (hw.hcc_id_1 = h2.hcc_id and hw.hcc_id_2 = h1.hcc_id))
					  WHERE id1.inventory_detail_id = @itemId
						AND id1.niin <> id2.niin
						AND h1.hcc is not null AND h2.hcc is not null 
						AND id2.inventory_detail_id > id1.inventory_detail_id
						AND io1.qty >0 and io2.qty >0
						AND io1.location_id = @location";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@itemId", itemId);
            cmd.Parameters.AddWithValue("@location", location);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getWorkcentersInUse(string workcenter_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT DISTINCT w.*
                        FROM workcenter w
                  INNER JOIN locations l
                          ON (w.workcenter_id = l.workcenter_id)
                  INNER JOIN inventory_onhand io
                          ON (io.location_id = l.location_id)";

            if (workcenter_id != "0")
            {
                selectStmt += "WHERE w.workcenter_id = "+workcenter_id;
            }

            SqlCommand cmd = new SqlCommand(selectStmt, conn);


            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getLocationsInUse(long workcenter_id, string location_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT DISTINCT l.*
                        FROM locations l
                  INNER JOIN inventory_onhand io
                          ON (io.location_id = l.location_id)
                       WHERE l.workcenter_id = @w_id";

            if (location_id != "0")
            {
                selectStmt += "AND l.location_id = " + location_id;
            }

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@w_id", workcenter_id);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }

        public DataTable getInventoryFromLocationsInUse(long location_id)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"SELECT id.fsc, id.niin, id.item_name
                        FROM inventory_detail id
                   LEFT JOIN inventory_onhand io
                          ON (id.inventory_detail_id = io.inventory_detail_id)
                   LEFT JOIN locations l
                          ON (io.location_id = l.location_id)
                       WHERE l.location_id = @l_id";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@l_id", location_id);

            DataTable dt = new DataTable();
            new SqlDataAdapter(cmd).Fill(dt);

            conn.Close();

            return dt;
        }
        #endregion

        public void AddCageToMfgCatalog(NewCageModel model)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"INSERT INTO mfg_catalog (niin, cage, manufacturer) values (@niin, @cage, @manufacturer)";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            cmd.Parameters.AddWithValue("@niin", model.data.niin);
            cmd.Parameters.AddWithValue("@cage", model.data.cage);
            cmd.Parameters.AddWithValue("@manufacturer", model.data.manufacturer);

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public void addImportedMSDS(string msdsStatements)
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlCommand cmd = new SqlCommand(msdsStatements, conn);
            cmd.CommandTimeout = 0;

            cmd.ExecuteNonQuery();

            conn.Close();
        }

        public List<string> getMSDSFileName(string fsc, string niin, string cage)
        {
            List<string> fileNames = new List<string>();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    "SELECT file_name "+
                        "FROM msds "+
                       "WHERE fsc= "+fsc+" AND niin ='"+niin+"'";

            if (cage != "NULL" && cage != "null" && cage != "Null" && cage != ""){ selectStmt += " AND cage = '" + cage +"'";}

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            //cmd.Parameters.AddWithValue("@fsc", fsc);
            //cmd.Parameters.AddWithValue("@niin", fsc);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read()) {
                    if (reader.GetString(0) != "NULL" && reader.GetString(0) != "null"){ fileNames.Add("resources/MSDS/" + reader.GetString(0)); }
                }
            }

            conn.Close();

            return fileNames;
        }

        public DataTable GetBackupSettings()
        {
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string selectStmt =
                    @"select backup_location, backup_frequency, CONVERT(VARCHAR(10),last_backup_date,10) as last_backup_date from config";

            SqlCommand cmd = new SqlCommand(selectStmt, conn);
            DataTable config = new DataTable();
            new SqlDataAdapter(cmd).Fill(config);
            conn.Close();
            return config;
        }

        public void RestoreBackup(string file, string backupLocation)
        {
            backupLocation.Replace('\\', '/');
            string sql = @"use master; alter database SHIMS2 SET SINGLE_USER with rollback immediate RESTORE DATABASE [SHIMS2] FROM  DISK = N'"+backupLocation+"/"+file+"' WITH REPLACE";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void UpdateBackupFrequency(int backupFrequency)
        {
            string sql = "update config set backup_frequency = @frequency where config_id = 1";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@frequency", backupFrequency);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        #region File Tables
        public bool updateFileTable(FILE_TABLES fileTable, String fileName, String fileType,
            DateTime processedDate, String username,
            int records_added) {

            return updateFileTable(fileTable, fileName, fileType,
                processedDate, username, 
                records_added, 0, 0);
        }
        public bool updateFileTable(FILE_TABLES fileTable, String fileName, String fileType,
            DateTime processedDate, String username,
            int records_added, int records_changed, int records_deleted) {
            bool result = false;
            string sqlStmt = "INSERT INTO file_uploads (" +
                "file_name, file_type, upload_date, uploaded_by, " +
                "records_added, records_changed, records_deleted) " +
                "VALUES(@file_name, @file_type,@process_date, @processed_by, " +
                "@records_added, @records_changed, @records_deleted)";

            if (fileTable == FILE_TABLES.FileDownLoad)
                sqlStmt = "INSERT INTO file_downloads (" +
                    "file_name, file_type, download_date, downloaded_by, " +
                    "records_downloaded) " +
                    "VALUES(@file_name, @file_type, @process_date, @processed_by, " +
                    "@records_added)";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@file_name", fileName);
                insertCmd.Parameters.AddWithValue("@file_type", fileType);
                insertCmd.Parameters.AddWithValue("@process_date", processedDate);
                insertCmd.Parameters.AddWithValue("@processed_by", username);
                insertCmd.Parameters.AddWithValue("@records_added", records_added);
                insertCmd.Parameters.AddWithValue("@records_changed", records_changed);
                insertCmd.Parameters.AddWithValue("@records_deleted", records_deleted);

                insertCmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex) {
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public List<FileLoadInfo> getFileTableInfo(FILE_TABLES fileTable, String fileType, bool latest) {
            List<FileLoadInfo> fileInfo = new List<FileLoadInfo>();

            string sqlStmt = "SELECT file_name, file_type, upload_date AS process_date, " +
                "uploaded_by AS processed_by, " +
                "records_added, records_changed, records_deleted FROM file_uploads " +
                "WHERE file_type = '" + fileType + "'";

            if (fileTable == FILE_TABLES.FileDownLoad)
                sqlStmt = "SELECT file_name, file_type, download_date AS process_date, " +
                "downloaded_by AS processed_by, " +
                "records_downloaded AS records_added, 0 AS records_changed, 0 AS records_deleted FROM file_downloads " +
                "WHERE file_type = '" + fileType + "'";

            if (latest)
                sqlStmt += " ORDER BY process_date DESC";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);
            SqlDataReader rdr = cmd.ExecuteReader();

            if (latest) {
                // Read just the latest record
                if (rdr.Read()) {
                    FileLoadInfo info = new FileLoadInfo();
                    info.FileName = Convert.ToString(rdr["file_name"]);
                    info.FileType = Convert.ToString(rdr["file_type"]);
                    info.ProcessDate = Convert.ToDateTime(rdr["process_date"]);
                    info.ProcessedBy = Convert.ToString(rdr["processed_by"]);
                    info.RecordsAdded = Convert.ToInt32(rdr["records_added"]);
                    info.RecordsChanged = Convert.ToInt32(rdr["records_changed"]);
                    info.RecordsDeleted = Convert.ToInt32(rdr["records_deleted"]);

                    fileInfo.Add(info);
                }
            }
            else {
                while (rdr.Read()) {
                    FileLoadInfo info = new FileLoadInfo();
                    info.FileName = Convert.ToString(rdr["file_name"]);
                    info.FileType = Convert.ToString(rdr["file_type"]);
                    info.ProcessDate = Convert.ToDateTime(rdr["process_date"]);
                    info.ProcessedBy = Convert.ToString(rdr["processed_by"]);
                    info.RecordsAdded = Convert.ToInt32(rdr["records_added"]);
                    info.RecordsChanged = Convert.ToInt32(rdr["records_changed"]);
                    info.RecordsDeleted = Convert.ToInt32(rdr["records_deleted"]);

                    fileInfo.Add(info);
                }
            }

            conn.Close();

            return fileInfo;
        }
        #endregion
    }
}
