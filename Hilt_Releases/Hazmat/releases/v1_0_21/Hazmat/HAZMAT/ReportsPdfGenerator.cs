﻿using System;
using System.Data;
using System.IO;

using iTextSharp.text;

using iTextSharp.text.pdf;
using HAZMAT.Api.Models;


namespace HAZMAT
{
    public class ReportsPdfGenerator
    {

        public string generateIncompatiblesReport(String tableMainHeader, DataTable dt, string imgPath, string filter)
        {
            //create document
            //Document doc = new Document(new Rectangle(1056, 816));
            Document doc = new Document(PageSize.LETTER.Rotate(), 50, 50, 25, 50);

            //path
            String completedFilePath = Path.GetTempFileName();

            //create
            PdfWriter PDFWriter = PdfWriter.GetInstance(doc, new FileStream(completedFilePath, FileMode.Create));
            PDFWriter.PageEvent = new IncompatiblesHelper() { filter = filter };
            doc.Open();
            PdfPTable table = createIncompatibleTable(dt, imgPath);
            table.SpacingAfter = 20;
            doc.Add(table);

            //add the legend and HCC-specific guidance to the end
            PdfPTable legend = new PdfPTable(2);
            PdfPCell title = new PdfPCell(new Phrase("Legend", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            title.Colspan = 1;
            title.HorizontalAlignment = 0;
            legend.AddCell(title);
            PdfPCell title2 = new PdfPCell(new Phrase("HCC-specific guidance", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            title2.Colspan = 1;
            title2.HorizontalAlignment = 0;
            legend.AddCell(title2);
            PdfPCell cell = new PdfPCell(new Phrase("+    Allowed - Stowage is authorized \n\nO     Restricted - Separated in a manner that, in the event of leakage, mixing of hazardous materials would not occur \n\nX     Prohibited - Cannot be stored in the same compartment unless segregated b a NAVSEA approved cabinet \n\n#     Consult MSDS for specific stowage requirements", new Font(Font.FontFamily.HELVETICA, 11f, Font.NORMAL, BaseColor.BLACK)));
            cell.Colspan = 1;
            cell.HorizontalAlignment = 0;
            legend.AddCell(cell);
            PdfPCell cell2 = new PdfPCell(new Phrase("(1) C1-C4: Store concentrated nitric acid in acid locker, and keep distance from other acids. Store Bromine Cartridges in dedicated cabinets\n\n(2) D4: Store Calcium Hypochorite in designated NAVSEA approved locker. Do not store oxidizers in same compartment with flammables or combustibles\n\n(3) G1-G9: Mount all stored gas cylinders to prevent them from falling or rolling (Grade B shock). Keep maximum distance possible between flammable (G2, G8) and oxidizer (G4, G7, G9) gases when not in user (e.g. oxygen/acetylene in welding)\n\n(4) V2, V3: All aerosols will be stored together within the same location within a storeroom. Further segregate aerosols from flammable liquids and gases in the same space using wire mesh or other barrier (e.g., locker) to prevent projectile in the case of fire", new Font(Font.FontFamily.HELVETICA, 12f, Font.NORMAL, BaseColor.BLACK)));
            cell2.Colspan = 1;
            cell2.HorizontalAlignment = 0;
            legend.AddCell(cell2);
            doc.Add(legend);


            //close
            doc.Close();

            return completedFilePath;
        }


        public string PrintInventoryDetails(int inventory_onhand_id, string username)
        {
            Document doc = new Document(PageSize.LETTER.Rotate(), 50, 50, 25, 50);

            DatabaseManager dbManager = new DatabaseManager();
            var data = dbManager.getInventoryDetailsForPrint(inventory_onhand_id);
            var onhand = new DataTable();
            var quantity = dbManager.getTotalInventoryQty(inventory_onhand_id);
            var user = dbManager.getUserRole(username);
            if (user == "WORKCENTER USER")
            {
                onhand = dbManager.getInventoryDetailsForReport(inventory_onhand_id, Convert.ToInt32(dbManager.getWorkcenterForUser(user).Rows[0]["workcenter_id"]));
            }
            else if (user != "WORKCENTER USER" && user != "BASE USER")
            {
                onhand = dbManager.getInventoryDetailsForReport(inventory_onhand_id, null);
            }
            var row = data.Rows[0];

            //path
            String completedFilePath = Path.GetTempFileName();

            //create
            PdfWriter PDFWriter = PdfWriter.GetInstance(doc, new FileStream(completedFilePath, FileMode.Create));
            PDFWriter.PageEvent = new InventoryDetailsHelper();
            doc.Open();
            PdfPTable table = new PdfPTable(7);
            table.WidthPercentage = 100;
            //section headers
            PdfPCell itemDescriptionLabel = new PdfPCell(new Phrase("Item Description", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            itemDescriptionLabel.Colspan = 3;
            itemDescriptionLabel.HorizontalAlignment = 0;
            itemDescriptionLabel.PaddingBottom = 5;
            itemDescriptionLabel.Border = Rectangle.NO_BORDER;
            table.AddCell(itemDescriptionLabel);
            table.AddCell(new PdfPCell() {Colspan = 1, Border=Rectangle.NO_BORDER});
            PdfPCell itemStatusLabel = new PdfPCell(new Phrase("Item Status", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            itemStatusLabel.Colspan = 3;
            itemStatusLabel.HorizontalAlignment = 0;
            itemStatusLabel.PaddingBottom = 5;
            itemStatusLabel.Border = Rectangle.NO_BORDER;
            table.AddCell(itemStatusLabel);
            //item data
            PdfPCell descriptionData = new PdfPCell();
            descriptionData.Colspan = 3;
            descriptionData.HorizontalAlignment = 0;
            descriptionData.Border = Rectangle.NO_BORDER;
            descriptionData.PaddingLeft = 5;
            descriptionData.BackgroundColor = BaseColor.LIGHT_GRAY;
            descriptionData.AddElement(new Paragraph(row["fsc"].ToString() + " - " + data.Rows[0]["niin"].ToString()));
            descriptionData.AddElement(new Paragraph(row["item_name"].ToString()));
            descriptionData.AddElement(new Paragraph("U/I: " + row["ui_abbrev"] + " - " + row["ui_desc"].ToString()));
            descriptionData.AddElement(new Paragraph("U/M: " + row["um"].ToString()));
            descriptionData.AddElement(new Paragraph("SPMIG: " + row["spmig"].ToString()));
            descriptionData.AddElement(new Paragraph("SPECS: " + row["specs"].ToString()) { SpacingAfter = 5 });
            table.AddCell(descriptionData);
            table.AddCell(new PdfPCell() { Colspan = 1, Border=Rectangle.NO_BORDER });
            PdfPCell statusData = new PdfPCell();
            statusData.Colspan = 3;
            statusData.HorizontalAlignment = 0;
            statusData.Border = Rectangle.NO_BORDER;
            statusData.BackgroundColor = BaseColor.LIGHT_GRAY;
            statusData.PaddingLeft = 5;
            statusData.AddElement(new Paragraph("Use Category: " +  row["usage_category"].ToString() + " - " + row["usage_category_desc"].ToString()));
            statusData.AddElement(new Paragraph("SLC: " + row["slc"].ToString() + " - " + row["slc_desc"].ToString()));
            statusData.AddElement(new Paragraph("SLAC: " + row["slac"].ToString() + " - " + row["slac_desc"].ToString()));
            statusData.AddElement(new Paragraph("HCC: " + row["hcc"].ToString() + " - " + row["hcc_desc"].ToString()));
            statusData.AddElement(new Paragraph("SMCC: " + row["smcc"].ToString() + " - " + row["smcc_desc"].ToString()));
            table.AddCell(statusData);
            table.AddCell(new PdfPCell() {Colspan = 7, Border = Rectangle.NO_BORDER, FixedHeight = 5});
            //remarks
            PdfPCell remarksLabel = new PdfPCell(new Phrase("Notes", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            remarksLabel.Colspan = 7;
            remarksLabel.HorizontalAlignment = 0;
            remarksLabel.PaddingBottom = 5;
            remarksLabel.Border = Rectangle.NO_BORDER;
            table.AddCell(remarksLabel);
            PdfPCell remarks = new PdfPCell();
            remarks.Colspan = 7;
            remarks.Border = Rectangle.NO_BORDER;
            remarks.BackgroundColor = BaseColor.LIGHT_GRAY;
            remarks.PaddingLeft = 3;
            remarks.PaddingBottom = 5;
            remarks.AddElement(new Paragraph(row["notes"].ToString()));
            table.AddCell(remarks);
            table.AddCell(new PdfPCell() { Colspan = 7, Border = Rectangle.NO_BORDER, FixedHeight = 15 });
            //quantity stuff
            PdfPCell onhandQuantity = new PdfPCell(new Phrase("Total Onhand Quantity: " + quantity, new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            onhandQuantity.Colspan = 2;
            onhandQuantity.HorizontalAlignment = 0;
            onhandQuantity.PaddingBottom = 5;
            onhandQuantity.Border = Rectangle.NO_BORDER;
            table.AddCell(onhandQuantity);
            table.AddCell(new PdfPCell() { Colspan = 1, Border = Rectangle.NO_BORDER });
            PdfPCell allowQuantity = new PdfPCell(new Phrase("Quantity Allowed: " + row["allowance_quantity"].ToString(), new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            allowQuantity.Colspan = 2;
            allowQuantity.HorizontalAlignment = 0;
            allowQuantity.PaddingBottom = 5;
            allowQuantity.Border = Rectangle.NO_BORDER;
            table.AddCell(allowQuantity);
            table.AddCell(new PdfPCell() { Colspan = 2, Border = Rectangle.NO_BORDER });
            doc.Add(table);

            //grid for specific items

            PdfPTable onhandTable = new PdfPTable(onhand.Columns.Count);
            onhandTable.WidthPercentage = 100;

            Font font = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK);
            Font warningFont = new Font(Font.FontFamily.HELVETICA, 5f, Font.NORMAL, BaseColor.BLACK);

            //column headers
            foreach (DataColumn column in onhand.Columns)
            {
                PdfPCell pdfCell = new PdfPCell(new Phrase(column.ColumnName.ToUpper(), new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD, BaseColor.WHITE)));

                pdfCell.BackgroundColor = new BaseColor(161, 161, 161);

                onhandTable.AddCell(pdfCell);
            }

            //rest of the table values
            foreach (DataRow r in onhand.Rows)
            {
                foreach (DataColumn column in onhand.Columns)
                {
                    PdfPCell pdfCell = null;
                    if ((column.ColumnName == "Inv Date") || (column.ColumnName == "Onboard Date") || (column.ColumnName == "Expiration Date"))
                    {
                        if (r[column.ColumnName] != DBNull.Value)
                        {
                            DateTime date = Convert.ToDateTime(r[column.ColumnName]);
                            pdfCell = new PdfPCell(new Phrase(date.ToString("MM/dd/yyyy"), font));
                        }
                        else pdfCell = new PdfPCell();
                    }
                    else
                    {
                        pdfCell = new PdfPCell(new Phrase(r[column.ColumnName].ToString(), font));
                    }

                    onhandTable.AddCell(pdfCell);
                }
            }
            doc.Add(onhandTable);

            doc.Close();
            return completedFilePath;
        }

        public string generateRestrictedMaterialReport(String tableMainHeader, DataTable dt)
        {
            //create document
            Document doc = new Document(new Rectangle(1056, 816));

            //path
            String completedFilePath = Path.GetTempFileName();

            //create
            PdfWriter.GetInstance(doc, new FileStream(completedFilePath, FileMode.Create));
            doc.Open();



            //build document
            Font font = new Font(Font.NORMAL, 5f);


            PdfPTable table = addTable(tableMainHeader, dt, font);

            doc.Add(table);

            doc.Add(new Paragraph(" "));
            doc.Add(new Paragraph(" "));
            doc.Add(new Chunk("SUPPO: __________________ DCA: __________________ WEPS: ___________________ NAV: __________________ ENG: __________________ XO: _____________________ "));

            //close
            doc.Close();

            return completedFilePath;
        }

        private iTextSharp.text.Image getHazardImage(string hazard)
        {



            iTextSharp.text.Image img1;
            if (hazard == "Flammable Gas")
            {
                img1 = FLAM_GAS_IMG;
            }
            else if (hazard == "Nonflammable Gas")
            {
                img1 = NON_FLAM_GAS_IMG;
            }
            else if (hazard == "Flammable/Combustible")
            {
                img1 = COMBUST_IMG;
            }
            else if (hazard == "Flammable & Other")
            {
                img1 = FLAM_OTHER_IMG;
            }
            else if (hazard == "Dangerous When Wet")
            {
                img1 = DANGER_WET_IMG;
            }
            else if (hazard == "Flammable/Reactive (Lithium)")
            {
                img1 = FLAM_REACTIVE_IMG;
            }
            else if (hazard == "Poison / Toxic")
            {
                img1 = POISON_IMG;
            }

            else if (hazard == "Oxidizer")
            {
                img1 = OXIDIZER_IMG;
            }

            else if (hazard == "Corrosive-Alkali (Base)")
            {
                img1 = CORR_ALK_IMG;
            }

            else if (hazard == "Low Hazard/Batteries")
            {
                img1 = LOW_HAZ_BATT_IMG;
            }

            else if (hazard == "Low Hazard/Other")
            {
                img1 = LOW_HAZ_OTHER_IMG;
            }

            else if (hazard == "Corrosive-Acid")
            {
                img1 = CORR_ACID_IMG;
            }


            else if (hazard == "Atmospheric Control")
            {
                img1 = ATM_CONTROL_IMG;
            }

            else if (hazard == "Radioactive Matl")
            {
                img1 = RADIOACTIVE_IMG;
            }
            else
            {
                img1 = HAZ_UNK;
            }
            return img1;

        }

        private iTextSharp.text.Image CORR_ACID_IMG;
        private iTextSharp.text.Image RADIOACTIVE_IMG;
        private iTextSharp.text.Image FLAM_OTHER_IMG;
        private iTextSharp.text.Image FLAM_REACTIVE_IMG;
        private iTextSharp.text.Image POISON_IMG;
        private iTextSharp.text.Image CORR_ALK_IMG;
        private iTextSharp.text.Image OXIDIZER_IMG;
        private iTextSharp.text.Image LOW_HAZ_BATT_IMG;
        private iTextSharp.text.Image LOW_HAZ_OTHER_IMG;
        private iTextSharp.text.Image HAZ_UNK;
        private iTextSharp.text.Image ATM_CONTROL_IMG;
        private iTextSharp.text.Image MULT_HAZ_IMG;
        private iTextSharp.text.Image FLAM_GAS_IMG;
        private iTextSharp.text.Image NON_FLAM_GAS_IMG;
        private iTextSharp.text.Image COMBUST_IMG;
        private iTextSharp.text.Image DANGER_WET_IMG;

        private PdfPTable createIncompatibleTable(DataTable dt, string imgPath)
        {



            CORR_ACID_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/12_corrosive_acid.gif");
            RADIOACTIVE_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/14_radioactive.gif");
            FLAM_OTHER_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/4_flammable_other.gif");
            FLAM_REACTIVE_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/6_flammable_reactive.gif");
            POISON_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/7_poison.gif");
            CORR_ALK_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/9_corrosive_alkali.gif");
            OXIDIZER_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/8_oxidizer.jpg");
            LOW_HAZ_BATT_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/10_low_hazard_batteries.jpg");
            LOW_HAZ_OTHER_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/11_low_hazard_other.jpg");
            HAZ_UNK = iTextSharp.text.Image.GetInstance(imgPath + "images/0_potential_hazards_unknown.png");
            ATM_CONTROL_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/13_atmosphere_control.png");
            MULT_HAZ_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/15_multiple_hazards.png");
            FLAM_GAS_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/1_flammable_gas.png");
            NON_FLAM_GAS_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/2_non_flammable_gas.png");
            COMBUST_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/3_combustible.png");
            DANGER_WET_IMG = iTextSharp.text.Image.GetInstance(imgPath + "images/5_dangerous_when_wet.png");



            int columns = dt.Columns.Count;

            PdfPTable table = new PdfPTable(columns);
            table.WidthPercentage = 100;

            Font font = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL, BaseColor.BLACK);
            Font warningFont = new Font(Font.FontFamily.HELVETICA, 5f, Font.NORMAL, BaseColor.BLACK);

            //column headers
            foreach (DataColumn column in dt.Columns)
            {
                PdfPCell pdfCell = new PdfPCell(new Phrase(column.ColumnName.ToUpper(), new Font(Font.FontFamily.HELVETICA, 5f, Font.BOLD, BaseColor.WHITE)));

                pdfCell.BackgroundColor = new BaseColor(161, 161, 161);

                table.AddCell(pdfCell);
            }

            //PdfPCell warningImage1 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 5f, Font.BOLD, BaseColor.WHITE)));
            //warningImage1.BackgroundColor = new BaseColor(161, 161, 161);
            //table.AddCell(warningImage1);

            //PdfPCell warningImage2 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 5f, Font.BOLD, BaseColor.WHITE)));
            //warningImage2.BackgroundColor = new BaseColor(161, 161, 161);
            //table.AddCell(warningImage2);


            //PdfPCell warning = new PdfPCell(new Phrase("WARNING", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD, BaseColor.WHITE)));
            //warning.BackgroundColor = new BaseColor(161, 161, 161);
            //table.AddCell(warning);

            int rowIndex = 0;

            //rest of the table values
            foreach (DataRow r in dt.Rows)
            {
                string warning_level = "";
                foreach (DataColumn column in dt.Columns)
                {
                    //if (column.ColumnName == "warning_level")
                    //{
                    //    warning_level = r["warning_level"].ToString();
                    //}
                    //else
                    //{
                    PdfPCell pdfCell = new PdfPCell(new Phrase(r[column.ColumnName].ToString(), font));

                    //alternate colors
                    if (rowIndex % 2 != 0)
                        pdfCell.BackgroundColor = new BaseColor(229, 229, 229);

                    table.AddCell(pdfCell);
                    //}
                }

                //    iTextSharp.text.Image img1 = getHazardImage(r["Hazard"].ToString());
                //    img1.ScaleAbsoluteHeight(30);
                //    img1.ScaleAbsoluteWidth(30);
                //    img1.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                //    PdfPCell imgCell1 = new PdfPCell(img1);
                //    imgCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //    imgCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                //    table.AddCell(imgCell1);

                //    iTextSharp.text.Image img2 = getHazardImage(r["Hazard1"].ToString());
                //    img2.ScaleAbsoluteHeight(30);
                //    img2.ScaleAbsoluteWidth(30);
                //    img2.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                //    PdfPCell imgCell2 = new PdfPCell(img2);
                //    imgCell2.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                //    imgCell2.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                //    table.AddCell(imgCell2);

                //    // warning level
                //    string warnMessage = "";
                //    string hazard_name = r["Hazard"].ToString();
                //    string hazard_name2 = r["Hazard1"].ToString();
                //    if (warning_level.Equals("X"))
                //    {
                //        warnMessage = hazard_name + " and " + hazard_name2 + " materials can not be stored together.";
                //    }
                //    if (warning_level.Equals("O"))
                //    {
                //        warnMessage = hazard_name + " and " + hazard_name2 + " materials may be stowed in the same compartment but require a 4 foot horizontal separation.";
                //    }
                //    if (warning_level.Equals("W"))
                //    {
                //        warnMessage = "WARNING - Acids and Bases must be stowed so as to prevent comingling.";
                //    }
                //    if (warning_level.EndsWith("U"))
                //    {
                //        warnMessage = "The potential hazards of storing these items together are unknown because the MSDS Hazard information of one or both is not known or could not be found.";
                //    }

                //    PdfPCell warningCell = new PdfPCell(new Phrase(warnMessage, warningFont));

                //    //alternate colors
                //    if (rowIndex % 2 != 0)
                //        warningCell.BackgroundColor = new BaseColor(229, 229, 229);

                //    table.AddCell(warningCell);

                //    rowIndex++;
            }

            //footer
            //column headers
            foreach (DataColumn column in dt.Columns)
            {

                PdfPCell pdfCell = new PdfPCell(new Phrase("   ", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.WHITE)));

                pdfCell.BackgroundColor = new BaseColor(161, 161, 161);

                table.AddCell(pdfCell);
            }

            return table;

        }

        private PdfPTable addTable(String tableMainHeader, DataTable dt, Font font)
        {
            int columns = dt.Columns.Count;

            PdfPTable table = new PdfPTable(columns);
            table.WidthPercentage = 100;


            //header
            PdfPCell cell = new PdfPCell(new Phrase(tableMainHeader, new Font(Font.FontFamily.HELVETICA, 15f, Font.BOLD, BaseColor.BLACK)));
            cell.Colspan = columns;
            cell.BackgroundColor = new BaseColor(229, 229, 229);
            cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            table.AddCell(cell);

            //column headers
            foreach (DataColumn column in dt.Columns)
            {

                PdfPCell pdfCell = new PdfPCell(new Phrase(column.ColumnName, new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.WHITE)));

                pdfCell.BackgroundColor = new BaseColor(161, 161, 161);

                table.AddCell(pdfCell);
            }

            int rowIndex = 0;

            //rest of the table values
            foreach (DataRow r in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    PdfPCell pdfCell = new PdfPCell(new Phrase(r[column.ColumnName].ToString()));

                    //alternate colors
                    if (rowIndex % 2 != 0)
                        pdfCell.BackgroundColor = new BaseColor(229, 229, 229);

                    table.AddCell(pdfCell);
                }
                rowIndex++;
            }

            //footer
            //column headers
            foreach (DataColumn column in dt.Columns)
            {

                PdfPCell pdfCell = new PdfPCell(new Phrase("   ", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.WHITE)));

                pdfCell.BackgroundColor = new BaseColor(161, 161, 161);

                table.AddCell(pdfCell);
            }

            return table;

        }




    }
}