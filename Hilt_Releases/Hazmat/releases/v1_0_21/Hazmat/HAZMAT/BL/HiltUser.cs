﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HAZMAT.BL
{
    public class HiltUser
    {
        private string username = null;

        public HiltUser (string username)
        {
            this.username = username;
        }

        public bool IsAdmin()
        {
            return new DatabaseManager().isAdmin(username);
        }

        public bool IsSupplyOfficer()
        {
            return new DatabaseManager().isSupplyOfficer(username);
        }


        public bool IsSupplyUser()
        {
            return new DatabaseManager().isSupplyUser(username);
        }


        public bool IsWorkCenterUser()
        {
            return new DatabaseManager().isWorkCenterUser(username);
        }

    }
}