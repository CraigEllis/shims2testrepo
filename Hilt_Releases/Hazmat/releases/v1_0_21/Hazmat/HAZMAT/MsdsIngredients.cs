﻿using System;

namespace HAZMAT
{
    [Serializable]
    public class MsdsIngredients
    {
        //PERCENT TEXT value
     
        public string PRCNT
        {
            get;
            set;
        }
        //PERCENT VOLUME value
      
        public string PRCNT_VOL_VALUE
        {
            get;
            set;
        }
        //PERCENT WEIGHT value
    
        public string PRCNT_VOL_WEIGHT
        {
            get;
            set;
        }
        //ACGIH STEL
     
        public string ACGIH_STEL
        {
            get;
            set;
        }
        //ACGIH TLV
     
        public string ACGIH_TLV
        {
            get;
            set;
        }
        //CAS NUMBER
     
        public string CAS
        {
            get;
            set;
        }
        //CHEMICAL MANUFACTURER COMPANY NAME
     
        public string CHEM_MFG_COMP_NAME
        {
            get;
            set;
        }
        //COMPONENT INGREDIENT NAME
     
        public string INGREDIENT_NAME
        {
            get;
            set;
        }
        //DOT RQ
      
        public string DOT_REPORT_QTY
        {
            get;
            set;
        }
        //EPA RQ
      
        public string EPA_REPORT_QTY
        {
            get;
            set;
        }
        //ODS INDICATOR
       
        public string ODS_IND
        {
            get;
            set;
        }
        //OSHA PEL
   
        public string OSHA_PEL
        {
            get;
            set;
        }
        //OSHA STEL
    
        public string OSHA_STEL
        {
            get;
            set;
        }
        //OTHER RECORDED LIMITS
    
        public string OTHER_REC_LIMITS
        {
            get;
            set;
        }
        //RTECS NUMBER
    
        public string RTECS_NUM
        {
            get;
            set;
        }
        //RTECS CODE
    
        public string RTECS_CODE
        {
            get;
            set;
        }
    }
}
