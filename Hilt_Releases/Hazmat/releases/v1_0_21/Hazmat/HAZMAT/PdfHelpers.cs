﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace HAZMAT
{
    public class IncompatiblesHelper : PdfPageEventHelper
    {
        public string filter;
        // This is the contentbyte object of the writer
        PdfContentByte cb;
        // we will put the final number of pages in a template
        PdfTemplate template;
        DatabaseManager dbManager;

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            DatabaseManager dbManager = new DatabaseManager();
            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;

            PdfPCell cell = new PdfPCell(new Phrase("HM HCC Incompatible List", new Font(Font.FontFamily.HELVETICA, 13f, Font.BOLD, BaseColor.BLACK)));
            cell.Colspan = 1;
            cell.BackgroundColor = new BaseColor(229, 229, 229);
            cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);
            var ship = dbManager.getCurrentShipName();
            PdfPCell cell3 = new PdfPCell(new Phrase(ship, new Font(Font.FontFamily.HELVETICA, 13f, Font.BOLD, BaseColor.BLACK)));
            cell3.Colspan = 1;
            cell3.BackgroundColor = new BaseColor(229, 229, 229);
            cell3.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
            cell3.Border = Rectangle.NO_BORDER;
            table.AddCell(cell3);
            PdfPCell date = new PdfPCell(new Phrase(DateTime.Today.ToString("dddd, MMMM dd, yyyy"), new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD, BaseColor.BLACK)));
            date.Colspan = 2;
            date.BackgroundColor = new BaseColor(229, 229, 229);
            date.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            date.Border = Rectangle.NO_BORDER;
            table.AddCell(date);
            PdfPCell cell2 = new PdfPCell(new Phrase("Warning – Per NSTM 670 HCC Compatibility Chart, the following stowage incompatibilities have been found in the specified location(s). Refer to the Action Code column and the legend for further instructions. The Action Code legend is located on the last page of this report. Consult the MSDS for any additional specific stowage requirements.", new Font(Font.FontFamily.HELVETICA, 9f, Font.NORMAL, BaseColor.BLACK)));
            cell2.Colspan = 2;
            cell2.BackgroundColor = new BaseColor(229, 229, 229);
            cell2.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cell2.Border = Rectangle.NO_BORDER;
            table.AddCell(cell2);
            table.SpacingAfter = 20;
            document.Add(table);

        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            base.OnOpenDocument(writer, document);
            cb = writer.DirectContent;
            template = cb.CreateTemplate(50, 50);
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            int pageN = writer.PageNumber;
            String text = "Page " + pageN + " of ";
            float len = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED).GetWidthPoint(text, 8);
            Rectangle pageSize = document.PageSize;
            cb.SetRGBColorFill(100, 100, 100);
            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 8);
            cb.BeginText();
            cb.SetTextMatrix(pageSize.GetRight(50), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();
            cb.AddTemplate(template, pageSize.GetRight(50) + len, pageSize.GetBottom(30));

            cb.BeginText();
            cb.SetTextMatrix(pageSize.GetLeft(50), pageSize.GetBottom(30));
            cb.ShowText("SHIMS – Submarine Hazardous Material Inventory & Management System. Definition of HCC codes can be found in the SHIMS Reference Library.");
            cb.EndText();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            template.BeginText();
            template.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 8);
            template.SetTextMatrix(0, 0);
            template.ShowText("" + (writer.PageNumber - 1));
            template.EndText();
        }
    }

    public class InventoryDetailsHelper : PdfPageEventHelper
    {
        DatabaseManager dbManager = new DatabaseManager();
        // This is the contentbyte object of the writer
        PdfContentByte cb;
        // we will put the final number of pages in a template
        PdfTemplate template;

        public override void OnStartPage(PdfWriter writer, Document document)
        {

            PdfPTable table = new PdfPTable(2);
            table.WidthPercentage = 100;

            string shipName = dbManager.getShip().Rows[0]["name"].ToString();

            PdfPCell cell = new PdfPCell(new Phrase(shipName, new Font(Font.FontFamily.HELVETICA, 13f, Font.BOLD, BaseColor.BLACK)));
            cell.Colspan = 1;
            cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);
            PdfPCell cell3 = new PdfPCell(new Phrase("HM Inventory Detail", new Font(Font.FontFamily.HELVETICA, 13f, Font.BOLD, BaseColor.BLACK)));
            cell3.Colspan = 1;
            cell3.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
            cell3.Border = Rectangle.NO_BORDER;
            table.AddCell(cell3);
            PdfPCell emptyCell = new PdfPCell();
            emptyCell.Border = Rectangle.NO_BORDER;
            emptyCell.HorizontalAlignment = 1;
            table.AddCell(emptyCell);
            PdfPCell date = new PdfPCell(new Phrase(DateTime.Today.ToString("dddd, MMMM dd, yyyy"), new Font(Font.FontFamily.HELVETICA, 10f, Font.BOLD, BaseColor.BLACK)));
            date.Colspan = 2;
            date.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
            date.Border = Rectangle.NO_BORDER;
            table.AddCell(date);
            table.SpacingAfter = 20;
            document.Add(table);

        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            base.OnOpenDocument(writer, document);
            cb = writer.DirectContent;
            template = cb.CreateTemplate(50, 50);
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            int pageN = writer.PageNumber;
            String text = "Page " + pageN + " of ";
            float len = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED).GetWidthPoint(text, 8);
            Rectangle pageSize = document.PageSize;
            cb.SetRGBColorFill(100, 100, 100);
            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 8);
            cb.BeginText();
            cb.SetTextMatrix(pageSize.GetRight(50), pageSize.GetBottom(30));
            cb.ShowText(text);
            cb.EndText();
            cb.AddTemplate(template, pageSize.GetRight(50) + len, pageSize.GetBottom(30));

            cb.BeginText();
            cb.SetTextMatrix(pageSize.GetLeft(50), pageSize.GetBottom(30));
            cb.ShowText("SHIMS – Submarine Hazardous Material Inventory & Management System");
            cb.EndText();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            template.BeginText();
            template.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 8);
            template.SetTextMatrix(0, 0);
            template.ShowText("" + (writer.PageNumber - 1));
            template.EndText();
        }
    }

}