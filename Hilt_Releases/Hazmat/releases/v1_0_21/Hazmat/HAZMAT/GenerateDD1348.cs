﻿using HAZMAT.Api.Models;
using System;
using System.Data;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;



namespace HAZMAT
{
    public class GenerateDD1348
    {
        private DatabaseManager dbManager;
        private string pdfTemplate;
        public string relPath;
        public string tempPath;
        public string fileName;
        public DateTime today;

        public GenerateDD1348()
        {
            this.dbManager = new DatabaseManager();
            pdfTemplate = AppDomain.CurrentDomain.BaseDirectory + "resources\\dd1348_template1.pdf";
        }

        /// <summary>
        /// Generates a DD 1348 in PDF form
        /// </summary>
        /// <param name="uic">The uic of the ship we want to send the DD 1348</param>
        /// <param name="offloadDetails">A dataTable of all the details of an offloaded item(s).</param>
        /// <returns>The relative path to the newly generated PDF</returns>
        public string generatePdf(string uic, DataTable offloadDetails)
        {
            updateDirectory();

            RandomAccessFileOrArray template = new RandomAccessFileOrArray(pdfTemplate);
            PdfReader reader = new PdfReader(template, null);
            MemoryStream ms = new MemoryStream();
            PdfStamper stamper = new PdfStamper(reader, ms);
            AcroFields pdfFormFields = stamper.AcroFields;
            stamper.Close();
            reader = new PdfReader(ms.ToArray());
            Document document = new Document(reader.GetPageSizeWithRotation(1));
            PdfCopy writer = new PdfCopy(document, new FileStream(tempPath, FileMode.Create));
            document.Open();

            ShipDetailModel shipFrom = new ShipDetailModel();
            shipFrom = dbManager.getShipFromDetails(dbManager.getCurrentShipName());

            ShipDetailModel shipTo = new ShipDetailModel();
            shipTo = dbManager.getShipToDetails(uic);

            foreach (DataRow item in offloadDetails.Rows)
            {
                reader = new PdfReader(template, null);
                ms = new MemoryStream();
                stamper = new PdfStamper(reader, ms);
                pdfFormFields = stamper.AcroFields;

                pdfFormFields.SetField("AddData2", shipFrom.Uic + "\n" + shipFrom.ShipName + "\n" + shipFrom.Address);
                pdfFormFields.SetField("AddData3", uic + "\n" + shipTo.Poc + "\n" + shipTo.Address);
                pdfFormFields.SetField("ShipFrom", shipFrom.Uic);
                pdfFormFields.SetField("ShipTo", uic);

                foreach (DataColumn detail in offloadDetails.Columns)
                {
                    pdfFormFields.SetField(detail.ColumnName, item[detail].ToString());
                }

                stamper.FormFlattening = true;
                stamper.Close();

                reader = new PdfReader(ms.ToArray());
                writer.AddPage(writer.GetImportedPage(reader, 1));
            }
            document.Close();

            return relPath;
        }

        /// <summary>
        /// Generates a random filename of 8 characters
        /// </summary>
        /// <param name="extension">The extension of the filename</param>
        /// <returns>The filename with the extension passed in</returns>
        public string getRandomfileName(string extension)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var fileName = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return fileName + "." + extension;
        }

        /// <summary>
        /// Generates a document number using the uic of the current ship, julian date, and a serial number
        /// </summary>
        /// <returns>A string in the form of "uic-julianDate-serial"</returns>
        public string generateDocNumber()
        {
            string docNumber = " ";

            ShipDetailModel shipFrom = new ShipDetailModel();
            shipFrom = dbManager.getShipFromDetails(dbManager.getCurrentShipName());

            today = DateTime.Now;
            int day = Convert.ToInt32(today.Day.ToString());
            int month = Convert.ToInt32(today.Month.ToString("d2"));
            int year = Convert.ToInt32(today.Year.ToString());

            int a = year / 100;
            int b = a / 4;
            int c = 2 - a + b;
            double e = 365.25 * (year + 4716);
            double f = 30.6001 * (month + 1);
            int julianDate = (int)(c + day + e + f - 1524.5);

            string serial = dbManager.getSerial();
            docNumber = shipFrom.Uic + "-" + julianDate.ToString() + "-" + serial;

            return docNumber;
        }

        /// <summary>
        /// Removes older pdf files that have been generated, and, if the temp folder does not exist, creates the temp folder
        /// </summary>
        public void updateDirectory()
        {
            relPath = "resources\\temp\\";
            tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            fileName = getRandomfileName("pdf");

            //Clear out the "temp" directory of any older pdfs
            if (Directory.Exists(tempPath))
            {
                System.IO.DirectoryInfo tempDir = new DirectoryInfo(tempPath);
                foreach (FileInfo file in tempDir.GetFiles()) { file.Delete(); }
            }
            else { Directory.CreateDirectory(tempPath); }
            relPath += fileName;
            tempPath += fileName;
        }

        /// <summary>
        /// Generates a Comma-Separated File of all offloaded items and its details
        /// </summary>
        /// <returns>An HttpResponseMessage that downloads the CSV</returns>
        public HttpResponseMessage generateCSV(DataTable offloadItems)
        {
            //Create Filename
            today = DateTime.Now;
            string date = today.Date.ToString("MM-dd-yyyy");
            fileName = "Offload-" + date + ".csv";
            tempPath = Path.GetTempFileName();

            var csvText = "\"OFFID\",\"FSC\",\"NIIN\",\"CAGE\",\"NOMENCLATURE\",\"MSDS\",\"LOCATION\"," +
                          "\"OFFLOAD QTY\",\"ONQTY\",\"OFFLOAD DATE\",\"UI\",\"UM\",\"DocIdent\",\"rifrom\"," +
                          "\"m&s\",\"quantity\",\"ser\",\"suppaddress\",\"sig\",\"fund\",\"distribution\"," +
                          "\"project\",\"pri\",\"reqddeldate\",\"adv\",\"ri\",\"o/p\",\"condition\",\"mgt\"," +
                          "\"unitprice_$\",\"totalprice_$\",\"shipfrom\",\"shipto\",\"markfor\",\"docdate\"," +
                          "\"nmfc\",\"frtrate\",\"typecargo\",\"ps\",\"qtyrecd\",\"up\",\"unitwt\",\"unitcube\"," +
                          "\"ufc\",\"sl\",\"frtclass\",\"itemname\",\"tycont\",\"nocont\",\"totalwt\",\"totalcube\"," +
                          "\"recdby\",\"daterecd\",\"docnum\",\"nsnniin\",\"memo26\",\"memoshipfrom\",\"memoshipto\"," +
                          "\"memoadd1\",\"memoadd2\"\r\n";

            foreach (DataRow item in offloadItems.Rows)
            {
                foreach (DataColumn detail in offloadItems.Columns)
                {
                    string itemDetail = item[detail].ToString().Replace("\r\n", "\r\n");
                    if (!String.IsNullOrEmpty(itemDetail)) 
                        {itemDetail = "\"" + itemDetail + "\"";}

                    if (detail.ColumnName == "NSN")
                    {
                        String[] fscNiin = itemDetail.Split('-');
                        csvText += fscNiin[0] + "\",\"" + fscNiin[1];
                    }
                    else if (detail.ColumnName == "ItemNomen" || detail.ColumnName == "Quant")
                        {csvText += itemDetail + ",";}

                    else if (detail.ColumnName == "DocNum")
                        {csvText += itemDetail + ",,,,";}
                    else
                        {csvText += itemDetail;}

                    csvText += ",";
                }
                csvText = csvText.TrimEnd(',');
                csvText += "\r\n";
            }

            //Fill in the .csv file
            File.WriteAllText(tempPath, csvText);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(tempPath, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/csv");
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = fileName };

            return result;
        }

        /// <summary>
        /// Adds Columns to a dataTable
        /// </summary>
        /// <param name="table">The table with no columns</param>
        /// <returns>The passed in dataTable with columns</returns>
        public DataTable addColumns(DataTable table)
        {                         
            table.Columns.Add("DocNum", typeof (string));
            table.Columns.Add("Dociden", typeof (string));
            table.Columns.Add("RIFrom", typeof (string));
            table.Columns.Add("MandS", typeof (string));
            table.Columns.Add("SER", typeof (string));
            table.Columns.Add("SupAdd", typeof (string));
            table.Columns.Add("Sig", typeof (string));			  
            table.Columns.Add("Fund", typeof (string));
            table.Columns.Add("Distribution", typeof (string));
            table.Columns.Add("Project", typeof (string));
            table.Columns.Add("Pri", typeof (string));
            table.Columns.Add("RecdDelDate", typeof (string));
            table.Columns.Add("Adv", typeof (string));
            table.Columns.Add("Ri", typeof (string));
            table.Columns.Add("OP", typeof (string));
            table.Columns.Add("MGT", typeof (string));
            table.Columns.Add("Cond", typeof (string));		  
            table.Columns.Add("UnitPrice", typeof (string));
            table.Columns.Add("TotalPrice", typeof (string));
            table.Columns.Add("MarkFor", typeof (string));
            table.Columns.Add("NMFC", typeof (string));
            table.Columns.Add("FRTRate", typeof (string));
            table.Columns.Add("TypeCargo", typeof (string));
            table.Columns.Add("PS", typeof (string));
            table.Columns.Add("QtyRecd", typeof (string));			  
            table.Columns.Add("UP", typeof (string));
            table.Columns.Add("UnitWeight", typeof (string));
            table.Columns.Add("UnitCube", typeof (string));
            table.Columns.Add("UFC", typeof (string));
            table.Columns.Add("SI", typeof (string));
            table.Columns.Add("FreightClassNomen", typeof (string));
            table.Columns.Add("TYCount", typeof (string));
            table.Columns.Add("NOCount", typeof (string));
            table.Columns.Add("TotalWeight", typeof (string));
            table.Columns.Add("TotalCube", typeof (string));		  
            table.Columns.Add("RecdBy", typeof (string));
            table.Columns.Add("DateRecd", typeof (string));
            table.Columns.Add("RIC", typeof (string));
            table.Columns.Add("AddData1", typeof(string));
            table.Columns.Add("CAGE", typeof(string));
            table.Columns.Add("ItemNomen", typeof(string));
            table.Columns.Add("UnitIss", typeof(string));
            table.Columns.Add("AddData4", typeof(string));
            table.Columns.Add("Quant", typeof(string));
            table.Columns.Add("NSN", typeof(string));
            table.Columns.Add("DocDate", typeof (string));
            table.Columns.Add("OffloadId", typeof(string));
        
            return table;
        }
    }
}