﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HAZMAT
{
    // this class is used to generate standard color coding for usage categories.

    public class UsageCategoryColor
    {
        public string TextColorName;
        public string TextColorHex;
        public string BackgroundColorName;
        public string BackgroundColorHex;
    }

    public enum UsageCategories
    {
        Prohibited = 1,
        Restricted = 2,
        Limited = 3,
        Permitted = 4
    }

    public class UsageCategoryColors
    {
        private readonly Dictionary<int, UsageCategoryColor> colors =
            new Dictionary<int, UsageCategoryColor>()
            {
                // prohibited
                {(int) UsageCategories.Prohibited, new UsageCategoryColor {TextColorName="", TextColorHex = "#a94442", BackgroundColorName = "", BackgroundColorHex = "#f2dede"}},

                // restricted
                {(int) UsageCategories.Restricted, new UsageCategoryColor {TextColorName="", TextColorHex = "#8a6d3b", BackgroundColorName = "", BackgroundColorHex = "#fcf8e3"}},

                // limited
                {(int) UsageCategories.Limited, new UsageCategoryColor {TextColorName="", TextColorHex = "#31708f", BackgroundColorName = "", BackgroundColorHex = "#d9edf7"}},

                // permitted
                {(int) UsageCategories.Permitted, new UsageCategoryColor {TextColorName="", TextColorHex = "#3c763d", BackgroundColorName = "", BackgroundColorHex = "#dff0d8"}}
            };

        private readonly UsageCategoryColor defaultColor =
            new UsageCategoryColor() { TextColorName = "", TextColorHex = "", BackgroundColorName = "", BackgroundColorHex = "" };

        public UsageCategoryColor getUsageCategoryColor(int catId)
        {
            if (!colors.ContainsKey(catId))
                return defaultColor;

            return colors[catId];
        }
    }
}