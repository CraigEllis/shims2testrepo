﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using CSSFriendly;

namespace HAZMAT
{
    public partial class Site : System.Web.UI.MasterPage
    {

        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

       

        private void setPermssions()
        {
            

            //if the user is not a member of a role, he/she does not get to see certain links.
            //if (!isWorkCenterUser() && !isAdmin() && !isSupplyOfficer() && !isSupplyUser())
            //{

            //    try
            //    {

            //        List<MenuItem> itemList = new List<MenuItem>();

            //        foreach (MenuItem m in Menu1.Items)
            //        {

            //            if (m.Text.ToLower().Equals("ship"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("users"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("msds"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("issue"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("decanting"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("inventory audit"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("offload"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("atm"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }

            //        }


            //        foreach (MenuItem m in itemList)
            //        {
            //            Menu1.Items.Remove(m);
            //        }

            //    }
            //    catch {}

            //}
            //else if (!isAdmin() && !isSupplyOfficer() && !isSupplyUser())
            //{
            //    try
            //    {

            //        List<MenuItem> itemList = new List<MenuItem>();

            //        foreach (MenuItem m in Menu1.Items)
            //        {

            //            if (m.Text.ToLower().Equals("ship"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("users"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("msds"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("issue"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }
            //            else if (m.Text.ToLower().Equals("atm"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }

            //        }


            //        foreach (MenuItem m in itemList)
            //        {
            //            Menu1.Items.Remove(m);
            //        }

            //    }
            //    catch {}
            //}
            //else if (!isAdmin() && !isSupplyOfficer())
            //{
            //     try
            //    {
            //        List<MenuItem> itemList = new List<MenuItem>();

            //        foreach (MenuItem m in Menu1.Items)
            //        {

            //            if (m.Text.ToLower().Equals("ship"))
            //            {
            //                //remove
            //                itemList.Add(m);                            
            //            }
            //            else if (m.Text.ToLower().Equals("users"))
            //            {
            //                //remove
            //                itemList.Add(m);                            
            //            }
            //            else if (m.Text.ToLower().Equals("atm"))
            //            {
            //                //remove
            //                itemList.Add(m);
            //            }

            //        }


            //        foreach (MenuItem m in itemList)
            //        {
            //            Menu1.Items.Remove(m);
            //        }

            //    }
            //     catch {}
            //}
        }






        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {

            //set permission
            setPermssions();

            if (ConfigurationManager.AppSettings["AuthenticationMode"].ToString().Equals("Windows"))
                btn_logout.Visible = false;
            else
                btn_logout.Visible = true;


            //save current page to the session for feedback
            Session.Add("feedbackPage", GetCurrentPageName());

            string shipName = new DatabaseManager().getCurrentShipName();

            if (shipName != null && !shipName.Equals(""))
            {
                Session.Add("current_ship_name", shipName);

                lbl_shipName.Text = Server.HtmlEncode(shipName.ToString());
            }
            else
            {
                lbl_shipName.Text = "Not yet selected";

            }

            string current_ship_name = "" + Session["current_ship_name"];
            lbl_shipName.Text = current_ship_name;

            string userName = Page.User.Identity.Name;
            string userRole = new DatabaseManager().getUserRole(userName);
            if (userRole != "LEVEL 1")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "HideAdmin", "$('#adminModule').hide();", true);
            }

            bool smclExpired = new DatabaseManager().CheckForExpiredSmcl();
            if (smclExpired && userRole == "LEVEL 1")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSmclAlert", "$('#smclWarning').show();", true);
            }

            lbl_userName.Text = userName + "(" + userRole + ")";
            // Set the version #
            string version = "v" + GetApplicationVersion();
            //version = version.Substring(0, version.LastIndexOf("."));
            //lbl_Version.Text = version;


            string thispage = this.Page.AppRelativeVirtualPath;
            int slashpos = thispage.LastIndexOf('/');
            string pagename = thispage.Substring(slashpos + 1);

            //foreach (MenuItem mi in Menu1.Items)
            //{
            //    if (mi.NavigateUrl.Contains(pagename))
            //    {
            //        mi.Selected = true;
            //        break;
            //    }
            //}

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            //logout
            FormsAuthentication.SignOut();
            Session.Add("current_user", "");
            Response.Redirect("SMCL.aspx");
        }

        public bool isAdmin()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isAdmin(username);
        }

        public bool isSupplyOfficer()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isSupplyOfficer(username);
        }


        public bool isSupplyUser()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isSupplyUser(username);
        }


        public bool isWorkCenterUser()
        {
            string username = Page.User.Identity.Name;
            return new DatabaseManager().isWorkCenterUser(username);
        }

        protected string GetApplicationVersion() {
            return System.Diagnostics.FileVersionInfo.GetVersionInfo(
                    System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion.ToString();
            //return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

    }
}