﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Offload.aspx.cs" Inherits="HAZMAT.Offload" ClientIDMode="Static"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 43px;
        }

        .spaced {
            padding: 10px 40px 10px 40px;
        }
        .form-field {
            border: none;
            background: ghostwhite;
        }
        .ship-info {
            border: none;
            background: ghostwhite;
        }
        .left-button {
            float: left;
            width: 37px;
            padding:310px 0px 310px 15px
        }
        .right-button {
            float: right;
            width: 37px;
            padding:310px 15px 310px 0px
        }
        .item-count {
            margin: 0 auto;
            position: relative;
            text-align: center;
            font-size: 24px;
        }
        .modal-header-btn {
            margin-right: 10px;
        }
        .ui-datepick-header {
            background:cadetblue
        }

        .selected{
            background: #DDDDE9;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" src="/resources/js/site.js"></script>
    <script type="text/javascript" src="/resources/js/Offload.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-header">
        <h1>Offload
        </h1>
    </div>

    <div class="tableHeaderDiv">
        <div class="table-grid-headwrap clearfix">

            <table class="pull-left">
                <tr>
                    <td>
                        <label>
                            <b>Ship To:</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="shipTo" class="shipSelect select2-container"style="width: 200px"></select>
                            </div>
                            </div>
                    </td>
                    <td>
                         <label>
                            <b></b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px; padding-top:20px">
                                <asp:TextBox runat="server" ID="shipToDetails" style="width: 200px"/>
                            </div>
                        </div>
                    </td>
                    <td>
                        <label style="padding-left:10px">
                            <b>POC:</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px">
                                <asp:TextBox runat="server" ID="shipPoc" style="width: 200px"/>
                            </div>
                        </div>
                    </td>               
                    <td>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px; padding-top:21px">
                                <button class="btn btn-info btn-sm btn-details btn-ddBlank"  id="blank1348" style="padding: 5px 10px; margin:0px auto">
                                    DD 1348 (Blank)
                                </button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px; padding-top:21px">
                                <button class="btn btn-info btn-sm btn-details btn-ddFilled " id="filled1348" style="padding: 5px 10px; margin:0px auto" disabled="disabled">
                                    DD 1348 (Filled)
                                </button>
                                <i class="fa fa-cog fa-spin"></i>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px; padding-top:21px">
                                <button class="btn btn-info btn-sm btn-details btn-toDisk"  style="padding: 5px 0px; margin:0px auto" >
                                   <a href="api/Offload/CSV" id="#toDisk"style="color:#ffffff; padding:8px 10px 8px 10px; text-decoration:blink">TO DISK</a>
                                </button>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px; padding-top:21px" >
                                <input type="button" class="btn btn-info btn-sm btn-details btn-archive"  id="archiveAll" value="Archive"/>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-inline">
                            <div class="form-group" style="padding-left:10px; padding-top:21px">
                                <button class="btn btn-info btn-sm btn-details btn-archived" style="padding: 5px 0px; margin:0px auto">
                                    <a href="/OffloadArchived.aspx" style="color:#ffffff; padding:8px 10px 8px 10px; text-decoration:blink">Archived</a>
                                </button>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div>
                <br />
                <br />
            </div>

        </div>
        <hr />
        <table id="OffloadTable" class="table" width="100%">
            <thead>
                <th>Document Number</th>
                <th>Offload Id</th>
                <th>FSC</th>
                <th>NIIN</th>
                <th>CAGE</th>
                <th>Item Name</th>
                <th>Offload Quantity</th>
                <th>UI</th>
                <th>UM</th>
                <th>Delete</th>
            </thead>
        </table>
        <hr />
        <br />
    </div>

    <div class="modal fade" id="confirmDeleteModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>Confirm Deletion of Offloaded Item</b></h>
                </div>
                <div class="modal-body">
                    Are you sure you wish to delete this Offloaded item?
                    <br />
                    <br />
                    <label>OFFLOAD ID: </label>
                    <label id="offload_id_label" ></label>
                    <br />
                     <label>NIIN: </label>
                    <label id="niin_label"></label>
                    <br />
                    <label>OFFLOAD QUANTITY: </label>
                    <label id="quantity_label"></label>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="confirmDeleteBtn">Delete</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmArchiveModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>Confirm Archival of Offload Items</b></h>
                </div>
                <div class="modal-body" style="text-align:center">
                    You are about to archive all Offload items that have a document number!<br />
                     Make sure that all Offload documents (DD1348) have been printed out!<br /><br />
                     <b>Are you sure you want to continue?</b>
                    <br />
                    <br />
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="confirmArchiveBtn">Archive</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    
     <div class="modal fade" id="dd1348Modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header"> 
                    <table style="width:100%">  
                        <tr>    
                            <td style="width:33%">     
                                <h class="modal-title"><b>Update DD 1348 Information</b></h>   
                            </td>  
                            <td style="width:33%">
                                <div class="item-count">
                                    <input type="text" style="width: 30px; text-align: right" id="currentItem" value="" />
                                    <span id="items"class="items"></span>
                                </div> 
                            </td>
                            <td style="width:33%">                                                                     
                                <button type="button"  class="btn btn-info modal-header-btn" style="float: right; display:none"id="modalCancel">Cancel</button> 
                                <button type="button" class="btn btn-info modal-header-btn" style="float: right; display:none"id="modalOk">Ok</button> 
                                <button type="button" class="btn btn-info modal-header-btn" style="float:right"id="modalClose">Close</button> 
                                <button type="button" class="btn btn-info modal-header-btn" style="float:right"id="confirmPrintBtn">
                                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate load-icon" style="display:none"></span>
                                    <span class="clicked" style="display:none">Creating PDF</span>
                                    <span class="not-clicked">Print</span>
                                </button>    
                                <button type="button" class="btn btn-info modal-header-btn" style="float:right"id="confirmBlankPrintBtn">
                                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate load-icon" style="display:none"></span>
                                    <span class="clicked" style="display:none">Creating PDF</span>
                                    <span class="not-clicked">Print</span>
                                </button>     
                                <button type="button" class="btn btn-info modal-header-btn" style="float:right"id="docNumBtn">Create Doc Number</button>
                            </td>  
                        </tr>   
                    </table>      
                </div>
                <div class="modal-body" style="padding-top:0px">
                    <div class="row">
                        <div class="col-md-1 left-button">
                            <button class="btn btn-info btn-lg" type="button" id="ddLeft" style="padding: 5px 0px; height:100px; margin:0px auto" >
                                <span class="glyphicon glyphicon-chevron-left" style="font-size:20px"></span>
                            </button>
                        </div>
                        <div class="col-md-11" style="background-image: url('resources/DD1348.jpg'); background-size: 1123px 766px; background-repeat:no-repeat">
                            <div class="col-md-6" style="width: 643px; padding-right:0px">
                            <div class="row" style="padding: 132px 0px 0px 35px">
                                <div class="col-md-1" style="padding: 0px 0px 0px 5px; width:35px">
                                    <input type="text" class="form-field" style="width:27px" id="docIdent" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 5px; width:46px">
                                    <input type="text" class="form-field" style="width:41px" id="riFrom" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 1px; width:15px">
                                    <input type="text" class="form-field" style="width:14px" id="mAndS" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 11px; width:38px">
                                    <input type="text" class="form-field" style="width:27px" id="UI" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 5px; width: 65px; text-align:center">
                                    <input type="text" class="form-field" style="width: 60px; text-align:center" id="quantity" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 15px; width:30px">
                                    <input type="text" class="form-field" style="width:15px" id="SVC" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 0px; width:66px">
                                    <input type="text" class="form-field" style="width:66px" id="suppAddress" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 1px; width:15px">
                                    <input type="text" class="form-field" style="width:14px" id="SIG" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 0px; width:27px">
                                    <input type="text" class="form-field" style="width:27px" id="fund" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 1px; width:39px">
                                    <input type="text" class="form-field" style="width:39px" id="dist" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 5px; width:40px">
                                    <input type="text" class="form-field" style="width:35px" id="project" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 2px; width:28px">
                                    <input type="text" class="form-field" style="width:26px" id="PRI" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 2px; width:41px">
                                    <input type="text" class="form-field" style="width:40px" id="reqdDelDate" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 1px; width:29px">
                                    <input type="text" class="form-field" style="width:27px" id="ADV" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 5px; width:35px">
                                    <input type="text" class="form-field" style="width:30px" id="RI" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 0px; width:15px">
                                    <input type="text" class="form-field" style="width:15px" id="OP" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 0px; width:15px">
                                    <input type="text" class="form-field" style="width:15px" id="CC" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 0px; width:15px">
                                    <input type="text" class="form-field" style="width:15px" id="MGT" value=""/>
                                </div>
                                <div class="col-md-1" style="padding: 0px 0px 0px 0px; width:15px">
                                    <input type="text" class="form-field" style="width:12px" id="MCC" value=""/>
                                </div>                           
                            </div>
                            <div class="row" style="padding: 33px 0px 0px 160px">
                               <div class="col-md-1" style="width: 482px">
                                   <input type="text" class="form-field" style="width:450px"id="docNumber" value=""/>
                               </div>
                            </div>
                            <div class="row" style="padding:63px 0px 0px 160px">
                                <div class="col-md-1" style="padding-right:264px">
                                    <input type="text" class="form-field" id="NSN" disabled="disabled" value=""/>
                                    <div id="blankNSN">
                                        <table>
                                            <tr>
                                                <td><input type="text" class="form-field" style="width:50px" id="FSC" value=""/></td>
                                                <td><span>-</span></td>
                                                <td><input type="text" class="form-field" style="width:125px" id="NIIN" value=""/></td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                </div>
                                <div class="col-md-1" style="width:200px">
                                    <input type="text" class="form-field" style="margin-bottom: 21px"id="CAGE" value=""/>
                                    <input type="text" class="form-field" id="MSDS" value=""/>
                                </div>
                            </div>
                            <div class="row" style="padding: 30px 0px 0px 160px">
                                <div class="col-md-1" style="width:482px">
                                    <textarea rows="3" class="form-field" style="width: 450px; resize:none" id="RIC"></textarea>
                                </div>
                            </div>
                            <div class="row" style="padding:60px 0px 0px 50px">
                                <div class="col-md-1" style="width:592px">
                                    <textarea rows="3" class="form-field" style="width: 562px; resize:none" id="addData1"></textarea>
                                </div>
                            </div>
                            <div class="row" style="padding: 45px 0px 15px 50px">
                               <div class="col-md-1" style="width: 257px; margin-right:78px">
                                   <textarea rows="5" class="ship-info" style="width: 227px; resize:none" id="addData2"></textarea>
                               </div>
                               <div class="col-md-1" style="width:257px">
                                   <textarea rows="5" class="ship-info" style="width: 227px; resize:none" id="addData3"></textarea>
                               </div>
                            </div>
                        </div>
                            <div class="col-md-4" style="width: 416px; padding:0px">
                                <div class="row" style="padding:77px 0px 0px 219px">
                                    <div class="col-md-1" style="width:105px">
                                        <input type="text" class="ship-info" style="width:75px" id="shipFrom" value=""/>
                                    </div>
                                    <div class="col-md-1" style="width:105px">
                                        <input type="text" class="ship-info" style="width:75px" id="ship" value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:44px 0px 0px 20px">
                                    <div class="col-md-1" style="width:102px">
                                        <input type="text" class="form-field" style="width: 72px; text-align: right" id="unitPrice"value=""
                                            data-toggle="tooltip" data-trigger="manual" data-placement="top" title="Invalid Amount"/>
                                    </div>
                                    <div class="col-md-1"  style="width:110px">
                                        <input type="text" class="form-field" style="width: 80px; text-align: right"id="totalPrice"value=""
                                            data-toggle="tooltip" data-trigger="manual" data-placement="top" title="Invalid Amount"/>
                                    </div>
                                    <div class="col-md-1" style="width:198px">
                                        <input type="text" class="form-field" id="markFor"value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="padding-right: 0px; width:78px ">
                                        <input type="text" class="form-field" style="width: 62px; font-size:11px" id="docDate"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:95px">
                                        <input type="text" class="form-field" style="width:79px" id="NMFC"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:96px">
                                        <input type="text" class="form-field" style="width:81px" id="frtRate"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:108px">
                                        <input type="text" class="form-field" style="width:93px" id="typeCargo"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding:0px 0px 0px 3px; width:50px">
                                        <input type="text" class="form-field" style="width:47px" id="PS"value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="padding-right: 0px; width:95px ">
                                        <input type="text" class="form-field" style="width:80px" id="qtyRecd"value=""/>
                                    </div>
                                    <div class="col-md-1"  style="padding:0px 0px 0px 2px; width:35px ">
                                        <input type="text" class="form-field" style="width:33px" id="UP"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:119px ">
                                        <input type="text" class="form-field" style="width:104px" id="unitWeight"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:84px ">
                                        <input type="text" class="form-field" style="width:69px" id="unitCube"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:57px ">
                                        <input type="text" class="form-field" style="width:42px" id="UFC"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding:0px 0px 0px 2px; width:37px ">
                                        <input type="text" class="form-field" style="width:35px" id="SL"value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="width:427px">
                                        <input type="text" class="form-field" style="width:398px" id="freightNomen" value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="width:427px">
                                        <input type="text" class="form-field" style="width:398px" id="itemNomen" value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="padding-right: 0px; width:77px ">
                                        <input type="text" class="form-field" style="width:62px" id="tyCont" value=""/>
                                    </div>
                                    <div class="col-md-1"  style="padding-right: 0px; width:102px ">
                                        <input type="text" class="form-field" style="width:87px" id="noCont" value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:134px ">
                                        <input type="text" class="form-field" style="width:119px" id="totalWeight" value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:115px ">
                                        <input type="text" class="form-field" style="width:99px" id="totalCube" value=""/>
                                    </div>
                                    </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="padding-right: 0px; width:313px ">
                                        <input type="text" class="form-field" style="width:298px" id="recdBy"value=""/>
                                    </div>
                                    <div class="col-md-1" style="padding-right: 0px; width:114px ">
                                        <input type="text" class="form-field" style="width:99px" id="recdDate"value=""/>
                                    </div>
                                </div>
                                <div class="row" style="padding:22px 0px 0px 3px">
                                    <div class="col-md-1" style="width:427px">
                                        <textarea rows="12" class="form-field" style="width: 397px; resize:none" id="addData4"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 right-button">
                            <button class="btn btn-info btn-lg" type="button" id="ddRight" style="padding: 5px 0px; height: 100px; margin:0px auto">
                                <span class="glyphicon glyphicon-chevron-right" style="font-size:20px"></span>
                            </button>    
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</asp:Content>