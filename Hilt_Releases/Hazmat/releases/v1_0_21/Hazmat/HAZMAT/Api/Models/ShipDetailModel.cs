﻿
namespace HAZMAT.Api.Models
{
    public class ShipDetailModel
    {
        public string Uic { get; set; }
        public string ShipName { get; set; }
        public string Poc { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}