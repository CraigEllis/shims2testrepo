﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class UserModel
    {
        public int user_id { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public Workcenter workcenter { get; set; }
    }

    public class UserUpdateModel
    {
        public int id { get; set; }
        public string action { get; set; }
        public UserModel data { get; set; }
    }

}