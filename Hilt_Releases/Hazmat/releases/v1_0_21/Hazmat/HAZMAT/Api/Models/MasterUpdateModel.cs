﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class MasterUpdateModel
    {
        public int id { get; set; }
        public string action { get; set; }
        public MasterUpdateData data { get; set; }
    }

    public class MasterUpdateData
    {
        public int? quantity { get; set; }
        public DateTime? shelf_life_date {get; set;}
        public DateTime? inv_date { get; set; }
        public int? fsc { get; set; }
        public string niin { get; set; }
        public string wid { get; set; }
        public string location_name { get; set; }
    }

    public class MasterTableRowModel
    {
        public int id;
        public int? quantity { get; set; }
        public string shelf_life_date { get; set; }
        public string inv_date { get; set; }
        public int? fsc { get; set; }
        public string niin { get; set; }
        public string wid { get; set; }
        public string location_name { get; set; }
    }
}