﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class ShipAdministrationModel
    {
        public string UIC {get; set;}
        public string ShipName {get; set;}
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string SubType { get; set; }
        public string HullNumber { get; set; }
        public string ACTCode { get; set; }
        public string BASE { get; set; }
        public string SNDL { get; set; }
        public string POCTitle { get; set; }
        public string POCName { get; set; }
        public string POCAddress { get; set; }
        public string POCPhone { get; set; }
        public string POCEmail { get; set; }
    }
}