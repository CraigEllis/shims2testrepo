﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api
{
    public class UpdateSMCLViewModel
    {
        public string Niin { get; set; }
        public string MsdsNum { get; set; }
        public string Remarks { get; set; }
    }
}