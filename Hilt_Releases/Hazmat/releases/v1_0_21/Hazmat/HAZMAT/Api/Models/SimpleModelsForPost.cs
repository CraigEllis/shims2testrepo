﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class InventoryIdModel
    {
        public int InventoryId;
    }

    public class OffloadIdModel
    {
        public int OffloadId;
    }

    public class AddOffloadModel
    {
        public int InventoryId;
        public int Quantity;
    }

    public class NewCageModel
    {
        public int id { get; set; }
        public string action { get; set; }
        public NewCageData data { get; set; }
    }

    public class NewCageData
    {
        public string cage { get; set; }
        public string manufacturer { get; set; }
        public string niin { get; set; }
    }

    public class LocationModel
    {
        public int location_id { get; set; }
        public string location_name { get; set; }
        public Workcenter workcenter { get; set; }
    }

    public class LocationUpdateModel
    {
        public int id { get; set; }
        public string action { get; set; }
        public LocationModel data { get; set; }
    }
}