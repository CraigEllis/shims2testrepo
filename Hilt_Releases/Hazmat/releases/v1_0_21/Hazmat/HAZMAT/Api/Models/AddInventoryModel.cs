﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class AddInventoryModel
    {
        public int? fsc;
        public string niin;
        public string name;
        public int? ui;
        public string um;
        public string spmig;
        public string specs;
        public string remarks;
        public string notes;
        public int? usage_category_id;
        public int? slc_id;
        public int? slac_id;
        public int? hcc_id;
        public int? smcc_id;
        public int? inventory_detail_id;
        public int? allowance_qty;
        public bool hasSfr;
    }
}