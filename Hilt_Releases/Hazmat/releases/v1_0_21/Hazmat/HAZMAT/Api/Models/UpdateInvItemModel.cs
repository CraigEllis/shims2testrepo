﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class UpdateInvItemModel
    {
        public int? id { get; set; }
        public string niin { get; set; }
        public int? ui { get; set; }
        public string um { get; set; }
        public int? slc { get; set; }
        public int? slac { get; set; }
        public int? hcc { get; set; }
        public string notes { get; set; }
        public int? qtyAllow { get; set; }
        public string name { get; set; }
        public string spmig { get; set; }
        public string specs { get; set; }
        public int? smcc { get; set; }
        public int? fsc { get; set; }
        public int? usage_category { get; set; }
    }
}