﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{

    //classes for dealing with the editor table
    public class SFRUpdateModel
    {
        public int id { get; set; }
        public string action { get; set; }
        public SFRUpdateRowData data { get; set; }
    }

    public class SFRRowModel
    {
        public int? sfr_id { get; set; }
        public string date_submitted { get; set; }
        public string date_mailed { get; set; }
        public string cage { get; set; }
        public string item_name { get; set; }
        public int? fsc { get; set; }
        public string niin { get; set; }
    }

    public class SFRUpdateRowData
    {
        public DateTime? date_submitted { get; set; }
        public DateTime? date_mailed { get; set; }
        public string cage { get; set; }
        public string item_name { get; set; }
        public int? fsc { get; set; }
        public string niin { get; set; }
    }

    //representation of sfr data in database
    public class SFRModel
    {
        public int? sfr_id { get; set; }
        public int? sfr_type { get; set; }
        public DateTime? date_submitted { get; set; }
        public DateTime? date_mailed { get; set; }
        public int? completed_flag { get; set; }
        public int? ship_id { get; set; }
        public int? ui_id { get; set; }
        public string username { get; set; }
        public string manufacturer { get; set; }
        public string cage { get; set; }
        public string part_no { get; set; }
        public string mfg_city { get; set; }
        public string mfg_address { get; set; }
        public string mfg_state { get; set; }
        public string mfg_zip { get; set; }
        public string mfg_poc { get; set; }
        public string mfg_phone { get; set; }
        public string system_equipment_material { get; set; }
        public string method_of_application { get; set; }
        public string proposed_usage { get; set; }
        public string negative_impact { get; set; }
        public string special_training { get; set; }
        public string precautions { get; set; }
        public string properties { get; set; }
        public string comments { get; set; }
        public string advantages { get; set; }
        public int? inventory_detail_id { get; set; }
        public string item_name { get; set; }
        public int? fsc { get; set; }
        public string niin { get; set; }
        public string specification_number { get; set; }
        public bool msds_attached { get; set; }
    }

    public class SimpleSfr
    {
        public string fsc { get; set; }
        public string niin { get; set; }
        public string item_name { get; set; }
        public int? ui { get; set; }
        public int? inventory_detail_id { get; set; }
        public string user;
        public DateTime submitted_date;
    }
}