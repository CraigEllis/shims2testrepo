﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HAZMAT.Api.Models
{
    public class UpdateOnHandModel
    {
        public int id { get; set; }
        public string action { get; set; }
        public UpdateOnHandData data { get; set; }
    }

    public class UpdateOnHandData
    {
        public Location location { get; set; }
        public Workcenter workcenter { get; set; }
        public int? inventory_detail_id { get; set; }
        public string cage { get; set; }
        public string manufacturer { get; set; }
        public int? qty { get; set; }
        public DateTime? inv_date { get; set; }
        public DateTime? onboard_date { get; set; }
        public DateTime? expiration_date { get; set; }
    }

    public class UpdateOnHandRowModel
    {
        public int id { get; set; }
        public int inventory_detail_id { get; set; }
        public Location location { get; set; }
        public string cage { get; set; }
        public string manufacturer { get; set; }
        public int? qty { get; set; }
        public Workcenter workcenter { get; set; }
        public string inv_date { get; set; }
        public string onboard_date { get; set; }
        public string expiration_date { get; set; }
    }

    public class Workcenter
    {
        public int? workcenter_id { get; set; }
        public string workcenter_name { get; set; }
    }

    public class Location
    {
        public int? location_id { get; set; }
        public string location_name { get; set; }
    }
}