﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace HAZMAT.Api.Controllers
{
    public class SFRController : ApiController
    {
        private DatabaseManager dbManager;

        public SFRController()
        {
            this.dbManager = new DatabaseManager();
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadSFRTable()
        {
            var results = dbManager.getSFRCollection();
            return new { aaData = results };
        }

        [HttpGet]
        public Object GetBlankSFRModal()
        {
            var results = dbManager.GetBlankSfrModal();
            var uis = dbManager.getAllUis();
            return new {shipData = results, uis = uis};
        }

        [HttpPost]
        public Object UpdateSFRStatus(SFRUpdateModel sfr)
        {
            var data = dbManager.UpdateSFRStatus(sfr);
            return new { row = data[0] };
        }

        [HttpPost]
        public string UpdateSFR(SFRModel sfr)
        {
            sfr.date_submitted = DateTime.Now;
            sfr.completed_flag = 0; //it's not completed, hasn't even been mailed yet
            sfr.username = this.User.Identity.Name;
            var response = dbManager.updateSFR(sfr);
            return response;
        }

        [HttpGet]
        public Object GetSFRById(int sfr_id)
        {
            var sfr = dbManager.getSfrById(sfr_id);
            return new { data = sfr };
        }

        [HttpGet]
        public HttpResponseMessage GetSfrPdf(int sfr_id)
        {
            SFRBuilder generator = new SFRBuilder();
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = generator.GetPdf(sfr_id);
            result.Content = new ByteArrayContent(stream.GetBuffer());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = "SFR.pdf" };
            return result;
        }
    }
}