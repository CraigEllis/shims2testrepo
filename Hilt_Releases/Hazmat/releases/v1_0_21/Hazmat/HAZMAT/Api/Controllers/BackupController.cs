﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HAZMAT.Api
{
    public class BackupController : ApiController
    {
        private DatabaseManager dbManager;

        public BackupController()
        {
            dbManager = new DatabaseManager();
        }

        [HttpGet]
        public Object GetBackupSettings()
        {
            var data = dbManager.GetBackupSettings();
            var backupLocation = data.Rows[0]["backup_location"].ToString();
            var files = Directory.EnumerateFiles(backupLocation).ToList();
            List<string> fileNames = new List<string>();
            foreach (var path in files)
            {
                fileNames.Add(Path.GetFileName(path));
            }
            return new { data = data, baks = fileNames };
        }

        [HttpGet]
        public bool RunBackup()
        {
            var user = dbManager.getUserRole(this.User.Identity.Name);
            //they should never be at the backup modal, but in case of funny business it won't work unless they are level 1
            if (user == "LEVEL 1")
            {

                try
                {
                    dbManager.ExecuteDatabaseBackup();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        [HttpGet]
        public bool RestoreBackup(string file, string backupLocation)
        {
            var user = dbManager.getUserRole(this.User.Identity.Name);
            if (user == "LEVEL 1")
            {
                try
                {
                    dbManager.RestoreBackup(file, backupLocation);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        [HttpGet]
        public void UpdateBackupSettings(int frequency)
        {
            var user = dbManager.getUserRole(this.User.Identity.Name);
            if (user == "LEVEL 1")
            {
                dbManager.UpdateBackupFrequency(frequency);
            }
        }
    }
}