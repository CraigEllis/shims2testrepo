﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.IO.Compression;
using System.IO;

namespace HAZMAT.Api
{
    public class MSDSController : ApiController
    {
        protected DatabaseManager dbManager;

        public MSDSController()
        {
            this.dbManager = new DatabaseManager();
        }

        [HttpPost]
        public string ImportMSDS()
        {
            string successDetails = "MSDS successfully added and updated.";
            string extractPath = AppDomain.CurrentDomain.BaseDirectory + "resources\\MSDS\\";
            if (!Directory.Exists(extractPath)) { Directory.CreateDirectory(extractPath); }

            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["MSDS"];
                var zipPath = extractPath + httpPostedFile.FileName;

                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)
                    if (httpPostedFile.ContentType != "application/zip")
                    {
                        //return "Error: You must upload a file of type .zip.";
                    }
                    //else
                    //{
                        httpPostedFile.SaveAs(zipPath);
                    //}

                    
                    //ZipFile.ExtractToDirectory(zipPath, extractPath);                   
                }
                else { return "No file to upload!"; }
            }
            else{ return "No file to upload!"; }

            return successDetails;
        }

    }
}