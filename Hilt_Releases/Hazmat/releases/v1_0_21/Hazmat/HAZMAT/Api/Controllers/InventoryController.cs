﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Services;

namespace HAZMAT.Api
{
    public class InventoryController : ApiController
    {
        private DatabaseManager dbManager;

        public InventoryController()
        {
            this.dbManager = new DatabaseManager();
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadInventoryTable(string niin, string itemName, string location, string workcenter, string spmig, string specs)
        {
            var results = dbManager.getFilteredInventoryCollection(niin, itemName, location, workcenter, spmig, specs, null);
            return new { aaData = results };
        }

        [HttpGet]
        public int GetTotalQty(int inventory_id)
        {
            return dbManager.getTotalInventoryQty(inventory_id);
        }

        [HttpGet]
        public int GetTotalQty(string niin)
        {
            return dbManager.getTotalInventoryQty(niin);
        }


        [HttpGet]
        public string GetUserRole()
        {
            return dbManager.getUserRole(this.User.Identity.Name);
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadDropdown()
        {
            var usageCategories = dbManager.getUsageCategories();
            var shelfLifeCodes = dbManager.getSlcs();
            var slacs = dbManager.getSlacs();
            var smccs = dbManager.getSmccs();
            var locations = dbManager.getLocations();
            var workcenters = dbManager.getWorkCenters();
            var spmigs = dbManager.getInvSPMIG();
            var niins = dbManager.getInvNIINs();
            var descriptions = dbManager.getInvItemDescriptions();
            var specs = dbManager.getInvSpecs();
            var hccs = dbManager.getAllHccs();
            var uis = dbManager.getAllUis();
            return new { usageCategories = usageCategories, shelfLifeCodes = shelfLifeCodes, slacs = slacs, hccs=hccs, smccs = smccs, locations = locations, workcenters = workcenters, spmigs = spmigs, specs = specs, descriptions = descriptions, niins = niins, uis=uis };
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadAddDropdowns()
        {
            var usageCategories = dbManager.getUsageCategories();
            var shelfLifeCodes = dbManager.getSlcs();
            var slacs = dbManager.getSlacs();
            var smccs = dbManager.getSmccs();
            var niins = dbManager.getAllNIINs();
            var hccs = dbManager.getAllHccs();
            var uis = dbManager.getAllUis();
            return new { usageCategories = usageCategories, shelfLifeCodes = shelfLifeCodes, slacs = slacs, smccs = smccs, niins = niins, hccs = hccs, uis = uis };
        }

        // Return SMCL details to display in modal
        [HttpGet]
        public Object GetInventoryDetailsModal(int inventory_id)
        {
            var results = dbManager.getInventoryDetailsForModal(inventory_id);
            var totalQuantity = dbManager.getTotalInventoryQty(results.niin);
            var user = this.User.Identity.Name;
            var userRole = dbManager.getUserRole(user);
            return new { aaData = results, totalQuantity = totalQuantity, userRole = userRole };
        }

        [HttpGet]
        public Object LoadInventoryDetailsTable(int? inventory_id)
        {
            if (inventory_id == null)
            {
                return new { aaData = new { } };
            }
            object data = null;
            var user = this.GetUserRole();
            //we just hide this data from users without appropriate permissions
            if (user=="LEVEL 1")
            {
                data = dbManager.getInventoryDetailsForGrid(inventory_id, null);
            }
            else if (user == "LEVEL 2")
            {
                var workcenterData = dbManager.getWorkcenterForUser(this.User.Identity.Name);
                int workcenter_id = Convert.ToInt32(workcenterData.Rows[0]["workcenter_id"]);
                data = dbManager.getInventoryDetailsForGrid(inventory_id, workcenter_id);
            }
            return new { aaData = data };
        }

        [HttpGet]
        public Object GetFilteredDropdowns(string niin)
        {
            if (!String.IsNullOrEmpty(niin))
            {
                var cages = dbManager.getFilteredCages(niin);
                var manufacturers = dbManager.getFilteredManufacturers(niin);
                return new { cages = cages, manufacturers = manufacturers };
            }
            else return new { };
        }

        // POST api/<controller>
        [HttpPost]
        public Object UpdateInventoryData(UpdateOnHandModel model)
        {
            var user = this.GetUserRole();
            var workcenter = dbManager.getWorkcenterForUser(this.User.Identity.Name);
            if ((user == "LEVEL 1") || ((user == "LEVEL 2") && (workcenter.Rows[0]["workcenter_id"].ToString() == model.data.workcenter.workcenter_id.ToString())))
            {
                UpdateOnHandRowModel data;
                if (model.action == "edit")
                {
                    dbManager.UpdateInventoryOnHand(model);
                    data = dbManager.getUpdatedOnHandItem(model.id);
                    return new { row = data };
                }
                else
                {
                    int? result = dbManager.AddInventoryItem(model);
                    if (result > 0)
                    {
                        data = dbManager.getUpdatedOnHandItem((int)result);
                        return new { row = data };
                    }
                    else
                    {
                        data = dbManager.getUpdatedOnHandItem(model.id);
                        return new { error = "Failed to add inventory item.", row = data };
                    }
                }
            }
            return "error";

        }

        [HttpPost]
        public Object UpdateCAGE(NewCageModel model)
        {
            dbManager.AddCageToMfgCatalog(model);
            return new {row = ""};
        }

        [HttpPost]
        public int? UpdateInvItem(UpdateInvItemModel model)
        {
            var user = this.GetUserRole();
            if (user == "LEVEL 1")
            {
                return dbManager.updateInvItem(model);
            }
            else return null;
        }

        [HttpGet]
        public Object GetDeleteModal(int inventory_id)
        {
            var results = dbManager.getInventoryItemForDeleteConfirm(inventory_id);
            return new { aaData = results };
        }

        [HttpPost]
        public void DeleteRecord(InventoryIdModel inventory_id)
        {
            dbManager.deleteInventory(inventory_id.InventoryId);
        }

        [HttpGet]
        public Object GetOffloadModal(int inventory_id)
        {
            var results = dbManager.getInventoryItemForOffloadConfirm(inventory_id);
            return new { aaData = results };
        }


        [HttpGet]
        public Object AttemptToPopulateInventoryAdd(string niin)
        {
            if (niin == null || niin == "")
            {
                var empty = new List<string>();
                return new {smclData = empty, data =  empty};
            }
            var user = this.GetUserRole();
            //get the SMCL data
            var smclDataForItem = dbManager.AttemptToGetSMCLInformation(niin);
            if (string.IsNullOrEmpty(smclDataForItem.niin) && smclDataForItem.hasSfr == false)
            {
                return "new";
            }
            var onHandData = new List<UpdateOnHandRowModel>();
            //get the on hand information for this niin if available
            if (smclDataForItem.inventory_detail_id != null)
            {
                if (user == "LEVEL 1")
                {
                    onHandData = dbManager.getOnHandInfoByInventoryDetailId(smclDataForItem.inventory_detail_id, null);
                }
                else if (user == "LEVEL 2")
                {
                    var workcenterData = dbManager.getWorkcenterForUser(this.User.Identity.Name);
                    int workcenter_id = Convert.ToInt32(workcenterData.Rows[0]["workcenter_id"]);
                    onHandData = dbManager.getOnHandInfoByInventoryDetailId(smclDataForItem.inventory_detail_id, workcenter_id);
                }
            }
            var totalQuantity = dbManager.getTotalInventoryQty(niin);
            user = this.User.Identity.Name;
            var userRole = dbManager.getUserRole(user);
            return new { smclData = smclDataForItem, data = onHandData, totalQuantity = totalQuantity, userRole = userRole };
        }

        
        [HttpGet]
        public Object GetSLCs()
        {
            var results = dbManager.getSlcs();
            return new { data = results };
        }

        [HttpPost]
        public void AddOffloadItem(AddOffloadModel newOffloadItem)
        {
            dbManager.AddOffloadItem(newOffloadItem.InventoryId, newOffloadItem.Quantity);
        }

        [HttpPost]
        public void AddSfr(SimpleSfr sfr)
        {
            sfr.submitted_date = DateTime.Now;
            sfr.user = this.User.Identity.Name;
            dbManager.AddSimpleSfr(sfr);
        }

        // GET api/<controller>/ACMLabels
        [HttpGet]
        [WebMethod(EnableSession=true)]
        public HttpResponseMessage PrintInventoryDetails(int inventory_onhand_id)
        {
            ReportsPdfGenerator generator = new ReportsPdfGenerator();
            string filePath = generator.PrintInventoryDetails(inventory_onhand_id, this.User.Identity.Name);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = "InventoryDetails" };

            return result;
        }

        [HttpGet]
        public string CheckIncompatibility(int inventory_id, int location_id)
        {
            return dbManager.CheckIncompatibility(inventory_id, location_id);
        }

        [HttpGet]
        public List<string> getMSDSFiles(string fsc, string niin, string cage)
        {
            return dbManager.getMSDSFileName(fsc, niin, cage);
        }
    }
}