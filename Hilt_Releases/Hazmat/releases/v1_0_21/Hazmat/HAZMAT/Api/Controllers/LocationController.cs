﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HAZMAT.Api.Controllers
{
    public class LocationController : ApiController
    {
        private DatabaseManager dbManager;

        public LocationController()
        {
            dbManager = new DatabaseManager();
        }

        [HttpGet]
        public Object GetWorkcenters()
        {
            var results = dbManager.getWorkCenters();
            return new { workcenters = results };
        }

        [HttpGet]
        public Object LoadLocationTable()
        {
            var results = dbManager.GetLocationList();
            return new { data = results };
        }

        [HttpPost]
        public Object UpdateLocation(LocationUpdateModel model)
        {
            if (model.action == "edit")
            {
                var result = dbManager.UpdateLocation(model);
                return new { row = result };
            }
            else
            {
                var result = dbManager.AddLocation(model);
                return new { row = result };
            }
        }

        [HttpDelete]
        public void DeleteLocation(int location_id)
        {
            dbManager.DeleteLocation(location_id);
        }
    }
}