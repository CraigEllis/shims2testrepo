﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace HAZMAT.Api.Controllers
{
    public class ShipManagementController : ApiController
    {
        private DatabaseManager dbManager;
        public ShipManagementController()
        {
            dbManager = new DatabaseManager();
        }

        [HttpGet]
        public Object GetUICs()
        {
            var UICList = dbManager.GetUICs();
            var currentUIC = dbManager.GetCurrentUIC();
            return new { uics = UICList, currentUic = currentUIC };
        }

        [HttpGet]
        public Object GetCurrentShipData(string uic)
        {
            var shipData = dbManager.GetCurrentShipData(uic);
            return new { shipData = shipData };
        }

        [HttpPost]
        public string UploadLogo()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)
                   /* if (httpPostedFile.ContentType != "image/jpeg")
                    {
                        return "Error: You must upload a file of type .jpg.";
                    }*/

                    // Get the complete file path
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/upload"), httpPostedFile.FileName);
                    var newCrestPath = Path.Combine(HttpContext.Current.Server.MapPath("~/upload"), "newCrest.png");

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    //Save filename in the database. 
                    dbManager.AddShipLogo(httpPostedFile.FileName);

                    if (httpPostedFile.ContentType != "image/png")
                    {
                        System.Drawing.Image image1 = System.Drawing.Image.FromFile(fileSavePath);
                        image1.Save(newCrestPath, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else
                    {
                        System.IO.File.Copy(fileSavePath, newCrestPath, true);
                    }

                    if(System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/images/crest.png"))){
                        System.IO.File.Delete(HttpContext.Current.Server.MapPath("~/images/crest.png"));
                    }

                    System.IO.File.Copy(newCrestPath, HttpContext.Current.Server.MapPath("~/images/crest.png"));
                }
                else
                {
                    return "No file to upload!";
                }
            }
            else
            {
                return "No file to upload!";
            }
            return "Successful upload.";
        }

        [HttpPost]
        public string UploadShipImage()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)
                    /*if (httpPostedFile.ContentType != "image/jpeg")
                    {
                        return "Error: You must upload a file of type .jpg.";
                    }*/

                    // Get the complete file path
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/upload"), httpPostedFile.FileName);
                    var newBackgroundPath = Path.Combine(HttpContext.Current.Server.MapPath("~/upload"), "background.jpg");

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    //Save filename in the database. 
                    dbManager.AddShipImage(httpPostedFile.FileName);

                    if (httpPostedFile.ContentType != "image/jpeg")
                    {
                        System.Drawing.Image image1 = System.Drawing.Image.FromFile(fileSavePath);
                        image1.Save(newBackgroundPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else
                    {
                        System.IO.File.Copy(fileSavePath, newBackgroundPath, true);
                    }

                    if (System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/images/background.jpg")))
                    {
                        System.IO.File.Delete(HttpContext.Current.Server.MapPath("~/images/background.jpg"));
                    }

                    System.IO.File.Copy(newBackgroundPath, HttpContext.Current.Server.MapPath("~/images/background.jpg"));
                }
                else
                {
                    return "No file to upload!";
                }
            }
            else
            {
                return "No file to upload!";
            }
            return "Successful upload.";
        }

        [HttpPost]
        public void UpdateShipInfo(ShipAdministrationModel model)
        {
            dbManager.UpdateCurrentShipData(model);
        }

        [HttpGet]
        public Object GetLastIncUpdate()
        {
            return dbManager.getLastIncUploadDate();
        }

        [HttpPost]
        public string UploadIncompatibles()
        {
            string successDetails;
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["Incompatibles"];

                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)
                    if (httpPostedFile.ContentType != "application/vnd.ms-excel")
                    {
                        return "Error: You must upload a file of type .xls.";
                    }

                    // Create a temp file on the server
                    String saveFilePath = Path.GetTempFileName();
                    httpPostedFile.SaveAs(saveFilePath);

                    successDetails = dbManager.updateIncompatibles(saveFilePath);
                }
                else
                {
                    return "No file to upload!";
                }
            }
            else
            {
                return "No file to upload!";
            }
            return successDetails;
        }

    }
}