﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace HAZMAT.Api
{

    public class OffloadArchivedController : ApiController
    {
        private DatabaseManager dbManager;

        public OffloadArchivedController()
        {
            this.dbManager = new DatabaseManager();
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadOffloadArchivedTable()
        {
            var results = dbManager.getFilteredOffloadArchivedCollection();
            return new { aaData = results };
        }

        [HttpGet]
        public Object GetShipTo()
        {
            var uic = dbManager.getShipToUic();
            var name = uic.Select(t => t.ToString()).Select(s => dbManager.getShipToName(s)).ToList();

            return new { uic = uic, name = name };
        }

        [HttpGet]
        public Object GetShipToDetails(string uic)
        {
            ShipDetailModel shipDetails = new ShipDetailModel();
            shipDetails = dbManager.getShipToDetails(uic);
            return new { uic = shipDetails.Uic, shipname = shipDetails.ShipName, poc = shipDetails.Poc, address = shipDetails.Address, telephone = shipDetails.Telephone };
        }

        [HttpGet]
        public Object GetShipFromDetails()
        {
            ShipDetailModel shipDetails = new ShipDetailModel();
            string name = dbManager.getCurrentShipName();
            shipDetails = dbManager.getShipFromDetails(name);
            return new { uic = shipDetails.Uic, name = shipDetails.ShipName, poc = shipDetails.Poc, address = shipDetails.Address };
        }

        [HttpGet]
        public HttpResponseMessage CSV()
        {
            GenerateDD1348 csv = new GenerateDD1348();
            DataTable offloadArchivedDetails = new DataTable();
            offloadArchivedDetails = dbManager.getOffloadArchivedItemDetails(0);
            offloadArchivedDetails.DefaultView.Sort = "ArchivedId";
            return csv.generateCSV(offloadArchivedDetails);
        }

        [HttpGet]
        public string GetOffloadArchivedItems()
        {
            List<long> ids = new List<long>();
            ids = dbManager.getOffloadArchivedIds();
            string archivedItems = "{";

            for (int i = 0; i < ids.Count; i++)
            {
                if (i > 0) { archivedItems += ","; }
                archivedItems += i.ToString() + ":" + ids[i].ToString();
            }

            archivedItems += "}";
            return archivedItems;
        }

        [HttpGet]
        public Object GetOffloadArchivedItemDetails(long id)
        {
            var details = dbManager.getOffloadArchivedItemDetails(id);
            return new { details = details };
        }

        [HttpPost]
        public void AddOffloadArchivedItem()
        {
            dbManager.AddOffloadArchivedItem();
        }

        [HttpGet]
        public Object createPDF(string uic)
        {
            GenerateDD1348 dd1348 = new GenerateDD1348();
            DataTable offloadArchivedDetails = new DataTable();
            offloadArchivedDetails = dbManager.getOffloadArchivedItemDetails(0);
            offloadArchivedDetails.DefaultView.Sort = "ArchivedId";

            return new { relPath = dd1348.generatePdf(uic, offloadArchivedDetails) };
        }

        [HttpGet]
        public Object printArchive()
        {
            ReportsGenerator archivePrint = new ReportsGenerator();
            DataTable offloadArchivedDetails = new DataTable();
            offloadArchivedDetails = dbManager.getFilteredOffloadArchivedCollection();
            DataView dv = offloadArchivedDetails.DefaultView;
            dv.Sort = "archived_id DESC";
            DataTable sortedTable = dv.ToTable();

            return new {relPath = archivePrint.GenerateOffloadArchivedReport(sortedTable)};
        }
    }
}