﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HAZMAT.Api
{
    public class HomeScreenController : ApiController
    {

        private DatabaseManager dbManager;

        public HomeScreenController()
        {
            this.dbManager = new DatabaseManager();
        }

        // Return info to populate dropdowns & tip of the day
        [HttpGet]
        public Object LoadInitialScreen()
        {
            var niins = dbManager.getAllNIINs();
            var tip = dbManager.getTipOfTheDay();
            var storageTypes = dbManager.getStorageTypes();
            var usageCategories = dbManager.getUsageCategories();
            var shelfLifeCodes = dbManager.getSlcs();
            var slacs = dbManager.getSlacs();
            var smccs = dbManager.getSmccs();
            return new { niins = niins, storageTypes = storageTypes, usageCategories = usageCategories, shelfLifeCodes = shelfLifeCodes, slacs = slacs, smccs = smccs, tip=tip };
        }

        // Return SMCL details to display in modal
        [HttpGet]
        public Object GetModal(string niin)
        {
            var results = dbManager.getSMCLByNiin(niin);
            var user = this.User.Identity.Name;
            var userRole = dbManager.getUserRole(user);
            return new { aaData = results, userRole = userRole };
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}