﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

namespace HAZMAT.Api.Controllers
{
    public class MasterUpdateController : ApiController
    {
        private DatabaseManager dbManager;

        public MasterUpdateController()
        {
            this.dbManager = new DatabaseManager();
        }
        [HttpGet]
        public Object LoadDropdowns()
        {
            object locations;
            object workcenters;
            if (dbManager.isWorkCenterUser(this.User.Identity.Name))
            {
                locations = dbManager.getLocationsForUser(this.User.Identity.Name);
                workcenters = dbManager.getWorkcenterForUser(this.User.Identity.Name);
            }
            else if (dbManager.isPlainUser(this.User.Identity.Name))
            {
                locations = null;
                workcenters = null;
            }
            else
            {
                locations = dbManager.getLocations();
                workcenters = dbManager.getWorkCenters();
            }
            return new {locations = locations, workcenters = workcenters};
        }


        // GET api/<controller>
        [HttpGet]
        public Object LoadMasterTable(string location, string workcenter)
        {
            var results = dbManager.getMasterUpdateData(null, workcenter, location);
            return new { aaData = results };
        }

        [HttpPost]
        public Object MasterUpdate(MasterUpdateModel model)
        {
            dbManager.masterUpdate(model);
            var results = dbManager.getMasterUpdateData(model.id, null, null);
            return new { row = results[0] };
        }

        [HttpGet]
        public string GenerateChecklist(string type, string sort)
        {
            ReportsGenerator checklist = new ReportsGenerator();

            string relpath = checklist.GenerateInventoryChecklist(type, sort);
            return relpath;   
        }

    }
}