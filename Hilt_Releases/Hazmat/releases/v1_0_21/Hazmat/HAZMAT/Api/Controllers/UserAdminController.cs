﻿using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HAZMAT.Api.Controllers
{
    public class UserAdminController : ApiController
    {
        private DatabaseManager dbManager;

        public UserAdminController()
        {
            dbManager = new DatabaseManager();
        }

        [HttpGet]
        public Object GetWorkcenters()
        {
            var results = dbManager.getWorkCenters();
            return new { workcenters = results };
        }

        [HttpGet]
        public Object LoadUserTable()
        {
            var results = dbManager.GetUserList();
            return new { data = results };
        }

        [HttpPost]
        public Object UpdateUser(UserUpdateModel model)
        {
            if (model.action == "edit")
            {
                var result = dbManager.UpdateUser(model);
                return new { row = result };
            }
            else
            {
                var result = dbManager.AddUser(model);
                return new { row = result };
            }
        }

        [HttpDelete]
        public void DeleteUser(int user_id)
        {
            dbManager.DeleteUser(user_id);
        }
    }
}