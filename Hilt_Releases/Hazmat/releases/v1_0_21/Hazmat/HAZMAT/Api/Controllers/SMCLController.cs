﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace HAZMAT.Api
{
    public class SMCLController : ApiController
    {
        protected DatabaseManager dbManager;

        public SMCLController()
        {
            this.dbManager = new DatabaseManager();
        }

        // Return info to populate dropdowns & tip of the day
        [HttpGet]
        public Object LoadInitialScreen()
        {
            var niins = dbManager.getAllNIINs();
            var usageCategories = dbManager.getUsageCategories();
            var smccs = dbManager.getSmccs();
            var descriptions = dbManager.getAllItemDescriptions();
            var specs = dbManager.getAllSpecs();
            var groups = dbManager.getAllInventoryGroups();
            var manufacturers = dbManager.getAllManufacturers();
            return new { niins = niins, usageCategories = usageCategories, smccs = smccs, descriptions = descriptions, specs=specs, groups = groups, manufacturers = manufacturers };
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadSMCLTable(string niin, string itemName, string specs, string tableName, string manufacturer, string ACMCategory)
        {
            var results = dbManager.getFilteredSMCLCollection(niin, itemName, specs, tableName, manufacturer, ACMCategory);
            return new { aaData = results };
        }

        // GET api/<controller>/ACMLabels
        [HttpGet]
        public HttpResponseMessage ACMLabels(string niin, string usageCategory, string description, string startingLabel, string labelCount, string location="")
        {
            //there will never be a location when generating labels from the SMCL page since we are not working with an actual inventory item
            //but rather an item type
            LabelGenerator generator = new LabelGenerator();
            string filePath = generator.PrintACMLabels(niin, usageCategory, description, startingLabel, labelCount, location);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = "ACM Labels" };

            return result;
        }

        [HttpGet]
        public string GetLastSMCLUpdate()
        {
            return dbManager.getLastSMCLUploadDate();
        }

        [HttpGet]
        public List<string> getMSDSFiles(string fsc, string niin, string cage)
        {
            return dbManager.getMSDSFileName(fsc, niin, cage);
        }

        [HttpPost]
        public string UploadSMCL()
        {
            string successDetails;
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["SMCL"];

                if (httpPostedFile != null)
                {
                    // Validate the uploaded image(optional)
                    if (httpPostedFile.ContentType != "application/vnd.ms-excel")
                    {
                        return "Error: You must upload a file of type .xls.";
                    }

                    // Create a temp file on the server
                    String saveFilePath = Path.GetTempFileName();
                    httpPostedFile.SaveAs(saveFilePath);

                    List<string[]> list = new ExcelParser().parseExcel(saveFilePath);

                    successDetails = dbManager.updateSMCL(list, httpPostedFile.FileName);
                }
                else
                {
                    return "No file to upload!";
                }
            }
            else
            {
                return "No file to upload!";
            }
            return successDetails;
        }

    }
}