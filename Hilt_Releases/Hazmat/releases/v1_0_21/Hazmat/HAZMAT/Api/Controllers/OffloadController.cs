using HAZMAT.Api.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace HAZMAT.Api
{
    
    public class OffloadController : ApiController
    {
        private DatabaseManager dbManager;

        public OffloadController()
        {
            this.dbManager = new DatabaseManager();
        }

        // GET api/<controller>
        [HttpGet]
        public Object LoadOffloadTable()
        {
            var results = dbManager.getFilteredOffloadCollection();
            return new { aaData = results };
        }

        [HttpGet]
        public Object GetShipTo()
        {
            var uic = dbManager.getShipToUic();
            var name = uic.Select(t => t.ToString()).Select(s => dbManager.getShipToName(s)).ToList();

            return new { uic = uic, name = name}; 
        }

        [HttpGet]
        public Object GetShipToDetails(string uic)
        {
            ShipDetailModel shipDetails = new ShipDetailModel();
            shipDetails = dbManager.getShipToDetails(uic);
            return new
            { uic = shipDetails.Uic, shipname = shipDetails.ShipName, poc = shipDetails.Poc, address = shipDetails.Address, telephone = shipDetails.Telephone};
        }

        [HttpGet]
        public Object GetShipFromDetails()
        {
            ShipDetailModel shipDetails = new ShipDetailModel();
            string name = dbManager.getCurrentShipName();
            shipDetails = dbManager.getShipFromDetails(name);
            return new { uic = shipDetails.Uic, name = shipDetails.ShipName, poc = shipDetails.Poc, address = shipDetails.Address };
        }

        [HttpGet]
        public Object GetDeleteModal(int offload_id)
        {
            var results = dbManager.getOffloadItemForDeleteConfirm(offload_id);
            return new { aaData = results };
        }

        [HttpPost]
        public void DeleteRecord(OffloadIdModel offload_id)
        {
            dbManager.deleteOffloadItem(offload_id.OffloadId);
        }

        [HttpGet]
        public HttpResponseMessage CSV()
        {
            GenerateDD1348 csv = new GenerateDD1348();
            DataTable offloadDetails = new DataTable();
            offloadDetails = dbManager.getOffloadItemDetails(0);
            offloadDetails.DefaultView.Sort = "OffloadId";
            return csv.generateCSV(offloadDetails);
        }

        [HttpGet]
        public string GetOffloadItems()
        {
            List<long> ids = new List<long>();
            ids = dbManager.getOffloadItemIds();
            string offloadItems = "{";

            for (int i = 0; i< ids.Count; i++)
            {
                if (i > 0){ offloadItems += ","; }                                      
                offloadItems += i.ToString()+":"+ids[i].ToString();
            }

            offloadItems += "}";
            return offloadItems;
        }

        [HttpGet]
        public Object GetOffloadItemDetails(long id)
        {
            var details = dbManager.getOffloadItemDetails(id);
            return new {details = details};
        }

        [HttpGet]
        public int updateOffloadItem(long id, string details)
        {
            int success = -1;
            List<string> updateDetails = new List<string>();
            updateDetails = details.Split('|').ToList();

            success = dbManager.updateOffloadItem(id, updateDetails);

            return success;
        }

        [HttpPost]
        public void AddOffloadItem(AddOffloadModel newOffloadItem)
        {
            dbManager.AddOffloadItem(newOffloadItem.InventoryId, newOffloadItem.Quantity);
        }

        [HttpGet]
        public Object PrintOffloadItem(string uic, string details)
        {
            GenerateDD1348 dd1348 = new GenerateDD1348();
            List<string> itemDetails = details.Split('|').ToList();
            string[] NSNArray = {itemDetails[44],"-", itemDetails[45]};
            string nsn = string.Concat(NSNArray);
            itemDetails[44] = nsn;
            itemDetails.RemoveAt(45);
            DataTable offloadDetails = new DataTable();

            offloadDetails = dd1348.addColumns(offloadDetails);
            offloadDetails.Rows.Add(itemDetails.ToArray());

            if (String.IsNullOrEmpty(uic)){uic = "0";}
            
            return new {relPath = dd1348.generatePdf(uic, offloadDetails)};
        }

        [HttpGet]
        public Object createPDF(string uic)
        {
            GenerateDD1348 dd1348 = new GenerateDD1348();
            DataTable offloadDetails = new DataTable();
            offloadDetails = dbManager.getOffloadItemDetails(0);
            offloadDetails.DefaultView.Sort = "OffloadId";
            
            return new {relPath = dd1348.generatePdf(uic, offloadDetails)};
        }

        [HttpGet]
        public string GenerateDocNumber()
        {
            GenerateDD1348 dd1348 = new GenerateDD1348();
            return dd1348.generateDocNumber();
        }

        [HttpGet]
        public int AddUniqueOffloadItem(string details)
        {
            int success = -1;
            List<string> uniqueDetails = new List<string>();
            uniqueDetails = details.Split('|').ToList();

            success = dbManager.AddUniqueOffloadItem(uniqueDetails);

            return success;
        }
    }
}