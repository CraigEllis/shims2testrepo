﻿using System;
using System.Collections.Generic;
//using System.Collections;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Net;
using System.Web;


namespace HAZMAT.Api
{

    public class ReportsController : ApiController
    {
        private DatabaseManager dbManager;

        public ReportsController()
        {
            this.dbManager = new DatabaseManager();
        }

        [HttpGet]
        public Object GetLocations()
        {
            var results = dbManager.getInventoryLocations();
            return new {aaData = results};
        }

        [HttpGet]
        public Object GetWorkcenters()
        {
            var results = dbManager.getInventoryWorkcenters();
            return new { aaData = results };
        }

        [HttpGet]
        public HttpResponseMessage ExportReport(string extension)
        {
            ReportsGenerator expReport = new ReportsGenerator();
            return expReport.GenerateReportWithExtension(extension);
        }

        [HttpGet]
        public string GenerateInventoryReport(string type, string sort, string signatures)
        {
            ReportsGenerator invReport = new ReportsGenerator();
            string whereStatement = " where oh.inventory_detail_id is not null ";
            string sortBy = sort;

            if (type == "specWorkcenter") { 
                whereStatement += @"and oh.location_id = l.location_id 
							        and l.workcenter_id = w.workcenter_id
									and w.wid = '"+sortBy+"'";
                type = dbManager.getWorkcenterDescription(sort);
                sortBy = "WCENTER";
            }
            else if (type == "specLocation")
            {
                whereStatement += "and oh.location_id = " + sortBy;
                type = dbManager.getLocationName(sort);
                sortBy = "LOCATION";
            }
            else if (type == "invVsSmcl")
            {
                whereStatement += "and n.niin IS NULL";
                type = "Not in SMCL";
            }
            else if (type == "qtyIsZero")
            {
                whereStatement += "and oh.qty = 0";
                type = "Qty is 0";
            }

            DataTable inventory = new DataTable();
            inventory = dbManager.getInventoryDetailsForReport(whereStatement);
            DataView dv = inventory.DefaultView;
            dv.Sort = sortBy.ToUpper() + " ASC";
            DataTable sortedInv = dv.ToTable();

            string relpath = invReport.GenerateInventoryReport(type, sortBy, signatures, sortedInv);
            return relpath;
        }

        [HttpGet]
        public string GenerateAcmReport(string type, string sort, string signatures)
        {
            ReportsGenerator acmReport = new ReportsGenerator();
            string whereStatement = " where oh.inventory_detail_id is not null ";
            string sortBy = sort;
            string typeBy = " ";

            if (type == "prohibited"){ typeBy = "'X'"; }
            else if (type == "restricted"){ typeBy = "'R'"; }
            else if (type == "limited"){ typeBy = "'L'"; }
            else { typeBy = "'X' or u.category = 'L' or u.category = 'R'"; }

            whereStatement += "and u.category = "+typeBy;

            DataTable acmTable = new DataTable();
            acmTable = dbManager.getAcmDetailsForReport(whereStatement);
            DataView dv = acmTable.DefaultView;
            dv.Sort = sortBy.ToUpper() + " ASC";
            DataTable sortedAcm = dv.ToTable();

            string relpath = acmReport.GenerateAcmReport(type, sortBy, signatures, sortedAcm);
            return relpath;
        }

        [HttpGet]
        public string GenerateExpirationReport(string type, string sort, string signatures)
        {
            ReportsGenerator expReport = new ReportsGenerator();
            string whereStatement = " where oh.inventory_detail_id is not null ";
            DateTime date = Convert.ToDateTime(type);

            whereStatement += "and DATEDIFF(DAY, '"+date.ToString("yyyy-MM-dd")+"', oh.expiration_date) < 0";

            DataTable inventory = new DataTable();
            inventory = dbManager.getExpirationDetailsForReport(whereStatement);
            DataView dv = inventory.DefaultView;
            dv.Sort = sort.ToUpper() + " ASC";
            DataTable sortedExp = dv.ToTable();

            string relpath = expReport.GenerateExpirationReport(type, sort, signatures, sortedExp);
            return relpath;
        }

        [HttpGet]
        public string GenerateStowageReport(string type, string sort, string signatures)
        {
            ReportsGenerator stowageReport = new ReportsGenerator();
            List<ListItem> stowage = new List<ListItem>();
            if (sort == "workcenter"){ stowage = dbManager.getWorkcenters(); }
            else { stowage = dbManager.getLocationIds(); }

            string relpath = stowageReport.GenerateStowageReport(type, sort, signatures, stowage);
            return relpath;

        }

        [HttpGet]
        public string GenerateIncompatiblesReport(string type, string sort)
        {
            ReportsGenerator incompReport = new ReportsGenerator();

            string relpath = incompReport.GenerateIncompatiblesReport(type, sort);
            return relpath;

        }
    }
}