﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.Script.Serialization;

namespace HAZMAT
{
    [CoverageExclude]
    public partial class MSDS : Page
    {
        private List<MsdsIngredients> ingredientList = new List<MsdsIngredients>();
        private List<MsdsContractors> contractorList = new List<MsdsContractors>();
        [CoverageExclude]
        void Page_PreInit(Object sender, EventArgs e)
        {
            
           if (Request.QueryString["msdsid"] != null)
           {
               if (Request.QueryString["edit"] != null) {
                   MasterPageFile = "Site.Master";
               }
               else {
                MasterPageFile = "SitePopup.Master";
           }
           }
           else
           {
                MasterPageFile = "Site.Master";
           }
        }
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //save ingredient list
                ViewState.Add("ingredientList", ingredientList);
                ViewState.Add("contractorList", contractorList);

                Populate_MonthddList();
                Populate_YearddList();
            }

           int msds_id;
           if (Request.QueryString["msdsid"] != null && Request.QueryString["edit"] == "N")
           {                
                //if the user is not a member of a role, he/she does not get to see certain links.
                if (!isAdmin() && !isSupplyOfficer() && !isSupplyUser())
                    Page.Form.Visible = true;

                btn_saveMSDS.Visible = false;

                //msds
                fileUploadMSDS.Visible = false;
                lbl_fileUpload.Visible = false;
                btn_viewFile.Visible = true;
                //translated
                fileUploadTranslated.Visible = false;
                lbl_translated.Visible = false;
                btn_viewTranslated.Visible = true;
                //neshap
                fileUploadNeshap.Visible = false;
                lbl_neshap.Visible = false;
                btn_viewNeshap.Visible = true;
                //other
                fileUploadOther.Visible = false;
                lbl_other.Visible = false;
                btn_viewOther.Visible = true;
                //prodSheet
                fileUploadProdSheet.Visible = false;
                lbl_prodSheet.Visible = false;
                btn_viewProdSheet.Visible = true;
                //transport
                fileUploadTransport.Visible = false;
                lbl_transport.Visible = false;
                btn_viewTransport.Visible = true;
                //label
                fileUploadLabel.Visible = false;
                lbl_label.Visible = false;
                btn_viewLabel.Visible = true;

                    // Set the form to read only mode
                    FormViewMSDS.DefaultMode = FormViewMode.ReadOnly;
                    FormViewContractorInfo.DefaultMode = FormViewMode.ReadOnly;
                    FormViewIngredients.DefaultMode = FormViewMode.ReadOnly;
                    FormViewItemDescription.DefaultMode = FormViewMode.ReadOnly;
                    FormViewPhysChemical.DefaultMode = FormViewMode.ReadOnly;
                    FormViewRadiologicalInfo.DefaultMode = FormViewMode.ReadOnly;
                    FormViewTransportation.DefaultMode = FormViewMode.ReadOnly;
                    FormViewDOT.DefaultMode = FormViewMode.ReadOnly;
                    FormViewAFJM.DefaultMode = FormViewMode.ReadOnly;
                    FormViewIATA.DefaultMode = FormViewMode.ReadOnly;
                    FormViewIMO.DefaultMode = FormViewMode.ReadOnly;
                    FormViewLabelInfo.DefaultMode = FormViewMode.ReadOnly;
                    FormViewDisposal.DefaultMode = FormViewMode.ReadOnly;
                    FormViewDocTypes.DefaultMode = FormViewMode.ReadOnly;

                msds_id = Int32.Parse(Request.QueryString["msdsid"].ToString());

                DatabaseManager manager = new DatabaseManager();
                FormViewMSDS.DataSource = manager.findMSDS(msds_id).DefaultView;
                FormViewMSDS.DataBind();
                
                try
                {
                    String fileName = ((DataRowView)FormViewMSDS.DataItem)["file_name"].ToString();
                    if (fileName.Equals(""))
                        btn_viewFile.Enabled = false;
                }
                catch
                {
                    btn_viewFile.Enabled = false;
                }

                FormViewContractorInfo.DataSource = manager.findMSDScontractorInfo(msds_id).DefaultView;
                FormViewContractorInfo.DataBind();

                FormViewIngredients.DataSource = manager.findMSDSingredients(msds_id).DefaultView;
                FormViewIngredients.DataBind();

                FormViewItemDescription.DataSource = manager.findMSDSitemDescription(msds_id).DefaultView;
                FormViewItemDescription.DataBind();

                FormViewPhysChemical.DataSource = manager.findMSDSphysChemical(msds_id).DefaultView;
                FormViewPhysChemical.DataBind();

                FormViewRadiologicalInfo.DataSource = manager.findMSDSradiologicalInfo(msds_id).DefaultView;
                FormViewRadiologicalInfo.DataBind();

                FormViewTransportation.DataSource = manager.findMSDStransportation(msds_id).DefaultView;
                FormViewTransportation.DataBind();

                FormViewDOT.DataSource = manager.findMSDSdotPSN(msds_id).DefaultView;
                FormViewDOT.DataBind();

                FormViewAFJM.DataSource = manager.findMSDSafjmPSN(msds_id).DefaultView;
                FormViewAFJM.DataBind();

                FormViewIATA.DataSource = manager.findMSDSiataPSN(msds_id).DefaultView;
                FormViewIATA.DataBind();

                FormViewIMO.DataSource = manager.findMSDSimoPSN(msds_id).DefaultView;
                FormViewIMO.DataBind();

                FormViewLabelInfo.DataSource = manager.findMSDSlabelInfo(msds_id).DefaultView;
                FormViewLabelInfo.DataBind();

                FormViewDisposal.DataSource = manager.findMSDSdisposal(msds_id).DefaultView;
                FormViewDisposal.DataBind();

                FormViewDocTypes.DataSource = manager.findMSDSdocTypes(msds_id).DefaultView;
                FormViewDocTypes.DataBind();

                if (!isAdmin() && !isSupplyOfficer() && !isSupplyUser()) {
                    FormViewMSDS.DefaultMode = FormViewMode.ReadOnly;
                    FormViewContractorInfo.DefaultMode = FormViewMode.ReadOnly;
                    FormViewIngredients.DefaultMode = FormViewMode.ReadOnly;
                    FormViewItemDescription.DefaultMode = FormViewMode.ReadOnly;
                    FormViewPhysChemical.DefaultMode = FormViewMode.ReadOnly;
                    FormViewRadiologicalInfo.DefaultMode = FormViewMode.ReadOnly;
                    FormViewTransportation.DefaultMode = FormViewMode.ReadOnly;
                    FormViewDOT.DefaultMode = FormViewMode.ReadOnly;
                    FormViewAFJM.DefaultMode = FormViewMode.ReadOnly;
                    FormViewIATA.DefaultMode = FormViewMode.ReadOnly;
                    FormViewIMO.DefaultMode = FormViewMode.ReadOnly;
                    FormViewLabelInfo.DefaultMode = FormViewMode.ReadOnly;
                    FormViewDisposal.DefaultMode = FormViewMode.ReadOnly;
                    FormViewDocTypes.DefaultMode = FormViewMode.ReadOnly;
                }

                try
                {
                    String translatedfileName = ((DataRowView)FormViewDocTypes.DataItem)["msds_translated_filename"].ToString();
                    if (translatedfileName.Equals(""))
                        btn_viewTranslated.Enabled = false;
                }
                catch
                {
                    btn_viewTranslated.Enabled = false;
                }
                try
                {
                    String fileName = ((DataRowView)FormViewDocTypes.DataItem)["neshap_comp_filename"].ToString();
                    if (fileName.Equals(""))
                        btn_viewNeshap.Enabled = false;
                }
                catch
                {
                    btn_viewNeshap.Enabled = false;
                }
                try
                {
                    String fileName = ((DataRowView)FormViewDocTypes.DataItem)["other_docs_filename"].ToString();
                    if (fileName.Equals(""))
                        btn_viewOther.Enabled = false;
                }
                catch
                {
                    btn_viewOther.Enabled = false;
                }
                try
                {
                    String fileName = ((DataRowView)FormViewDocTypes.DataItem)["product_sheet_filename"].ToString();
                    if (fileName.Equals(""))
                        btn_viewProdSheet.Enabled = false;
                }
                catch
                {
                    btn_viewProdSheet.Enabled = false;
                }
                try
                {
                    String fileName = ((DataRowView)FormViewDocTypes.DataItem)["transportation_cert_filename"].ToString();
                    if (fileName.Equals(""))
                        btn_viewTransport.Enabled = false;
                }
                catch
                {
                    btn_viewTransport.Enabled = false;
                }
                try
                {
                     String fileName = ((DataRowView)FormViewDocTypes.DataItem)["manufacturer_label_filename"].ToString();
                    if (fileName.Equals(""))
                        btn_viewLabel.Enabled = false;
                }                
                catch
                {
                    btn_viewLabel.Enabled = false;
                }

            }//end if
            else
            {
                //if the user is not a member of a role, he/she does not get to see certain links.
                if (!isAdmin() && !isSupplyOfficer() && !isSupplyUser())
                {
                    Page.Form.Visible = false;
                }

                btn_saveMSDS.Visible = true;

                // New MSDS

                //msds
                fileUploadMSDS.Visible = true;
                lbl_fileUpload.Visible = true;
                btn_viewFile.Visible = false;
                //translated
                fileUploadTranslated.Visible = true;
                lbl_translated.Visible = true;
                btn_viewTranslated.Visible = false;
                //neshap
                fileUploadNeshap.Visible = true;
                lbl_neshap.Visible = true;
                btn_viewNeshap.Visible = false;
                //other
                fileUploadOther.Visible = true;
                lbl_other.Visible = true;
                btn_viewOther.Visible = false;
                //prodSheet
                fileUploadProdSheet.Visible = true;
                lbl_prodSheet.Visible = true;
                btn_viewProdSheet.Visible = false;
                //transport
                fileUploadTransport.Visible = true;
                lbl_transport.Visible = true;
                btn_viewTransport.Visible = false;
                //label
                fileUploadLabel.Visible = true;
                lbl_label.Visible = true;
                btn_viewLabel.Visible = false;

                FormViewMSDS.DefaultMode = FormViewMode.Insert;               
                FormViewIngredients.DefaultMode = FormViewMode.Insert;
                FormViewItemDescription.DefaultMode = FormViewMode.Insert;
                FormViewPhysChemical.DefaultMode = FormViewMode.Insert;

                FormViewContractorInfo.DefaultMode = FormViewMode.Insert;
                FormViewRadiologicalInfo.DefaultMode = FormViewMode.Insert;
                FormViewTransportation.DefaultMode = FormViewMode.Insert;
                FormViewDOT.DefaultMode = FormViewMode.Insert;
                FormViewAFJM.DefaultMode = FormViewMode.Insert;
                FormViewIATA.DefaultMode = FormViewMode.Insert;
                FormViewIMO.DefaultMode = FormViewMode.Insert;
                FormViewLabelInfo.DefaultMode = FormViewMode.Insert;
                FormViewDisposal.DefaultMode = FormViewMode.Insert;
                FormViewDocTypes.DefaultMode = FormViewMode.Insert;

                //populate HCC drop down list
                DropDownList dd_hcc = (DropDownList) FormViewMSDS.FindControl("dd_hcc");
                if (dd_hcc.Items.Count == 0) {
                    List<ListItem> usageCategories = new DatabaseManager().getHccList(true);
                    foreach (ListItem item in usageCategories) {
                        dd_hcc.Items.Add(item);
                    }
                }

                // See if we want to switch to edit mode
                if (Request.QueryString["msdsid"] != null) {
                    msds_id = Int32.Parse(Request.QueryString["msdsid"].ToString());
                    String msdsSerNo = new DatabaseManager().getMSDSSerNoForID(msds_id);

                    if (!IsPostBack)
                        btn_editMSDS_Click(msdsSerNo);
                }
            }
            
            makeDropDownVisible(!fadePanel.Visible);
        }
        private void fixFields(MsdsRecord msds)
        {
            // Hide the calendar select if we have load date
            if (msds.PRODUCT_LOAD_DATE.Length > 0)
                ((Button) FormViewMSDS.FindControl("btn_PRODUCT_LOAD_DATE_select")).Visible = false;

            if (msds.MSDSSERNO.Trim().Length > 0) {
                ((TextBox) FormViewMSDS.FindControl("MSDSSERNOTextBox")).ReadOnly = true;
                ((TextBox) FormViewMSDS.FindControl("MSDSSERNOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.CAGE.Trim().Length > 0) {
                ((TextBox) FormViewMSDS.FindControl("CAGETextBox")).ReadOnly = true;
                ((TextBox) FormViewMSDS.FindControl("CAGETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.MANUFACTURER.Trim().Length > 0)
            { 
                ((TextBox)FormViewMSDS.FindControl("MANUFACTURERTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("MANUFACTURERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PARTNO.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PARTNOTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PARTNOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.FSC.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("FSCTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("FSCTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NIIN.Trim().Length > 0)
            { 
                ((TextBox)FormViewMSDS.FindControl("NIINTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("NIINTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.ARTICLE_IND.Trim().Length > 0)
            { 
                ((TextBox)FormViewMSDS.FindControl("ARTICLE_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("ARTICLE_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DESCRIPTION.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("DESCRIPTIONTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("DESCRIPTIONTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EMERGENCY_TEL.Trim().Length > 0)
            { 
                ((TextBox)FormViewMSDS.FindControl("EMERGENCY_TELTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("EMERGENCY_TELTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.END_COMP_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("END_COMP_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("END_COMP_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.END_ITEM_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("END_ITEM_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("END_ITEM_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.KIT_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("KIT_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("KIT_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.KIT_PART_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("KIT_PART_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("KIT_PART_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.MANUFACTURER_MSDS_NO.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("MANUFACTURER_MSDS_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("MANUFACTURER_MSDS_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.MIXTURE_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("MIXTURE_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("MIXTURE_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PRODUCT_IDENTITY.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_IDENTITYTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_IDENTITYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PRODUCT_LOAD_DATE.Trim().Length > 0)
            {
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_LOAD_DATETextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_LOAD_DATETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PRODUCT_RECORD_STATUS.Trim().Length > 0)
            { 
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_RECORD_STATUSTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_RECORD_STATUSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PRODUCT_REVISION_NO.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_REVISION_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_REVISION_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PROPRIETARY_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PROPRIETARY_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PROPRIETARY_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PUBLISHED_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PUBLISHED_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PUBLISHED_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PURCHASED_PROD_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PURCHASED_PROD_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PURCHASED_PROD_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PURE_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PURE_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PURE_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.RADIOACTIVE_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("RADIOACTIVE_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("RADIOACTIVE_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SERVICE_AGENCY.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("SERVICE_AGENCYTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("SERVICE_AGENCYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.TRADE_NAME.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("TRADE_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("TRADE_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.TRADE_SECRET_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("TRADE_SECRET_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("TRADE_SECRET_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PRODUCT_IND.Trim().Length > 0) { 
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PRODUCT_LANGUAGE.Trim().Length > 0) {
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_LANGUAGETextBox")).ReadOnly = true;
                ((TextBox)FormViewMSDS.FindControl("PRODUCT_LANGUAGETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //phys chemical
            if (msds.APP_ODOR.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("APP_ODORTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("APP_ODORTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.VAPOR_PRESS.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("VAPOR_PRESSTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("VAPOR_PRESSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.VAPOR_DENS.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("VAPOR_DENSTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("VAPOR_DENSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SPECIFIC_GRAV.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("SPECIFIC_GRAVTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("SPECIFIC_GRAVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EVAP_RATE_REF.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("EVAP_RATE_REFTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("EVAP_RATE_REFTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SOL_IN_WATER.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("SOL_IN_WATERTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("SOL_IN_WATERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PERCENT_VOL_VOLUME.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("PERCENT_VOL_VOLUMETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("PERCENT_VOL_VOLUMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.VISCOSITY.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("VISCOSITYTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("VISCOSITYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.VOC_POUNDS_GALLON.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("VOC_POUNDS_GALLONTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("VOC_POUNDS_GALLONTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.VOC_GRAMS_LITER.Trim().Length> 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("VOC_GRAMS_LITERTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("VOC_GRAMS_LITERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PH.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("PHTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("PHTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AUTOIGNITION_TEMP.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("AUTOIGNITION_TEMPTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("AUTOIGNITION_TEMPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.CARCINOGEN_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("CARCINOGEN_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("CARCINOGEN_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_ACUTE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("EPA_ACUTETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("EPA_ACUTETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_CHRONIC.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("EPA_CHRONICTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("EPA_CHRONICTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_FIRE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("EPA_FIRETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("EPA_FIRETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_PRESSURE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("EPA_PRESSURETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("EPA_PRESSURETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_REACTIVITY.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("EPA_REACTIVITYTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("EPA_REACTIVITYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.FLASH_PT_TEMP.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("FLASH_PT_TEMPTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("FLASH_PT_TEMPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NEUT_AGENT.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("NEUT_AGENTTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("NEUT_AGENTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NFPA_FLAMMABILITY.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_FLAMMABILITYTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_FLAMMABILITYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NFPA_HEALTH.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_HEALTHTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_HEALTHTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NFPA_REACTIVITY.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_REACTIVITYTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_REACTIVITYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NFPA_SPECIAL.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_SPECIALTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("NFPA_SPECIALTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_CARCINOGENS.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_CARCINOGENSTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_CARCINOGENSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_COMB_LIQUID.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMB_LIQUIDTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMB_LIQUIDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_COMP_GAS.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMP_GASTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMP_GASTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_CORROSIVE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_CORROSIVETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_CORROSIVETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_EXPLOSIVE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_EXPLOSIVETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_EXPLOSIVETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_FLAMMABLE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_FLAMMABLETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_FLAMMABLETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_HIGH_TOXIC.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_HIGH_TOXICTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_HIGH_TOXICTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_IRRITANT.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_IRRITANTTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_IRRITANTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_ORG_PEROX.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_ORG_PEROXTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_ORG_PEROXTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_OTHERLONGTERM.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_OTHERLONGTERMTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_OTHERLONGTERMTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_OXIDIZER.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_OXIDIZERTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_OXIDIZERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_PYRO.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_PYROTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_PYROTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_SENSITIZER.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_SENSITIZERTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_SENSITIZERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_TOXIC.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_TOXICTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_TOXICTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_UNST_REACT.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_UNST_REACTTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_UNST_REACTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OTHER_SHORT_TERM.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OTHER_SHORT_TERMTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OTHER_SHORT_TERMTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.PHYS_STATE_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("PHYS_STATE_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("PHYS_STATE_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.VOL_ORG_COMP_WT.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("VOL_ORG_COMP_WTTextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("VOL_ORG_COMP_WTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OSHA_WATER_REACTIVE.Trim().Length > 0)
            {
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_WATER_REACTIVETextBox")).ReadOnly = true;
                ((TextBox)FormViewPhysChemical.FindControl("OSHA_WATER_REACTIVETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //Ingredients
            MsdsIngredients ingredient = msds.ingredientsList[0];
            if (ingredient.INGREDIENT_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("INGREDIENT_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("INGREDIENT_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.PRCNT.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("PRCNTTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("PRCNTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.CAS.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("CAS_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("CAS_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.RTECS_NUM.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("RTECS_NUMTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("RTECS_NUMTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.RTECS_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("RTECS_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("RTECS_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.OSHA_PEL.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("OSHA_PELTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("OSHA_PELTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.OSHA_STEL.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("OSHA_STELTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("OSHA_STELTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.ACGIH_TLV.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("ACGIH_TLVTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("ACGIH_TLVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.ACGIH_STEL.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("ACGIH_STELTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("ACGIH_STELTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.EPA_REPORT_QTY.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("EPA_REPORT_QTYTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("EPA_REPORT_QTYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.DOT_REPORT_QTY.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("DOT_REPORT_QTYTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("DOT_REPORT_QTYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.PRCNT_VOL_VALUE.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_VALUETextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_VALUETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.PRCNT_VOL_WEIGHT.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_WEIGHTTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_WEIGHTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.CHEM_MFG_COMP_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("CHEM_MFG_COMP_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("CHEM_MFG_COMP_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.ODS_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("ODS_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("ODS_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (ingredient.OTHER_REC_LIMITS.Trim().Length > 0)
            {
                ((TextBox)FormViewIngredients.FindControl("OTHER_REC_LIMITSTextBox")).ReadOnly = true;
                ((TextBox)FormViewIngredients.FindControl("OTHER_REC_LIMITSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //contractor information;
            MsdsContractors contractor = msds.contractorsList[0];
            if (contractor.CT_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_CAGE.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_CAGETextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_CAGETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_CITY.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_CITYTextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_CITYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_COMPANY_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_COMPANY_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_COMPANY_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_COUNTRY.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_COUNTRYTextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_COUNTRYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_PO_BOX.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_PO_BOXTextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_PO_BOXTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_PHONE.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_PHONETextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_PHONETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.PURCHASE_ORDER_NO.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("PURCHASE_ORDER_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("PURCHASE_ORDER_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (contractor.CT_STATE.Trim().Length > 0)
            {
                ((TextBox)FormViewContractorInfo.FindControl("CT_STATETextBox")).ReadOnly = true;
                ((TextBox)FormViewContractorInfo.FindControl("CT_STATETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //radiological info;
            if (msds.NRC_LP_NUM.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("NRC_LP_NUMTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("NRC_LP_NUMTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.OPERATOR.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("OPERATORTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("OPERATORTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.RAD_AMOUNT_MICRO.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_AMOUNT_MICROTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_AMOUNT_MICROTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.RAD_FORM.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_FORMTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_FORMTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.RAD_CAS.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_CASTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_CASTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.RAD_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.RAD_SYMBOL.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_SYMBOLTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_SYMBOLTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.REP_NSN.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("REP_NSNTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("REP_NSNTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SEALED.Trim().Length > 0)
            {
                ((TextBox)FormViewRadiologicalInfo.FindControl("SEALEDTextBox")).ReadOnly = true;
                ((TextBox)FormViewRadiologicalInfo.FindControl("SEALEDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //transportation;
            if (msds.AF_MMAC_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("AF_MMAC_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("AF_MMAC_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.CERTIFICATE_COE.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("CERTIFICATE_COETextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("CERTIFICATE_COETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.COMPETENT_CAA.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("COMPETENT_CAATextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("COMPETENT_CAATextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOD_ID_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("DOD_ID_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("DOD_ID_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_EXEMPTION_NO.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("DOT_EXEMPTION_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("DOT_EXEMPTION_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_RQ_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("DOT_RQ_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("DOT_RQ_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EX_NO.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("EX_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("EX_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.TRAN_FLASH_PT_TEMP.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("TRAN_FLASH_PT_TEMPTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("TRAN_FLASH_PT_TEMPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.HIGH_EXPLOSIVE_WT.ToString().Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("HIGH_EXPLOSIVE_WTTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("HIGH_EXPLOSIVE_WTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LTD_QTY_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("LTD_QTY_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("LTD_QTY_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.MAGNETIC_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("MAGNETIC_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("MAGNETIC_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.MAGNETISM.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("MAGNETISMTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("MAGNETISMTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.MARINE_POLLUTANT_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("MARINE_POLLUTANT_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("MARINE_POLLUTANT_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NET_EXP_QTY_DIST.ToString().Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("NET_EXP_QTY_DISTTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("NET_EXP_QTY_DISTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NET_EXP_WEIGHT.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("NET_EXP_WEIGHTTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("NET_EXP_WEIGHTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NET_PROPELLANT_WT.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("NET_PROPELLANT_WTTextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("NET_PROPELLANT_WTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NOS_TECHNICAL_SHIPPING_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("NOS_TECHNICAL_SHIPPING_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("NOS_TECHNICAL_SHIPPING_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.TRANSPORTATION_ADDITIONAL_DATA.Trim().Length > 0)
            {
                ((TextBox)FormViewTransportation.FindControl("TRANSPORTATION_ADDITIONAL_DATATextBox")).ReadOnly = true;
                ((TextBox)FormViewTransportation.FindControl("TRANSPORTATION_ADDITIONAL_DATATextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //dot psn
            if (msds.DOT_HAZARD_CLASS_DIV.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_CLASS_DIVTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_CLASS_DIVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_HAZARD_LABEL.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_LABELTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_LABELTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_MAX_CARGO.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_MAX_CARGOTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_MAX_CARGOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_MAX_PASSENGER.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_MAX_PASSENGERTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_MAX_PASSENGERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PACK_BULK.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_BULKTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_BULKTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PACK_EXCEPTIONS.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_EXCEPTIONSTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_EXCEPTIONSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PACK_NONBULK.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_NONBULKTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_NONBULKTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PROP_SHIP_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PROP_SHIP_MODIFIER.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_MODIFIERTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_MODIFIERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PSN_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PSN_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PSN_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_SPECIAL_PROVISION.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_SPECIAL_PROVISIONTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_SPECIAL_PROVISIONTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_SYMBOLS.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_SYMBOLSTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_SYMBOLSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_UN_ID_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_UN_ID_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_UN_ID_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_WATER_OTHER_REQ.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_WATER_OTHER_REQTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_WATER_OTHER_REQTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_WATER_VESSEL_STOW.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_WATER_VESSEL_STOWTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_WATER_VESSEL_STOWTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.DOT_PACK_GROUP.Trim().Length > 0)
            {
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_GROUPTextBox")).ReadOnly = true;
                ((TextBox)FormViewDOT.FindControl("DOT_PACK_GROUPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //afjm psn
            if (msds.AFJM_HAZARD_CLASS.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_HAZARD_CLASSTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_HAZARD_CLASSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_PACK_PARAGRAPH.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_PARAGRAPHTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_PARAGRAPHTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_PACK_GROUP.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_GROUPTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_GROUPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_PROP_SHIP_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_PROP_SHIP_MODIFIER.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_MODIFIERTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_MODIFIERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_PSN_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_PSN_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_PSN_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_SPECIAL_PROV.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_SPECIAL_PROVTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_SPECIAL_PROVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_SUBSIDIARY_RISK.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_SUBSIDIARY_RISKTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_SUBSIDIARY_RISKTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_SYMBOLS.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_SYMBOLSTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_SYMBOLSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.AFJM_UN_ID_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewAFJM.FindControl("AFJM_UN_ID_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewAFJM.FindControl("AFJM_UN_ID_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //iata psn
            if (msds.IATA_CARGO_PACKING.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACKINGTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACKINGTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_HAZARD_CLASS.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_CLASSTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_CLASSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_HAZARD_LABEL.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_LABELTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_LABELTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PACK_GROUP.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PACK_GROUPTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PACK_GROUPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PASS_AIR_PACK_LMT_INSTR.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_INSTRTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_INSTRTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PASS_AIR_PACK_LMT_PER_PKG.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_PER_PKGTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_PER_PKGTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PASS_AIR_PACK_NOTE.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACKINGTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACKINGTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PROP_SHIP_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PROP_SHIP_MODIFIER.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_MODIFIERTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_MODIFIERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_CARGO_PACK_MAX_QTY.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACK_MAX_QTYTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACK_MAX_QTYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PSN_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PSN_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PSN_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_PASS_AIR_MAX_QTY.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_MAX_QTYTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_MAX_QTYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_SPECIAL_PROV.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_SPECIAL_PROVTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_SPECIAL_PROVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_SUBSIDIARY_RISK.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_SUBSIDIARY_RISKTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_SUBSIDIARY_RISKTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IATA_UN_ID_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewIATA.FindControl("IATA_UN_ID_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewIATA.FindControl("IATA_UN_ID_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //imo psn          
            if (msds.IMO_EMS_NO.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_EMS_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_EMS_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_HAZARD_CLASS.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_HAZARD_CLASSTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_HAZARD_CLASSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_IBC_INSTR.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_IBC_INSTRTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_IBC_INSTRTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_LIMITED_QTY.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_LIMITED_QTYTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_LIMITED_QTYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_PACK_GROUP.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_PACK_GROUPTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_PACK_GROUPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_PACK_INSTRUCTIONS.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_PACK_INSTRUCTIONSTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_PACK_INSTRUCTIONSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_PACK_PROVISIONS.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_PACK_PROVISIONSTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_PACK_PROVISIONSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_PROP_SHIP_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_PROP_SHIP_MODIFIER.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_MODIFIERTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_MODIFIERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_PSN_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_PSN_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_PSN_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_SPECIAL_PROV.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_SPECIAL_PROVTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_SPECIAL_PROVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_STOW_SEGR.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_STOW_SEGRTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_STOW_SEGRTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_SUBSIDIARY_RISK.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_SUBSIDIARY_RISKTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_SUBSIDIARY_RISKTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_TANK_INSTR_IMO.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_IMOTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_IMOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_TANK_INSTR_PROV.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_PROVTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_PROVTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_TANK_INSTR_UN.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_UNTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_UNTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_UN_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_UN_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_UN_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.IMO_IBC_PROVISIONS.Trim().Length > 0)
            {
                ((TextBox)FormViewIMO.FindControl("IMO_IBC_PROVISIONSTextBox")).ReadOnly = true;
                ((TextBox)FormViewIMO.FindControl("IMO_IBC_PROVISIONSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //item description;
            if (msds.ITEM_MANAGER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("ITEM_MANAGERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("ITEM_MANAGERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.ITEM_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("ITEM_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("ITEM_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SPECIFICATION_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("SPECIFICATION_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("SPECIFICATION_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.TYPE_GRADE_CLASS.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("TYPE_GRADE_CLASSTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("TYPE_GRADE_CLASSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.UNIT_OF_ISSUE.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("UNIT_OF_ISSUETextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("UNIT_OF_ISSUETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.QUANTITATIVE_EXPRESSION.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("QUANTITATIVE_EXPRESSIONTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("QUANTITATIVE_EXPRESSIONTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.UI_CONTAINER_QTY.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("UI_CONTAINER_QTYTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("UI_CONTAINER_QTYTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.TYPE_OF_CONTAINER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("TYPE_OF_CONTAINERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("TYPE_OF_CONTAINERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.BATCH_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("BATCH_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("BATCH_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LOT_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("LOT_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("LOT_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LOG_FLIS_NIIN_VER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("LOG_FLIS_NIIN_VERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("LOG_FLIS_NIIN_VERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LOG_FSC.ToString().Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("LOG_FSCTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("LOG_FSCTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.NET_UNIT_WEIGHT.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("NET_UNIT_WEIGHTTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("NET_UNIT_WEIGHTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SHELF_LIFE_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("SHELF_LIFE_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("SHELF_LIFE_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SPECIAL_EMP_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("SPECIAL_EMP_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("SPECIAL_EMP_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.UN_NA_NUMBER.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("UN_NA_NUMBERTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("UN_NA_NUMBERTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.UPC_GTIN.Trim().Length > 0)
            {
                ((TextBox)FormViewItemDescription.FindControl("UPC_GTINTextBox")).ReadOnly = true;
                ((TextBox)FormViewItemDescription.FindControl("UPC_GTINTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //label information;
            if (msds.COMPANY_CAGE_RP.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("COMPANY_CAGE_RPTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("COMPANY_CAGE_RPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.COMPANY_NAME_RP.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("COMPANY_NAME_RPTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("COMPANY_NAME_RPTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_EMERG_PHONE.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_EMERG_PHONETextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_EMERG_PHONETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_ITEM_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_ITEM_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_ITEM_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_PROC_YEAR.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROC_YEARTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROC_YEARTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_PROD_IDENT.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_IDENTTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_IDENTTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_PROD_SERIALNO.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_SERIALNOTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_SERIALNOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_SIGNAL_WORD.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_SIGNAL_WORDTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_SIGNAL_WORDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.LABEL_STOCK_NO.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_STOCK_NOTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("LABEL_STOCK_NOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.SPECIFIC_HAZARDS.Trim().Length > 0)
            {
                ((TextBox)FormViewLabelInfo.FindControl("SPECIFIC_HAZARDSTextBox")).ReadOnly = true;
                ((TextBox)FormViewLabelInfo.FindControl("SPECIFIC_HAZARDSTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            //disposal info;
            if (msds.DISPOSAL_ADD_INFO.Trim().Length > 0)
            {
                ((TextBox)FormViewDisposal.FindControl("DISPOSAL_ADD_INFOTextBox")).ReadOnly = true;
                ((TextBox)FormViewDisposal.FindControl("DISPOSAL_ADD_INFOTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_HAZ_WASTE_CODE.Trim().Length > 0)
            {
                ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_CODETextBox")).ReadOnly = true;
                ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_CODETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_HAZ_WASTE_IND.Trim().Length > 0)
            {
                ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_INDTextBox")).ReadOnly = true;
                ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_INDTextBox")).BackColor = System.Drawing.Color.LightGray;
            }
            if (msds.EPA_HAZ_WASTE_NAME.Trim().Length > 0)
            {
                ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_NAMETextBox")).ReadOnly = true;
                ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_NAMETextBox")).BackColor = System.Drawing.Color.LightGray;
            }
        }
        private void Populate_MonthddList()
        {
            drpMonthCal.Items.Add("January");
            drpMonthCal.Items.Add("February");
            drpMonthCal.Items.Add("March");
            drpMonthCal.Items.Add("April");
            drpMonthCal.Items.Add("May");
            drpMonthCal.Items.Add("June");
            drpMonthCal.Items.Add("July");
            drpMonthCal.Items.Add("August");
            drpMonthCal.Items.Add("September");
            drpMonthCal.Items.Add("October");
            drpMonthCal.Items.Add("November");
            drpMonthCal.Items.Add("December");
            drpMonthCal.SelectedIndex = DateTime.Now.Month - 1;
        }
        private void Populate_YearddList()
        {
            int intYear;
            for (intYear = Convert.ToInt16(DateTime.Now.Year) - 30; intYear <= DateTime.Now.Year + 30; intYear++)
            {
                drpYearCal.Items.Add(Convert.ToString(intYear));
            }
            drpYearCal.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }
        protected void drpCalMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            cal_productDate.TodaysDate = Convert.ToDateTime(drpMonthCal.SelectedItem.Value + " 1, " + drpYearCal.SelectedItem.Value);
        }
        protected void drpCalYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            cal_productDate.TodaysDate = Convert.ToDateTime(drpMonthCal.SelectedItem.Value + " 1, " + drpYearCal.SelectedItem.Value);
        }
        protected void cal_expDate_MonthChanged(Object sender, MonthChangedEventArgs e)
        {
            drpMonthCal.SelectedIndex = e.NewDate.Month - 1;
            drpYearCal.SelectedValue = e.NewDate.Year.ToString();
        }
        protected void btn_cancelCalendar_Click(object sender, EventArgs e)
        {
            CalendarPopup.Visible = false;
        }

        protected void btn_editMSDS_Click(String msdsSerNo)
        {
            msdsOriginalValues.Value = "";
            DatabaseManager manager = new DatabaseManager();
            MsdsRecord msds = manager.getMSDSRecordforSerialNumber(msdsSerNo);
            if (msds == null) return;

            // enumerate all the members of the original Msds Record and serialized into a JSON string
            MsdsRecord msdsOriginal = manager.getMSDSRecordforSerialNumber(msdsSerNo, "N");
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            msdsOriginalValues.Value = "oVals = " + oSerializer.Serialize(msdsOriginal);

            ((TextBox) FormViewMSDS.FindControl("MSDSSERNOTextBox")).Text = msds.MSDSSERNO;
            ((TextBox) FormViewMSDS.FindControl("CAGETextBox")).Text = msds.CAGE;
            ((TextBox) FormViewMSDS.FindControl("MANUFACTURERTextBox")).Text = msds.MANUFACTURER;
            ((TextBox)FormViewMSDS.FindControl("PARTNOTextBox")).Text = msds.PARTNO;
            ((TextBox)FormViewMSDS.FindControl("FSCTextBox")).Text = msds.FSC;
            ((TextBox)FormViewMSDS.FindControl("NIINTextBox")).Text = msds.NIIN;

            if (msds.hcc_id > 0)
            ((DropDownList)FormViewMSDS.FindControl("dd_hcc")).SelectedValue = msds.hcc_id.ToString();

            ((TextBox)FormViewMSDS.FindControl("ARTICLE_INDTextBox")).Text = msds.ARTICLE_IND;
            ((TextBox)FormViewMSDS.FindControl("DESCRIPTIONTextBox")).Text = msds.DESCRIPTION;
            ((TextBox)FormViewMSDS.FindControl("EMERGENCY_TELTextBox")).Text = msds.EMERGENCY_TEL;
            ((TextBox)FormViewMSDS.FindControl("END_COMP_INDTextBox")).Text = msds.END_COMP_IND;
            ((TextBox)FormViewMSDS.FindControl("END_ITEM_INDTextBox")).Text = msds.END_ITEM_IND;
            ((TextBox)FormViewMSDS.FindControl("KIT_INDTextBox")).Text = msds.KIT_IND;
            ((TextBox)FormViewMSDS.FindControl("KIT_PART_INDTextBox")).Text = msds.KIT_PART_IND;
            ((TextBox)FormViewMSDS.FindControl("MANUFACTURER_MSDS_NOTextBox")).Text = msds.MANUFACTURER_MSDS_NO;
            ((TextBox)FormViewMSDS.FindControl("MIXTURE_INDTextBox")).Text = msds.MIXTURE_IND;
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_IDENTITYTextBox")).Text = msds.PRODUCT_IDENTITY;
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_LOAD_DATETextBox")).Text = msds.PRODUCT_LOAD_DATE;
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_RECORD_STATUSTextBox")).Text = msds.PRODUCT_RECORD_STATUS;
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_REVISION_NOTextBox")).Text = msds.PRODUCT_REVISION_NO;
            ((TextBox)FormViewMSDS.FindControl("PROPRIETARY_INDTextBox")).Text = msds.PROPRIETARY_IND;
            ((TextBox)FormViewMSDS.FindControl("PUBLISHED_INDTextBox")).Text = msds.PUBLISHED_IND;
            ((TextBox)FormViewMSDS.FindControl("PURCHASED_PROD_INDTextBox")).Text = msds.PURCHASED_PROD_IND;
            ((TextBox)FormViewMSDS.FindControl("PURE_INDTextBox")).Text = msds.PURE_IND;
            ((TextBox)FormViewMSDS.FindControl("RADIOACTIVE_INDTextBox")).Text = msds.RADIOACTIVE_IND;
            ((TextBox)FormViewMSDS.FindControl("SERVICE_AGENCYTextBox")).Text = msds.SERVICE_AGENCY;
            ((TextBox)FormViewMSDS.FindControl("TRADE_NAMETextBox")).Text = msds.TRADE_NAME;
            ((TextBox)FormViewMSDS.FindControl("TRADE_SECRET_INDTextBox")).Text = msds.TRADE_SECRET_IND;
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_INDTextBox")).Text = msds.PRODUCT_IND;
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_LANGUAGETextBox")).Text = msds.PRODUCT_LANGUAGE;

            //phys chemical
            ((TextBox)FormViewPhysChemical.FindControl("APP_ODORTextBox")).Text = msds.APP_ODOR;
            ((TextBox)FormViewPhysChemical.FindControl("VAPOR_PRESSTextBox")).Text = msds.VAPOR_PRESS;
            ((TextBox)FormViewPhysChemical.FindControl("VAPOR_DENSTextBox")).Text = msds.VAPOR_DENS;
            ((TextBox)FormViewPhysChemical.FindControl("SPECIFIC_GRAVTextBox")).Text = msds.SPECIFIC_GRAV;
            ((TextBox)FormViewPhysChemical.FindControl("EVAP_RATE_REFTextBox")).Text = msds.EVAP_RATE_REF;
            ((TextBox)FormViewPhysChemical.FindControl("SOL_IN_WATERTextBox")).Text = msds.SOL_IN_WATER;
            ((TextBox)FormViewPhysChemical.FindControl("PERCENT_VOL_VOLUMETextBox")).Text = msds.PERCENT_VOL_VOLUME;
            ((TextBox)FormViewPhysChemical.FindControl("VISCOSITYTextBox")).Text = msds.VISCOSITY;
            ((TextBox)FormViewPhysChemical.FindControl("VOC_POUNDS_GALLONTextBox")).Text = msds.VOC_POUNDS_GALLON;
            ((TextBox)FormViewPhysChemical.FindControl("VOC_GRAMS_LITERTextBox")).Text = msds.VOC_GRAMS_LITER;
            ((TextBox)FormViewPhysChemical.FindControl("PHTextBox")).Text = msds.PH;
            ((TextBox)FormViewPhysChemical.FindControl("AUTOIGNITION_TEMPTextBox")).Text = msds.AUTOIGNITION_TEMP;
            ((TextBox)FormViewPhysChemical.FindControl("CARCINOGEN_INDTextBox")).Text = msds.CARCINOGEN_IND;
            ((TextBox)FormViewPhysChemical.FindControl("EPA_ACUTETextBox")).Text = msds.EPA_ACUTE;
            ((TextBox)FormViewPhysChemical.FindControl("EPA_CHRONICTextBox")).Text = msds.EPA_CHRONIC;
            ((TextBox)FormViewPhysChemical.FindControl("EPA_FIRETextBox")).Text = msds.EPA_FIRE;
            ((TextBox)FormViewPhysChemical.FindControl("EPA_PRESSURETextBox")).Text = msds.EPA_PRESSURE;
            ((TextBox)FormViewPhysChemical.FindControl("EPA_REACTIVITYTextBox")).Text = msds.EPA_REACTIVITY;
            ((TextBox)FormViewPhysChemical.FindControl("FLASH_PT_TEMPTextBox")).Text = msds.FLASH_PT_TEMP;
            ((TextBox)FormViewPhysChemical.FindControl("NEUT_AGENTTextBox")).Text = msds.NEUT_AGENT;
            ((TextBox)FormViewPhysChemical.FindControl("NFPA_FLAMMABILITYTextBox")).Text = msds.NFPA_FLAMMABILITY;
            ((TextBox)FormViewPhysChemical.FindControl("NFPA_HEALTHTextBox")).Text = msds.NFPA_HEALTH;
            ((TextBox)FormViewPhysChemical.FindControl("NFPA_REACTIVITYTextBox")).Text = msds.NFPA_REACTIVITY;
            ((TextBox)FormViewPhysChemical.FindControl("NFPA_SPECIALTextBox")).Text = msds.NFPA_SPECIAL;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_CARCINOGENSTextBox")).Text = msds.OSHA_CARCINOGENS;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMB_LIQUIDTextBox")).Text = msds.OSHA_COMB_LIQUID;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMP_GASTextBox")).Text = msds.OSHA_COMP_GAS;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_CORROSIVETextBox")).Text = msds.OSHA_CORROSIVE;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_EXPLOSIVETextBox")).Text = msds.OSHA_EXPLOSIVE;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_FLAMMABLETextBox")).Text = msds.OSHA_FLAMMABLE;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_HIGH_TOXICTextBox")).Text = msds.OSHA_HIGH_TOXIC;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_IRRITANTTextBox")).Text = msds.OSHA_IRRITANT;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_ORG_PEROXTextBox")).Text = msds.OSHA_ORG_PEROX;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_OTHERLONGTERMTextBox")).Text = msds.OSHA_OTHERLONGTERM;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_OXIDIZERTextBox")).Text = msds.OSHA_OXIDIZER;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_PYROTextBox")).Text = msds.OSHA_PYRO;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_SENSITIZERTextBox")).Text = msds.OSHA_SENSITIZER;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_TOXICTextBox")).Text = msds.OSHA_TOXIC;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_UNST_REACTTextBox")).Text = msds.OSHA_UNST_REACT;
            ((TextBox)FormViewPhysChemical.FindControl("OTHER_SHORT_TERMTextBox")).Text = msds.OTHER_SHORT_TERM;
            ((TextBox)FormViewPhysChemical.FindControl("PHYS_STATE_CODETextBox")).Text = msds.PHYS_STATE_CODE;
            ((TextBox)FormViewPhysChemical.FindControl("VOL_ORG_COMP_WTTextBox")).Text = msds.VOL_ORG_COMP_WT;
            ((TextBox)FormViewPhysChemical.FindControl("OSHA_WATER_REACTIVETextBox")).Text = msds.OSHA_WATER_REACTIVE;

            //ingredients;
            MsdsIngredients ingredient = msds.ingredientsList[0];
            ((TextBox)FormViewIngredients.FindControl("INGREDIENT_NAMETextBox")).Text = ingredient.INGREDIENT_NAME;
            ((TextBox)FormViewIngredients.FindControl("PRCNTTextBox")).Text = ingredient.PRCNT;
            ((TextBox)FormViewIngredients.FindControl("CAS_CODETextBox")).Text = ingredient.CAS;
            ((TextBox)FormViewIngredients.FindControl("RTECS_NUMTextBox")).Text = ingredient.RTECS_NUM;
            ((TextBox)FormViewIngredients.FindControl("RTECS_CODETextBox")).Text = ingredient.RTECS_CODE;
            ((TextBox)FormViewIngredients.FindControl("OSHA_PELTextBox")).Text = ingredient.OSHA_PEL;
            ((TextBox)FormViewIngredients.FindControl("OSHA_STELTextBox")).Text = ingredient.OSHA_STEL;
            ((TextBox)FormViewIngredients.FindControl("ACGIH_TLVTextBox")).Text = ingredient.ACGIH_TLV;
            ((TextBox)FormViewIngredients.FindControl("ACGIH_STELTextBox")).Text = ingredient.ACGIH_STEL;
            ((TextBox)FormViewIngredients.FindControl("EPA_REPORT_QTYTextBox")).Text = ingredient.EPA_REPORT_QTY;
            ((TextBox)FormViewIngredients.FindControl("DOT_REPORT_QTYTextBox")).Text = ingredient.DOT_REPORT_QTY;
            ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_VALUETextBox")).Text = ingredient.PRCNT_VOL_VALUE;
            ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_WEIGHTTextBox")).Text = ingredient.PRCNT_VOL_WEIGHT;
            ((TextBox)FormViewIngredients.FindControl("CHEM_MFG_COMP_NAMETextBox")).Text = ingredient.CHEM_MFG_COMP_NAME;
            ((TextBox)FormViewIngredients.FindControl("ODS_INDTextBox")).Text = ingredient.ODS_IND;
            ((TextBox)FormViewIngredients.FindControl("OTHER_REC_LIMITSTextBox")).Text = ingredient.OTHER_REC_LIMITS;
            

            //contractor information;
            MsdsContractors contractor = msds.contractorsList[0];
            ((TextBox)FormViewContractorInfo.FindControl("CT_NUMBERTextBox")).Text = contractor.CT_NUMBER;
            ((TextBox)FormViewContractorInfo.FindControl("CT_CAGETextBox")).Text = contractor.CT_CAGE;
            ((TextBox)FormViewContractorInfo.FindControl("CT_CITYTextBox")).Text = contractor.CT_CITY;
            ((TextBox)FormViewContractorInfo.FindControl("CT_COMPANY_NAMETextBox")).Text = contractor.CT_COMPANY_NAME;
            ((TextBox)FormViewContractorInfo.FindControl("CT_COUNTRYTextBox")).Text = contractor.CT_COUNTRY;
            ((TextBox)FormViewContractorInfo.FindControl("CT_PO_BOXTextBox")).Text = contractor.CT_PO_BOX;
            ((TextBox)FormViewContractorInfo.FindControl("CT_PHONETextBox")).Text = contractor.CT_PHONE;
            ((TextBox)FormViewContractorInfo.FindControl("PURCHASE_ORDER_NOTextBox")).Text = contractor.PURCHASE_ORDER_NO;
            ((TextBox)FormViewContractorInfo.FindControl("CT_STATETextBox")).Text = contractor.CT_STATE;


            //radiological info;
            ((TextBox)FormViewRadiologicalInfo.FindControl("NRC_LP_NUMTextBox")).Text = msds.NRC_LP_NUM;
            ((TextBox)FormViewRadiologicalInfo.FindControl("OPERATORTextBox")).Text = msds.OPERATOR;
            ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_AMOUNT_MICROTextBox")).Text = msds.RAD_AMOUNT_MICRO;
            ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_FORMTextBox")).Text = msds.RAD_FORM;
            ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_CASTextBox")).Text = msds.RAD_CAS;
            ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_NAMETextBox")).Text = msds.RAD_NAME;
            ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_SYMBOLTextBox")).Text = msds.RAD_SYMBOL;
            ((TextBox)FormViewRadiologicalInfo.FindControl("REP_NSNTextBox")).Text = msds.REP_NSN;
            ((TextBox)FormViewRadiologicalInfo.FindControl("SEALEDTextBox")).Text = msds.SEALED;

            //transportation;
            ((TextBox)FormViewTransportation.FindControl("AF_MMAC_CODETextBox")).Text = msds.AF_MMAC_CODE;
            ((TextBox)FormViewTransportation.FindControl("CERTIFICATE_COETextBox")).Text = msds.CERTIFICATE_COE;
            ((TextBox)FormViewTransportation.FindControl("COMPETENT_CAATextBox")).Text = msds.COMPETENT_CAA;
            ((TextBox)FormViewTransportation.FindControl("DOD_ID_CODETextBox")).Text = msds.DOD_ID_CODE;
            ((TextBox)FormViewTransportation.FindControl("DOT_EXEMPTION_NOTextBox")).Text = msds.DOT_EXEMPTION_NO;
            ((TextBox)FormViewTransportation.FindControl("DOT_RQ_INDTextBox")).Text = msds.DOT_RQ_IND;
            ((TextBox)FormViewTransportation.FindControl("EX_NOTextBox")).Text = msds.EX_NO;
            ((TextBox)FormViewTransportation.FindControl("TRAN_FLASH_PT_TEMPTextBox")).Text = msds.TRAN_FLASH_PT_TEMP;
            ((TextBox)FormViewTransportation.FindControl("HIGH_EXPLOSIVE_WTTextBox")).Text = msds.HIGH_EXPLOSIVE_WT.ToString();
            ((TextBox)FormViewTransportation.FindControl("LTD_QTY_INDTextBox")).Text = msds.LTD_QTY_IND;
            ((TextBox)FormViewTransportation.FindControl("MAGNETIC_INDTextBox")).Text = msds.MAGNETIC_IND;
            ((TextBox)FormViewTransportation.FindControl("MAGNETISMTextBox")).Text = msds.MAGNETISM;
            ((TextBox)FormViewTransportation.FindControl("MARINE_POLLUTANT_INDTextBox")).Text = msds.MARINE_POLLUTANT_IND;
            ((TextBox)FormViewTransportation.FindControl("NET_EXP_QTY_DISTTextBox")).Text = msds.NET_EXP_QTY_DIST.ToString();
            ((TextBox)FormViewTransportation.FindControl("NET_EXP_WEIGHTTextBox")).Text = msds.NET_EXP_WEIGHT;
            ((TextBox)FormViewTransportation.FindControl("NET_PROPELLANT_WTTextBox")).Text = msds.NET_PROPELLANT_WT;
            ((TextBox)FormViewTransportation.FindControl("NOS_TECHNICAL_SHIPPING_NAMETextBox")).Text = msds.NOS_TECHNICAL_SHIPPING_NAME;
            ((TextBox)FormViewTransportation.FindControl("TRANSPORTATION_ADDITIONAL_DATATextBox")).Text = msds.TRANSPORTATION_ADDITIONAL_DATA;



            //dot psn
            ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_CLASS_DIVTextBox")).Text = msds.DOT_HAZARD_CLASS_DIV;
            ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_LABELTextBox")).Text = msds.DOT_HAZARD_LABEL;
            ((TextBox)FormViewDOT.FindControl("DOT_MAX_CARGOTextBox")).Text = msds.DOT_MAX_CARGO;
            ((TextBox)FormViewDOT.FindControl("DOT_MAX_PASSENGERTextBox")).Text = msds.DOT_MAX_PASSENGER;
            ((TextBox)FormViewDOT.FindControl("DOT_PACK_BULKTextBox")).Text = msds.DOT_PACK_BULK;
            ((TextBox)FormViewDOT.FindControl("DOT_PACK_EXCEPTIONSTextBox")).Text = msds.DOT_PACK_EXCEPTIONS;
            ((TextBox)FormViewDOT.FindControl("DOT_PACK_NONBULKTextBox")).Text = msds.DOT_PACK_NONBULK;
            ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_NAMETextBox")).Text = msds.DOT_PROP_SHIP_NAME;
            ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_MODIFIERTextBox")).Text = msds.DOT_PROP_SHIP_MODIFIER;
            ((TextBox)FormViewDOT.FindControl("DOT_PSN_CODETextBox")).Text = msds.DOT_PSN_CODE;
            ((TextBox)FormViewDOT.FindControl("DOT_SPECIAL_PROVISIONTextBox")).Text = msds.DOT_SPECIAL_PROVISION;
            ((TextBox)FormViewDOT.FindControl("DOT_SYMBOLSTextBox")).Text = msds.DOT_SYMBOLS;
            ((TextBox)FormViewDOT.FindControl("DOT_UN_ID_NUMBERTextBox")).Text = msds.DOT_UN_ID_NUMBER;
            ((TextBox)FormViewDOT.FindControl("DOT_WATER_OTHER_REQTextBox")).Text = msds.DOT_WATER_OTHER_REQ;
            ((TextBox)FormViewDOT.FindControl("DOT_WATER_VESSEL_STOWTextBox")).Text = msds.DOT_WATER_VESSEL_STOW;
            ((TextBox)FormViewDOT.FindControl("DOT_PACK_GROUPTextBox")).Text = msds.DOT_PACK_GROUP;


            //afjm psn
            ((TextBox)FormViewAFJM.FindControl("AFJM_HAZARD_CLASSTextBox")).Text = msds.AFJM_HAZARD_CLASS;
            ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_PARAGRAPHTextBox")).Text = msds.AFJM_PACK_PARAGRAPH;
            ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_GROUPTextBox")).Text = msds.AFJM_PACK_GROUP;
            ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_NAMETextBox")).Text = msds.AFJM_PROP_SHIP_NAME;
            ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_MODIFIERTextBox")).Text = msds.AFJM_PROP_SHIP_MODIFIER;
            ((TextBox)FormViewAFJM.FindControl("AFJM_PSN_CODETextBox")).Text = msds.AFJM_PSN_CODE;
            ((TextBox)FormViewAFJM.FindControl("AFJM_SPECIAL_PROVTextBox")).Text = msds.AFJM_SPECIAL_PROV;
            ((TextBox)FormViewAFJM.FindControl("AFJM_SUBSIDIARY_RISKTextBox")).Text = msds.AFJM_SUBSIDIARY_RISK;
            ((TextBox)FormViewAFJM.FindControl("AFJM_SYMBOLSTextBox")).Text = msds.AFJM_SYMBOLS;
            ((TextBox)FormViewAFJM.FindControl("AFJM_UN_ID_NUMBERTextBox")).Text = msds.AFJM_UN_ID_NUMBER;



            //iata psn
            ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACKINGTextBox")).Text = msds.IATA_CARGO_PACKING;
            ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_CLASSTextBox")).Text = msds.IATA_HAZARD_CLASS;
            ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_LABELTextBox")).Text = msds.IATA_HAZARD_LABEL;
            ((TextBox)FormViewIATA.FindControl("IATA_PACK_GROUPTextBox")).Text = msds.IATA_PACK_GROUP;
            ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_INSTRTextBox")).Text = msds.IATA_PASS_AIR_PACK_LMT_INSTR;
            ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_PER_PKGTextBox")).Text = msds.IATA_PASS_AIR_PACK_LMT_PER_PKG;
            ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_NOTETextBox")).Text = msds.IATA_PASS_AIR_PACK_NOTE;
            ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_NAMETextBox")).Text = msds.IATA_PROP_SHIP_NAME;
            ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_MODIFIERTextBox")).Text = msds.IATA_PROP_SHIP_MODIFIER;
            ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACK_MAX_QTYTextBox")).Text = msds.IATA_CARGO_PACK_MAX_QTY;
            ((TextBox)FormViewIATA.FindControl("IATA_PSN_CODETextBox")).Text = msds.IATA_PSN_CODE;
            ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_MAX_QTYTextBox")).Text = msds.IATA_PASS_AIR_MAX_QTY;
            ((TextBox)FormViewIATA.FindControl("IATA_SPECIAL_PROVTextBox")).Text = msds.IATA_SPECIAL_PROV;
            ((TextBox)FormViewIATA.FindControl("IATA_SUBSIDIARY_RISKTextBox")).Text = msds.IATA_SUBSIDIARY_RISK;
            ((TextBox)FormViewIATA.FindControl("IATA_UN_ID_NUMBERTextBox")).Text = msds.IATA_UN_ID_NUMBER;



            //imo psn          
            ((TextBox)FormViewIMO.FindControl("IMO_EMS_NOTextBox")).Text = msds.IMO_EMS_NO;
            ((TextBox)FormViewIMO.FindControl("IMO_HAZARD_CLASSTextBox")).Text = msds.IMO_HAZARD_CLASS;
            ((TextBox)FormViewIMO.FindControl("IMO_IBC_INSTRTextBox")).Text = msds.IMO_IBC_INSTR;
            ((TextBox)FormViewIMO.FindControl("IMO_LIMITED_QTYTextBox")).Text = msds.IMO_LIMITED_QTY;
            ((TextBox)FormViewIMO.FindControl("IMO_PACK_GROUPTextBox")).Text = msds.IMO_PACK_GROUP;
            ((TextBox)FormViewIMO.FindControl("IMO_PACK_INSTRUCTIONSTextBox")).Text = msds.IMO_PACK_INSTRUCTIONS;
            ((TextBox)FormViewIMO.FindControl("IMO_PACK_PROVISIONSTextBox")).Text = msds.IMO_PACK_PROVISIONS;
            ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_NAMETextBox")).Text = msds.IMO_PROP_SHIP_NAME;
            ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_MODIFIERTextBox")).Text = msds.IMO_PROP_SHIP_MODIFIER;
            ((TextBox)FormViewIMO.FindControl("IMO_PSN_CODETextBox")).Text = msds.IMO_PSN_CODE;
            ((TextBox)FormViewIMO.FindControl("IMO_SPECIAL_PROVTextBox")).Text = msds.IMO_SPECIAL_PROV;
            ((TextBox)FormViewIMO.FindControl("IMO_STOW_SEGRTextBox")).Text = msds.IMO_STOW_SEGR;
            ((TextBox)FormViewIMO.FindControl("IMO_SUBSIDIARY_RISKTextBox")).Text = msds.IMO_SUBSIDIARY_RISK;
            ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_IMOTextBox")).Text = msds.IMO_TANK_INSTR_IMO;
            ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_PROVTextBox")).Text = msds.IMO_TANK_INSTR_PROV;
            ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_UNTextBox")).Text = msds.IMO_TANK_INSTR_UN;
            ((TextBox)FormViewIMO.FindControl("IMO_UN_NUMBERTextBox")).Text = msds.IMO_UN_NUMBER;
            ((TextBox)FormViewIMO.FindControl("IMO_IBC_PROVISIONSTextBox")).Text = msds.IMO_IBC_PROVISIONS;



            //item description;
            ((TextBox)FormViewItemDescription.FindControl("ITEM_MANAGERTextBox")).Text = msds.ITEM_MANAGER;
            ((TextBox)FormViewItemDescription.FindControl("ITEM_NAMETextBox")).Text = msds.ITEM_NAME;
            ((TextBox)FormViewItemDescription.FindControl("SPECIFICATION_NUMBERTextBox")).Text = msds.SPECIFICATION_NUMBER;
            ((TextBox)FormViewItemDescription.FindControl("TYPE_GRADE_CLASSTextBox")).Text = msds.TYPE_GRADE_CLASS;
            ((TextBox)FormViewItemDescription.FindControl("UNIT_OF_ISSUETextBox")).Text = msds.UNIT_OF_ISSUE;
            ((TextBox)FormViewItemDescription.FindControl("QUANTITATIVE_EXPRESSIONTextBox")).Text = msds.QUANTITATIVE_EXPRESSION;
            ((TextBox)FormViewItemDescription.FindControl("UI_CONTAINER_QTYTextBox")).Text = msds.UI_CONTAINER_QTY;
            ((TextBox)FormViewItemDescription.FindControl("TYPE_OF_CONTAINERTextBox")).Text = msds.TYPE_OF_CONTAINER;
            ((TextBox)FormViewItemDescription.FindControl("BATCH_NUMBERTextBox")).Text = msds.BATCH_NUMBER;
            ((TextBox)FormViewItemDescription.FindControl("LOT_NUMBERTextBox")).Text = msds.LOT_NUMBER;
            ((TextBox)FormViewItemDescription.FindControl("LOG_FLIS_NIIN_VERTextBox")).Text = msds.LOG_FLIS_NIIN_VER;
            ((TextBox)FormViewItemDescription.FindControl("LOG_FSCTextBox")).Text = msds.LOG_FSC.ToString();
            ((TextBox)FormViewItemDescription.FindControl("NET_UNIT_WEIGHTTextBox")).Text = msds.NET_UNIT_WEIGHT;
            ((TextBox)FormViewItemDescription.FindControl("SHELF_LIFE_CODETextBox")).Text = msds.SHELF_LIFE_CODE;
            ((TextBox)FormViewItemDescription.FindControl("SPECIAL_EMP_CODETextBox")).Text = msds.SPECIAL_EMP_CODE;
            ((TextBox)FormViewItemDescription.FindControl("UN_NA_NUMBERTextBox")).Text = msds.UN_NA_NUMBER;
            ((TextBox)FormViewItemDescription.FindControl("UPC_GTINTextBox")).Text = msds.UPC_GTIN;

            //label information;
            ((TextBox)FormViewLabelInfo.FindControl("COMPANY_CAGE_RPTextBox")).Text = msds.COMPANY_CAGE_RP;
            ((TextBox)FormViewLabelInfo.FindControl("COMPANY_NAME_RPTextBox")).Text = msds.COMPANY_NAME_RP;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_EMERG_PHONETextBox")).Text = msds.LABEL_EMERG_PHONE;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_ITEM_NAMETextBox")).Text = msds.LABEL_ITEM_NAME;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROC_YEARTextBox")).Text = msds.LABEL_PROC_YEAR;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_IDENTTextBox")).Text = msds.LABEL_PROD_IDENT;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_SERIALNOTextBox")).Text = msds.LABEL_PROD_SERIALNO;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_SIGNAL_WORDTextBox")).Text = msds.LABEL_SIGNAL_WORD;
            ((TextBox)FormViewLabelInfo.FindControl("LABEL_STOCK_NOTextBox")).Text = msds.LABEL_STOCK_NO;
            ((TextBox)FormViewLabelInfo.FindControl("SPECIFIC_HAZARDSTextBox")).Text = msds.SPECIFIC_HAZARDS;

            //disposal info;
            ((TextBox)FormViewDisposal.FindControl("DISPOSAL_ADD_INFOTextBox")).Text = msds.DISPOSAL_ADD_INFO;
            ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_CODETextBox")).Text = msds.EPA_HAZ_WASTE_CODE;
            ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_INDTextBox")).Text = msds.EPA_HAZ_WASTE_IND;
            ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_NAMETextBox")).Text = msds.EPA_HAZ_WASTE_NAME;

            // If msdsOriginal exists, make existing data uneditable
            if (msdsOriginal != null) {
                fixFields(msdsOriginal);
            }

        }
        private void makeDropDownVisible(bool visible)
        {
            try
            {
                DropDownList dd_hcc = (DropDownList)FormViewMSDS.FindControl("dd_hcc");
                dd_hcc.Visible = visible;

                Image dd_image1 = (Image)FormViewMSDS.FindControl("dd_image1");
                dd_image1.Visible = !visible;
            }
            catch { }

        }
        [CoverageExclude]
        protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            

            
        }
        [CoverageExclude]
        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {



        }
        [CoverageExclude]
        protected void btn_saveMSDS_Click(object sender, EventArgs e)
        {
            //Save
            DatabaseManager manager = new DatabaseManager();
            MsdsRecord msds = new MsdsRecord();

            //msds
            msds.MSDSSERNO = ((TextBox)FormViewMSDS.FindControl("MSDSSERNOTextBox")).Text;
            msds.CAGE = ((TextBox)FormViewMSDS.FindControl("CAGETextBox")).Text;            
            msds.MANUFACTURER = ((TextBox)FormViewMSDS.FindControl("MANUFACTURERTextBox")).Text;
            msds.PARTNO = ((TextBox)FormViewMSDS.FindControl("PARTNOTextBox")).Text;
            msds.FSC = ((TextBox)FormViewMSDS.FindControl("FSCTextBox")).Text;
            msds.NIIN = ((TextBox)FormViewMSDS.FindControl("NIINTextBox")).Text;

            msds.ARTICLE_IND = ((TextBox)FormViewMSDS.FindControl("ARTICLE_INDTextBox")).Text;
            msds.DESCRIPTION = ((TextBox)FormViewMSDS.FindControl("DESCRIPTIONTextBox")).Text;
            msds.EMERGENCY_TEL = ((TextBox)FormViewMSDS.FindControl("EMERGENCY_TELTextBox")).Text;
            msds.END_COMP_IND = ((TextBox)FormViewMSDS.FindControl("END_COMP_INDTextBox")).Text;
            msds.END_ITEM_IND = ((TextBox)FormViewMSDS.FindControl("END_ITEM_INDTextBox")).Text;
            msds.KIT_IND = ((TextBox)FormViewMSDS.FindControl("KIT_INDTextBox")).Text;
            msds.KIT_PART_IND = ((TextBox)FormViewMSDS.FindControl("KIT_PART_INDTextBox")).Text;
            msds.MANUFACTURER_MSDS_NO = ((TextBox)FormViewMSDS.FindControl("MANUFACTURER_MSDS_NOTextBox")).Text;
            msds.MIXTURE_IND = ((TextBox)FormViewMSDS.FindControl("MIXTURE_INDTextBox")).Text;
            msds.PRODUCT_IDENTITY = ((TextBox)FormViewMSDS.FindControl("PRODUCT_IDENTITYTextBox")).Text;
            msds.PRODUCT_LOAD_DATE = ((TextBox)FormViewMSDS.FindControl("PRODUCT_LOAD_DATETextBox")).Text;
            msds.PRODUCT_RECORD_STATUS = ((TextBox)FormViewMSDS.FindControl("PRODUCT_RECORD_STATUSTextBox")).Text;
            msds.PRODUCT_REVISION_NO = ((TextBox)FormViewMSDS.FindControl("PRODUCT_REVISION_NOTextBox")).Text;
            msds.PROPRIETARY_IND = ((TextBox)FormViewMSDS.FindControl("PROPRIETARY_INDTextBox")).Text;
            msds.PUBLISHED_IND = ((TextBox)FormViewMSDS.FindControl("PUBLISHED_INDTextBox")).Text;
            msds.PURCHASED_PROD_IND = ((TextBox)FormViewMSDS.FindControl("PURCHASED_PROD_INDTextBox")).Text;
            msds.PURE_IND = ((TextBox)FormViewMSDS.FindControl("PURE_INDTextBox")).Text;
            msds.RADIOACTIVE_IND = ((TextBox)FormViewMSDS.FindControl("RADIOACTIVE_INDTextBox")).Text;
            msds.SERVICE_AGENCY = ((TextBox)FormViewMSDS.FindControl("SERVICE_AGENCYTextBox")).Text;
            msds.TRADE_NAME = ((TextBox)FormViewMSDS.FindControl("TRADE_NAMETextBox")).Text;
            msds.TRADE_SECRET_IND = ((TextBox)FormViewMSDS.FindControl("TRADE_SECRET_INDTextBox")).Text;
            msds.PRODUCT_IND = ((TextBox)FormViewMSDS.FindControl("PRODUCT_INDTextBox")).Text;
            msds.PRODUCT_LANGUAGE = ((TextBox)FormViewMSDS.FindControl("PRODUCT_LANGUAGETextBox")).Text;
            
            
            DropDownList d = (DropDownList)FormViewMSDS.FindControl("dd_hcc");
            if(d.Items.Count!=0)
            {
                msds.hcc_id = Int32.Parse(((DropDownList)FormViewMSDS.FindControl("dd_hcc")).SelectedValue);
            }

            //phys chemical
            msds.APP_ODOR = ((TextBox)FormViewPhysChemical.FindControl("APP_ODORTextBox")).Text;
            msds.VAPOR_PRESS = ((TextBox)FormViewPhysChemical.FindControl("VAPOR_PRESSTextBox")).Text;
            msds.VAPOR_DENS = ((TextBox)FormViewPhysChemical.FindControl("VAPOR_DENSTextBox")).Text;
            msds.SPECIFIC_GRAV = ((TextBox)FormViewPhysChemical.FindControl("SPECIFIC_GRAVTextBox")).Text;
            msds.EVAP_RATE_REF = ((TextBox)FormViewPhysChemical.FindControl("EVAP_RATE_REFTextBox")).Text;
            msds.SOL_IN_WATER = ((TextBox)FormViewPhysChemical.FindControl("SOL_IN_WATERTextBox")).Text;
            msds.PERCENT_VOL_VOLUME = ((TextBox)FormViewPhysChemical.FindControl("PERCENT_VOL_VOLUMETextBox")).Text;
            msds.VISCOSITY = ((TextBox)FormViewPhysChemical.FindControl("VISCOSITYTextBox")).Text;
            msds.VOC_POUNDS_GALLON = ((TextBox)FormViewPhysChemical.FindControl("VOC_POUNDS_GALLONTextBox")).Text;
            msds.VOC_GRAMS_LITER = ((TextBox)FormViewPhysChemical.FindControl("VOC_GRAMS_LITERTextBox")).Text;
            msds.PH = ((TextBox)FormViewPhysChemical.FindControl("PHTextBox")).Text;
            msds.AUTOIGNITION_TEMP = ((TextBox)FormViewPhysChemical.FindControl("AUTOIGNITION_TEMPTextBox")).Text;
            msds.CARCINOGEN_IND = ((TextBox)FormViewPhysChemical.FindControl("CARCINOGEN_INDTextBox")).Text;
            msds.EPA_ACUTE = ((TextBox)FormViewPhysChemical.FindControl("EPA_ACUTETextBox")).Text;
            msds.EPA_CHRONIC = ((TextBox)FormViewPhysChemical.FindControl("EPA_CHRONICTextBox")).Text;
            msds.EPA_FIRE = ((TextBox)FormViewPhysChemical.FindControl("EPA_FIRETextBox")).Text;
            msds.EPA_PRESSURE = ((TextBox)FormViewPhysChemical.FindControl("EPA_PRESSURETextBox")).Text;
            msds.EPA_REACTIVITY = ((TextBox)FormViewPhysChemical.FindControl("EPA_REACTIVITYTextBox")).Text;
            msds.FLASH_PT_TEMP = ((TextBox)FormViewPhysChemical.FindControl("FLASH_PT_TEMPTextBox")).Text;
            msds.NEUT_AGENT = ((TextBox)FormViewPhysChemical.FindControl("NEUT_AGENTTextBox")).Text;
            msds.NFPA_FLAMMABILITY = ((TextBox)FormViewPhysChemical.FindControl("NFPA_FLAMMABILITYTextBox")).Text;
            msds.NFPA_HEALTH = ((TextBox)FormViewPhysChemical.FindControl("NFPA_HEALTHTextBox")).Text;
            msds.NFPA_REACTIVITY = ((TextBox)FormViewPhysChemical.FindControl("NFPA_REACTIVITYTextBox")).Text;
            msds.NFPA_SPECIAL = ((TextBox)FormViewPhysChemical.FindControl("NFPA_SPECIALTextBox")).Text;
            msds.OSHA_CARCINOGENS = ((TextBox)FormViewPhysChemical.FindControl("OSHA_CARCINOGENSTextBox")).Text;
            msds.OSHA_COMB_LIQUID = ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMB_LIQUIDTextBox")).Text;
            msds.OSHA_COMP_GAS = ((TextBox)FormViewPhysChemical.FindControl("OSHA_COMP_GASTextBox")).Text;
            msds.OSHA_CORROSIVE = ((TextBox)FormViewPhysChemical.FindControl("OSHA_CORROSIVETextBox")).Text;
            msds.OSHA_EXPLOSIVE = ((TextBox)FormViewPhysChemical.FindControl("OSHA_EXPLOSIVETextBox")).Text;
            msds.OSHA_FLAMMABLE = ((TextBox)FormViewPhysChemical.FindControl("OSHA_FLAMMABLETextBox")).Text;
            msds.OSHA_HIGH_TOXIC = ((TextBox)FormViewPhysChemical.FindControl("OSHA_HIGH_TOXICTextBox")).Text;
            msds.OSHA_IRRITANT = ((TextBox)FormViewPhysChemical.FindControl("OSHA_IRRITANTTextBox")).Text;
            msds.OSHA_ORG_PEROX = ((TextBox)FormViewPhysChemical.FindControl("OSHA_ORG_PEROXTextBox")).Text;
            msds.OSHA_OTHERLONGTERM = ((TextBox)FormViewPhysChemical.FindControl("OSHA_OTHERLONGTERMTextBox")).Text;
            msds.OSHA_OXIDIZER = ((TextBox)FormViewPhysChemical.FindControl("OSHA_OXIDIZERTextBox")).Text;
            msds.OSHA_PYRO = ((TextBox)FormViewPhysChemical.FindControl("OSHA_PYROTextBox")).Text;
            msds.OSHA_SENSITIZER = ((TextBox)FormViewPhysChemical.FindControl("OSHA_SENSITIZERTextBox")).Text;
            msds.OSHA_TOXIC = ((TextBox)FormViewPhysChemical.FindControl("OSHA_TOXICTextBox")).Text;
            msds.OSHA_UNST_REACT = ((TextBox)FormViewPhysChemical.FindControl("OSHA_UNST_REACTTextBox")).Text;
            msds.OTHER_SHORT_TERM = ((TextBox)FormViewPhysChemical.FindControl("OTHER_SHORT_TERMTextBox")).Text;
            msds.PHYS_STATE_CODE = ((TextBox)FormViewPhysChemical.FindControl("PHYS_STATE_CODETextBox")).Text;
            msds.VOL_ORG_COMP_WT = ((TextBox)FormViewPhysChemical.FindControl("VOL_ORG_COMP_WTTextBox")).Text;
            msds.OSHA_WATER_REACTIVE = ((TextBox)FormViewPhysChemical.FindControl("OSHA_WATER_REACTIVETextBox")).Text;            
            

            //ingredients
            ingredientList = (List<MsdsIngredients>)ViewState["ingredientList"];

            MsdsIngredients ingredient = new MsdsIngredients();
            ingredient.INGREDIENT_NAME = ((TextBox)FormViewIngredients.FindControl("INGREDIENT_NAMETextBox")).Text;
            ingredient.PRCNT = ((TextBox)FormViewIngredients.FindControl("PRCNTTextBox")).Text;
            ingredient.CAS = ((TextBox)FormViewIngredients.FindControl("CAS_CODETextBox")).Text;
            ingredient.RTECS_NUM = ((TextBox)FormViewIngredients.FindControl("RTECS_NUMTextBox")).Text;
            ingredient.RTECS_CODE = ((TextBox)FormViewIngredients.FindControl("RTECS_CODETextBox")).Text;
            ingredient.OSHA_PEL = ((TextBox)FormViewIngredients.FindControl("OSHA_PELTextBox")).Text;
            ingredient.OSHA_STEL = ((TextBox)FormViewIngredients.FindControl("OSHA_STELTextBox")).Text;
            ingredient.ACGIH_TLV = ((TextBox)FormViewIngredients.FindControl("ACGIH_TLVTextBox")).Text;
            ingredient.ACGIH_STEL = ((TextBox)FormViewIngredients.FindControl("ACGIH_STELTextBox")).Text;
            ingredient.EPA_REPORT_QTY = ((TextBox)FormViewIngredients.FindControl("EPA_REPORT_QTYTextBox")).Text;
            ingredient.DOT_REPORT_QTY = ((TextBox)FormViewIngredients.FindControl("DOT_REPORT_QTYTextBox")).Text;
            ingredient.PRCNT_VOL_VALUE = ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_VALUETextBox")).Text;
            ingredient.PRCNT_VOL_WEIGHT = ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_WEIGHTTextBox")).Text;
            ingredient.CHEM_MFG_COMP_NAME = ((TextBox)FormViewIngredients.FindControl("CHEM_MFG_COMP_NAMETextBox")).Text;
            ingredient.ODS_IND = ((TextBox)FormViewIngredients.FindControl("ODS_INDTextBox")).Text;
            ingredient.OTHER_REC_LIMITS = ((TextBox)FormViewIngredients.FindControl("OTHER_REC_LIMITSTextBox")).Text;

            ingredientList.Add(ingredient);

            msds.ingredientsList = ingredientList;

            //contractor information
            contractorList = (List<MsdsContractors>)ViewState["contractorList"];

            MsdsContractors contractor = new MsdsContractors();
            contractor.CT_NUMBER = ((TextBox)FormViewContractorInfo.FindControl("CT_NUMBERTextBox")).Text;
            contractor.CT_CAGE = ((TextBox)FormViewContractorInfo.FindControl("CT_CAGETextBox")).Text;
            contractor.CT_CITY = ((TextBox)FormViewContractorInfo.FindControl("CT_CITYTextBox")).Text;
            contractor.CT_COMPANY_NAME = ((TextBox)FormViewContractorInfo.FindControl("CT_COMPANY_NAMETextBox")).Text;
            contractor.CT_COUNTRY = ((TextBox)FormViewContractorInfo.FindControl("CT_COUNTRYTextBox")).Text;
            contractor.CT_PO_BOX = ((TextBox)FormViewContractorInfo.FindControl("CT_PO_BOXTextBox")).Text;
            contractor.CT_PHONE = ((TextBox)FormViewContractorInfo.FindControl("CT_PHONETextBox")).Text;
            contractor.PURCHASE_ORDER_NO = ((TextBox)FormViewContractorInfo.FindControl("PURCHASE_ORDER_NOTextBox")).Text;
            contractor.CT_STATE = ((TextBox)FormViewContractorInfo.FindControl("CT_STATETextBox")).Text;
           
            contractorList.Add(contractor);

            msds.contractorsList = contractorList;


            //radiological info
            msds.NRC_LP_NUM = ((TextBox)FormViewRadiologicalInfo.FindControl("NRC_LP_NUMTextBox")).Text;
            msds.OPERATOR = ((TextBox)FormViewRadiologicalInfo.FindControl("OPERATORTextBox")).Text;
            msds.RAD_AMOUNT_MICRO = ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_AMOUNT_MICROTextBox")).Text;
            msds.RAD_FORM = ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_FORMTextBox")).Text;
            msds.RAD_CAS = ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_CASTextBox")).Text;
            msds.RAD_NAME = ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_NAMETextBox")).Text;
            msds.RAD_SYMBOL = ((TextBox)FormViewRadiologicalInfo.FindControl("RAD_SYMBOLTextBox")).Text;
            msds.REP_NSN = ((TextBox)FormViewRadiologicalInfo.FindControl("REP_NSNTextBox")).Text;
            msds.SEALED = ((TextBox)FormViewRadiologicalInfo.FindControl("SEALEDTextBox")).Text;

            //transportation
            msds.AF_MMAC_CODE = ((TextBox)FormViewTransportation.FindControl("AF_MMAC_CODETextBox")).Text;
            msds.CERTIFICATE_COE = ((TextBox)FormViewTransportation.FindControl("CERTIFICATE_COETextBox")).Text;
            msds.COMPETENT_CAA = ((TextBox)FormViewTransportation.FindControl("COMPETENT_CAATextBox")).Text;
            msds.DOD_ID_CODE = ((TextBox)FormViewTransportation.FindControl("DOD_ID_CODETextBox")).Text;
            msds.DOT_EXEMPTION_NO = ((TextBox)FormViewTransportation.FindControl("DOT_EXEMPTION_NOTextBox")).Text;
            msds.DOT_RQ_IND = ((TextBox)FormViewTransportation.FindControl("DOT_RQ_INDTextBox")).Text;
            msds.EX_NO = ((TextBox)FormViewTransportation.FindControl("EX_NOTextBox")).Text;
            msds.TRAN_FLASH_PT_TEMP = ((TextBox)FormViewTransportation.FindControl("TRAN_FLASH_PT_TEMPTextBox")).Text;
            msds.HIGH_EXPLOSIVE_WT = getInt(((TextBox)FormViewTransportation.FindControl("HIGH_EXPLOSIVE_WTTextBox")).Text);
            msds.LTD_QTY_IND = ((TextBox)FormViewTransportation.FindControl("LTD_QTY_INDTextBox")).Text;
            msds.MAGNETIC_IND = ((TextBox)FormViewTransportation.FindControl("MAGNETIC_INDTextBox")).Text;
            msds.MAGNETISM = ((TextBox)FormViewTransportation.FindControl("MAGNETISMTextBox")).Text;
            msds.MARINE_POLLUTANT_IND = ((TextBox)FormViewTransportation.FindControl("MARINE_POLLUTANT_INDTextBox")).Text;
            msds.NET_EXP_QTY_DIST = getInt(((TextBox)FormViewTransportation.FindControl("NET_EXP_QTY_DISTTextBox")).Text);
            msds.NET_EXP_WEIGHT = ((TextBox)FormViewTransportation.FindControl("NET_EXP_WEIGHTTextBox")).Text;
            msds.NET_PROPELLANT_WT = ((TextBox)FormViewTransportation.FindControl("NET_PROPELLANT_WTTextBox")).Text;
            msds.NOS_TECHNICAL_SHIPPING_NAME = ((TextBox)FormViewTransportation.FindControl("NOS_TECHNICAL_SHIPPING_NAMETextBox")).Text;
            msds.TRANSPORTATION_ADDITIONAL_DATA = ((TextBox)FormViewTransportation.FindControl("TRANSPORTATION_ADDITIONAL_DATATextBox")).Text;
            


            //dot psn
            msds.DOT_HAZARD_CLASS_DIV = ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_CLASS_DIVTextBox")).Text;
            msds.DOT_HAZARD_LABEL = ((TextBox)FormViewDOT.FindControl("DOT_HAZARD_LABELTextBox")).Text;
            msds.DOT_MAX_CARGO = ((TextBox)FormViewDOT.FindControl("DOT_MAX_CARGOTextBox")).Text;
            msds.DOT_MAX_PASSENGER = ((TextBox)FormViewDOT.FindControl("DOT_MAX_PASSENGERTextBox")).Text;
            msds.DOT_PACK_BULK = ((TextBox)FormViewDOT.FindControl("DOT_PACK_BULKTextBox")).Text;
            msds.DOT_PACK_EXCEPTIONS = ((TextBox)FormViewDOT.FindControl("DOT_PACK_EXCEPTIONSTextBox")).Text;
            msds.DOT_PACK_NONBULK = ((TextBox)FormViewDOT.FindControl("DOT_PACK_NONBULKTextBox")).Text;
            msds.DOT_PROP_SHIP_NAME = ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_NAMETextBox")).Text;
            msds.DOT_PROP_SHIP_MODIFIER = ((TextBox)FormViewDOT.FindControl("DOT_PROP_SHIP_MODIFIERTextBox")).Text;
            msds.DOT_PSN_CODE = ((TextBox)FormViewDOT.FindControl("DOT_PSN_CODETextBox")).Text;
            msds.DOT_SPECIAL_PROVISION = ((TextBox)FormViewDOT.FindControl("DOT_SPECIAL_PROVISIONTextBox")).Text;
            msds.DOT_SYMBOLS = ((TextBox)FormViewDOT.FindControl("DOT_SYMBOLSTextBox")).Text;
            msds.DOT_UN_ID_NUMBER = ((TextBox)FormViewDOT.FindControl("DOT_UN_ID_NUMBERTextBox")).Text;
            msds.DOT_WATER_OTHER_REQ = ((TextBox)FormViewDOT.FindControl("DOT_WATER_OTHER_REQTextBox")).Text;
            msds.DOT_WATER_VESSEL_STOW = ((TextBox)FormViewDOT.FindControl("DOT_WATER_VESSEL_STOWTextBox")).Text;
            msds.DOT_PACK_GROUP = ((TextBox)FormViewDOT.FindControl("DOT_PACK_GROUPTextBox")).Text;


            //afjm psn
            msds.AFJM_HAZARD_CLASS = ((TextBox)FormViewAFJM.FindControl("AFJM_HAZARD_CLASSTextBox")).Text;
            msds.AFJM_PACK_PARAGRAPH = ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_PARAGRAPHTextBox")).Text;
            msds.AFJM_PACK_GROUP = ((TextBox)FormViewAFJM.FindControl("AFJM_PACK_GROUPTextBox")).Text;
            msds.AFJM_PROP_SHIP_NAME = ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_NAMETextBox")).Text;
            msds.AFJM_PROP_SHIP_MODIFIER = ((TextBox)FormViewAFJM.FindControl("AFJM_PROP_SHIP_MODIFIERTextBox")).Text;
            msds.AFJM_PSN_CODE = ((TextBox)FormViewAFJM.FindControl("AFJM_PSN_CODETextBox")).Text;
            msds.AFJM_SPECIAL_PROV = ((TextBox)FormViewAFJM.FindControl("AFJM_SPECIAL_PROVTextBox")).Text;
            msds.AFJM_SUBSIDIARY_RISK = ((TextBox)FormViewAFJM.FindControl("AFJM_SUBSIDIARY_RISKTextBox")).Text;
            msds.AFJM_SYMBOLS = ((TextBox)FormViewAFJM.FindControl("AFJM_SYMBOLSTextBox")).Text;
            msds.AFJM_UN_ID_NUMBER = ((TextBox)FormViewAFJM.FindControl("AFJM_UN_ID_NUMBERTextBox")).Text;



            //iata psn
            msds.IATA_CARGO_PACKING = ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACKINGTextBox")).Text;
            msds.IATA_HAZARD_CLASS = ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_CLASSTextBox")).Text;
            msds.IATA_HAZARD_LABEL = ((TextBox)FormViewIATA.FindControl("IATA_HAZARD_LABELTextBox")).Text;
            msds.IATA_PACK_GROUP = ((TextBox)FormViewIATA.FindControl("IATA_PACK_GROUPTextBox")).Text;
            msds.IATA_PASS_AIR_PACK_LMT_INSTR = ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_INSTRTextBox")).Text;
            msds.IATA_PASS_AIR_PACK_LMT_PER_PKG = ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_LMT_PER_PKGTextBox")).Text;
            msds.IATA_PASS_AIR_PACK_NOTE = ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_PACK_NOTETextBox")).Text;
            msds.IATA_PROP_SHIP_NAME = ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_NAMETextBox")).Text;
            msds.IATA_PROP_SHIP_MODIFIER = ((TextBox)FormViewIATA.FindControl("IATA_PROP_SHIP_MODIFIERTextBox")).Text;
            msds.IATA_CARGO_PACK_MAX_QTY = ((TextBox)FormViewIATA.FindControl("IATA_CARGO_PACK_MAX_QTYTextBox")).Text;
            msds.IATA_PSN_CODE = ((TextBox)FormViewIATA.FindControl("IATA_PSN_CODETextBox")).Text;
            msds.IATA_PASS_AIR_MAX_QTY = ((TextBox)FormViewIATA.FindControl("IATA_PASS_AIR_MAX_QTYTextBox")).Text;
            msds.IATA_SPECIAL_PROV = ((TextBox)FormViewIATA.FindControl("IATA_SPECIAL_PROVTextBox")).Text;
            msds.IATA_SUBSIDIARY_RISK = ((TextBox)FormViewIATA.FindControl("IATA_SUBSIDIARY_RISKTextBox")).Text;
            msds.IATA_UN_ID_NUMBER = ((TextBox)FormViewIATA.FindControl("IATA_UN_ID_NUMBERTextBox")).Text;
                        
            

            //imo psn           
            msds.IMO_EMS_NO = ((TextBox)FormViewIMO.FindControl("IMO_EMS_NOTextBox")).Text;
            msds.IMO_HAZARD_CLASS = ((TextBox)FormViewIMO.FindControl("IMO_HAZARD_CLASSTextBox")).Text;
            msds.IMO_IBC_INSTR = ((TextBox)FormViewIMO.FindControl("IMO_IBC_INSTRTextBox")).Text;
            msds.IMO_LIMITED_QTY = ((TextBox)FormViewIMO.FindControl("IMO_LIMITED_QTYTextBox")).Text;
            msds.IMO_PACK_GROUP = ((TextBox)FormViewIMO.FindControl("IMO_PACK_GROUPTextBox")).Text;
            msds.IMO_PACK_INSTRUCTIONS = ((TextBox)FormViewIMO.FindControl("IMO_PACK_INSTRUCTIONSTextBox")).Text;
            msds.IMO_PACK_PROVISIONS = ((TextBox)FormViewIMO.FindControl("IMO_PACK_PROVISIONSTextBox")).Text;
            msds.IMO_PROP_SHIP_NAME = ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_NAMETextBox")).Text;
            msds.IMO_PROP_SHIP_MODIFIER = ((TextBox)FormViewIMO.FindControl("IMO_PROP_SHIP_MODIFIERTextBox")).Text;
            msds.IMO_PSN_CODE = ((TextBox)FormViewIMO.FindControl("IMO_PSN_CODETextBox")).Text;
            msds.IMO_SPECIAL_PROV = ((TextBox)FormViewIMO.FindControl("IMO_SPECIAL_PROVTextBox")).Text;
            msds.IMO_STOW_SEGR = ((TextBox)FormViewIMO.FindControl("IMO_STOW_SEGRTextBox")).Text;
            msds.IMO_SUBSIDIARY_RISK = ((TextBox)FormViewIMO.FindControl("IMO_SUBSIDIARY_RISKTextBox")).Text;
            msds.IMO_TANK_INSTR_IMO = ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_IMOTextBox")).Text;
            msds.IMO_TANK_INSTR_PROV = ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_PROVTextBox")).Text;
            msds.IMO_TANK_INSTR_UN = ((TextBox)FormViewIMO.FindControl("IMO_TANK_INSTR_UNTextBox")).Text;
            msds.IMO_UN_NUMBER = ((TextBox)FormViewIMO.FindControl("IMO_UN_NUMBERTextBox")).Text;
            msds.IMO_IBC_PROVISIONS = ((TextBox)FormViewIMO.FindControl("IMO_IBC_PROVISIONSTextBox")).Text;
           


            //item description
            msds.ITEM_MANAGER = ((TextBox)FormViewItemDescription.FindControl("ITEM_MANAGERTextBox")).Text;
            msds.ITEM_NAME = ((TextBox)FormViewItemDescription.FindControl("ITEM_NAMETextBox")).Text;
            msds.SPECIFICATION_NUMBER = ((TextBox)FormViewItemDescription.FindControl("SPECIFICATION_NUMBERTextBox")).Text;
            msds.TYPE_GRADE_CLASS = ((TextBox)FormViewItemDescription.FindControl("TYPE_GRADE_CLASSTextBox")).Text;
            msds.UNIT_OF_ISSUE = ((TextBox)FormViewItemDescription.FindControl("UNIT_OF_ISSUETextBox")).Text;
            msds.QUANTITATIVE_EXPRESSION = ((TextBox)FormViewItemDescription.FindControl("QUANTITATIVE_EXPRESSIONTextBox")).Text;
            msds.UI_CONTAINER_QTY = ((TextBox)FormViewItemDescription.FindControl("UI_CONTAINER_QTYTextBox")).Text;
            msds.TYPE_OF_CONTAINER = ((TextBox)FormViewItemDescription.FindControl("TYPE_OF_CONTAINERTextBox")).Text;
            msds.BATCH_NUMBER = ((TextBox)FormViewItemDescription.FindControl("BATCH_NUMBERTextBox")).Text;
            msds.LOT_NUMBER = ((TextBox)FormViewItemDescription.FindControl("LOT_NUMBERTextBox")).Text;
            msds.LOG_FLIS_NIIN_VER = ((TextBox)FormViewItemDescription.FindControl("LOG_FLIS_NIIN_VERTextBox")).Text;
            msds.LOG_FSC = getInt(((TextBox)FormViewItemDescription.FindControl("LOG_FSCTextBox")).Text);
            msds.NET_UNIT_WEIGHT = ((TextBox)FormViewItemDescription.FindControl("NET_UNIT_WEIGHTTextBox")).Text;
            msds.SHELF_LIFE_CODE = ((TextBox)FormViewItemDescription.FindControl("SHELF_LIFE_CODETextBox")).Text;
            msds.SPECIAL_EMP_CODE = ((TextBox)FormViewItemDescription.FindControl("SPECIAL_EMP_CODETextBox")).Text;
            msds.UN_NA_NUMBER = ((TextBox)FormViewItemDescription.FindControl("UN_NA_NUMBERTextBox")).Text;
            msds.UPC_GTIN = ((TextBox)FormViewItemDescription.FindControl("UPC_GTINTextBox")).Text;


            //label information
            msds.COMPANY_CAGE_RP = ((TextBox)FormViewLabelInfo.FindControl("COMPANY_CAGE_RPTextBox")).Text;
            msds.COMPANY_NAME_RP = ((TextBox)FormViewLabelInfo.FindControl("COMPANY_NAME_RPTextBox")).Text;
            msds.LABEL_EMERG_PHONE = ((TextBox)FormViewLabelInfo.FindControl("LABEL_EMERG_PHONETextBox")).Text;
            msds.LABEL_ITEM_NAME = ((TextBox)FormViewLabelInfo.FindControl("LABEL_ITEM_NAMETextBox")).Text;
            msds.LABEL_PROC_YEAR = ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROC_YEARTextBox")).Text;
            msds.LABEL_PROD_IDENT = ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_IDENTTextBox")).Text;
            msds.LABEL_PROD_SERIALNO = ((TextBox)FormViewLabelInfo.FindControl("LABEL_PROD_SERIALNOTextBox")).Text;
            msds.LABEL_SIGNAL_WORD = ((TextBox)FormViewLabelInfo.FindControl("LABEL_SIGNAL_WORDTextBox")).Text;
            msds.LABEL_STOCK_NO = ((TextBox)FormViewLabelInfo.FindControl("LABEL_STOCK_NOTextBox")).Text;
            msds.SPECIFIC_HAZARDS = ((TextBox)FormViewLabelInfo.FindControl("SPECIFIC_HAZARDSTextBox")).Text;


            //disposal info
            msds.DISPOSAL_ADD_INFO = ((TextBox)FormViewDisposal.FindControl("DISPOSAL_ADD_INFOTextBox")).Text;
            msds.EPA_HAZ_WASTE_CODE = ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_CODETextBox")).Text;
            msds.EPA_HAZ_WASTE_IND = ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_INDTextBox")).Text;
            msds.EPA_HAZ_WASTE_NAME = ((TextBox)FormViewDisposal.FindControl("EPA_HAZ_WASTE_NAMETextBox")).Text;

            //File Upload            
            try
            {
                
                // Get the existing msds record
                MsdsRecord original = manager.getMSDSRecordforSerialNumber(msds.MSDSSERNO);

                if (fileUploadMSDS.HasFile) {
                    msds.msds_file = fileUploadMSDS.FileBytes;
                    msds.file_name = fileUploadMSDS.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.msds_file = original.msds_file;
                    msds.file_name = original.file_name;
                }
                if (fileUploadTranslated.HasFile)
                {
                    msds.MSDS_TRANSLATED = fileUploadTranslated.FileBytes;
                    msds.msds_translated_filename = fileUploadTranslated.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.MSDS_TRANSLATED = original.MSDS_TRANSLATED;
                    msds.msds_translated_filename = original.msds_translated_filename;
                }
                if (fileUploadNeshap.HasFile)
                {
                    msds.NESHAP_COMP_CERT = fileUploadNeshap.FileBytes;
                    msds.neshap_comp_filename = fileUploadNeshap.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.NESHAP_COMP_CERT = original.NESHAP_COMP_CERT;
                    msds.neshap_comp_filename = original.neshap_comp_filename;
                }
                if (fileUploadOther.HasFile)
                {
                    msds.OTHER_DOCS= fileUploadOther.FileBytes;
                    msds.other_docs_filename = fileUploadOther.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.OTHER_DOCS = original.OTHER_DOCS;
                    msds.other_docs_filename = original.other_docs_filename;
                }
                if (fileUploadProdSheet.HasFile)
                {
                    msds.PRODUCT_SHEET = fileUploadProdSheet.FileBytes;
                    msds.product_sheet_filename = fileUploadProdSheet.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.PRODUCT_SHEET = original.PRODUCT_SHEET;
                    msds.product_sheet_filename = original.product_sheet_filename;
                }
                if (fileUploadTransport.HasFile)
                {
                    msds.TRANSPORTATION_CERT = fileUploadTransport.FileBytes;
                    msds.transportation_cert_filename = fileUploadTransport.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.TRANSPORTATION_CERT = original.TRANSPORTATION_CERT;
                    msds.transportation_cert_filename = original.transportation_cert_filename;
                }
                if (fileUploadLabel.HasFile)
                {
                    msds.MANUFACTURER_LABEL = fileUploadLabel.FileBytes;
                    msds.manufacturer_label_filename = fileUploadLabel.FileName;
                }
                else {
                    // Populate the new record with the existing file data
                    msds.MANUFACTURER_LABEL = original.MANUFACTURER_LABEL;
                    msds.manufacturer_label_filename = original.manufacturer_label_filename;
                }

            }
            catch
            {
            }


            manager.insertMSDS(msds, true);

            //clear fields
            FormViewMSDS.DataBind();
            FormViewIngredients.DataBind();
            FormViewItemDescription.DataBind();
            FormViewPhysChemical.DataBind();
            FormViewContractorInfo.DataBind();
            FormViewRadiologicalInfo.DataBind();
            FormViewTransportation.DataBind();
            FormViewDOT.DataBind();
            FormViewAFJM.DataBind();
            FormViewIATA.DataBind();
            FormViewIMO.DataBind();
            FormViewLabelInfo.DataBind();
            FormViewDisposal.DataBind();
            FormViewDocTypes.DataBind();
            ingredientList.Clear();
            contractorList.Clear();

            // Redirect to the MSDSGrid
            Response.Redirect("MSDSGrid.aspx");

        }
        [CoverageExclude]
        private int getInt(string s)
        {
            if (s == null || s.Equals(""))
                return 0;
            else
                return Int32.Parse(s);

        }
         [CoverageExclude]
        protected void btn_viewFile_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDoc("msds_file", "file_name");
        }
           [CoverageExclude]
        private void viewMsdsDoc(String dbBlobField, String dbFileNameField)
        {
            //View MSDS document
            byte[] file = (byte[])((DataRowView)FormViewMSDS.DataItem)[dbBlobField];

            string filename = ((DataRowView)FormViewMSDS.DataItem)[dbFileNameField].ToString();
            filename = filename.Replace(" ", "_");           

            Response.Buffer = true;
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();

            Response.AddHeader("Accept-Ranges", "bytes");
            Response.AddHeader("Accept-Header", file.Length.ToString());
            Response.AddHeader("Cache-Control", "public");
            Response.AddHeader("Cache-Control", "must-revalidate");
            Response.AddHeader("Pragma", "public");

            Response.ContentType = "application/octet-stream";
               
            Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(filename).Replace(' ', '_'));
            Response.AddHeader("Content-Length", file.Length.ToString());
           
            Response.AddHeader("expires", "0");
            Response.BinaryWrite(file);         
            Response.Flush();
            Response.End();

        }
           [CoverageExclude]
        private void viewMsdsDocType(String dbBlobField, String dbFileNameField)
        {
            //View MSDS document
            byte[] file = (byte[])((DataRowView)FormViewDocTypes.DataItem)[dbBlobField];

            string filename = ((DataRowView)FormViewDocTypes.DataItem)[dbFileNameField].ToString();
            filename = filename.Replace(" ", "_");

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename.Replace(' ', '_'));
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(file);
            Response.Flush();
            Response.End();
        }
           [CoverageExclude]
        protected void FormViewIngredients_PageIndexChanging(object sender, FormViewPageEventArgs e)
        {
            FormViewIngredients.PageIndex = e.NewPageIndex;            
            FormViewIngredients.DataBind();            
        }
           [CoverageExclude]
        protected void btn_addIngredient_Click(object sender, EventArgs e)
        {
            //Add new ingredient
            ingredientList = (List<MsdsIngredients>)ViewState["ingredientList"];

            MsdsIngredients ingredient = new MsdsIngredients();

            //ingredients
            ingredient.INGREDIENT_NAME = ((TextBox)FormViewIngredients.FindControl("INGREDIENT_NAMETextBox")).Text;
            ingredient.PRCNT = ((TextBox)FormViewIngredients.FindControl("PRCNTTextBox")).Text;
            ingredient.CAS = ((TextBox)FormViewIngredients.FindControl("CAS_CODETextBox")).Text;
            ingredient.RTECS_NUM = ((TextBox)FormViewIngredients.FindControl("RTECS_NUMTextBox")).Text;
            ingredient.RTECS_CODE = ((TextBox)FormViewIngredients.FindControl("RTECS_CODETextBox")).Text;
            ingredient.OSHA_PEL = ((TextBox)FormViewIngredients.FindControl("OSHA_PELTextBox")).Text;
            ingredient.OSHA_STEL = ((TextBox)FormViewIngredients.FindControl("OSHA_STELTextBox")).Text;
            ingredient.ACGIH_TLV = ((TextBox)FormViewIngredients.FindControl("ACGIH_TLVTextBox")).Text;
            ingredient.ACGIH_STEL = ((TextBox)FormViewIngredients.FindControl("ACGIH_STELTextBox")).Text;
            ingredient.EPA_REPORT_QTY = ((TextBox)FormViewIngredients.FindControl("EPA_REPORT_QTYTextBox")).Text;
            ingredient.DOT_REPORT_QTY = ((TextBox)FormViewIngredients.FindControl("DOT_REPORT_QTYTextBox")).Text;
            ingredient.PRCNT_VOL_VALUE = ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_VALUETextBox")).Text;
            ingredient.PRCNT_VOL_WEIGHT = ((TextBox)FormViewIngredients.FindControl("PRCNT_VOL_WEIGHTTextBox")).Text;
            ingredient.CHEM_MFG_COMP_NAME = ((TextBox)FormViewIngredients.FindControl("CHEM_MFG_COMP_NAMETextBox")).Text;
            ingredient.ODS_IND = ((TextBox)FormViewIngredients.FindControl("ODS_INDTextBox")).Text;
            ingredient.OTHER_REC_LIMITS = ((TextBox)FormViewIngredients.FindControl("OTHER_REC_LIMITSTextBox")).Text;
          

            ingredientList.Add(ingredient);

            //save ingredient list
            ViewState.Add("ingredientList", ingredientList);

            FormViewIngredients.DataBind(); //clear the fields

            GridView view = ((GridView)FormViewIngredients.FindControl("GridViewIngredients"));
            view.DataSource = ingredientList;
            view.DataBind();
        }
           [CoverageExclude]
        protected void btn_addContractor_Click(object sender, EventArgs e)
        {
            //Add new ingredient
            contractorList = (List<MsdsContractors>)ViewState["contractorList"];

            MsdsContractors contractor = new MsdsContractors();
            contractor.CT_NUMBER = ((TextBox)FormViewContractorInfo.FindControl("CT_NUMBERTextBox")).Text;
            contractor.CT_CAGE = ((TextBox)FormViewContractorInfo.FindControl("CT_CAGETextBox")).Text;
            contractor.CT_CITY = ((TextBox)FormViewContractorInfo.FindControl("CT_CITYTextBox")).Text;
            contractor.CT_COMPANY_NAME = ((TextBox)FormViewContractorInfo.FindControl("CT_COMPANY_NAMETextBox")).Text;
            contractor.CT_COUNTRY = ((TextBox)FormViewContractorInfo.FindControl("CT_COUNTRYTextBox")).Text;
            contractor.CT_PO_BOX = ((TextBox)FormViewContractorInfo.FindControl("CT_PO_BOXTextBox")).Text;
            contractor.CT_PHONE = ((TextBox)FormViewContractorInfo.FindControl("CT_PHONETextBox")).Text;
            contractor.PURCHASE_ORDER_NO = ((TextBox)FormViewContractorInfo.FindControl("PURCHASE_ORDER_NOTextBox")).Text;
            contractor.CT_STATE = ((TextBox)FormViewContractorInfo.FindControl("CT_STATETextBox")).Text;

            contractorList.Add(contractor);

            //save ingredient list
            ViewState.Add("contractorList", contractorList);

            FormViewContractorInfo.DataBind(); //clear the fields

            GridView view = ((GridView)FormViewContractorInfo.FindControl("GridViewContractors"));
            view.DataSource = contractorList;
            view.DataBind();
        }
           [CoverageExclude]
        protected void btn_excelExport_Click(object sender, EventArgs e)
        {
                MemoryStream zipStream = new MSDSExport().getZipFileExport();

                    string filename = "ExportedMSDS_Data.zip";
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename.Replace(' ', '_'));
                Response.ContentType = "application/unknown";
                Response.BinaryWrite(zipStream.ToArray());
                Response.Flush();
                Response.End();
           
        }
           [CoverageExclude]
        protected void btn_viewTranslated_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDocType("MSDS_TRANSLATED", "msds_translated_filename");
        }
           [CoverageExclude]
        protected void btn_viewNeshap_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDocType("NESHAP_COMP_CERT", "neshap_comp_filename");
        }
           [CoverageExclude]
        protected void btn_viewOther_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDocType("OTHER_DOCS", "other_docs_filename");
        }
           [CoverageExclude]
        protected void btn_viewProdSheet_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDocType("PRODUCT_SHEET", "product_sheet_filename");
        }
           [CoverageExclude]
        protected void btn_viewTransport_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDocType("TRANSPORTATION_CERT", "transportation_cert_filename");
        }
           [CoverageExclude]
        protected void FormViewContractorInfo_PageIndexChanging(object sender, FormViewPageEventArgs e)
        {
            FormViewContractorInfo.PageIndex = e.NewPageIndex;
            FormViewContractorInfo.DataBind();
        }
           [CoverageExclude]
        protected void btn_PRODUCT_LOAD_DATE_select_Click(object sender, EventArgs e)
        {
            CalendarPopup.Visible = true;
            fadePanel.Visible = true;

            makeDropDownVisible(!fadePanel.Visible);
        }
           [CoverageExclude]
        protected void cal_productDate_SelectionChanged(object sender, EventArgs e)
        {
            ((TextBox)FormViewMSDS.FindControl("PRODUCT_LOAD_DATETextBox")).Text = cal_productDate.SelectedDate.ToShortDateString();
            CalendarPopup.Visible = false;
            fadePanel.Visible = false;

            makeDropDownVisible(!fadePanel.Visible);
        }
           [CoverageExclude]
        protected void btn_viewLabel_Click(object sender, EventArgs e)
        {
            //View MSDS document
            viewMsdsDocType("MANUFACTURER_LABEL", "manufacturer_label_filename");
        }
           protected void btnFeedback_Click(object sender, ImageClickEventArgs e)
           {

               //save current page to the session for feedback
               Session.Add("feedbackPage", GetCurrentPageName());

               ClientScript.RegisterStartupScript(Page.GetType(), "",
  "window.open('FEEDBACK.aspx','Feedback','height=400,width=590, scrollbars=no');", true);


           }
           public string GetCurrentPageName()
           {
               string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
               System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
               string sRet = oInfo.Name;
               return sRet;
           }
           protected void btnHelp_Click(object sender, ImageClickEventArgs e)
           {

               ClientScript.RegisterStartupScript(Page.GetType(), "",
     "window.open('Help.html','Help','height=750,width=790, scrollbars=yes');", true);
           }
           public bool isAdmin()
           {
               string username = Page.User.Identity.Name;
               return new DatabaseManager().isAdmin(username);
           }
           public bool isSupplyOfficer()
           {
               string username = Page.User.Identity.Name;
               return new DatabaseManager().isSupplyOfficer(username);
           }
           public bool isSupplyUser()
           {
               string username = Page.User.Identity.Name;
               return new DatabaseManager().isSupplyUser(username);
           }
           public bool isWorkCenterUser()
           {
               string username = Page.User.Identity.Name;
               return new DatabaseManager().isWorkCenterUser(username);
           }

        protected string StrEval(string s)
        {
            var v = Eval(s);
            
            if (v == null)
                return "";

            return v.ToString();
        }
    }
}