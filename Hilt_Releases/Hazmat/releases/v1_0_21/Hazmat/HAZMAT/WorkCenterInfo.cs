﻿namespace HAZMAT
{
    public class WorkCenterInfo
    {
              
        private string description;

        public string Description
        {
            [CoverageExclude]
            get { return description; }
            [CoverageExclude]
            set { description = value; }
        }
       
       

    }
}