﻿using System;
using System.Collections.Generic;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace HAZMAT
{
    public class DD1348Generator
    {

        public string ShipFromDODAAC { get; set; }
        public string ShipToDODAAC { get; set; }

        public string SupAdd { get; set; }            

        public string DocDate { get; set; }

        public string ShipFromBoat { get; set; }
        public string ShipFromAddressLine1 { get; set; }
        public string ShipFromAddressLine2 { get; set; }

        public string ShipToAddressLine1 { get; set; }
        public string ShipToAddressLine2 { get; set; }
        public string ShipToAddressLine3 { get; set; }

        public string AddData { get; set; }

        public string templateFilePath {get; set;}

        public DD1348Generator()//Empty constructor for unit tests only.
        {

        }
        private string getJulian(DateTime? d)
        {
            string dayOfYear = d.Value.DayOfYear.ToString("000");
            string julian =  (d.Value.ToString("yy") + dayOfYear);
            julian = julian.Substring(1, 4);
            return julian;
        }
        public DD1348Generator(string templateFilePath)
        {
            this.templateFilePath = templateFilePath;
                      

            SupAdd = "";
        }

        private void buildDocumentNumber(DD1348Item itemData)
        {
            //UIC-DATE-HZ01
            try
            {
                string documentNumber = itemData.UIC + "-";

                string julianDate = getJulian(itemData.date_value);

                documentNumber += julianDate + "-HZ" + padText(itemData.serial_number.ToString(), 2, "0");

                itemData.DOCUMENT_NUMBER = documentNumber;
            }
            catch
            {
                itemData.DOCUMENT_NUMBER = null;
            }
        }


        public string generateSingleDD1348(DD1348Item itemData)
        {
            buildDocumentNumber(itemData);

            String file = templateFilePath;
            PdfReader.unethicalreading = true; 
            PdfReader reader = new PdfReader(file);

            String completedFilePath = Path.GetTempFileName();

            PdfStamper stamper = new PdfStamper(reader, new FileStream(
                        completedFilePath, FileMode.Create));
            AcroFields fields = stamper.AcroFields;
            setFields(fields, itemData);
            setBarcodes(stamper, itemData);

            // flatten form fields and close document
            stamper.FormFlattening = true;
            stamper.Close();

            return completedFilePath;
        }

        public string generateDD1348s(List<DD1348Item> itemDataList)
        {
            String destinationFile = Path.GetTempFileName();
            string[] sourceFiles = new string[itemDataList.Count];

            int i = 0;
            foreach(DD1348Item itemData in itemDataList)
            {
                buildDocumentNumber(itemData);

                String file = templateFilePath;

                PdfReader reader = new PdfReader(file);

                String completedFilePath = Path.GetTempFileName();

                PdfStamper stamper = new PdfStamper(reader, new FileStream(
                            completedFilePath, FileMode.Create));
                AcroFields fields = stamper.AcroFields;

                setFields(fields, itemData);

                setBarcodes(stamper, itemData);

                // flatten form fields and close document
                stamper.FormFlattening = true;
                stamper.FreeTextFlattening = true;
                stamper.Close();

                 sourceFiles[i] = completedFilePath;
                    i++;
            }

            //Create a single pdf
            MergeFiles(destinationFile, sourceFiles);
           
            return destinationFile;
        }
               

        private void setBarcodes(PdfStamper stamper, DD1348Item itemData)
        {

            PdfContentByte pdfContentByte = stamper.GetOverContent(1);           


            if (itemData.DOCUMENT_NUMBER != null && itemData.DOCUMENT_NUMBER != "")
            {
                //barcode 24. Document Number
                Barcode39 b = new Barcode39();
                b.Code = itemData.DOCUMENT_NUMBER.ToUpper() + " ";
                b.BarHeight = 35;
                Image imageDocNum = b.CreateImageWithBarcode(pdfContentByte, BaseColor.BLACK, BaseColor.WHITE);
                //placement for #24. barcode
                imageDocNum.SetAbsolutePosition(55, 642);
                imageDocNum.ScaleAbsoluteWidth(215f);
                pdfContentByte.AddImage(imageDocNum);
            }
            if (itemData.NSNa != null && itemData.NSNa != "")
            {
                //barcode 25. NSN
                Barcode39 b = new Barcode39();
                b.Code = itemData.NSNa + itemData.NSNb + " ";
                b.BarHeight = 35;
                Image imageNSN = b.CreateImageWithBarcode(pdfContentByte, BaseColor.BLACK, BaseColor.WHITE);
                //placement for #25. barcode
                imageNSN.SetAbsolutePosition(55, 568);
                imageNSN.ScaleAbsoluteWidth(215f);
                pdfContentByte.AddImage(imageNSN);
            }

            string RICUIQTY = getRICUIQTYBarcode(itemData, true);
            if (RICUIQTY != "")
            {
                //barcode 26. RICUIQTY
                Barcode39 b = new Barcode39();
                b.Code = RICUIQTY.ToUpper();
                b.BarHeight = 35;
                Image imageRIC = b.CreateImageWithBarcode(pdfContentByte, BaseColor.BLACK, BaseColor.WHITE);
                //placement for #26. barcode
                imageRIC.SetAbsolutePosition(80, 495);
                imageRIC.ScaleAbsoluteWidth(260f);
                pdfContentByte.AddImage(imageRIC);
            }

            if (itemData.PCN != null && itemData.PCN != "")
            {
                //barcode 26. PCN
                Barcode39 b = new Barcode39();
                b.Code = itemData.PCN.ToUpper();
                b.BarHeight = 35;
                Image imagePCN = b.CreateImageWithBarcode(pdfContentByte, BaseColor.BLACK, BaseColor.WHITE);
                //placement for PCN barcode
                imagePCN.SetAbsolutePosition(385, 503);
                imagePCN.ScaleAbsoluteWidth(140f);
                pdfContentByte.AddImage(imagePCN);
            }

            //PDF417
            BarcodePDF417 code417 = new BarcodePDF417();
            code417.SetText(getPDf417Value(itemData));
              
            Image image417 = code417.GetImage();
                
            //placement for 417 barcode
            image417.SetAbsolutePosition(284, 543);
            image417.RotationDegrees = 90f;
            image417.ScaleAbsoluteWidth(145f);
            image417.ScaleAbsoluteHeight(70f);
            pdfContentByte.AddImage(image417);
        }

        public string getPDf417Value(DD1348Item itemData)
        {
            char RS = (char)30;
            char GS = (char)29;
            char EOT = (char)4;

            string value = "";

            //section1
            value += "[)>"+RS+ "06"+GS;

            //Document Number
            if (!isEmpty(itemData.DOCUMENT_NUMBER))
                value += "12S" + itemData.DOCUMENT_NUMBER.ToUpper() + GS;

            //NSN
            if (!isEmpty(itemData.NSNb))
                value += "N" + itemData.NSNa + itemData.NSNb + GS;

            //Quantity and UI
            if (!isEmpty(itemData.UnitIss))
            value += "7Q" + itemData.Quant + itemData.UnitIss.ToUpper() + GS;

            //RI FROM
            if (!isEmpty(itemData.RI_FROM))
                value += "V" + itemData.RI_FROM.ToUpper() + GS;

            //COND
            if (!isEmpty(itemData.COND))
                value += "2R" + itemData.COND.ToUpper() + GS;

            //Unit Price
            float cents = getIntFromString(itemData.UNIT_CTS) * .01f;
            float price = getIntFromString(itemData.UNIT_DOLLARS);
            price += cents;
            string up = "" + price;
            if (price < 1.00)
                up = up.Substring(1);
            if (price == 0.00)
                up = ".00";
            value += "12Q" + up + "USD" + GS;

            //NMFC
            if (!isEmpty(itemData.NMFC))
                value += "5P" + itemData.NMFC.ToUpper();

            if (value.EndsWith("" + GS))
                value = value.Substring(0, value.Length - 1);

            //section 2
            value +=  RS + "07" + GS;

            //Project
            if (!isEmpty(itemData.PROJECT))
                value += "03" + itemData.PROJECT.ToUpper() + GS;

            //Distribution
            if (!isEmpty(itemData.DISTRIBUTION))
                value += "B6" + itemData.DISTRIBUTION.ToUpper() + GS;

            //ShipTo DODAAC
            if (!isEmpty(itemData.SHIP_TO))
                value += "27" + itemData.SHIP_TO.ToUpper() + GS;

            //Nomenclature
            if (!isEmpty(itemData.ITEM_NOM))
                value += "38" + itemData.ITEM_NOM.ToUpper() + GS;

            //Req'd Del Date
            if (!isEmpty(itemData.REQ_DEL_DATE))
                value += "32" + itemData.REQ_DEL_DATE.ToUpper() + GS;

            //PRI
            if (!isEmpty(itemData.PRI))
                value += "B7" + itemData.PRI.ToUpper() + GS;

            //Supplementary Address
            if (!isEmpty(itemData.SUPP_ADDRESS))
                value += "81" + itemData.SER.ToUpper() + itemData.SUPP_ADDRESS.ToUpper() + GS;

            //RI
            if (!isEmpty(itemData.RI))
                value += "7V" + itemData.RI.ToUpper();

            if (value.EndsWith(""+GS))
                value = value.Substring(0, value.Length - 1);

            //end
            value += RS +""+ EOT;


            return value;
        }

        private bool isEmpty(String s)
        {
            if (s != null && s != "")
                return false;
            else
                return true;
        }

        private int getIntFromString(String s)
        {
            int i = 0;

            if (s != null && s != "")
                i = Int32.Parse(s);

            return i;
        }

        private string padText(String original, int length, String paddingChar)
        {
            if (isEmpty(original))
                original = "";
            string padding = "";
            while (padding.Length+ original.Length < length)
                padding += paddingChar;
            original = padding + original;

            return original;
        }

        private string getRICUIQTYBarcode(DD1348Item itemData, bool isBarcode)
        {
            string value = "";

            string space = " ";
            if (isBarcode)//Difference in displayed text and actual barcode text
                space = "";

            //RIC            
            string ric = itemData.RI_FROM;
            ric = padText(ric, 3, " ");
            value += ric + space;
          
            //UI
            string ui = itemData.UnitIss;
            ui = padText(ui, 2, " ");
            value += ui + space;
            
            //QTY
            string qty = itemData.Quant;
            qty = padText(qty, 5, "0");
            value += qty + space;
            //itemData.Quant = qty;

            //COND CODE
            string cond = itemData.COND;
            cond = padText(cond, 1, " ");
            value += cond + space;

            //DIST
            string dist = itemData.DISTRIBUTION;
            dist = padText(dist, 2, " ");
            while (dist.Length > 2)
                dist = dist.Substring(1);
            value += dist + space;

            //UP
            string up = itemData.UNIT_DOLLARS;
            up = padText(up, 5, "0");
            string cts = itemData.UNIT_CTS;
            cts = padText(cts, 2, "0");

            itemData.UNIT_DOLLARS = up;
            itemData.UNIT_CTS = cts;

            value += up+cts;

            return value.ToUpper();
        }

        private void setFields(AcroFields fields, DD1348Item itemData)
        {            
            fields.SetField("RICUIQTY", "" + getRICUIQTYBarcode(itemData, false));

            fields.SetField("TPDollars", "" + itemData.TOTAL_DOLLARS);
            fields.SetField("TPCents", "" + itemData.TOTAL_CTS);
            fields.SetField("ShipFrom", "" + itemData.SHIP_FROM);
            fields.SetField("ShipTo", "" + itemData.SHIP_TO);
            fields.SetField("MarkFor", "" + itemData.MARK_FOR);
            fields.SetField("DocDate", "" + itemData.DOC_DATE);
            fields.SetField("NMFC", "" + itemData.NMFC);
            fields.SetField("TypeCargo", "" + itemData.TYPE_CARGO);
            fields.SetField("PS", "" + itemData.PS);
            fields.SetField("UP", "" + itemData.UP);
            fields.SetField("UnitWeight", "" + itemData.UNIT_WEIGHT);
            fields.SetField("UnitCube", "" + itemData.UNIT_CUBE);
            fields.SetField("UFC", "" + itemData.UFC);
            fields.SetField("SI", "" + itemData.SL);
            fields.SetField("FreightClassNomen", "" + itemData.FRGHT_CLASS_NOM);
            fields.SetField("ItemNomen", "" + itemData.ITEM_NOM);
            fields.SetField("DocNum", "" + itemData.DOCUMENT_NUMBER); //24 DOCUMENT NUMBER BARCODE AREA
            fields.SetField("NSNb", "" + itemData.NSNa + itemData.NSNb); //NIIN   NSN = FSC+NIIN (BARCODE AREA)
            fields.SetField("DocIden", "" + itemData.DOC_IDENT);
            fields.SetField("RIFrom", "" + itemData.RI_FROM);
            fields.SetField("MandS", "" + itemData.M_S);
            fields.SetField("UnitIss", "" + itemData.UnitIss); //UI
            fields.SetField("Quant", "" + padText(itemData.Quant, 5, "0")); //Quantity
            fields.SetField("SER", "" + itemData.SER);
            fields.SetField("SupAdd", "" + itemData.SUPP_ADDRESS);
            fields.SetField("Sig", "" + itemData.SIG);
            fields.SetField("Fund", "" + itemData.FUND);
            fields.SetField("Distribution", "" + itemData.DISTRIBUTION);
            fields.SetField("Project", "" + itemData.PROJECT);
            fields.SetField("Pri", "" + itemData.PRI);
            fields.SetField("RecdDelDate", "" + itemData.REQ_DEL_DATE);
            fields.SetField("Ri", "" + itemData.RI);
            fields.SetField("OP", "" + itemData.O_P);
            fields.SetField("Cond", "" + itemData.COND);
            fields.SetField("MGT", "" + itemData.MGT);
            fields.SetField("UPDollars", "" + itemData.UNIT_DOLLARS);
            fields.SetField("UPCents", "" + itemData.UNIT_CTS);
            fields.SetField("PCN", "" + itemData.PCN);

            //Additional data section:
            fields.SetField("PROJ", "PROJ:");
            fields.SetField("RECOCN", "REC OCN:");
            fields.SetField("JON", "JON: "+itemData.JON);
            fields.SetField("SPI", "SPI: " );
            fields.SetField("DMIL", "DMIL: " + itemData.DMIL);
            fields.SetField("HCC", "HCC: " + itemData.HCC);
            fields.SetField("CIIC", "CIIC: " + itemData.CIIC);
            fields.SetField("HCCMSG", "HCC MSG: " + itemData.HCC_MSG);
            fields.SetField("TYCARGOMSG", "TY CARGO MSG: " + itemData.TY_CARGO_MSG);
            fields.SetField("MFRDT", "MFRDT:  " + getDateFormatted(itemData.manufacturer_date));
            fields.SetField("EXPDT", "EXPDT:  " + getDateFormatted(itemData.expiration_date));
            fields.SetField("MSDS", "MSDS:  "+itemData.MSDS);
            fields.SetField("BIN", "BIN:");
            fields.SetField("DSG", "DSG:");
            fields.SetField("PCNtxt", "PCN: "+itemData.PCN);

        }

        private string getDateFormatted(String dateString)
        {
            string value = "";

            if(!isEmpty(dateString))
            {
                DateTime dt = DateTime.Parse(dateString);

               value = ""+ dt.Year + padText(""+dt.Month, 2, "0");

            }

            return value;
        }       

        public void MergeFiles(string destinationFile, string[] sourceFiles)
        {
            Rectangle pageDim = iTextSharp.text.PageSize.LETTER;
            pageDim = new Rectangle(pageDim.Width,pageDim.Height/2 - 3, 0, 800);

            try
            {
                int f = 0;
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(sourceFiles[f]);
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                Console.WriteLine("There are " + n + " pages in the original file.");
                // step 1: creation of a document-object
                Document document = new Document(pageDim, 10, 10, 42, 35);
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));
                // step 3: we open the document
                document.Open();
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;
                int rotation;
                // step 4: we add content
                while (f < sourceFiles.Length)
                {
                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(pageDim);
                        document.NewPage();
                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, pageDim.Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                        Console.WriteLine("Processed page " + i);
                    }
                    f++;
                    if (f < sourceFiles.Length)
                    {
                        reader = new PdfReader(sourceFiles[f]);
                        // we retrieve the total number of pages
                        n = reader.NumberOfPages;
                        Console.WriteLine("There are " + n + " pages in the original file.");
                    }
                }
                // step 5: we close the document
                document.Close();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }

        }
    
    }

    
}