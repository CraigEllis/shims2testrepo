﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="HAZMAT.Inventory" ClientIDMode="Static"
    MaintainScrollPositionOnPostback="true" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 43px;
        }

        .spaced {
            padding: 10px 40px 10px 40px;
        }
    </style>
    <script type="text/javascript" src="/resources/js/site.js"></script>
    <script type="text/javascript" src="/resources/js/MasterUpdate.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-header">
        <h1>Master Inventory Update</h1>
    </div>


    <div class="tableHeaderDiv">
        <div class="table-grid-headwrap clearfix">

            <table class="pull-left">
                <tr>
                    <td style="padding-left: 10px">
                        <label>
                            <b>Location</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterLocation" style="width: 200px"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>Work Center</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterWorkCenter" style="width: 200px"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <div class="form-inline">
                            <div class="form-group">
                               <button class="btn btn-primary" id="printAll" style="margin-left: 10px; margin-top: 17px;">
                                   Print Whole List
                               </button>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <div class="form-inline">
                            <div class="form-group">
                               <button class="btn btn-primary" id="printSelected" style="margin-left: 10px; margin-top: 17px;">
                                   Print Selected List
                               </button>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br />
        </div>
        <hr />
        <table id="masterUpdateTable" class="table" width="100%">
            <thead>
                <th>FSC</th>
                <th>NIIN</th>
                <th>WORK CENTER</th>
                <th>LOCATION</th>
                <th>ON-HAND QTY</th>
                <th>SHELF LIFE DATE</th>
                <th>INV DATE</th>
            </thead>
        </table>
        <hr />
        <br />
    </div>

</asp:Content>
