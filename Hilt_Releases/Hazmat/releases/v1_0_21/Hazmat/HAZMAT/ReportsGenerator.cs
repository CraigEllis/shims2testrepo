﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.IO;
using System.Net;
using iTextSharp.text;
using System.Net.Http.Headers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data;
using System.Text;
using iTextSharp.text.pdf;
using DataTable = System.Data.DataTable;
using System.Windows.Forms;
using System.Linq;
using System.Web.UI.WebControls;


namespace HAZMAT
{
    public class ReportsGenerator
    {
        private DatabaseManager dbManager;
        private BaseFont helveticaBoldItalic = BaseFont.CreateFont(BaseFont.HELVETICA_BOLDOBLIQUE, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        private BaseFont helveticaItalic = BaseFont.CreateFont(BaseFont.HELVETICA_OBLIQUE, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        private BaseFont helveticaBold = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        public static BaseFont helvetica = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        private iTextSharp.text.Font blueHelvetica = new iTextSharp.text.Font(helvetica, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLUE);

        public int currentPage;
        public int currentLine;
        public int numOfPages;
        public string shipName;
        private const int landscapeLine = 485;
        private const int portraitLine = 715;

        public ReportsGenerator()
        {
            this.dbManager = new DatabaseManager();
        }

        public HttpResponseMessage GenerateReportWithExtension(string extension)
        {
            var result = new HttpResponseMessage();
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();

            string fileName = naming.getRandomfileName(extension);
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;


            DataTable inventory = new DataTable();
            inventory = dbManager.getAllInventory();

            if (extension == "xlsx"){ result = ExportExcel(inventory, tempPath); }
            else if (extension == "html"){ result = ExportHtml(inventory, tempPath); }
           // else if (extension == "idc") { result = ExportIdc(inventory, tempPath); }
            else if (extension == "xml") { result = ExportXml(inventory, tempPath); }
            //else if (extension == "pdf") { result = ExportPdf(inventory, tempPath); }
            else if (extension == "rtfx") { result = ExportRtf(inventory, tempPath); }

            return result;
        }

        public HttpResponseMessage ExportExcel(DataTable inv, string tempPath)
        {
            FileInfo newFile = new FileInfo(tempPath);
            using (ExcelPackage pck = new ExcelPackage(newFile))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Inventory");
                ws.Cells["A1"].LoadFromDataTable(inv, true);
                ws.Cells.AutoFitColumns();

                var cells = ws.Cells["A1:T1"];
                cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cells.Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                cells.Style.Border.Bottom.Color.SetColor(Color.Black);

                cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                cells.Style.Border.Left.Color.SetColor(Color.Black);

                cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                cells.Style.Border.Right.Color.SetColor(Color.Black);

                pck.Workbook.Properties.Title = "Inventory";
                pck.Save();
            }

            return FillReport("xlsx", tempPath);
        }

        public HttpResponseMessage ExportHtml(DataTable inv, string tempPath)
        {
            string htmlText = "<HTML><HEAD>"+
                              "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html;charset=windows-1252\">"+
                              "<TITLE>ALL INVENTORY</TITLE>"+
                              "</HEAD>"+
                              "<BODY>"+
                              "<TABLE BORDER=1 BGCOLOR=#ffffff CELLSPACING=0><FONT FACE=\"Calibri\" COLOR=#000000><CAPTION><B>ALL INVENTORY</B></CAPTION></FONT>";
            htmlText += "<THEAD><TR>";

            foreach (DataColumn column in inv.Columns)
            {
                htmlText += "<TH BGCOLOR=#c0c0c0 BORDERCOLOR=#000000 ><FONT style=FONT-SIZE:11pt FACE=\"Calibri\" COLOR=#000000>";
                htmlText += column.ColumnName + "</FONT></TH>";
            }
            htmlText += "</TR></THEAD><TBODY>";

            foreach (DataRow item in inv.Rows)
            {
                htmlText += "<TR VALIGN=TOP>";
                foreach (DataColumn detail in inv.Columns)
                {
                    htmlText += "<TD BORDERCOLOR=#d0d7e5 ><FONT style=FONT-SIZE:11pt FACE=\"Calibri\""+
                             " COLOR=#000000>"+item[detail] +"</FONT></TD>";
                }

                htmlText += "</TR>";
            }

            htmlText += "</TBODY>"+
                        "<TFOOT></TFOOT></TABLE>" +
                        "</BODY>" +
                        "</HTML>";

            File.WriteAllText(tempPath, htmlText);

            return FillReport("html", tempPath);
        }

        public HttpResponseMessage ExportXml(DataTable inv, string tempPath)
        {
           string  xmlText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                             "<dataroot xmlns:od=\"urn:schemas-microsoft-com:officedata\""+
                             "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "+
                             "xsi:noNamespaceSchemaLocation=\"ALL%20INVENTORY.xsd\" generated=\""+DateTime.Now+"\">\n";

            foreach (DataRow item in inv.Rows)
            {
                xmlText += "<ALL_x0020_INVENTORY>\n";
                foreach (DataColumn detail in inv.Columns)
                {
                    xmlText += "<"+detail.ColumnName+">"+item[detail]+"</"+detail.ColumnName+">\n";
                }
                xmlText += "</ALL_x0020_INVENTORY>\n";
            }
            xmlText += "</dataroot>";

            File.WriteAllText(tempPath, xmlText);

            return FillReport("xml", tempPath);
        }

        /*public HttpResponseMessage ExportIdc(DataTable inv, string tempPath)
        {
            string idcText = "Datasource:\n"+
                             "Template:ALL INVENTORY.htx\n"+
                             "SQLStatement:SELECT"+ 
                                         "i.fsc AS FSC,"+ 
                                        "i.niin AS NIIN,"+
                                   "i.item_name AS ITEM_NAME,"+
								"m.manufacturer AS MFG,"+
                                        "m.cage AS CAGE,"+
									 "smcc.smcc	AS SMCC,"+ 
                                    "u.category AS ACM_CAT,"+
									   "hcc.hcc	AS HCC,"+
 "CONVERT(varchar(10), oh.expiration_date, 110) AS SHELF_LIFE_EXP_DATE,"+
                                        "oh.qty AS ON_HAND_QTY,"+
                                        "l.name AS LOCATION,"+
                                 "w.description AS WCENTER,"+
                          "i.allowance_quantity AS ALLOWANCE_QUANTITY,"+ 
                             "unit.abbreviation AS UNIT_OF_ISSUE,"+
                                          "i.um AS UNIT_OF_MEASURE,"+
			  "CONCAT(n.remarks,' - ', i.notes)	AS REMARKS,"+
									   "slc.slc	AS SHELF_LIFE_CODE,"+
									 "slac.slac	AS SHELF_LIFE_ACT_CODE,"+
"CONVERT(varchar(10), oh.inventoried_date, 110) AS INV_DATE,"+
    "CONVERT(varchar(10), oh.onboard_date, 110) AS ONBOARD_DATE"+

                    "FROM inventory_onhand oh LEFT JOIN inventory_detail i ON i.inventory_detail_id = oh.inventory_detail_id"+
                                             "LEFT JOIN mfg_catalog m ON m.mfg_catalog_id = oh.mfg_catalog_id" +
                                             "LEFT JOIN locations l ON l.location_id = oh.location_id" +
                                             "LEFT JOIN workcenter w ON w.workcenter_id = l.workcenter_id" +
                                             "LEFT JOIN usage_category u ON u.usage_category_id = i.usage_category_id" +
                                             "LEFT JOIN ui unit ON i.ui_id = unit.ui_id" +
                                             "LEFT JOIN smcc smcc ON i.smcc_id = smcc.smcc_id" +
                                             "LEFT JOIN shelf_life_code slc ON i.slc_id = slc.shelf_life_code_id" +
                                             "LEFT JOIN shelf_life_action_code slac ON i.slac_id = slac.shelf_life_action_code_id" +
                                             "LEFT JOIN hcc hcc	ON i.hcc_id = hcc.hcc_id" +
                                             "LEFT JOIN niin_catalog n ON i.niin = n.niin \n\n" +
                                             "Password:\n Username\n";

            File.WriteAllText(tempPath, idcText);

            return FillReport("idc", tempPath);
        }*/

        /*public HttpResponseMessage ExportPdf(DataTable inv, string tempPath)
        {
            Document document = new Document();
            bool fileExists = File.Exists(tempPath);
            if (!fileExists){PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempPath, FileMode.Create));}
            document.SetPageSize(PageSize.A4.Rotate());
            document.Open();

            iTextSharp.text.Font font5 = FontFactory.GetFont(FontFactory.HELVETICA, 5);
            PdfPTable table = new PdfPTable(inv.Columns.Count);
            PdfPRow row = null;
            PdfPCell cell = new PdfPCell(new Phrase("Inventory"));


            table.WidthPercentage = 100;
            cell.Colspan = inv.Columns.Count;

            foreach (DataColumn column in inv.Columns)
            {
                PdfPCell nameCell = new PdfPCell(new Phrase(column.ColumnName, font5));
                nameCell.BackgroundColor = new BaseColor(216, 216, 216);
                table.AddCell(nameCell);
            }

            foreach (DataRow item in inv.Rows)
            {
                foreach (DataColumn details in inv.Columns){ table.AddCell(new Phrase(item[details].ToString(), font5)); }
            } 
            document.Add(table);
            document.Close();

            return FillReport("pdf", tempPath);
        }*/

        public HttpResponseMessage ExportRtf(DataTable inv, string tempPath)
        {
            int numOfColumns = inv.Columns.Count;
            int totalWidth = 0;

            int[] twips = new int[numOfColumns];

            StringBuilder rtf = new StringBuilder();
            string richText = " ";
            rtf.Append(@"{\rtf1\ansi\deff0 "+"\r\n");//beginning of rtf      
            rtf.Append(@"{\colortbl ;\red0\green0\blue0;\red213\green213\blue213;} "+"\r\n");
            rtf.Append(@"\trowd\trgaph20\trql" + "\r\n");

            for (int i = 0; i < numOfColumns; i++)
            {
                string colName = inv.Columns[i].ColumnName;
                System.Drawing.Font stringFont = new System.Drawing.Font("Arial", 16);
                int stringWidth = TextRenderer.MeasureText(colName, stringFont).Width;

                int pixelToTwips = stringWidth*(1440/72);
                totalWidth += pixelToTwips;
                twips[i] = totalWidth;

                rtf.Append(@"\clbrdrt\brdrs\clbrdrl\brdrs\clbrdrb\brdrs\clbrdrr\brdrs\clcbpat2"+"\r\n\\cellx"+totalWidth+"\r\n");
            }
            
            foreach (DataColumn colName in inv.Columns){ rtf.Append("\\intbl "+ colName+"\\cell"+"\r\n");}
            rtf.Append(@"\row "+"\r\n");

            foreach (DataRow item in inv.Rows)
            {
                rtf.Append(@"\trowd\trgaph20\trql" + "\r\n");
                int totalRowWidth = 0;
                for (int i = 0; i < numOfColumns; i++)
                {
                    totalRowWidth += twips[i];
                    rtf.Append(@"\clbrdrt\brdrs\clbrdrl\brdrs\clbrdrb\brdrs\clbrdrr\brdrs"+"\r\n\\cellx" + twips[i]+"\r\n");
                }

                foreach (DataColumn detail in inv.Columns)
                {
                    rtf.Append("\\intbl "+ item[detail]+"\\cell"+"\r\n");
                }
                rtf.Append(@"\row "+"\r\n");
            }
            rtf.Append(@"}");

            richText = rtf.ToString();
            File.WriteAllText(tempPath, richText);

            return FillReport("rtf", tempPath);
        }

        public HttpResponseMessage FillReport(string ext, string path)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/"+ext);
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = "ALL INVENTORY."+ext };

            return result;
        }

        public string GenerateInventoryReport(string type, string sort, string signatures, DataTable sortedInv)
        {
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "HM Inventory by " + sort.ToUpper() + " - " + type.ToUpper();

            Document document = new Document();
            document.SetPageSize(PageSize.LETTER.Rotate());
            FileStream fs = new FileStream(tempPath,  FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            float itemCount = sortedInv.Rows.Count;
            numOfPages = (int)Math.Ceiling(itemCount/5);
            if (numOfPages < 1) { numOfPages = 1;}
            currentPage = 0;
            int currentRow = 1;

            document.Open();
            for (int i = 0; i < numOfPages; i++)
            {
                document.NewPage();
                currentPage += 1;
                currentLine = landscapeLine;
                PdfContentByte cb = writer.DirectContent;
                cb.SaveState();
                cb.SetColorFill(BaseColor.BLACK);

                GenerateLandscapeHeader(cb, reportTitle);              

                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 53, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 85, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CAGE", 135, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 200, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 400, 500, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ISSUE", 400, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 470, 500, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MEASURE", 470, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LOCATION", 540, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "WORK", 610, 500, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "CENTER", 610, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 660, 500, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 660, 490, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SHELF", 715, 500, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LIFE EXP", 715, 490, 0);
                cb.EndText();

                DrawLine(cb, 2.0f);

                currentLine -= 15;
                
                /************************
                 * Start Item Details   *
                 ************************/
                for (currentRow = currentRow; currentRow <= itemCount; currentRow++)
                {
                    //First row of an item's details.
                    cb.SetFontAndSize(helvetica, 8f);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            sortedInv.Rows[currentRow - 1].ItemArray[0].ToString(), 53, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            sortedInv.Rows[currentRow - 1].ItemArray[1].ToString(), 85, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            sortedInv.Rows[currentRow - 1].ItemArray[2].ToString(), 135, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            sortedInv.Rows[currentRow - 1].ItemArray[3].ToString(), 200, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            sortedInv.Rows[currentRow - 1].ItemArray[4].ToString(), 400, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            sortedInv.Rows[currentRow - 1].ItemArray[5].ToString(), 470, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            sortedInv.Rows[currentRow - 1].ItemArray[6].ToString(), 540, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            sortedInv.Rows[currentRow - 1].ItemArray[7].ToString(), 610, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            sortedInv.Rows[currentRow - 1].ItemArray[8].ToString(), 660, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            sortedInv.Rows[currentRow - 1].ItemArray[9].ToString(), 715, currentLine, 0);
                    cb.EndText();

                    currentLine -= 15;

                    //Second row of an item's details
                    cb.SetFontAndSize(helveticaBold, 8f);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SMCC:  ", 58, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "HCC:   ", 375, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SLC:   ", 590, currentLine, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string smcc = sortedInv.Rows[currentRow - 1].ItemArray[10] + " - " + sortedInv.Rows[currentRow - 1].ItemArray[11];
                    string[] styledSmcc = StyleLongStrings(cb, smcc, 60);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[0], 95, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[1], 93, currentLine - 10, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[2], 93, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string hcc = sortedInv.Rows[currentRow - 1].ItemArray[12] + " - " + sortedInv.Rows[currentRow - 1].ItemArray[13];
                    string[] styledHcc = StyleLongStrings(cb, hcc, 40);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[0], 405, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[1], 403, currentLine - 10, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[2], 403, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string slc = sortedInv.Rows[currentRow - 1].ItemArray[14].ToString();
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, slc, 620, currentLine, 0);
                    cb.EndText();

                    currentLine -= 25;

                    //Third row of an item's details
                    cb.SetFontAndSize(helveticaBold, 8f);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "MANUFACTURER:  ", 58, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NOTES:   ", 375, currentLine, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string man = sortedInv.Rows[currentRow - 1].ItemArray[15].ToString();
                    string[] styledMan = StyleLongStrings(cb, man, 45);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[0], 140, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[1], 135, currentLine - 10, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[2], 135, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string notes = sortedInv.Rows[currentRow - 1].ItemArray[16].ToString();
                    string[] styledNotes = StyleLongStrings(cb, notes, 45);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[0], 415, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[1], 413, currentLine - 8, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[2], 413, currentLine - 16, 0);
                    cb.EndText();

                    currentLine -= 22;

                    DrawLine(cb, 1.0f);

                    currentLine -= 16;

                    if (currentRow % 5 == 0)
                    {
                        currentRow += 1;
                        //currentPage += 1;
                        currentLine = landscapeLine;
                        break;
                    }
                }
                /********************
                 * End Item Details *
                 ********************/

                GenerateLandscapeFooter(cb, signatures);
                cb.RestoreState();
            }
            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        public string GenerateOffloadArchivedReport(DataTable archivedItems)
        {
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "HM Offload Archive List - All";

            Document document = new Document();
            document.SetPageSize(PageSize.LETTER.Rotate());
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            numOfPages = (int)Math.Ceiling((float)archivedItems.Rows.Count / 25);
            if (numOfPages < 1) { numOfPages = 1; }
            currentPage = 1;

            document.Open();
            document.NewPage();

            PdfContentByte cb = writer.DirectContent;
            cb.SaveState();
            cb.SetColorFill(BaseColor.BLACK);

            currentLine = landscapeLine;
            GenerateLandscapeHeader(cb, reportTitle);

            cb.SetFontAndSize(helveticaBold, 10f);
            cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "DOCUMENT #", 53, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 160, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 190, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CAGE", 240, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 280, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 535, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ISSUE", 535, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 585, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MEASURE", 585, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "OFFLOAD", 640, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 640, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SHELF", 705, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LIFE EXP", 705, 490, 0);
            cb.EndText();

            DrawLine(cb, 2.0f);

            int itemNum = 0;

            foreach (DataRow item in archivedItems.Rows)
            {
                itemNum += 1;
                currentLine -= 15;
                cb.SetFontAndSize(helvetica, 8f);

                cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.ItemArray[1].ToString(), 53, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.ItemArray[9].ToString(), 160, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.ItemArray[8].ToString(), 190, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.ItemArray[4].ToString(), 240, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, item.ItemArray[7].ToString(), 280, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, item.ItemArray[5].ToString(), 535, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, item.ItemArray[6].ToString(), 590, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, item.ItemArray[2].ToString(), 640, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, item.ItemArray[3].ToString(), 705, currentLine, 0);
                cb.EndText();

                if (itemNum >= 25)
                {
                    GenerateLandscapeFooter(cb, " ");

                    currentLine = landscapeLine;
                    itemNum = 0;

                    cb.RestoreState();
                    document.NewPage();
                    currentPage += 1;
                    cb.SaveState();

                    GenerateLandscapeHeader(cb, reportTitle);

                    cb.SetFontAndSize(helveticaBold, 10f);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "DOCUMENT #", 53, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 160, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 190, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CAGE", 240, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 280, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 535, 500, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ISSUE", 535, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 585, 500, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MEASURE", 585, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "OFFLOAD", 640, 500, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 640, 490, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SHELF", 705, 500, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LIFE EXP", 705, 490, 0);
                    cb.EndText();

                    cb.SetColorStroke(new BaseColor(0, 0, 0));
                    cb.SetLineWidth(2.0f);
                    cb.MoveTo(48, 485);
                    cb.LineTo(744, 485);
                    cb.Stroke();
                }
            }

            GenerateLandscapeFooter(cb, " ");

            cb.RestoreState();
            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        public string GenerateAcmReport(string type, string sort, string signatures, DataTable sortedAcm)
        {
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "ACM Inventory: " + type.ToUpper();

            Document document = new Document();
            document.SetPageSize(PageSize.LETTER.Rotate());
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            float itemCount = sortedAcm.Rows.Count;
            numOfPages = (int)Math.Ceiling(itemCount / 5);
            if (numOfPages < 1) { numOfPages = 1; }
            currentPage = 0;
            int currentRow = 1;

            document.Open();
            for (int i = 0; i < numOfPages; i++)
            {
                document.NewPage();
                currentPage += 1;
                currentLine = landscapeLine;
                PdfContentByte cb = writer.DirectContent;
                cb.SaveState();
                cb.SetColorFill(BaseColor.BLACK);

                GenerateLandscapeHeader(cb, reportTitle);

                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ACM", 53, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 85, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 125, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CAGE", 175, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 220, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 400, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ISSUE", 400, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LOCATION", 460, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "WORK", 530, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "CENTER", 530, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ALLOWANCE", 595, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 595, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 660, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 660, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ONBOARD", 715, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "DATE", 715, 490, 0);
                cb.EndText();

                DrawLine(cb, 2.0f);

                currentLine -= 15;

                /************************
                 * Start Item Details   *
                 ************************/
                for (currentRow = currentRow; currentRow <= itemCount; currentRow++)
                {
                    //First row of an item's details.
                    cb.SetFontAndSize(helvetica, 8f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedAcm.Rows[currentRow - 1].ItemArray[0].ToString(), 58, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedAcm.Rows[currentRow - 1].ItemArray[1].ToString(), 85, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedAcm.Rows[currentRow - 1].ItemArray[2].ToString(), 125, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedAcm.Rows[currentRow - 1].ItemArray[3].ToString(), 175, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedAcm.Rows[currentRow - 1].ItemArray[4].ToString(), 220, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedAcm.Rows[currentRow - 1].ItemArray[5].ToString(), 400, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedAcm.Rows[currentRow - 1].ItemArray[6].ToString(), 460, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedAcm.Rows[currentRow - 1].ItemArray[7].ToString(), 530, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedAcm.Rows[currentRow - 1].ItemArray[8].ToString(), 595, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedAcm.Rows[currentRow - 1].ItemArray[9].ToString(), 660, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedAcm.Rows[currentRow - 1].ItemArray[10].ToString(), 715, currentLine, 0);
                    cb.EndText();

                    currentLine -= 15;

                    //Second row of an item's details
                    cb.SetFontAndSize(helveticaBold, 8f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SMCC:  ", 58, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "HCC:   ", 375, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SLC:   ", 590, currentLine, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string smcc = sortedAcm.Rows[currentRow - 1].ItemArray[11] + " - " + sortedAcm.Rows[currentRow - 1].ItemArray[12];
                    string[] styledSmcc = StyleLongStrings(cb, smcc, 60);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[0], 95, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[1], 93, currentLine - 10, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[2], 93, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string hcc = sortedAcm.Rows[currentRow - 1].ItemArray[13] + " - " + sortedAcm.Rows[currentRow - 1].ItemArray[14];
                    string[] styledHcc = StyleLongStrings(cb, hcc, 40);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[0], 405, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[1], 403, currentLine - 10, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[2], 403, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string slc = sortedAcm.Rows[currentRow - 1].ItemArray[15].ToString();
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, slc, 620, currentLine, 0);
                    cb.EndText();

                    currentLine -= 25;

                    //Third row of an item's details
                    cb.SetFontAndSize(helveticaBold, 8f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "MANUFACTURER:  ", 58, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NOTES:   ", 375, currentLine, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string man = sortedAcm.Rows[currentRow - 1].ItemArray[16].ToString();
                    string[] styledMan = StyleLongStrings(cb, man, 45);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[0], 140, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[1], 135, currentLine - 10, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[2], 135, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string notes = sortedAcm.Rows[currentRow - 1].ItemArray[17].ToString();
                    string[] styledNotes = StyleLongStrings(cb, notes, 45);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[0], 415, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[1], 413, currentLine - 8, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[2], 413, currentLine - 16, 0);
                    cb.EndText();

                    currentLine -= 22;

                    DrawLine(cb, 1.0f);

                    currentLine -= 16;

                    if (currentRow % 5 == 0)
                    {
                        currentRow += 1;
                        //currentPage += 1;
                        currentLine = landscapeLine;
                        break;
                    }
                }
                /********************
                 * End Item Details *
                 ********************/

                GenerateLandscapeFooter(cb, signatures);
                cb.RestoreState();
            }
            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        public string GenerateExpirationReport(string type, string sort, string signatures, DataTable sortedDate)
        {
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "HM Inventory Shelf Life by " + sort.ToUpper() + " - All";

            Document document = new Document();
            document.SetPageSize(PageSize.LETTER.Rotate());
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            float itemCount = sortedDate.Rows.Count;
            numOfPages = (int)Math.Ceiling(itemCount / 5);
            if (numOfPages < 1) { numOfPages = 1; }
            currentPage = 0;
            int currentRow = 1;

            document.Open();
            for (int i = 0; i < numOfPages; i++)
            {
                document.NewPage();
                currentPage += 1;
                currentLine = landscapeLine;
                PdfContentByte cb = writer.DirectContent;
                cb.SaveState();
                cb.SetColorFill(BaseColor.BLACK);

                GenerateLandscapeHeader(cb, reportTitle);

                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 53, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 85, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CAGE", 135, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 175, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 407, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ISSUE", 407, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "UNIT OF", 457, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "MEASURE", 457, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "WORK", 507, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "CENTER", 507, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LOCATION", 567, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 625, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 625, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SHELF", 675, 500, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "LIFE EXP", 675, 490, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SLAC", 720, 490, 0);
                cb.EndText();

                DrawLine(cb, 2.0f);

                currentLine -= 15;

                /************************
                 * Start Item Details   *
                 ************************/
                for (currentRow = currentRow; currentRow <= itemCount; currentRow++)
                {
                    //First row of an item's details.
                    cb.SetFontAndSize(helvetica, 8f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedDate.Rows[currentRow - 1].ItemArray[0].ToString(), 53, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedDate.Rows[currentRow - 1].ItemArray[1].ToString(), 85, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedDate.Rows[currentRow - 1].ItemArray[2].ToString(), 135, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                        sortedDate.Rows[currentRow - 1].ItemArray[3].ToString(), 175, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[4].ToString(), 407, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[5].ToString(), 457, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[6].ToString(), 507, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[7].ToString(), 567, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[8].ToString(), 625, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[9].ToString(), 675, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                        sortedDate.Rows[currentRow - 1].ItemArray[10].ToString(), 720, currentLine, 0);
                    cb.EndText();

                    currentLine -= 15;

                    //Second row of an item's details
                    cb.SetFontAndSize(helveticaBold, 8f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SMCC:  ", 58, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "HCC:   ", 375, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "SLC:   ", 590, currentLine, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string smcc = sortedDate.Rows[currentRow - 1].ItemArray[11] + " - " + sortedDate.Rows[currentRow - 1].ItemArray[12];
                    string[] styledSmcc = StyleLongStrings(cb, smcc, 60);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[0], 95, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[1], 93, currentLine - 10, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledSmcc[2], 93, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string hcc = sortedDate.Rows[currentRow - 1].ItemArray[13] + " - " + sortedDate.Rows[currentRow - 1].ItemArray[14];
                    string[] styledHcc = StyleLongStrings(cb, hcc, 40);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[0], 405, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[1], 403, currentLine - 10, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledHcc[2], 403, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string slc = sortedDate.Rows[currentRow - 1].ItemArray[15].ToString();
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, slc, 620, currentLine, 0);
                    cb.EndText();

                    currentLine -= 25;

                    //Third row of an item's details
                    cb.SetFontAndSize(helveticaBold, 8f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "MANUFACTURER:  ", 58, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NOTES:   ", 375, currentLine, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string man = sortedDate.Rows[currentRow - 1].ItemArray[16].ToString();
                    string[] styledMan = StyleLongStrings(cb, man, 45);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[0], 140, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[1], 135, currentLine - 10, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledMan[2], 135, currentLine - 20, 0);
                    cb.EndText();

                    cb.SetFontAndSize(helvetica, 8f);
                    string notes = sortedDate.Rows[currentRow - 1].ItemArray[17].ToString();
                    string[] styledNotes = StyleLongStrings(cb, notes, 45);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[0], 415, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[1], 413, currentLine - 8, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, styledNotes[2], 413, currentLine - 16, 0);
                    cb.EndText();

                    currentLine -= 22;

                    DrawLine(cb, 1.0f);

                    currentLine -= 16;

                    if (currentRow % 5 == 0)
                    {
                        currentRow += 1;
                        //currentPage += 1;
                        currentLine = landscapeLine;
                        break;
                    }
                }
                /********************
                 * End Item Details *
                 ********************/

                GenerateLandscapeFooter(cb, signatures);
                cb.RestoreState();
            }
            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        public string GenerateStowageReport(string type, string sort, string signatures, List<System.Web.UI.WebControls.ListItem> hws)
        {
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "HAZMAT Stowage by "+sort.ToUpper();

            Document document = new Document();
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            float itemCount = hws.Count;
            numOfPages = (int)Math.Ceiling(itemCount / 50);
            if (numOfPages < 1) { numOfPages = 1; }
            currentPage = 0;
            int currentRow = 1;

            string leftHeader = " ";
            string rightHeader = " ";
            if (sort == "workcenter"){ leftHeader = "WORKCENTER"; rightHeader = "LOCATION"; }
            else { leftHeader = "LOCATION"; rightHeader = "WORKCENTER"; }

            document.Open();
            document.NewPage();
            currentPage += 1;
            currentLine = portraitLine;
            PdfContentByte cb = writer.DirectContent;
            cb.SetColorFill(BaseColor.BLACK);
            GeneratePortraitHeader(cb, reportTitle);
            GeneratePortraitFooter(cb, signatures);

            cb.SetFontAndSize(helveticaBold, 15f);
            cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, leftHeader, 246, currentLine, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, rightHeader, 256, currentLine, 0);
            cb.EndText();

            currentLine -= 10;

            DrawPortraitLine(cb, 1.0f);

            cb.SetColorStroke(new BaseColor(0, 0, 0));
            cb.SetLineWidth(1.0f);
            cb.MoveTo(251, currentLine);
            cb.LineTo(251, 200);
            cb.Stroke();

            currentLine -= 10;

            int count = 0;

            foreach (System.Web.UI.WebControls.ListItem id in hws)
            {
                DataTable hwsTable = new DataTable();
                long longId = Convert.ToInt64(id.ToString());
                if(sort == "workcenter"){ hwsTable = dbManager.getLocationsByWorkcenterId(longId); }
                else { hwsTable = dbManager.getWorkcenterByLocationId(longId); }

                cb.SetFontAndSize(helvetica, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, id.ToString(), 150, currentLine, 0);
                cb.EndText();

                int currentWorkCenterCount = 0;
                foreach (DataRow rightData in hwsTable.Rows)
                {
                    currentWorkCenterCount += 1;
                    int dataPlacement = 260;

                        cb.SetFontAndSize(helvetica, 10f);
                        cb.BeginText();
                        if(sort == "workcenter")
                        {
                            if(currentWorkCenterCount == 1){
                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, rightData.ItemArray[0].ToString(), 
                                    dataPlacement-85, currentLine, 0);
                            }
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, rightData.ItemArray[1].ToString(), dataPlacement, currentLine, 0);
                        }
                        else
                        {
                            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, rightData.ItemArray[0].ToString(),
                                dataPlacement-15, currentLine, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, rightData.ItemArray[1].ToString(),
                                dataPlacement, currentLine, 0);
                        }
                            
                        cb.EndText();
                    
                    currentLine -= 10;

                    cb.SaveState();
                    cb.RestoreState();
                    count += 1;

                    if (count >= 50)
                    {
                        count = 0;
                        document.NewPage();
                        currentPage += 1;
                        currentLine = portraitLine;
                        GeneratePortraitHeader(cb, reportTitle);
                        GeneratePortraitFooter(cb, signatures);

                        cb.SetFontAndSize(helveticaBold, 15f);
                        cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, leftHeader, 246, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, rightHeader, 256, currentLine, 0);
                        cb.EndText();

                        currentLine -= 10;

                        DrawPortraitLine(cb, 1.0f);

                        cb.SetColorStroke(new BaseColor(0, 0, 0));
                        cb.SetLineWidth(1.0f);
                        cb.MoveTo(251, currentLine);
                        cb.LineTo(251, 200);
                        cb.Stroke();

                        currentLine -= 10;
                    }
                }

                cb.SaveState();
                cb.RestoreState();          

                if (count >= 50)
                {
                    count = 0;
                    document.NewPage();
                    currentPage += 1;
                    currentLine = portraitLine;
                    GeneratePortraitHeader(cb, reportTitle);
                    GeneratePortraitFooter(cb, signatures);

                    cb.SetFontAndSize(helveticaBold, 15f);
                    cb.BeginText();
                    cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, leftHeader, 246, currentLine, 0);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, rightHeader, 256, currentLine, 0);
                    cb.EndText();

                    currentLine -= 10;

                    DrawPortraitLine(cb, 1.0f);

                    cb.SetColorStroke(new BaseColor(0, 0, 0));
                    cb.SetLineWidth(1.0f);
                    cb.MoveTo(251, currentLine);
                    cb.LineTo(251, 200);
                    cb.Stroke();

                    currentLine -= 10;
                }
            }
            
            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        public string GenerateIncompatiblesReport(string type, string sort){
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "HM HCC Incompatible List - " + sort.ToUpper();

            Document document = new Document();
            document.SetPageSize(PageSize.LETTER.Rotate());
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            writer.PageEvent = new IncompatiblesHelper();
            string whereStatement = " ";
            if (type == "specLocation")
            {
               whereStatement = "AND l.location_id = "+sort;
            }
            if(type == "specWorkcenter")
            {
                whereStatement = "AND w.wid = '" + sort+"'";
            }

            DataTable locations = new DataTable();
            locations = dbManager.getIncompatibleLocations(whereStatement);

            int itemCount = 0;

            document.Open();

            PdfContentByte cb = writer.DirectContent;
            cb.SetColorFill(BaseColor.BLACK);

            cb.SaveState(); // save 1

            foreach(DataRow location in locations.Rows)
            {
                cb.RestoreState(); //r1
                itemCount = 0;
                currentPage += 1;
                if (currentPage > 1) { document.NewPage(); }
                currentLine = landscapeLine - 40;

                cb.SetColorFill(BaseColor.BLACK);

                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 53, currentLine + 5, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 100, currentLine + 5, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 320, currentLine + 15, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 320, currentLine + 5, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "HCC", 360, currentLine + 5, 0);

                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 380, currentLine + 5, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 430, currentLine + 5, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 640, currentLine + 15, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 640, currentLine + 5, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "HCC", 680, currentLine + 5, 0);

                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ACTION", 720, currentLine + 15, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "CODE", 720, currentLine + 5, 0);
                cb.EndText();

                DrawLine(cb, 2.0f);

                //cb.SetLineWidth(1.0f);
                //cb.MoveTo(375, currentLine);
                //cb.LineTo(375, 75);
                //cb.Stroke();

                //cb.SetColorStroke(new BaseColor(0, 0, 255));
                //cb.SetLineWidth(1.0f);
                //cb.MoveTo(695, currentLine);
                //cb.LineTo(695, 75);
                //cb.Stroke();
                //cb.SetColorStroke(new BaseColor(0, 0, 0));

                currentLine -= 20;

                long locationId = Convert.ToInt64(location.ItemArray[0]);
                DataTable workcenter = new DataTable();            
                workcenter = dbManager.getWorkcenterByLocationId(locationId);

                cb.SetFontAndSize(helveticaItalic, 15f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                    "WORKCENTER: " + workcenter.Rows[0].ItemArray[1].ToString(), 53, currentLine, 0);
                cb.EndText();

                currentLine -= 15;

                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                    "LOCATION: "+dbManager.getLocationName(locationId.ToString()), 53, currentLine, 0);
                cb.EndText();

                itemCount += 3;
                currentLine -= 20;

                DataTable items = new DataTable();
                items = dbManager.getItemsWithHcc(Convert.ToInt64(location.ItemArray[0]));

                foreach (DataRow item in items.Rows)
                {
                    DataTable incompatibles = new DataTable();
                    incompatibles = dbManager.getIncompatiblesForReport(Convert.ToInt64(item.ItemArray[0]), 
                        Convert.ToInt64(location.ItemArray[0]));

                    foreach (DataRow incompatible in incompatibles.Rows)                   
                    {
                        if(itemCount >= 25){
                            document.NewPage();
                            itemCount += 0;
                            currentPage += 1;

                            currentLine = landscapeLine - 40;

                            cb.SetColorFill(BaseColor.BLACK);

                            cb.SetFontAndSize(helveticaBold, 10f);
                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 53, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 100, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 320, currentLine + 15, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 320, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "HCC", 360, currentLine + 5, 0);

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 380, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 430, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 640, currentLine + 15, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 640, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "HCC", 680, currentLine + 5, 0);

                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ACTION", 720, currentLine + 15, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "CODE", 720, currentLine + 5, 0);
                            cb.EndText();

                            DrawLine(cb, 2.0f);

                            cb.SetLineWidth(1.0f);
                            cb.MoveTo(375, currentLine + 30);
                            cb.LineTo(375, 75);
                            cb.Stroke();

                            cb.SetColorStroke(new BaseColor(0, 0, 255));
                            cb.SetLineWidth(1.0f);
                            cb.MoveTo(695, currentLine + 30);
                            cb.LineTo(695, 75);
                            cb.Stroke();
                            cb.SetColorStroke(new BaseColor(0, 0, 0));

                            currentLine -= 20;
                        }
                        
                        //item details
                        cb.SetFontAndSize(helvetica, 8f);
                        cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            incompatible.ItemArray[0].ToString(), 53, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            incompatible.ItemArray[1].ToString(), 100, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            incompatible.ItemArray[2].ToString(), 320, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            incompatible.ItemArray[3].ToString(), 360, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            incompatible.ItemArray[4].ToString(), 380, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            incompatible.ItemArray[5].ToString(), 430, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            incompatible.ItemArray[6].ToString(), 640, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            incompatible.ItemArray[7].ToString(), 680, currentLine, 0);
                        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            incompatible.ItemArray[8].ToString(), 720, currentLine, 0);
                        cb.EndText();

                        itemCount += 1;
                        currentLine -= 15;
                    }
                }
                cb.SaveState(); //matches beginning of foreach
            }

            cb.RestoreState(); //restore from last save in foreach

            GenerateIncompatiblesLegend(cb);


            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        public string GenerateInventoryChecklist(string type, string sort)
        {
            GenerateDD1348 naming = new GenerateDD1348();
            naming.updateDirectory();
            string fileName = naming.getRandomfileName("pdf");
            string relPath = "resources\\temp\\" + fileName;
            string tempPath = AppDomain.CurrentDomain.BaseDirectory + relPath;
            shipName = dbManager.getCurrentShipName();
            string reportTitle = "HM Inventory List ";

            Document document = new Document();
            FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);

            float itemCount = 0;
            numOfPages = (int)Math.Ceiling(itemCount / 50);
            if (numOfPages < 1) { numOfPages = 1; }
            currentPage = 0;
            int currentRow = 1;

            string workcenter_id = "0";
            if (type == "location")
            {
                DataTable work = new DataTable();
                work = dbManager.getWorkcenterByLocationId(Convert.ToInt64(sort));
                workcenter_id = work.Rows[0].ItemArray[2].ToString();
            }

            DataTable workcenter = new DataTable();
            workcenter = dbManager.getWorkcentersInUse(workcenter_id);

            document.Open();
            document.NewPage();

            currentPage += 1;
            currentLine = portraitLine;
            PdfContentByte cb = writer.DirectContent;
            cb.SetColorFill(BaseColor.BLACK);
            GeneratePortraitHeader(cb, reportTitle);

            cb.SetFontAndSize(helveticaBold, 10f);
            cb.BeginText();
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 60, currentLine + 5, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 85, currentLine + 5, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 135, currentLine + 5, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 413, currentLine + 15, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 413, currentLine + 5, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SHELF LIFE", 498, currentLine + 15, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "EXP DATE", 498, currentLine + 5, 0);
            cb.EndText();

            DrawPortraitLine(cb, 2.0f);

            currentLine -= 20;

            foreach (DataRow wc in workcenter.Rows)
            {
                cb.SetFontAndSize(helveticaBoldItalic, 15f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                   "WORKCENTER: " + wc.ItemArray[1].ToString(), 50, currentLine, 0);
                cb.EndText();

                currentLine -= 5;
                DrawPortraitLine(cb, 2.0f);
                currentLine -= 15;

                string loc_id = "0";
                if (type == "location")
                {
                    loc_id = sort;
                }

                DataTable locations = new DataTable();
                locations = dbManager.getLocationsInUse(Convert.ToInt64(wc.ItemArray[0].ToString()), loc_id);

                foreach (DataRow l in locations.Rows)
                {
                    long location_id = Convert.ToInt64(l.ItemArray[0].ToString());
                    cb.SetFontAndSize(helveticaBold, 10f);
                    cb.BeginText();
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                            "LOCATION: " + l.ItemArray[1].ToString(), 50, currentLine, 0);
                    cb.EndText();

                    currentLine -=15;

                    DataTable inventory = new DataTable();
                    inventory = dbManager.getInventoryFromLocationsInUse(location_id);

                    foreach (DataRow i in inventory.Rows)
                    {
                        cb.SetFontAndSize(helvetica, 8f);
                        cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                                i.ItemArray[0].ToString(), 60, currentLine, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                                i.ItemArray[1].ToString(), 85, currentLine, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                                i.ItemArray[2].ToString(), 135, currentLine, 0);                     
                        cb.EndText();

                        cb.SetLineWidth(1f);
                        cb.MoveTo(375, currentLine);
                        cb.LineTo(450, currentLine);
                        cb.Stroke();

                        cb.SetLineWidth(1f);
                        cb.MoveTo(460, currentLine);
                        cb.LineTo(535, currentLine);
                        cb.Stroke();

                        currentLine -= 20;

                        if (currentLine <= 210)
                        {
                            GeneratePortraitFooter(cb, " ");

                            currentLine = portraitLine;
                            currentPage += 1;
                            document.NewPage();

                            GeneratePortraitHeader(cb, reportTitle);

                            cb.SetFontAndSize(helveticaBold, 10f);
                            cb.BeginText();
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FSC", 60, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NIIN", 85, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ITEM NAME", 135, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ON HAND", 413, currentLine + 15, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "QTY", 413, currentLine + 5, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "SHELF LIFE", 498, currentLine + 15, 0);
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "EXP DATE", 498, currentLine + 5, 0);
                            cb.EndText();

                            DrawPortraitLine(cb, 2.0f);

                            currentLine -= 20;
                        }
                    }
                }
                currentLine -= 30;
            }

            GeneratePortraitFooter(cb, " ");
            
            document.Close();
            fs.Close();
            writer.Close();

            return relPath;
        }

        private void GenerateLandscapeFooter(PdfContentByte cb, string signatures)
        {
            //Line before footer
            cb.SetLineWidth(2.0f);
            cb.MoveTo(48, 95);
            cb.LineTo(744, 95);
            cb.Stroke();

            //Footer
            cb.SetFontAndSize(helvetica, 10f);
            cb.BeginText();
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "SHIMS - Submarine Hazardous Material Inventory & Management System", 53, 85, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + currentPage + " of " + numOfPages, 744, 85, 0);
            cb.EndText();

            #region Signature Block
            if (!String.IsNullOrEmpty(signatures) && signatures != " ")
            {
                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Signature & Date", 53, 75, 0);
                cb.EndText();

                //Signature Lines
                cb.SetLineWidth(0.5f);
                cb.MoveTo(53, 45);
                cb.LineTo(217, 45);
                cb.Stroke();

                cb.MoveTo(227, 45);
                cb.LineTo(391, 45);
                cb.Stroke();

                cb.MoveTo(401, 45);
                cb.LineTo(565, 45);
                cb.Stroke();

                cb.MoveTo(575, 45);
                cb.LineTo(739, 45);
                cb.Stroke();

                //Signature Names
                List<string> sig = new List<string>();
                sig = signatures.Split('|').ToList();

                cb.SetFontAndSize(helvetica, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[0], 55, 35, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[1], 229, 35, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[2], 404, 35, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[3], 577, 35, 0);
                cb.EndText();

            }
            #endregion
        }

        private void GenerateLandscapeHeader(PdfContentByte cb, string reportTitle)
        {
            //Header of Report
            if (currentPage <= 1)
            {
                cb.SetFontAndSize(helveticaBoldItalic, 20f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, shipName, 48, 540, 0);
                cb.EndText();

                cb.SetFontAndSize(helveticaBold, 20f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, reportTitle, 744, 540, 0);
                cb.EndText();
            }
            //Sub header of Report
            cb.SetFontAndSize(helveticaBold, 10f);
            cb.BeginText();
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reportTitle, 53, 520, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, DateTime.Today.ToString("MMMM dd, yyyy"), 744, 520, 0);
            cb.EndText();

            currentLine += 30;
            DrawLine(cb, 1.0f);
            currentLine -= 30;
        }

        private void GeneratePortraitFooter(PdfContentByte cb, string signatures)
        {
            //Line before footer
            cb.SetLineWidth(2.0f);
            cb.MoveTo(48, 200);
            cb.LineTo(550, 200);
            cb.Stroke();

            //Footer
            cb.SetFontAndSize(helvetica, 10f);
            cb.BeginText();
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "SHIMS - Submarine Hazardous Material Inventory & Management System", 53, 190, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Page " + currentPage + " of " + numOfPages, 550, 190, 0);
            cb.EndText();

            #region Signature Block
            if (!String.IsNullOrEmpty(signatures) && signatures != " ")
            {
                cb.SetFontAndSize(helveticaBold, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Signature & Date", 207, 180, 0);
                cb.EndText();

                //Signature Lines
                cb.SetLineWidth(0.5f);
                cb.MoveTo(207, 140);
                cb.LineTo(371, 140);
                cb.Stroke();

                cb.MoveTo(381, 140);
                cb.LineTo(545, 140);
                cb.Stroke();

                cb.MoveTo(207, 90);
                cb.LineTo(371, 90);
                cb.Stroke();

                cb.MoveTo(381, 90);
                cb.LineTo(545, 90);
                cb.Stroke();

                //Signature Names
                List<string> sig = new List<string>();
                sig = signatures.Split('|').ToList();

                cb.SetFontAndSize(helvetica, 10f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[0], 209, 130, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[1], 383, 130, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[2], 209, 80, 0);
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sig[3], 383, 80, 0);
                cb.EndText();

            }
            #endregion
        }

        private void GeneratePortraitHeader(PdfContentByte cb, string reportTitle)
        {
            //Header of Report
            if (currentPage <= 1)
            {
                cb.SetFontAndSize(helveticaBoldItalic, 18f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, shipName, 48, 775, 0);
                cb.EndText();

                cb.SetFontAndSize(helveticaBold, 18f);
                cb.BeginText();
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, reportTitle, 550, 775, 0);
                cb.EndText();
            }
            //Sub header of Report
            cb.SetFontAndSize(helveticaBold, 10f);
            cb.BeginText();
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, reportTitle, 53, 750, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, DateTime.Today.ToString("MMMM dd, yyyy"), 545, 750, 0);
            cb.EndText();

            currentLine += 30;
            DrawPortraitLine(cb, 1.0f);
            currentLine -= 30;
        }

        private string[] StyleLongStrings(PdfContentByte cb, string longString, int lineLength)
        {
            string[] modifiedString = {longString, "", ""};
            int stringLength = longString.Length;

            if (stringLength > lineLength)
            {
                if (stringLength >= 200)
                {
                    cb.SetFontAndSize(helvetica, 7f);
                    int firstClosestSpace = longString.IndexOf(' ', stringLength / 3);
                    int secondClosestSpace = longString.IndexOf(' ', 2 * stringLength / 3);
                    if (firstClosestSpace != -1 && secondClosestSpace != -1)
                    {
                        modifiedString[2] = longString.Substring(secondClosestSpace, stringLength - secondClosestSpace);
                        modifiedString[1] = longString.Substring(firstClosestSpace, secondClosestSpace - firstClosestSpace);
                        modifiedString[0] = longString.Substring(0, firstClosestSpace);
                    }
                }
                else
                {
                    cb.SetFontAndSize(helvetica, 8f);
                    int closestSpace = longString.IndexOf(' ', lineLength);
                    if (closestSpace != -1)
                    {
                        modifiedString[1] = longString.Substring(closestSpace, stringLength - closestSpace);
                        modifiedString[0] = longString.Substring(0, closestSpace);
                    }
                }
            }

            return modifiedString;
        }

        private void DrawLine(PdfContentByte cb, float lineHeight)
        {
            cb.SetColorStroke(new BaseColor(0, 0, 0));
            cb.SetLineWidth(lineHeight);
            cb.MoveTo(48, currentLine);
            cb.LineTo(744, currentLine);
            cb.Stroke();
        }

        private void DrawPortraitLine(PdfContentByte cb, float lineHeight)
        {
            cb.SetColorStroke(new BaseColor(0, 0, 0));
            cb.SetLineWidth(lineHeight);
            cb.MoveTo(48, currentLine);
            cb.LineTo(550, currentLine);
            cb.Stroke();
        }

        private void GenerateIncompatiblesLegend(PdfContentByte cb)
        {
            cb.SetColorStroke(new BaseColor(0, 0, 0));
            cb.SetLineWidth(2.0f);
            cb.MoveTo(48, 220);
            cb.LineTo(744, 220);
            cb.Stroke();

            cb.SetFontAndSize(helveticaBold, 8f);
            cb.BeginText();
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, 
                "Legend:", 53, 210, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, 
                "+  Allowed - Storage together is authorized", 53, 200, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, 
                "O  Restricted - Separated in a manner that, in the event of leakage, mixing of hazardous materials would not occur", 53, 190, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, 
                "X  Prohibited - Cannot be stored in the same compartment unless segregated by a NAVSEA approved cabinet", 53, 180, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, 
                "#  Consult MSDS for specific stowage requirements", 53, 170, 0);

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "HCC Specific Guidance:", 53, 150, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "(1) C1-C4: Store concentrated nitric acid in acid locker, and keep distance from other acids. Store Bromine Catridges in dedicated cabinet.", 53, 140, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "(2) D4: Store Calcium Hypochorite in designated NAVSEA approved locker. Do not store oxidizers in same compartment with flammables or combustables.", 53, 130, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "(3) G1-G9: Mount all stored gas cylinders to prevent them from falling or rolling (Grade B Shock). Keep maximum distance possible between flammable (G2, G8) and", 53, 120, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "oxidizer (G4, G7, G9) gases when not in use.", 53, 110, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "(4) V2, V3: All aerosols will be stored together within the same location within a storeroom. Further segregate aerosols from flammable liquids and gases in the same.", 53, 100, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "space using wire mesh or other barrier (e.g, locker) to prevent projectiles in case of fire.", 53, 90, 0);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT,
                "(5) Solids shall be stored above liquids.", 53, 80, 0);

            cb.EndText();
        }
    }
}