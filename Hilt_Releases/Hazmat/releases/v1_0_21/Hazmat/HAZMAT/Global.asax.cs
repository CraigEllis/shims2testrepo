﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Web.UI;
using System.Web.Http;

namespace HAZMAT
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //RouteTable.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{niin}/{cosal}/{serial}",
            //    defaults: new { niin = System.Web.Http.RouteParameter.Optional, cosal = System.Web.Http.RouteParameter.Optional, serial = System.Web.Http.RouteParameter.Optional }
            //);
            RouteTable.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { action = System.Web.Http.RouteParameter.Optional}
            );


            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled <b style="color: black; background-color: rgb(153, 255, 153);">error</b> occurs    
            Exception ex = Server.GetLastError().GetBaseException();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<br/><h1><b>SHIMS encountered an unexpected problem</b></h1><br/><h1>What to do: </h1> <br><b>Close your browser and try the attempted action again. If the problem persists please contact your system administrator. </b><br/><br/><hr/><br/>");
            sb.AppendLine("<h1>More info: </h1><br><h3><b>Message</b>:</h3><br/> <b>" + ex.Message.ToString() + "</b><br/><br/>");
            sb.AppendLine("<h3><b>Exception</b>:</h3><br/> <b>" + ex.GetType().ToString() + "</b><br/><br/>");
            if (ex.TargetSite != null)
                sb.AppendLine("<h3><b>Targetsite</b>:</h3><br/> " + ex.TargetSite.ToString() + "<br/><br/>");
            sb.AppendLine("<h3><b>Source</b>:</h3><br/>  " + ex.Source + "<br/><br/>");
            sb.AppendLine("<h3><b>StackTrace</b></h3><br/> " + ex.StackTrace.ToString().
                                          Replace(Environment.NewLine, "<br/>") + "<br/><br/>");
            sb.AppendLine("<h3><b>Data count</b>:</h3><br/> " + ex.Data.Count.ToString() + " ");
            if (ex.Data.Count > 0)
            {
                HtmlTable tbl = new HtmlTable();
                tbl.Border = 1;
                HtmlTableRow htr = new HtmlTableRow();
                HtmlTableCell htc1 = new HtmlTableCell();
                HtmlTableCell htc2 = new HtmlTableCell();
                HtmlTableCell htc3 = new HtmlTableCell();
                HtmlTableCell htc4 = new HtmlTableCell();
                htc1.InnerHtml = "<b>Key</b>";
                htc2.InnerHtml = "<b>Value</b>";
                htc3.InnerHtml = "Key Type";
                htc4.InnerHtml = "Value Type";

                htr.Cells.Add(htc1);
                htr.Cells.Add(htc2);
                htr.Cells.Add(htc3);
                htr.Cells.Add(htc4);
                tbl.Rows.Add(htr);

                foreach (DictionaryEntry de in ex.Data)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell tc1 = new HtmlTableCell();
                    HtmlTableCell tc2 = new HtmlTableCell();
                    HtmlTableCell tc3 = new HtmlTableCell();
                    HtmlTableCell tc4 = new HtmlTableCell();
                    tc1.InnerHtml = "<b>" + de.Key + "</b>";
                    tc2.InnerHtml = "<b>" + de.Value + "</b>";
                    tc3.InnerHtml = de.Key.GetType().Name;
                    tc4.InnerHtml = de.Value.GetType().Name;
                    tc3.Align = "center";
                    tc4.Align = "center";

                    tr.Cells.Add(tc1);
                    tr.Cells.Add(tc2);
                    tr.Cells.Add(tc3);
                    tr.Cells.Add(tc4);
                    tbl.Rows.Add(tr);
                }
                StringBuilder tblSb = new StringBuilder();
                System.IO.StringWriter sw = new System.IO.StringWriter(tblSb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                tbl.RenderControl(htw);
                sb.AppendLine(tblSb.ToString());
            }
            sb.AppendLine("<br/><br/>");
            sb.AppendLine("<h3><b>Exception</b>:</h3><br/> " + ex.ToString().
                                          Replace(Environment.NewLine, "<br/>") + "");

            try
            {
                Session["Exception"] = sb.ToString();
                Server.ClearError();

                Session["ExPath"] = Request.Path;
                if (ex.InnerException != null)
                {
                    Session["ExTitle"] = ex.InnerException.Message;
                }
                else
                    Session["ExTitle"] = null;
                Session["ExMess"] = ex.ToString();
                Session["ExTrace"] = ex.StackTrace;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc);
            }

            Response.Redirect("~/exception.aspx");



        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}

