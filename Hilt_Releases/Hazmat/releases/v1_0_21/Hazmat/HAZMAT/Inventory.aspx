﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="HAZMAT.Inventory" ClientIDMode="Static"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/AutoCompleteSearch.ascx" TagName="AutoCompleteSearch" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc" TagName="SMCLDetails"
    Src="~/Modals/SMCLDetailsModal.ascx" %>

<%@ Register TagPrefix="uc" TagName="ACMLabelModal"
    Src="~/Modals/ACMLabelModal.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 43px;
        }

        .spaced {
            padding: 10px 40px 10px 40px;
        }
         .left-button {
            float: left;
            width: 37px;
            padding:310px 0px 310px 15px
        }
        .right-button {
            float: right;
            width: 37px;
            padding:310px 15px 310px 0px
        }
    </style>
    <script type="text/javascript" src="/resources/js/site.js"></script>
    <script type="text/javascript" src="/resources/js/editor.select2.js"></script>
    <script type="text/javascript" src="/resources/js/Inventory.js"></script>
    <script type="text/javascript" src="/resources/js/InventoryAdd.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-header">
        <h1>Inventory
        </h1>
    </div>


    <div class="tableHeaderDiv">
        <div class="table-grid-headwrap clearfix">

            <table class="pull-left">
                <tr>
                    <td>
                        <label>
                            <b>NIIN</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterNiinList" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>Work Center</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterWorkCenter" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>SPMIG</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterSPMIG" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 15px;" valign="bottom">
                        <label>
                            Search Inventory:
                        </label>
                        <input id="searchInv" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <b>Item Name</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterItemName" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>Location</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterLocation" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>SPECS</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterSpecs" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 15px;" id="tableCountCell" valign="bottom"></td>
                </tr>
            </table>
            <div>
                <br />
                <br />
            </div>

        </div>
        <button class="btn btn-primary" id="btnAddNewInventory" style="margin-left: 10px; margin-top: 10px;">Add New Inventory</button>
        <hr />
        <table id="inventoryTable" class="table" width="100%">
            <thead>
                <th>MSDS</th>
                <th>SMCL DETAILS</th>
                <th>FSC</th>
                <th>NIIN</th>
                <th>ITEM NAME</th>
                <th>WORK CENTER</th>
                <th>Work Center ID</th>
                <th>LOCATION</th>
                <th>ON-HAND QTY</th>
                <th>ALLOWED QTY</th>
                <th>ACM CATEGORY</th>
                <th>U/I</th>
                <th>U/M</th>
                <th>SHELF LIFE DATE</th>
                <th>SPMIG</th>
                <th>MANUFACTURER</th>
                <th>CAGE</th>
                <th>DELETE ITEM</th>
                <th>OFFLOAD</th>
            </thead>
        </table>
        <hr />
        <br />
    </div>

    <div class="modal" id="confirmDeleteModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>Confirm Deletion of Inventory Item</b></h>
                </div>
                <div class="modal-body">
                    You are about to delete the selected NIIN:
                    <label id="niin_label"></label>
                    at location:
                    <label id="location_label"></label>
                    with CAGE:
                    <label id="cage_label"></label>
                    and Shelf Life Date:
                    <label id="expiration_label"></label>
                    . Are you sure you want to delete this record?
                    <label id="inventory_id_label" style="visibility: hidden"></label>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="confirmDeleteBtn">Delete</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="msdsModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>MSDS modal</b></h>
                </div>
                <div class="modal-body">
                     <div class="row">
                        <div class="col-md-1 left-button" style="margin-right:10px">
                            <button class="btn btn-info btn-lg" type="button" id="msdsLeft" style="padding: 5px 0px; height:100px; margin:0px auto" >
                                <span class="glyphicon glyphicon-chevron-left" style="font-size:20px"></span>
                            </button>
                        </div>
                        <div class="col-md-11">
                                <object id="msdsPdf" style="width:100%; height:700px; margin:0 auto;" data=""></object>
                                <iframe id="msdsDoc" style="width:100%; height:700px; margin:0 auto; display:none"></iframe>
                                
                                <div id="noMsds" style="display:none; text-align:center; padding-top:345px">
                                    <span style="color:grey; font-size:250%">NO MSDS FOUND FOR THIS ITEM</span>
                                </div>
                        </div>

                        <div class="col-md-1 right-button">
                            <button class="btn btn-info btn-lg" type="button" id="msdsRight" style="padding: 5px 0px; height: 100px; margin:0px auto">
                                <span class="glyphicon glyphicon-chevron-right" style="font-size:20px"></span>
                            </button>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <uc:SMCLDetails ID="SMCLDetailsModal" runat="server"></uc:SMCLDetails>

    <div class="modal" id="InventoryDetailsModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title"><b>Item Details</b>
                        <a id="printInvDetails">
                            <button class="btn btn-info btn-sm btn-details" type="button" style="float: right; margin-right: 30px;">Print</button></a>
                        <button class="btn btn-info btn-sm btn-details" id="btn-labels" style="float: right; margin-right: 30px">ACM Labels</button>
                        <button class="btn btn-info btn-sm btn-details" id="btn-SMCLDetails" style="float: right; margin-right: 30px">SMCL Details</button>
                    </h3>
                </div>
                <div class="modal-body" id="inv-modal-body-section">
                    <label id="locationTxt" style="visibility: hidden"></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-size: small">Item Description</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <label style="font-size: smaller">FSC-NIIN</label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input class="form-control smcl-field" id="inv_FSC_label" disabled="disabled" maxlength="4" />
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control smcl-field" id="inv_NIIN_label" disabled="disabled" />
                                    </div>
                                </div>
                                <label style="font-size: smaller">Description</label>
                                <input class="form-control smcl-field" id="inv_description_label" disabled="disabled" />

                                <label style="font-size: smaller">
                                    U/I</label><br />
                                <select id="inv_ui" class="smcl-field" disabled="disabled"></select>
                                <br />
                                <label style="font-size: smaller">U/M</label>
                                <input class="form-control smcl-field" id="inv_um_label" disabled="disabled" />

                                <label style="font-size: smaller">SPMIG</label>
                                <input class="form-control smcl-field" id="inv_spmig_label" disabled="disabled" />

                                <label style="font-size: smaller">SPECS</label>
                                <input class="form-control smcl-field" id="inv_specs_label" disabled="disabled" />
                            </div>
                            <br />
                            <div class="form-group">
                                <label style="font-size: small">SMCL Remarks</label>
                                <textarea class="form-control" id="inv_remarks_text" rows="2" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-size: small">Item Status</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <label style="font-size: smaller">Usage Category</label>
                                <select id="inv_usage_category" class="form-control smcl-field" disabled="disabled"></select>
                                <label style="font-size: smaller">Shelf Life Code</label>
                                <br />
                                <select id="inv_slc" class="smcl-field" disabled="disabled" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">Shelf Life Action Code</label>
                                <br />
                                <select id="inv_slac" class="smcl-field" disabled="disabled" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">HCC</label>
                                <br />
                                <select id="inv_hcc" disabled="disabled" class="smcl-field" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">SMCC</label>
                                <br />
                                <select class="smcl-field" id="inv_smcc" disabled="disabled" style="width: 500px;"></select>
                            </div>
                            <br />
                            <div class="form-group">
                                <label style="font-size: small">Notes</label>
                                <textarea class="form-control" id="inv_notes_text" rows="2" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 10px">
                            <div class="col-md-12" style="padding: 25px">
                                <label style="font-size: small">On-Hand Inventory</label>
                                <div style="border: 1px solid lightgrey; padding: 10px;">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label style="font-size: smaller">Total Quantity Available</label>
                                            <input class="form-control" id="inv_total_qty" disabled="disabled" />
                                        </div>
                                        <div class="col-md-3">
                                            <label style="font-size: smaller">Quantity Allowed</label>
                                            <input class="form-control" id="inv_qty_allowed" disabled="disabled" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="invDetailsTable" class="table invTable">
                                                <thead>
                                                    <th>Location</th>
                                                    <th>CAGE</th>
                                                    <th>Manufacturer</th>
                                                    <th>Qty</th>
                                                    <th>Workcenter</th>
                                                    <th>Inventoried Date</th>
                                                    <th>Onboard Date</th>
                                                    <th>Expiration Date</th>
                                                    <th>InventoryDetailIdHidden</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <label id="successLabelInv" style="padding-right: 20px;"></label>
                    <button id="btn_updateInvItem" class="btn btn-info">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                    <label id="lbl_inventory_onhand_id" style="visibility: collapse;"></label>
                    <label id="lbl_inventory_detail_id" style="visibility: collapse;"></label>
                </div>
            </div>
        </div>
    </div>


    <div class="modal" id="AddInventoryModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h3 class="modal-title"><b>Add Inventory Item</b>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-size: small">Item Description</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <label class="req" style="font-size: smaller">FSC-NIIN</label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input class="form-control smcl-field" id="add_inv_FSC_label" />
                                    </div>
                                    <div class="col-md-6">
                                        <input id="add_inv_niin" style="width: 300px" />
                                    </div>
                                </div>
                                <label class="req" style="font-size: smaller">Description</label>
                                <input class="form-control smcl-field" id="add_inv_description_label" />

                                <label style="font-size: smaller">
                                    U/I</label><br />
                                <select class="smcl-field" id="add_inv_ui" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">U/M</label>
                                <input class="form-control smcl-field" id="add_inv_um_label" />

                                <label style="font-size: smaller">SPMIG</label>
                                <input class="form-control smcl-field" id="add_inv_spmig_label" />

                                <label style="font-size: smaller">SPECS</label>
                                <input class="form-control smcl-field" id="add_inv_specs_label" />
                            </div>
                            <br />
                            <div class="form-group">
                                <label style="font-size: small">SMCL Remarks</label>
                                <textarea class="form-control" id="add_inv_remarks_text" rows="4" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-size: small">Item Status</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <label style="font-size: smaller">Usage Category</label>
                                <select id="add_inv_usage_category" class="form-control" disabled="disabled" style="width: 500px"></select>
                                <%--not yet evaluated--%>
                                <label style="font-size: smaller">Shelf Life Code</label>
                                <br />
                                <select id="add_inv_slc" class="smcl-field" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">Shelf Life Action Code</label>
                                <br />
                                <select id="add_inv_slac" class="smcl-field" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">HCC</label>
                                <br />
                                <select id="add_inv_hcc" class="smcl-field" style="width: 500px"></select>
                                <br />
                                <label style="font-size: smaller">SMCC</label>
                                <br />
                                <select id="add_inv_smcc" class="smcl-field" style="width: 500px"></select>
                            </div>
                            <br />
                            <div class="form-group">
                                <label style="font-size: small">Notes</label>
                                <textarea class="form-control" id="add_inv_notes_text" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 10px">
                            <div class="col-md-12" style="padding: 25px">
                                <label style="font-size: small">On-Hand Inventory</label>
                                <div style="border: 1px solid lightgrey; padding: 10px;">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label style="font-size: smaller">Total Quantity Available</label>
                                            <input class="form-control" id="add_inv_total_qty" disabled="disabled" />
                                        </div>
                                        <div class="col-md-3">
                                            <label style="font-size: smaller">Quantity Allowed</label>
                                            <input class="form-control" id="add_inv_qty_allowed" disabled="disabled" value="0" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="addInvDetailsTable" class="table invTable">
                                                <thead>
                                                    <th>Location</th>
                                                    <th>CAGE</th>
                                                    <th>Manufacturer</th>
                                                    <th>Qty</th>
                                                    <th>Workcenter</th>
                                                    <th>Inventoried Date</th>
                                                    <th>Onboard Date</th>
                                                    <th>Expiration Date</th>
                                                    <th>InventoryDetailIdHidden</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <label id="successLabelAddInv" style="padding-right: 20px;"></label>
                    <button id="btn_addInvItem" class="btn btn-info" disabled="disabled">Update</button>
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                    <label id="lbl_add_inventory_detail_id" style="visibility: collapse;"></label>

                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="sfrNotificationModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>SMCL Feedback Report</b></h>
                </div>
                <div class="modal-body">
                    This NIIN is not in the SMCL and also not in your inventory. It has not been authorized for submarine use. If you add this item to your inventory,
                    an SFR will be saved in the SHIMS system. You can access it from the SFR module.
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Okay</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="confirmOffloadModal">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>Confirm Offload of Inventory Item</b></h>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-12">
                            <label style="font-size: small">ITEM DESCRIPTION</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="fscText" disabled="disabled" />
                                </div>
                                <div class="col-md-1">
                                    <label>-</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="niinText" disabled="disabled" />
                                </div>
                                <br />
                                <br />
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="nameText" disabled="disabled" />
                                </div>
                                <br />
                                <br />
                                <div class="col-md-6">
                                    <label style="line-height: 36px">TOTAL QUANTITY ONBOARD: </label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" id="totalText" disabled="disabled" />
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" id="uiText" disabled="disabled" />
                                </div>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" id="umText" disabled="disabled" />
                                </div>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-9">
                            <label style="font-size: small">ENTER DATA</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <div class="col-md-6">
                                    <label style="line-height: 36px">CAGE: </label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="cageText" disabled="disabled" />
                                </div>
                                <br />
                                <br />
                                <br />
                                <div class="col-md-6">
                                    <label style="line-height: 36px">OFFLOAD QUANTITY: </label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="offloadQuantity" />
                                    <input type="hidden" id="inventoryId" />
                                </div>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="confirmOffloadBtn">Offload</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <uc:ACMLabelModal ID="labelsModal" runat="server"></uc:ACMLabelModal>

</asp:Content>
