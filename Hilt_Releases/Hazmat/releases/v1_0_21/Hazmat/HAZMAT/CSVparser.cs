﻿using System.Collections.Generic;
using System.IO;

namespace HAZMAT
{
    public class CSVparser
    {
        public List<string[]> parseCSV(Stream stream)
        {
            List<string[]> parsedData = new List<string[]>();


                 using (StreamReader readFile = new StreamReader(stream))
                {
                    string line;
                    string[] row;

                    while ((line = readFile.ReadLine()) != null)
                    {
                        row = line.Split('\t');
                        parsedData.Add(row);                       
                    }
                }
            

            return parsedData;
        }
    }
}