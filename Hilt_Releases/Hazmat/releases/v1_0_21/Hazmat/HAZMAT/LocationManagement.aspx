﻿<%@ Page Title="" StylesheetTheme="WINXP_Blue" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LocationManagement.aspx.cs" Inherits="HAZMAT.LocationManagement" %>

<%@ Register Src="~/AutoCompleteSearch.ascx" TagName="AutoCompleteSearch"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .style1 {
            width: 43px;
        }

        .warning {
            color: Red;
        }
    </style>
    <script type="text/javascript" src="/resources/js/LocationAdmin.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-header">
        <h1>Location Administration
        </h1>
    </div>

    <hr id="line" />
    <label>Search Locations: </label>
    <input id="searchLocations" style="margin-right:10px;" />
    <table id="locationTable" class="table">
        <thead>
            <th>Location</th>
            <th>Workcenter</th>
            <th>Delete</th>
        </thead>
    </table>
    <hr />

</asp:Content>
