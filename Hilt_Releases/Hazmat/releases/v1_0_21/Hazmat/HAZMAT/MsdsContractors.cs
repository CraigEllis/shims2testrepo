﻿using System;

namespace HAZMAT
{
    [Serializable]
    public class MsdsContractors
    {

        //CONTRACT NUMBER

        public string CT_NUMBER
        {
            get;
            set;
        }
        //CONTRACTOR CAGE
     
        public string CT_CAGE
        {
            get;
            set;
        }
        //CONTRACTOR CITY
     
        public string CT_CITY
        {
            get;
            set;
        }
        //CONTRACTOR COMPANY NAME
     
        public string CT_COMPANY_NAME
        {
            get;
            set;
        }
        //CONTRACTOR COUNTRY
    
        public string CT_COUNTRY
        {
            get;
            set;
        }
        //CONTRACTOR PO BOX
    
        public string CT_PO_BOX
        {
            get;
            set;
        }
        //CONTRACTOR STATE
     
        public string CT_STATE
        {
            get;
            set;
        }
        //CONTRACTOR TELE NUMBER
     
        public string CT_PHONE
        {
            get;
            set;
        }
        //PURCHASE ORDER NUMBER
    
        public string PURCHASE_ORDER_NO
        {
            get;
            set;
        }

    }
}