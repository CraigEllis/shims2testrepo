﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAZMAT
{
    [CoverageExclude]
    public partial class OffloadArchived : Page
    {
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {
        }

    }
  /*  [CoverageExclude]
    public partial class OffloadArchived : Page
    {
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {

            loadArchives();

            loadOffload();

            loadGarbage();

        }

        private void loadArchives()
        {
            GridViewArchived.DataSource = new DatabaseManager().getArchivedList().DefaultView;
            GridViewArchived.DataBind();
        }

        private void loadOffload()
        {
            int archived_id = 0;
            if (GridViewArchived.SelectedIndex != -1 && GridViewArchived.SelectedDataKey != null)               
                 archived_id= Int32.Parse(GridViewArchived.SelectedDataKey["archived_id"].ToString());

            String filterColumn = "";
            String filterText = "";
            if (chkbox_filter.Checked)
            {
                filterColumn = DropDownListSearch.SelectedValue;
                filterText = TxtBoxSearch.Text;
            }
            GridViewOffload.DataSource = new DatabaseManager().getArchivedOffloadTopViewList(filterColumn, filterText, archived_id).DefaultView;
            GridViewOffload.DataBind();
        }

       

        private void loadGarbage()
        {
            int archived_id = 0;
            if (GridViewArchived.SelectedIndex != -1 && GridViewArchived.SelectedDataKey != null)
                archived_id = Int32.Parse(GridViewArchived.SelectedDataKey["archived_id"].ToString());

            String filterColumn = "";
            String filterText = "";
            if (chkbox_filterGarbage.Checked)
            {
                filterColumn = DropDownListGarbageSearch.SelectedValue;
                filterText = TxtBoxGarbageSearch.Text;
            }
            GridViewGarbage.DataSource = new DatabaseManager().getArchivedGarbageTopViewList(filterColumn, filterText, archived_id).DefaultView;
            GridViewGarbage.DataBind();
        }


        [CoverageExclude]
        protected void GridViewOffload_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewOffload.PageIndex = e.NewPageIndex;
            GridViewOffload.SelectedIndex = -1;
            GridViewOffload.DataBind();
        }

        [CoverageExclude]
        protected void GridViewGarbage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewGarbage.PageIndex = e.NewPageIndex;
            GridViewGarbage.SelectedIndex = -1;
            GridViewGarbage.DataBind();
        }
        [CoverageExclude]
        protected void GridViewArchived_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SelectRow")
            {
                GridViewArchived.SelectedIndex = Convert.ToInt16(e.CommandArgument);

                loadOffload();

                loadGarbage();
               
            }
        }
        [CoverageExclude]
        protected void GridViewArchived_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewArchived.PageIndex = e.NewPageIndex;
            GridViewArchived.SelectedIndex = -1;
            GridViewArchived.DataBind();
        }

        protected void btnFeedback_Click(object sender, ImageClickEventArgs e)
        {

            //save current page to the session for feedback
            Session.Add("feedbackPage", GetCurrentPageName());

            ClientScript.RegisterStartupScript(Page.GetType(), "",
"window.open('FEEDBACK.aspx','Feedback','height=400,width=590, scrollbars=no');", true);


        }

        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        protected void btnHelp_Click(object sender, ImageClickEventArgs e)
        {

            ClientScript.RegisterStartupScript(Page.GetType(), "",
    "window.open('Help.html','Help','height=750,width=790, scrollbars=yes');", true);
        } 
      
        
      

        
    }*/
}