﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAZMAT
{
   /* public partial class MSDSHCCReport : Page
    {
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {

            loadGridView();

            if (!Page.IsPostBack)
                new Reports().populateReportsDropdown(DropDownListReport, "MSDS's with changed HCC's"); 
        }

        protected void loadGridView()
        {
            String filterColumn = "";
            String filterText = "";
            if (popupCkBox.Checked)
            {
                filterColumn = popupDropDown.SelectedValue;
                filterText = popupTxtSearch.Text;
            }

            gridView1.DataSource = new DatabaseManager().getMSDSWithChangedHCC(
                    filterColumn, filterText).DefaultView;
            gridView1.DataBind();
            addHeaderRow();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridView1.PageIndex = e.NewPageIndex;
            gridView1.SelectedIndex = -1;
            gridView1.DataBind();
            addHeaderRow();
        }

        protected void WrongLocationGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridView1.PageIndex = e.NewPageIndex;
            gridView1.SelectedIndex = -1;
            gridView1.DataBind();
            addHeaderRow();
        }

        protected void btnFeedback_Click(object sender, ImageClickEventArgs e)
        {

            //save current page to the session for feedback
            Session.Add("feedbackPage", GetCurrentPageName());

            ClientScript.RegisterStartupScript(Page.GetType(), "",
                "window.open('FEEDBACK.aspx','Feedback','height=400,width=590, scrollbars=no');", true);


        }

        protected void btnChooseReport_Click(object sender, EventArgs e)
        {

            String selectedValue = DropDownListReport.SelectedValue;

            if (!selectedValue.Equals("Feedback"))
            {
                string url = new Reports().getReportUrl(selectedValue);
                Response.Redirect(url);
            }
            else if (selectedValue.Equals("Feedback"))
            {


                //get feedback info
                List<FeedbackInfo> infoList = new DatabaseManager().getFeedbackInfo();


                //call class or method to create tab delimited report
                byte[] file = new DatabaseManager().createFeedbackReport(infoList);

                //allow user to download report
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "FeedbackReport.xls");
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(file);
                Response.Flush();
                Response.End();

            }



        }

        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        protected void btnHelp_Click(object sender, ImageClickEventArgs e)
        {

            ClientScript.RegisterStartupScript(Page.GetType(), "",
   "window.open('Help.html','Help','height=750,width=790, scrollbars=yes');", true);
        }


        [CoverageExclude]
        protected void btnExcelReport_Click(object sender, EventArgs e)
        {




            ExcelReports report = new ExcelReports();
            string reportPath = Request.PhysicalApplicationPath + "resources/Reports.xls";
            string filename = report.exportReports(reportPath);


            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=ExcelReports.xls");
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(filename);
            Response.Flush();
            Response.End();

        }

        private void addHeaderRow() {
            // Add a header row with colspans to make the grid view prettier
            Table tbl = (Table) gridView1.Controls[0];

            GridViewRow row = new GridViewRow(0, -1, DataControlRowType.Header, DataControlRowState.Normal);
            TableCell th = new TableHeaderCell();

            // MSDS SerNo - blank - colspan = 1
            row.Cells.Add(th);

            // Original MSDS
            th = new TableHeaderCell();
            th.HorizontalAlign = HorizontalAlign.Center;
            th.ColumnSpan = 3;
            th.Text = "Original MSDS";
            row.Cells.Add(th);

            // Current MSDS
            th = new TableHeaderCell();
            th.HorizontalAlign = HorizontalAlign.Center;
            th.ColumnSpan = 3;
            th.Text = "Current MSDS";
            row.Cells.Add(th);

            // Current MSDS
            th = new TableHeaderCell();
            row.Cells.Add(th);

            tbl.Rows.AddAt(0, row);
        }

        protected void btnExportChangedHCCs_Click(object sender, EventArgs e)
        {

            String location = "";
            String workCenter = "";

            ExcelReports report = new ExcelReports();
            string reportPath = Request.PhysicalApplicationPath + "resources/ChangedHCCsReport.xls";
            string filename = report.exportChangedHCCsReport(reportPath, location, workCenter);


            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=ChangedHCCsReport.xls");
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(filename);
            Response.Flush();
            Response.End();
        }


    }*/
}