﻿var editor;
var idCount = 0;
var itemCount = 1;
var jsonIds;
var shipDetails;
var canSave = false;

$(document).ready(function () {

    $.get("api/Offload/getShipTo", function (data) {
        var option = '<option value=" "> </option>';
        var ships = $('#shipTo');

        for (i = 0; i < data.uic.length; i++) {
            option += '<option value="' + data.uic[i] + '">' + data.uic[i]+" | "+data.name[i] + ' </option>';
        }
        ships.append(option);
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('#toDisk').attr('href', function () {
        var url = "api/Offload/CSV";
        return (url);
    });

    loadTables();

    $(document).on('click', 'tr.odd', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            $('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            itemCount = $("tbody tr.selected").index()+1;
        }
    });
    $(document).on('click', 'tr.even', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            $('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            itemCount = $("tbody tr.selected").index() + 1;
        }
    });

    $(".shipSelect").select2({ allowClear: true });

    $('#recdDate').datepicker({ dateFormat:'yy-mm-dd' });
    $('#docDate').datepicker({ dateFormat: 'yy-mm-dd' });
});

function loadTables() {
    $('#OffloadTable').dataTable({
        "ajax": {
            "url": "/api/Offload/LoadOffloadTable",
            "type": "GET"
        },
        "order":[[1,"desc"]],
        "columns": [
            { data: "document_number" },
            { data: "offload_list_id" },
            { data: "fsc" },
            { data: "niin" },
            { data: "cage" },
            { data: "description" },
            { data: "quantity" },
            { data: "ui" },
            { data: "um" },
            {
              "mRender": function (data, type, row) {
                  return '<input type="button" class="btn btn-info btn-sm btn-details btnDelete" data-OffloadId ="' + row.offload_list_id+ '" value="Delete"/>';
              }
            }]
    });


}

$(document).on('change', function () {
    var select = $('#select2-chosen-1').text();
    var selected = select.split(" | ");
    var uic = selected[0].trim();
    if (uic) {
        $.get("api/Offload/GetShipToDetails", { uic: uic }, function (data) {

            $('#select2-chosen-1').text(uic);
            $('#shipToDetails').val(data.shipname);
            $('#shipPoc').val(data.poc + ', ' + data.telephone);
            $('.btn-ddFilled').removeAttr('disabled');
        });
    } else {
        $('#shipToDetails').val(' ');
        $('#shipPoc').val(' ');
        $('.btn-ddFilled').attr('disabled', 'disabled');
    }
});

$(document).on('click', '.btnDelete', function (e) {
    e.preventDefault();
    var offload_id = $(this).attr('data-OffloadId');
    var $modal = $('#confirmDeleteModal');
    $.get("api/Offload/GetDeleteModal?offload_id=" + offload_id, function (data) {
        $modal.find('#offload_id_label').text(data.aaData[0].offload_list_id);
        $modal.find('#niin_label').text(data.aaData[0].niin);
        $modal.find('#quantity_label').text(data.aaData[0].quantity);

        $modal.modal('show');
    });
    $modal.modal('show');
});

$(document).on('click', '#confirmDeleteBtn', function (e) {
    e.preventDefault();
    var offload_id = $('#offload_id_label').text();
    var jsonData = { OffloadId: offload_id };
    $.ajax({
        type: 'POST',
        url: "api/Offload/DeleteRecord",
        data: JSON.stringify(jsonData),
        contentType: 'application/json',
        dataType: 'json',
        success: function () { location.reload() }
    });
});

$(document).on('click', '#archiveAll', function (e) {
    e.preventDefault();
    var offload_id = $(this).attr('data-OffloadId');
    var $modal = $('#confirmArchiveModal');
    $modal.modal('show');
});

$(document).on('click', '#modalClose', function(e) {
    saveOffloadItemDetails(itemCount);
    location.reload();
});

$(document).on('click', '#modalCancel', function (e) {
    //saveOffloadItemDetails(itemCount);
    location.reload();
});

$(document).on('click', '#ddLeft', function(e) {

    saveOffloadItemDetails(itemCount);

    if (itemCount > 1 && itemCount <= idCount) { itemCount = parseInt(itemCount) - 1; }
    else if (itemCount > idCount) {itemCount = 1;}
    else { itemCount = idCount;}

    loadOffloadItemDetails(itemCount);
    $('#currentItem').val(itemCount);

});

$(document).on('click', '#ddRight', function (e) {

    saveOffloadItemDetails(itemCount);

    if (itemCount >= 1 && itemCount < idCount) { itemCount = parseInt(itemCount)+1; }
    else if (itemCount > idCount) { itemCount = idCount; }
    else { itemCount = 1 ; }

    loadOffloadItemDetails(itemCount);
    $('#currentItem').val(itemCount);
});

$(document).on('click', '#blank1348', function (e) {
    e.preventDefault();
    var uic = $('#select2-chosen-1').text().trim();
    var $modal = $('#dd1348Modal');
    $('#ship').val(' ');
    $('#addData3').val(' ');

    $('#ddLeft').hide();
    $('#ddRight').hide();
    $('#confirmPrintBtn').hide();
    $('#modalClose').hide();
    $('#currentItem').hide();
    $('#NSN').hide();
    $('#modalCancel').show();
    $('#modalOk').show();
    $('#confirmBlankPrintBtn').show();
    $('#blankNSN').show();


    $.get("api/Offload/GetShipFromDetails", function (data) {
        var shipFromAddress = data.uic + "\r\n" + data.name + "\r\n" + data.address;
        $('#shipFrom').val(data.uic);       
        $('#addData2').val(shipFromAddress);
    });

    if (uic) {
        $.get("api/Offload/GetShipToDetails", { uic: uic }, function (data) {
            var shipToAddress = uic + "\r\n" + data.poc + "\r\n" + data.address;
            $('#ship').val(uic);
            $('#addData3').val(shipToAddress);
        });
    }
    $modal.modal('show');
});

$(document).on('input', '.form-field', function (e) {
    var thisValue = $.trim(this.value);
    canSave = false;

    if (thisValue) {
        canSave = true;
    } else {

        $('#dd1348Modal :input.form-field').each(function() {
            if ($.trim($(this).val())) { canSave = true; }
        });
    }
});

$(document).on('input', '#currentItem', function(e) {
    var currentItem = $('#currentItem').val();

    saveOffloadItemDetails(itemCount);

    if (currentItem >= 1 && currentItem <= idCount) {
        itemCount = currentItem;
        loadOffloadItemDetails(itemCount);
        $('#currentItem').val(itemCount);
        $('#ddLeft').removeAttr("disabled");
        $('#ddRight').removeAttr("disabled");
    } else {
        itemCount = itemCount;
        /*$('#ddLeft').attr("disabled", "disabled");
        $('#ddRight').attr("disabled", "disabled");*/
}
});

$(document).on('change', '#currentItem', function (e) {
    var currentItem = $('#currentItem').val();

    if (currentItem < 1 || currentItem > idCount) {
        $('#currentItem').val(itemCount);
        $('#ddLeft').removeAttr("disabled");
        $('#ddRight').removeAttr("disabled");
    }
});

$(document).on('click', '#modalOk', function (e) {
    var $modal = $('#dd1348Modal');

    if (canSave) {
        var uniqueDetails = stringifyDetails(true);
        $.get("api/Offload/AddUniqueOffloadItem", { details: uniqueDetails }, function (data) { });
        location.reload();
    } else {$modal.modal('hide');}
});

$(document).on('click', '#filled1348', function (e) {
    e.preventDefault();
    var uic = $('#select2-chosen-1').text();
    var $modal = $('#dd1348Modal');

    $('#confirmPrintBtn').show();
    $('#modalClose').show();
    $('#ddLeft').show();
    $('#ddRight').show();
    $('#currentItem').show();
    $('#NSN').show();
    $('#confirmBlankPrintBtn').hide();
    $('#modalCancel').hide();
    $('#modalOk').hide();
    $('#blankNSN').hide();

    $.get("api/Offload/GetShipFromDetails", function (data) {
        var shipFromAddress = data.uic + "\r\n" + data.name + "\r\n" + data.address;
        $('#shipFrom').val(data.uic);
        $('#addData2').val(shipFromAddress);
    });

    $.get("api/Offload/GetShipToDetails", { uic: uic }, function (data) {
        var shipToAddress = uic + "\r\n" + data.poc + "\r\n" + data.address;
        $('#ship').val(uic);
        $('#addData3').val(shipToAddress);
    });

    $.get("api/Offload/GetOffloadItems", function (data) {
        var jsonData = [JSON.stringify(eval("(" + data + ")"))];
        jsonIds = JSON.parse(jsonData);
        idCount = Object.keys(jsonIds).length;

        if (idCount > 0) {
            $('#currentItem').val(itemCount);
            $('#items').text("/" + idCount);
            loadOffloadItemDetails(itemCount);
            $modal.modal('show');
        }
    });
});

$(document).on('click', '#confirmPrintBtn', function (e) {
    e.preventDefault();
    var uic = $('#select2-chosen-1').text();
    var $modal = $('#dd1348Modal');

    saveOffloadItemDetails(itemCount);

    $('#confirmPrintBtn').attr("disabled", "disabled");
    $('#modalClose').attr("disabled", "disabled");
    $('.close').attr("disabled", "disabled");
    $('#ddLeft').attr("disabled", "disabled");
    $('#ddRight').attr("disabled", "disabled");
    $('.clicked').show();
    $('.load-icon').show();
    $('.not-clicked').hide();

    $.get("api/Offload/CreatePdf", { uic: uic}, function (data) {

        window.open(data.relPath);
        $('.btn-ddFilled').removeAttr("disabled");
        $('.clicked').hide();
        $('.load-icon').hide();
        $('.not-clicked').show();
        $('#confirmPrintBtn').removeAttr("disabled");
        $('#modalClose').removeAttr("disabled");
        $('#close').removeAttr("disabled");
        $modal.modal('hide');
        location.reload();
    });
});

$(document).on('click', '#confirmBlankPrintBtn', function(e) {
    e.preventDefault();
    printOffloadItemDetails();
});

$(document).on('click', '#confirmArchiveBtn', function (e) {
    e.preventDefault();
    /*var offload_id = $('#offload_id_label').text();
    var jsonData = { OffloadId: offload_id };*/
    $.ajax({
        type: 'POST',
        url: "api/OffloadArchived/AddOffloadArchivedItem",
        success: function () { location.reload() }
    });
})

$(document).on('click', '#docNumBtn', function (e) {

    $.get("api/Offload/GenerateDocNumber", function (data) {
        $('#docNumber').val(data);
    });
});

$(document).on('blur', '#unitPrice', function(e) {
    var unitPrice = $('#unitPrice').val();
    var decCount = unitPrice.replace(/[^.]/g, "").length;
    var decIndex = unitPrice.indexOf('.');
    var isCorrect = true;


    if (decIndex === -1) {
        if (unitPrice.length > 0) {$('#unitPrice').val(unitPrice + ".00");}
        isCorrect = true;
    }
    else if (decIndex > 0) {
        if (decCount === 1) {
            var unitLength = unitPrice.length - 1;

            if (unitLength - decIndex > 2) { isCorrect = false; }
            else if (unitLength - decIndex === 1) {
                $('#unitPrice').val(unitPrice + "0");
                isCorrect = true;
            } else if (unitLength - decIndex === 0) {
                $('#unitPrice').val(unitPrice + "00");
                isCorrect = true;
            }else {isCorrect = true;}
        } else if (decCount >= 2) {isCorrect = false;}
    }
    else if (decIndex === 0) {
        var unitLength = unitPrice.length - 1;

        if (unitLength <= 1) { isCorrect = false; }
        else {
            $('#unitPrice').val("0" + unitPrice);
            isCorrect = true;
        }
    }
    else { isCorrect = false; }

    if (isCorrect) {
        $('#confirmPrintBtn').removeAttr("disabled");
        $('#modalClose').removeAttr("disabled");
        $('#modalOk').removeAttr("disabled");
        $('#unitPrice').css("background", "ghostwhite");
        $('#unitPrice').tooltip('hide');
    } else {
        $('#confirmPrintBtn').attr("disabled", "disabled");
        $('#modalClose').attr("disabled", "disabled");
        $('#modalOk').attr("disabled", "disabled");
        $('#unitPrice').css("background", "#f08080");
        $('#unitPrice').tooltip('show');
    }
});

$(document).on('blur', '#totalPrice', function(e) {
    var totalPrice = $('#totalPrice').val();
    var decCount = totalPrice.replace(/[^.]/g, "").length;
    var decIndex = totalPrice.indexOf('.');
    var isCorrect = true;


    if (decIndex === -1) {
        if (totalPrice.length > 0) {$('#totalPrice').val(totalPrice + ".00");}
        isCorrect = true;
    }
    else if (decIndex > 0) {
        if (decCount === 1) {
            var totalLength = totalPrice.length - 1;

            if (totalLength - decIndex > 2) { isCorrect = false; }
            else if (totalLength - decIndex === 1) {
                $('#totalPrice').val(totalPrice + "0");
                isCorrect = true;
            } else if (totalLength - decIndex === 0) {
                $('#totalPrice').val(totalPrice + "00");
                isCorrect = true;
            } else {isCorrect = true;}
        } else if (decCount >= 2) { isCorrect = false; }
    }
    else if (decIndex === 0) {
        var totalLength = totalPrice.length - 1;

        if (totalLength <= 1) { isCorrect = false; }
        else {
            $('#totalPrice').val("0" + totalPrice);
            isCorrect = true;
        }
    }
    else { isCorrect = false; }

    if (isCorrect) {
        $('#confirmPrintBtn').removeAttr("disabled");
        $('#modalClose').removeAttr("disabled");
        $('#modalOk').removeAttr("disabled");
        $('#totalPrice').css("background", "ghostwhite");
        $('#totalPrice').tooltip('hide');
    }else{
        $('#confirmPrintBtn').attr("disabled", "disabled");
        $('#modalClose').attr("disabled", "disabled");
        $('#modalOk').attr("disabled", "disabled");
        $('#totalPrice').css("background", "#f08080");
        $('#totalPrice').tooltip('show');
    }
});

function saveOffloadItemDetails(key) {
    var value = jsonIds[key - 1];
    var newDetails = stringifyDetails(false);

    $.get("api/Offload/UpdateOffloadItem", {id:value, details:newDetails }, function(data) {});
}

function loadOffloadItemDetails(key) {
    var value = jsonIds[key - 1];

    $.get("api/Offload/GetOffloadItemDetails", { id: value }, function (data) {
        $('#NSN').val(data.details[0].NSN);
        $('#CAGE').val(data.details[0].CAGE);
        $('#itemNomen').val(data.details[0].ItemNomen);
        $('#quantity').val(data.details[0].Quant);
        $('#UI').val(data.details[0].UnitIss);
        $('#addData4').val((data.details[0].AddData4).split('|').join('\n'));
        $('#docNumber').val(data.details[0].DocNum);
        $('#docIdent').val(data.details[0].Dociden);
        $('#riFrom').val(data.details[0].RIFrom);
        $('#mAndS').val(data.details[0].MandS);
        $('#SVC').val(data.details[0].SER);
        $('#suppAddress').val(data.details[0].SupAdd);
        $('#SIG').val(data.details[0].Sig);
        $('#fund').val(data.details[0].Fund);
        $('#dist').val(data.details[0].Distribution);
        $('#project').val(data.details[0].Project);
        $('#PRI').val(data.details[0].Pri);
        $('#reqdDelDate').val(data.details[0].RecdDelDate);
        $('#ADV').val(data.details[0].Adv);
        $('#RI').val(data.details[0].Ri);
        $('#OP').val(data.details[0].OP);
        $('#MGT').val(data.details[0].MGT);
        $('#CC').val(data.details[0].Cond);
        $('#unitPrice').val(data.details[0].UnitPrice);
        $('#totalPrice').val(data.details[0].TotalPrice);
        $('#markFor').val(data.details[0].MarkFor);
        $('#NMFC').val(data.details[0].NMFC);
        $('#frtRate').val(data.details[0].FRTRate);
        $('#typeCargo').val(data.details[0].TypeCargo);
        $('#PS').val(data.details[0].PS);
        $('#qtyRecd').val(data.details[0].QtyRecd);
        $('#UP').val(data.details[0].UP);
        $('#unitWeight').val(data.details[0].UnitWeight);
        $('#unitCube').val(data.details[0].UnitCube);
        $('#UFC').val(data.details[0].UFC);
        $('#SL').val(data.details[0].SI);
        $('#freightNomen').val(data.details[0].FreightClassNomen);
        $('#tyCont').val(data.details[0].TYCount);
        $('#noCont').val(data.details[0].NOCount);
        $('#totalWeight').val(data.details[0].TotalWeight);
        $('#totalCube').val(data.details[0].TotalCube);
        $('#recdBy').val(data.details[0].RecdBy);
        $('#recdDate').val(data.details[0].DateRecd);
        $('#RIC').val(data.details[0].RIC);
        $('#addData1').val(data.details[0].AddData1);
        var docDate = data.details[0].DocDate.split("T");
        $('#docDate').val(docDate[0]);
    });
}

function printOffloadItemDetails() {

    var printDetails = stringifyDetails(true);
    //printDetails = printDetails.replace(",","-");
    var uic = $('#select2-chosen-1').text();
    var $modal = $('#dd1348Modal');
    $.get("api/Offload/PrintOffloadItem", {uic:uic, details:printDetails }, function(data) {
        window.open(data.relPath);
        $modal.modal('hide');
    });
}

function stringifyDetails(isBlank) {
    var details = "";
    details += $('#docNumber').val() + "|"; //0
    details += $('#docIdent').val() + "|"; //1
    details += $('#riFrom').val() + "|"; //2
    details += $('#mAndS').val() + "|"; //3
    details += $('#SVC').val() + "|"; //4
    details += $('#suppAddress').val() + "|"; //5
    details += $('#SIG').val() + "|"; //6
    details += $('#fund').val() + "|"; //7
    details += $('#dist').val() + "|"; //8
    details += $('#project').val() + "|"; //9
    details += $('#PRI').val() + "|"; //10
    details += $('#reqdDelDate').val() + "|"; //11
    details += $('#ADV').val() + "|"; //12
    details += $('#RI').val() + "|"; //13
    details += $('#OP').val() + "|"; //14
    details += $('#MGT').val() + "|"; //15
    details += $('#CC').val() + "|"; //16
    details += $('#unitPrice').val() + "|"; //17
    details += $('#totalPrice').val() + "|"; //18
    details += $('#markFor').val() + "|"; //19
    details += $('#NMFC').val() + "|"; //20
    details += $('#frtRate').val() + "|"; //21
    details += $('#typeCargo').val() + "|"; //22
    details += $('#PS').val() + "|"; //23
    details += $('#qtyRecd').val() + "|"; //24
    details += $('#UP').val() + "|"; //25
    details += $('#unitWeight').val() + "|"; //26
    details += $('#unitCube').val() + "|"; //27
    details += $('#UFC').val() + "|"; //28
    details += $('#SL').val() + "|"; //29
    details += $('#freightNomen').val() + "|"; //30
    details += $('#tyCont').val() + "|"; //31
    details += $('#noCont').val() + "|"; //32
    details += $('#totalWeight').val() + "|"; //33
    details += $('#totalCube').val() + "|"; //34
    details += $('#recdBy').val() + "|"; //35
    details += $('#recdDate').val() + "|"; //36 
    details += $('#RIC').val() + "|"; //37
    details += $('#addData1').val() + "|"; //38
    details += $('#CAGE').val() + "|"; //39
    details += $('#itemNomen').val() + "|"; //40 
    details += $('#UI').val() + "|"; //41
    details += $('#addData4').val() + "|"; //42
    details += $('#quantity').val() + "|"; //43
    if (isBlank) {
        details += $('#FSC').val() + "|"; //44
        details += $('#NIIN').val() + "|"; //45
        details += $('#docDate').val() + "|"; //46
    }

    return details;
}
