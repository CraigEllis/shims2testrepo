﻿/**
 * This script starts a timeout that scans the DOM For elements with .datepicker class and attaches
 * the bootstrap 3 datepicker to them.  Will not attach more than once
 */
$(function () {
    function scan() {
        $('.datepickerattach').each(function () {
            if (!$(this).is('[data-pickerattached]')) {
                $(this).datetimepicker({
                    pickTime : false
                });
                $(this).attr('data-pickerattached', 1);
            }
        });
        setTimeout(scan, 500);
    }
    scan();
});