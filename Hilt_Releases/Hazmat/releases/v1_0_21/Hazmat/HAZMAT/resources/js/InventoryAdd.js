﻿var doSubmitSFR;

$(document).on('click', '#btnAddNewInventory', function (e) {
    e.preventDefault();
    $('#add_inv_niin').select2("val", null);
    updateAddModal($('#add_inv_niin').val(null));
    $('#successLabelAddInv').text("");
    $('#AddInventoryModal').modal('show');
})

$(document).ready(function () {
    $.get("api/Inventory/LoadAddDropdowns", function (data) {
        var niins = [];
        for (var i = 0; i < data.niins.length; i++) {
            niins.push({ id: data.niins[i], text: data.niins[i] });
        }
        var option = '';
        var usageCategories = $('#add_inv_usage_category');
        usageCategories.append('<option></option>');
        for (var i = 0; i < data.usageCategories.length; i++) {
            option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + ' - ' + data.usageCategories[i].description + '</option>';
        }
        usageCategories.append(option);
        var option = '';
        var shelfLifeCodes = $('#add_inv_slc');
        shelfLifeCodes.append('<option></option>')
        for (var i = 0; i < data.shelfLifeCodes.length; i++) {
            option += '<option value="' + data.shelfLifeCodes[i].shelf_life_code_id + '">' + data.shelfLifeCodes[i].slc + ' - ' + data.shelfLifeCodes[i].description + '</option>';
        }
        shelfLifeCodes.append(option);
        var option = '';
        var hccs = $('#add_inv_hcc');
        hccs.append('<option></option>');
        for (var i = 0; i < data.hccs.length; i++) {
            option += '<option value="' + data.hccs[i].hcc_id + '">' + data.hccs[i].hcc + ' - ' + data.hccs[i].description + '</option>';
        }
        hccs.append(option);
        var option = '';
        var uis = $('#add_inv_ui');
        uis.append('<option></option>');
        for (var i = 0; i < data.uis.length; i++) {
            option += '<option value="' + data.uis[i].ui_id + '">' + data.uis[i].abbreviation + ' - ' + data.uis[i].description + '</option>';
        }
        uis.append(option);
        var option = '';
        var slacs = $('#add_inv_slac');
        slacs.append('<option></option>');
        for (var i = 0; i < data.slacs.length; i++) {
            option += '<option value="' + data.slacs[i].shelf_life_action_code_id + '">' + data.slacs[i].slac + ' - ' + data.slacs[i].description + '</option>';
        }
        slacs.append(option);
        var option = '';
        var smccs = $('#add_inv_smcc');
        smccs.append('<option></option>');
        for (var i = 0; i < data.smccs.length; i++) {
            option += '<option value="' + data.smccs[i].smcc_id + '">' + data.smccs[i].smcc + ' - ' + data.smccs[i].description + '</option>';
        }
        smccs.append(option);
        $("#add_inv_ui").select2({ allowClear: true });
        $("#add_inv_slc").select2({ allowClear: true });
        $("#add_inv_slac").select2({ allowClear: true });
        $("#add_inv_smcc").select2({ allowClear: true });
        $("#add_inv_hcc").select2({ allowClear: true });
        $("#add_inv_niin").select2(
            {
                allowClear: true,
                createSearchChoice: function (term, data) {
                    if ($(data).filter(function () {
                        return this.text.localeCompare(term) === 0;
                    }).length === 0) {
                        return { id: term, text: term };
                    }
                },
                multiple: false,
                data: niins
            });
        $('#s2id_autogen13_search').prop('maxLength', 9);
    });
});

$(document).on('change', '#add_inv_niin', function (e) {
    e.preventDefault();
    if ($('#add_inv_niin').val().length != 9)
    {
        alert("NIIN must be nine characters long!");
        return false;
    }
    $('#successLabelAddInv').text("");
    updateAddModal($('#add_inv_niin').val());
})

function updateAddModal(niin) {
    $.get("api/Inventory/getFilteredDropdowns?niin=" + $('#add_inv_niin').val(), function (data) {
        cageList = data.cages;
        manufacturerList = data.manufacturers;
        if (userRole != "BASE USER") {
            var table = $('#addInvDetailsTable').DataTable();
            table.ajax.reload();
            addEditor.field('manufacturer').update(manufacturerList);
        }
        //attempt  to populate data from SMCL
        $.get("api/Inventory/AttemptToPopulateInventoryAdd?niin=" + $('#add_inv_niin').val(), function (data) {
            if (!data) {
                $('#sfrNotificationModal').modal('show');
            }
            else {
                populateAddInvDetails(data);
            }
        });
    });
}

$(document).on('change', '.smcl-field', function (e) {
    e.preventDefault();
})

function populateAddInvDetails(data) {
    if (data == "new") {
        $('#add_inv_usage_category').val(5); //not yet evaluated
        alert('This item is not a part of the current SMCL and does not have a current SFR. If you add it to the inventory, an SFR will be automatically started for you and stored in the SFR table.');
        doSubmitSFR = true;
        return;
    }
    else {
        doSubmitSFR = false;
        if (data.smclData.usage_category_id == 1 || data.smclData.usage_category_id == 11 || data.smclData.usage_category_id == 13 || data.smclData.usage_category_id == 15 || data.smclData.usage_category_id == 18) {
            alert("This NIIN is prohibited by the SMCL! Do not procure. Offload any existing stock as soon as possible.");
        }
        if (data.smclData.usage_category_id == 2 || data.smclData.usage_category_id == 12 || data.smclData.usage_category_id == 17) {
            alert("This NIIN is restricted and requires authorization to be procured.");
        }
        var $modal = $('#AddInventoryModal');
        $modal.find('#add_inv_FSC_label').val(data.smclData.fsc);
        $modal.find('#add_inv_description_label').val(data.smclData.name);
        $modal.find('#inv_ui_label').val(data.smclData.ui); 
        $modal.find('#add_inv_um_label').val(data.smclData.um);
        $modal.find('#add_inv_spmig_label').val(data.smclData.spmig);
        $modal.find('#add_inv_specs_label').val(data.smclData.specs);
        $modal.find('#add_inv_remarks_text').val(data.smclData.remarks);
        $modal.find('#add_inv_notes_text').val(data.smclData.notes);
        $modal.find('#add_inv_usage_category').val(data.smclData.usage_category_id);
        $modal.find('#add_inv_slc').select2("val", data.smclData.slc_id);
        $modal.find('#add_inv_slac').select2("val", data.smclData.slac_id);
        $modal.find('#add_inv_hcc').select2("val", data.smclData.hcc_id);
        $modal.find('#add_inv_smcc').select2("val", data.smclData.smcc_id);
        $modal.find('#lbl_add_inventory_detail_id').val(data.smclData.inventory_detail_id);
        //disable all non-blank fields - SMCL data is definitive if available, otherwise allow editing
        $('.smcl-field').each(function () {
            if ($(this).is("select")) {
                if ($(this).val() != null && $(this).val() != "") {
                    $(this).select2('disable');
                }
                else {
                    $(this).select2('enable');
                }
            }
            else {
                if (this.value != null && this.value != "") {
                    this.disabled = true;
                }
                else {
                    this.disabled = false;
                }
            }
        })

        $modal.find('#add_inv_total_qty').val(data.totalQuantity);
        $modal.find('#add_inv_qty_allowed').val(data.smclData.allowance_quantity);
    }
        //only users above the bottom level (user) can update stuff
        if (!(data.userRole === "BASE USER")) {
            $('#add_inv_qty_allowed').prop("disabled", false);
            $('#add_inv_notes_text').prop("disabled", false);
        }
        else {
            $('#add_inv_qty_allowed').prop("disabled", true);
            $('#add_inv_slc').prop("disabled", true);
            $('#add_inv_slac').prop("disabled", true);
            $('#add_inv_hcc').prop("disabled", true);
            $('#add_inv_ui_label').prop("disabled", true);
            $('#add_inv_um_label').prop("disabled", true);
            $('#add_inv_notes_text').prop("disabled", true);
        }

        if (userRole != "BASE USER") {
            var table = $('#addInvDetailsTable').DataTable();
            table.ajax.reload();
            addEditor.field('manufacturer').update(manufacturerList);
        }
        else {
            $('.invTable').css('visibility', 'hidden');
        }
}

$(document).on('click', '#btn_addInvItem', function (e) {
    e.preventDefault();
    doSubmitSFR = false; //we will never need to do the SFR here - it happens when you add a row to the inventory table, which is mandatory
    AddUpdateInventoryInfo(false);
});

function AddUpdateInventoryInfo(needsTableUpdate) {
    var table = $('#addInvDetailsTable').dataTable();
    var tableData = table.fnGetData();
    var newId = $('#lbl_add_inventory_detail_id').val();
    var newFsc = $('#add_inv_FSC_label').val();
    var newNiin = $('#add_inv_niin').val();
    var newName = $('#add_inv_description_label').val();
    var newUI = $('#add_inv_ui_label').val();
    var newUM = $('#add_inv_um_label').val();
    var newSpmig = $('#add_inv_spmig_label').val();
    var newSpecs = $('#add_inv_specs_label').val();
    var newSLC = $('#add_inv_slc').val();
    var newSLAC = $('#add_inv_slac').val();
    var newHCC = $('#add_inv_hcc').val();
    var newSmcc = $('#add_inv_smcc').val();
    var newNotes = $('#add_inv_notes_text').val();
    var newUsageCategory = $('#add_inv_usage_category').val();
    var newQtyAllowed = $('#add_inv_qty_allowed').val();
    if (((newQtyAllowed != parseInt(newQtyAllowed, 10)) || (newQtyAllowed < 0)) && newQtyAllowed != "") {
        $('#successLabelAddInv').text("Quantity allowed must be a positive whole number.");
        $('#successLabelAddInv').css('color', 'red');
        return;
    }
    if (!newFsc || !newNiin) {
        $('#successLabelAddInv').text("You must enter an FSC and a NIIN.");
        $('#successLabelAddInv').css('color', 'red');
    }
    var jsonData = { niin: newNiin, fsc: newFsc, usage_category: newUsageCategory, ui: newUI, um: newUM, slc: newSLC, slac: newSLAC, HCC: newHCC, notes: newNotes, qtyAllow: newQtyAllowed, id: newId, name: newName, spmig: newSpmig, specs: newSpecs, smcc: newSmcc };
    return $.ajax({
        type: 'POST',
        url: "api/Inventory/UpdateInvItem",
        data: JSON.stringify(jsonData),
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            if (result > 0) {
                $('#lbl_add_inventory_detail_id').val(result);
            }
            if (doSubmitSFR) {
                $.ajax({
                    type: 'POST',
                    url: "api/Inventory/AddSfr",
                    data: { niin: newNiin, fsc: newFsc, ui: newUI, item_name: newName, inventory_detail_id: result },
                    success: function (data) {
                        doSubmitSFR = false;
                    }
                });
            }
            $('#successLabelAddInv').text("Update successful!");
            $('#successLabelAddInv').css('color', 'green');
        }
    });
}
