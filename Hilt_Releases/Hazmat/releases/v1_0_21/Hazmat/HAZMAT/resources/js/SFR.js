﻿$(document).ready(function () {
    $('#sfrTable').dataTable({
        "ajax": {
            "url": "/api/SFR/LoadSFRTable",
            "type": "GET"
        },
        "columnDefs": [
        { className: "editable", "targets": [5] },
        {
            "targets": [8],
            "visible": false,
            "searchable": false
        }],
        "columns": [
            { "data": "fsc" },
            { "data": "niin" },
            { "data": "cage" },
            { "data": "item_name" },
            { "data": "date_submitted" },
            { "data": "date_mailed" },
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btn-SFR-details" value="SFR Details" data-sfr-id="' + row.sfr_id + '"/>';
                }
            },
            {
               "mRender": function (data, type, row) {
                    return '<a href= "api/sfr/getsfrpdf?sfr_id='+row.sfr_id+'"><input type="button" class="btn btn-info btn-sm btn-details" value="PDF" data-sfr-id="'+row.sfr_id+'"/></a>';
               }
            },
            { "data": "sfr_id" }, ]
    });

    sfrEditor = new $.fn.dataTable.Editor({
        ajax: '/api/SFR/UpdateSFRStatus',
        table: '#sfrTable',
        idSrc: 'sfr_id',
        fields: [{
            label: "Date Mailed",
            name: "date_mailed",
            type: "date",
        }]
    });

    $('#sfrTable').on('click', '.editable', function (e) {
        sfrEditor.inline(this, { submitOnBlur: true });
    });

    //grab ui list and ship details
    $.get("api/SFR/GetBlankSFRModal", function (data) {
        var option = '';
        var uis = $('#sfr_ui');
        for (var i = 0; i < data.uis.length; i++) {
            option += '<option value="' + data.uis[i].ui_id + '">' + data.uis[i].abbreviation + ' - ' + data.uis[i].description + '</option>';
        }
        uis.append(option);
        $('#hullLabel').text(data.shipData[0].hull_number);
        $('#uicLabel').text(data.shipData[0].uic);
        $('#shipPocLabel').text(data.shipData[0].POC_Name);
        $('#telephoneLabel').text(data.shipData[0].POC_Telephone);
    });
});

$(document).on('click', '#btnBlankSFR', function (e) {
    e.preventDefault();
    sfr_id = null;
    $('#sfrSuccessLabel').text('');
    $('.sfr').val('');
    $('#SFRModal').modal('show');
});

$(document).on('click', '#submitSFR', function (e) {
    e.preventDefault();
    if (!$('#sfr_niin').val()) {
        alert('You must enter a NIIN.');
        return false;
    }
    var sfr = new Object();
    $('.sfr').each(function (i, obj) {
        var type = $(this).prop('type');
        if (type == "checkbox") {
            sfr[$(this).prop('name')] = $(this).prop('checked');
        }
        else {
            sfr[$(this).prop('name')] = $(this).val();
        }
    });
    if (sfr_id) {
        sfr["sfr_id"] = sfr_id;
    }
    $.ajax({
        type: 'POST',
        url: "api/SFR/UpdateSFR",
        data: sfr,
        success: function (data) {
            if (data == "sfrAdded" || data == "sfrUpdated") {
                var table = $('#sfrTable').DataTable()
                table.ajax.reload();
                $('#sfrSuccessLabel').text("Update successful!");
            }
            else {
                alert("You already have an SFR with this NIIN. Please edit the current SFR instead of adding a new one.");
            }
        }
    });
});

$(document).on('click', '.btn-SFR-details', function (e) {
    $('#sfrSuccessLabel').text('');
    sfr_id = $(this).attr('data-sfr-id');
    var modal = $('#SFRModal');
    $('.sfr').val(null);
    $.get("api/SFR/GetSFRById?sfr_id=" + sfr_id, function (data) {
        $.each(data.data, function (name, val) {
            var $el = $('[name="' + name + '"]'),
                type = $el.attr('type');
            switch (type) {
                case 'checkbox':
                    $el.attr('checked', val);
                    break;
                default:
                    $el.val(val);
            }
        });
        modal.modal('show');
    });
});