﻿var sort = "all";
var type = " ";

$(document).ready(function () {
    $.get("api/MasterUpdate/LoadDropdowns", function (data) {
        var option;
        option += "<option></option>";
        var locations = $('#filterLocation');
        for (var i = 0; i < data.locations.length; i++) {
            option += '<option value="' + data.locations[i].location_id + '">' + data.locations[i].name + '</option>';
        }
        locations.append(option);
        option = '';
        option += "<option></option>";
        var workcenters = $('#filterWorkCenter');
        for (var i = 0; i < data.workcenters.length; i++) {
            option += '<option value="' + data.workcenters[i].workcenter_id + '">' + data.workcenters[i].wid + '</option>';
        }
        workcenters.append(option);
        loadTable();
    });
    $("#filterLocation").select2({ allowClear: true });
    $("#filterWorkCenter").select2({ allowClear: true });
});

function loadTable() {
    editor = new $.fn.dataTable.Editor({
        ajax: '/api/MasterUpdate/MasterUpdate',
        table: '#masterUpdateTable',
        "dateImage": '/resources/css/calender.png',
        idSrc: 'id',
        fields: [
            {
                label: "FSC",
                name: "fsc"
            },
            {
                label: "NIIN",
                name: "niin"
            },
            {
                label: "Work Center",
                name: "wid"
            },
            {
                label: "Location",
                name: "location_name"
            },
            {
                label: "Qty",
                name: "quantity"
            },
             {
                 label: "Expiration Date",
                 name: "shelf_life_date",
                 type: "date",
                 "dateImage": '/resources/css/calender.png',
                 dateFormat: 'mm/dd/yy'
             }, {
                 label: "Inventoried Date",
                 name: "inv_date",
                 type: "date",
                 dateFormat: 'mm/dd/yy'
             }
        ]
    });

    editor.on('preSubmit', function(e, data, action)
    {
        var now = new Date();
        var strDateTime = [AddZero(now.getMonth() + 1), AddZero(now.getDate()), now.getFullYear()].join("/");

        function AddZero(num) {
            return (num >= 0 && num < 10) ? "0" + num : num + "";
        }
        data.data.inv_date = strDateTime;
    });

    $('#masterUpdateTable').on('click', '.editable', function (e) {
        editor.inline(this, { submitOnBlur: true });
    });

    $('#masterUpdateTable').on('click', '.bubble', function (e) {
        editor.bubble(this);
    });

    $('#masterUpdateTable').dataTable({
        dom: "Tfrtip",
        "ajax": {
            "url": "/api/MasterUpdate/LoadMasterTable",
            "type": "GET",
            "data": function (d) {
                d.location = $("#filterLocation").val();
                d.workcenter = $("#filterWorkCenter").val();
            }
        },
        "columnDefs": [
          { className: "editable", "targets": [4] },
          { className: "bubble", "targets": [5] }
        ],
        "columns": [
            { "data": "fsc" },
            { "data": "niin" },
            { "data": "wid" },
            { "data": "location_name" },
            { "data": "quantity" },
            { "data": "shelf_life_date" },
            { "data": "inv_date" }
        ],
        tableTools: {
            sRowSelect: "os",
            "aButtons": []
        }
    });
}

$(document).on('change', '#filterLocation', function () {
    var table = $('#masterUpdateTable').DataTable();
    table.ajax.reload();
    var location = $('#filterLocation').val();

    if (location != ' ') { type = 'location'; sort = location; }
    else { type = 'all'; sort = ' '; }
});

$(document).on('change', '#filterWorkCenter', function () {
    var table = $('#masterUpdateTable').DataTable();
    table.ajax.reload();
    var workcenter = $('#filterWorkCenter').val();

    if (workcenter != ' ') { type = 'workcenter'; sort = workcenter; }
    else { type = 'all'; sort = ' '; }
});

$(document).on('click', '#printAll', function (e) {
    e.preventDefault();
    type = "all";
    var signatures = " ";
    $.get("api/MasterUpdate/GenerateChecklist", { type: type, sort: sort }, function (data) {
        window.open(data);
    });
});

