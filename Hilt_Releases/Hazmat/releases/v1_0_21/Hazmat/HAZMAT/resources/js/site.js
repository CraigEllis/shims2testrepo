﻿function populateSMCLDetails(niin) {
    $.get("api/HomeScreen/GetModal?niin=" + niin, function (data) {
        $('#successLabel').text('');
        var $modal = $('#SMCLDetails');
        $modal.find('#fscTxt').val(data.aaData[0].fsc);
        $modal.find('#niinTxt').val(data.aaData[0].niin);
        $modal.find('#NomenclatureTxt').val(data.aaData[0].description);
        $modal.find('#dd_usagecategory').val(data.aaData[0].usage_category_id);
        $modal.find('#UITxt').val(data.aaData[0].ui);
        $modal.find('#UMTxt').val(data.aaData[0].um);
        $modal.find('#SPECSTxt').val(data.aaData[0].specs);
        $modal.find('#SPMIGTxt').val(data.aaData[0].spmig);
        $modal.find('#CAGETxt').val(data.aaData[0].cage);
        $modal.find('#mfgTxt').val(data.aaData[0].manufacturer);
        $modal.find('#NMCPHCTxt').val(data.aaData[0].NMCPHC);
        if (data.aaData[0].hcc != null && data.aaData[0].hcc_description != null) {
            $modal.find('#hccTxt').val(data.aaData[0].hcc + ' - ' + data.aaData[0].hcc_description);
        }
        $modal.find('#dd_smcc').val(data.aaData[0].smcc_id);
        $modal.find('#msdsSerNumtxt').val(data.aaData[0].msds_num);
        $modal.find('#remarksTxt').val(data.aaData[0].remarks);
        $modal.modal('show');
    });
}

function openReportModal() {
    var $modal = $('#reportsModal');
    $('#invTab').trigger("click");
    $modal.modal('show');
}

function openReportsModalFromNav(tab) {
    var $modal = $('#reportsModal');
    $modal.modal('show');
    $(tab).trigger("click");
}

function openShipAdminModal() {
    var $modal = $('#shipManagementModal');
    $('#updateShipSuccessLabel').text("");
    $modal.modal('show');
}

function openSmclUpdate() {
    var $modal = $('#smclUpdateModal');
    getLastIncUpdate();
    getLastUpdate();
    $('#lblSmclUpdateSuccess').text("");
    $('#lblIncUpdateSuccess').text("");
    $modal.modal('show');
}

function openDbModal() {
    var $modal = $('#dbModal');
    $('#lblDbUpdateSuccess').text("");
    $modal.modal('show');
}

function openMSDSImport() {
    var $modal = $('#MSDSImportModal');
    $('#lblMSDSImportSuccess').text("");
    $modal.modal('show');
}