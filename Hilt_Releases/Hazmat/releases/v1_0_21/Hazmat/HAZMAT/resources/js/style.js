﻿/**
 * This file contains style specific javascript functionality that works around
 * some of the limitations of old webforms apps, to bring it up to speed for 
 * Bootstrap 3
 */
$(document).ready(function () {
    $('ul.AspNet-Menu').removeClass('AspNet-Menu');
    $('li.AspNet-Menu-Leaf').removeClass('AspNet-Menu-Leaf');
    $('li.AspNet-Menu-Selected').addClass('active');
    $('.AspNet-Menu-Horizontal > ul').addClass('nav nav-tabs');
    $('#main-nav-horizontal > li').removeClass('active');
    $('#main-nav-horizontal li a[href="' + window.location.pathname + '"]').closest('.tl').addClass('active');
    $('.queryBox').css('width', '130px');
    $('.autoCompleteWrapper').css('width', '135px');
    $('.list').css('width', '135px');
    $('.listOption').css('width', '135px');
});