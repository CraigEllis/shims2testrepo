﻿var workcenterList = [""];


$(document).ready(function () {
    //grab workcenter list
    $.get("api/UserAdmin/GetWorkcenters", function (data) {
        for (var i = 0; i < data.workcenters.length; i++) {
            var thing = { label: data.workcenters[i].wid, value: data.workcenters[i].workcenter_id };
            workcenterList.push(thing);
        }
        loadTables();
    });
});

function loadTables() {
    userEditor = new $.fn.dataTable.Editor({
        ajax: '/api/UserAdmin/UpdateUser',
        table: '#userTable',
        idSrc: 'user_id',
        fields: [{
            label: "User Name",
            name: "username"
        },
        {
            label: "Role",
            name: "role",
            type: "select",
            ipOpts: ["LEVEL 1", "LEVEL 2", "LEVEL 3"]
        },
        {
            label: "Workcenter Name",
            name: "workcenter.workcenter_id",
            type: "select",
            ipOpts: workcenterList
        }
        ]
    });

    $('#userTable').on('init.dt', function () {
        $('#line').after($('#ToolTables_userTable_0'));
    }).DataTable({
        dom: "Trftip",
        "ajax": {
            "url": "/api/UserAdmin/LoadUserTable",
            "type": "GET"
        },
        "columnDefs": [
        { className: "editable", "targets": [0, 1] },
        { className: "workcenter", "targets":  [2] },

        {
            "targets": [4],
            "visible": false,
            "searchable": false
        }],
        "columns": [
            { "data": "username" },
            { "data": "role" },
            { "data": "workcenter.workcenter_name" },
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btnDeleteUser" value="Delete" data-user-id="' + row.user_id + '"/>';
                }
            },
            { "data": "user_id" }],
        tableTools: {
            sRowSelect: "os",
            "aButtons": [
            { sExtends: "editor_create", editor: userEditor },
            ]
        }
    });

    $('#userTable').on('click', '.editable', function (e) {
        userEditor.inline(this, { submitOnBlur: true });
    });

    $('#userTable').on('click', '.workcenter', function (e) {
        userEditor.inline(this, 'workcenter.workcenter_id', { submitOnBlur: true });
    });

    userEditor.hide('workcenter.workcenter_id');


    $(document).on('change', '#DTE_Field_role', function (e) {
        if (userEditor.get('role') == "LEVEL 2") {
            userEditor.show('workcenter.workcenter_id');
        }
        else {
            userEditor.hide('workcenter.workcenter_id');
        }
    });

}

$(document).on('click', '.btnDeleteUser', function (e) {
    user_id = $(this).attr('data-user-id');
    $.ajax({
        type: 'DELETE',
        url: "api/UserAdmin/DeleteUser?user_id="+user_id,
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            var table = $('#userTable').DataTable()
            table.ajax.reload();
        }
    });
});