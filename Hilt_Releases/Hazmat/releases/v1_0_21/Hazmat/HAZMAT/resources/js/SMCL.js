﻿var msdsList = [];
var msdsCount = 0;
var currentMSDS = 1;

$(document).ready(function () {
    loadSMCLTable();
    $.get("api/SMCL/LoadInitialScreen", function (data) {
        option = '';
        var usageCategories = $('#dd_usagecategory');
        for (var i = 0; i < data.usageCategories.length; i++) {
            option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + ' - ' + data.usageCategories[i].description + '</option>';
        }
        option = '';
        option += "<option></option>";
        var usageCategories = $('#filterACM');
        for (var i = 0; i < data.usageCategories.length; i++) {
            option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + '</option>';
        }
        usageCategories.append(option);
        option = '';
        var smccs = $('#dd_smcc');
        for (var i = 0; i < data.smccs.length; i++) {
            option += '<option value="' + data.smccs[i].smcc_id + '">' + data.smccs[i].smcc + ' - ' + data.smccs[i].description + '</option>';
        }
        smccs.append(option);
        option = '';
        option += "<option></option>";
        var niins = $('#filterNiinList');
        for (var i = 0; i < data.niins.length; i++) {
            option += '<option value="' + data.niins[i] + '">' + data.niins[i] + '</option>';
        }
        niins.append(option);
        option = '';
        option += "<option></option>";
        var descriptions = $('#filterItemName');
        for (var i = 0; i < data.descriptions.length; i++) {
            option += '<option value="' + data.descriptions[i] + '">' + data.descriptions[i] + '</option>';
        }
        descriptions.append(option);
        option = '';
        option += "<option></option>";
        var specs = $('#filterSpecs');
        for (var i = 0; i < data.specs.length; i++) {
            option += '<option value="' + data.specs[i] + '">' + data.specs[i] + '</option>';
        }
        specs.append(option);
        option = '';
        option += "<option></option>";
        var tablenames = $('#filterTableName');
        for (var i = 0; i < data.groups.length; i++) {
            option += '<option value="' + data.groups[i].catalog_group_id + '">' + data.groups[i].group_name + '</option>';
        }
        tablenames.append(option);
        option = '';
        option += "<option></option>";
        var manufacturers = $('#filterManufacturer');
        for (var i = 0; i < data.manufacturers.length; i++) {
            option += '<option value="' + data.manufacturers[i] + '">' + data.manufacturers[i] + '</option>';
        }
        manufacturers.append(option);
    });
    $("#filterNiinList").select2({ allowClear: true });
    $("#filterItemName").select2({ allowClear: true });
    $("#filterSpecs").select2({ allowClear: true });
    $("#filterTableName").select2({ allowClear: true });
    $("#filterManufacturer").select2({ allowClear: true });
    $("#filterACM").select2({ allowClear: true });
});
$(document).on('click', '.btn-smclDetails', function (e) {
    e.preventDefault();
    var niin = $(this).attr('data-niin');
    populateSMCLDetails(niin);
});
$(document).on('click', '#btn-MSDS', function (e) {
    e.preventDefault();
    var fsc = $(this).attr('data-fsc');
    var niin = $(this).attr('data-niin');
    var cage = $(this).attr('data-cage');
    $.get("api/SMCL/getMSDSFiles", { fsc: fsc, niin: niin, cage: cage }, function (data) {

        msdsCount = data.length;
        if (msdsCount <= 1) {
            $('#msdsLeft').attr("disabled", "disabled");
            $('#msdsRight').attr("disabled", "disabled");
        }
        else {
            $('#msdsLeft').removeAttr("disabled");
            $('#msdsRight').removeAttr("disabled");
        }
        if (msdsCount > 0) {
            $('#noMsds').hide();
            $('.item-count').show();
            for (var i = 0; i < data.length; i++) {
                //window.open(data[i]);
                if (i == 0) {
                    currentMSDS = 1;
                    $('#currentMSDS').text(currentMSDS);
                    $('#totalMSDS').text(msdsCount);

                    var splitName = data[currentMSDS - 1].split(".");
                    if (splitName[1] != "PDF" && splitName[1] != "pdf") {
                        $('#msdsPdf').hide();
                        $('#msdsDoc').show();
                        $('#msdsDoc').attr("src", data[currentMSDS - 1]);
                    }
                    else {
                        $('#msdsDoc').hide();
                        $('#msdsPdf').show();
                        $('#msdsPdf').attr("data", data[currentMSDS - 1]);
                    }

                }
                msdsList[i] = data[i];
            }
        }
        else {
            $('#msdsPdf').hide();
            $('#msdsDoc').hide();
            $('.item-count').hide();
            $('#noMsds').show();

            
        }
    });
    var $modal = $('#msdsModal');
    $modal.modal('show');
});

$(document).on('click', '#msdsLeft', function (e) {

    if (currentMSDS > 1 && currentMSDS <= msdsCount) { currentMSDS = parseInt(currentMSDS) - 1; }
    else if (currentMSDS > msdsCount) { currentMSDS = 1; }
    else { currentMSDS = msdsCount; }

    $('#currentMSDS').text(currentMSDS);

    var splitName = msdsList[currentMSDS-1].split(".");
    if (splitName[1] != "PDF" && splitName[1] != "pdf") {
        $('#msdsPdf').hide();
        $('#msdsDoc').show();
        $('#msdsDoc').attr("src", msdsList[currentMSDS-1]);
    }
    else {
        $('#msdsDoc').hide();
        $('#msdsPdf').show();
        $('#msdsPdf').attr("data", msdsList[currentMSDS - 1]);
    }
    //$('#msdsDoc').attr("src", msdsList[currentMSDS]);

});

$(document).on('click', '#msdsRight', function (e) {

    if (currentMSDS >= 1 && currentMSDS < msdsCount) { currentMSDS = parseInt(currentMSDS) + 1; }
    else if (currentMSDS > msdsCount) { currentMSDS = msdsCount; }
    else { currentMSDS = 1; }
    $('#currentMSDS').text(currentMSDS);

    var splitName = msdsList[currentMSDS-1].split(".");
    if (splitName[1] != "PDF" && splitName[1] != "pdf") {
        $('#msdsPdf').hide();
        $('#msdsDoc').show();
        $('#msdsDoc').attr("src", msdsList[currentMSDS-1]);
    }
    else {
        $('#msdsDoc').hide();
        $('#msdsPdf').show();
        $('#msdsPdf').attr("data", msdsList[currentMSDS-1]);
    }
    //$('#msdsDoc').attr("src", msdsList[currentMSDS]);
});

$(document).on('click', '#btn-labels', function (e) {
    e.preventDefault();
    var $modal = $('#labelsModal');
    niinTxt = $(this).attr('data-niin');
    usageCategoryTxt = $(this).attr('data-usage_category');
    descriptionTxt = $(this).attr('data-description');
    $('#lblItemName').val(descriptionTxt);
    $('#lblItemNiin').val(niinTxt);
    //default values
    startingLabelTxt = "1";
    labelCountTxt = "30";
    adjustPdfUrl();
    $modal.modal('show');
});
$(document).on('change', '#labelNumber', function (e) {
    e.preventDefault();
    startingLabelTxt = $("#labelNumber").val();
    labelCountTxt = $("#labelCount").val();
    var labelNumberInt = parseInt(startingLabelTxt);
    if (startingLabelTxt < 1 || startingLabelTxt > 30) {
        $('#errorMsg').css("visibility", "visible");
        return;
    }
    else {
        $('#errorMsg').css("visibility", "hidden");
    }
    adjustPdfUrl();
})
$(document).on('change', '#labelCount', function (e) {
    e.preventDefault();
    startingLabelTxt = $("#labelNumber").val();
    labelCountTxt = $("#labelCount").val();
    if (labelCountTxt == "" || labelCountTxt == null) {
        $('#labelCountError').css("visibility", "visible");
        return;
    }
    else {
        $('#labelCountError').css("visibility", "hidden");
    }
    adjustPdfUrl();
})
$(document).on('click', '#getPDF', function (e) {
    return validatePage();
});

function validatePage() {
    var isValid = true;
    if ($('#labelCount').val() == "" || $('#labelCount').val() == null) {
        isValid = false;
        return isValid;
    }
    labelNumberInt = parseInt($('#labelNumber').val());
    if (labelNumberInt < 1 || labelNumberInt > 30) {
        isValid = false;
    }
    return isValid;
}

function adjustPdfUrl() {
    $('#getPDF').attr('href', function (niin, usageCategory, description, startingLabel, labelCount) {
        var params = {
            niin: niinTxt,
            usageCategory: usageCategoryTxt,
            description: descriptionTxt,
            startingLabel: startingLabelTxt,
            labelCount: labelCountTxt
        };
        var str = $.param(params);
        var url = "api/SMCL/ACMLabels?";
        url += str;
        return (url);
    });
}

$(document).on('change', '.filter', (function (e) {
    var table = $('#smclDataTable').DataTable()
    table.ajax.reload();
}));

function loadSMCLTable() {
    smclTable = $('#smclDataTable').on('init.dt', function () {
        $('#tableCountCell').append($('#smclDataTable_length'));
    }).DataTable({
        "dom": "ltrtip",
        "ajax": {
            "url": "/api/SMCL/LoadSMCLTable",
            "type": "GET",
            "data": function (d) {
                d.niin = $("#filterNiinList").val();
                d.itemName = $("#filterItemName").val();
                d.specs = $("#filterSpecs").val();
                d.tableName = $("#filterTableName").val();
                d.manufacturer = $("#filterManufacturer").val();
                d.ACMCategory = $("#filterACM").val();
            }
        },
        "columns": [
              {
                  "mRender": function (data, type, row) {
                      return '<input type="button" class="btn btn-info btn-sm btn-details" id="btn-MSDS" value="MSDS" data-niin="' + row.niin + '" data-fsc="' + row.fsc + '" data-cage="' + row.cage + '"/>';
                   }
              },
              {
                  "mRender": function (data, type, row) {
                      return '<input type="button" class="btn btn-info btn-sm btn-details" id=btn-labels value="ACM Labels" data-niin="' + row.niin + '" data-description="' + row.description + '" data-usage_category="' + row.category + '"/>';
                  }
              },
            { "data": "fsc" },
            {
              "mRender": function (data, type, row) {
                     return '<a style="cursor: pointer; text-decoration:underline;" class="btn-smclDetails" data-niin="' + row.niin + '">'+row.niin+'</a>';
                 }
            },
            { "data": "description" },
            { "data": "category" },
            { "data": "ui" },
            { "data": "spmig" },
            { "data": "specs" }, 
            { "data": "manufacturer" },
            { "data": "cage" },
            { "data": "part_name" }
        ],
        tableTools: {
            "sRowSelect": "multi",
            "aButtons": []
        }
    });

    $('#searchSMCL').keyup(function () {
        smclTable.search($(this).val()).draw();
    })
}