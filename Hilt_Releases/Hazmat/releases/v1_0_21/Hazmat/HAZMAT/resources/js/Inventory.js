﻿var locationList = [];
var manufacturerList = [];
var cageList = [];
var niin = '000000';
var inventory_id = 0;
var editor;
var addEditor;
var userRole;
var msdsList = [];
var msdsCount = 0;

$(document).ready(function () {
    $.get("api/Inventory/GetUserRole", function (data) {
        userRole = data;
        if (userRole === "LEVEL 1") {
            $('#btn_addInvItem').prop('disabled', false);
        }
        loadDropDowns();
    });
    $("#filterNiinList").select2({ allowClear: true });
    $("#filterItemName").select2({ allowClear: true });
    $("#filterSpecs").select2({ allowClear: true });
    $("#filterLocation").select2({ allowClear: true });
    $("#filterWorkCenter").select2({ allowClear: true });
    $("#filterSPMIG").select2({ allowClear: true });
});

function loadDropDowns() {
    $.get("api/Inventory/LoadDropdown", function (data) {
        var option = '';
        var usageCategories = $('#inv_usage_category');
        for (var i = 0; i < data.usageCategories.length; i++) {
            option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + ' - ' + data.usageCategories[i].description + '</option>';
        }
        usageCategories.append(option);
        var option = '';
        var shelfLifeCodes = $('#inv_slc');
        for (var i = 0; i < data.shelfLifeCodes.length; i++) {
            option += '<option value="' + data.shelfLifeCodes[i].shelf_life_code_id + '">' + data.shelfLifeCodes[i].slc + ' - ' + data.shelfLifeCodes[i].description + '</option>';
        }
        shelfLifeCodes.append(option);
        var option = '';
        var hccs = $('#inv_hcc');
        for (var i = 0; i < data.hccs.length; i++) {
            option += '<option value="' + data.hccs[i].hcc_id + '">' + data.hccs[i].hcc + ' - ' + data.hccs[i].description + '</option>';
        }
        hccs.append(option);
        var option = '';
        var uis = $('#inv_ui');
        for (var i = 0; i < data.uis.length; i++) {
            option += '<option value="' + data.uis[i].ui_id + '">' + data.uis[i].abbreviation + ' - ' + data.uis[i].description + '</option>';
        }
        uis.append(option);
        var option = '';
        var slacs = $('#inv_slac');
        for (var i = 0; i < data.slacs.length; i++) {
            option += '<option value="' + data.slacs[i].shelf_life_action_code_id + '">' + data.slacs[i].slac + ' - ' + data.slacs[i].description + '</option>';
        }
        slacs.append(option);
        var option = '';
        var smccs = $('#inv_smcc');
        for (var i = 0; i < data.smccs.length; i++) {
            option += '<option value="' + data.smccs[i].smcc_id + '">' + data.smccs[i].smcc + ' - ' + data.smccs[i].description + '</option>';
        }
        smccs.append(option);
        option = '';
        //need to add to both SMCL modal and inventory details modal dropdowns
        var usageCategories = $('#dd_usagecategory');
        for (var i = 0; i < data.usageCategories.length; i++) {
            option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + ' - ' + data.usageCategories[i].description + '</option>';
        }
        usageCategories.append(option);
        option = '';
        var smccs = $('#dd_smcc');
        for (var i = 0; i < data.smccs.length; i++) {
            option += '<option value="' + data.smccs[i].smcc_id + '">' + data.smccs[i].smcc + ' - ' + data.smccs[i].description + '</option>';
        }
        smccs.append(option);
        option = '';
        option += "<option></option>";
        var niins = $('#filterNiinList');
        for (var i = 0; i < data.niins.length; i++) {
            option += '<option value="' + data.niins[i] + '">' + data.niins[i] + '</option>';
        }
        niins.append(option);
        option = '';
        option += "<option></option>";
        var descriptions = $('#filterItemName');
        for (var i = 0; i < data.descriptions.length; i++) {
            option += '<option value="' + data.descriptions[i] + '">' + data.descriptions[i] + '</option>';
        }
        descriptions.append(option);
        option = '';
        option += "<option></option>";
        var specs = $('#filterSpecs');
        for (var i = 0; i < data.specs.length; i++) {
            option += '<option value="' + data.specs[i] + '">' + data.specs[i] + '</option>';
        }
        specs.append(option);
        option = '';
        option += "<option></option>";
        var locations = $('#filterLocation');
        for (var i = 0; i < data.locations.length; i++) {
            option += '<option value="' + data.locations[i].location_id + '">' + data.locations[i].name + '</option>';
            var thing = { label: data.locations[i].name, value: data.locations[i].location_id };
            locationList.push(thing);
        }
        locations.append(option);
        option = '';
        option += "<option></option>";
        var workcenters = $('#filterWorkCenter');
        for (var i = 0; i < data.workcenters.length; i++) {
            option += '<option value="' + data.workcenters[i].workcenter_id + '">' + data.workcenters[i].wid + '</option>';
        }
        workcenters.append(option);
        option = '';
        option += "<option></option>";
        var spmigs = $('#filterSPMIG');
        for (var i = 0; i < data.spmigs.length; i++) {
            option += '<option value="' + data.spmigs[i] + '">' + data.spmigs[i] + '</option>';
        }
        spmigs.append(option);
        $("#inv_ui").select2({ allowClear: true });
        $("#inv_slc").select2({ allowClear: true });
        $("#inv_slac").select2({ allowClear: true });
        $("#inv_smcc").select2({ allowClear: true });
        $("#inv_hcc").select2({ allowClear: true });
        loadTables();
    });
}

function loadTables() {
    inventoryTable = $('#inventoryTable').on('init.dt', function () {
        if (userRole != "LEVEL 1") {
            $('.btnDelete').prop('disabled', true);
        }
        $('#tableCountCell').append($('#inventoryTable_length'));
        $('#invDetailsTable').after($('#ToolTables_invDetailsTable_0'));
        $('#addInvDetailsTable').after($('#ToolTables_addInvDetailsTable_0'));
    }).on('draw.dt', function () {
        //disable SMCL button for non-SMCL items
        $('.btn-SMCL').each(function (i, obj) {
            if ($(this).attr('data-smcl') < 1) {
                $(this).prop('disabled', true);
            }
        });
    }).DataTable({
        dom: "ltrtip",
        "ajax": {
            "url": "/api/Inventory/LoadInventoryTable",
            "type": "GET",
            "data": function (d) {
                d.niin = $("#filterNiinList").val();
                d.itemName = $("#filterItemName").val();
                d.specs = $("#filterSpecs").val();
                d.location = $("#filterLocation").val();
                d.workcenter = $("#filterWorkCenter").val();
                d.spmig = $("#filterSPMIG").val();
            }
        },
        "columnDefs": [
    {
        "targets": [6],
        "visible": false,
        "searchable": false
    },
        ],
        "columns": [
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btn-MSDS" value="MSDS" data-niin="' + row.niin + '" data-fsc="' + row.fsc + '" data-cage="' + row.cage + '"/>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btn-SMCL" value="SMCL Details" data-niin="' + row.niin + '" data-smcl="' + row.is_smcl + '"/>';
                }
            },
            { "data": "fsc" },
            {
                "mRender": function (data, type, row) {
                    return '<a style="cursor: pointer; text-decoration:underline;" class="btn-Inv" data-niin="' + row.niin + '"  data-location="' + row.location + '" data-inventory_detail_id="' + row.inventory_detail_id + '" data-inventory_id = "' + row.onhand_id + '">' + row.niin + '</a>';
                 }
            },
            { "data": "item_name" },
            { "data": "workcenter" },
            { "data": "workcenter_id" },
            { "data": "location" },
            { "data": "qty" },
            { "data": "allowance_quantity" },
            { "data": "category" },
            { "data": "ui" },
            { "data": "um" },
            { "data": "expiration_date" },
            { "data": "spmig" },
            { "data": "manufacturer" },
            { "data": "cage" },
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btnDelete" data-inventoryId ="' + row.onhand_id + '" value="Delete"/>';
                }
            },
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btnOffload" data-inventoryId ="' + row.onhand_id + '" value="Offload"/>';
                }
            }],
        tableTools: {
            "sRowSelect": "multi",
            "aButtons": []
        }
    });

    $('#searchInv').keyup(function () {
        inventoryTable.search($(this).val()).draw();
    })

    if (userRole != "BASE USER") {
        editor = new $.fn.dataTable.Editor({
            ajax: '/api/Inventory/UpdateInventoryData',
            table: '#invDetailsTable',
            idSrc: 'id',
            fields: [{
                label: "Location",
                name: "location.location_id",
                type: "select2",
                ipOpts: locationList,
                opts: { allowClear: true, width: 'resolve', dropdownAutoWidth: true }

            },
            {
                label: "Manufacturer",
                name: "manufacturer",
                type: "select2",
                ipOpts: manufacturerList,
                opts: { allowClear: true, width: 'resolve', dropdownAutoWidth: true }
            }, {
                label: "Qty",
                name: "qty"
            }, {
                label: "Inventoried Date",
                name: "inv_date",
                type: "date",
                dateFormat: 'mm/dd/yy',
                "dateImage": '/resources/css/calender.png'
            }, {
                label: "Onboard Date",
                name: "onboard_date",
                type: "date",
                dateFormat: 'mm/dd/yy',
                "dateImage": '/resources/css/calender.png'
            }, {
                label: "Expiration Date",
                name: "expiration_date",
                type: "date",
                dateFormat: 'mm/dd/yy',
                "dateImage": '/resources/css/calender.png'
            }
            ]
        });

        cageEditor = new $.fn.dataTable.Editor({
            ajax: '/api/Inventory/UpdateCAGE',
            idSrc: 'id',
            fields: [{
                label: "Cage",
                name: "cage",
            },
            {
                label: "Manufacturer",
                name: "manufacturer",
            }]
        });

        cageEditor.on('preSubmit', function (e, data, action) {
            data.data.niin = $('#inv_NIIN_label').val();
        });

        cageEditor.on('create', function (e, data, action) {
            getFilteredDropdowns(niin);
            var table = $('#invDetailsTable').DataTable()
            table.ajax.reload();
        });

        editor.on('preSubmit', function (e, data, action) {
            $.get("api/Inventory/CheckIncompatibility?inventory_id=" + inventory_id + "&location_id=" + data.data.location.location_id, function (data) {
                if (data === "RESTRICTED") {
                    alert("WARNING! Per NSTM 670 HCC Compatibility Chart, this item is incompatible with another item in the selected location. This item must be separated from other items in this location in a manner that, in the event of leakage, mixing of hazardous materials would not occur.");
                }
                if (data === "PROHIBITED") {
                    alert("WARNING! Per NSTM 670 HCC Compatibility Chart, this item is incompatible with another item in the selected location. This item cannot be stored in this location.");

                }
            });

            now = new Date();
            var strDateTime = [AddZero(now.getMonth() + 1), AddZero(now.getDate()), now.getFullYear()].join("/");

            function AddZero(num) {
                return (num >= 0 && num < 10) ? "0" + num : num + "";
            }
            data.data.inv_date = strDateTime;

            if ((data.data.qty != parseInt(data.data.qty, 10)) || (data.qty < 0)) {
                this.error('qty', 'Quantity must be a positive whole number.');
                return false;
            }

            if (!data.data.location.location_id) {
                this.error('location', 'You must enter a location.');
                return false;
            }

            if (data.action == "create") {
                var table = $('#invDetailsTable').dataTable();
                var tableData = table.fnGetData();
                for (i = 0; i < tableData.length; i++) {
                    if ((tableData[i].location.location_id == data.data.location.location_id) && ((tableData[i].cage == data.data.cage) || (!tableData[i].cage && !data.data.cage)) && (tableData[i].inv_date == data.data.inv_date)) {
                        this.error('There already exists an on-hand item with this niin, location, cage, and inventoried date. Please edit that item instead of adding a duplicate.');
                        return false;
                    }
                }
            }

            if (data.action == "edit") {
                var table = $('#invDetailsTable').dataTable();
                var tableData = table.fnGetData();
                for (i = 0; i < tableData.length; i++) {
                    if ((tableData[i].location.location_id == data.data.location.location_id) && ((tableData[i].cage == data.data.cage) || (!tableData[i].cage && !data.data.cage)) && (tableData[i].expiration_date == data.data.expiration_date) && (data.id != tableData[i].id)) {
                        alert("Duplicate error - you cannot have multiple records with the same NIIN, CAGE, and expiration date in the same location.");
                        return false;
                    }
                }
            }

            data.data.inventory_detail_id = $('#lbl_inventory_detail_id').val();
        });

        editor.on('edit', function (e, data, action) {
            $.get("api/Inventory/GetTotalQty?inventory_id=" + inventory_id, function (qty) {
                $('#inv_total_qty').val(qty);
            });
            var table = $('#inventoryTable').DataTable()
            table.ajax.reload();
        });

        editor.on('create', function (e, data, action) {
            $.get("api/Inventory/GetTotalQty?inventory_id=" + inventory_id, function (qty) {
                $('#inv_total_qty').val(qty)
            });
            var table = $('#inventoryTable').DataTable()
            table.ajax.reload();
        });

        $('#invDetailsTable').on('click', '.editable', function (e) {
            editor.inline(this, { submitOnBlur: true });
        });

        $('#invDetailsTable').on('click', '.location', function (e) {
            editor.inline(this, 'location.location_id', { submitOnBlur: true });
        });

        $('#invDetailsTable').on('click', '.bubble', function (e) {
            editor.bubble(this);
        });

        $('#invDetailsTable').dataTable({
            dom: "Trtifp",
            "searching": false,
            "ajax": {
                "url": "/api/Inventory/LoadInventoryDetailsTable",
                "type": "GET",
                "data": function (d) {
                    d.inventory_id = $("#lbl_inventory_onhand_id").val();
                }
            },
            "columnDefs": [
                { className: "editable", "targets": [2, 3] },
                { className: "location", "targets": [0] },
                 { className: "bubble", "targets": [5, 6, 7] },
                     {
                         "targets": [8],
                         "visible": false,
                         "searchable": false
                     }
            ],
            "columns": [
                { "data": "location.location_name" },
                { "data": "cage" },
                { "data": "manufacturer" },
                { "data": "qty" },
                { "data": "workcenter.workcenter_name" },
                { "data": "inv_date" },
                { "data": "onboard_date" },
                { "data": "expiration_date" },
                { "data": "inventory_detail_id" }
            ],
            tableTools: {
                sRowSelect: "os",
                "aButtons": [
                { sExtends: "editor_create", editor: editor },
                {
                    sExtends: "editor_create", editor: cageEditor, "sButtonText": "Add New Cage/Manufacturer",
                    fnClick: function () {
                        cageEditor.create({ title: 'Add New CAGE/Manufacturer', buttons: 'Add' });
                    }
                }
                ]
            }
        });
    }

    if (userRole != "BASE USER") {
        addEditor = new $.fn.dataTable.Editor({
            ajax: '/api/Inventory/UpdateInventoryData',
            table: '#addInvDetailsTable',
            idSrc: 'id',
            fields: [{
                label: "Location",
                name: "location.location_id",
                type: "select2",
                ipOpts: locationList,
                opts: { allowClear: true, width: 'resolve', dropdownAutoWidth: true }

            },
            {
                label: "Manufacturer",
                name: "manufacturer",
                type: "select2",
                ipOpts: manufacturerList,
                opts: { allowClear: true, width: 'resolve', dropdownAutoWidth: true }
            }, {
                label: "Qty",
                name: "qty"
            }, {
                label: "Inventoried Date",
                name: "inv_date",
                type: "date",
                dateFormat: 'mm/dd/yy',
                "dateImage": '/resources/css/calender.png'
            }, {
                label: "Onboard Date",
                name: "onboard_date",
                type: "date",
                dateFormat: 'mm/dd/yy',
                "dateImage": '/resources/css/calender.png'
            }, {
                label: "Expiration Date",
                name: "expiration_date",
                type: "date",
                dateFormat: 'mm/dd/yy',
                "dateImage": '/resources/css/calender.png'
            }
            ]
        });

        addEditor.on('preSubmit', function (e, data, action) {
            e.preventDefault();

            $.get("api/Inventory/CheckIncompatibility?inventory_id=" + inventory_id + "&location_id=" + data.data.location.location_id, function (data) {
                if (data === "RESTRICTED") {
                    alert("WARNING! Per NSTM 670 HCC Compatibility Chart, this item is incompatible with another item in the selected location. This item must be separated from other items in this location in a manner that, in the event of leakage, mixing of hazardous materials would not occur.");
                }
                if (data === "PROHIBITED") {
                    alert("WARNING! Per NSTM 670 HCC Compatibility Chart, this item is incompatible with another item in the selected location. This item cannot be stored in this location.");

                }
            });

            now = new Date();
            var strDateTime = [AddZero(now.getMonth() + 1), AddZero(now.getDate()), now.getFullYear()].join("/");

            function AddZero(num) {
                return (num >= 0 && num < 10) ? "0" + num : num + "";
            }
            data.data.inv_date = strDateTime;

            if ((data.data.qty != parseInt(data.data.qty, 10)) || (data.qty < 0)) {
                this.error('qty', 'Quantity must be a positive whole number.');
                return false;
            }
            //need to make sure the item exists before updating on hand
            if (!$('#add_inv_niin').val()) {
                this.error('You must select or enter a niin on the Add Inventory screen before you can add on hand inventory.');
                return false;
            }
            if (!data.data.location.location_id) {
                this.error('location', 'You must enter a location.');
                return false;
            }


            if (data.action == "create") {
                var table = $('#invDetailsTable').dataTable();
                var tableData = table.fnGetData();
                for (i = 0; i < tableData.length; i++) {
                    if ((tableData[i].location.location_id == data.data.location.location_id) && ((tableData[i].cage == data.data.cage) || (!tableData[i].cage && !data.data.cage)) && (tableData[i].inv_date == data.data.inv_date)) {
                        this.error('There already exists an on-hand item with this niin, location, cage, and inventoried date. Please edit that item instead of adding a duplicate.');
                        return false;
                    }
                }
            }

            if (data.action == "edit") {
                var table = $('#invDetailsTable').dataTable();
                var tableData = table.fnGetData();
                for (i = 0; i < tableData.length; i++) {
                    if ((tableData[i].location.location_id == data.data.location.location_id) && ((tableData[i].cage == data.data.cage) || (!tableData[i].cage && !data.data.cage)) && (tableData[i].expiration_date == data.data.expiration_date) && (data.id != tableData[i].id)) {
                        alert("Duplicate error - you cannot have multiple records with the same NIIN, CAGE, and expiration date in the same location.");
                        return false;
                    }
                }
            }
            AddUpdateInventoryInfo(true);
            data.data.inventory_detail_id = $('#lbl_add_inventory_detail_id').val();

        });

        addEditor.on('edit', function (e, data, action) {
            $.get("api/Inventory/GetTotalQty?niin=" + $('#add_inv_niin').val(), function (qty) {
                $('#add_inv_total_qty').val(qty);
            });
            var table = $('#addInvDetailsTable').DataTable()
            table.ajax.reload();
        });

        addEditor.on('create', function (e, data, action) {
            $.get("api/Inventory/GetTotalQty?niin=" + $('#add_inv_niin').val(), function (qty) {
                $('#add_inv_total_qty').val(qty);
            });
            var table = $('#inventoryTable').DataTable()
            table.ajax.reload();
        });

        $('#addInvDetailsTable').on('click', '.editable', function (e) {
            addEditor.inline(this, { submitOnBlur: true });
        });

        $('#addInvDetailsTable').on('click', '.location', function (e) {
            addEditor.inline(this, 'location.location_id', { submitOnBlur: true });
        });

        $('#addInvDetailsTable').on('click', '.bubble', function (e) {
            addEditor.bubble(this);
        });

        $('#addInvDetailsTable').dataTable({
            dom: "Tfrtip",
            "searching": false,
            "ajax": {
                "url": "/api/Inventory/AttemptToPopulateInventoryAdd",
                "type": "GET",
                "data": function (d) {
                    d.niin = $("#add_inv_niin").val();
                }
            },
            "columnDefs": [
                { className: "editable", "targets": [2, 3] },
                { className: "location", "targets": [0] },
                 { className: "bubble", "targets": [5, 6, 7] },
                     {
                         "targets": [8],
                         "visible": false,
                         "searchable": false
                     }
            ],
            "columns": [
                { "data": "location.location_name" },
                { "data": "cage" },
                { "data": "manufacturer" },
                { "data": "qty" },
                { "data": "workcenter.workcenter_name" },
                { "data": "inv_date" },
                { "data": "onboard_date" },
                { "data": "expiration_date" },
                { "data": "inventory_detail_id" }
            ],
            tableTools: {
                sRowSelect: "os",
                "aButtons": [
                { sExtends: "editor_create", editor: addEditor },
                ]
            }
        });
    }
}


function populateInvDetails() {
    $.get("api/Inventory/GetInventoryDetailsModal?inventory_id=" + inventory_id, function (data) {
        var $modal = $('#InventoryDetailsModal');
        $modal.find('#inv_FSC_label').val(data.aaData.fsc);
        $modal.find('#inv_NIIN_label').val(data.aaData.niin);
        $modal.find('#inv_description_label').val(data.aaData.name);
        $modal.find('#inv_ui_label').val(data.aaData.ui);
        $modal.find('#inv_um_label').val(data.aaData.um);
        $modal.find('#inv_spmig_label').val(data.aaData.spmig);
        $modal.find('#inv_specs_label').val(data.aaData.specs);
        $modal.find('#inv_usage_category').val(data.aaData.usage_category_id);
        $modal.find('#inv_slc').select2("val", data.aaData.slc_id);
        $modal.find('#inv_slac').select2("val", data.aaData.slac_id);
        $modal.find('#inv_hcc').select2("val", data.aaData.hcc_id);
        $modal.find('#inv_smcc').select2("val", data.aaData.smcc_id);
        $modal.find('#inv_remarks_text').val(data.aaData.remarks);
        $modal.find('#inv_notes_text').val(data.aaData.notes);
        $modal.find('#inv_total_qty').val(data.totalQuantity);
        $modal.find('#inv_qty_allowed').val(data.aaData.allowance_qty);

        $('.smcl-field').each(function () {
            if ($(this).is("select")) {
                if ($(this).val() != null && $(this).val() != "") {
                    $(this).select2('disable');
                }
                else {
                    $(this).select2('enable');
                }
            }
            else {
                if (this.value != null && this.value != "") {
                    this.disabled = true;
                }
                else {
                    this.disabled = false;
                }
            }
        })

        $modal.find('#inv_total_qty').val(data.totalQuantity);

        //only users above the bottom level (user) can update stuff
        if (!(data.userRole === "BASE USER")) {
            $('#inv_qty_allowed').prop("disabled", false);
            $('#inv_notes_text').prop("disabled", false);
        }
        else {
            $('#inv_qty_allowed').prop("disabled", true);
            $('#inv_slc').prop("disabled", true);
            $('#inv_slac').prop("disabled", true);
            $('#inv_hcc').prop("disabled", true);
            $('#inv_ui_label').prop("disabled", true);
            $('#inv_um_label').prop("disabled", true);
            $('#inv_notes_text').prop("disabled", true);
        }

        if (userRole != "BASE USER") {
            var table = $('#invDetailsTable').DataTable();
            table.ajax.reload();
            editor.field('manufacturer').update(manufacturerList);
        }
        else {
            $('.invTable').css('visibility', 'hidden');
        }
        $modal.modal('show');
    });
}

$(document).on('click', '#btn_updateInvItem', function (e) {
    e.preventDefault();
    if (userRole == "BASE USER") {
        alert('You do not have permission to update inventory items.');
        return false;
    }
    var newId = $('#lbl_inventory_detail_id').val();
    var newNiin = $('#inv_NIIN_label').val();
    var newFsc = $('#inv_FSC_label').val();
    var newUI = $('#inv_ui_label').val();
    var newName = $('#inv_description_label').val();
    var newSpmig = $('#inv_spmig_label').val();
    var newSpecs = $('#inv_specs_label').val();
    var newUsageCategory = $('#inv_usage_category').val();
    var newUM = $('#inv_um_label').val();
    var newSLC = $('#inv_slc').val();
    var newSLAC = $('#inv_slac').val();
    var newHCC = $('#inv_hcc').val();
    var newSmcc = $('#inv_smcc').val();
    var newNotes = $('#inv_notes_text').val();
    var newQtyAllowed = $('#inv_qty_allowed').val();
    if ((newQtyAllowed != parseInt(newQtyAllowed, 10)) || (newQtyAllowed < 0)) {
        $('#successLabelInv').text("Quantity allowed must be a positive whole number.");
        $('#successLabelInv').css('color', 'red');
        return;
    }
    var jsonData = { id: newId, niin: newNiin, fsc: newFsc, name: newName, spmig: newSpmig, specs: newSpecs, smcc: newSmcc, usage_category: newUsageCategory, ui: newUI, um: newUM, slc: newSLC, slac: newSLAC, HCC: newHCC, notes: newNotes, qtyAllow: newQtyAllowed, id: newId };
    $.ajax({
        type: 'POST',
        url: "api/Inventory/UpdateInvItem",
        data: JSON.stringify(jsonData),
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            $('#successLabelInv').text("Update successful!");
            $('#successLabelInv').css('color', 'green');
        }
    });
});

$(document).on('click', '.btnDelete', function (e) {
    e.preventDefault();
    var inventory_id = $(this).attr('data-inventoryId');
    var $modal = $('#confirmDeleteModal');
    $.get("api/Inventory/GetDeleteModal?inventory_id=" + inventory_id, function (data) {
        $modal.find('#niin_label').text(data.aaData[0].niin);
        $modal.find('#cage_label').text(data.aaData[0].cage);
        $modal.find('#location_label').text(data.aaData[0].location_name);
        $modal.find('#expiration_label').text(data.aaData[0].expiration_date);
        $modal.find('#inventory_id_label').text(data.aaData[0].inventory_id);
        $modal.modal('show');
    });
    $modal.modal('show');
});

$(document).on('click', '#confirmDeleteBtn', function (e) {
    e.preventDefault();
    var inventory_id = $('#inventory_id_label').text();
    var jsonData = { InventoryId: inventory_id };
    $.ajax({
        type: 'POST',
        url: "api/Inventory/DeleteRecord",
        data: JSON.stringify(jsonData),
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            var table = $('#inventoryTable').DataTable()
            table.ajax.reload();
        }
    });
});

$(document).on('click', '.btn-MSDS', function (e) {
    e.preventDefault();
    var fsc = $(this).attr('data-fsc');
    var niin = $(this).attr('data-niin');
    var cage = $(this).attr('data-cage');
    $.get("api/Inventory/getMSDSFiles", { fsc: fsc, niin: niin, cage: cage }, function (data) {

        msdsCount = data.length;
        if (msdsCount <= 1) {
            $('#msdsLeft').attr("disabled", "disabled");
            $('#msdsRight').attr("disabled", "disabled");
        }
        else {
            $('#msdsLeft').removeAttr("disabled");
            $('#msdsRight').removeAttr("disabled");
        }
        if (msdsCount > 0) {
            $('#noMsds').hide();
            $('.item-count').show();
            for (var i = 0; i < data.length; i++) {
                //window.open(data[i]);
                if (i == 0) {
                    currentMSDS = 1;
                    $('#currentMSDS').text(currentMSDS);
                    $('#totalMSDS').text(msdsCount);

                    var splitName = data[currentMSDS - 1].split(".");
                    if (splitName[1] != "PDF" && splitName[1] != "pdf") {
                        $('#msdsPdf').hide();
                        $('#msdsDoc').show();
                        $('#msdsDoc').attr("src", data[currentMSDS - 1]);
                    }
                    else {
                        $('#msdsDoc').hide();
                        $('#msdsPdf').show();
                        $('#msdsPdf').attr("data", data[currentMSDS - 1]);
                    }

                }
                msdsList[i] = data[i];
            }
        }
        else {
            $('#msdsPdf').hide();
            $('#msdsDoc').hide();
            $('.item-count').hide();
            $('#noMsds').show();


        }
    });
    var $modal = $('#msdsModal');
    $modal.modal('show');

    var $modal = $('#msdsModal');
    $modal.modal('show');
});

$(document).on('click', '#msdsLeft', function (e) {

    if (currentMSDS > 1 && currentMSDS <= msdsCount) { currentMSDS = parseInt(currentMSDS) - 1; }
    else if (currentMSDS > msdsCount) { currentMSDS = 1; }
    else { currentMSDS = msdsCount; }

    $('#currentMSDS').text(currentMSDS);

    var splitName = msdsList[currentMSDS - 1].split(".");
    if (splitName[1] != "PDF" && splitName[1] != "pdf") {
        $('#msdsPdf').hide();
        $('#msdsDoc').show();
        $('#msdsDoc').attr("src", msdsList[currentMSDS - 1]);
    }
    else {
        $('#msdsDoc').hide();
        $('#msdsPdf').show();
        $('#msdsPdf').attr("data", msdsList[currentMSDS - 1]);
    }
    //$('#msdsDoc').attr("src", msdsList[currentMSDS]);

});

$(document).on('click', '#msdsRight', function (e) {

    if (currentMSDS >= 1 && currentMSDS < msdsCount) { currentMSDS = parseInt(currentMSDS) + 1; }
    else if (currentMSDS > msdsCount) { currentMSDS = msdsCount; }
    else { currentMSDS = 1; }
    $('#currentMSDS').text(currentMSDS);

    var splitName = msdsList[currentMSDS - 1].split(".");
    if (splitName[1] != "PDF" && splitName[1] != "pdf") {
        $('#msdsPdf').hide();
        $('#msdsDoc').show();
        $('#msdsDoc').attr("src", msdsList[currentMSDS - 1]);
    }
    else {
        $('#msdsDoc').hide();
        $('#msdsPdf').show();
        $('#msdsPdf').attr("data", msdsList[currentMSDS - 1]);
    }
    //$('#msdsDoc').attr("src", msdsList[currentMSDS]);
});

$(document).on('click', '.btn-SMCL', function (e) {
    e.preventDefault();
    var niin = $(this).attr('data-niin');
    populateSMCLDetails(niin);
});

$(document).on('click', '.btn-Inv', function (e) {
    e.preventDefault();
    niin = $(this).attr('data-niin');
    inventory_id = $(this).attr('data-inventory_id');
    inventory_detail_id = $(this).attr('data-inventory_detail_id');
    $('#printInvDetails').prop('href', 'api/Inventory/PrintInventoryDetails?inventory_onhand_id=' + inventory_id);
    $('#lbl_inventory_onhand_id').val(inventory_id);
    $('#lbl_inventory_detail_id').val(inventory_detail_id);
    $('#locationTxt').text($(this).attr('data-location'));
    getFilteredDropdowns(niin);

});

$(document).on('click', '.btnOffload', function (e) {
    e.preventDefault();
    inventory_id = $(this).attr('data-inventoryId');
    $('#confirmOffloadBtn').attr("disabled", "disabled");
    var $modal = $('#confirmOffloadModal');
    $.get("api/Inventory/GetOffloadModal?inventory_id=" + inventory_id, function (data) {
        $modal.find('#fscText').val(data.aaData[0].fsc);
        $modal.find('#niinText').val(data.aaData[0].niin);
        $modal.find('#nameText').val(data.aaData[0].name);
        $modal.find('#totalText').val(data.aaData[0].total);
        $modal.find('#uiText').val(data.aaData[0].ui);
        $modal.find('#umText').val(data.aaData[0].um);
        $modal.find('#cageText').val(data.aaData[0].cage);
        $modal.find('#inventoryId').val(data.aaData[0].id);
        $modal.modal('show');
    });
    $modal.modal('show');
});

$(document).on('input', '#offloadQuantity', function (e) {
    var quantity = $('#offloadQuantity').val();
    var allowedQuantity = $('#totalText').val();
    var totalQuantity = allowedQuantity - quantity;

    if (parseInt(totalQuantity) >= parseInt(allowedQuantity) || totalQuantity < 0) { $('#confirmOffloadBtn').attr("disabled", "disabled"); }
    else { $('#confirmOffloadBtn').removeAttr("disabled"); }
})

$(document).on('click', '#confirmOffloadBtn', function (e) {
    e.preventDefault();
    var id = $('#inventoryId').val();
    var quantity = $('#offloadQuantity').val();
    var jsonData = { InventoryId: id, Quantity: quantity };
    $.ajax({
        type: 'POST',
        url: "api/Inventory/AddOffloadItem",
        data: JSON.stringify(jsonData),
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            var table = $('#inventoryTable').DataTable();
            table.ajax.reload();
        }
    });
});

function getFilteredDropdowns(niin) {
    $.get("api/Inventory/getFilteredDropdowns?niin=" + niin, function (data) {
        cageList = data.cages;
        manufacturerList = data.manufacturers;
        populateInvDetails();
    });
}

function getFilteredDropdownsNoPopulate(niin) {
    $.get("api/Inventory/getFilteredDropdowns?niin=" + niin, function (data) {
        cageList = data.cages;
        manufacturerList = data.manufacturers;
        editor.field('manufacturer').update(manufacturerList);
    });
}

$(document).on('click', '#btn-labels', function (e) {
    e.preventDefault();
    var $modal = $('#labelsModal');
    niinTxt = $('#inv_NIIN_label').val();
    $('#lblItemNiin').val(niinTxt);
    var strings = $('#inv_usage_category option:selected').text().split('-');
    usageCategoryTxt = strings[0].trim();
    descriptionTxt = $('#inv_description_label').val();
    $('#lblItemName').val(descriptionTxt);
    locationTxt = $('#locationTxt').text();
    //default values
    startingLabelTxt = "1";
    labelCountTxt = "30";
    adjustPdfUrl();
    $modal.modal('show');
});

$(document).on('change', '#labelNumber', function (e) {
    e.preventDefault();
    startingLabelTxt = $("#labelNumber").val();
    labelCountTxt = $("#labelCount").val();
    var labelNumberInt = parseInt(startingLabelTxt);
    if (startingLabelTxt < 1 || startingLabelTxt > 30) {
        $('#errorMsg').css("visibility", "visible");
        return;
    }
    else {
        $('#errorMsg').css("visibility", "hidden");
    }
    adjustPdfUrl();
})
$(document).on('change', '#labelCount', function (e) {
    e.preventDefault();
    startingLabelTxt = $("#labelNumber").val();
    labelCountTxt = $("#labelCount").val();
    if (labelCountTxt == "" || labelCountTxt == null) {
        $('#labelCountError').css("visibility", "visible");
        return;
    }
    else {
        $('#labelCountError').css("visibility", "hidden");
    }
    adjustPdfUrl();
})
$(document).on('click', '#getPDF', function (e) {
    return validatePage();
});

function validatePage() {
    var isValid = true;
    if ($('#labelCount').val() == "" || $('#labelCount').val() == null) {
        isValid = false;
        return isValid;
    }
    labelNumberInt = parseInt($('#labelNumber').val());
    if (labelNumberInt < 1 || labelNumberInt > 30) {
        isValid = false;
    }
    return isValid;
}

function adjustPdfUrl() {
    $('#getPDF').attr('href', function (niin, usageCategory, description, startingLabel, labelCount, location) {
        var params = {
            niin: niinTxt,
            usageCategory: usageCategoryTxt,
            description: descriptionTxt,
            startingLabel: startingLabelTxt,
            labelCount: labelCountTxt,
            location: locationTxt
        };
        var str = $.param(params);
        var url = "api/SMCL/ACMLabels?";
        url += str;
        return (url);
    });
}

$(document).on('change', '.filter', (function (e) {
    var table = $('#inventoryTable').DataTable()
    table.ajax.reload();
}));

$(document).on('click', '#btn-SMCLDetails', (function (e) {
    e.preventDefault();
    var niin = $('#inv_NIIN_label').val();
    populateSMCLDetails(niin);
    $('#SMCLDetails').css("z-index", "10000");
}));



