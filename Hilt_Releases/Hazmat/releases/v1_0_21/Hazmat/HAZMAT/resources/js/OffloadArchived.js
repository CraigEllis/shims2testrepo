﻿var idCount = 0;
var itemCount = 1;
var jsonIds;
var shipDetails;
var canSave = false;

$(document).ready(function () {

    $.get("api/OffloadArchived/getShipTo", function (data) {
        var option = '<option value=" "> </option>';
        var ships = $('#shipTo');

        for (i = 0; i < data.uic.length; i++) {
            option += '<option value="' + data.uic[i] + '">' + data.uic[i] + " | " + data.name[i] + ' </option>';
        }
        ships.append(option);
    });

    $('#toDisk').attr('href', function () {
        var url = "api/OffloadArchived/CSV";
        return (url);
    });

    loadTables();
    $(document).on('click', 'tr.odd', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            $('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            itemCount = $("tbody tr.selected").index() + 1;
        }
    });
    $(document).on('click', 'tr.even', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            $('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            itemCount = $("tbody tr.selected").index() + 1;
        }
    });

    $(".shipSelect").select2({ allowClear: true });

    $('#recdDate').datepicker({ dateFormat: 'mm-dd-yy' });
    $('#docDate').datepicker({ dateFormat: 'mm-dd-yy' });
});

function loadTables() {
    $('#OffloadArchivedTable').dataTable({
        "ajax": {
            "url": "/api/OffloadArchived/LoadOffloadArchivedTable",
            "type": "GET"
        },
        "order": [[0, "desc"]],
        "columns": [
            { data: "archived_id" },
            { data: "document_number" },
            { data: "fsc" },
            { data: "niin" },
            { data: "cage" },
            { data: "description" },
            { data: "quantity" },
            { data: "offload_date" },
            { data: "ui" },
            { data: "um" }]
    });
}

$(document).on('click', '#printArchive', function (e) {
    e.preventDefault();
    $.get("api/OffloadArchived/printArchive", function (data) {
        window.open(data.relPath);
    });
});

$(document).on('change', function () {
    var select = $('#select2-chosen-1').text();
    var selected = select.split(" | ");
    var uic = selected[0].trim();
    if (uic) {
        $.get("api/OffloadArchived/GetShipToDetails", { uic: uic }, function (data) {

            $('#select2-chosen-1').text(uic);
            $('#shipToDetails').val(data.shipname);
            $('#shipPoc').val(data.poc + ', ' + data.telephone);
            $('.btn-ddFilled').removeAttr('disabled');
        });
    } else {
        $('#shipToDetails').val(' ');
        $('#shipPoc').val(' ');
        $('.btn-ddFilled').attr('disabled', 'disabled');
    }
});

$(document).on('click', '#modalClose', function (e) {
   // saveOffloadItemDetails(itemCount);
    location.reload();
});

$(document).on('click', '#ddLeft', function (e) {

    //saveOffloadItemDetails(itemCount);

    if (itemCount > 1) { itemCount -= 1; }
    else { itemCount = idCount; }

    loadOffloadArchivedItemDetails(itemCount);
    $('#currentItem').val(itemCount);

});

$(document).on('click', '#ddRight', function (e) {

   // saveOffloadItemDetails(itemCount);

    if (itemCount == idCount) { itemCount = 1; }
    else { itemCount += 1; }

    loadOffloadArchivedItemDetails(itemCount);
    $('#currentItem').val(itemCount);
});

$(document).on('input', '.form-field', function (e) {
    var thisValue = $.trim(this.value);
    canSave = false;

    if (thisValue) {
        canSave = true;
    } else {

        $('#dd1348Modal :input.form-field').each(function () {
            if ($.trim($(this).val())) { canSave = true; }
        });
    }
    console.log(canSave);
});

$(document).on('input', '#currentItem', function (e) {
    var currentItem = $('#currentItem').val();

    //saveOffloadItemDetails(itemCount);

    if (currentItem >= 1 || currentItem <= idCount) {
        itemCount = currentItem;
        loadOffloadArchivedItemDetails(itemCount);
        $('#currentItem').val(itemCount);
    }
});

$(document).on('click', '#filled1348', function (e) {
    e.preventDefault();
    var uic = $('#select2-chosen-1').text();
    var $modal = $('#dd1348Modal');

    $('#confirmPrintBtn').show();
    $('#modalClose').show();
    $('#ddLeft').show();
    $('#ddRight').show();
    $('#currentItem').show();
    $('#NSN').show();

    $.get("api/OffloadArchived/GetShipFromDetails", function (data) {
        var shipFromAddress = data.uic + "\r\n" + data.name + "\r\n" + data.address;
        $('#shipFrom').val(data.uic);
        $('#addData2').val(shipFromAddress);
    });

    $.get("api/OffloadArchived/GetShipToDetails", { uic: uic }, function (data) {
        var shipToAddress = uic + "\r\n" + data.poc + "\r\n" + data.address;
        $('#ship').val(uic);
        $('#addData3').val(shipToAddress);
    });

    $.get("api/OffloadArchived/GetOffloadArchivedItems", function (data) {
        var jsonData = [JSON.stringify(eval("(" + data + ")"))];
        jsonIds = JSON.parse(jsonData);
        idCount = Object.keys(jsonIds).length;

        if (idCount > 0) {
            $('#currentItem').val(itemCount);
            $('#items').text("/" + idCount);
            loadOffloadArchivedItemDetails(itemCount);
            $modal.modal('show');
        }
    });
});

$(document).on('click', '#confirmPrintBtn', function (e) {
    e.preventDefault();
    var uic = $('#select2-chosen-1').text();
    var $modal = $('#dd1348Modal');

    //saveOffloadItemDetails(itemCount);

    $('#confirmPrintBtn').attr("disabled", "disabled");
    $('#modalClose').attr("disabled", "disabled");
    $('.close').attr("disabled", "disabled");
    $('#ddLeft').attr("disabled", "disabled");
    $('#ddRight').attr("disabled", "disabled");
    $('.clicked').show();
    $('.load-icon').show();
    $('.not-clicked').hide();

    $.get("api/OffloadArchived/CreatePdf", { uic: uic }, function (data) {

        window.open(data.relPath);
        $('.btn-ddFilled').removeAttr("disabled");
        $('.clicked').hide();
        $('.load-icon').hide();
        $('.not-clicked').show();
        $('#confirmPrintBtn').removeAttr("disabled");
        $('#modalClose').removeAttr("disabled");
        $('#close').removeAttr("disabled");
        $modal.modal('hide');
        location.reload();
    });
});

function loadOffloadArchivedItemDetails(key) {
    var value = jsonIds[key - 1];

    $.get("api/OffloadArchived/GetOffloadArchivedItemDetails", { id: value }, function (data) {
        $('#NSN').val(data.details[0].NSN);
        $('#CAGE').val(data.details[0].CAGE);
        $('#itemNomen').val(data.details[0].ItemNomen);
        $('#quantity').val(data.details[0].Quant);
        $('#UI').val(data.details[0].UnitIss);
        $('#addData4').val(data.details[0].AddData4);
        $('#docNumber').val(data.details[0].DocNum);
        $('#docIdent').val(data.details[0].Dociden);
        $('#riFrom').val(data.details[0].RIFrom);
        $('#mAndS').val(data.details[0].MandS);
        $('#SVC').val(data.details[0].SER);
        $('#suppAddress').val(data.details[0].SupAdd);
        $('#SIG').val(data.details[0].Sig);
        $('#fund').val(data.details[0].Fund);
        $('#dist').val(data.details[0].Distribution);
        $('#project').val(data.details[0].Project);
        $('#PRI').val(data.details[0].Pri);
        $('#reqdDelDate').val(data.details[0].RecdDelDate);
        $('#ADV').val(data.details[0].Adv);
        $('#RI').val(data.details[0].Ri);
        $('#OP').val(data.details[0].OP);
        $('#MGT').val(data.details[0].MGT);
        $('#CC').val(data.details[0].Cond);
        $('#unitPrice').val(data.details[0].UnitPrice);
        $('#totalPrice').val(data.details[0].TotalPrice);
        $('#markFor').val(data.details[0].MarkFor);
        $('#NMFC').val(data.details[0].NMFC);
        $('#frtRate').val(data.details[0].FRTRate);
        $('#typeCargo').val(data.details[0].TypeCargo);
        $('#PS').val(data.details[0].PS);
        $('#qtyRecd').val(data.details[0].QtyRecd);
        $('#UP').val(data.details[0].UP);
        $('#unitWeight').val(data.details[0].UnitWeight);
        $('#unitCube').val(data.details[0].UnitCube);
        $('#UFC').val(data.details[0].UFC);
        $('#SL').val(data.details[0].SI);
        $('#freightNomen').val(data.details[0].FreightClassNomen);
        $('#tyCont').val(data.details[0].TYCount);
        $('#noCont').val(data.details[0].NOCount);
        $('#totalWeight').val(data.details[0].TotalWeight);
        $('#totalCube').val(data.details[0].TotalCube);
        $('#recdBy').val(data.details[0].RecdBy);
        $('#recdDate').val(data.details[0].DateRecd);
        $('#RIC').val(data.details[0].RIC);
        $('#addData1').val(data.details[0].AddData1);
        $('#docDate').val(data.details[0].DocDate);
    });
}

function stringifyDetails(isBlank) {
    var details = "";
    if (isBlank) {
        details += $('#FSC').val() + ",";
        details += $('#NIIN').val() + ",";
        details += $('#CAGE').val() + ",";
        details += $('#itemNomen').val() + ",";
        details += $('#quantity').val() + ",";
        details += $('#UI').val() + ",";
        details += $('#addData4').val() + ",";
        details += " ,";
    }
    details += $('#docNumber').val() + ",";
    details += $('#docIdent').val() + ",";
    details += $('#riFrom').val() + ",";
    details += $('#mAndS').val() + ",";
    details += $('#SVC').val() + ",";
    details += $('#suppAddress').val() + ",";
    details += $('#SIG').val() + ",";
    details += $('#fund').val() + ",";
    details += $('#dist').val() + ",";
    details += $('#project').val() + ",";
    details += $('#PRI').val() + ",";
    details += $('#reqdDelDate').val() + ",";
    details += $('#ADV').val() + ",";
    details += $('#RI').val() + ",";
    details += $('#OP').val() + ",";
    details += $('#MGT').val() + ",";
    details += $('#CC').val() + ",";
    details += $('#unitPrice').val() + ",";
    details += $('#totalPrice').val() + ",";
    details += $('#markFor').val() + ",";
    details += $('#NMFC').val() + ",";
    details += $('#frtRate').val() + ",";
    details += $('#typeCargo').val() + ",";
    details += $('#PS').val() + ",";
    details += $('#qtyRecd').val() + ",";
    details += $('#UP').val() + ",";
    details += $('#unitWeight').val() + ",";
    details += $('#unitCube').val() + ",";
    details += $('#UFC').val() + ",";
    details += $('#SL').val() + ",";
    details += $('#freightNomen').val() + ",";
    details += $('#tyCont').val() + ",";
    details += $('#noCont').val() + ",";
    details += $('#totalWeight').val() + ",";
    details += $('#totalCube').val() + ",";
    details += $('#recdBy').val() + ",";
    details += $('#recdDate').val() + ",";
    details += $('#RIC').val() + ",";
    details += $('#addData1').val() + ",";

    if (isBlank) {
        details += $('#docDate').val() + ",";
    }

    return details;
}
