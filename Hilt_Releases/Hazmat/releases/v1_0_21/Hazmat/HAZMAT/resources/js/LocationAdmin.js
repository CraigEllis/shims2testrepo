﻿workcenter_list = [""];

$(document).ready(function () {
    $.get("/api/Location/GetWorkcenters", function (data) {
        for (var i = 0; i < data.workcenters.length; i++) {
            var thing = { label: data.workcenters[i].wid, value: data.workcenters[i].workcenter_id };
            workcenter_list.push(thing);
        }
        loadTable();
    });
});

function loadTable() {
    locationEditor = new $.fn.dataTable.Editor({
        ajax: '/api/Location/UpdateLocation',
        table: '#locationTable',
        idSrc: 'location_id',
        fields: [{
            label: "Location",
            name: "location_name"
        },
        {
            label: "Workcenter",
            name: "workcenter.workcenter_id",
            type: "select",
            ipOpts: workcenter_list
        }
        ]
    });

    locationTable = $('#locationTable').on('init.dt', function () {
        $('#line').after($('#ToolTables_userTable_0'));
    }).DataTable({
        dom: "Trtip",
        "ajax": {
            "url": "/api/Location/LoadLocationTable",
            "type": "GET"
        },
        "columnDefs": [
        { className: "editable", "targets": [0] },
        { className: "workcenter", "targets": [1] },

        {
            "targets": [3],
            "visible": false,
            "searchable": false
        }],
        "columns": [
            { "data": "location_name" },
            { "data": "workcenter.workcenter_name" },
            {
                "mRender": function (data, type, row) {
                    return '<input type="button" class="btn btn-info btn-sm btn-details btnDeleteLocation" value="Delete" data-location-id="' + row.location_id + '"/>';
                }
            },
            { "data": "location_id" }],
        tableTools: {
            sRowSelect: "os",
            "aButtons": [
            { sExtends: "editor_create", editor: locationEditor, "sButtonText": "Add New Location" },
            ]
        }
    });

    $('#locationTable').on('click', '.editable', function (e) {
        locationEditor.inline(this, { submitOnBlur: true });
    });

    $('#locationTable').on('click', '.workcenter', function (e) {
        locationEditor.inline(this, 'workcenter.workcenter_id', { submitOnBlur: true });
    });

    $('#searchLocations').keyup(function () {
        locationTable.search($(this).val()).draw();
    })

    $('#searchLocations').after($('#ToolTables_locationTable_0'));
    $('#ToolTables_locationTable_0').after($('#ToolTables_locationTable_1'));
}

$(document).on('click', '.btnDeleteLocation', function (e) {
    location_id = $(this).attr('data-location-id');
    $.ajax({
        type: 'DELETE',
        url: "api/Location/DeleteLocation?location_id=" + location_id,
        contentType: 'application/json',
        dataType: 'json',
        success: function () {
            var table = $('#locationTable').DataTable()
            table.ajax.reload();
        }
    });
});