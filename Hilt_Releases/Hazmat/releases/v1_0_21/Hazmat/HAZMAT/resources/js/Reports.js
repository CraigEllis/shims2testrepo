﻿var report = "invTab";
var type;
var sort;
var expType;
var sigBlock = false;
var signatures = "";

$(document).ready(function() {
    $('#expirDate').datepicker({ dateFormat: 'MM dd, yy' });

    $.get("api/Reports/getLocations", function (data) {
        var option = '<option value=" "> </option>';
        var invLocation = $('#invLocSelect');
        var incompLocation = $('#incompLocSelect');

        for (i = 0; i < data.aaData.length; i++) {
            option += '<option value="' + data.aaData[i].id + '">' +
                data.aaData[i].id + " | " + data.aaData[i].name + ' </option>';
        }
        invLocation.append(option);
        incompLocation.append(option);
    });

    $.get("api/Reports/getWorkcenters", function (data) {
        var option = '<option value=" "> </option>';
        var invWork = $('#invWorkSelect');
        var incompWork = $('#incompWorkSelect');

        for (i = 0; i < data.aaData.length; i++) {
            option += '<option value="' + data.aaData[i].id + '">' +
                data.aaData[i].id + " | " + data.aaData[i].id + ' </option>';
        }
        invWork.append(option);
        incompWork.append(option);
    });
});

$(document).on('click', '#includeSig', function (e) {


    if ($('#includeSig').is(':checked')) {
        $('#sig1').removeAttr("disabled");
        $('#sig2').removeAttr("disabled");
        $('#sig3').removeAttr("disabled");
        $('#sig4').removeAttr("disabled");
        signatures = $('#sig1').val() + "|" + $('#sig2').val() + "|" + $('#sig3').val() + "|" + $('#sig4').val();
    } else {
        $('#sig1').attr("disabled", "disabled");
        $('#sig2').attr("disabled", "disabled");
        $('#sig3').attr("disabled", "disabled");
        $('#sig4').attr("disabled", "disabled");
        signatures = "";
    }
});

$(document).on('click', 'input[name="invtype"]',function(e) {
    var selected = $(this).val();

    if (selected == "specWorkcenter") {
        $("#invSort").hide();
        $("#invSpecSort").show();
        $("#invWorkSelect").show();
        $("#invLocSelect").hide();
        if ($('#invWorkSelect option:selected').val() == ' ') {
            $('#printReport').attr("disabled", "disabled");
        } else {
            $('#printReport').removeAttr("disabled");
        }
    }
    else if (selected == "specLocation") {
        $("#invSort").hide();
        $("#invSpecSort").show();
        $("#invLocSelect").show();
        $("#invWorkSelect").hide();
        if ($('#invLocSelect option:selected').val() == ' ') {
            $('#printReport').attr("disabled", "disabled");
        } else {
            $('#printReport').removeAttr("disabled");
        }
        
    } else {
        $("#invSort").show();
        $("#invSpecSort").hide();
        $('#printReport').removeAttr("disabled");
    }
});

$(document).on('click', 'input[name="incomptype"]', function(e) {
    var selected = $(this).val();

    if (selected == "specWorkcenter") {
        $("#incompSpecSort").show();
        $("#incompWorkSelect").show();
        $("#incompLocSelect").hide();

    }
    else if (selected == "specLocation") {
        $("#incompSpecSort").show();
        $("#incompLocSelect").show();
        $("#incompWorkSelect").hide();
    } else {
        $("#incompSpecSort").hide();
    }
});

$(document).on('click', '#printReport', function(e) {
    type = $('.active #type :input:checked').val();
    var apiReport = " ";

    if (report == "hwsTab") { type = "all"; apiReport = "api/Reports/GenerateStowageReport";}
    if (report == "shelfTab") { type = $('#expirDate').val(); apiReport = "api/Reports/GenerateExpirationReport"; }
    if (report == "invTab") { apiReport = "api/Reports/GenerateInventoryReport"; }
    if (report == "acmTab") { apiReport = "api/Reports/GenerateAcmReport"; }
    if (report == "incompTabNoSig") { apiReport = "api/Reports/GenerateIncompatiblesReport"; }

    if (type == "specWorkcenter" || type == "specLocation") {
        if (type == "specWorkcenter") {
            if (report == "invTab") { sort = $('#invWorkSelect option:selected').val(); }
            else if (report == "incompTabNoSig") { sort = $('#incompWorkSelect option:selected').val(); }
        } else {
            if (report == "invTab") { sort = $('#invLocSelect option:selected').val(); }
            else if (report == "incompTabNoSig") { sort = $('#incompLocSelect option:selected').val(); }
        }
    } else { sort = $('.active #sort :input:checked').val(); }

    if (report == "incompTabNoSig" && type == "all") { sort = "all"; }

    $.get(apiReport, { type: type, sort: sort, signatures: signatures }, function(data) {
        window.open(data);
    });

});

$(document).on('click', 'a', function (e) {
    if (e.target.id == "invTab" || e.target.id == "acmTab" || e.target.id == "shelfTab"
        || e.target.id == "hwsTab" || e.target.id == "incompTabNoSig" || e.target.id == "otherTabNoSig") { report = e.target.id; }

    if (report == "otherTabNoSig") {
        $('#printReport').hide();
        $('#exportReport').show();
    } else {
        $('#printReport').show();
        $('#exportReport').hide();
    }

    if (report == "shelfTab") {
        if ($('#expirDate').val() == "") { $('#printReport').attr("disabled", "disabled"); }
        else { $('#printReport').removeAttr("disabled"); }
    }
    else { $('#printReport').removeAttr("disabled"); }
    
});

$(document).on('click', 'input[name="exptype"]', function(e) {
    
    expType = $('.active #type :input:checked').val();
    $("#export").attr("href", "api/Reports/ExportReport?extension="+expType);
})

$(document).on('change', '#invLocSelect', function(e) {

    if ($('#invLocSelect option:selected').val() == ' ') {
        $('#printReport').attr("disabled", "disabled");
    } else {
        $('#printReport').removeAttr("disabled");
    }
});

$(document).on('change', '#invWorkSelect', function (e) {

    if ($('#invWorkSelect option:selected').val() == ' ') {
        $('#printReport').attr("disabled", "disabled");
    } else {
        $('#printReport').removeAttr("disabled");
    }
});

$(document).on('change', "input[id^='sig']", function(e) {
    signatures = signatures = $('#sig1').val() + "|" + $('#sig2').val() + "|" + $('#sig3').val() + "|" + $('#sig4').val();
});

$(document).on('input', "#expirDate", function (e) {
    if ($('#expirDate').val() == "") { $('#printReport').attr("disabled", "disabled"); }
    else { $('#printReport').removeAttr("disabled"); }
});

$(document).on('change', "#expirDate", function (e) {
    if ($('#expirDate').val() == "") { $('#printReport').attr("disabled", "disabled"); }
    else { $('#printReport').removeAttr("disabled"); }
});
