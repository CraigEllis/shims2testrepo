﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoCompleteSearch.ascx.cs" Inherits="HAZMAT.AutoCompleteSearch" %>
<link href="AutoCompleteSearch.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="AjAXAutoCompleteUtilities.js" language="javascript"></script>
<script type="text/javascript" language="javascript">    
    // intMatchKeys: specifies minimum number of keystrokes necessary before match is attempted
    var intMatchKeys = <%= MatchKeyCount %>;
    
    // blnEnableAutoPostBack: specifies whether to submit the form on selection
    var blnAutoPostBack = "<%= AutoPostBack %>" == "True";
    
    // blnEnableAutoPostBack: specifies whether to submit the form on selection
    var strSubmitButtonName = "<%= UniqueID %>_buttonSearch";
    
    // strQueryBoxName: sets the name of the QueryBox
    var strQueryBoxName = '<%= UniqueID.Replace("$", "_") %>_txtQuery';
    
    // strResultDivName: specifies the name of the Result Div
    var strResultDivName = "AutoCompleteResults";
    
    // strOptionCSS: sets the name of the normal Option CSS class
    var strOptionCSS = "<%= ListOptionCSSClass %>";
    
    // strOptionHightlightedCSS: sets the name of the highlighted Option Css class
    var strOptionHighlightedCSS = "<%= ListOptionHighlightedCSSClass %>"; 
    
    // ** function GetAutoComplete **
    // parameters: none
    // performs: Checks if MatchKeys requirement has been met and fires the Autofill
    // returns: none
    function GetAutoComplete()
    {
        var txtQuery = getObj(strQueryBoxName);
        if(txtQuery.value.length >= intMatchKeys)
        {
            var strQuery = txtQuery.value;
            //alert(strQuery);
            var strContext="";
            <%= CallbackScriptBlock %>
        } 
        else 
        {
            divResults.style.display = "none"
        }
    }
</script>
<div id="AutoCompleteWrapper" class="<%= AutoCompleteWrapperCssClass %>">
<asp:TextBox runat="server" AutoCompleteType="None" ID="txtQuery" autocomplete="off"></asp:TextBox>
    <!--<asp:Button runat="server" ID="buttonSearch" OnClick="buttonSearch_Click" />-->
    <div onmouseout="javascript: var d=getObj(strResultDivName); d.display = 'hidden';" id="AutoCompleteResults" class="<%= ListCSSClass %>" style="display: none"></div>
</div>