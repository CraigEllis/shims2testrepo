﻿using HAZMAT.Api.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace HAZMAT
{
    public class SFRBuilder
    {
        public MemoryStream GetPdf(int sfr_id)
        {
            DatabaseManager dbManager = new DatabaseManager();
            SFRModel sfr = dbManager.getSfrById(sfr_id);
            DataTable dt = dbManager.GetBlankSfrModal();
            string ui = dbManager.GetUiById(sfr.ui_id);
            return StampPdf(sfr, dt, ui);
        }

        public MemoryStream StampPdf(SFRModel sfr, DataTable dt, string ui)
        {
            PdfReader reader = new PdfReader(HttpContext.Current.Server.MapPath("/resources/SFR.pdf"));
            var outputStream = new MemoryStream();
            PdfStamper stamper = new PdfStamper(reader, outputStream);
            stamper.AcroFields.SetField("HullNumber", dt.Rows[0]["hull_number"].ToString());
            stamper.AcroFields.SetField("UIC", dt.Rows[0]["uic"].ToString());
            stamper.AcroFields.SetField("POCName", dt.Rows[0]["POC_Name"].ToString());
            stamper.AcroFields.SetField("POCPhone", dt.Rows[0]["POC_Telephone"].ToString());
            if (sfr.sfr_type == 0)
            {
                stamper.AcroFields.SetField("RecommendedAction", "Add");
            }
            if (sfr.sfr_type == 1)
            {
                stamper.AcroFields.SetField("RecommendedAction", "Delete");
            }
            else
            {
                stamper.AcroFields.SetField("RecommendedAction", "Other");
            }
            stamper.AcroFields.SetField("NSN", sfr.fsc + " - " + sfr.niin);
            stamper.AcroFields.SetField("TradeName", sfr.item_name);
            stamper.AcroFields.SetField("PartNumber", sfr.part_no);
            stamper.AcroFields.SetField("SpecificationNumber", sfr.specification_number);
            stamper.AcroFields.SetField("UI", ui);
            stamper.AcroFields.SetField("Manufacturer", sfr.manufacturer);
            stamper.AcroFields.SetField("ManufacturerAddress", sfr.mfg_address);
            stamper.AcroFields.SetField("Cage", sfr.cage);
            stamper.AcroFields.SetField("ManufacturerCity", sfr.mfg_city);
            stamper.AcroFields.SetField("ManufacturerState", sfr.mfg_state);
            stamper.AcroFields.SetField("ManufacturerZip", sfr.mfg_zip);
            stamper.AcroFields.SetField("ManufacturerPOC", sfr.mfg_poc);
            stamper.AcroFields.SetField("ManufacturerPhone", sfr.mfg_phone);
            stamper.AcroFields.SetField("MaterialUse", sfr.system_equipment_material);
            stamper.AcroFields.SetField("MethodOfApplication", sfr.method_of_application);
            stamper.AcroFields.SetField("ProposedUsage", sfr.proposed_usage);
            stamper.AcroFields.SetField("NegativeImpact", sfr.negative_impact);
            stamper.AcroFields.SetField("SpecialTrainingRequirements", sfr.special_training);
            stamper.AcroFields.SetField("Precautions", sfr.precautions);
            stamper.AcroFields.SetField("Properties", sfr.properties);
            if (sfr.msds_attached == true)
            {
                stamper.AcroFields.SetField("MSDSYes", "X");
            }
            else
            {
                stamper.AcroFields.SetField("MSDSNo", "X");
            }
            stamper.AcroFields.SetField("Advantages", sfr.advantages);
            stamper.AcroFields.SetField("Comments", sfr.comments);

           	stamper.Writer.CloseStream = false;
	        stamper.FormFlattening = true;
	        stamper.Close();
            return outputStream;
        }
    }
}