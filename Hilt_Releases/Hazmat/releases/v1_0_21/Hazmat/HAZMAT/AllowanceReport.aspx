﻿<%@ Page Title=""  StylesheetTheme="WINXP_Blue"  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllowanceReport.aspx.cs" Inherits="HAZMAT.AllowanceReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br /><br />
<table style="width: 430px"><tr><td>
    <asp:Image ID="Image1" runat="server" ImageUrl="images/hazard_icon.png"  /></td><td>
    <asp:Label  ID="Label1" runat="server" Text="Reports"
     SkinID="LabelPageHeader"></asp:Label></td>
     <td align="right">
                <asp:ImageButton ID="btnFeedback" runat="server" ImageUrl="images/feedback.gif" 
                    ToolTip="Click to provide feedback" onclick="btnFeedback_Click" />
                    <asp:ImageButton ID="btnHelp" runat="server" ImageUrl="images/help.gif" 
                    ToolTip="Click to access manuals and training material" onclick="btnHelp_Click" />
            </td></tr></table>
     
        
        <asp:Button runat="server" ID="btnExcelReport" OnClick="btnExcelReport_Click" Text="Download Dashboard Excel Reports" /> 
        <br /><br />

    Choose Report:<asp:DropDownList ID="DropDownListReport" runat="server">
          
    </asp:DropDownList>
    <asp:Button runat="server" ID="btnChooseReport" OnClick="btnChooseReport_Click" Text="Submit" />
    <br /><br />

  <table class="filterTable" >
    <tr><td>
        Filter:</td><td><asp:CheckBox ID="chkbox_filter" runat="server" /></td><td>
<asp:DropDownList ID="DropDownListSearch" runat="server">
            <asp:ListItem>NIIN</asp:ListItem>
            <asp:ListItem Value="Description">Nomenclature</asp:ListItem>
            <asp:ListItem>CAGE</asp:ListItem>
            <asp:ListItem>SPECS</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="TxtBoxSearch" runat="server" ></asp:TextBox>
    </td>
    </tr>
       <tr><td>

     Usage Category:</td><td>
    <asp:CheckBox ID="chkbox_usageCategory" runat="server" /></td><td>
    <asp:DropDownList ID="DropDownListUsageCategory" runat="server">
        </asp:DropDownList>
        
        </td>
    </tr>
       <tr><td>

    Inventory Group:</td><td>
    <asp:CheckBox ID="chkbox_catalogGroup" runat="server" /></td><td>
    <asp:DropDownList ID="DropDownListCatalogGroup" runat="server" Width="374px">
        </asp:DropDownList>
        <asp:Button ID="btn_filter" runat="server" Text="Apply Filter(s)" 
        />
     </td>
    </tr>
    </table>
    <br />
   
  
            <div class="tableHeaderDivReport" style="width:95%;">
    <asp:Label CssClass="floatLeft" ID="Label2" runat="server" SkinID="LabelTableHeaderReport" Text="Quantities Over Allowance" 
               Font-Bold="True"></asp:Label>
                
               <br />
               <br />
                <asp:GridView CssClass="gridWidth" DataKeyNames=""
                    SkinID="GridViewReport" ID="AllowanceGridView" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False"
                    ShowFooter="True" 
                    EmptyDataText=" No data to display" 
                    onpageindexchanging="AllowanceGridView_PageIndexChanging">
                    <Columns>
                        <asp:BoundField Visible="false" DataField="niin_catalog_id" HeaderText="ms_catalog_id" SortExpression="niin_catalog_id" />
                      <asp:BoundField DataField="COSAL" HeaderText="COSAL" />            
                        <asp:BoundField DataField="NIIN" HeaderText="NIIN" />
                        <asp:BoundField DataField="AT" HeaderText="AT" />
                        <asp:BoundField DataField="description" HeaderText="Nomenclature" />
                        <asp:BoundField DataField="usage_category" HeaderText="Use Category" />  
                        <asp:BoundField DataField="SPECS" HeaderText="SPECS" Visible="false" /> 
                        <asp:BoundField DataField="SPMIG" HeaderText="SPMIG" />                      
                        <asp:BoundField DataField="COG" HeaderText="COG" />
                        <asp:BoundField DataField="storage_type" HeaderText="Storage" Visible="false" />
                        <asp:BoundField DataField="shelf_life_code" HeaderText="SLC" />
                        <asp:BoundField DataField="shelf_life_action_code" HeaderText="SLAC" />
                         
                        <asp:TemplateField HeaderText="U/I">
                <ItemTemplate>
                 <%# ((System.Data.DataRowView)Container.DataItem)["alternate_ui"] == DBNull.Value ? ((System.Data.DataRowView)Container.DataItem)["ui"] : ((System.Data.DataRowView)Container.DataItem)["alternate_ui"]%>
		  </ItemTemplate>                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="U/M">
                <ItemTemplate>
                 <%# ((System.Data.DataRowView)Container.DataItem)["alternate_um"] == DBNull.Value ? ((System.Data.DataRowView)Container.DataItem)["um"] : ((System.Data.DataRowView)Container.DataItem)["alternate_um"]%>
		  </ItemTemplate>                
            </asp:TemplateField>

              <asp:TemplateField HeaderText="Allowance Qty">
                <ItemTemplate>
                 <%# (((System.Data.DataRowView)Container.DataItem)["alternate_ui"] == DBNull.Value && ((System.Data.DataRowView)Container.DataItem)["alternate_um"] == DBNull.Value) ? ((System.Data.DataRowView)Container.DataItem)["allowance_qty"] : "?"%>
		  </ItemTemplate>                
            </asp:TemplateField>
            <asp:BoundField DataField="qty" HeaderText="Current Qty" />
                          <asp:TemplateField HeaderText="Amount Over Allowance">
                <ItemTemplate>
                 <%# ((System.Data.DataRowView)Container.DataItem)["alternate_ui"] == DBNull.Value ? ((System.Data.DataRowView)Container.DataItem)["qty_difference"] : "?"%>
		  </ItemTemplate>                
            </asp:TemplateField>                      
                    </Columns>
                   
                </asp:GridView>
                </div>
    <br />
    <br />
</asp:Content>
