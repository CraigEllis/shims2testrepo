﻿using System;
using System.Web.UI;

namespace HAZMAT
{
    public partial class _Default : Page
    {
            [CoverageExclude]
            protected void Page_Load(object sender, EventArgs e)
            {
                // Call up the SMCL page
                Response.Redirect("SMCL.aspx");
            }
        }
    }
