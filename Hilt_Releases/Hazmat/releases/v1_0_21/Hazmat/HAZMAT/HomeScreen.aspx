﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomeScreen.aspx.cs" 
    Inherits="HAZMAT.HomeScreen" %>

<%@ Register TagPrefix="uc" TagName="SMCLDetails"
    Src="~/Modals/SMCLDetailsModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="Reports"
    Src="~/Modals/ReportsModal.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Shortcut Icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/select2.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/smoothness/jquery-ui.structure.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/smoothness/jquery-ui.theme.min.css" />
    <!-- jQuery -->
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/resources/js/moment.min.js"></script>
    <script type="text/javascript" src="/resources/js/style.js"></script>
    <script type="text/javascript" src="/resources/js/site.js"></script>
    <script type="text/javascript" src="/resources/js/select2.js"></script>
    <script type="text/javascript" src="/resources/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.get("api/HomeScreen/LoadInitialScreen", function (data) {
                //populate all the various dropdowns  
                var niinList = $('#NIINList');
                var option = '';
                for (i = 0; i < data.niins.length; i++) {
                    option += '<option value="' + data.niins[i] + '">' + data.niins[i] + '</option>';
                }
                niinList.append(option);
                option = '';
                var usageCategories = $('#dd_usagecategory');
                for (i = 0; i < data.usageCategories.length; i++) {
                    option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + ' - ' + data.usageCategories[i].description + '</option>';
                }
                usageCategories.append(option);
                option = '';
                var smccs = $('#dd_smcc');
                for (i = 0; i < data.smccs.length; i++) {
                    option += '<option value="' + data.smccs[i].smcc_id + '">' + data.smccs[i].smcc + ' - ' + data.smccs[i].description + '</option>';
                }
                smccs.append(option);
                //tip of the day!
                $('#tiptext').text(data.tip);

                $('#NIINList').select2({ allowClear: true });

            });
        });

        $(document).on('click', '#btn-details', function (e) {
            e.preventDefault();
            var niin = $('#NIINList').val();
            populateSMCLDetails(niin);
        });

        $(document).on('click', '#reports', function (e) {
            e.preventDefault();
            openReportModal();
        });
    </script>
    <title>SHIMS 2</title>
</head>
<body style="background-image: url('../../images/submarine.jpg'); background-repeat:repeat ; background-size: 100%;">
    <form runat="server">
        <div class="row" style="margin-right:0px">
            <div class="col-md-6">
                <div class="row" style="margin-right:0px">
                    <div class="col-md-5" style="padding:0px 30px; text-align:center">
                        <h1 style="font-family:Impact; font-size:710%; color:#ffd800; text-shadow: 3px 3px 5px #000000; 
                            margin: 40px 0 0px; letter-spacing:-4px" class="page-header">SHIMS</h1>
                    </div>
                    <div class="col-md-7" style="margin-top:47px; font-size:235%; color:#BBB; text-align:center">
                        <small style="font-family:Impact; text-shadow: 2px 2px 5px #000000">Submarine Hazardous Material Inventory & Management System</small>
                    </div>
                </div>
                <div class="row" style="margin-right:0px">
                    <div class="col-md-4" style="text-align:center">
                        <h4 runat="server" id="ShipName" style=""></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="padding-top:20px">
                <!--<div class="col-md-2" style="padding-top: 50px; ">-->
                    <img src="../../images/navsea.png" style="margin 0 auto; padding:10px 20px"/>
                <!--</div>-->
                <!--<div class="col-md-2" style="padding-top: 20px;">-->
                    <img src="../../images/comsubpac.gif" style="margin 0 auto; padding:10px 20px"/>
                <!--</div>-->
                <!--<div class="col-md-2" style="padding-top: 20px;">-->
                    <img src="../../images/comsublant.png" style="margin 0 auto; padding:10px 20px" />
                <!--</div>-->
                    <img src="../../images/crest.png" style="margin 0 auto; max-width:149px; max-height:155px; padding:10px 20px" />
            </div>
        </div>
        <div class ="row" style="margin-right:0px">
           <div id="wrapper" style="padding:0px">              
                <div class="col-md-3">
                    <div id="sidebar" style="margin:0 auto; position:relative">
                        <ul class="sidebar-nav" style="background-color: #C5C6C7; border: solid 1px white; position:relative">
                            <li class="sidebar-brand"><a href="SMCL.aspx">SMCL</a></li>
                            <li class="sidebar-brand"><a href="Inventory.aspx">Inventory Management</a></li>
                            <li class="sidebar-brand"><a href="Offload.aspx">Offload Management</a></li>
                            <li class="sidebar-brand"><a id="reports"style="cursor:pointer">Reports</a></li>
                            <li class="sidebar-brand"><a href="#">Reference Library</a></li>
                            <li style="padding:5px 0px 10px 8px"><b>Quick Search: Select a NIIN</b>
                                <div id="dropdown-1" class="dropdown dropdown-tip">
                                    <select id="NIINList"></select>
                                    <button class="btn-info" id="btn-details">Go!</button>
                                </div>
                            </li>
                  <!--  <li style="padding-top: 20px; padding-left: 5px"><b>Tip of the Day</b>
                        <div id="tipoftheday">
                            <p id="tiptext"></p>
                        </div>
                    </li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div id="tipBox" style="margin:0 auto; position:relative">
                        <ul class="sidebar-nav" style="background-color: #C5C6C7; border: solid 1px white; position:relative">
                            <li style="padding-top: 10px; padding-left: 5px">
                                <b style="font-size:20px; text-decoration:underline">Tip of the Day</b>
                                <div id="tipoftheday" style="margin-right:10px">
                                    <p id="tiptext"></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
           <!-- <div id="page-content-wrapper">
                <div class="page-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- content of page -->
                          <!--  </div>
                        </div>
                    </div>
                </div>
            </div>-->
       </div>
    </form>

    <uc:SMCLDetails ID="SMCLDetailsModal" runat="server"></uc:SMCLDetails>
    <uc:Reports ID="ReportsModal" runat="server"></uc:Reports>

</body>
</html>
