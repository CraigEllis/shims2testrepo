﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="SFR.aspx.cs" Inherits="HAZMAT.SFRPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 43px;
        }

        .spaced {
            padding: 10px 40px 10px 40px;
        }
    </style>
    <script type="text/javascript" src="/resources/js/SFR.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>SMCL Feedback Report</h1>
    </div>
    <button class="btn btn-primary" id="btnBlankSFR" style="margin-left: 10px; margin-top: 10px;">Complete a Blank SFR</button>
    <hr />
    <table id="sfrTable" class="table">
        <thead>
            <th>FSC</th>
            <th>NIIN</th>
            <th>CAGE</th>
            <th>Description</th>
            <th>Date Submitted</th>
            <th>Date Mailed</th>
            <th>SFR Details</th>
            <th>PDF</th>
            <th>SFR Id</th>
        </thead>
    </table>
    <hr />
    <br />

    <div class="modal" id="SFRModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" class="close">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title"><b>SFR</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Submarine/Hull No.: </label>
                            <label id="hullLabel"></label>
                            <br />
                            <label>UIC: </label>
                            <label id="uicLabel"></label>
                            <br />
                            <label>SFR Type</label>
                            <select class="sfr" name="sfr_type">
                                <option value="0">Add</option>
                                <option value="1">Delete</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Ship's Point of Contact: </label>
                            <label id="shipPocLabel"></label>
                            <br />
                            <label>Telephone: </label>
                            <label id="telephoneLabel"></label>
                        </div>
                    </div>
                    <hr />
                    <br />
                    <label style="font-size: small">Manufacturer Data</label>
                    <div style="border: 1px solid lightgrey; padding: 10px;">
                        <div class="row">
                            <div class="col-md-2">
                                <label style="font-size: smaller">FSC</label>
                                <input class="form-control sfr" name="fsc" id="sfr_fsc" maxlength="4" />
                            </div>
                            <div class="col-md-4">
                                <label class="req" style="font-size: smaller">NIIN</label>
                                <input class="form-control sfr" id="sfr_niin" name="niin" maxlength="9"/>
                            </div>
                            <div class="col-md-6">
                                <label style="font-size: smaller">Trade Name/Nomenclature</label>
                                <input class="form-control sfr" id="sfr_name" name="item_name" maxlength="100"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size: smaller">Part Number</label>
                                <input class="form-control sfr" id="sfr_part_no" name="part_number" maxlength="255" />
                            </div>
                            <div class="col-md-4">
                                <label style="font-size: smaller">Specification Number</label>
                                <input class="form-control sfr" id="sfr_spec_no" name="specification_number" maxlength="10"/>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size: smaller">UI</label>
                                <select class="form-control sfr" id="sfr_ui" name="ui_id"></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label style="font-size: smaller">Manufacturer</label>
                                <input class="form-control sfr" id="sfr_manufacturer" name="manufacturer" maxlength="255" />
                            </div>
                            <div class="col-md-6">
                                <label style="font-size: smaller">CAGE</label>
                                <input class="form-control sfr" id="sfr_cage" name ="cage" maxlength="5" />
                            </div>
                        </div>
                        <div>
                            <label style="font-size: smaller">Address</label>
                            <input class="form-control sfr" id="sfr_address" name="mfg_address" maxlength="255"/>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size: smaller">City</label>
                                <input class="form-control sfr" id="sfr_city" name="mfg_city" maxlength="255"/>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size: smaller">State</label>
                                <input class="form-control sfr" id="sfr_state" name="mfg_state" maxlength="20"/>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size: smaller">ZIP</label>
                                <input class="form-control sfr" id="sfr_zip" name="mfg_zip" maxlength="5"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label style="font-size: smaller">Point of Contact</label>
                                <input class="form-control sfr" id="sfr_poc" name="mfg_poc" maxlength="255"/>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size: smaller">Phone</label>
                                <input class="form-control sfr" id="sfr_poc_phone" name="mfg_phone" maxlength="25"/>
                            </div>
                        </div>
                    </div>
                    <br />
                    <label style="font-size: small">Technical Data</label>
                    <div style="border: 1px solid lightgrey; padding: 10px;">
                        <label style="font-size: smaller">System/Equipment/Material Use (including typical/average/maximum ambient and surface temperatures where material will be used.)</label>
                        <textarea class="form-control sfr" rows="3" id="sfr_material_use" name="system_equipment_material"></textarea>
                        <label style="font-size: smaller">Method of Application</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_method" name="method_of_application"></textarea>
                        <label style="font-size: smaller">Proposed Usage</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_proposed_usage" name="proposed_usage"></textarea>
                        <label style="font-size: smaller">Negative Impact of Not Having Material Available</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_negative_impact" name="negative_impact"></textarea>
                        <label style="font-size: smaller">Special Training Requirements</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_training" name="special_training"></textarea>
                        <label style="font-size: smaller">Precautions (including local/general ventilation, personal protection equipment, including respiratory protection to be used)</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_precautions" name="precautions"></textarea>
                        <label style="font-size: smaller">Properties (i.e. corrosiveness, reactivity, toxicity, etc)</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_properties" name="properties"></textarea>
                        <label style="font-size: smaller">MSDS Attached?</label>
                        <input type="checkbox" class="sfr" id="sfr_msdsYes" name="msds_attached" />Yes<br />
                        <label style="font-size: smaller">Advantages of Using this Material over Materials Used in the Past</label>
                        <textarea class="form-control sfr" rows="2" id="sfr_advantages" name="advantages"></textarea>
                        <label style="font-size: smaller">Comments</label>
                        <textarea class="form-control sfr" rows="1" id="sfr_comments" name="comments"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <label id="sfrSuccessLabel" style="color:green;"></label>
                    <button type="button" class="btn btn-primary" id="submitSFR">Submit</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

