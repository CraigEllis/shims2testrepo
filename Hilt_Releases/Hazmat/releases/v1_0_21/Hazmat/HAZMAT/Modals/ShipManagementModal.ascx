﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipManagementModal.ascx.cs" Inherits="HAZMAT.Modals.ShipManagementModal" %>

<div class="modal" id="shipManagementModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h class="modal-title"><b>Ship Management</b></h>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div style="margin-left: 10px; border: 3px lightgrey solid; padding: 10px" class="col-md-6">
                        <label>UIC: </label>
                        <select id="uic_select" style="width:100px"></select>
                        <br />
                        <br />
                        <label>Ship Name: </label>
                        <input class="form-control" id="shipName" disabled="disabled" />
                        <br />
                        <label>Address: </label>
                        <input class="form-control" id="shipAddress" />
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <label>City: </label>
                                <input class="form-control" id="shipCity" />
                            </div>
                            <div class="col-md-2">
                                <label>State: </label>
                                <input class="form-control" id="shipState" />
                                <br />
                            </div>
                            <div class="col-md-4">
                                <label>ZIP: </label>
                                <input class="form-control" id="shipZip" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Sub Type: </label>
                                <input class="form-control" id="shipSubType" disabled="disabled" />
                            </div>
                            <div class="col-md-6">
                                <label>Hull Number:  </label>
                                <input class="form-control" id="shipHullNumber" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>ACT Code: </label>
                                <input class="form-control" id="shipACT" />
                            </div>
                            <div class="col-md-4">
                                <label>BASE: </label>
                                <input class="form-control" id="shipBASE" />
                            </div>
                            <div class="col-md-4">
                                <label>SNDL: </label>
                                <input class="form-control" id="shipSNDL" />
                            </div>
                        </div>
                        <label style="padding-left: 15px; padding-top: 10px">Upload Ship Logo: </label>
                        <div class="row" style="padding-left: 15px; padding-top: 10px">
                            <div class="col-md-6">
                                <input id="logoUpload" type="file" /><label id="lblSuccess"></label>
                            </div>

                            <div class="col-md-4">
                                <input id="btnUploadLogo" type="button" value="Upload" />
                            </div>
                            <div class="col-md-2">
                                <label id="uploadLogoSuccess"></label>
                            </div>

                        </div>
                        <label style="padding-left: 15px; padding-top: 10px">Upload Ship Image: </label>
                        <div class="row" style="padding-left: 15px; padding-top: 10px">
                            <div class="col-md-6">
                                <input id="shipImageUpload" type="file" /><label id="lblShipImageSuccess"></label>
                            </div>
                            <div class="col-md-4">
                                <input id="btnUploadImage" type="button" value="Upload" />
                            </div>
                            <div class="col-md-2">
                                <label id="uploadImageSuccess"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="border: 3px lightgrey solid; padding: 10px;">
                            <label>POC Title: </label>
                            <input class="form-control" id="pocTitle" />
                            <label>POC Name:  </label>
                            <input class="form-control" id="pocName" />
                            <label>POC Address: </label>
                            <input class="form-control" id="pocAddress" />
                            <label>POC Phone: </label>
                            <input class="form-control" id="pocPhone" />
                            <label>POC Email:  </label>
                            <input class="form-control" id="pocEmail" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <label id="updateShipSuccessLabel"></label>
                <button class="btn btn-default" id="updateShipInfo">Update Ship Information</button>
                <button id="btn_Cancel" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $.get("api/ShipManagement/GetUICs", function (data) {
            var option = '';
            var uics = $('#uic_select');
            for (i = 0; i < data.uics.length; i++) {
                option += '<option value="' + data.uics[i].uic + '">' + data.uics[i].uic + '</option>';
            }
            uics.append(option);
            $('#uic_select').val(data.currentUic);
            $("#uic_select").select2({ allowClear: true });
            getCurrentShipData(data.currentUic);
        });
    });
    function getCurrentShipData(uic) {
        $.get("api/ShipManagement/GetCurrentShipData?uic=" + uic, function (data) {
            $('#shipName').val(data.shipData[0].name);
            $('#shipAddress').val(data.shipData[0].address);
            $('#shipCity').val(data.shipData[0].city);
            $('#shipState').val(data.shipData[0].state);
            $('#shipZip').val(data.shipData[0].zip);
            $('#shipSubType').val(data.shipData[0].hull_type);
            $('#shipHullNumber').val(data.shipData[0].hull_number);
            $('#shipACT').val(data.shipData[0].act_code);
            $('#shipBASE').val(data.shipData[0].base);
            $('#shipSNDL').val(data.shipData[0].sndl);
            $('#pocName').val(data.shipData[0].POC_Name);
            $('#pocEmail').val(data.shipData[0].POC_Email);
            $('#pocPhone').val(data.shipData[0].POC_Phone);
            $('#pocAddress').val(data.shipData[0].POC_Address);
            $('#pocTitle').val(data.shipData[0].POC_Title)
        });
    }
    $(document).on('change', '#uic_select', function (e) {
        getCurrentShipData($('#uic_select option:selected').text());
    });

    $(document).on('click', '#btnUploadLogo', function () {

        var data = new FormData();

        var files = $("#logoUpload").get(0).files;

        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("UploadedImage", files[0]);
        }

        // Make Ajax request with the contentType = false, and processDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: "/api/ShipManagement/UploadLogo",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                $('#uploadLogoSuccess').text(data);
            }
        });
    });
    $(document).on('click', '#btnUploadImage', function () {

        var data = new FormData();

        var files = $("#shipImageUpload").get(0).files;

        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("UploadedImage", files[0]);
        }

        // Make Ajax request with the contentType = false, and processDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: "/api/ShipManagement/UploadShipImage",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                $('#uploadImageSuccess').text(data);
            }
        });
    });
    $(document).on('click', '#updateShipInfo', function () {
        var source = {
            UIC: $('#uic_select').val(),
            ShipName: $('#shipName').val(),
            Address: $('#shipAddress').val(),
            City: $('#shipCity').val(),
            State: $('#shipState').val(),
            Zip: $('#shipZip').val(),
            SubType: $('#shipSubType').val(),
            HullNumber: $('#shipHullNumber').val(),
            ACTCode: $('#shipACT').val(),
            BASE: $('#shipBASE').val(),
            SNDL: $('#shipSNDL').val(),
            POCName: $('#pocName').val(),
            POCEmail: $('#pocEmail').val(),
            POCPhone: $('#pocPhone').val(),
            POCAddress: $('#pocAddress').val(),
            POCTitle: $('#pocTitle').val(),
        }
        $.ajax({
            type: "POST",
            url: "/api/ShipManagement/UpdateShipInfo",
            data: source,
            success: function () {
                updateCurrentShip($('#shipName').val());
                $('#updateShipSuccessLabel').text("Update successful.");
            }
        });
    });
</script>
