﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DbModal.ascx.cs" Inherits="HAZMAT.Modals.DbModal" %>

<div class="modal" id="dbModal">
    <div class="modal-dialog" style="width: 700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h class="modal-title"><b>Backup/Restore Database</b></h>
            </div>
            <div class="modal-body">
                <label style="font-size: small">Backup</label>
                <div style="border: 1px solid lightgrey; padding: 10px;">
                    Automatic Backup Frequency
                    <select id="backupFrequency" style="margin-bottom: 5px;">
                        <option label="Daily" value="4">Daily</option>
                        <option label="Weekly" value="8">Weekly</option>
                        <option label="Monthly" value="16">Monthly</option>
                    </select>
                    <br />
                    Backup Location:
                    <input style="font-size: small" id="backupLocation" disabled="disabled" />
                    <br />
                    Last Backup Date: 
                    <label style="font-size: small" id="lastBackupDate"></label>
                    <br />
                    <button class="btn btn-info btn-sm" id="updateBackupSettings" style="margin-top: 10px;">Update Backup Settings</button>
                    <button class="btn btn-info btn-sm" id="runBackup" style="margin-top: 10px;">
                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate load-icon" style="display: none"></span>
                        <span class="clicked" style="display: none">Backing up...</span>
                        <span class="not-clicked">Run Backup Now</span></button>
                    <label id="runBackupSuccess"></label>
                </div>
                <label style="font-size: small">Restore</label>
                <div style="border: 1px solid lightgrey; padding: 10px;">
                    <p>Select a .bak file to restore.</p>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-12">
                            <select id="existingBackups"></select>
                        </div>
                    </div>
                     <button class="btn btn-info btn-sm" id="restoreDatabase" style="margin-top: 10px;">
                         <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate load-icon-r" style="display: none"></span>
                         <span class="clicked-r" style="display: none">Restoring...</span>
                         <span class="not-clicked-r">Restore Database</span></button>
                            <label id="Label1"></label>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn_Cancel" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            getBackupSettings();
        });

        $(document).on('click', '#runBackup', function (e) {
            $('.clicked').show();
            $('.load-icon').show();
            $('.not-clicked').hide();
            $.get("api/Backup/RunBackup", function (data) {
                if (data == true) {
                    $('#runBackupSuccess').text('Backup started successfully.');
                }
                else {
                    $('#runBackupSuccess').text('Backup failed to start.');
                }
                var today = new Date();
                $('#lastBackupDate').text(today.getMonth() + 1 + '-' + today.getDate() + '-' + today.getFullYear());
                $('.clicked').hide();
                $('.load-icon').hide();
                $('.not-clicked').show();
                getBackupSettings();
            });
        });

        $(document).on('click', '#restoreDatabase', function (e) {
            $('.clicked-r').show();
            $('.load-icon-r').show();
            $('.not-clicked-r').hide();
            $.get("api/Backup/RestoreBackup?file=" + $('#existingBackups').val() + "&backupLocation=" + $('#backupLocation').val(), function (data) {
                if (data == true) {
                    $('#restoreBackupSuccess').text('Restore successful.');
                }
                else {
                    $('#restoreBackupSuccess').text('Restore was not successful.');
                }
                var today = new Date();
                $('.clicked-r').hide();
                $('.load-icon-r').hide();
                $('.not-clicked-r').show();
                location.reload();
            });
        });

        $(document).on('click', '#updateBackupSettings', function (e) {
            $.get("api/Backup/UpdateBackupSettings?frequency=" + $('#backupFrequency').val(), function (data) {
                    $('#runBackupSuccess').text('Update successful.');
            });
        });


        function getBackupSettings() {
            $('#existingBackups').empty();
            $.get("api/Backup/GetBackupSettings", function (data) {
                $('#backupFrequency').val(data.data[0].backup_frequency);
                $('#lastBackupDate').text(data.data[0].last_backup_date);
                $('#backupLocation').val(data.data[0].backup_location);
                var option = '';
                var baks = $('#existingBackups');
                for (var i = 0; i < data.baks.length; i++) {
                    option += '<option value="' + data.baks[i] + '">' + data.baks[i] + '</option>';
                }
                baks.append(option);
            });
        }
    </script>
