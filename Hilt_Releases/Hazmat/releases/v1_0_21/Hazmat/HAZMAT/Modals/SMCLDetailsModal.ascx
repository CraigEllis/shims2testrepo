﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMCLDetailsModal.ascx.cs" Inherits="HAZMAT.SMCLDetailsModal" %>

<div class="modal" id="SMCLDetails">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                    <h3 class="modal-title"><b>SMCL Details</b>
                        <a id="printSMCLDetails">
<%--                            <button class="btn btn-info btn-sm btn-details" type="button" style="float: right; margin-right: 30px;">Print</button></a>--%>
<%--                        <button class="btn btn-info btn-sm btn-details" id="btn-SMCLlabels" style="float: right; margin-right: 30px">ACM Labels</button>--%>
<%--                        <button class="btn btn-info btn-sm btn-details" id="btn-InvDetails" style="float: right; margin-right: 30px">Inventory Details</button>--%>
                    </h3>
            </div>
            <div class="modal-body" id="SMCLDetailsContents">
                <div class="row">
                    <div class="col-md-6">
                        <label style="font-size: small">Item Description</label>
                        <div style="border: 1px solid lightgrey; padding: 10px;">
                            <label style="font-size: smaller">COG-FSC-NIIN</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <input class="form-control" id="cogTxt" disabled="disabled" />
                                </div>
                                <div class="col-md-3">
                                    <input class="form-control" id="fscTxt" disabled="disabled" />
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" id="niinTxt" disabled="disabled" />
                                </div>
                            </div>

                            <label style="font-size: smaller">Description</label>
                            <input class="form-control" id="NomenclatureTxt" disabled="disabled" />

                            <label style="font-size: smaller">U/I</label>
                            <input class="form-control" id="UITxt" disabled="disabled" />

                            <label style="font-size: smaller">U/M</label>
                            <input class="form-control" id="UMTxt" disabled="disabled" />

                            <label style="font-size: smaller">SPMIG</label>
                            <input class="form-control" id="SPMIGTxt" disabled="disabled" />

                            <label style="font-size: smaller">SPECS</label>
                            <input class="form-control" id="SPECSTxt" disabled="disabled" />

                            <label style="font-size: smaller">CAGE</label>
                            <input class="form-control" id="CAGETxt" disabled="disabled" />

                            <label style="font-size: smaller">Manufacturer</label>
                            <input class="form-control" id="mfgTxt" disabled="disabled" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label style="font-size: small">Item Status</label>
                        <div style="border: 1px solid lightgrey; padding: 10px;">
                            <label style="font-size: smaller">Usage Category</label>
                            <select id="dd_usagecategory" class="form-control" disabled="disabled"></select>
                            <label style="font-size: smaller">NMCPHC Report</label>
                            <input id="NMCPHCTxt" class="form-control" disabled="disabled" />
                            <label style="font-size: smaller">HCC</label>
                            <input class="form-control" id="hccTxt" disabled="disabled" />
                            <label style="font-size: smaller">SMCC</label>
                            <select class="form-control" id="dd_smcc" disabled="disabled"></select>
                            <label style="font-size: smaller">MSDS Number</label>
                            <input class="form-control" id="msdsSerNumtxt" disabled="disabled" />
                        </div>
                        <br />
                            <div class="form-group">
                                <label style="font-size: small">Remarks</label>
                                <textarea class="form-control" id="remarksTxt" rows="5" disabled="disabled"></textarea>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn_Cancel" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click', '#btn-InvDetails', (function (e) {
        e.preventDefault();
        niin = $('#niinTxt').val();
        inventory_id = $(this).attr('data-inventory_id');
        inventory_detail_id = $(this).attr('data-inventory_detail_id');
        $('#printInvDetails').prop('href', 'api/Inventory/PrintInventoryDetails?inventory_onhand_id=' + inventory_id);
        $('#lbl_inventory_onhand_id').val(inventory_id);
        $('#lbl_inventory_detail_id').val(inventory_detail_id);
        $('#locationTxt').text($(this).attr('data-location'));
        getFilteredDropdowns(niin);
        $('#InventoryDetailsModal').css("z-index", "10000");
    }));
</script>