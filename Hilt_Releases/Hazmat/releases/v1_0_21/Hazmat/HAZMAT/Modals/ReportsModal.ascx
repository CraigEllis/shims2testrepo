﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportsModal.ascx.cs" Inherits="HAZMAT.Modals.ReportsModal" %>
    <script type="text/javascript" src="/resources/js/Reports.js"></script>
<style>
    
    li.active.a {
        background: #eeeeee;
    }
</style>

<div class="modal fade" id="reportsModal">
    <div class="modal-dialog modal-md"style="width:720px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                    <h3 class="modal-title"><b>Reports</b>
                        <button class="btn btn-info btn-sm btn-details" type="button" id="printReport" style="float: right; margin-right: 30px;">Print</button>
                        <button class="btn btn-info btn-sm btn-details" type="button" id="exportReport"style="float: right; margin-right: 30px; display:none">
                            <a href="api/Reports/ExportReport?extension=xlsx" id="export" style="color:#ffffff; padding:8px 10px 8px 10px; text-decoration:blink">Export</a>
                        </button>
                    </h3>
            </div>
            <div class="modal-body" id="reportsList" style="padding-bottom:0px">
                <div class="row">
                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" id="inv"class="active"><a href="#inventory"  style="color:#555555" id="invTab" aria-controls="inventory" role="tab" data-toggle="tab">Inventory</a></li>
                            <li role="presentation" id="acm"><a href="#acmlog"  style="color:#555555" id="acmTab" aria-controls="acmlog" role="tab" data-toggle="tab">ACM Log</a></li>
                            <li role="presentation" id="shelf"><a href="#shelflife"  style="color:#555555" id="shelfTab" aria-controls="shelflife" role="tab" data-toggle="tab">Shelf Life</a></li>
                            <li role="presentation" id="hws"><a href="#hwstowage"  style="color:#555555" id="hwsTab"aria-controls="hwstowage" role="tab" data-toggle="tab">HW Stowage</a></li>
                            <li role="presentation" id="incomp"><a href="#incompcheck"  style="color:#555555" id="incompTabNoSig"aria-controls="incompcheck" role="tab" data-toggle="tab">Incompatibility Check</a></li>
                            <li role="presentation" id="excel"><a href="#other"  style="color:#555555" id="otherTabNoSig"aria-controls="other" role="tab" data-toggle="tab">Other</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active in" data-id="inventory" id="inventory">
                            <div class="row" style="padding:30px 60px 30px 60px">
                                <div class="col-md-12"style="padding:10px 20px 10px 20px">
                                    <label style="font-size: 100%">INVENTORY</label>
                                </div>
                                <div class="row"style="border: 1px solid lightgrey; padding:10px 20px 10px 20px">
                                    <div class="col-md-6" id="type">
                                        <input type="radio" name="invtype" id="invAll"value="all" checked>ALL<br>
                                        <input type="radio" name="invtype" id="specWork" value="specWorkcenter">SPECIFIC WORKCENTER<br>
                                        <input type="radio" name="invtype" id="specLoc" value="specLocation">SPECIFIC LOCATION<br>
                                        <input type="radio" name="invtype" id="invVSmcl"value="invVsSmcl">INVENTORY VS SMCL<br>
                                        <input type="radio" name="invtype" id="invQtyIsZero"value="qtyIsZero">INVENTORY QTY IS ZERO<br>
                                    </div>
                                    <div id="sort">
                                        <div class="col-md-6 sort" id="invSort">
                                            <label style="font-size: small; margin-bottom:0px">SORT BY:</label><br/>
                                            <input type="radio" name="invsort" id="invNiin"value="niin" checked>NIIN<br>
                                            <input type="radio" name="invsort" id="invLoc"value="location">LOCATION<br>
                                            <input type="radio" name="invsort" id="invWork"value="wcenter">WORKCENTER<br>
                                            <input type="radio" name="invsort" id="invHcc"value="hcc">HCC<br>
                                        </div>
                                        <div class="col-md-6" id="invSpecSort" style="display:none">
                                            <select class="dropdown" id="invWorkSelect" style="top:25px"></select>
                                            <select class="dropdown" id="invLocSelect" style="top:48px"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div role="tabpanel" class="tab-pane fade" data-id="acmlog"id="acmlog">
                            <div class="row" style="padding:30px 60px 30px 60px">
                                <div class="col-md-12"style="padding:10px 20px 10px 20px">
                                    <label style="font-size: 100%">ACM LOG</label>
                                </div>
                                <div class="row"style="border: 1px solid lightgrey; padding:10px 20px 10px 20px">
                                    <div class="col-md-6" id="type">
                                        <input type="radio" name="acmtype" id="acmLimit"value="limited" checked>LIMITED<br>
                                        <input type="radio" name="acmtype" id="acmRestrict"value="restricted">RESTRICTED<br>
                                        <input type="radio" name="acmtype" id="acmProhib"value="prohibited">PROHIBITED<br>
                                        <input type="radio" name="acmtype" id="acmAll"value="all">ALL<br>
                                    </div>
                                    <div class="col-md-6" id="sort">
                                        <label style="font-size: small; margin-bottom:0px">SORT BY:</label><br/>
                                        <input type="radio" name="acmsort" id="acmNiin"value="niin" checked>NIIN<br>
                                        <input type="radio" name="acmsort" id="acmLoc"value="location">LOCATION<br>
                                        <input type="radio" name="acmsort" id="acmWork"value="workcenter">WORKCENTER<br>
                                    </div>
                                </div>
                            </div> 
                        </div>
                            <div role="tabpanel" class="tab-pane fade" data-id="shelflife" id="shelflife">
                            <div class="row" style="padding:30px 60px 30px 60px">
                                <div class="col-md-12"style="padding:10px 20px 10px 20px">
                                    <label style="font-size: 100%">SHELF LIFE</label>
                                </div>
                                <div class="row"style="border: 1px solid lightgrey; padding:10px 20px 10px 20px">
                                    <div class="col-md-6" id="type">
                                        <label style="font-size: small; margin-bottom:0px">EXPIRATION DATE:</label><br/>
                                        <input type="text" id="expirDate" id="shelfExpire" name="expDate" value=""/>
                                        <img class="ui-datepicker-trigger"src="/resources/css/calender.png"/>
                                    </div>
                                    <div class="col-md-6" id="sort">
                                        <label style="font-size: small; margin-bottom:0px">SORT BY:</label><br/>
                                        <input type="radio" name="expsort" id="shelfNiin" value="niin" checked>NIIN<br>
                                        <input type="radio" name="expsort" id="shelfLoc" value="location">LOCATION<br>
                                        <input type="radio" name="expsort" id="shelfWork" value="workcenter">WORKCENTER<br>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div role="tabpanel" class="tab-pane fade" data-id="hwstowage" id="hwstowage">
                            <div class="row" style="padding:30px 60px 30px 60px">
                                <div class="col-md-12"style="padding:10px 20px 10px 20px">
                                    <label style="font-size: 100%">HM STOWAGE</label>
                                </div>
                                <div class="row"style="border: 1px solid lightgrey; padding:10px 20px 10px 20px">
                                    <div class="col-md-6" id="sort">
                                        <input type="radio" name="sort" id="hwsWork" value="workcenter" checked>WORKCENTER<br>
                                        <input type="radio" name="sort" id="hwsLoc" value="location">LOCATION<br>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div role="tabpanel" class="tab-pane" data-id="incompcheck" id="incompcheck">
                            <div class="row" style="padding:30px 60px 30px 60px">
                                <div class="col-md-12"style="padding:10px 20px 10px 20px">
                                    <label style="font-size: 100%">INCOMPATIBILITY CHECK (HCC)</label>
                                </div>
                                <div class="row"style="border: 1px solid lightgrey; padding:10px 20px 10px 20px">
                                    <div class="col-md-6" id="type">
                                        <input type="radio" name="incomptype" id="incompAll" value="all" checked>ALL<br>
                                        <input type="radio" name="incomptype" id="incompSpecWork" value="specWorkcenter">SPECIFIC WORKCENTER<br>
                                        <input type="radio" name="incomptype" id="incompSpecLoc" value="specLocation">SPECIFIC LOCATION<br>
                                    </div>
                                    <div id="sort">
                                        <div class="col-md-6" id="incompSpecSort"style="display:none">
                                            <select class="dropdown" id="incompWorkSelect" style="top:25px"></select>
                                            <select class="dropdown" id="incompLocSelect" style="top:48px"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div role="tabpanel" class="tab-pane fade" data-id="other" id="other">
                            <div class="row" style="padding:30px 60px 30px 60px">
                                <div class="row"style="border: 1px solid lightgrey; padding:10px 20px 10px 20px">
                                    <div class="col-md-12"style="padding: 10px 20px 10px 20px; margin: 0 auto; left:184px">
                                    <div class="col-md-6" id="type">
                                        <input type="radio" name="exptype" id="toXls"value="xlsx" checked>Excel 97 - 2003 Workbook (*.xlsx)<br>
                                        <input type="radio" name="exptype" id="toHtml"value="html">HTML (*.html)<br>
                                        <input type="radio" name="exptype" id="toRtf"value="rtfx">Rich Text Format (*.rtf)<br>
                                        <input type="radio" name="exptype" id="toXml"value="xml">XML(*.xml)<br>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row " id="sigBlock"style="padding:0px 25px 0px 25px">
                    <div class="row" style=" padding:10px 20px 10px 20px">
                        
                    <div class="col-md-4">
                        <input type="checkbox" id="includeSig"style="margin-right:4px" ><span><b>Include Signature Block:</b></span><br/>
                    </div>
                    <div class="col-md-8" style="border: 1px solid lightgrey; padding:10px 15px 15px 15px">
                        <label style="font-size: small; margin-bottom:0px">SIGNATURE BLOCK:</label><br/>
                        <input type="text" style="margin-bottom:2px; width:100%" id="sig1" name="sig1" value="" disabled><br>
                        <input type="text" style="margin-bottom:2px; width:100%" id="sig2" name="sig2" value="" disabled><br>
                        <input type="text" style="margin-bottom:2px; width:100%" id="sig3" name="sig3" value="" disabled><br>
                        <input type="text" style="margin-bottom:2px; width:100%" id="sig4" name="sig4" value="" disabled><br>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="margin:0 auto">
                <button id="btn_Cancel" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on("click", "a[id$='Tab']", function(e) {
        $('#sigBlock').show();
    });

    $(document).on("click", "a[id$='NoSig']", function (e) {
        $('#sigBlock').hide();
    });

</script>
