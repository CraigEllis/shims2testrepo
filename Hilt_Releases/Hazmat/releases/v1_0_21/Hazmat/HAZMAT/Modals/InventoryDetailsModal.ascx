﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InventoryDetailsModal.ascx.cs" Inherits="HAZMAT.Modals.InventoryDetailsModal" %>

<div class="modal" id="InventoryDetailsModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title"><b>Inventory Details</b>
                    <a id="printInvDetails">
                        <button class="btn btn-info btn-sm btn-details" type="button" style="float: right; margin-right: 30px;">Print</button></a>
                    <button class="btn btn-info btn-sm btn-details" id="btn-labels" style="float: right; margin-right: 30px">ACM Labels</button>
                    <button class="btn btn-info btn-sm btn-details" id="btn-SMCLDetails" style="float: right; margin-right: 30px">SMCL Details</button>
                </h3>
            </div>
            <div class="modal-body" id="inv-modal-body-section">
                <label id="locationTxt" style="visibility: hidden"></label>
                <div class="row">
                    <div class="col-md-6">
                        <label style="font-size: small">Item Description</label>
                        <div style="border: 1px solid lightgrey; padding: 10px;">
                            <label style="font-size: smaller">FSC-NIIN</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <input class="form-control" id="inv_FSC_label" disabled="disabled" />
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control" id="inv_NIIN_label" disabled="disabled" />
                                </div>
                            </div>
                            <label style="font-size: smaller">Description</label>
                            <input class="form-control" id="inv_description_label" disabled="disabled" />

                            <label style="font-size: smaller">
                                U/I</label><br />
                            <select id="inv_ui" disabled="disabled"></select>
                            <br />
                            <label style="font-size: smaller">U/M</label>
                            <input class="form-control" id="inv_um_label" disabled="disabled" />

                            <label style="font-size: smaller">SPMIG</label>
                            <input class="form-control" id="inv_spmig_label" disabled="disabled" />

                            <label style="font-size: smaller">SPECS</label>
                            <input class="form-control" id="inv_specs_label" disabled="disabled" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label style="font-size: small">Item Status</label>
                        <div style="border: 1px solid lightgrey; padding: 10px;">
                            <label style="font-size: smaller">Usage Category</label>
                            <select id="inv_usage_category" class="form-control" disabled="disabled"></select>
                            <label style="font-size: smaller">Shelf Life Code</label>
                            <br />
                            <select id="inv_slc" disabled="disabled" style="max-width: 500px"></select>
                            <br />
                            <label style="font-size: smaller">Shelf Life Action Code</label>
                            <br />
                            <select id="inv_slac" disabled="disabled" style="max-width: 500px"></select>
                            <br />
                            <label style="font-size: smaller">HCC</label>
                            <br />
                            <select id="inv_hcc" disabled="disabled" style="max-width: 500px"></select>
                            <br />
                            <label style="font-size: smaller">SMCC</label>
                            <select class="form-control" id="inv_smcc" disabled="disabled"></select>
                        </div>
                        <br />
                        <div class="form-group">
                            <label style="font-size: small">Notes</label>
                            <textarea class="form-control" id="inv_remarks_text" rows="2" disabled="disabled"></textarea>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-12" style="padding: 25px">
                            <label style="font-size: small">On-Hand Inventory</label>
                            <div style="border: 1px solid lightgrey; padding: 10px;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label style="font-size: smaller">Total Quantity Available</label>
                                        <input class="form-control" id="inv_total_qty" disabled="disabled" />
                                    </div>
                                    <div class="col-md-3">
                                        <label style="font-size: smaller">Quantity Allowed</label>
                                        <input class="form-control" id="inv_qty_allowed" disabled="disabled" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="invDetailsTable" class="table invTable">
                                            <thead>
                                                <th>Location</th>
                                                <th>CAGE</th>
                                                <th>Manufacturer</th>
                                                <th>Qty</th>
                                                <th>Workcenter</th>
                                                <th>Inventoried Date</th>
                                                <th>Onboard Date</th>
                                                <th>Expiration Date</th>
                                                <th>InventoryDetailIdHidden</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <label id="successLabelInv" style="padding-right: 20px;"></label>
                <button id="btn_updateInvItem" class="btn btn-info">Update</button>
                <button class="btn btn-default" data-dismiss="modal">Close</button>
                <label id="lbl_inventory_onhand_id" style="visibility: collapse;"></label>
                <label id="lbl_inventory_detail_id" style="visibility: collapse;"></label>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function prepareInventoryModal(niin, inventory_detail_id, inventory_onhand_id, location) {
        $('#printInvDetails').prop('href', 'api/Inventory/PrintInventoryDetails?inventory_onhand_id=' + inventory_onhand_id);
        $('#lbl_inventory_onhand_id').val(inventory_onhand_id);
        $('#lbl_inventory_detail_id').val(inventory_detail_id);
        $('#locationTxt').text(location);
        getFilteredDropdowns(niin, inventory_onhand_id);
    }

    function getFilteredDropdowns(niin, inventory_id) {
        $.get("api/Inventory/getFilteredDropdowns?niin=" + niin, function (data) {
            cageList = data.cages;
            manufacturerList = data.manufacturers;
            populateInvDetails(niin, inventory_id);
        });
    }

    function populateInvDetails(niin, inventory_id) {
        $.get("api/Inventory/GetInventoryDetailsModal?inventory_id=" + inventory_id, function (data) {
            var $modal = $('#InventoryDetailsModal');
            $modal.find('#inv_FSC_label').val(data.aaData[0].fsc);
            $modal.find('#inv_NIIN_label').val(data.aaData[0].niin);
            $modal.find('#inv_description_label').val(data.aaData[0].description);
            $modal.find('#inv_ui_label').val(data.aaData[0].ui);
            $modal.find('#inv_um_label').val(data.aaData[0].um);
            $modal.find('#inv_spmig_label').val(data.aaData[0].spmig);
            $modal.find('#inv_specs_label').val(data.aaData[0].specs);
            $modal.find('#inv_usage_category').val(data.aaData[0].usage_category_id);
            $modal.find('#inv_slc').val(data.aaData[0].shelf_life_code_id);
            $modal.find('#inv_slac').val(data.aaData[0].shelf_life_action_code_id);
            //add select population for hcc and ui
            if (data.aaData[0].hcc != null && data.aaData[0].hcc_description != null) {
                $modal.find('#inv_hcc').val(data.aaData[0].hcc + ' - ' + data.aaData[0].hcc_description);
            }
            $modal.find('#inv_smcc').val(data.aaData[0].smcc_id);
            $modal.find('#inv_remarks_text').val(data.aaData[0].remarks);
            $modal.find('#inv_total_qty').val(data.totalQuantity);
            $modal.find('#inv_qty_allowed').val(data.aaData[0].allowance_quantity);

            //only users above the bottom level (user) can update stuff
            if (!(data.userRole === "BASE USER")) {
                $('#inv_qty_allowed').prop("disabled", false);
                $('#inv_slc').prop("disabled", false);
                $('#inv_slac').prop("disabled", false);
                $('#inv_hcc').prop("disabled", false);
                $('#inv_remarks_text').prop("disabled", false);
                $('#inv_ui').prop("disabled", false);
                $('#inv_um_label').prop("disabled", false);
            }
            else {
                $('#inv_qty_allowed').prop("disabled", true);
                $('#inv_slc').prop("disabled", true);
                $('#inv_slac').prop("disabled", true);
                $('#inv_hcc').prop("disabled", true);
                $('#inv_remarks_text').prop("disabled", true);
                $('#inv_ui_label').prop("disabled", true);
                $('#inv_um_label').prop("disabled", true);
            }

            if (userRole != "BASE USER") {
                var table = $('#invDetailsTable').DataTable();
                table.ajax.reload();
                editor.field('cage').update(cageList);
                editor.field('manufacturer').update(manufacturerList);
            }
            else {
                $('.invTable').css('visibility', 'hidden');
            }
            $("#inv_ui").select2({ allowClear: true });
            $("#inv_slc").select2({ allowClear: true });
            $("#inv_slac").select2({ allowClear: true });
            $("#inv_hcc").select2({ allowClear: true });
            $modal.modal('show');
        });
    }

    function populateInvDetailModalDropdowns(data) {
        var option = '';
        var usageCategories = $('#inv_usage_category');
        for (i = 0; i < data.usageCategories.length; i++) {
            option += '<option value="' + data.usageCategories[i].usage_category_id + '">' + data.usageCategories[i].category + ' - ' + data.usageCategories[i].description + '</option>';
        }
        usageCategories.append(option);
        var option = '';
        var shelfLifeCodes = $('#inv_slc');
        for (i = 0; i < data.shelfLifeCodes.length; i++) {
            option += '<option value="' + data.shelfLifeCodes[i].shelf_life_code_id + '">' + data.shelfLifeCodes[i].slc + ' - ' + data.shelfLifeCodes[i].description + '</option>';
        }
        shelfLifeCodes.append(option);
        var option = '';
        var hccs = $('#inv_hcc');
        for (i = 0; i < data.hccs.length; i++) {
            option += '<option value="' + data.hccs[i].hcc_id + '">' + data.hccs[i].hcc + ' - ' + data.hccs[i].description + '</option>';
        }
        hccs.append(option);
        var option = '';
        var uis = $('#inv_ui');
        for (i = 0; i < data.uis.length; i++) {
            option += '<option value="' + data.uis[i].ui_id + '">' + data.uis[i].abbreviation + ' - ' + data.uis[i].description + '</option>';
        }
        uis.append(option);
        var option = '';
        var slacs = $('#inv_slac');
        for (i = 0; i < data.slacs.length; i++) {
            option += '<option value="' + data.slacs[i].shelf_life_action_code_id + '">' + data.slacs[i].slac + ' - ' + data.slacs[i].description + '</option>';
        }
        slacs.append(option);
        var option = '';
        var smccs = $('#inv_smcc');
        for (i = 0; i < data.smccs.length; i++) {
            option += '<option value="' + data.smccs[i].smcc_id + '">' + data.smccs[i].smcc + ' - ' + data.smccs[i].description + '</option>';
        }
        smccs.append(option);
    }

    function loadTableForModal() {
        if (userRole != "BASE USER") {
            editor = new $.fn.dataTable.Editor({
                ajax: '/api/Inventory/UpdateInventoryData',
                table: '#invDetailsTable',
                idSrc: 'id',
                fields: [{
                    label: "Location",
                    name: "location_id",
                    type: "select",
                    ipOpts: locationList
                }, {
                    label: "CAGE",
                    name: "cage",
                    type: "select",
                    ipOpts: cageList
                }, {
                    label: "Manufacturer",
                    name: "manufacturer",
                    type: "select",
                    ipOpts: manufacturerList
                }, {
                    label: "Qty",
                    name: "qty"
                }, {
                    label: "Inventoried Date",
                    name: "inv_date",
                    type: "date",
                    dateFormat: 'mm/dd/yy',
                    "dateImage": '/resources/css/calender.png'
                }, {
                    label: "Onboard Date",
                    name: "onboard_date",
                    type: "date",
                    dateFormat: 'mm/dd/yy',
                    "dateImage": '/resources/css/calender.png'
                }, {
                    label: "Expiration Date",
                    name: "expiration_date",
                    type: "date",
                    dateFormat: 'mm/dd/yy',
                    "dateImage": '/resources/css/calender.png'
                }
                ]
            });

            editor.on('preSubmit', function (e, data, action) {
                now = new Date();
                var strDateTime = [AddZero(now.getMonth() + 1), AddZero(now.getDate()), now.getFullYear()].join("/");

                function AddZero(num) {
                    return (num >= 0 && num < 10) ? "0" + num : num + "";
                }
                data.data.inv_date = strDateTime;

                if ((data.data.qty != parseInt(data.data.qty, 10)) || (data.qty < 0)) {
                    this.error('qty', 'Quantity must be a positive whole number.');
                    return false;
                }
                data.data.inventory_detail_id = $('#lbl_inventory_detail_id').val();
            });

            editor.on('edit', function (e, data, action) {
                $.get("api/Inventory/GetTotalQty?inventory_id=" + inventory_id, function (qty) {
                    $('#inv_total_qty').val(qty);
                });
                var table = $('#inventoryTable').DataTable()
                table.ajax.reload();
            });

            editor.on('create', function (e, data, action) {
                $.get("api/Inventory/GetTotalQty?inventory_id=" + inventory_id, function (qty) {
                    $('#inv_total_qty').val(qty);
                });
                var table = $('#inventoryTable').DataTable()
                table.ajax.reload();
            });

            $('#invDetailsTable').on('click', '.editable', function (e) {
                editor.inline(this, { submitOnBlur: true });
            });


            $('#invDetailsTable').on('click', '.bubble', function (e) {
                editor.bubble(this);
            });

            $('#invDetailsTable').dataTable({
                dom: "Tfrtip",
                "searching": false,
                "ajax": {
                    "url": "/api/Inventory/LoadInventoryDetailsTable",
                    "type": "GET",
                    "data": function (d) {
                        d.inventory_id = $('#lbl_inventory_onhand_id').val();
                    }
                },
                "columnDefs": [
                    { className: "editable", "targets": [0, 2, 3] },
                     { className: "bubble", "targets": [5, 6, 7] },
                         {
                             "targets": [8],
                             "visible": false,
                             "searchable": false
                         }
                ],
                "columns": [
                    { "data": "location_id" },
                    { "data": "cage" },
                    { "data": "manufacturer" },
                    { "data": "qty" },
                    { "data": "workcenter_id" },
                    { "data": "inv_date" },
                    { "data": "onboard_date" },
                    { "data": "expiration_date" },
                    { "data": "inventory_detail_id" }
                ],
                tableTools: {
                    sRowSelect: "os",
                    "aButtons": [
                    { sExtends: "editor_create", editor: editor },
                    ]
                }
            });
        }
    }

    $(document).on('click', '#btn_updateInvItem', function (e) {
        e.preventDefault();
        var newId = $('#lbl_inventory_onhand_id').val();
        var newNiin = $('#inv_NIIN_label').val();
        var newUI = $('#inv_ui_label').val();
        var newUM = $('#inv_um_label').val();
        var newSLC = $('#inv_slc').val();
        var newSLAC = $('#inv_slac').val();
        var newHCC = $('#inv_hcc_slac').val();
        var newRemarks = $('#inv_remarks_text').val();
        var newQtyAllowed = $('#inv_qty_allowed').val();
        if ((newQtyAllowed != parseInt(newQtyAllowed, 10)) || (newQtyAllowed < 0)) {
            $('#successLabelInv').text("Quantity allowed must be a positive whole number.");
            $('#successLabelInv').css('color', 'red');
            return;
        }
        var jsonData = { niin: newNiin, ui: newUI, um: newUM, slc: newSLC, slac: newSLAC, HCC: newHCC, remarks: newRemarks, qtyAllow: newQtyAllowed, id: newId };
        $.ajax({
            type: 'POST',
            url: "api/Inventory/UpdateInvItem",
            data: JSON.stringify(jsonData),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data == 0) {
                    $('#successLabelInv').text("Update failed.");
                    $('#successLabelInv').css('color', 'red');
                }
                else {
                    $('#successLabelInv').text("Update successful!");
                    $('#successLabelInv').css('color', 'green');
                }
            }
        });
    });
</script>
