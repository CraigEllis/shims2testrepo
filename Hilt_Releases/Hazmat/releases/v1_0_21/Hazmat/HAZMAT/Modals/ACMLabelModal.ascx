﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ACMLabelModal.ascx.cs" Inherits="HAZMAT.ACMLabelModal" %>

<div class="modal" id="labelsModal">
    <div class="modal-dialog" style="width: 700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h class="modal-title"><b>ACM Labels</b></h>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div style="border: 1px solid lightgrey; padding: 10px; margin-bottom:15px;">
                            <div class="form-group">
                                <label>Item Name: </label>
                                <input id="lblItemName" class="form-control" disabled="disabled" />
                            </div>
                            <div class="form-group">
                                <label>NIIN: </label>
                                <input id="lblItemNiin" class="form-control" disabled="disabled" />
                            </div>
                        </div>
                        <div style="border: 1px solid lightgrey; padding: 10px;">
                            <div class="form-group">
                                <label>Starting label number:</label>
                                <input type="text" id="labelNumber" class="form-control" value="1" />
                                <label id="errorMsg" style="visibility: hidden; color: red;"><b>Please enter a number from 1 to 30.</b></label>
                            </div>
                            <div class="form-group">
                                <label>Number of labels to print:</label>
                                <input type="text" class="form-control" id="labelCount" value="30" />
                                <label id="labelCountError" style="visibility: hidden; color: red;"><b>Please enter the number of labels to print.</b></label>
                            </div>
                        </div>
                        <br />
                        <center>
                                <a href="api/SMCL/ACMLabels?niin=" id="getPDF">
                                    <button class="btn btn-info btn-sm btn-details" type="button">Download ACM Labels</button></a></center>
                        <br />
                        <label style="color:red">Note: formatted to print to Avery 5160 labels or equivalent</label>
                    </div>
                    <div style="width: auto; padding-bottom: 10px" class="col-md-6">
                        <table style="border: 1px solid black">
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">1</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">2</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">3</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">4</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">5</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">6</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">7</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">8</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">9</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">10</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">11</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">12</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">13</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">14</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">15</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">16</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">17</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">18</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">19</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">20</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">21</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">22</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">23</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">24</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">25</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">26</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">27</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spaced">
                                    <label style="align-content: center;">28</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">29</label></td>
                                <td class="spaced">
                                    <label style="align-content: center;">30</label>
                                </td>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
