﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SMCLUpdateModal.ascx.cs" Inherits="HAZMAT.Modals.SMCLUpdateModal" %>

<div class="modal" id="smclUpdateModal">
    <div class="modal-dialog" style="width: 700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h class="modal-title"><b>SMCL/Incompatibles Update</b></h>
            </div>
            <div class="modal-body">
                <div class="row" style="padding-bottom:10px;">
                    <div style="margin-left: 10px; border: 3px lightgrey solid; padding: 10px" class="col-md-11">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p>Select a .xls SMCL file to upload. This will replace the current SMCL with the latest version.</p>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-12">
                                        <input id="smclUpload" type="file" /><label id="lblSmclUpdateSuccess"></label>
                                    </div>
                                </div>
                                <button id="btn_uploadSmcl" class="btn-small btn-info" value="Upload" style="margin-top: 10px;">
                                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate load-icon" style="display: none"></span>
                                    <span class="clicked" style="display: none">Importing SMCL</span>
                                    <span class="not-clicked">Upload</span></button>
                                <br />
                                <br />
                                <label id="lastUploadDateLabel"></label>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="margin-left: 10px; border: 3px lightgrey solid; padding: 10px" class="col-md-11">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p>Select a .xls incompatibles file to upload. The sheet name should be Incompatibles.</p>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-12">
                                        <input id="incUpload" type="file" /><label id="lblIncUpdateSuccess"></label>
                                    </div>
                                </div>
                                <button id="btn_uploadInc" class="btn-small btn-info" value="Upload" style="margin-top: 10px;">
                                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate load-icon-i" style="display: none"></span>
                                    <span class="clicked-i" style="display: none">Importing Incompatibles</span>
                                    <span class="not-clicked-i">Upload</span></button>
                                <br />
                                <br />
                                <label id="lastUploadIncDateLabel"></label>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function getLastUpdate() {
        $.get("api/SMCL/GetLastSMCLUpdate", function (data) {
            if (data) {
                $('#lastUploadDateLabel').text("Last SMCL Update: " + data);
            }
            else {
                $('#lastUploadDateLabel').text("No SMCL has been loaded.");
            }
        });
    }

    $(document).on('click', '#btn_uploadSmcl', function (e) {
        e.preventDefault();
        $('.clicked').show();
        $('.load-icon').show();
        $('.not-clicked').hide();
        var data = new FormData();

        var files = $("#smclUpload").get(0).files;

        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("SMCL", files[0]);
        }

        // Make Ajax request with the contentType = false, and processDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: "/api/SMCL/UploadSMCL",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                $('.clicked').hide();
                $('.load-icon').hide();
                $('.not-clicked').show();
                $('#lblSmclUpdateSuccess').text(data);
                if (data == "SMCL data successfully updated.") {
                    $('#lblSmclUpdateSuccess').css('color', 'green');
                    var table = $('#smclDataTable').DataTable()
                    table.ajax.reload();
                    $('#smclWarning').hide();
                }
                else {
                    $('#lblSmclUpdateSuccess').css('color', 'red');
                }
                getLastUpdate();
            }
        });

    });

    function getLastIncUpdate() {
        $.get("api/ShipManagement/GetLastIncUpdate", function (data) {
            if (data) {
                $('#lastUploadIncDateLabel').text("Last Incompatibles Update: " + data);
            }
            else {
                $('#lastUploadIncDateLabel').text("No incompatibles information has been loaded since installation.");
            }
        });
    }

    $(document).on('click', '#btn_uploadInc', function (e) {
        e.preventDefault();
        $('.clicked-i').show();
        $('.load-icon-i').show();
        $('.not-clicked-i').hide();
        var data = new FormData();

        var files = $("#incUpload").get(0).files;

        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("Incompatibles", files[0]);
        }

        // Make Ajax request with the contentType = false, and processDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: "/api/ShipManagement/UploadIncompatibles",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                $('.clicked-i').hide();
                $('.load-icon-i').hide();
                $('.not-clicked-i').show();
                $('#lblIncUpdateSuccess').text(data);
                if (data == "Success!") {
                    $('#lblIncUpdateSuccess').css('color', 'green');
                }
                else {
                    $('#lblIncUpdateSuccess').css('color', 'red');
                }
                getLastIncUpdate();
            }
        });

    });
</script>
