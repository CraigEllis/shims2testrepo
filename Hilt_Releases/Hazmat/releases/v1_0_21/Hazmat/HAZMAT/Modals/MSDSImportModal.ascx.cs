﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Ionic.Zip;

namespace HAZMAT.Modals
{
    public partial class MSDSImportModal : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string start = "";
        }

        protected void uploadZip(object sender, EventArgs e)
        {
            string extractPath = AppDomain.CurrentDomain.BaseDirectory + "resources\\MSDS\\";
           
            using (ZipFile zip = ZipFile.Read(MSDSZip.PostedFile.InputStream))
            {
                zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
            }

            DatabaseManager dbManager = new DatabaseManager();

            string[] insertStatements =  Directory.GetFiles(extractPath, "*.txt");
            foreach (string insert in insertStatements)
            {
                string stmt = File.ReadAllText(insert);
                dbManager.addImportedMSDS(stmt);

                File.Delete(insert);
            }

        }
    }
}