﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Data;

namespace HAZMAT
{
    public class MSDSExport
    {

        public MemoryStream getZipFileExport()
        {

            //Build zip file
            MemoryStream zipStream = new MemoryStream();

                // 'using' statements guarantee the stream is closed properly which is a big source
                // of problems otherwise.  Its exception safe as well which is great.
               
                List<KeyValuePair<string, byte[]>> uploadedFileList = new DatabaseManager().getManuallyEnteredMSDSfiles();              

                using (ZipOutputStream s = new ZipOutputStream(zipStream))
                {

                    s.SetLevel(9); // 0 - store only to 9 - means best compression
                    //Add each uploaded file from each MSDS records
                    foreach (KeyValuePair<string, byte[]> kvp in uploadedFileList)
                    {                    
                        
                        byte[] file = kvp.Value;
                        string file_name = kvp.Key;
                        
                        // Using GetFileName makes the result compatible with XP
                        // as the resulting path is not absolute.
                        file_name = ZipEntry.CleanName(file_name);

                        ZipEntry entry = new ZipEntry(file_name);

                        // Setup the entry data as required.

                        // Crc and size are handled by the library for seakable streams
                        // so no need to do them here.
                        entry.Size = file.Length;
                        s.UseZip64 = UseZip64.Off;

                        // Could also use the last write time or similar for the file.
                        entry.DateTime = DateTime.Now;
                        s.PutNextEntry(entry);
                        s.Write(file, 0, file.Length);
                    }//end

                    //csv
                    string csv_file_name = "MSDS.xls";
                    csv_file_name = ZipEntry.CleanName(csv_file_name);

                    ZipEntry csv_entry = new ZipEntry(csv_file_name);

                    // Setup the entry data as required.
                    byte[] msdsFile = buildCSV().ToArray();

                    // Crc and size are handled by the library for seakable streams
                    // so no need to do them here.
                    csv_entry.Size = msdsFile.Length;
                    s.UseZip64 = UseZip64.Off;

                    // Could also use the last write time or similar for the file.
                    csv_entry.DateTime = DateTime.Now;
                    s.PutNextEntry(csv_entry);

                    s.Write(msdsFile, 0, msdsFile.Length);

                    // Finish/Close arent needed strictly as the using statement does this automatically

                    // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                    // the created file would be invalid.
                    s.Finish();

                    // Close is important to wrap things up and unlock the file.
                    s.Close();
                   

                }


           

            return zipStream;

        }

        private MemoryStream buildCSV()
        {

            MemoryStream ms = new MemoryStream(); //stream for the csv file

            StreamWriter sw = new StreamWriter(ms);

            DatabaseManager manager = new DatabaseManager();
           
            string delimiter = "\t";

            bool addHeader = true;
            string headerData = "";
            
            DataTable msds = manager.getManuallyEnteredMSDS();

            List<string> usedMsds = new List<string>();
            List<string> usedIngredient = new List<string>();
            List<string> usedContractor = new List<string>();


                
                foreach (DataRow r in msds.Rows)
                {
                    bool ignoreMsds = false;
                    bool ignoreIngredient = false;
                    bool ignoreContractor = false;

                    if (usedMsds.Contains(r["msds_id"].ToString()))
                        ignoreMsds = true;
                    if (usedIngredient.Contains(r["ingredients_id"].ToString()))
                        ignoreIngredient = true;
                    if (usedContractor.Contains(r["contractor_id"].ToString()))
                        ignoreContractor = true;

                    usedMsds.Add(r["msds_id"].ToString());
                    usedIngredient.Add(r["ingredients_id"].ToString());
                    usedContractor.Add(r["contractor_id"].ToString());

                    String rowData = "";
                    foreach (DataColumn c in msds.Columns)
                    {                        
                        if (!c.ColumnName.Equals("msds_id") && !c.ColumnName.Equals("ingredients_id") && !c.ColumnName.Equals("contractor_id"))
                        {
                            if ((c.Ordinal < 76 || c.Ordinal > 100) && !ignoreMsds)
                            {
                                rowData += r[c].ToString() + delimiter;
                                if (addHeader)
                                    headerData += c.ColumnName + delimiter;
                            }
                            else if (c.Ordinal >= 76 && c.Ordinal <= 91 && !ignoreIngredient)
                            {
                                rowData += r[c].ToString() + delimiter;
                                if (addHeader)
                                    headerData += c.ColumnName + delimiter;
                            }
                            else if (c.Ordinal >= 92 && c.Ordinal <= 100 && !ignoreContractor)
                            {
                                rowData += r[c].ToString() + delimiter;
                                if (addHeader)
                                    headerData += c.ColumnName + delimiter;
                            }
                            else
                            {
                                rowData += " " + delimiter;
                                if (addHeader)
                                    headerData += c.ColumnName + delimiter;
                            }

                        }
                    }
                    if(addHeader)
                    {
                        sw.WriteLine(headerData);
                        addHeader = false;
                    }
                    sw.WriteLine(rowData);
                }
                

            sw.Close();

            return ms;
        }

    }
}