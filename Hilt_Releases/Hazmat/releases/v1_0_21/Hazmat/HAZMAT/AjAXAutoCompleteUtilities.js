﻿// ** function getObj(name) **
// parameters: name = id of the object to get
// performs: sets the variable to the DOM object associated with the ID
// returns: object (x)
function getObj(strName)
{
  if (document.getElementById)
  {
    return document.getElementById(strName);
  }
  else if (document.all)
  {
    return document.all[strName];
  }
  else if (document.layers)
  {
    return document.layers[strName];
  } 
  else 
  {
    return null;
  }
}

// ** function fill AutoComplete **
// parameters:  result - pipe-delimited list of matching entries
// performs: Clears list, then fills matching values
// returns: none
function fillAutoComplete(strResult) 
{
    var divResults = getObj(strResultDivName);
    
    strFields = strResult.split('|');  // split the results into a string[]
    var strOptionString = "";
    
    if(strFields.length >= 1 && strFields[0] != "")
    {
        divResults.style.display = "block"
        var strOptionID;
        for (var i = 0; i <  strFields.length; i++)
        {
             strOptionString += "<a href=\"javascript:Choose('" + addslashes(strFields[i]) + "'); \"><div class=\"" + strOptionCSS + "\" onmouseover=\"className='" + strOptionHighlightedCSS + "'\" onmouseout=\"className='" + strOptionCSS + "'\">" + strFields[i] + "</div></a>\n"; // add Results to result group
        }
    } 
    else 
    {
        divResults.style.display = "none"
    }
            
    divResults.innerHTML = strOptionString; //add result group to Div
}

// ** function Choose **
// parameters:  strSelectedTerm - selected value
// performs: Fills Querybox with selected value, and submits if AutoPostBack is enabled
function Choose(strSelectedTerm)
{
     
  var txtQuery = getObj(strQueryBoxName);
  txtQuery.value = strSelectedTerm;
  
  var divResults = getObj(strResultDivName);
  divResults.style.display = "none";

  if(blnAutoPostBack)
  {
      var submitButton = getObj(strSubmitButtonName);

      if (submitButton != null) {
          submitButton.click();
      }
  }
  
}

// ** function onError(message) **
// parameters:  message - error message to be appended
// performs: pops up a Javascript alert when there's an error
// returns: none
function onError(strMessage) 
{
    alert("Error:\n" + strMessage);
}

function addslashes(str) {
    return (str + '').replace(/[\\']/g, '\\$&').replace(/["]/g, '&quot;');
}