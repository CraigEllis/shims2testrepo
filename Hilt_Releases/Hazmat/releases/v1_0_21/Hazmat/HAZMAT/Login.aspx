﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="HAZMAT.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HILT LOGIN</title>
</head>
<body>
    <form id="form1" runat="server">
     <div style=" background-color:#D6DFF5;">
         <div style="float:left;color:#003366; font-size:xx-large;font-family:Verdana;"><br /> &nbsp;HILT&nbsp;</div>
        <br /><asp:login ID="Login1" runat="server" BackColor="#D6DFF5" BorderColor="#D6DFF5" 
            BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            Font-Size="0.8em" ForeColor="#333333" Orientation="Vertical">
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
            <TextBoxStyle Font-Size="0.8em" />
            <TitleTextStyle BackColor="#6487DC" Font-Bold="True" Font-Size="0.9em" 
                ForeColor="White" />
        </asp:login>
    
        <br />
       
    </div>
    </form>
</body>
</html>

