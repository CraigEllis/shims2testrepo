﻿using System;
using System.Web.UI;

namespace HAZMAT
{
    public partial class ExceptionPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Exception"] != null)
                {
                    Session["InnerException"] = Session["Exception"];
                    Session["Exception"] = null;
                }
                else
                {
                    if (Session["InnerException"] == null) { }
                        Response.Redirect("HomeScreen.aspx");
                }

                if (Session["InnerException"] != null)
                    DivException.InnerHtml = (String)Session["InnerException"];

                try
                {
                    string userName = Page.User.Identity.Name;
                    string page = (String)Session["ExPath"];
                    string title = (String)Session["ExTitle"];
                    string message = (String)Session["ExMess"];
                    string stacktrace = (String)Session["ExTrace"];
                    string total = "[Error logged]: " + title + " " + message + " " + stacktrace;
                }
                catch {}
            }
        }
    }
}