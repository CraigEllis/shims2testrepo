﻿<%@ Page StylesheetTheme="WINXP_Blue" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MSDSGrid.aspx.cs" Inherits="HAZMAT.MSDSGrid" %>
 <%@ Register Src="~/AutoCompleteSearch.ascx"  TagName="AutoCompleteSearch"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 43px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br /><br />
    <table  style="width: 410px;"><tr><td align="left" class="style1">
    <asp:Image ID="Image1" runat="server" ImageUrl="images/hazard_icon.png"  /></td>
    <td>
    <asp:Label SkinID="LabelPageHeader"  ID="Label1" runat="server" Text="MSDS"
     ></asp:Label></td>
      <td align="right">
                <asp:ImageButton ID="btnFeedback" runat="server" ImageUrl="images/feedback.gif" 
                    ToolTip="Click to provide feedback" onclick="btnFeedback_Click" />
                    <asp:ImageButton ID="btnHelp" runat="server" ImageUrl="images/help.gif" 
                    ToolTip="Click to access manuals and training material" onclick="btnHelp_Click" />
            </td>
     </tr></table>



    <table class="filterTable" >
    <tr><td>
        Filter:</td><td><asp:CheckBox ID="chkbox_filter" runat="server" /></td><td>
            <asp:DropDownList ID="DropDownListSearch" runat="server">
            <asp:ListItem>MSDSSERNO</asp:ListItem>
            <asp:ListItem>NIIN</asp:ListItem>
            <asp:ListItem Value="Description">Description</asp:ListItem>
            <asp:ListItem>CAGE</asp:ListItem>
            <asp:ListItem>MANUFACTURER</asp:ListItem>
            <asp:ListItem>SPECS</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="TxtBoxSearch" runat="server" ></asp:TextBox>
    </td>
      <td>              
      <asp:CheckBox ID="chkbox_ManuallyEntered" runat="server" />
     </td>
     <td>Manually Entered Only</td>
   </tr>
       <tr><td>

     Usage Category:</td><td>
    <asp:CheckBox ID="chkbox_usageCategory" runat="server" /></td><td>
    <asp:DropDownList ID="DropDownListUsageCategory" runat="server">
        </asp:DropDownList>
        </td>
    </tr>
       <tr><td>

    Inventory Group:</td><td>
    <asp:CheckBox ID="chkbox_catalogGroup" runat="server" /></td><td>
    <asp:DropDownList  ID="DropDownListCatalogGroup" runat="server" Width="374px">
        </asp:DropDownList>
        <asp:Button ID="btn_filter" runat="server" Text="Apply Filter(s)" 
        onclick="btn_filter_Click" />
     </td>
    </tr>
    </table>
    
        <br />
        
    <div class="tableHeaderDiv" style="width:95%;">
    <asp:Label CssClass="floatLeft" ID="Label2" runat="server" SkinID="LabelTableHeader" Text="Manufacturer Safety Data Sheets" 
               Font-Bold="True"></asp:Label>
       
    <asp:Button CssClass="floatRight" ID="btnNewMSDS" runat="server" Visible="false" 
        Text="Create new MSDS" OnClick="btnNewMSDS_Click" />
    <asp:Button CssClass="floatRight" ID="btnSFRNewSMCL" runat="server"
        Text="Create new MSDS" OnClick="btnSFRNewSMCL_Click" />
        <br /><br />
        <asp:GridView CssClass="gridWidth" SkinID="GridView"  ID="GridView1" runat="server" 
            AllowPaging="True" AutoGenerateColumns="False" 
           AllowSorting="False" EnableViewState="false" CellPadding="5" 
            onpageindexchanging="GridView1_PageIndexChanging" 
            onsorting="GridView1_Sorting" 
            onselectedindexchanging="GridView1_SelectedIndexChanging" 
            ShowFooter="True" onrowcommand="GridView1_RowCommand"  
        
            DataKeyNames="msds_id, MSDSSERNO, CAGE, CT_NUMBER, NIIN, manually_entered, Manufacturer, part_no, fsc, hcc_id, hcc_description, description, Usage_Description, Catalog_Group_name, product_load_date, radioactive_ind, trade_name" 
            EmptyDataText=" No data to display" onrowdatabound="GridView1_RowDataBound">          
                    <Columns>
            <asp:BoundField Visible="false" DataField="msds_id" HeaderText="msds_id" SortExpression="msds_id" />
            <asp:BoundField Visible="false" DataField="hcc_id" HeaderText="hcc_id" SortExpression="hcc_id" />
            <asp:BoundField Visible="false" DataField="MSDSSERNO" HeaderText="MSDSSERNO" SortExpression="MSDSSERNO" />
            <asp:BoundField Visible="false" DataField="NIIN" HeaderText="NIIN" SortExpression="NIIN" />
            
            <asp:TemplateField HeaderText="" Visible="false">
                    <ItemTemplate>
                        <a href="MSDS.aspx?msds_id=<%# ((System.Data.DataRowView)Container.DataItem)["msds_id"].ToString() %>&edit=N">
                        <%# ((System.Data.DataRowView)Container.DataItem)["msds_id"].ToString() %></a>
                    </ItemTemplate>
                </asp:TemplateField> 
                                      
                <asp:ButtonField ButtonType="Link" CommandName="SelectRow" DataTextField="MSDSSERNO" HeaderText="MSDSSERNO" SortExpression="MSDSSERNO" />
                <asp:ButtonField ButtonType="Link" CommandName="SelectRow" DataTextField="CAGE" HeaderText="CAGE" SortExpression="CAGE" />
                <asp:ButtonField ButtonType="Link" CommandName="SelectRow" DataTextField="CT_NUMBER" HeaderText="CT_NUMBER" SortExpression="CT_NUMBER" />
                <asp:ButtonField ButtonType="Link" CommandName="SelectRow" DataTextField="NIIN" HeaderText="NIIN" SortExpression="NIIN" />
                <asp:ButtonField ButtonType="Link" CommandName="SelectRow" DataTextField="manually_Entered" HeaderText="manually_entered" SortExpression="manually_entered" />                             
                <asp:TemplateField HeaderText="Manufacturer">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView)Container.DataItem)["Manufacturer"]%>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Part #">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView)Container.DataItem)["part_no"]%>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView)Container.DataItem)["Description"]%>
                    </ItemTemplate>
                </asp:TemplateField>  
                
                <asp:TemplateField HeaderText="HCC">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView)Container.DataItem)["HCC_Description"]%>
                    </ItemTemplate>
                </asp:TemplateField>                              

                <asp:TemplateField HeaderText="Usage">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView) Container.DataItem)["Usage_Description"]%>
                    </ItemTemplate>
                </asp:TemplateField>                              

                <asp:TemplateField HeaderText="Catalog Group">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView)Container.DataItem)["Catalog_Group_Name"]%>
                    </ItemTemplate>
                </asp:TemplateField> 

                <asp:TemplateField HeaderText="Last Updated">
                    <ItemTemplate>
                        <%# ((System.Data.DataRowView)Container.DataItem)["last_update"]%>
                    </ItemTemplate>
                </asp:TemplateField>                              
                               
                <asp:ButtonField ButtonType="Link" CommandName="Edit1" text="Edit" HeaderText="Edit" ItemStyle-ForeColor="#0066ff" />
                <asp:ButtonField  ButtonType="Link" CommandName="ViewMSDS" Text="View" HeaderText="MSDS" ItemStyle-ForeColor="#0066FF" />
             </Columns>
            
            <SelectedRowStyle BackColor="#003399" ForeColor="White" />
        </asp:GridView>    
    </div>
  
  <br />
   
   
   <br />


    <br />
    <!-- invisible control to forward data to next page -->
    <asp:TextBox ID="INVIDparam" Text="-1" visible="false" runat="server"></asp:TextBox>
</asp:Content>
