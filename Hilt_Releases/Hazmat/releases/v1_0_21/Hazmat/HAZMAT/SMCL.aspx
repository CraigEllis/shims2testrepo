﻿<%@ Page StylesheetTheme="WINXP_Blue" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SMCL.aspx.cs" Inherits="HAZMAT.SMCL" ClientIDMode="Static" %>

<%@ Register TagPrefix="uc" TagName="SMCLDetails"
    Src="~/Modals/SMCLDetailsModal.ascx" %>

<%@ Register TagPrefix="uc" TagName="ACMLabelModal"
    Src="~/Modals/ACMLabelModal.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 43px;
        }

        .limHeight {
            display: block;
            max-height: 100px;
            overflow-y: scroll;
        }

        .msdsGrid td {
            padding: 3px;
            vertical-align: top;
        }

        .alert-danger {
            clear: both;
            width: 100%;
        }

        .bs-callout-danger {
            color: #d9534f;
        }

        .bs-callout-warning {
            color: #f0ad4e;
        }

        .spaced {
            padding: 10px 40px 10px 40px;
        }

        .left-button {
            float: left;
            width: 37px;
            padding:310px 0px 310px 15px
        }
        .right-button {
            float: right;
            width: 37px;
            padding:310px 15px 310px 0px
        }

        .item-count {
            margin: 0 auto;
            position: relative;
            text-align: center;
            font-size: 24px;
        }
    </style>
    <script type="text/javascript" src="/resources/js/site.js"></script>
    <script type="text/javascript" src="/resources/js/SMCL.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="page-header">
        <h1>SMCL
        </h1>
    </div>

    <div class="tableHeaderDiv">
        <div class="table-grid-headwrap clearfix" id="filterDiv">
            <table class="filterTable pull-left">
                <tr>
                    <td>
                        <label>
                            <b>NIIN</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterNiinList" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>Item Name</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterItemName" style="width: 300px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>SPECS</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterSpecs" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 15px;" valign="bottom">
                        <label>
                            Search SMCL Database:
                        </label>
                        <input id="searchSMCL" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <b>Table Name</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterTableName" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>Manufacturer</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterManufacturer" style="width: 300px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 10px">
                        <label>
                            <b>ACM Category</b>
                        </label>
                        <div class="form-inline">
                            <div class="form-group">
                                <select id="filterACM" style="width: 200px" class="filter"></select>
                            </div>
                        </div>
                    </td>
                    <td style="padding-left: 15px;" id="tableCountCell" valign="bottom"></td>
                </tr>
            </table>
        </div>

        <hr />
        <table id="smclDataTable" class="table" width="100%">
            <thead>
                <th>MSDS</th>
                <th>ACM LABELS</th>
                <th>FSC</th>
                <th>NIIN</th>
                <th>ITEM NAME</th>
                <th>ACM CATEGORY</th>
                <th>U/I</th>
                <th>SPMIG</th>
                <th>SPECS</th>
                <th>MANUFACTURER</th>
                <th>CAGE</th>
                <th>PART/TRADE NAME</th>
            </thead>
        </table>
        <hr />
        <br />
    </div>

    <div class="modal" id="msdsModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h class="modal-title"><b>MSDS modal</b></h>
                    <div class="item-count">
                                    <span style="width: 30px; text-align: right" id="currentMSDS"></span> 
                                    <span>of</span> 
                                    <span id="totalMSDS" class="items"></span>
                                </div> 
                </div>
                <div class="modal-body">
                    <!--Needs to display all MSDS value-added data + link to PDF MSDS-->
                    <div class="row">
                        <div class="col-md-1 left-button" style="margin-right:10px">
                            <button class="btn btn-info btn-lg" type="button" id="msdsLeft" style="padding: 5px 0px; height:100px; margin:0px auto" >
                                <span class="glyphicon glyphicon-chevron-left" style="font-size:20px"></span>
                            </button>
                        </div>
                        <div class="col-md-11">
                                <object id="msdsPdf" style="width:100%; height:700px; margin:0 auto;" data=""></object>
                                <iframe id="msdsDoc" style="width:100%; height:700px; margin:0 auto; display:none"></iframe>
                                
                                <div id="noMsds" style="display:none; text-align:center; padding-top:345px">
                                    <span style="color:grey; font-size:250%">NO MSDS FOUND FOR THIS ITEM</span>
                                </div>
                        </div>

                        <div class="col-md-1 right-button">
                            <button class="btn btn-info btn-lg" type="button" id="msdsRight" style="padding: 5px 0px; height: 100px; margin:0px auto">
                                <span class="glyphicon glyphicon-chevron-right" style="font-size:20px"></span>
                            </button>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <uc:SMCLDetails ID="SMCLDetailsModal" runat="server"></uc:SMCLDetails>
    <uc:ACMLabelModal ID="labelsModal" runat="server"></uc:ACMLabelModal>

</asp:Content>
