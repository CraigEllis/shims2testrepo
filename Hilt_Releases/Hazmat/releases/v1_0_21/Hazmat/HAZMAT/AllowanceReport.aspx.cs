﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HAZMAT
{
   /* public partial class AllowanceReport : Page
    {
        [CoverageExclude]
        protected void Page_Load(object sender, EventArgs e)
        {
            loadReportGrid();

            //populate usage category drop down list
            if (DropDownListUsageCategory.Items.Count == 0)
            {
                List<ListItem> usageCategories = new DatabaseManager().getUsageCategoryList(true);
                foreach (ListItem item in usageCategories)
                {
                    DropDownListUsageCategory.Items.Add(item);
                }
            }

            //populate inventory/catalog group drop down list
            if (DropDownListCatalogGroup.Items.Count == 0)
            {
                List<ListItem> usageCategories = new DatabaseManager().getCatalogGroupList();
                foreach (ListItem item in usageCategories)
                {
                    DropDownListCatalogGroup.Items.Add(item);
                }
            }


            if (!Page.IsPostBack)
                new Reports().populateReportsDropdown(DropDownListReport, "Quanties over allowance");
        }
        protected void btnChooseReport_Click(object sender, EventArgs e)
        {

            String selectedValue = DropDownListReport.SelectedValue;

            if (!selectedValue.Equals("Feedback"))
            {
                string url = new Reports().getReportUrl(selectedValue);
                Response.Redirect(url);
            }
            else if (selectedValue.Equals("Feedback"))
            {


                //get feedback info
                List<FeedbackInfo> infoList = new DatabaseManager().getFeedbackInfo();


                //call class or method to create tab delimited report
                byte[] file = new DatabaseManager().createFeedbackReport(infoList);

                //allow user to download report
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "FeedbackReport.xls");
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(file);
                Response.Flush();
                Response.End();

            }
            

        }

        [CoverageExclude]
        private void loadReportGrid()
        {
            String filterColumn = "";
            String filterText = "";
            if (chkbox_filter.Checked)
            {
                filterColumn = DropDownListSearch.SelectedValue;
                filterText = TxtBoxSearch.Text;
            }
            String usageCategory = "";
            if (chkbox_usageCategory.Checked)
            {
                usageCategory = DropDownListUsageCategory.SelectedValue;
            }
            String catalogGroup = "";
            if (chkbox_catalogGroup.Checked)
            {
                catalogGroup = DropDownListCatalogGroup.SelectedValue;
            }
            AllowanceGridView.DataSource = new DatabaseManager().getFilteredAllowanceQtyCollection(filterColumn, filterText, usageCategory, catalogGroup).DefaultView;
            AllowanceGridView.DataBind();
        }

       

        [CoverageExclude]
        protected void AllowanceGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AllowanceGridView.PageIndex = e.NewPageIndex;
            AllowanceGridView.SelectedIndex = -1;
            AllowanceGridView.DataBind();
        }

        [CoverageExclude]
        protected void btnExcelReport_Click(object sender, EventArgs e)
        {




            ExcelReports report = new ExcelReports();
            string reportPath = Request.PhysicalApplicationPath + "resources/Reports.xls";
            string filename = report.exportReports(reportPath);


            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=ExcelReports.xls");
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(filename);
            Response.Flush();
            Response.End();

        }


        protected void btnFeedback_Click(object sender, ImageClickEventArgs e)
        {

            //save current page to the session for feedback
            Session.Add("feedbackPage", GetCurrentPageName());

            ClientScript.RegisterStartupScript(Page.GetType(), "",
"window.open('FEEDBACK.aspx','Feedback','height=400,width=590, scrollbars=no');", true);


        }

        public string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        protected void btnHelp_Click(object sender, ImageClickEventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "",
           "window.open('Help.html','Help','height=750,width=790, scrollbars=yes');", true);
        }
    }*/
}