﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HAZMAT._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 782px">
    
        TEST PAGE - see Catalog.aspx page for better example
   
        <br />
        <br />
        Catalog<br />
        <br />
        Submarine Material Control List<br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" DataSourceID="HAZMAT_Desktop">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="FSC" HeaderText="FSC" SortExpression="FSC" />
                <asp:BoundField DataField="NIIN" HeaderText="NIIN" SortExpression="NIIN" />
                <asp:BoundField DataField="ITEM_NAME" HeaderText="ITEM_NAME" 
                    SortExpression="ITEM_NAME" />
                <asp:BoundField DataField="UI" HeaderText="UI" SortExpression="UI" />
                <asp:BoundField DataField="UM" HeaderText="UM" SortExpression="UM" />
                <asp:BoundField DataField="ACM_CATEGORY" HeaderText="ACM_CATEGORY" 
                    SortExpression="ACM_CATEGORY" />
                <asp:BoundField DataField="SHELF_LIFE_ACTION_CODE" 
                    HeaderText="SHELF_LIFE_ACTION_CODE" SortExpression="SHELF_LIFE_ACTION_CODE" />
                <asp:BoundField DataField="SHELF_LIFE_CODE" HeaderText="SHELF_LIFE_CODE" 
                    SortExpression="SHELF_LIFE_CODE" />
                <asp:BoundField DataField="SPECS" HeaderText="SPECS" SortExpression="SPECS" />
                <asp:BoundField DataField="SMCC" HeaderText="SMCC" SortExpression="SMCC" />
                <asp:BoundField DataField="STORAGE_TYPE" HeaderText="STORAGE_TYPE" 
                    SortExpression="STORAGE_TYPE" />
                <asp:BoundField DataField="SPMIG" HeaderText="SPMIG" SortExpression="SPMIG" />
                <asp:BoundField DataField="HCC" HeaderText="HCC" SortExpression="HCC" />
                <asp:BoundField DataField="REMARKS" HeaderText="REMARKS" 
                    SortExpression="REMARKS" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="HAZMAT_Desktop" runat="server" 
            ConnectionString="<%$ ConnectionStrings:HAZMAT %>" 
            SelectCommand="SELECT [FSC], [NIIN], [ITEM_NAME], [UI], [UM], [ACM_CATEGORY], [SHELF_LIFE_ACTION_CODE], [SHELF_LIFE_CODE], [SPECS], [SMCC], [STORAGE_TYPE], [SPMIG], [HCC], [REMARKS] FROM [catalog]">
        </asp:SqlDataSource>
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" DataSourceID="HAZMAT_Desktop0">
            <Columns>
                <asp:BoundField DataField="CAGE" HeaderText="CAGE" SortExpression="CAGE" />
                <asp:BoundField DataField="MANUFACTURER" HeaderText="MANUFACTURER" 
                    SortExpression="MANUFACTURER" />
                <asp:BoundField DataField="shelf_life_expiration_date" 
                    HeaderText="shelf_life_expiration_date" 
                    SortExpression="shelf_life_expiration_date" />
                <asp:BoundField DataField="on_hand_quantity" HeaderText="on_hand_quantity" 
                    SortExpression="on_hand_quantity" />
                <asp:BoundField DataField="action_complete" HeaderText="action_complete" 
                    SortExpression="action_complete" />
                <asp:BoundField DataField="WID" HeaderText="WID" SortExpression="WID" />
                <asp:BoundField DataField="description" HeaderText="description" 
                    SortExpression="description" />
                <asp:BoundField DataField="location_name" HeaderText="location_name" 
                    SortExpression="location_name" />
            </Columns>
        </asp:GridView>
        <br />
        <br />
        <asp:SqlDataSource ID="HAZMAT_Desktop0" runat="server" 
            ConnectionString="<%$ ConnectionStrings:HAZMAT %>" 
            SelectCommand="SELECT [CAGE], [MANUFACTURER], [shelf_life_expiration_date], [on_hand_quantity], [action_complete], [WID], [description], [location_name] FROM [v_inventory_in_locations_top] WHERE ([catalog_id] = 1)">
            
        </asp:SqlDataSource>
        <br />
        <br />
        <asp:MultiView ID="MultiView1" runat="server">
        </asp:MultiView>
        <br />
    
    </div>
    </form>
</body>
</html>
