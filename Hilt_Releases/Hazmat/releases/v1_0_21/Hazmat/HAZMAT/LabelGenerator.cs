﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;

namespace HAZMAT
{
    public class LabelGenerator
    {
        private DatabaseManager dbManager;

        public LabelGenerator()
        {
            dbManager = new DatabaseManager();
        }

        public string PrintACMLabels(string niin, string usageCategory, string description, string labelNumber, string labelCount, string location)
        {
            int topMargin = 36;
            int sideMargin = 14;
            float labelHeight = 72f;
            int pageCols = 3;
            int startingNumber = Int32.Parse(labelNumber);
            int countOfLabels = Int32.Parse(labelCount);

            String completedFilePath = Path.GetTempFileName();

            var doc = new Document(PageSize.LETTER);
            doc.SetMargins(sideMargin, sideMargin, topMargin, topMargin);

            var pdfWriter = PdfWriter.GetInstance(doc, new FileStream(completedFilePath, FileMode.Create));
            doc.Open();

            // Create the Label table

            PdfPTable table = new PdfPTable(pageCols);
            table.WidthPercentage = 100f;
            table.DefaultCell.Border = 0;

            for (int i = 0; i < startingNumber-1; i++)
            {
                //add blank cells for all the cells prior to the one selected as starting point
                PdfPCell cell = new PdfPCell();
                cell.Border = 0;
                cell.FixedHeight = labelHeight;
                table.AddCell(cell);
            }

            for (int i = 0; i < countOfLabels; i++)
            {
                #region Label Construction

                PdfPCell cell = new PdfPCell();
                cell.Border = 0;
                cell.FixedHeight = labelHeight;
                cell.VerticalAlignment = Element.ALIGN_LEFT;
                PdfPTable innerTable = new PdfPTable(4);
                innerTable.HorizontalAlignment = Element.ALIGN_LEFT;

                string usageWarning = "";
                switch (usageCategory) {
                    case "L":
                        usageWarning = "LIMITED - Only use while underway for a specific purpose for which no non-toxic substitute exists.";
                        break;
                    case "X":
                        usageWarning = "PROHIBITED - This item is not allowed aboard submarine at any time.";
                        break;
                    case "R":
                        usageWarning = "RESTRICTED - Only use in port while ventilating. Not allowed onboard underway.";
                        break;
                }

                PdfPCell cell2 = new PdfPCell(new Phrase(usageWarning, new Font(Font.FontFamily.HELVETICA, 5f, Font.BOLD, BaseColor.BLACK)));
                cell2.HorizontalAlignment = Element.ALIGN_LEFT;
                cell2.Border = Rectangle.NO_BORDER;
                cell2.Colspan = 6;
                innerTable.AddCell(cell2);

                cell.AddElement(innerTable);
                cell.AddElement(new Paragraph("Item: " + description, new Font(Font.FontFamily.HELVETICA, 7f, Font.NORMAL, BaseColor.BLACK)));
                cell.AddElement(new Paragraph("NIIN: " + niin, new Font(Font.FontFamily.HELVETICA, 7f, Font.NORMAL, BaseColor.BLACK)));
                if (location != "")
                {
                    cell.AddElement(new Paragraph("Location: " + location, new Font(Font.FontFamily.HELVETICA, 7f, Font.NORMAL, BaseColor.BLACK)));
                }
                else
                {
                    cell.AddElement(new Paragraph("Location: _______________________________________", new Font(Font.FontFamily.HELVETICA, 7f, Font.NORMAL, BaseColor.BLACK)));
                }
                cell.AddElement(new Paragraph("SUPPO Signature: ________________________________", new Font(Font.FontFamily.HELVETICA, 7f, Font.NORMAL, BaseColor.BLACK)));

                table.AddCell(cell);

                #endregion
            }

            table.CompleteRow();
            doc.Add(table);

            doc.Close();

            return completedFilePath;
        }
    }
}