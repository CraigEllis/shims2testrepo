﻿<%@ Page StylesheetTheme="WINXP_Blue" Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MSDS.aspx.cs" Inherits="HAZMAT.MSDS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    $(document).ready(
        function ()
        {
            // Show both C and F for temperature labels
            $('[id$="FLASH_PT_TEMPLabel"], [id$="AUTOIGNITION_TEMPLabel"], [id$="TRAN_FLASH_PT_TEMPLabel"]').each(
                function ()
                {
                    var cVal = $(this).text();
                    if (cVal != null && cVal != '') {
                        $(this).text(cVal + 'C, ' + Math.round(cVal * (9/5) + 32) + 'F');    
                    }
                }
            );
            
            // Get the original (unedited) values for this MSDS
            var oVals = '';
            eval($('#<%= msdsOriginalValues.ClientID %>').val());
            if (oVals == null || $("input[name$='MSDSSERNOTextBox']").val() == '')
                return;

            for (var index in oVals)
            {
                var elTb = $('input[name$=' + index + 'TextBox]'); // textbox names in this form always end in name of the field + 'TextBox'
                var elDd = $('select[name$=dd_' + index.toLowerCase() + ']');

                // Compare textbox values with original values, show when different
                if (elTb.length > 0)
                {
                    if (oVals[index] != elTb.val())
                    {
                        try
                        {
                            elTb.after('<div class="divOriginalValue">WAS: '
                            + (oVals[index] == '' ? '{blank}' : oVals[index])
                            + '</div>');
                        }
                        catch (err)
                        { }
                    }
                }

                // Compare dropdown list values with original values, show when different
                if (elDd.length > 0)
                {
                    var elDdSelected = $('select[name$=dd_' + index.toLowerCase() + '] :selected');

                    if (index == 'HCC')
                    {
                        var curValSplitArr = $.trim(elDdSelected.text()).split(' ');
                        var curVal = curValSplitArr[0];
                        if (oVals[index] != curVal)
                            elDd.after('<div class="divOriginalValue">WAS: '
                            + (oVals[index] == '' ? '{blank}' : oVals[index])
                            + '</div>');
                    }
                    else
                    {
                        if ($.trim(oVals[index]) != $.trim(elDdSelected.text()))
                            elDd.after('<div class="divOriginalValue">WAS: '
                            + (oVals[index] == '' ? '{blank}' : oVals[index])
                            + '</div>');
                    }
                }
            }
        }
    );
</script>

<style type="text/css">
        .divOriginalValue
        {
            display: inline;
            margin-left: 10px;
            background-color: Yellow;
            padding: 3px;
        }
        .style1
        {
            width: 43px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="msdsOriginalValues" runat="server" />
        <br /><br />
    <table style="width: 565px;"><tr><td align="left" class="style1">
    <asp:Image ID="Image1" runat="server" ImageUrl="images/hazard_icon.png"  /></td><td>
    <asp:Label  ID="Label1" runat="server" Text="MSDS"
     SkinID="LabelPageHeader"></asp:Label></td>
      <td align="right">
                <asp:ImageButton ID="btnFeedback" runat="server" ImageUrl="images/feedback.gif" 
                    ToolTip="Click to provide feedback" onclick="btnFeedback_Click" 
                    CausesValidation="False" />
                    <asp:ImageButton ID="btnHelp" runat="server" ImageUrl="images/help.gif" 
                    ToolTip="Click to access manuals and training material" onclick="btnHelp_Click" CausesValidation="false" />
            </td>
     </tr></table>
        
        
        
     <div style="position:absolute; top:110px; left:330px; background-color:#eeeeee;z-index: 9002;">
    <asp:Panel Visible="false" ID="CalendarPopup" runat="server" BackColor="#CCCCCC" BorderStyle="Solid" 
        Height="270px" Width="160px">
       
       <center>
         <br />
        <asp:DropDownList ID="drpMonthCal" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCalMonth_SelectedIndexChanged" Width="105px">
        </asp:DropDownList>
        <asp:DropDownList ID="drpYearCal" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCalYear_SelectedIndexChanged" Width="105px">
        </asp:DropDownList>
        <br /><br />
        <asp:Calendar ID="cal_productDate" runat="server" Height="16px" OnVisibleMonthChanged="cal_expDate_MonthChanged" onselectionchanged="cal_productDate_SelectionChanged" SkinID="CalendarView" Width="105px">
        </asp:Calendar>
        <br />
         <asp:Button ID="ButtonCal" runat="server" Text="Cancel" onclick="btn_cancelCalendar_Click" />

        </center>
    </asp:Panel>
    </div>

     <asp:Panel Visible="false" ID="fadePanel" runat="server" cssClass="blanket" /><br />

    <b>MSDS Value-Added Data - Header Information  | </b>
    <asp:LinkButton CausesValidation="false" Visible="true"  ID="btn_excelExport" runat="server" 
            onclick="btn_excelExport_Click">Export All Manually Entered MSDS</asp:LinkButton>
    <asp:FormView ID="FormViewMSDS" runat="server" 
            EnableModelValidation="True" SkinID="FormView" DefaultMode="Insert" 
            EmptyDataText="No data to display" Width="100%">

    <InsertItemTemplate>
            <table>
            <tr><td> FSC: </td><td>
                <asp:TextBox ID="FSCTextBox" MaxLength="18" runat="server" Text='<%# Server.HtmlEncode(StrEval("FSC")) %>' />
           </td>           </tr>
           <tr><td>NIIN:</td><td>
                <asp:TextBox ID="NIINTextBox" MaxLength="9"  runat="server" Text='<%# Server.HtmlEncode(StrEval("NIIN")) %>' />
                <asp:RequiredFieldValidator  ControlToValidate="NIINTextBox" ID="RequiredFieldValidator1" 
                runat="server" ErrorMessage="*NIIN is required. " ForeColor="White"></asp:RequiredFieldValidator>
               </td></tr><tr><td> MSDSSERNO/Product Serial Number:</td><td>
                <asp:TextBox ID="MSDSSERNOTextBox"  MaxLength="10" runat="server" 
                    Text='<%# Server.HtmlEncode(StrEval("MSDSSERNO")) %>' />
               </td></tr><tr><td> CAGE (Responsible Party):</td><td>
                <asp:TextBox ID="CAGETextBox" runat="server" MaxLength="5"  Text='<%# Server.HtmlEncode(StrEval("CAGE")) %>' />
                <asp:RequiredFieldValidator  ControlToValidate="CAGETextBox" ID="RequiredFieldValidator0" 
                runat="server" ErrorMessage="*CAGE is required. " ForeColor="White"></asp:RequiredFieldValidator>
                </td></tr>
                </table><br />
                MANUFACTURER/Company Name (Responsible Party):<br />
                <asp:TextBox ID="MANUFACTURERTextBox" runat="server" 
                    Text='<%# Server.HtmlEncode(StrEval("MANUFACTURER")) %>'  MaxLength="255" Width="450" />
                    <asp:RequiredFieldValidator  ControlToValidate="MANUFACTURERTextBox" ID="RequiredFieldValidator2" 
                runat="server" ErrorMessage="*MANUFACTURER is required. " ForeColor="White"></asp:RequiredFieldValidator>
                <br />
                PART #:<br />
                <asp:TextBox ID="PARTNOTextBox" runat="server"  MaxLength="100" Text='<%# Server.HtmlEncode(StrEval("PARTNO")) %>' Width="450" />
                <br />
                HCC:<br /><asp:DropDownList ID="dd_hcc" runat="server" Width="456">
                </asp:DropDownList><asp:Image Visible="true" ID="dd_image1" runat="server" ImageUrl="images/dropdown2.JPG" />                
                <br /><br />
                <table>
            <tr><td>      Article Ind:</td><td>
            <asp:TextBox ID="ARTICLE_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("ARTICLE_IND")) %>' />
            </td></tr><tr><td>
            Description:</td><td>
            <asp:TextBox ID="DESCRIPTIONTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DESCRIPTION")) %>' />
           </td></tr><tr><td>
            Emergency Response Phone # (Responsible Party):</td><td>
            <asp:TextBox ID="EMERGENCY_TELTextBox" runat="server"   MaxLength="50" 
                Text='<%# Server.HtmlEncode(StrEval("EMERGENCY_TEL")) %>' />
           </td></tr><tr><td>
            End Item Component Ind:</td><td>
            <asp:TextBox ID="END_COMP_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("END_COMP_IND")) %>' />
           </td></tr><tr><td>
            End Item Ind:</td><td>
            <asp:TextBox ID="END_ITEM_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("END_ITEM_IND")) %>' />
           </td></tr><tr><td>
            Kit Ind:</td><td>
            <asp:TextBox ID="KIT_INDTextBox" runat="server"   MaxLength="1" Text='<%# Server.HtmlEncode(StrEval("KIT_IND")) %>' />
            </td></tr><tr><td>
            Kit Part Ind:</td><td>
            <asp:TextBox ID="KIT_PART_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("KIT_PART_IND")) %>' />
           </td></tr><tr><td>
            Manufacturer MSDS #:</td><td>
            <asp:TextBox ID="MANUFACTURER_MSDS_NOTextBox" runat="server"   MaxLength="50" 
                Text='<%# Server.HtmlEncode(StrEval("MANUFACTURER_MSDS_NO")) %>' />
           </td></tr><tr><td>
            Mixture Ind:</td><td>
            <asp:TextBox ID="MIXTURE_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("MIXTURE_IND")) %>' />
            </td></tr><tr><td>
            Product Identity:</td><td>
            <asp:TextBox ID="PRODUCT_IDENTITYTextBox" runat="server"   MaxLength="255" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_IDENTITY")) %>' />
                 <asp:RequiredFieldValidator  ControlToValidate="PRODUCT_IDENTITYTextBox" ID="RequiredFieldValidator3" 
                runat="server" ErrorMessage="*PRODUCT IDENTITY is required. " ForeColor="White"></asp:RequiredFieldValidator>
           </td></tr><tr><td>
             Product Ind:</td><td>
            <asp:TextBox ID="PRODUCT_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_IND")) %>' />
           </td></tr><tr><td>
            Product Language:</td><td>
            <asp:TextBox ID="PRODUCT_LANGUAGETextBox" runat="server"   MaxLength="10" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_LANGUAGE")) %>' />
            </td></tr><tr><td>
            Product Load Date:</td><td>
            <asp:TextBox ID="PRODUCT_LOAD_DATETextBox" runat="server"   MaxLength="10" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_LOAD_DATE")) %>' ReadOnly="True" />
                   <asp:Button ID="btn_PRODUCT_LOAD_DATE_select" runat="server" Text="Select" 
            onclick="btn_PRODUCT_LOAD_DATE_select_Click" />
           </td></tr><tr><td> 
            Product Record Status:</td><td>
            <asp:TextBox ID="PRODUCT_RECORD_STATUSTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_RECORD_STATUS")) %>' />
            </td></tr><tr><td>
             Product Revision #:</td><td>
            <asp:TextBox ID="PRODUCT_REVISION_NOTextBox" runat="server"   MaxLength="18" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_REVISION_NO")) %>' />
            </td></tr><tr><td>
            Proprietary Indicator:</td><td>
            <asp:TextBox ID="PROPRIETARY_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("PROPRIETARY_IND")) %>' />
           </td></tr><tr><td>
            Published Ind:</td><td>
            <asp:TextBox ID="PUBLISHED_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("PUBLISHED_IND")) %>' />
            </td></tr><tr><td>
            Purchased Product Ind:</td><td>
            <asp:TextBox ID="PURCHASED_PROD_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("PURCHASED_PROD_IND")) %>' />
            </td></tr><tr><td>
            Pure Ind:</td><td>
            <asp:TextBox ID="PURE_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("PURE_IND")) %>' />
           </td></tr><tr><td>
            Radioactive Ind:</td><td>
            <asp:TextBox ID="RADIOACTIVE_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("RADIOACTIVE_IND")) %>' />
            </td></tr><tr><td>
            Service/Agency:</td><td>
            <asp:TextBox ID="SERVICE_AGENCYTextBox" runat="server"   MaxLength="10" 
                Text='<%# Server.HtmlEncode(StrEval("SERVICE_AGENCY")) %>' />
            </td></tr><tr><td>
            Trade Name:</td><td>
            <asp:TextBox ID="TRADE_NAMETextBox" runat="server"   MaxLength="255" 
                Text='<%# Server.HtmlEncode(StrEval("TRADE_NAME")) %>' />
           </td></tr><tr><td>
            Trade Secret Ind:</td><td>
            <asp:TextBox ID="TRADE_SECRET_INDTextBox" runat="server"   MaxLength="1" 
                Text='<%# Server.HtmlEncode(StrEval("TRADE_SECRET_IND")) %>' />
           </td></tr>
                </table>
                <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                    CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />     
    </InsertItemTemplate>

    <ItemTemplate>
                <b>FSC:</b>
                <asp:Label ID="FSCLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("FSC")) %>' />
                <asp:Label CssClass="floatRight" ID="LastUpdateLabel" runat="server" Font-Bold="True" 
                    Text='<%# Server.HtmlEncode("Last Updated: " + StrEval("last_update")) %>' />
                <br /><br />
                <b>NIIN:</b>
                <asp:Label ID="NIINLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("NIIN")) %>' />
                <br /><br />
               <b> MSDSSERNO/Product Serial Number:</b>
                <asp:Label ID="MSDSSERNOLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("MSDSSERNO")) %>' />
                <br /><br />
                <b>CAGE (Responsible Party):</b>
                <asp:Label ID="CAGELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CAGE")) %>' />
                <br /><br />
                <b>MANUFACTURER/Company Name (Responsible Party):</b>
                <asp:Label ID="MANUFACTURERLabel" runat="server" 
                    Text='<%# Server.HtmlEncode(StrEval("MANUFACTURER")) %>' />
                <br /><br />
                <b>PART #:</b>
                <asp:Label ID="PARTNOLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("PARTNO")) %>' />
                <br />   <br />
                <b>Article Ind:</b>
            <asp:Label ID="ARTICLE_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("ARTICLE_IND")) %>' />
            <br /><br />
            <b>Description:</b>
            <asp:Label ID="DESCRIPTIONLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DESCRIPTION")) %>' />
            <br /><br />
            <b>HCC:</b>
            <asp:Label ID="HCCLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("HCC_Description")) %>' />
            <br /><br />
            <b>Emergency Response Phone # (Responsible Party):</b>
            <asp:Label ID="EMERGENCY_TELLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EMERGENCY_TEL")) %>' />
            <br /><br />
           <b> End Item Component Ind:</b>
            <asp:Label ID="END_COMP_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("END_COMP_IND")) %>' />
            <br /><br />
           <b> End Item Ind:</b>
            <asp:Label ID="END_ITEM_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("END_ITEM_IND")) %>' />
            <br /><br />
           <b> Kit Ind:</b>
            <asp:Label ID="KIT_INDLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("KIT_IND")) %>' />
            <br /><br />
           <b> Kit Part Ind:</b>
            <asp:Label ID="KIT_PART_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("KIT_PART_IND")) %>' />
            <br /><br />
          <b>  Manufacturer MSDS #:</b>
            <asp:Label ID="MANUFACTURER_MSDS_NOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("MANUFACTURER_MSDS_NO")) %>' />
            <br /><br />
           <b> Mixture Ind:</b>
            <asp:Label ID="MIXTURE_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("MIXTURE_IND")) %>' />
            <br /><br />
          <b>  Product Identity:</b>
            <asp:Label ID="PRODUCT_IDENTITYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_IDENTITY")) %>' />
            <br /><br />
          <b>  Product Ind:</b>
            <asp:Label ID="PRODUCT_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_IND")) %>' />
            <br /><br />
          <b>  Product Language:</b>
            <asp:Label ID="PRODUCT_LANGUAGELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_LANGUAGE")) %>' />
            <br /><br />
           <b> Product Load Date:</b>
            <asp:Label ID="PRODUCT_LOAD_DATELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_LOAD_DATE")) %>' />
            <br /><br />
          <b>  Product Record Status:</b>
            <asp:Label ID="PRODUCT_RECORD_STATUSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_RECORD_STATUS")) %>' />
            <br /><br />
          <b>  Product Revision #:</b>
            <asp:Label ID="PRODUCT_REVISION_NOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRODUCT_REVISION_NO")) %>' />
            <br /><br />
          <b>  Proprietary Indicator:</b>
            <asp:Label ID="PROPRIETARY_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PROPRIETARY_IND")) %>' />
            <br /><br />
          <b>  Published Ind:</b>
            <asp:Label ID="PUBLISHED_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PUBLISHED_IND")) %>' />
            <br /><br />
          <b>  Purchased Product Ind:</b>
            <asp:Label ID="PURCHASED_PROD_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PURCHASED_PROD_IND")) %>' />
            <br /><br />
          <b>  Pure Ind:</b>
            <asp:Label ID="PURE_INDLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("PURE_IND")) %>' />
            <br /><br />
          <b>  Radioactive Ind:</b>
            <asp:Label ID="RADIOACTIVE_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("RADIOACTIVE_IND")) %>' />
            <br /><br />
          <b>  Service/Agency:</b>
            <asp:Label ID="SERVICE_AGENCYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SERVICE_AGENCY")) %>' />
            <br /><br />
         <b>   Trade Name:</b>
            <asp:Label ID="TRADE_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("TRADE_NAME")) %>' />
            <br /><br />
        <b>    Trade Secret Ind:</b>
            <asp:Label ID="TRADE_SECRET_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("TRADE_SECRET_IND")) %>' />
            <br />    <br />      
    </ItemTemplate>    
</asp:FormView>

    <b>Safety & Health Information:</b>
    <asp:FormView ID="FormViewPhysChemical" runat="server"  SkinID="FormViewAlt"
             EnableModelValidation="True" EmptyDataText="No data to display" DefaultMode="Edit"
            Width="100%">
        <EditItemTemplate>
            Appearance Odor Text:<br />
            <asp:TextBox ID="APP_ODORTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("APP_ODOR")) %>' Width="450"   MaxLength="255" />
            <br />
            <table>
            <tr><td>Vapor Pressure:</td><td>
            <asp:TextBox ID="VAPOR_PRESSTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VAPOR_PRESS")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>Vapor Density:</td><td>
            <asp:TextBox ID="VAPOR_DENSTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VAPOR_DENS")) %>'   MaxLength="10" />
            
            </td></tr><tr><td>Specific Gravity:</td><td>
            <asp:TextBox ID="SPECIFIC_GRAVTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SPECIFIC_GRAV")) %>'   MaxLength="30" />
            
                        
            </td></tr><tr><td>Evaporation Rate:</td><td>
            <asp:TextBox ID="EVAP_RATE_REFTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EVAP_RATE_REF")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>Solubility In Water:</td><td>
            <asp:TextBox ID="SOL_IN_WATERTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SOL_IN_WATER")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>% Volatiles By Volume:</td><td>
            <asp:TextBox ID="PERCENT_VOL_VOLUMETextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PERCENT_VOL_VOLUME")) %>'   MaxLength="15" />
            
            </td></tr><tr><td>Viscosity:</td><td>
            <asp:TextBox ID="VISCOSITYTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VISCOSITY")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>pH:</td><td>
            <asp:TextBox ID="PHTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("PH")) %>'   MaxLength="15" />
            
          </td></tr><tr><td> Autoignition Temp. (C):</td><td>

            <asp:TextBox ID="AUTOIGNITION_TEMPTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("AUTOIGNITION_TEMP")) %>' />
            
         </td></tr><tr><td>   Carcinogen Ind. :</td><td>

            <asp:TextBox ID="CARCINOGEN_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("CARCINOGEN_IND")) %>' />
            
         </td></tr><tr><td>   EPA - Acute :</td><td>

            <asp:TextBox ID="EPA_ACUTETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_ACUTE")) %>' />
            
          </td></tr><tr><td>  EPA - Chronic:</td><td>

            <asp:TextBox ID="EPA_CHRONICTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_CHRONIC")) %>' />
            
        </td></tr><tr><td>    EPA - Fire :</td><td>

            <asp:TextBox ID="EPA_FIRETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_FIRE")) %>' />
            
         </td></tr><tr><td>   EPA - Pressure :</td><td>

            <asp:TextBox ID="EPA_PRESSURETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_PRESSURE")) %>' />
            
        </td></tr><tr><td>    EPA - Reactivity :</td><td>

            <asp:TextBox ID="EPA_REACTIVITYTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_REACTIVITY")) %>' />
            
          </td></tr><tr><td>  Flash Pt. Temp. (C):</td><td>

            <asp:TextBox ID="FLASH_PT_TEMPTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("FLASH_POINT_TEMP")) %>' />
                <asp:RangeValidator ControlToValidate="FLASH_PT_TEMPTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="valRng_txt_qty" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            
         </td></tr><tr><td>   Neutralizing Agent Text:</td><td>

            <asp:TextBox ID="NEUT_AGENTTextBox" runat="server" MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("NEUT_AGENT")) %>' />
            
         </td></tr><tr><td>   NFPA - Flammability :</td><td>

            <asp:TextBox ID="NFPA_FLAMMABILITYTextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_FLAMMABILITY")) %>' />
            
          </td></tr><tr><td>  NFPA - Health:</td><td>

            <asp:TextBox ID="NFPA_HEALTHTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_HEALTH")) %>' />
            
          </td></tr><tr><td>  NFPA - Reactivity :</td><td>

            <asp:TextBox ID="NFPA_REACTIVITYTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_REACTIVITY")) %>' />
            
          </td></tr><tr><td>  NFPA - Special :</td><td>

            <asp:TextBox ID="NFPA_SPECIALTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_SPECIAL")) %>' />
            
          </td></tr><tr><td>  OSHA - Carcinogens:</td><td>

            <asp:TextBox ID="OSHA_CARCINOGENSTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_CARCINOGENS")) %>' />
            
          </td></tr><tr><td>  OSHA - Combustion Liquid:</td><td>

            <asp:TextBox ID="OSHA_COMB_LIQUIDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_COMB_LIQUID")) %>' />
            
          </td></tr><tr><td>  OSHA - Compressed Gas:</td><td>

            <asp:TextBox ID="OSHA_COMP_GASTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_COMP_GAS")) %>' />
            
          </td></tr><tr><td>  OSHA - Corrosive :</td><td>

            <asp:TextBox ID="OSHA_CORROSIVETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_CORROSIVE")) %>' />
            
          </td></tr><tr><td>  OSHA - Explosive:</td><td>

            <asp:TextBox ID="OSHA_EXPLOSIVETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_EXPLOSIVE")) %>' />
           
         </td></tr><tr><td>   OSHA - Flammable:</td><td>

            <asp:TextBox ID="OSHA_FLAMMABLETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_FLAMMABLE")) %>' />
           
         </td></tr><tr><td>   OSHA - Highly Toxic :</td><td>

            <asp:TextBox ID="OSHA_HIGH_TOXICTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_HIGH_TOXIC")) %>' />
           
          </td></tr><tr><td>  OSHA - Irritant:</td><td>

            <asp:TextBox ID="OSHA_IRRITANTTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_IRRITANT")) %>' />
           
          </td></tr><tr><td>  OSHA - Organic Peroxide:</td><td>

            <asp:TextBox ID="OSHA_ORG_PEROXTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_ORG_PEROX")) %>' />
           
         </td></tr><tr><td>   OSHA - Other/Long Term :</td><td>

            <asp:TextBox ID="OSHA_OTHERLONGTERMTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_OTHERLONGTERM")) %>' />
           
          </td></tr><tr><td>  OSHA - Oxidizer :</td><td>

            <asp:TextBox ID="OSHA_OXIDIZERTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_OXIDIZER")) %>' />
          
          </td></tr><tr><td>  OSHA - Pyrophoric :</td><td>

            <asp:TextBox ID="OSHA_PYROTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_PYRO")) %>' />
           
          </td></tr><tr><td>  OSHA - Toxic :</td><td>

            <asp:TextBox ID="OSHA_TOXICTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_TOXIC")) %>' />
           
          </td></tr><tr><td>  OSHA - Sensitizer:</td><td>

            <asp:TextBox ID="OSHA_SENSITIZERTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_SENSITIZER")) %>' />
           
           </td></tr><tr><td> OSHA - Unstable Reactive :</td><td>

            <asp:TextBox ID="OSHA_UNST_REACTTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_UNST_REACT")) %>' />
            
          </td></tr><tr><td>  OSHA - Water Reactive:</td><td>

            <asp:TextBox ID="OSHA_WATER_REACTIVETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_WATER_REACTIVE")) %>' />
           
          </td></tr><tr><td>  Other/Short Term :</td><td>

            <asp:TextBox ID="OTHER_SHORT_TERMTextBox" runat="server" MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("OTHER_SHORT_TERM")) %>' />
            
           </td></tr><tr><td> Physical State Code:</td><td>

            <asp:TextBox ID="PHYS_STATE_CODETextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("PHYS_STATE_CODE")) %>' />
            
           </td></tr><tr><td> Volatile Organic Compound (lb./g):</td><td>

            <asp:TextBox ID="VOC_POUNDS_GALLONTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("VOC_POUNDS_GALLON")) %>' />
            
           </td></tr><tr><td> Volatile Organic Compound (gm/L):</td><td>

            <asp:TextBox ID="VOC_GRAMS_LITERTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("VOC_GRAMS_LITER")) %>' />
            
           </td></tr><tr><td> Volatile Organic Compound (wt %):</td><td>

            <asp:TextBox ID="VOL_ORG_COMP_WTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("VOL_ORG_COMP_WT")) %>' />
            
            </td></tr></table>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            Appearance Odor Text:<br />
            <asp:TextBox ID="APP_ODORTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("APP_ODOR")) %>' Width="450"   MaxLength="255" />
            <br />
            <table>
            <tr><td>Vapor Pressure:</td><td>
            <asp:TextBox ID="VAPOR_PRESSTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VAPOR_PRESS")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>Vapor Density:</td><td>
            <asp:TextBox ID="VAPOR_DENSTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VAPOR_DENS")) %>'   MaxLength="10" />
            
            </td></tr><tr><td>Specific Gravity:</td><td>
            <asp:TextBox ID="SPECIFIC_GRAVTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SPECIFIC_GRAV")) %>'   MaxLength="30" />
            
                        
            </td></tr><tr><td>Evaporation Rate:</td><td>
            <asp:TextBox ID="EVAP_RATE_REFTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EVAP_RATE_REF")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>Solubility In Water:</td><td>
            <asp:TextBox ID="SOL_IN_WATERTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SOL_IN_WATER")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>% Volatiles By Volume:</td><td>
            <asp:TextBox ID="PERCENT_VOL_VOLUMETextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PERCENT_VOL_VOLUME")) %>'   MaxLength="15" />
            
            </td></tr><tr><td>Viscosity:</td><td>
            <asp:TextBox ID="VISCOSITYTextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VISCOSITY")) %>'   MaxLength="30" />
            
            </td></tr><tr><td>pH:</td><td>
            <asp:TextBox ID="PHTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("PH")) %>'   MaxLength="15" />
            
          </td></tr><tr><td> Autoignition Temp. (C):</td><td>

            <asp:TextBox ID="AUTOIGNITION_TEMPTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("AUTOIGNITION_TEMP")) %>' />
            
         </td></tr><tr><td>   Carcinogen Ind. :</td><td>

            <asp:TextBox ID="CARCINOGEN_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("CARCINOGEN_IND")) %>' />
            
         </td></tr><tr><td>   EPA - Acute :</td><td>

            <asp:TextBox ID="EPA_ACUTETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_ACUTE")) %>' />
            
          </td></tr><tr><td>  EPA - Chronic:</td><td>

            <asp:TextBox ID="EPA_CHRONICTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_CHRONIC")) %>' />
            
        </td></tr><tr><td>    EPA - Fire :</td><td>

            <asp:TextBox ID="EPA_FIRETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_FIRE")) %>' />
            
         </td></tr><tr><td>   EPA - Pressure :</td><td>

            <asp:TextBox ID="EPA_PRESSURETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_PRESSURE")) %>' />
            
        </td></tr><tr><td>    EPA - Reactivity :</td><td>

            <asp:TextBox ID="EPA_REACTIVITYTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_REACTIVITY")) %>' />
            
          </td></tr><tr><td>  Flash Pt. Temp. (C):</td><td>

            <asp:TextBox ID="FLASH_PT_TEMPTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("FLASH_PT_TEMP")) %>' />
                <asp:RangeValidator ControlToValidate="FLASH_PT_TEMPTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="valRng_txt_qty" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            
         </td></tr><tr><td>   Neutralizing Agent Text:</td><td>

            <asp:TextBox ID="NEUT_AGENTTextBox" runat="server" MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("NEUT_AGENT")) %>' />
            
         </td></tr><tr><td>   NFPA - Flammability :</td><td>

            <asp:TextBox ID="NFPA_FLAMMABILITYTextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_FLAMMABILITY")) %>' />
            
          </td></tr><tr><td>  NFPA - Health:</td><td>

            <asp:TextBox ID="NFPA_HEALTHTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_HEALTH")) %>' />
            
          </td></tr><tr><td>  NFPA - Reactivity :</td><td>

            <asp:TextBox ID="NFPA_REACTIVITYTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_REACTIVITY")) %>' />
            
          </td></tr><tr><td>  NFPA - Special :</td><td>

            <asp:TextBox ID="NFPA_SPECIALTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("NFPA_SPECIAL")) %>' />
            
          </td></tr><tr><td>  OSHA - Carcinogens:</td><td>

            <asp:TextBox ID="OSHA_CARCINOGENSTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_CARCINOGENS")) %>' />
            
          </td></tr><tr><td>  OSHA - Combustion Liquid:</td><td>

            <asp:TextBox ID="OSHA_COMB_LIQUIDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_COMB_LIQUID")) %>' />
            
          </td></tr><tr><td>  OSHA - Compressed Gas:</td><td>

            <asp:TextBox ID="OSHA_COMP_GASTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_COMP_GAS")) %>' />
            
          </td></tr><tr><td>  OSHA - Corrosive :</td><td>

            <asp:TextBox ID="OSHA_CORROSIVETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_CORROSIVE")) %>' />
            
          </td></tr><tr><td>  OSHA - Explosive:</td><td>

            <asp:TextBox ID="OSHA_EXPLOSIVETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_EXPLOSIVE")) %>' />
           
         </td></tr><tr><td>   OSHA - Flammable:</td><td>

            <asp:TextBox ID="OSHA_FLAMMABLETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_FLAMMABLE")) %>' />
           
         </td></tr><tr><td>   OSHA - Highly Toxic :</td><td>

            <asp:TextBox ID="OSHA_HIGH_TOXICTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_HIGH_TOXIC")) %>' />
           
          </td></tr><tr><td>  OSHA - Irritant:</td><td>

            <asp:TextBox ID="OSHA_IRRITANTTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_IRRITANT")) %>' />
           
          </td></tr><tr><td>  OSHA - Organic Peroxide:</td><td>

            <asp:TextBox ID="OSHA_ORG_PEROXTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_ORG_PEROX")) %>' />
           
         </td></tr><tr><td>   OSHA - Other/Long Term :</td><td>

            <asp:TextBox ID="OSHA_OTHERLONGTERMTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_OTHERLONGTERM")) %>' />
           
          </td></tr><tr><td>  OSHA - Oxidizer :</td><td>

            <asp:TextBox ID="OSHA_OXIDIZERTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_OXIDIZER")) %>' />
          
          </td></tr><tr><td>  OSHA - Pyrophoric :</td><td>

            <asp:TextBox ID="OSHA_PYROTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_PYRO")) %>' />
           
          </td></tr><tr><td>  OSHA - Toxic :</td><td>

            <asp:TextBox ID="OSHA_TOXICTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_TOXIC")) %>' />
           
          </td></tr><tr><td>  OSHA - Sensitizer:</td><td>

            <asp:TextBox ID="OSHA_SENSITIZERTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_SENSITIZER")) %>' />
           
           </td></tr><tr><td> OSHA - Unstable Reactive :</td><td>

            <asp:TextBox ID="OSHA_UNST_REACTTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_UNST_REACT")) %>' />
            
          </td></tr><tr><td>  OSHA - Water Reactive:</td><td>

            <asp:TextBox ID="OSHA_WATER_REACTIVETextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("OSHA_WATER_REACTIVE")) %>' />
           
          </td></tr><tr><td>  Other/Short Term :</td><td>

            <asp:TextBox ID="OTHER_SHORT_TERMTextBox" runat="server" MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("OTHER_SHORT_TERM")) %>' />
            
           </td></tr><tr><td> Physical State Code:</td><td>

            <asp:TextBox ID="PHYS_STATE_CODETextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("PHYS_STATE_CODE")) %>' />
            
           </td></tr><tr><td> Volatile Organic Compound (lb./g):</td><td>

            <asp:TextBox ID="VOC_POUNDS_GALLONTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("VOC_POUNDS_GALLON")) %>' />
            
           </td></tr><tr><td> Volatile Organic Compound (gm/L):</td><td>

            <asp:TextBox ID="VOC_GRAMS_LITERTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("VOC_GRAMS_LITER")) %>' />
            
           </td></tr><tr><td> Volatile Organic Compound (wt %):</td><td>

            <asp:TextBox ID="VOL_ORG_COMP_WTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("VOL_ORG_COMP_WT")) %>' />
            
            </td></tr>
                </table>
           
            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <b>Appearance and Odor:</b>
            <asp:Label ID="APP_ODORLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("APP_ODOR")) %>' />
            <br /><br />
           
           <b> Vapor Pressure:</b>
            <asp:Label ID="VAPOR_PRESSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VAPOR_PRESS")) %>' />
            <br /><br />
           <b> Vapor Density:</b>
            <asp:Label ID="VAPOR_DENSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VAPOR_DENS")) %>' />
            <br /><br />
           <b> Specific Gravity:</b>
            <asp:Label ID="SPECIFIC_GRAVLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SPECIFIC_GRAV")) %>' />
            <br /><br />          
           <b> Evaporation Rate:</b>
            <asp:Label ID="EVAP_RATE_REFLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EVAP_RATE_REF")) %>' />
            <br /><br />
           <b> Solubility In Water:</b>
            <asp:Label ID="SOL_IN_WATERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SOL_IN_WATER")) %>' />
            <br /><br />
           <b> % Volatiles By Volume:</b>
            <asp:Label ID="PERCENT_VOL_VOLUMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PERCENT_VOL_VOLUME")) %>' />
            <br /><br />
           <b> Viscosity:</b>
            <asp:Label ID="VISCOSITYLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("VISCOSITY")) %>' />
            <br /><br />
           <b> pH:</b>
            <asp:Label ID="PHLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("PH")) %>' />
            <br /><br />
              <b>     Autoignition Temp.:</b>
            <asp:Label ID="AUTOIGNITION_TEMPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AUTOIGNITION_TEMP")) %>' />
            <br /><br />
           <b>     Carcinogen Ind.:</b>
            <asp:Label ID="CARCINOGEN_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("CARCINOGEN_IND")) %>' />
            <br /><br />
          <b>      EPA - Acute :</b>
            <asp:Label ID="EPA_ACUTELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("EPA_ACUTE")) %>' />
            <br /><br />
           <b>     EPA - Chronic:</b>
            <asp:Label ID="EPA_CHRONICLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EPA_CHRONIC")) %>' />
            <br /><br />
           <b>     EPA - Fire :</b>
            <asp:Label ID="EPA_FIRELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("EPA_FIRE")) %>' />
            <br /><br />
           <b>     EPA - Pressure :</b>
            <asp:Label ID="EPA_PRESSURELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EPA_PRESSURE")) %>' />
            <br /><br />
           <b>     EPA - Reactivity :</b>
            <asp:Label ID="EPA_REACTIVITYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EPA_REACTIVITY")) %>' />
            <br /><br />
          <b>      Flash Pt. Temp. (C):</b>
            <asp:Label ID="FLASH_PT_TEMPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("FLASH_PT_TEMP")) %>' />
            <br /><br />
          <b>      Neutralizing Agent Text:</b>
            <asp:Label ID="NEUT_AGENTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NEUT_AGENT")) %>' />
            <br /><br />
          <b>      NFPA - Flammability :</b>
            <asp:Label ID="NFPA_FLAMMABILITYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NFPA_FLAMMABILITY")) %>' />
            <br /><br />
         <b>       NFPA - Health:</b>
            <asp:Label ID="NFPA_HEALTHLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NFPA_HEALTH")) %>' />
            <br /><br />
          <b>      NFPA - Reactivity :</b>
            <asp:Label ID="NFPA_REACTIVITYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NFPA_REACTIVITY")) %>' />
            <br /><br />
         <b>       NFPA - Special :</b>
            <asp:Label ID="NFPA_SPECIALLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NFPA_SPECIAL")) %>' />
            <br /><br />
         <b>       OSHA - Carcinogens:</b>
            <asp:Label ID="OSHA_CARCINOGENSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_CARCINOGENS")) %>' />
            <br /><br />
          <b>      OSHA - Combustion Liquid:</b>
            <asp:Label ID="OSHA_COMB_LIQUIDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_COMB_LIQUID")) %>' />
            <br /><br />
         <b>       OSHA - Compressed Gas:</b>
            <asp:Label ID="OSHA_COMP_GASLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_COMP_GAS")) %>' />
            <br /><br />
          <b>      OSHA - Corrosive :</b>
            <asp:Label ID="OSHA_CORROSIVELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_CORROSIVE")) %>' />
            <br /><br />
          <b>      OSHA - Explosive:</b>
            <asp:Label ID="OSHA_EXPLOSIVELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_EXPLOSIVE")) %>' />
            <br /><br />
         <b>       OSHA - Flammable:</b>
            <asp:Label ID="OSHA_FLAMMABLELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_FLAMMABLE")) %>' />
            <br /><br />
         <b>        OSHA - Highly Toxic :</b>
            <asp:Label ID="OSHA_HIGH_TOXICLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_HIGH_TOXIC")) %>' />
            <br /><br />
         <b>       OSHA - Irritant:</b>
            <asp:Label ID="OSHA_IRRITANTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_IRRITANT")) %>' />
            <br /><br />
          <b>      OSHA - Organic Peroxide:</b>
            <asp:Label ID="OSHA_ORG_PEROXLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_ORG_PEROX")) %>' />
            <br /><br />
          <b>       OSHA - Other/Long Term :</b>
            <asp:Label ID="OSHA_OTHERLONGTERMLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_OTHERLONGTERM")) %>' />
            <br /><br />
          <b>      OSHA - Oxidizer :</b>
            <asp:Label ID="OSHA_OXIDIZERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_OXIDIZER")) %>' />
            <br /><br />
         <b>       OSHA - Pyrophoric :</b>
            <asp:Label ID="OSHA_PYROLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("OSHA_PYRO")) %>' />
            <br /><br />
         <b>       OSHA - Toxic :</b>
            <asp:Label ID="OSHA_TOXICLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_TOXIC")) %>' />
            <br /><br />
        <b>        OSHA - Sensitizer:</b>
            <asp:Label ID="OSHA_SENSITIZERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_SENSITIZER")) %>' />
            <br /><br />
        <b>         OSHA - Unstable Reactive :</b>
            <asp:Label ID="OSHA_UNST_REACTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_UNST_REACT")) %>' />
            <br /><br />
         <b>         OSHA - Water Reactive:</b>
            <asp:Label ID="OSHA_WATER_REACTIVELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OSHA_WATER_REACTIVE")) %>' />
            <br /><br />
        <b>        Other/Short Term :</b>
            <asp:Label ID="OTHER_SHORT_TERMLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OTHER_SHORT_TERM")) %>' />
            <br /><br />
        <b>        Physical State Code:</b>
            <asp:Label ID="PHYS_STATE_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PHYS_STATE_CODE")) %>' />
            <br /><br />
        <b>        Volatile Organic Compound (lb./g):</b>
            <asp:Label ID="VOC_POUNDS_GALLONLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VOC_POUNDS_GALLON")) %>' />
            <br /><br />
        <b>         Volatile Organic Compound (gm/L):</b>
            <asp:Label ID="VOC_GRAMS_LITERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VOC_GRAMS_LITER")) %>' />
            <br /><br />
         <b>       Volatile Organic Compound (wt %):  </b>
            <asp:Label ID="VOL_ORG_COMP_WTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("VOL_ORG_COMP_WT")) %>' />
            <br /><br />
        </ItemTemplate>
    </asp:FormView>

    <b> Component/Ingredient Information:</b>
    <a name="ingredients"></a>
    <asp:FormView ID="FormViewIngredients" runat="server" SkinID="FormView" 
             EmptyDataText="No data to display" 
            EnableModelValidation="True" Width="100%" AllowPaging="True" DefaultMode="Edit"
            onpageindexchanging="FormViewIngredients_PageIndexChanging">
           <EditItemTemplate>
                <div style="float:right">
               <asp:GridView ID="GridViewIngredients" runat="server" AutoGenerateColumns="false">               
               <Columns>
               <asp:BoundField DataField="INGREDIENT_NAME" HeaderText="Ingredient Names" />
               <asp:BoundField DataField="PRCNT" HeaderText="(%)Percent" />
               </Columns>
               </asp:GridView>
           </div>
               <table>
            <tr><td>Component Ingredient Name:</td><td>
               <asp:TextBox ID="INGREDIENT_NAMETextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("INGREDIENT_NAME")) %>'  MaxLength="255" />
               <br />
               </td></tr><tr><td>% Text Value:</td><td>
               <asp:TextBox ID="PRCNTTextBox" runat="server"  MaxLength="20" Text='<%# Server.HtmlEncode(StrEval("PRCNT")) %>' />
               <br />
               
               </td></tr><tr><td>CAS Number:</td><td>
               <asp:TextBox ID="CAS_CODETextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("CAS")) %>'  MaxLength="20" />
               <br />
               </td></tr><tr><td>RTECS #:</td><td>
               <asp:TextBox ID="RTECS_NUMTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("RTECS_NUM")) %>'  MaxLength="10" />
               <br />
               </td></tr><tr><td>RTECS Code:</td><td>
               <asp:TextBox ID="RTECS_CODETextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("RTECS_CODE")) %>'  MaxLength="10" />
               <br />
               </td></tr><tr><td>OSHA PEL:</td><td>
               <asp:TextBox ID="OSHA_PELTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("OSHA_PEL")) %>'  MaxLength="20" />
               <br />              
               </td></tr><tr><td>OSHA STEL:</td><td>
               <asp:TextBox ID="OSHA_STELTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("OSHA_STEL")) %>'  MaxLength="20" />
               <br />               
               </td></tr><tr><td>ACGIH TLV:</td><td>
               <asp:TextBox ID="ACGIH_TLVTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("ACGIH_TLV")) %>'  MaxLength="20" />
               <br />               
               </td></tr><tr><td>ACGIH STEL:</td><td>
               <asp:TextBox ID="ACGIH_STELTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("ACGIH_STEL")) %>'  MaxLength="20" />
               <br />              
               </td></tr><tr><td>EPA RQ:</td><td>
               <asp:TextBox ID="EPA_REPORT_QTYTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("EPA_REPORT_QTY")) %>'  MaxLength="10" />
               <br />
               </td></tr><tr><td>DOT RQ:</td><td>
               <asp:TextBox ID="DOT_REPORT_QTYTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("DOT_REPORT_QTY")) %>'  MaxLength="10" />
               <br />
              </td></tr> <tr><td> % Volume Value:  </td><td>
            <asp:TextBox ID="PRCNT_VOL_VALUETextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("PRCNT_VOL_VALUE")) %>' />
            <br />
           </td></tr><tr><td> % Weight Value:</td><td>
            <asp:TextBox ID="PRCNT_VOL_WEIGHTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("PRCNT_VOL_WEIGHT")) %>' />
            <br />
           </td></tr><tr><td> Chemical Manufacturer Company Name:</td><td>
            <asp:TextBox ID="CHEM_MFG_COMP_NAMETextBox" runat="server"   MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("CHEM_MFG_COMP_NAME")) %>' />
            <br />
           </td></tr><tr><td> ODS Ind:</td><td>
            <asp:TextBox ID="ODS_INDTextBox" runat="server"  MaxLength="1"
            Text='<%# Server.HtmlEncode(StrEval("ODS_IND")) %>' />
            <br />
           </td></tr><tr><td> Other Recorded Limits:</td><td>
            <asp:TextBox ID="OTHER_REC_LIMITSTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("OTHER_REC_LIMITS")) %>' />
               
               
             </td></tr><tr><td> </td><td> <asp:LinkButton ID="btn_addIngredient" runat="server" 
            Text="Enter Another Ingredient" onclick="btn_addIngredient_Click" /><br /></td></tr>
                </table>
               <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                   CommandName="Update" Text="Update" />
               &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                   CausesValidation="False" CommandName="Cancel" Text="Cancel" />
           </EditItemTemplate>
           <InsertItemTemplate>
           <div style="float:right">
               <asp:GridView ID="GridViewIngredients" runat="server" AutoGenerateColumns="false">               
               <Columns>
               <asp:BoundField DataField="INGREDIENT_NAME" HeaderText="Ingredient Names" />
               <asp:BoundField DataField="PRCNT" HeaderText="(%)Percent" />
               </Columns>
               </asp:GridView>
           </div>
               <table>
            <tr><td>Component Ingredient Name:</td><td>
               <asp:TextBox ID="INGREDIENT_NAMETextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("INGREDIENT_NAME")) %>'  MaxLength="255" />
               <br />
               </td></tr><tr><td>% Text Value:</td><td>
               <asp:TextBox ID="PRCNTTextBox" runat="server"  MaxLength="20" Text='<%# Server.HtmlEncode(StrEval("PRCNT")) %>' />
               <br />
               
               </td></tr><tr><td>CAS Number:</td><td>
               <asp:TextBox ID="CAS_CODETextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("CAS")) %>'  MaxLength="20" />
               <br />
               </td></tr><tr><td>RTECS #:</td><td>
               <asp:TextBox ID="RTECS_NUMTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("RTECS_NUM")) %>'  MaxLength="10" />
               <br />
               </td></tr><tr><td>RTECS Code:</td><td>
               <asp:TextBox ID="RTECS_CODETextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("RTECS_CODE")) %>'  MaxLength="10" />
               <br />
               </td></tr><tr><td>OSHA PEL:</td><td>
               <asp:TextBox ID="OSHA_PELTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("OSHA_PEL")) %>'  MaxLength="20" />
               <br />              
               </td></tr><tr><td>OSHA STEL:</td><td>
               <asp:TextBox ID="OSHA_STELTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("OSHA_STEL")) %>'  MaxLength="20" />
               <br />               
               </td></tr><tr><td>ACGIH TLV:</td><td>
               <asp:TextBox ID="ACGIH_TLVTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("ACGIH_TLV")) %>'  MaxLength="20" />
               <br />               
               </td></tr><tr><td>ACGIH STEL:</td><td>
               <asp:TextBox ID="ACGIH_STELTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("ACGIH_STEL")) %>'  MaxLength="20" />
               <br />              
               </td></tr><tr><td>EPA RQ:</td><td>
               <asp:TextBox ID="EPA_REPORT_QTYTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("EPA_REPORT_QTY")) %>'  MaxLength="10" />
               <br />
               </td></tr><tr><td>DOT RQ:</td><td>
               <asp:TextBox ID="DOT_REPORT_QTYTextBox" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("DOT_REPORT_QTY")) %>'  MaxLength="10" />
               <br />
              </td></tr> <tr><td> % Volume Value:  </td><td>
            <asp:TextBox ID="PRCNT_VOL_VALUETextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("PRCNT_VOL_VALUE")) %>' />
            <br />
           </td></tr><tr><td> % Weight Value:</td><td>
            <asp:TextBox ID="PRCNT_VOL_WEIGHTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("PRCNT_VOL_WEIGHT")) %>' />
            <br />
           </td></tr><tr><td> Chemical Manufacturer Company Name:</td><td>
            <asp:TextBox ID="CHEM_MFG_COMP_NAMETextBox" runat="server"   MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("CHEM_MFG_COMP_NAME")) %>' />
            <br />
           </td></tr><tr><td> ODS Ind:</td><td>
            <asp:TextBox ID="ODS_INDTextBox" runat="server"  MaxLength="1"
            Text='<%# Server.HtmlEncode(StrEval("ODS_IND")) %>' />
            <br />
           </td></tr><tr><td> Other Recorded Limits:</td><td>
            <asp:TextBox ID="OTHER_REC_LIMITSTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("OTHER_REC_LIMITS")) %>' />
               
               
             </td></tr><tr><td> </td><td> <asp:LinkButton ID="btn_addIngredient" runat="server" 
            Text="Enter Another Ingredient" onclick="btn_addIngredient_Click" /><br /></td></tr>
                </table>

                <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                   CommandName="Insert" Text="Insert" />
               &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                   CausesValidation="False" CommandName="Cancel" Text="Cancel" />
           </InsertItemTemplate>
           <ItemTemplate>
               <b>Component Ingredient Name:</b>
               <asp:Label ID="INGREDIENT_NAMELabel" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("INGREDIENT_NAME")) %>' />
               <br /><br />
               <b>% Text Value:</b>
               <asp:Label ID="PRCNTLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("PRCNT")) %>' />
               <br /><br />               
               <b>CAS Number:</b>
               <asp:Label ID="CAS_CODELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CAS")) %>' />
               <br /><br />
               <b>RTECS #:</b>
               <asp:Label ID="RTECS_NUMLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("RTECS_NUM")) %>' />
               <br /><br />
              <b> RTECS Code:</b>
               <asp:Label ID="RTECS_CODELabel" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("RTECS_CODE")) %>' />
               <br /><br />
              <b> OSHA PEL:</b>
               <asp:Label ID="OSHA_PELLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("OSHA_PEL")) %>' />
               <br /><br />             
              <b> OSHA STEL:</b>
               <asp:Label ID="OSHA_STELLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("OSHA_STEL")) %>' />
               <br /><br />             
              <b> ACGIH TLV:</b>
               <asp:Label ID="ACGIH_TLVLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("ACGIH_TLV")) %>' />
               <br />
              <br />
              <b> ACGIH STEL:</b>
               <asp:Label ID="ACGIH_STELLabel" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("ACGIH_STEL")) %>' />
               <br />
               <br />
               <b>EPA RQ:</b>
               <asp:Label ID="EPA_REPORT_QTYLabel" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("EPA_REPORT_QTY")) %>' />
               <br /><br />
               <b>DOT RQ:</b>
               <asp:Label ID="DOT_REPORT_QTYLabel" runat="server" 
                   Text='<%# Server.HtmlEncode(StrEval("DOT_REPORT_QTY")) %>' />
               <br /><br />
              <b>% Volume Value :</b>
            <asp:Label ID="PRCNT_VOL_VALUELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRCNT_VOL_VALUE")) %>' />
            <br /><br />
            <b>% Weight Value :</b>
            <asp:Label ID="PRCNT_VOL_WEIGHTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PRCNT_VOL_WEIGHT")) %>' />
            <br /><br />
            <b>Chemical Manufacturer Company Name :</b>
            <asp:Label ID="CHEM_MFG_COMP_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("CHEM_MFG_COMP_NAME")) %>' />
            <br /><br />
           <b> ODS Ind:</b>
            <asp:Label ID="ODS_INDLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("ODS_IND")) %>' />
            <br /><br />
           <b> Other Recorded Limits:</b>
            <asp:Label ID="OTHER_REC_LIMITSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("OTHER_REC_LIMITS")) %>' />
            <br /><br />

           </ItemTemplate>
    </asp:FormView>

    <b>Contractor Information:</b>
    <asp:FormView ID="FormViewContractorInfo" SkinID="FormViewAlt" runat="server"  
        EnableModelValidation="True" Width="100%" AllowPaging="True"
            EmptyDataText="No data to display" DefaultMode="Edit"
            onpageindexchanging="FormViewContractorInfo_PageIndexChanging">
        <EditItemTemplate>
          
         <div style="float:right">
               <asp:GridView ID="GridViewContractors" runat="server" AutoGenerateColumns="false">               
               <Columns>
               <asp:BoundField DataField="CT_NUMBER" HeaderText="Contract Number" />
               <asp:BoundField DataField="CT_CAGE" HeaderText="Contractor CAGE" />
               <asp:BoundField DataField="PURCHASE_ORDER_NO" HeaderText="Purchase Order No." />
               </Columns>
               </asp:GridView>
           </div>


         <table>
            <tr><td>   Contract Number:</td><td>
            <asp:TextBox ID="CT_NUMBERTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("CT_NUMBER")) %>' />
            </td></tr><tr><td>  
            Contractor CAGE:</td><td>
            <asp:TextBox ID="CT_CAGETextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_CAGE")) %>' MaxLength="10"/>
            </td></tr><tr><td>  
            Contractor City:</td><td>
            <asp:TextBox ID="CT_CITYTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_CITY")) %>' MaxLength="50"/>
            </td></tr><tr><td>  
            Contractor State:</td><td>
            <asp:TextBox ID="CT_STATETextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("CT_STATE")) %>' />
            </td></tr><tr><td>  
            Contractor Company Name:</td><td>
            <asp:TextBox ID="CT_COMPANY_NAMETextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("CT_COMPANY_NAME")) %>' />
           </td></tr><tr><td>  
            Contractor Country :</td><td>
            <asp:TextBox ID="CT_COUNTRYTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("CT_COUNTRY")) %>' />
            </td></tr><tr><td>  
            Contractor P.O. Box:</td><td>
            <asp:TextBox ID="CT_PO_BOXTextBox" runat="server" MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("CT_PO_BOX")) %>' />
           </td></tr><tr><td>  
            Contractor Telephone Number:</td><td>
            <asp:TextBox ID="CT_PHONETextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("CT_PHONE")) %>' />
            </td></tr><tr><td>  
            Purchase Order Number:</td><td>
            <asp:TextBox ID="PURCHASE_ORDER_NOTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("PURCHASE_ORDER_NO")) %>' />
             
             </td></tr><tr><td> </td><td> <asp:LinkButton ID="btn_addContractor" runat="server" 
            Text="Enter Another Contractor" onclick="btn_addContractor_Click" /><br /></td></tr>
                </table>

            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>

         <div style="float:right">
               <asp:GridView ID="GridViewContractors" runat="server" AutoGenerateColumns="false">               
               <Columns>
               <asp:BoundField DataField="CT_NUMBER" HeaderText="Contract Number" />
               <asp:BoundField DataField="CT_CAGE" HeaderText="Contractor CAGE" />
               <asp:BoundField DataField="PURCHASE_ORDER_NO" HeaderText="Purchase Order No." />
               </Columns>
               </asp:GridView>
           </div>


         <table>
            <tr><td>   Contract Number:</td><td>
            <asp:TextBox ID="CT_NUMBERTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("CT_NUMBER")) %>' />
            </td></tr><tr><td>  
            Contractor CAGE:</td><td>
            <asp:TextBox ID="CT_CAGETextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_CAGE")) %>' MaxLength="10"/>
            </td></tr><tr><td>  
            Contractor City:</td><td>
            <asp:TextBox ID="CT_CITYTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_CITY")) %>' MaxLength="50"/>
            </td></tr><tr><td>  
            Contractor State:</td><td>
            <asp:TextBox ID="CT_STATETextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("CT_STATE")) %>' />
            </td></tr><tr><td>  
            Contractor Company Name:</td><td>
            <asp:TextBox ID="CT_COMPANY_NAMETextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("CT_COMPANY_NAME")) %>' />
           </td></tr><tr><td>  
            Contractor Country :</td><td>
            <asp:TextBox ID="CT_COUNTRYTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("CT_COUNTRY")) %>' />
            </td></tr><tr><td>  
            Contractor P.O. Box:</td><td>
            <asp:TextBox ID="CT_PO_BOXTextBox" runat="server" MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("CT_PO_BOX")) %>' />
           </td></tr><tr><td>  
            Contractor Telephone Number:</td><td>
            <asp:TextBox ID="CT_PHONETextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("CT_PHONE")) %>' />
            </td></tr><tr><td>  
            Purchase Order Number:</td><td>
            <asp:TextBox ID="PURCHASE_ORDER_NOTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("PURCHASE_ORDER_NO")) %>' />
             
             </td></tr><tr><td> </td><td> <asp:LinkButton ID="btn_addContractor" runat="server" 
            Text="Enter Another Contractor" onclick="btn_addContractor_Click" /><br /></td></tr>
                </table>

            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <b>Contract Number:</b>
            <asp:Label ID="CT_NUMBERLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_NUMBER")) %>' />
            <br /><br />
            <b> Contractor CAGE:</b>
            <asp:Label ID="CT_CAGELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_CAGE")) %>' />
            <br /><br />
           <b> Contractor City:</b>
            <asp:Label ID="CT_CITYLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_CITY")) %>' />
            <br /><br />
           <b> Contractor State:</b>
            <asp:Label ID="CT_STATELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_STATE")) %>' />
            <br /><br />
           <b> Contractor Company Name:</b>
            <asp:Label ID="CT_COMPANY_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("CT_COMPANY_NAME")) %>' />
            <br /><br />
            <b>Contractor Country:</b>
            <asp:Label ID="CT_COUNTRYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("CT_COUNTRY")) %>' />
            <br /><br />
           <b> Contractor P.O. Box:</b>
            <asp:Label ID="CT_PO_BOXLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_PO_BOX")) %>' />
            <br /><br />
           <b> Contractor Telephone Number:</b>
            <asp:Label ID="CT_PHONELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("CT_PHONE")) %>' />
            <br /><br />
           <b> Purchase Order Number:</b>
            <asp:Label ID="PURCHASE_ORDER_NOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("PURCHASE_ORDER_NO")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>Radiological Information:</b>
    <asp:FormView ID="FormViewRadiologicalInfo" SkinID="FormView" runat="server"  
        EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
          <table>
            <tr><td> NRC License/ Permit Number:</td><td>
            <asp:TextBox ID="NRC_LP_NUMTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("NRC_LP_NUM")) %>' />
            </td></tr><tr><td>
            Operator:</td><td>
            <asp:TextBox ID="OPERATORTextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("OPERATOR")) %>' />
            </td></tr><tr><td>
            Radioactive Amount (Microcuries):</td><td>
            <asp:TextBox ID="RAD_AMOUNT_MICROTextBox" runat="server" MaxLength="22"
                Text='<%# Server.HtmlEncode(StrEval("RAD_AMOUNT_MICRO")) %>' />
            </td></tr><tr><td>
            Radioactive Form:</td><td>
            <asp:TextBox ID="RAD_FORMTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("RAD_FORM")) %>' />
            </td></tr><tr><td>
            Radioisotope CAS:</td><td>
            <asp:TextBox ID="RAD_CASTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("RAD_CAS")) %>' MaxLength="20"/>
            </td></tr><tr><td>
            Radioisotope Name:</td><td>
            <asp:TextBox ID="RAD_NAMETextBox" runat="server" MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("RAD_NAME")) %>' />
            </td></tr><tr><td>
            Radioisotope Symbol:</td><td>
            <asp:TextBox ID="RAD_SYMBOLTextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("RAD_SYMBOL")) %>' />
            </td></tr><tr><td>
            Replacement NSN:</td><td>
            <asp:TextBox ID="REP_NSNTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("REP_NSN")) %>' MaxLength="15" />
            </td></tr><tr><td>
            Sealed:</td><td>
            <asp:TextBox ID="SEALEDTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("SEALED")) %>' MaxLength="1" />
           </td></tr></table>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
           <table>
            <tr><td> NRC License/ Permit Number:</td><td>
            <asp:TextBox ID="NRC_LP_NUMTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("NRC_LP_NUM")) %>' />
            </td></tr><tr><td>
            Operator:</td><td>
            <asp:TextBox ID="OPERATORTextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("OPERATOR")) %>' />
            </td></tr><tr><td>
            Radioactive Amount (Microcuries):</td><td>
            <asp:TextBox ID="RAD_AMOUNT_MICROTextBox" runat="server" MaxLength="22"
                Text='<%# Server.HtmlEncode(StrEval("RAD_AMOUNT_MICRO")) %>' />
            </td></tr><tr><td>
            Radioactive Form:</td><td>
            <asp:TextBox ID="RAD_FORMTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("RAD_FORM")) %>' />
            </td></tr><tr><td>
            Radioisotope CAS:</td><td>
            <asp:TextBox ID="RAD_CASTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("RAD_CAS")) %>' MaxLength="20"/>
            </td></tr><tr><td>
            Radioisotope Name:</td><td>
            <asp:TextBox ID="RAD_NAMETextBox" runat="server" MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("RAD_NAME")) %>' />
            </td></tr><tr><td>
            Radioisotope Symbol:</td><td>
            <asp:TextBox ID="RAD_SYMBOLTextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("RAD_SYMBOL")) %>' />
            </td></tr><tr><td>
            Replacement NSN:</td><td>
            <asp:TextBox ID="REP_NSNTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("REP_NSN")) %>' MaxLength="15" />
            </td></tr><tr><td>
            Sealed:</td><td>
            <asp:TextBox ID="SEALEDTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("SEALED")) %>' MaxLength="1" />
           </td></tr></table>


            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
           <b>  NRC License/ Permit Number:</b>
            <asp:Label ID="NRC_LP_NUMLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NRC_LP_NUM")) %>' />
            <br /><br />
            <b>Operator:</b>
            <asp:Label ID="OPERATORLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("OPERATOR")) %>' />
            <br /><br />
            <b>Radioactive Amount (Microcuries):</b>
            <asp:Label ID="RAD_AMOUNT_MICROLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("RAD_AMOUNT_MICRO")) %>' />
            <br /><br />
            <b>Radioactive Form:</b>
            <asp:Label ID="RAD_FORMLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("RAD_FORM")) %>' />
            <br /><br />
            <b>Radioisotope CAS:</b>
            <asp:Label ID="RAD_CASLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("RAD_CAS")) %>' />
            <br /><br />
           <b> Radioisotope Name:</b>
            <asp:Label ID="RAD_NAMELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("RAD_NAME")) %>' />
            <br /><br />
            <b>Radioisotope Symbol:</b>
            <asp:Label ID="RAD_SYMBOLLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("RAD_SYMBOL")) %>' />
            <br /><br />
            <b>Replacement NSN:</b>
            <asp:Label ID="REP_NSNLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("REP_NSN")) %>' />
            <br /><br />
           <b> Sealed:</b>
            <asp:Label ID="SEALEDLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("SEALED")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>Transportation Information:</b>
    <asp:FormView ID="FormViewTransportation" runat="server"  SkinID="FormViewAlt"
        EnableModelValidation="True"  Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
        <table>
            <tr><td>  AF MMAC Code:</td><td>
            <asp:TextBox ID="AF_MMAC_CODETextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AF_MMAC_CODE")) %>' />
            </td></tr><tr><td>
            Certificate of Equivalency (COE):</td><td>
            <asp:TextBox ID="CERTIFICATE_COETextBox" runat="server" MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("CERTIFICATE_COE")) %>' />
            </td></tr><tr><td>
            Competent Authority Approval (CAA):</td><td>
            <asp:TextBox ID="COMPETENT_CAATextBox" runat="server" MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("COMPETENT_CAA")) %>' />
            </td></tr><tr><td>
            Department of Defense ID Code:</td><td>
            <asp:TextBox ID="DOD_ID_CODETextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("DOD_ID_CODE")) %>' />
            </td></tr><tr><td>
            DOT Exemption #:</td><td>
            <asp:TextBox ID="DOT_EXEMPTION_NOTextBox" runat="server" MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("DOT_EXEMPTION_NO")) %>' />
            </td></tr><tr><td>
            DOT RQ Ind:</td><td>
            <asp:TextBox ID="DOT_RQ_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("DOT_RQ_IND")) %>' />
            </td></tr><tr><td>
            EX #:</td><td>
            <asp:TextBox ID="EX_NOTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("EX_NO")) %>' MaxLength="15" />
            </td></tr><tr><td>
            Flash Pt. Temp.:</td><td>
            <asp:TextBox ID="TRAN_FLASH_PT_TEMPTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("FLASH_PT_TEMP")) %>' />
                <asp:RangeValidator ControlToValidate="TRAN_FLASH_PT_TEMPTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="valRng_txt_qty" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            </td></tr><tr><td>            
            High Explosive Weight:</td><td>
            <asp:TextBox ID="HIGH_EXPLOSIVE_WTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("HIGH_EXPLOSIVE_WT")) %>' />
                 <asp:RangeValidator ControlToValidate="HIGH_EXPLOSIVE_WTTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="RangeValidator1" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            </td></tr><tr><td>
            Ltd Qty Ind:</td><td>
            <asp:TextBox ID="LTD_QTY_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("LTD_QTY_IND")) %>' />
            </td></tr><tr><td>
            Magnetic Ind:</td><td>
            <asp:TextBox ID="MAGNETIC_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("MAGNETIC_IND")) %>' />
            </td></tr><tr><td>
            Magnetism (Magnetic Strength):</td><td>
            <asp:TextBox ID="MAGNETISMTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("MAGNETISM")) %>' />
            </td></tr><tr><td>
            Marine Pollutant Ind:</td><td>
            <asp:TextBox ID="MARINE_POLLUTANT_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("MARINE_POLLUTANT_IND")) %>' />
            </td></tr><tr><td>
            Net Explosive Qty. Distance Weight:</td><td>
            <asp:TextBox ID="NET_EXP_QTY_DISTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("NET_EXP_QTY_DIST")) %>' />
                 <asp:RangeValidator ControlToValidate="NET_EXP_QTY_DISTTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="RangeValidator2" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            </td></tr><tr><td>
            Net Explosive Weight (kg):</td><td>
            <asp:TextBox ID="NET_EXP_WEIGHTTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("NET_EXP_WEIGHT")) %>' />
            </td></tr><tr><td>
            Net Propellant Weight (kg):</td><td>
            <asp:TextBox ID="NET_PROPELLANT_WTTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("NET_PROPELLANT_WT")) %>' />
            </td></tr><tr><td>
            NOS Technical Shipping Name:</td><td>
            <asp:TextBox ID="NOS_TECHNICAL_SHIPPING_NAMETextBox" runat="server" MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("NOS_TECHNICAL_SHIPPING_NAME")) %>' />
            </td></tr><tr><td>
            Transportation Additional Data:</td><td>
            <asp:TextBox ID="TRANSPORTATION_ADDITIONAL_DATATextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("TRANSPORTATION_ADDITIONAL_DATA")) %>' />
            </td></tr></table>

            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
          <table>
            <tr><td>  AF MMAC Code:</td><td>
            <asp:TextBox ID="AF_MMAC_CODETextBox" runat="server" MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AF_MMAC_CODE")) %>' />
            </td></tr><tr><td>
            Certificate of Equivalency (COE):</td><td>
            <asp:TextBox ID="CERTIFICATE_COETextBox" runat="server" MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("CERTIFICATE_COE")) %>' />
            </td></tr><tr><td>
            Competent Authority Approval (CAA):</td><td>
            <asp:TextBox ID="COMPETENT_CAATextBox" runat="server" MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("COMPETENT_CAA")) %>' />
            </td></tr><tr><td>
            Department of Defense ID Code:</td><td>
            <asp:TextBox ID="DOD_ID_CODETextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("DOD_ID_CODE")) %>' />
            </td></tr><tr><td>
            DOT Exemption #:</td><td>
            <asp:TextBox ID="DOT_EXEMPTION_NOTextBox" runat="server" MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("DOT_EXEMPTION_NO")) %>' />
            </td></tr><tr><td>
            DOT RQ Ind:</td><td>
            <asp:TextBox ID="DOT_RQ_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("DOT_RQ_IND")) %>' />
            </td></tr><tr><td>
            EX #:</td><td>
            <asp:TextBox ID="EX_NOTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("EX_NO")) %>' MaxLength="15" />
            </td></tr><tr><td>
            Flash Pt. Temp.:</td><td>
            <asp:TextBox ID="TRAN_FLASH_PT_TEMPTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("TRAN_FLASH_POINT_TEMP")) %>' />
                <asp:RangeValidator ControlToValidate="TRAN_FLASH_PT_TEMPTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="valRng_txt_qty" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            </td></tr><tr><td>            
            High Explosive Weight:</td><td>
            <asp:TextBox ID="HIGH_EXPLOSIVE_WTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("HIGH_EXPLOSIVE_WT")) %>' />
                 <asp:RangeValidator ControlToValidate="HIGH_EXPLOSIVE_WTTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="RangeValidator1" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            </td></tr><tr><td>
            Ltd Qty Ind:</td><td>
            <asp:TextBox ID="LTD_QTY_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("LTD_QTY_IND")) %>' />
            </td></tr><tr><td>
            Magnetic Ind:</td><td>
            <asp:TextBox ID="MAGNETIC_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("MAGNETIC_IND")) %>' />
            </td></tr><tr><td>
            Magnetism (Magnetic Strength):</td><td>
            <asp:TextBox ID="MAGNETISMTextBox" runat="server" MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("MAGNETISM")) %>' />
            </td></tr><tr><td>
            Marine Pollutant Ind:</td><td>
            <asp:TextBox ID="MARINE_POLLUTANT_INDTextBox" runat="server" MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("MARINE_POLLUTANT_IND")) %>' />
            </td></tr><tr><td>
            Net Explosive Qty. Distance Weight:</td><td>
            <asp:TextBox ID="NET_EXP_QTY_DISTTextBox" runat="server" MaxLength="18"
                Text='<%# Server.HtmlEncode(StrEval("NET_EXP_QTY_DIST")) %>' />
                 <asp:RangeValidator ControlToValidate="NET_EXP_QTY_DISTTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="RangeValidator2" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     
            </td></tr><tr><td>
            Net Explosive Weight (kg):</td><td>
            <asp:TextBox ID="NET_EXP_WEIGHTTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("NET_EXP_WEIGHT")) %>' />
            </td></tr><tr><td>
            Net Propellant Weight (kg):</td><td>
            <asp:TextBox ID="NET_PROPELLANT_WTTextBox" runat="server" MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("NET_PROPELLANT_WT")) %>' />
            </td></tr><tr><td>
            NOS Technical Shipping Name:</td><td>
            <asp:TextBox ID="NOS_TECHNICAL_SHIPPING_NAMETextBox" runat="server" MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("NOS_TECHNICAL_SHIPPING_NAME")) %>' />
            </td></tr><tr><td>
            Transportation Additional Data:</td><td>
            <asp:TextBox ID="TRANSPORTATION_ADDITIONAL_DATATextBox" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("TRANSPORTATION_ADDITIONAL_DATA")) %>' />
            </td></tr></table>

            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <b> AF MMAC Code:</b>
            <asp:Label ID="AF_MMAC_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AF_MMAC_CODE")) %>' />
            <br /><br />
           <b> Certificate of Equivalency (COE):</b>
            <asp:Label ID="CERTIFICATE_COELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("CERTIFICATE_COE")) %>' />
            <br /><br />
            <b>Competent Authority Approval (CAA):</b>
            <asp:Label ID="COMPETENT_CAALabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("COMPETENT_CAA")) %>' />
            <br /><br />
            <b>Department of Defense ID Code:</b>
            <asp:Label ID="DOD_ID_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOD_ID_CODE")) %>' />
            <br /><br />
          <b>  DOT Exemption #:</b>
            <asp:Label ID="DOT_EXEMPTION_NOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_EXEMPTION_NO")) %>' />
            <br /><br />
           <b> DOT RQ Ind:</b>
            <asp:Label ID="DOT_RQ_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_RQ_IND")) %>' />
            <br /><br />
          <b>  EX #:</b>
            <asp:Label ID="EX_NOLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("EX_NO")) %>' />
            <br /><br />
          <b>  Flash Pt. Temp.:</b>
            <asp:Label ID="TRAN_FLASH_PT_TEMPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("FLASH_PT_TEMP")) %>' />                
            <br /><br />         
           <b> High Explosive Weight:</b>
            <asp:Label ID="HIGH_EXPLOSIVE_WTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("HIGH_EXPLOSIVE_WT")) %>' />
            <br /><br />
          <b>  Ltd Qty Ind:</b>
            <asp:Label ID="LTD_QTY_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LTD_QTY_IND")) %>' />
            <br /><br />
          <b>  Magnetic Ind:</b>
            <asp:Label ID="MAGNETIC_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("MAGNETIC_IND")) %>' />
            <br /><br />
           <b> Magnetism (Magnetic Strength):</b>
            <asp:Label ID="MAGNETISMLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("MAGNETISM")) %>' />
            <br /><br />
          <b>  Marine Pollutant Ind:</b>
            <asp:Label ID="MARINE_POLLUTANT_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("MARINE_POLLUTANT_IND")) %>' />
            <br /><br />
          <b>  Net Explosive Qty. Distance Weight:</b>
            <asp:Label ID="NET_EXP_QTY_DISTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NET_EXP_QTY_DIST")) %>' />
            <br /><br />
          <b>  Net Explosive Weight (kg):</b>
            <asp:Label ID="NET_EXP_WEIGHTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NET_EXP_WEIGHT")) %>' />
            <br /><br />
           <b> Net Propellant Weight (kg):</b>
            <asp:Label ID="NET_PROPELLANT_WTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NET_PROPELLANT_WT")) %>' />
            <br /><br />
          <b>  NOS Technical Shipping Name:</b>
            <asp:Label ID="NOS_TECHNICAL_SHIPPING_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NOS_TECHNICAL_SHIPPING_NAME")) %>' />
            <br /><br />
          <b>  Transportation Additional Data:</b>
            <asp:Label ID="TRANSPORTATION_ADDITIONAL_DATALabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("TRANSPORTATION_ADDITIONAL_DATA")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>DOT PSN Information:</b>
    <asp:FormView ID="FormViewDOT" runat="server"  SkinID="FormView"
        EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
        <table>
            <tr><td> DOT Hazard Class/Div:</td><td>
            <asp:TextBox ID="DOT_HAZARD_CLASS_DIVTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_HAZARD_CLASS_DIV")) %>' />
            </td></tr><tr><td>  
            DOT Hazard Label:</td><td>
            <asp:TextBox ID="DOT_HAZARD_LABELTextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("DOT_HAZARD_LABEL")) %>' />
            </td></tr><tr><td>  
            DOT Max. Quantity:  Cargo Aircraft Only:</td><td>
            <asp:TextBox ID="DOT_MAX_CARGOTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_MAX_CARGO")) %>' />
            </td></tr><tr><td>  
            DOT Max. Quantity: Passenger Aircraft/Rail:</td><td>
            <asp:TextBox ID="DOT_MAX_PASSENGERTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_MAX_PASSENGER")) %>' />
            </td></tr><tr><td>  
            DOT Packaging (Bulk):</td><td>
            <asp:TextBox ID="DOT_PACK_BULKTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_BULK")) %>' />
            </td></tr><tr><td>  
            DOT Packaging (Exceptions):</td><td>
            <asp:TextBox ID="DOT_PACK_EXCEPTIONSTextBox" runat="server"  MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_EXCEPTIONS")) %>' />
            </td></tr><tr><td>  
            DOT Packaging (Non Bulk):</td><td>
            <asp:TextBox ID="DOT_PACK_NONBULKTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_NONBULK")) %>' />
            </td></tr><tr><td>  
             DOT Packing Group:</td><td>
            <asp:TextBox ID="DOT_PACK_GROUPTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_GROUP")) %>' />
            </td></tr><tr><td>  
            DOT Proper Shipping Name:</td><td>
            <asp:TextBox ID="DOT_PROP_SHIP_NAMETextBox" runat="server"  MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PROP_SHIP_NAME")) %>' />
            </td></tr><tr><td>  
            DOT Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="DOT_PROP_SHIP_MODIFIERTextBox" runat="server"  MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PROP_SHIP_MODIFIER")) %>' />
            </td></tr><tr><td>  
            DOT PSN Code:</td><td>
            <asp:TextBox ID="DOT_PSN_CODETextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PSN_CODE")) %>' />
            </td></tr><tr><td>  
            DOT Special Provision:</td><td>
            <asp:TextBox ID="DOT_SPECIAL_PROVISIONTextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("DOT_SPECIAL_PROVISION")) %>' />
            </td></tr><tr><td>  
            DOT Symbols:</td><td>
            <asp:TextBox ID="DOT_SYMBOLSTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_SYMBOLS")) %>' />
            </td></tr><tr><td>  
            DOT UN ID Number:</td><td>
            <asp:TextBox ID="DOT_UN_ID_NUMBERTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_UN_ID_NUMBER")) %>' />
            </td></tr><tr><td>  
            DOT Water Shipment: Other Requirements:</td><td>
            <asp:TextBox ID="DOT_WATER_OTHER_REQTextBox" runat="server"  MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("DOT_WATER_OTHER_REQ")) %>' />
            </td></tr><tr><td>  
            DOT Water Shipment: Vessel Stowage:</td><td>
            <asp:TextBox ID="DOT_WATER_VESSEL_STOWTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_WATER_VESSEL_STOW")) %>' />
           </td></tr></table>  
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
           <table>
            <tr><td> DOT Hazard Class/Div:</td><td>
            <asp:TextBox ID="DOT_HAZARD_CLASS_DIVTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_HAZARD_CLASS_DIV")) %>' />
            </td></tr><tr><td>  
            DOT Hazard Label:</td><td>
            <asp:TextBox ID="DOT_HAZARD_LABELTextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("DOT_HAZARD_LABEL")) %>' />
            </td></tr><tr><td>  
            DOT Max. Quantity:  Cargo Aircraft Only:</td><td>
            <asp:TextBox ID="DOT_MAX_CARGOTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_MAX_CARGO")) %>' />
            </td></tr><tr><td>  
            DOT Max. Quantity: Passenger Aircraft/Rail:</td><td>
            <asp:TextBox ID="DOT_MAX_PASSENGERTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_MAX_PASSENGER")) %>' />
            </td></tr><tr><td>  
            DOT Packaging (Bulk):</td><td>
            <asp:TextBox ID="DOT_PACK_BULKTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_BULK")) %>' />
            </td></tr><tr><td>  
            DOT Packaging (Exceptions):</td><td>
            <asp:TextBox ID="DOT_PACK_EXCEPTIONSTextBox" runat="server"  MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_EXCEPTIONS")) %>' />
            </td></tr><tr><td>  
            DOT Packaging (Non Bulk):</td><td>
            <asp:TextBox ID="DOT_PACK_NONBULKTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_NONBULK")) %>' />
            </td></tr><tr><td>  
             DOT Packing Group:</td><td>
            <asp:TextBox ID="DOT_PACK_GROUPTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_GROUP")) %>' />
            </td></tr><tr><td>  
            DOT Proper Shipping Name:</td><td>
            <asp:TextBox ID="DOT_PROP_SHIP_NAMETextBox" runat="server"  MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PROP_SHIP_NAME")) %>' />
            </td></tr><tr><td>  
            DOT Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="DOT_PROP_SHIP_MODIFIERTextBox" runat="server"  MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PROP_SHIP_MODIFIER")) %>' />
            </td></tr><tr><td>  
            DOT PSN Code:</td><td>
            <asp:TextBox ID="DOT_PSN_CODETextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_PSN_CODE")) %>' />
            </td></tr><tr><td>  
            DOT Special Provision:</td><td>
            <asp:TextBox ID="DOT_SPECIAL_PROVISIONTextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("DOT_SPECIAL_PROVISION")) %>' />
            </td></tr><tr><td>  
            DOT Symbols:</td><td>
            <asp:TextBox ID="DOT_SYMBOLSTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_SYMBOLS")) %>' />
            </td></tr><tr><td>  
            DOT UN ID Number:</td><td>
            <asp:TextBox ID="DOT_UN_ID_NUMBERTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("DOT_UN_ID_NUMBER")) %>' />
            </td></tr><tr><td>  
            DOT Water Shipment: Other Requirements:</td><td>
            <asp:TextBox ID="DOT_WATER_OTHER_REQTextBox" runat="server"  MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("DOT_WATER_OTHER_REQ")) %>' />
            </td></tr><tr><td>  
            DOT Water Shipment: Vessel Stowage:</td><td>
            <asp:TextBox ID="DOT_WATER_VESSEL_STOWTextBox" runat="server"  MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("DOT_WATER_VESSEL_STOW")) %>' />
           </td></tr></table>  
           
            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
           <b>  DOT Hazard Class/Div:</b>
            <asp:Label ID="DOT_HAZARD_CLASS_DIVLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_HAZARD_CLASS_DIV")) %>' />
            <br /><br />
          <b>    DOT Hazard Label:</b>
            <asp:Label ID="DOT_HAZARD_LABELLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_HAZARD_LABEL")) %>' />
            <br /><br />
            <b>  DOT Max. Quantity:  Cargo Aircraft Only:</b>
            <asp:Label ID="DOT_MAX_CARGOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_MAX_CARGO")) %>' />
            <br /><br />
            <b>  DOT Max. Quantity: Passenger Aircraft/Rail:</b>
            <asp:Label ID="DOT_MAX_PASSENGERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_MAX_PASSENGER")) %>' />
            <br /><br />
           <b>   DOT Packaging (Bulk):</b>
            <asp:Label ID="DOT_PACK_BULKLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_BULK")) %>' />
            <br /><br />
            <b>  DOT Packaging (Exceptions):</b>
            <asp:Label ID="DOT_PACK_EXCEPTIONSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_EXCEPTIONS")) %>' />
            <br /><br />
            <b>  DOT Packaging (Non Bulk):</b>
            <asp:Label ID="DOT_PACK_NONBULKLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_NONBULK")) %>' />
            <br /><br />
           <b>   DOT Packing Group:</b>
            <asp:Label ID="DOT_PACK_GROUPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PACK_GROUP")) %>' />
            <br /><br />
           <b>   DOT Proper Shipping Name:</b>
            <asp:Label ID="DOT_PROP_SHIP_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PROP_SHIP_NAME")) %>' />
            <br /><br />
          <b>    DOT Proper Shipping Name Modifier:</b>
            <asp:Label ID="DOT_PROP_SHIP_MODIFIERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PROP_SHIP_MODIFIER")) %>' />
            <br /><br />
           <b>   DOT PSN Code:</b>
            <asp:Label ID="DOT_PSN_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_PSN_CODE")) %>' />
            <br /><br />
           <b>   DOT Special Provision:</b>
            <asp:Label ID="DOT_SPECIAL_PROVISIONLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_SPECIAL_PROVISION")) %>' />
            <br /><br />
           <b>   DOT Symbols:</b>
            <asp:Label ID="DOT_SYMBOLSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_SYMBOLS")) %>' />
            <br /><br />
           <b>   DOT UN ID Number:</b>
            <asp:Label ID="DOT_UN_ID_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_UN_ID_NUMBER")) %>' />
            <br /><br />
          <b>    DOT Water Shipment: Other Requirements:</b>
            <asp:Label ID="DOT_WATER_OTHER_REQLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_WATER_OTHER_REQ")) %>' />
            <br /><br />
           <b>   DOT Water Shipment: Vessel Stowage:</b>
            <asp:Label ID="DOT_WATER_VESSEL_STOWLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DOT_WATER_VESSEL_STOW")) %>' />
            <br /><br />
            

        </ItemTemplate>
    </asp:FormView>

    <b>AFJM PSN Information:</b>
    <asp:FormView ID="FormViewAFJM" runat="server" SkinID="FormViewAlt"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
          <table>
            <tr><td> AFJM Hazard Class/Div:</td><td>
            <asp:TextBox ID="AFJM_HAZARD_CLASSTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_HAZARD_CLASS")) %>' />
           </td></tr><tr><td>
            AFJM Packaging Paragraph:</td><td>
            <asp:TextBox ID="AFJM_PACK_PARAGRAPHTextBox" runat="server"   MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PACK_PARAGRAPH")) %>' />
            </td></tr><tr><td>
            AFJM Packing Group:</td><td>
            <asp:TextBox ID="AFJM_PACK_GROUPTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PACK_GROUP")) %>' />
           </td></tr><tr><td>
            AFJM Proper Shipping Name:</td><td>
            <asp:TextBox ID="AFJM_PROP_SHIP_NAMETextBox" runat="server"   MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PROP_SHIP_NAME")) %>' />
            </td></tr><tr><td>
            AFJM Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="AFJM_PROP_SHIP_MODIFIERTextBox" runat="server"   MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PROP_SHIP_MODIFIER")) %>' />
            </td></tr><tr><td>
            AFJM PSN Code:</td><td>
            <asp:TextBox ID="AFJM_PSN_CODETextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PSN_CODE")) %>' />
           </td></tr><tr><td>
            AFJM Special Provisions:</td><td>
            <asp:TextBox ID="AFJM_SPECIAL_PROVTextBox" runat="server"   MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SPECIAL_PROV")) %>' />
            </td></tr><tr><td>
            AFJM Subsidiary Risk:</td><td>
            <asp:TextBox ID="AFJM_SUBSIDIARY_RISKTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SUBSIDIARY_RISK")) %>' />
            </td></tr><tr><td>
            AFJM Symbols:</td><td>
            <asp:TextBox ID="AFJM_SYMBOLSTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SYMBOLS")) %>' />
            </td></tr><tr><td>
            AFJM UN ID Number:</td><td>
            <asp:TextBox ID="AFJM_UN_ID_NUMBERTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_UN_ID_NUMBER")) %>' />
           </td></tr></table>

            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
           <table>
            <tr><td> AFJM Hazard Class/Div:</td><td>
            <asp:TextBox ID="AFJM_HAZARD_CLASSTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_HAZARD_CLASS")) %>' />
           </td></tr><tr><td>
            AFJM Packaging Paragraph:</td><td>
            <asp:TextBox ID="AFJM_PACK_PARAGRAPHTextBox" runat="server"   MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PACK_PARAGRAPH")) %>' />
            </td></tr><tr><td>
            AFJM Packing Group:</td><td>
            <asp:TextBox ID="AFJM_PACK_GROUPTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PACK_GROUP")) %>' />
           </td></tr><tr><td>
            AFJM Proper Shipping Name:</td><td>
            <asp:TextBox ID="AFJM_PROP_SHIP_NAMETextBox" runat="server"   MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PROP_SHIP_NAME")) %>' />
            </td></tr><tr><td>
            AFJM Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="AFJM_PROP_SHIP_MODIFIERTextBox" runat="server"   MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PROP_SHIP_MODIFIER")) %>' />
            </td></tr><tr><td>
            AFJM PSN Code:</td><td>
            <asp:TextBox ID="AFJM_PSN_CODETextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PSN_CODE")) %>' />
           </td></tr><tr><td>
            AFJM Special Provisions:</td><td>
            <asp:TextBox ID="AFJM_SPECIAL_PROVTextBox" runat="server"   MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SPECIAL_PROV")) %>' />
            </td></tr><tr><td>
            AFJM Subsidiary Risk:</td><td>
            <asp:TextBox ID="AFJM_SUBSIDIARY_RISKTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SUBSIDIARY_RISK")) %>' />
            </td></tr><tr><td>
            AFJM Symbols:</td><td>
            <asp:TextBox ID="AFJM_SYMBOLSTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SYMBOLS")) %>' />
            </td></tr><tr><td>
            AFJM UN ID Number:</td><td>
            <asp:TextBox ID="AFJM_UN_ID_NUMBERTextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("AFJM_UN_ID_NUMBER")) %>' />
           </td></tr></table>


            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
          <b>  AFJM Hazard Class/Div:</b> 
            <asp:Label ID="AFJM_HAZARD_CLASSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_HAZARD_CLASS")) %>' />
            <br /><br />
          <b>  AFJM Packaging Paragraph:</b> 
            <asp:Label ID="AFJM_PACK_PARAGRAPHLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PACK_PARAGRAPH")) %>' />
            <br /><br />
           <b> AFJM Packing Group:</b> 
            <asp:Label ID="AFJM_PACK_GROUPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PACK_GROUP")) %>' />
            <br /><br />
          <b>  AFJM Proper Shipping Name:</b> 
            <asp:Label ID="AFJM_PROP_SHIP_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PROP_SHIP_NAME")) %>' />
            <br /><br />
          <b>  AFJM Proper Shipping Name Modifier:</b> 
            <asp:Label ID="AFJM_PROP_SHIP_MODIFIERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PROP_SHIP_MODIFIER")) %>' />
            <br /><br />
          <b>  AFJM PSN Code:</b> 
            <asp:Label ID="AFJM_PSN_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_PSN_CODE")) %>' />
            <br /><br />
          <b>  AFJM Special Provisions:</b> 
            <asp:Label ID="AFJM_SPECIAL_PROVLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SPECIAL_PROV")) %>' />
            <br /><br />
          <b>  AFJM Subsidiary Risk:</b> 
            <asp:Label ID="AFJM_SUBSIDIARY_RISKLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SUBSIDIARY_RISK")) %>' />
            <br /><br />
          <b>  AFJM Symbols:</b> 
            <asp:Label ID="AFJM_SYMBOLSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_SYMBOLS")) %>' />
            <br /><br />
          <b>  AFJM UN ID Number:</b> 
            <asp:Label ID="AFJM_UN_ID_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("AFJM_UN_ID_NUMBER")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>IATA PSN Information:</b>
    <asp:FormView ID="FormViewIATA" runat="server"  SkinID="FormView"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
        <table>
            <tr><td>IATA Cargo Packing: Note Cargo Aircraft Packing Instructions:</td><td>
            <asp:TextBox ID="IATA_CARGO_PACKINGTextBox" runat="server"     MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_CARGO_PACKING")) %>' />
            </td></tr><tr><td>
            IATA Hazard Class/Div:</td><td>
            <asp:TextBox ID="IATA_HAZARD_CLASSTextBox" runat="server"    MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IATA_HAZARD_CLASS")) %>' />
            </td></tr><tr><td>
            IATA Hazard Label:</td><td>
            <asp:TextBox ID="IATA_HAZARD_LABELTextBox" runat="server"    MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("IATA_HAZARD_LABEL")) %>' />
            </td></tr><tr><td>
            IATA Packing Group:</td><td>
            <asp:TextBox ID="IATA_PACK_GROUPTextBox" runat="server"    MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PACK_GROUP")) %>' />
           </td></tr><tr><td>
            IATA Passenger Air Packing: Lmt. Qty Pkg. Instr:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_PACK_LMT_INSTRTextBox" runat="server"    MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_LMT_INSTR")) %>' />
          </td></tr><tr><td>
            IATA Passenger Air Packing: Lmt. Qty. Max Qty. per Pkg:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_PACK_LMT_PER_PKGTextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_LMT_PER_PKG")) %>' />
           </td></tr><tr><td>
            IATA Passenger Air Packing: Note Passenger Air Packing Instr:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_PACK_NOTETextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_NOTE")) %>' />
           </td></tr><tr><td>
            IATA Proper Shipping Name:</td><td>
            <asp:TextBox ID="IATA_PROP_SHIP_NAMETextBox" runat="server"    MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PROP_SHIP_NAME")) %>' />
            </td></tr><tr><td>
            IATA Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="IATA_PROP_SHIP_MODIFIERTextBox" runat="server"    MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PROP_SHIP_MODIFIER")) %>' />
           </td></tr><tr><td>
            IATA Cargo Packing:  Max. Quantity:</td><td>
            <asp:TextBox ID="IATA_CARGO_PACK_MAX_QTYTextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_CARGO_PACK_MAX_QTY")) %>' />
           </td></tr><tr><td>
            IATA PSN Code:</td><td>
            <asp:TextBox ID="IATA_PSN_CODETextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PSN_CODE")) %>' />
            </td></tr><tr><td>
            IATA Passenger Air Packing: Max. Quantity:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_MAX_QTYTextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_MAX_QTY")) %>' />
           </td></tr><tr><td>
            IATA Special Provisions:</td><td>
            <asp:TextBox ID="IATA_SPECIAL_PROVTextBox" runat="server"    MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IATA_SPECIAL_PROV")) %>' />
            </td></tr><tr><td>
            IATA Subsidiary Risk:</td><td>
            <asp:TextBox ID="IATA_SUBSIDIARY_RISKTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("IATA_SUBSIDIARY_RISK")) %>' />
           </td></tr><tr><td>
            IATA UN ID Number:</td><td>
            <asp:TextBox ID="IATA_UN_ID_NUMBERTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("IATA_UN_ID_NUMBER")) %>' />
            </td></tr></table>

            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
            <table>
            <tr><td>IATA Cargo Packing: Note Cargo Aircraft Packing Instructions:</td><td>
            <asp:TextBox ID="IATA_CARGO_PACKINGTextBox" runat="server"     MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_CARGO_PACKING")) %>' />
            </td></tr><tr><td>
            IATA Hazard Class/Div:</td><td>
            <asp:TextBox ID="IATA_HAZARD_CLASSTextBox" runat="server"    MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IATA_HAZARD_CLASS")) %>' />
            </td></tr><tr><td>
            IATA Hazard Label:</td><td>
            <asp:TextBox ID="IATA_HAZARD_LABELTextBox" runat="server"    MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("IATA_HAZARD_LABEL")) %>' />
            </td></tr><tr><td>
            IATA Packing Group:</td><td>
            <asp:TextBox ID="IATA_PACK_GROUPTextBox" runat="server"    MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PACK_GROUP")) %>' />
           </td></tr><tr><td>
            IATA Passenger Air Packing: Lmt. Qty Pkg. Instr:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_PACK_LMT_INSTRTextBox" runat="server"    MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_LMT_INSTR")) %>' />
          </td></tr><tr><td>
            IATA Passenger Air Packing: Lmt. Qty. Max Qty. per Pkg:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_PACK_LMT_PER_PKGTextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_LMT_PER_PKG")) %>' />
           </td></tr><tr><td>
            IATA Passenger Air Packing: Note Passenger Air Packing Instr:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_PACK_NOTETextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_NOTE")) %>' />
           </td></tr><tr><td>
            IATA Proper Shipping Name:</td><td>
            <asp:TextBox ID="IATA_PROP_SHIP_NAMETextBox" runat="server"    MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PROP_SHIP_NAME")) %>' />
            </td></tr><tr><td>
            IATA Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="IATA_PROP_SHIP_MODIFIERTextBox" runat="server"    MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PROP_SHIP_MODIFIER")) %>' />
           </td></tr><tr><td>
            IATA Cargo Packing:  Max. Quantity:</td><td>
            <asp:TextBox ID="IATA_CARGO_PACK_MAX_QTYTextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_CARGO_PACK_MAX_QTY")) %>' />
           </td></tr><tr><td>
            IATA PSN Code:</td><td>
            <asp:TextBox ID="IATA_PSN_CODETextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PSN_CODE")) %>' />
            </td></tr><tr><td>
            IATA Passenger Air Packing: Max. Quantity:</td><td>
            <asp:TextBox ID="IATA_PASS_AIR_MAX_QTYTextBox" runat="server"    MaxLength="20"
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_MAX_QTY")) %>' />
           </td></tr><tr><td>
            IATA Special Provisions:</td><td>
            <asp:TextBox ID="IATA_SPECIAL_PROVTextBox" runat="server"    MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IATA_SPECIAL_PROV")) %>' />
            </td></tr><tr><td>
            IATA Subsidiary Risk:</td><td>
            <asp:TextBox ID="IATA_SUBSIDIARY_RISKTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("IATA_SUBSIDIARY_RISK")) %>' />
           </td></tr><tr><td>
            IATA UN ID Number:</td><td>
            <asp:TextBox ID="IATA_UN_ID_NUMBERTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("IATA_UN_ID_NUMBER")) %>' />
            </td></tr></table>

            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton  Visible="false"  ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <b>  IATA Cargo Packing: Note Cargo Aircraft Packing Instructions:</b>
            <asp:Label ID="IATA_CARGO_PACKINGLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_CARGO_PACKING")) %>' />
            <br /><br />
            <b> IATA Hazard Class/Div:</b>
            <asp:Label ID="IATA_HAZARD_CLASSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_HAZARD_CLASS")) %>' />
            <br /><br />
           <b>  IATA Hazard Label:</b>
            <asp:Label ID="IATA_HAZARD_LABELLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_HAZARD_LABEL")) %>' />
            <br /><br />
           <b>  IATA Packing Group:</b>
            <asp:Label ID="IATA_PACK_GROUPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PACK_GROUP")) %>' />
            <br /><br />
           <b>  IATA Passenger Air Packing: Lmt. Qty Pkg. Instr:</b>
            <asp:Label ID="IATA_PASS_AIR_PACK_LMT_INSTRLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_LMT_INSTR")) %>' />
            <br /><br />
          <b>   IATA Passenger Air Packing: Lmt. Qty. Max Qty. per Pkg:</b>
            <asp:Label ID="IATA_PASS_AIR_PACK_LMT_PER_PKGLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_LMT_PER_PKG")) %>' />
            <br /><br />
          <b>   IATA Passenger Air Packing: Note Passenger Air Packing Instr:</b>
            <asp:Label ID="IATA_PASS_AIR_PACK_NOTELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_PACK_NOTE")) %>' />
            <br /><br />
          <b>   IATA Proper Shipping Name:</b>
            <asp:Label ID="IATA_PROP_SHIP_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PROP_SHIP_NAME")) %>' />
            <br /><br />
           <b>  IATA Proper Shipping Name Modifier:</b>
            <asp:Label ID="IATA_PROP_SHIP_MODIFIERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PROP_SHIP_MODIFIER")) %>' />
            <br /><br />
           <b>  IATA Cargo Packing:  Max. Quantity:</b>
            <asp:Label ID="IATA_CARGO_PACK_MAX_QTYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_CARGO_PACK_MAX_QTY")) %>' />
            <br /><br />
           <b>  IATA PSN Code:</b>
            <asp:Label ID="IATA_PSN_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PSN_CODE")) %>' />
            <br /><br />
          <b>   IATA Passenger Air Packing: Max. Quantity:</b>
            <asp:Label ID="IATA_PASS_AIR_MAX_QTYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_PASS_AIR_MAX_QTY")) %>' />
            <br /><br />
           <b>  IATA Special Provisions:</b>
            <asp:Label ID="IATA_SPECIAL_PROVLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_SPECIAL_PROV")) %>' />
            <br /><br />
           <b>  IATA Subsidiary Risk:</b>
            <asp:Label ID="IATA_SUBSIDIARY_RISKLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_SUBSIDIARY_RISK")) %>' />
            <br /><br />
           <b>  IATA UN ID Number:</b>
            <asp:Label ID="IATA_UN_ID_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IATA_UN_ID_NUMBER")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>IMO PSN Information:</b>
    <asp:FormView ID="FormViewIMO" runat="server" SkinID="FormViewAlt"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
         <table>
            <tr><td> IMO EMS Number:</td><td>
            <asp:TextBox ID="IMO_EMS_NOTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_EMS_NO")) %>' />
          </td></tr><tr><td>  
            IMO Hazard Class/Div:</td><td>
            <asp:TextBox ID="IMO_HAZARD_CLASSTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_HAZARD_CLASS")) %>' />
         </td></tr><tr><td>  
            IMO IBC Instructions:</td><td>
            <asp:TextBox ID="IMO_IBC_INSTRTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_IBC_INSTR")) %>' />
         </td></tr><tr><td>  
            IMO IBC Provisions:</td><td>
            <asp:TextBox ID="IMO_IBC_PROVISIONSTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_IBC_PROVISIONS")) %>' />
        </td></tr><tr><td>  
            IMO Limited Quantity:</td><td>
            <asp:TextBox ID="IMO_LIMITED_QTYTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_LIMITED_QTY")) %>' />
         </td></tr><tr><td>  
            IMO Packing Group:</td><td>
            <asp:TextBox ID="IMO_PACK_GROUPTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_GROUP")) %>' />
          </td></tr><tr><td>  
            IMO Packing Instructions:</td><td>
            <asp:TextBox ID="IMO_PACK_INSTRUCTIONSTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_INSTRUCTIONS")) %>' />
         </td></tr><tr><td>  
            IMO Packing Provisions:</td><td>
            <asp:TextBox ID="IMO_PACK_PROVISIONSTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_PROVISIONS")) %>' />
          </td></tr><tr><td>  
            IMO Proper Shipping Name:</td><td>
            <asp:TextBox ID="IMO_PROP_SHIP_NAMETextBox" runat="server"     MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PROP_SHIP_NAME")) %>' />
         </td></tr><tr><td>  
            IMO Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="IMO_PROP_SHIP_MODIFIERTextBox" runat="server"     MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PROP_SHIP_MODIFIER")) %>' />
           </td></tr><tr><td>  
            IMO PSN Code:</td><td>
            <asp:TextBox ID="IMO_PSN_CODETextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PSN_CODE")) %>' />
           </td></tr><tr><td>  
            IMO Special Provisions:</td><td>
            <asp:TextBox ID="IMO_SPECIAL_PROVTextBox" runat="server"     MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("IMO_SPECIAL_PROV")) %>' />
           </td></tr><tr><td>  
            IMO Stowage and Segregation:</td><td>
            <asp:TextBox ID="IMO_STOW_SEGRTextBox" runat="server"     MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("IMO_STOW_SEGR")) %>' />
           </td></tr><tr><td>  
            IMO Subsidiary Risk Label:</td><td>
            <asp:TextBox ID="IMO_SUBSIDIARY_RISKTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_SUBSIDIARY_RISK")) %>' />
          </td></tr><tr><td>  
            IMO Tank Instructions, IMO:</td><td>
            <asp:TextBox ID="IMO_TANK_INSTR_IMOTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_IMO")) %>' />
           </td></tr><tr><td>  
            IMO Tank Instructions, Provisions:</td><td>
            <asp:TextBox ID="IMO_TANK_INSTR_PROVTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_PROV")) %>' />
           </td></tr><tr><td>  
            IMO Tank Instructions, UN:</td><td>
            <asp:TextBox ID="IMO_TANK_INSTR_UNTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_UN")) %>' />
            </td></tr><tr><td>  
            IMO UN Number:</td><td>
            <asp:TextBox ID="IMO_UN_NUMBERTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_UN_NUMBER")) %>' />
            </td></tr></table>  
          
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
           <table>
            <tr><td> IMO EMS Number:</td><td>
            <asp:TextBox ID="IMO_EMS_NOTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_EMS_NO")) %>' />
          </td></tr><tr><td>  
            IMO Hazard Class/Div:</td><td>
            <asp:TextBox ID="IMO_HAZARD_CLASSTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_HAZARD_CLASS")) %>' />
         </td></tr><tr><td>  
            IMO IBC Instructions:</td><td>
            <asp:TextBox ID="IMO_IBC_INSTRTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_IBC_INSTR")) %>' />
         </td></tr><tr><td>  
            IMO IBC Provisions:</td><td>
            <asp:TextBox ID="IMO_IBC_PROVISIONSTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_IBC_PROVISIONS")) %>' />
        </td></tr><tr><td>  
            IMO Limited Quantity:</td><td>
            <asp:TextBox ID="IMO_LIMITED_QTYTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_LIMITED_QTY")) %>' />
         </td></tr><tr><td>  
            IMO Packing Group:</td><td>
            <asp:TextBox ID="IMO_PACK_GROUPTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_GROUP")) %>' />
          </td></tr><tr><td>  
            IMO Packing Instructions:</td><td>
            <asp:TextBox ID="IMO_PACK_INSTRUCTIONSTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_INSTRUCTIONS")) %>' />
         </td></tr><tr><td>  
            IMO Packing Provisions:</td><td>
            <asp:TextBox ID="IMO_PACK_PROVISIONSTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_PROVISIONS")) %>' />
          </td></tr><tr><td>  
            IMO Proper Shipping Name:</td><td>
            <asp:TextBox ID="IMO_PROP_SHIP_NAMETextBox" runat="server"     MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PROP_SHIP_NAME")) %>' />
         </td></tr><tr><td>  
            IMO Proper Shipping Name Modifier:</td><td>
            <asp:TextBox ID="IMO_PROP_SHIP_MODIFIERTextBox" runat="server"     MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PROP_SHIP_MODIFIER")) %>' />
           </td></tr><tr><td>  
            IMO PSN Code:</td><td>
            <asp:TextBox ID="IMO_PSN_CODETextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_PSN_CODE")) %>' />
           </td></tr><tr><td>  
            IMO Special Provisions:</td><td>
            <asp:TextBox ID="IMO_SPECIAL_PROVTextBox" runat="server"     MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("IMO_SPECIAL_PROV")) %>' />
           </td></tr><tr><td>  
            IMO Stowage and Segregation:</td><td>
            <asp:TextBox ID="IMO_STOW_SEGRTextBox" runat="server"     MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("IMO_STOW_SEGR")) %>' />
           </td></tr><tr><td>  
            IMO Subsidiary Risk Label:</td><td>
            <asp:TextBox ID="IMO_SUBSIDIARY_RISKTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_SUBSIDIARY_RISK")) %>' />
          </td></tr><tr><td>  
            IMO Tank Instructions, IMO:</td><td>
            <asp:TextBox ID="IMO_TANK_INSTR_IMOTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_IMO")) %>' />
           </td></tr><tr><td>  
            IMO Tank Instructions, Provisions:</td><td>
            <asp:TextBox ID="IMO_TANK_INSTR_PROVTextBox" runat="server"     MaxLength="30"
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_PROV")) %>' />
           </td></tr><tr><td>  
            IMO Tank Instructions, UN:</td><td>
            <asp:TextBox ID="IMO_TANK_INSTR_UNTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_UN")) %>' />
            </td></tr><tr><td>  
            IMO UN Number:</td><td>
            <asp:TextBox ID="IMO_UN_NUMBERTextBox" runat="server"     MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("IMO_UN_NUMBER")) %>' />
            </td></tr></table>  
            
            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
            <b>  IMO EMS Number:</b>
            <asp:Label ID="IMO_EMS_NOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_EMS_NO")) %>' />
            <br /><br />
            <b>  IMO Hazard Class/Div:</b>
            <asp:Label ID="IMO_HAZARD_CLASSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_HAZARD_CLASS")) %>' />
            <br /><br />
            <b>  IMO IBC Instructions:</b>
            <asp:Label ID="IMO_IBC_INSTRLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_IBC_INSTR")) %>' />
            <br /><br />
            <b>  IMO IBC Provisions:</b>
            <asp:Label ID="IMO_IBC_PROVISIONSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_IBC_PROVISIONS")) %>' />
            <br /><br />
            <b>  IMO Limited Quantity:</b>
            <asp:Label ID="IMO_LIMITED_QTYLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_LIMITED_QTY")) %>' />
            <br /><br />
            <b>  IMO Packing Group:</b>
            <asp:Label ID="IMO_PACK_GROUPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_GROUP")) %>' />
            <br /><br />
           <b>   IMO Packing Instructions:</b>
            <asp:Label ID="IMO_PACK_INSTRUCTIONSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_INSTRUCTIONS")) %>' />
            <br /><br />
           <b>   IMO Packing Provisions:</b>
            <asp:Label ID="IMO_PACK_PROVISIONSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_PACK_PROVISIONS")) %>' />
            <br /><br />
            <b>  IMO Proper Shipping Name:</b>
            <asp:Label ID="IMO_PROP_SHIP_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_PROP_SHIP_NAME")) %>' />
            <br /><br />
           <b>   IMO Proper Shipping Name Modifier:</b>
            <asp:Label ID="IMO_PROP_SHIP_MODIFIERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_PROP_SHIP_MODIFIER")) %>' />
            <br /><br />
            <b>  IMO PSN Code:</b>
            <asp:Label ID="IMO_PSN_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_PSN_CODE")) %>' />
            <br /><br />
            <b>  IMO Special Provisions:</b>
            <asp:Label ID="IMO_SPECIAL_PROVLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_SPECIAL_PROV")) %>' />
            <br /><br />
            <b>  IMO Stowage and Segregation:</b>
            <asp:Label ID="IMO_STOW_SEGRLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_STOW_SEGR")) %>' />
            <br /><br />
           <b>   IMO Subsidiary Risk Label:</b>
            <asp:Label ID="IMO_SUBSIDIARY_RISKLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_SUBSIDIARY_RISK")) %>' />
            <br /><br />
           <b>   IMO Tank Instructions, IMO:</b>
            <asp:Label ID="IMO_TANK_INSTR_IMOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_IMO")) %>' />
            <br /><br />
           <b>   IMO Tank Instructions, Provisions:</b>
            <asp:Label ID="IMO_TANK_INSTR_PROVLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_PROV")) %>' />
            <br /><br />
            <b>  IMO Tank Instructions, UN:</b>
            <asp:Label ID="IMO_TANK_INSTR_UNLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_TANK_INSTR_UN")) %>' />
            <br /><br />
            <b>  IMO UN Number:</b>
            <asp:Label ID="IMO_UN_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("IMO_UN_NUMBER")) %>' />
            <br /><br />
            

        </ItemTemplate>
    </asp:FormView>

    <b>Logistical Information:</b>
    <asp:FormView ID="FormViewItemDescription" runat="server"  SkinID="FormView"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
         <EditItemTemplate>
          <table>
            <tr><td>Item Manager:</td><td>
             <asp:TextBox ID="ITEM_MANAGERTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("ITEM_MANAGER")) %>'   MaxLength="10" />
             
             </td></tr><tr><td>Item Name:</td><td>
             <asp:TextBox ID="ITEM_NAMETextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("ITEM_NAME")) %>'   MaxLength="100" />
             
             </td></tr><tr><td>Specification #:</td><td>
             <asp:TextBox ID="SPECIFICATION_NUMBERTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("SPECIFICATION_NUMBER")) %>'   MaxLength="20" />
             
             </td></tr><tr><td>Specification Type /Grade/Class:</td><td>
             <asp:TextBox ID="TYPE_GRADE_CLASSTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("TYPE_GRADE_CLASS")) %>'   MaxLength="20" />
             
             </td></tr><tr><td>Unit Of Issue:</td><td>
             <asp:TextBox ID="UNIT_OF_ISSUETextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("UNIT_OF_ISSUE")) %>'   MaxLength="10" />
             
             </td></tr><tr><td>Quantitative Expression Exp:</td><td>
             <asp:TextBox ID="QUANTITATIVE_EXPRESSIONTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("QUANTITATIVE_EXPRESSION")) %>'   MaxLength="15" />
             
             </td></tr><tr><td>Unit of Issue Container Quantity:</td><td>
             <asp:TextBox ID="UI_CONTAINER_QTYTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("UI_CONTAINER_QTY")) %>'   MaxLength="15" />
             
             </td></tr><tr><td>Type Of Container:</td><td>
             <asp:TextBox ID="TYPE_OF_CONTAINERTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("TYPE_OF_CONTAINER")) %>'   MaxLength="15" />
            

             </td></tr><tr><td>Batch Number:</td><td>
            <asp:TextBox ID="BATCH_NUMBERTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("BATCH_NUMBER")) %>' />
            
            </td></tr><tr><td>Lot Number:</td><td>
            <asp:TextBox ID="LOT_NUMBERTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("LOT_NUMBER")) %>' />
            
            </td></tr><tr><td>Logistics FLIS NIIN Verified:</td><td>
            <asp:TextBox ID="LOG_FLIS_NIIN_VERTextBox" runat="server"    MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("LOG_FLIS_NIIN_VER")) %>' />
            
            </td></tr><tr><td>Logistics FSC:</td><td>
            <asp:TextBox ID="LOG_FSCTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("LOG_FSC")) %>'    MaxLength="18" />
           <asp:RangeValidator ControlToValidate="LOG_FSCTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="valRng_txt_qty" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     

          </td></tr><tr><td>  Net Unit Weight:</td><td>
            <asp:TextBox ID="NET_UNIT_WEIGHTTextBox" runat="server"  MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("NET_UNIT_WEIGHT")) %>' />
           
          </td></tr><tr><td>  Shelf Life Code:</td><td>
            <asp:TextBox ID="SHELF_LIFE_CODETextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("SHELF_LIFE_CODE")) %>' />          
         
           
         </td></tr><tr><td>   Special Emphasis Code:</td><td>
            <asp:TextBox ID="SPECIAL_EMP_CODETextBox" runat="server"  MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("SPECIAL_EMP_CODE")) %>' />
           
         </td></tr><tr><td>   UN/NA Number:</td><td>
            <asp:TextBox ID="UN_NA_NUMBERTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("UN_NA_NUMBER")) %>' />
            
          </td></tr><tr><td>  UPC/GTIN:</td><td>
            <asp:TextBox ID="UPC_GTINTextBox" runat="server"  MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("UPC_GTIN")) %>' />


             </td></tr>
                </table>
             <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                 CommandName="Update" Text="Update" />
             &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                 CausesValidation="False" CommandName="Cancel" Text="Cancel" />
         </EditItemTemplate>
         <InsertItemTemplate>
         
             <table>
            <tr><td>Item Manager:</td><td>
             <asp:TextBox ID="ITEM_MANAGERTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("ITEM_MANAGER")) %>'   MaxLength="10" />
             
             </td></tr><tr><td>Item Name:</td><td>
             <asp:TextBox ID="ITEM_NAMETextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("ITEM_NAME")) %>'   MaxLength="100" />
             
             </td></tr><tr><td>Specification #:</td><td>
             <asp:TextBox ID="SPECIFICATION_NUMBERTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("SPECIFICATION_NUMBER")) %>'   MaxLength="20" />
             
             </td></tr><tr><td>Specification Type /Grade/Class:</td><td>
             <asp:TextBox ID="TYPE_GRADE_CLASSTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("TYPE_GRADE_CLASS")) %>'   MaxLength="20" />
             
             </td></tr><tr><td>Unit Of Issue:</td><td>
             <asp:TextBox ID="UNIT_OF_ISSUETextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("UNIT_OF_ISSUE")) %>'   MaxLength="10" />
             
             </td></tr><tr><td>Quantitative Expression Exp:</td><td>
             <asp:TextBox ID="QUANTITATIVE_EXPRESSIONTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("QUANTITATIVE_EXPRESSION")) %>'   MaxLength="15" />
             
             </td></tr><tr><td>Unit of Issue Container Quantity:</td><td>
             <asp:TextBox ID="UI_CONTAINER_QTYTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("UI_CONTAINER_QTY")) %>'   MaxLength="15" />
             
             </td></tr><tr><td>Type Of Container:</td><td>
             <asp:TextBox ID="TYPE_OF_CONTAINERTextBox" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("TYPE_OF_CONTAINER")) %>'   MaxLength="15" />
            

             </td></tr><tr><td>Batch Number:</td><td>
            <asp:TextBox ID="BATCH_NUMBERTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("BATCH_NUMBER")) %>' />
            
            </td></tr><tr><td>Lot Number:</td><td>
            <asp:TextBox ID="LOT_NUMBERTextBox" runat="server"    MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("LOT_NUMBER")) %>' />
            
            </td></tr><tr><td>Logistics FLIS NIIN Verified:</td><td>
            <asp:TextBox ID="LOG_FLIS_NIIN_VERTextBox" runat="server"    MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("LOG_FLIS_NIIN_VER")) %>' />
            
            </td></tr><tr><td>Logistics FSC:</td><td>
            <asp:TextBox ID="LOG_FSCTextBox" runat="server" Text='<%# Server.HtmlEncode(StrEval("LOG_FSC")) %>'    MaxLength="18" />
           <asp:RangeValidator ControlToValidate="LOG_FSCTextBox" MinimumValue="0" MaximumValue="32767" Type="Integer" ID="valRng_txt_qty" runat="server" ErrorMessage="Please enter digits only." ForeColor="#990000" />
     

          </td></tr><tr><td>  Net Unit Weight:</td><td>
            <asp:TextBox ID="NET_UNIT_WEIGHTTextBox" runat="server"  MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("NET_UNIT_WEIGHT")) %>' />
           
          </td></tr><tr><td>  Shelf Life Code:</td><td>
            <asp:TextBox ID="SHELF_LIFE_CODETextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("SHELF_LIFE_CODE")) %>' />          
         
           
         </td></tr><tr><td>   Special Emphasis Code:</td><td>
            <asp:TextBox ID="SPECIAL_EMP_CODETextBox" runat="server"  MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("SPECIAL_EMP_CODE")) %>' />
           
         </td></tr><tr><td>   UN/NA Number:</td><td>
            <asp:TextBox ID="UN_NA_NUMBERTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("UN_NA_NUMBER")) %>' />
            
          </td></tr><tr><td>  UPC/GTIN:</td><td>
            <asp:TextBox ID="UPC_GTINTextBox" runat="server"  MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("UPC_GTIN")) %>' />


             </td></tr>
                </table><asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                 CommandName="Insert" Text="Insert" />
             &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                 CausesValidation="False" CommandName="Cancel" Text="Cancel" />
         </InsertItemTemplate>
         <ItemTemplate>
             <b>Item Manager:</b>
             <asp:Label ID="ITEM_MANAGERLabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("ITEM_MANAGER")) %>' />
             <br /><br />
            <b> Item Name:</b>
             <asp:Label ID="ITEM_NAMELabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("ITEM_NAME")) %>' />
             <br /><br />
             <b>Specification #:</b>
             <asp:Label ID="SPECIFICATION_NUMBERLabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("SPECIFICATION_NUMBER")) %>' />
             <br /><br />
             <b>Specification Type /Grade/Class:</b>
             <asp:Label ID="TYPE_GRADE_CLASSLabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("TYPE_GRADE_CLASS")) %>' />
             <br /><br />
             <b>Unit Of Issue:</b>
             <asp:Label ID="UNIT_OF_ISSUELabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("UNIT_OF_ISSUE")) %>' />
             <br /><br />
             <b>Quantitative Expression Exp:</b>
             <asp:Label ID="QUANTITATIVE_EXPRESSIONLabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("QUANTITATIVE_EXPRESSION")) %>' />
             <br /><br />
            <b> Unit Of Issue Container Qty:</b>
             <asp:Label ID="UI_CONTAINER_QTYLabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("UI_CONTAINER_QTY")) %>' />
             <br /><br />
             <b>Type Of Container:</b>
             <asp:Label ID="TYPE_OF_CONTAINERLabel" runat="server" 
                 Text='<%# Server.HtmlEncode(StrEval("TYPE_OF_CONTAINER")) %>' />
             <br /><br />
              <b>Batch Number:</b>
            <asp:Label ID="BATCH_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("BATCH_NUMBER")) %>' />
            <br /><br />
           <b> Lot Number:</b>
            <asp:Label ID="LOT_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LOT_NUMBER")) %>' />
            <br /><br />
           <b> Logistics FLIS NIIN Verified:</b>
            <asp:Label ID="LOG_FLIS_NIIN_VERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LOG_FLIS_NIIN_VER")) %>' />
            <br /><br />
           <b> Logistics FSC:</b>
            <asp:Label ID="LOG_FSCLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("LOG_FSC")) %>' />
            <br /><br />
           <b> Net Unit Weight:</b>
            <asp:Label ID="NET_UNIT_WEIGHTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("NET_UNIT_WEIGHT")) %>' />
            <br /><br />
           <b> Shelf Life Code:</b>
            <asp:Label ID="SHELF_LIFE_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SHELF_LIFE_CODE")) %>' />
            <br /><br />
          
          <b>  Special Emphasis Code:</b>
            <asp:Label ID="SPECIAL_EMP_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SPECIAL_EMP_CODE")) %>' />
            <br /><br />
           <b> UN/NA Number:</b>
            <asp:Label ID="UN_NA_NUMBERLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("UN_NA_NUMBER")) %>' />
            <br /><br />
           <b> UPC/GTIN:</b>
            <asp:Label ID="UPC_GTINLabel" runat="server" Text='<%# Server.HtmlEncode(StrEval("UPC_GTIN")) %>' />
            <br /><br />
         </ItemTemplate>
        </asp:FormView>

    <b>Label Information:</b>
    <asp:FormView ID="FormViewLabelInfo" runat="server" SkinID="FormViewAlt"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
        <table>
            <tr><td> Company CAGE (Responsible Party):</td><td>
            <asp:TextBox ID="COMPANY_CAGE_RPTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("COMPANY_CAGE_RP")) %>' />
        </td></tr><tr><td>
            Company Name (Responsible Party):</td><td>
            <asp:TextBox ID="COMPANY_NAME_RPTextBox" runat="server"  MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("COMPANY_NAME_RP")) %>' />
        </td></tr><tr><td>
            Label Emergency Telephone Number#:</td><td>
            <asp:TextBox ID="LABEL_EMERG_PHONETextBox" runat="server"  MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_EMERG_PHONE")) %>' />
         </td></tr><tr><td>
            Label Item Name:</td><td>
            <asp:TextBox ID="LABEL_ITEM_NAMETextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_ITEM_NAME")) %>' />
          </td></tr><tr><td>
            Label Procurement Year:</td><td>
            <asp:TextBox ID="LABEL_PROC_YEARTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROC_YEAR")) %>' />
           </td></tr><tr><td>
            Label Product Identity:</td><td>
            <asp:TextBox ID="LABEL_PROD_IDENTTextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROD_IDENT")) %>' />
          </td></tr><tr><td>
            Label Product Serial Number:</td><td>
            <asp:TextBox ID="LABEL_PROD_SERIALNOTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROD_SERIALNO")) %>' />
          </td></tr><tr><td>
            Label Signal Word:</td><td>
            <asp:TextBox ID="LABEL_SIGNAL_WORDTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_SIGNAL_WORD")) %>' />
           </td></tr><tr><td>
            Label Stock Number:</td><td>
            <asp:TextBox ID="LABEL_STOCK_NOTextBox" runat="server"  MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_STOCK_NO")) %>' />
            </td></tr><tr><td>
            Specific Hazards:</td><td>
            <asp:TextBox ID="SPECIFIC_HAZARDSTextBox" runat="server"  
                Text='<%# Server.HtmlEncode(StrEval("SPECIFIC_HAZARDS")) %>' />
           </td></tr></table>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
           <table>
            <tr><td> Company CAGE (Responsible Party):</td><td>
            <asp:TextBox ID="COMPANY_CAGE_RPTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("COMPANY_CAGE_RP")) %>' />
        </td></tr><tr><td>
            Company Name (Responsible Party):</td><td>
            <asp:TextBox ID="COMPANY_NAME_RPTextBox" runat="server"  MaxLength="255"
                Text='<%# Server.HtmlEncode(StrEval("COMPANY_NAME_RP")) %>' />
        </td></tr><tr><td>
            Label Emergency Telephone Number#:</td><td>
            <asp:TextBox ID="LABEL_EMERG_PHONETextBox" runat="server"  MaxLength="50"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_EMERG_PHONE")) %>' />
         </td></tr><tr><td>
            Label Item Name:</td><td>
            <asp:TextBox ID="LABEL_ITEM_NAMETextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_ITEM_NAME")) %>' />
          </td></tr><tr><td>
            Label Procurement Year:</td><td>
            <asp:TextBox ID="LABEL_PROC_YEARTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROC_YEAR")) %>' />
           </td></tr><tr><td>
            Label Product Identity:</td><td>
            <asp:TextBox ID="LABEL_PROD_IDENTTextBox" runat="server"  MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROD_IDENT")) %>' />
          </td></tr><tr><td>
            Label Product Serial Number:</td><td>
            <asp:TextBox ID="LABEL_PROD_SERIALNOTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROD_SERIALNO")) %>' />
          </td></tr><tr><td>
            Label Signal Word:</td><td>
            <asp:TextBox ID="LABEL_SIGNAL_WORDTextBox" runat="server"  MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_SIGNAL_WORD")) %>' />
           </td></tr><tr><td>
            Label Stock Number:</td><td>
            <asp:TextBox ID="LABEL_STOCK_NOTextBox" runat="server"  MaxLength="15"
                Text='<%# Server.HtmlEncode(StrEval("LABEL_STOCK_NO")) %>' />
            </td></tr><tr><td>
            Specific Hazards:</td><td>
            <asp:TextBox ID="SPECIFIC_HAZARDSTextBox" runat="server"  
                Text='<%# Server.HtmlEncode(StrEval("SPECIFIC_HAZARDS")) %>' />
           </td></tr></table>


            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
           <b>  Company CAGE (Responsible Party):</b>
            <asp:Label ID="COMPANY_CAGE_RPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("COMPANY_CAGE_RP")) %>' />
            <br /><br />
           <b>  Company Name (Responsible Party):</b>
            <asp:Label ID="COMPANY_NAME_RPLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("COMPANY_NAME_RP")) %>' />
            <br /><br />
          <b>   Label Emergency Telephone Number#:</b>
            <asp:Label ID="LABEL_EMERG_PHONELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_EMERG_PHONE")) %>' />
            <br /><br />
          <b>   Label Item Name:</b>
            <asp:Label ID="LABEL_ITEM_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_ITEM_NAME")) %>' />
            <br /><br />
          <b>   Label Procurement Year:</b>
            <asp:Label ID="LABEL_PROC_YEARLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROC_YEAR")) %>' />
            <br /><br />
           <b>  Label Product Identity:</b>
            <asp:Label ID="LABEL_PROD_IDENTLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROD_IDENT")) %>' />
            <br /><br />
          <b>   Label Product Serial Number:</b>
            <asp:Label ID="LABEL_PROD_SERIALNOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_PROD_SERIALNO")) %>' />
            <br /><br />
          <b>   Label Signal Word:</b>
            <asp:Label ID="LABEL_SIGNAL_WORDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_SIGNAL_WORD")) %>' />
            <br /><br />
          <b>   Label Stock Number:</b>
            <asp:Label ID="LABEL_STOCK_NOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("LABEL_STOCK_NO")) %>' />
            <br /><br />
          <b>   Specific Hazards:</b>
            <asp:Label ID="SPECIFIC_HAZARDSLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("SPECIFIC_HAZARDS")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>Disposal Information:</b>
    <asp:FormView ID="FormViewDisposal" runat="server" SkinID="FormView"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
         <table>
            <tr><td>    Disposal Additional Information:</td><td>
            <asp:TextBox ID="DISPOSAL_ADD_INFOTextBox" runat="server"  
                Text='<%# Server.HtmlEncode(StrEval("DISPOSAL_ADD_INFO")) %>' />
           </td></tr><tr><td>
            EPA Hazardous Waste Code:</td><td>
            <asp:TextBox ID="EPA_HAZ_WASTE_CODETextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_CODE")) %>' />
          </td></tr><tr><td>
            EPA Hazardous Waste Ind:</td><td>
            <asp:TextBox ID="EPA_HAZ_WASTE_INDTextBox" runat="server"   MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_IND")) %>' />
           </td></tr><tr><td>
            EPA Hazardous Waste Name:</td><td>
            <asp:TextBox ID="EPA_HAZ_WASTE_NAMETextBox" runat="server"   MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_NAME")) %>' />
           </td></tr></table>

            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
          <table>
            <tr><td>    Disposal Additional Information:</td><td>
            <asp:TextBox ID="DISPOSAL_ADD_INFOTextBox" runat="server"  
                Text='<%# Server.HtmlEncode(StrEval("DISPOSAL_ADD_INFO")) %>' />
           </td></tr><tr><td>
            EPA Hazardous Waste Code:</td><td>
            <asp:TextBox ID="EPA_HAZ_WASTE_CODETextBox" runat="server"   MaxLength="10"
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_CODE")) %>' />
          </td></tr><tr><td>
            EPA Hazardous Waste Ind:</td><td>
            <asp:TextBox ID="EPA_HAZ_WASTE_INDTextBox" runat="server"   MaxLength="1"
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_IND")) %>' />
           </td></tr><tr><td>
            EPA Hazardous Waste Name:</td><td>
            <asp:TextBox ID="EPA_HAZ_WASTE_NAMETextBox" runat="server"   MaxLength="100"
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_NAME")) %>' />
           </td></tr></table>


            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton  Visible="false"  ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
           <b>  Disposal Additional Information:</b>
            <asp:Label ID="DISPOSAL_ADD_INFOLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("DISPOSAL_ADD_INFO")) %>' />
            <br /><br />
           <b>   EPA Hazardous Waste Code:</b>
            <asp:Label ID="EPA_HAZ_WASTE_CODELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_CODE")) %>' />
            <br /><br />
           <b>   EPA Hazardous Waste Ind:</b>
            <asp:Label ID="EPA_HAZ_WASTE_INDLabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_IND")) %>' />
            <br /><br />
           <b>   EPA Hazardous Waste Name:</b>
            <asp:Label ID="EPA_HAZ_WASTE_NAMELabel" runat="server" 
                Text='<%# Server.HtmlEncode(StrEval("EPA_HAZ_WASTE_NAME")) %>' />
            <br /><br />

        </ItemTemplate>
    </asp:FormView>

    <b>Document Types:</b>
    <asp:FormView ID="FormViewDocTypes" runat="server" SkinID="FormViewAlt"
            EnableModelValidation="True" Width="100%" DefaultMode="Edit"
            EmptyDataText="No data to display">
        <EditItemTemplate>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Update" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </EditItemTemplate>
        <InsertItemTemplate>
           
            <asp:LinkButton Visible="false" ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Insert" />
            &nbsp;<asp:LinkButton Visible="false" ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancel" />
        </InsertItemTemplate>
        <ItemTemplate>
        </ItemTemplate>
    </asp:FormView>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image2" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_fileUpload" runat="server" Text="MATERIAL SAFETY DATA SHEET Upload:"></asp:Label> <asp:FileUpload ID="fileUploadMSDS" runat="server" />
     
    <asp:Button ID="btn_viewFile" runat="server" Text="View MATERIAL SAFETY DATA SHEET" 
    onclick="btn_viewFile_Click" />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">The MSDS document may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image3" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_translated" runat="server" Text="MSDS, TRANSLATED Upload:"></asp:Label> <asp:FileUpload ID="fileUploadTranslated" runat="server" />
     
    <asp:Button ID="btn_viewTranslated" runat="server" Text="View MSDS, TRANSLATED" onclick="btn_viewTranslated_Click" 
     />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">The MSDS, TRANSLATED may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image4" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_neshap" runat="server" Text="NESHAP COMPLIANCE CERTIFICATE Upload:"></asp:Label> <asp:FileUpload ID="fileUploadNeshap" runat="server" />
     
    <asp:Button ID="btn_viewNeshap" runat="server" Text="View NESHAP COMPLIANCE CERTIFICATE" onclick="btn_viewNeshap_Click" 
     />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">The NESHAP COMPLIANCE CERTIFICATE may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image5" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_other" runat="server" Text="OTHER DOCUMENTS Upload:"></asp:Label> <asp:FileUpload ID="fileUploadOther" runat="server" />
     
    <asp:Button ID="btn_viewOther" runat="server" Text="View OTHER DOCUMENTS" onclick="btn_viewOther_Click" 
     />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">OTHER DOCUMENTS may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image6" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_prodSheet" runat="server" Text="PRODUCT SHEET Upload:"></asp:Label> <asp:FileUpload ID="fileUploadProdSheet" runat="server" />
     
    <asp:Button ID="btn_viewProdSheet" runat="server" Text="View PRODUCT SHEET" onclick="btn_viewProdSheet_Click" 
     />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">The PRODUCT SHEET may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image7" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_transport" runat="server" Text="TRANSPORTATION CERTIFICATE Upload:"></asp:Label> <asp:FileUpload ID="fileUploadTransport" runat="server" />
     
    <asp:Button ID="btn_viewTransport" runat="server" Text="View TRANSPORTATION CERTIFICATE" onclick="btn_viewTransport_Click" 
     />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">The TRANSPORTATION CERTIFICATE may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

     <div class="uploadHeaderDiv" style="width:100%;"> 
     <table><tr><td>
    <asp:Image ID="Image8" runat="server" ImageUrl="images/arrow_upload.gif"  /> </td><td>
    <asp:Label ID="lbl_label" runat="server" Text="MANUFACTURER LABEL Upload:"></asp:Label> <asp:FileUpload ID="fileUploadLabel" runat="server" />
     
    <asp:Button ID="btn_viewLabel" runat="server" Text="View MANUFACTURER LABEL" onclick="btn_viewLabel_Click" 
     />

    
    </td></tr>
    <tr>
    <td></td>
    <td class="tdToolTip">The MANUFACTURER LABEL may be a file of type .txt, .pdf, .html, etc.</td>
    </tr>
    </table>   </div>

    <p>
        <asp:Button ID="btn_saveMSDS" runat="server" Text="Save" 
            onclick="btn_saveMSDS_Click" />    
    </p>
    
</asp:Content>
