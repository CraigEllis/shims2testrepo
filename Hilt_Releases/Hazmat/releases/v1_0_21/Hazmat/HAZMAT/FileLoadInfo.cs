﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HAZMAT {
    public class FileLoadInfo {
        public FileLoadInfo() {
        }

        public FileLoadInfo(string fileName, string fileType, DateTime processDate,
                string processedBy, int recordsAdded, int recordsChanged, int recordsDeleted) {
            file_name = fileName;
            file_type = fileType;
            process_date = processDate;
            processed_by = processedBy;
            records_added = recordsAdded;
            records_changed = recordsChanged;
            records_deleted = recordsDeleted;
        }

        
        private string file_name;
        public String FileName {
            get { return file_name; }
            set { file_name = value; }
        }
        
        private string file_type;
        public String FileType {
            get { return file_type; }
            set { file_type = value; }
        }
        
        private DateTime process_date;
        public DateTime ProcessDate {
            get { return process_date; }
            set { process_date = value; }
        }
        
        private string processed_by;
        public String ProcessedBy {
            get { return processed_by; }
            set { processed_by = value; }
        }
        
        private int records_added;
        public int RecordsAdded {
            get { return records_added; }
            set { records_added = value; }
        }
        
        private int records_changed;
        public int RecordsChanged {
            get { return records_changed; }
            set { records_changed = value; }
        }

        private int records_deleted;
        public int RecordsDeleted {
            get { return records_deleted; }
            set { records_deleted = value; }
        }
    }
}