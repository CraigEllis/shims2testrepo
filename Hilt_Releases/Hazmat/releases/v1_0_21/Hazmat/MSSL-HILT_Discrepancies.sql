SELECT * FROM (
SELECT (v.COSAL + v.niin + v.[name]) AS hashkey, v.cosal AS cosal, v.niin, 
	nc.[description], nc.ui, v.[name] AS location, 0 AS mssl_qty, 
	sum(v.qty) AS inv_qty, sum(v.qty) AS qty_difference, 1 AS sortkey 
FROM vInventory v 
JOIN niin_catalog nc ON nc.niin = v.niin
WHERE (v.COSAL + v.niin + v.[name]) NOT IN
	(SELECT distinct (ati + niin + location) FROM mssl_inventory WHERE location IS NOT NULL)
GROUP BY (v.COSAL + v.niin + v.[name]), v.cosal, v.niin, v.[name], nc.[description], nc.ui
UNION
SELECT (m.ati + m.niin + m.location) AS hashkey, m.ati AS cosal, m.niin, 
	nc.[description], nc.ui, m.location AS location, sum(m.qty) AS mssl_qty, 0 AS inv_qty, sum(m.qty) AS qty_difference, 2 AS sortkey
FROM mssl_inventory m
JOIN niin_catalog nc ON nc.niin = m.niin
WHERE m.location IS NOT NULL AND (m.ati + m.niin + m.location) NOT IN
(SELECT distinct (COSAL + niin + name) FROM vinventory)
GROUP BY (m.ati + m.niin + m.location), m.ati, m.niin, m.location, nc.[description], nc.ui
UNION
SELECT (v.COSAL + v.niin + v.[name]) AS hashkey, v.cosal AS cosal, v.niin, 
	nc.[description], nc.ui, v.[name] AS location, SUM(m.qty) AS mssl_qty, sum(v.qty) AS inv_qty, 
	(sum(v.qty) - SUM(m.qty)) AS qty_difference, 3 AS sortkey 
FROM vInventory v, mssl_inventory m 
JOIN niin_catalog nc ON nc.niin = m.niin
WHERE (v.COSAL + v.niin + v.[name]) = (m.ati + m.niin + m.location)
GROUP BY (v.COSAL + v.niin + v.[name]), v.cosal, v.niin, v.[name], nc.[description], nc.ui
) AS q
WHERE (q.qty_difference != 0 and q.sortkey = 3) OR q.sortkey < 3
order by q.sortkey, q.hashkey;


StringBuilder  sb = new StringBuilder();
sb.AppendLine("SELECT * FROM ");
sb.AppendLine("  (SELECT ( v.cosal + v.niin + v.[name] ) AS hashkey, ");
sb.AppendLine("     v.cosal     AS cosal, ");
sb.AppendLine("     v.niin, ");
sb.AppendLine("     nc.[description], ");
sb.AppendLine("     nc.ui, ");
sb.AppendLine("     v.[name]    AS location, ");
sb.AppendLine("     0           AS mssl_qty, ");
sb.AppendLine("     SUM(v.qty)  AS inv_qty, ");
sb.AppendLine("     SUM(v.qty)  AS qty_difference, ");
sb.AppendLine("     1           AS sortkey ");
sb.AppendLine("  FROM vinventory v ");
sb.AppendLine("  JOIN niin_catalog nc ON nc.niin = v.niin ");
sb.AppendLine("    WHERE ( v.cosal + v.niin + v.[name] ) NOT IN ");
sb.AppendLine("     (SELECT DISTINCT ( ati + niin + location ) ");
sb.AppendLine("        FROM   mssl_inventory ");
sb.AppendLine("        WHERE location IS NOT NULL) ");
sb.AppendLine("  GROUP BY ( v.cosal + v.niin + v.[name] ), ");
sb.AppendLine("      v.cosal, ");
sb.AppendLine("      v.niin, ");
sb.AppendLine("      v.[name], ");
sb.AppendLine("      nc.[description], ");
sb.AppendLine("      nc.ui ");
sb.AppendLine("  UNION ");
sb.AppendLine("  SELECT ( m.ati + m.niin + m.location ) AS hashkey, ");
sb.AppendLine("     m.ati       AS cosal, ");
sb.AppendLine("     m.niin, ");
sb.AppendLine("     nc.[description], ");
sb.AppendLine("     nc.ui, ");
sb.AppendLine("     m.location  AS location, ");
sb.AppendLine("     SUM(m.qty)  AS mssl_qty, ");
sb.AppendLine("     0           AS inv_qty, ");
sb.AppendLine("     SUM(m.qty)  AS qty_difference, ");
sb.AppendLine("     2           AS sortkey ");
sb.AppendLine("  FROM mssl_inventory m ");
sb.AppendLine("     JOIN niin_catalog nc ");
sb.AppendLine("     ON nc.niin = m.niin ");
sb.AppendLine("  WHERE m.location IS NOT NULL ");
sb.AppendLine("     AND ( m.ati + m.niin + m.location ) NOT IN (SELECT DISTINCT ");
sb.AppendLine("       ( cosal + niin + name ) ");
sb.AppendLine("      FROM   vinventory) ");
sb.AppendLine("  GROUP BY ( m.ati + m.niin + m.location ), ");
sb.AppendLine("      m.ati, ");
sb.AppendLine("      m.niin, ");
sb.AppendLine("      m.location, ");
sb.AppendLine("      nc.[description], ");
sb.AppendLine("      nc.ui ");
sb.AppendLine("  UNION ");
sb.AppendLine("  SELECT ( v.cosal + v.niin + v.[name] ) AS hashkey, ");
sb.AppendLine("     v.cosal     AS cosal, ");
sb.AppendLine("     v.niin, ");
sb.AppendLine("     nc.[description], ");
sb.AppendLine("     nc.ui, ");
sb.AppendLine("     v.[name]    AS location, ");
sb.AppendLine("     SUM(m.qty)  AS mssl_qty, ");
sb.AppendLine("     SUM(v.qty)  AS inv_qty, ");
sb.AppendLine("     ( SUM(v.qty) - SUM(m.qty) ) AS qty_difference, ");
sb.AppendLine("     3           AS sortkey ");
sb.AppendLine("  FROM vinventory v, ");
sb.AppendLine("     mssl_inventory m ");
sb.AppendLine("     JOIN niin_catalog nc ");
sb.AppendLine("     ON nc.niin = m.niin ");
sb.AppendLine("  WHERE ( v.cosal + v.niin + v.[name] ) = ( m.ati + m.niin + m.location ) ");
sb.AppendLine("  GROUP BY ( v.cosal + v.niin + v.[name] ), ");
sb.AppendLine("      v.cosal, ");
sb.AppendLine("      v.niin, ");
sb.AppendLine("      v.[name], ");
sb.AppendLine("      nc.[description], ");
sb.AppendLine("      nc.ui ");
sb.AppendLine(") AS q ");
sb.AppendLine("WHERE ( q.qty_difference != 0 ");
sb.AppendLine("  AND q.sortkey = 3 ) ");
sb.AppendLine("  OR q.sortkey < 3 ");
sb.AppendLine("ORDER BY q.sortkey, ");
sb.AppendLine("    q.hashkey");


SELECT * FROM 
  (SELECT ( v.cosal + v.niin + v.[name] ) AS hashkey, 
     v.cosal     AS cosal, 
     v.niin, 
     nc.[description], 
     nc.ui, 
     v.[name]    AS location, 
     0           AS mssl_qty, 
     SUM(v.qty)  AS inv_qty, 
     SUM(v.qty)  AS qty_difference, 
     1           AS sortkey 
  FROM vinventory v 
  JOIN niin_catalog nc ON nc.niin = v.niin 
    WHERE ( v.cosal + v.niin + v.[name] ) NOT IN 
     (SELECT DISTINCT ( ati + niin + location ) 
        FROM   mssl_inventory 
        WHERE location IS NOT NULL) 
  GROUP BY ( v.cosal + v.niin + v.[name] ), 
      v.cosal, 
      v.niin, 
      v.[name], 
      nc.[description], 
      nc.ui 
  UNION 
  SELECT ( m.ati + m.niin + m.location ) AS hashkey, 
     m.ati       AS cosal, 
     m.niin, 
     nc.[description], 
     nc.ui, 
     m.location  AS location, 
     SUM(m.qty)  AS mssl_qty, 
     0           AS inv_qty, 
     SUM(m.qty)  AS qty_difference, 
     2           AS sortkey 
  FROM mssl_inventory m 
     JOIN niin_catalog nc 
     ON nc.niin = m.niin 
  WHERE m.location IS NOT NULL 
     AND ( m.ati + m.niin + m.location ) NOT IN (SELECT DISTINCT 
       ( cosal + niin + name ) 
      FROM   vinventory) 
  GROUP BY ( m.ati + m.niin + m.location ), 
      m.ati, 
      m.niin, 
      m.location, 
      nc.[description], 
      nc.ui 
  UNION 
  SELECT ( v.cosal + v.niin + v.[name] ) AS hashkey, 
     v.cosal     AS cosal, 
     v.niin, 
     nc.[description], 
     nc.ui, 
     v.[name]    AS location, 
     SUM(m.qty)  AS mssl_qty, 
     SUM(v.qty)  AS inv_qty, 
     ( SUM(v.qty) - SUM(m.qty) ) AS qty_difference, 
     3           AS sortkey 
  FROM vinventory v, 
     mssl_inventory m 
     JOIN niin_catalog nc 
     ON nc.niin = m.niin 
  WHERE ( v.cosal + v.niin + v.[name] ) = ( m.ati + m.niin + m.location ) 
  GROUP BY ( v.cosal + v.niin + v.[name] ), 
      v.cosal, 
      v.niin, 
      v.[name], 
      nc.[description], 
      nc.ui 
) AS q 
WHERE ( q.qty_difference != 0 
  AND q.sortkey = 3 ) 
  OR q.sortkey < 3 
ORDER BY q.sortkey, 
    q.hashkey

