// script to run on loading a Search Filter view
$(function () {
    // hide more & less if there's no data
    if ($('#mainGrid th').length < 2)
    {
        $('#btnGroupMoreLess').hide();
    }

    // set up links for more & less
    var pageSize = null;

    var minPageSize = 10;
    var maxPageSize = 30;

    if ($.cookie('pageSize'))
        pageSize = $.cookie('pageSize');
    else {
        if (pageSize == null)
            pageSize = 10;
    }

    if (pageSize >= maxPageSize) {
        $('#pagerMore').hide();
        pageSize = 30;
    }

    if (pageSize <= minPageSize) {
        $('#pagerLess').hide();
        pageSize = 10;
    }

    $.cookie('pageSize', pageSize);

    $('#pagerMore').click(
        function () {
            pageSize += 10;
            $.cookie('pageSize', pageSize);
            $(this).closest("form").submit();
        });

    $('#pagerLess').click(
        function () {
            pageSize -= 10;
            $.cookie('pageSize', pageSize);
            $(this).closest("form").submit();
        });

    // clear filters on ESC
    $("body").keypress(
                    function (event) {
                        if (event.which == 27) {
                            $('#clearFilters').click();
                        }
                    });

    // submit search on ENTER
    $("input[type=text]").keypress(
            function (event) {
                if (event.which == 13) {
                    $('form').submit();
                }
            }
        );

    // Enable auto-postback for drop-downs, radio buttons, and checkboxes
    $("select:[autopostback='True']").change(
	function () {
	    $(this).closest("form").submit();
	});

    // rearrange pagination buttons
    $(".paginationRight a:contains(first)").prepend('<i class="icon-fast-backward" />&nbsp;');
    $(".paginationRight a:contains(prev)").prepend('<i class="icon-step-backward" />');
    $(".paginationRight a:contains(next)").append('&nbsp;<i class="icon-step-forward" />');
    $(".paginationRight a:contains(last)").append('&nbsp;<i class="icon-fast-forward" />');
    $(".paginationRight").addClass("btn-group");
    $(".paginationRight a").addClass("btn");
    if ($('.paginationRight').length > 0) {
        $('.paginationRight').html($('.paginationRight').html().replace(/\|/g, ''));
        $('.paginationRight').html($('.paginationRight').html().replace(/first\s*prev/, ''));
        $('.paginationRight').html($('.paginationRight').html().replace(/next\s*last/, ''));
    }

    if ($(".paginationRight").html()) {
        removePaginationString("next");
        removePaginationString("prev");
        removePaginationString("first");
        removePaginationString("last");
    }

    // Add sort arrows
    $('.sort_asc').html($('.sort_asc').html() + " &#9650;");
    $('.sort_desc').html($('.sort_desc').html() + " &#9660;");

    // turn on ligatures
    ligature(false); 	        
});

function removePaginationString(str) {
    if ($(".paginationRight").html().indexOf(str) > 0)
        $(".paginationRight").html($(".paginationRight").html().replace(str, ""));
}