﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;

namespace HiltHub.Models
{
    public class MsdsListContainerViewModel
    {
        public IPagination<v_MSDS_HullType_Detail> MsdsPagedList { get; set; }
        public MsdsFilterViewModel MsdsFilterViewModel { get; set; }
        public GridSortOptions GridSortOptions { get; set; }
    }

    public class MsdsFilterViewModel
    {
        public string niin { get; set; }
        public string ShipName { get; set; }
        public string Master_MSDSSERNO { get; set; }
        public string Product { get; set; }
        public string Manufacturer { get; set; }

        public SelectList Ships { get; set; }
        public int ShipId  { get; set; }

        public SelectList HullTypes { get; set; }
        public int HullTypeId { get; set; }
    }

    public class MsdsAuditModel
    {
        public AUDIT_msds_master MsdsMaster { get; set; }
    }

    public class MsdsEditViewModel
    {
        public string MsdsOriginalValues { get; set; }
        public msds_master MsdsMaster { get; set; }
        public msds_afjm_psn MsdsAfjmPsn { get; set; }
        public v_MSDS_AFJM_PSN_Detail MsdsAfjmPsnDetail { get; set; }
        public IQueryable<msds_contractor_info> MsdsContractorInfo { get; set; }
        public msds_disposal MsdsDisposal { get; set; }
        public msds_document_type MsdsDocumentTypes { get; set; }
        public msds_dot_psn MsdsDotPsn { get; set; }
        public v_MSDS_DOT_PSN_Detail MsdsDotPsnDetail { get; set; }
        public msds_iata_psn MsdsIataPsn { get; set; }
        public v_MSDS_IATA_PSN_Detail MsdsIataPsnDetail { get; set; }
        public msds_imo_psn MsdsImoPsn { get; set; }
        public v_MSDS_IMO_PSN_Detail MsdsImoPsnDetail { get; set; }
        public msds_item_description MsdsItemDescription { get; set; }
        public msds_label_info MsdsLabelInfo { get; set; }
        public msds_phys_chemical MsdsPhysChemical { get; set; }
        public msds_transportation MsdsTransportation { get; set; }

        // Lists for multiple record sub tables
        public List<msds_contractor_info> MsdsContractorList { get; set; }
        public List<msds_ingredient> MsdsIngredientList { get; set; }
        public List<msds_radiological_info> MsdsRadiologicalInfo { get; set; }

        // Drop-down lists
        public SelectList HCCs { get; set; }

        public MsdsEditViewModel()
        {
            // Populate drop-down lists
            hubDataClassesDataContext dc = new hubDataClassesDataContext();
            HCCs = new SelectList(dc.hccs.AsEnumerable(), "hcc_id", "description");
        }
    }
}