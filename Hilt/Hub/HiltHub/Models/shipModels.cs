﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;

namespace HiltHub.Models
{
    public class ShipListContainerViewModel
    {
        public IPagination<v_Ship> ShipPagedList { get; set; }
        public ShipFilterViewModel ShipFilterView { get; set; }
        public GridSortOptions GridSortOptions { get; set; }
    }

    public class ShipEditViewModel
    {
        [Required]
        public int StatusID { get; set; }
        public SelectList Statuses { get; set; }

        public int HullTypeID { get; set; }
        public SelectList HullTypes { get; set; }

        public ship Ship { get; set; }
        public ShipEditViewModel()
        {
            var dc = new hubDataClassesDataContext();
            Statuses = new SelectList(dc.ship_statuses.AsEnumerable(), "status_id", "description");
            HullTypes =
                new SelectList(dc.hull_types.Where(x => x.active).Where(x => x.parent_type_id > 0).AsEnumerable(),
                               "hull_type_id",
                               "hull_description");
        }
    }

    public class ShipFilterViewModel
    {
        public string Name { get; set; }
        public string Uic { get; set; }
        public string HullNumber { get; set; }
        public int HullTypeID { get; set; }
        public SelectList HullTypes { get; set; }
    }
}
