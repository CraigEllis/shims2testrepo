﻿using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;

namespace HiltHub.Models
{
    public class AulListContainerViewModel
    {
        public IPagination<v_AUL_HullType_Detail> AulPagedList { get; set; }
        public AulFilterViewModel AulFilterViewModel { get; set; }
        public GridSortOptions GridSortOptions { get; set; }
    }

    public class AulFilterViewModel
    {
        public string NIIN { get; set; }
        public string Description { get; set; }
        public string CAGE { get; set; }
        public string Specs { get; set; }
        public string UsageCategoryDescription { get; set; }

        public SelectList Ships { get; set; }
        public int ShipId { get; set; }

        public SelectList CatalogGroups { get; set; }
        public int CatalogGroupId { get; set; }
    }
}