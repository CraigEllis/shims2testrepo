﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;

namespace HiltHub.Models {
    public class fileImportContainerViewModel {
        public IPagination<v_Ship> FileImportPagedList {
            get;
            set;
        }
        public fileImportFilterViewModel FileImportFilterViewModel {
            get;
            set;
        }
        public GridSortOptions GridSortOptions {
            get;
            set;
        }

        public string fileImportName {
            get;
            set;
        }

        public string fileTempName {
            get;
            set;
        }

        public string fileImportMsg {
            get;
            set;
        }
        public int selectedShipId {
            get;
            set;
        }
        public int selectedImportShipId {
            get;
            set;
        }
        public int selectedImportFileTypeId {
            get;
            set;
        }

        public bool ignoreChanges {
            get;
            set;
        }
    }

    public class fileImportFilterViewModel {
        public fileImportFilterViewModel()
        {
            SelectedHullTypeId = -1;
            selectedFileType = Configuration.UPLOAD_TYPES.MSSL;
            SelectedShipId = -1;
        }

        public List<SelectListItem> Ships {
            get;
            set;
        }

        public int SelectedShipId { get; set; }

        public List<SelectListItem> FileTypes {
            get;
            set;
        }

        public Configuration.UPLOAD_TYPES selectedFileType { get; set; }

        public List<SelectListItem> HullTypes { get; set; }

        public int SelectedHullTypeId { get; set; }

        public void Fill() {

            var dataContext = new hubDataClassesDataContext();

            Ships = dataContext.v_Ships
                            .Select(a =>
                                new {
                                    a.Ship_Name,
                                    a.ship_id
                                }
                            )
                            .ToList()
                            .Select(a =>
                                new SelectListItem {
                                    Text = a.Ship_Name,
                                    Value = a.ship_id.ToString(CultureInfo.InvariantCulture),
                                    Selected = a.ship_id == SelectedShipId
                                }).ToList();

            HullTypes = dataContext.v_Hull_Types.Where(x => x.parent_type_id != 0)
                            .Select(a =>
                                new {
                                    a.Hull_Type,
                                    a.hull_type_id
                                }
                            )
                            .ToList()
                            .Select(a =>
                                new SelectListItem {
                                    Text = a.Hull_Type,
                                    Value = a.hull_type_id.ToString(CultureInfo.InvariantCulture),
                                    Selected = a.hull_type_id == SelectedHullTypeId
                                }).ToList();

            FileTypes = new List<SelectListItem>();
            // Just fill with the MSSL file type for now
            selectedFileType = Configuration.UPLOAD_TYPES.MSSL;
            SelectListItem temp = 
                new SelectListItem
                    {
                        Value =
                            ((int) Configuration.UPLOAD_TYPES.MSSL).ToString(
                                CultureInfo.InvariantCulture),
                        Text = Configuration.UPLOAD_TYPES.MSSL.ToString(),
                        Selected = true
                    };
            FileTypes.Add(temp);
        }
    }

    public class fileImportDetailsViewModel {
        public v_Ship shipModel {
            get;
            set;
        }

        public IPagination<file_upload> MSSLImportPagedList {
            get;
            set;
        }
        public GridSortOptions MSSLGridSortOptions {
            get;
            set;
        }



    }
}