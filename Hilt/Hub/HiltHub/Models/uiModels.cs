﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HiltHub.Models
{
    public class DetailsHeaderData
    {
        public DetailsHeaderData()
        {
            ShowSave = true;
        }

        public string Title {get; set;}
        public string SubTitle {get; set;}
        public bool ShowSave {get; set;}
    }
}