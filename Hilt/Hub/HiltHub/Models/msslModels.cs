﻿using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.UI.Grid;

namespace HiltHub.Models
{
    public class MsslListContainerViewModel
    {
        public IPagination<v_MSSL_Inventory_Detail> MsslPagedList { get; set; }
        public MsslFilterViewModel MsslFilterViewModel { get; set; }
        public GridSortOptions GridSortOptions { get; set; }
    }

    public class MsslFilterViewModel
    {
        public string NIIN { get; set; }
        public string ShipName { get; set; }
        public string Product { get; set; }
        public SelectList ShelfLife { get; set; }

        public SelectList Ships { get; set; }
        public int ShipId { get; set; }
    }
}