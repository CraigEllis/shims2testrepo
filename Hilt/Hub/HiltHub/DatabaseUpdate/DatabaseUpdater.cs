﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using AESCommon.DatabaseUpdate;
using Common.Logging;

namespace HiltHub.DatabaseUpdate {

    public class DatabaseUpdater : AESCommon.DatabaseUpdate.DatabaseUpdater {
        private static readonly ILog logger = LogManager.GetCurrentClassLogger();

        // AppDBVersion 
        // ***** Remember to change the AppDBVersion to the new date when adding a new DBUpdate *****
        override public int AppDBVersion {
            get {
                return 20111104;
            }
        }

        // Update method
        public override int update(IDbConnection connection) {
            // Make sure the connection is open
            if (connection.State != ConnectionState.Open) {
                connection.Open();
            }

            // Check to see if the database is current
            DatabaseStatus status = null;
            try {
                status = checkDatabaseStatus(connection);

                if (status.Status == DatabaseStatus.DBStatus.Current) {
                    connection.Close();
                    logger.InfoFormat("Database Status - Current @ {0}", status.Version.ToString(CultureInfo.InvariantCulture));

                    return 0;
                }

            }
            catch (Exception ex) {
                logger.ErrorFormat("Database Update - {0}", ex.Message); 
            }

            // Build the list of updates to apply based on the version returned
            // from the checkDatabaseStatus call
            // DO NOT REMOVE OLD DBUpdates - simply add your to the bottom of the stack
            // ***** Remember to change the AppDBVersion to the new date when adding a new DBUpdate *****
            List<IDatabaseUpdate> commands = new List<IDatabaseUpdate>();
            if (status != null && status.Version < 20111103) {
                commands.Add(new DBUpdate20111103());
            }
            if (status != null && status.Version < 20111104) {
                commands.Add(new DBUpdate20111104());
            }

            // Apply the updates

            foreach (IDatabaseUpdate command in commands) {
                try
                {
                    int result = command.update(connection);

                    if (result != 0) {
                        // Update the database with the current AppDBVersion
                        updateDBVersion(command.AppDBVersion, connection);

                        logger.InfoFormat("DatabaseUpdate - Updated to version: {0}",
                                command.AppDBVersion.ToString(CultureInfo.InvariantCulture));
                    }
                    else {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();

                        // Something went wrong with the update - throw an Exception
                        Exception ex = new Exception("An error occured while processing " +
                            "database updates - please check the HILT_HUB log file for " +
                            "details. Error occured in DB Update: " + command.AppDBVersion.ToString(CultureInfo.InvariantCulture));
                        throw ex;
                    }
                }
                catch (Exception ex) {
                    logger.Error(command + " - " + ex.Message);
                }
            }

            connection.Close();

            return commands.Count;
        }
    }
}
