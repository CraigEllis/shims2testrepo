﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common.Logging;

namespace HiltHub.DatabaseUpdate {
    public class DBUpdate20111104 : AESCommon.DatabaseUpdate.IDatabaseUpdate {
        private static ILog logger = LogManager.GetCurrentClassLogger();

        SqlConnection connLocal = null;
        SqlCommand command = null;

        public int AppDBVersion {
            get { 
                return 20111104;
            }
        }

        public int update(IDbConnection connection) {
            int result = 0;

            // Set the connection and command
            connLocal = (SqlConnection) connection;

            if (connLocal.State != ConnectionState.Open) {
                connLocal.Open();
            }

            // Drop the created_by and changed_by columns
            // Then recreate them as strings 
            String[] tableNames = {"AUDIT_aul_inventory",
                "AUDIT_auth_use_list",
                "AUDIT_msds_inventory",
                "AUDIT_msds_master",
                "AUDIT_mssl_inventory",
                "AUDIT_mssl_master",
                "AUDIT_ships",
                "aul_inventory",
                "auth_use_list",
                "msds_inventory",
                "msds_master",
                "mssl_inventory",
                "mssl_master",
                "ships"};

            // Modify the created_by and changed_by columns 
            result = drop_by_idColumns(tableNames);
            result = add_byColumns(tableNames);

            // Update the triggers
            result = updateTriggers();

            return result;
        }

        private int drop_by_idColumns(String[] tableNames) {
            int result = 0;
            String sqlStmt = null;

            command = connLocal.CreateCommand();

            foreach (String tableName in tableNames) {
                // Created_by_id
                try {
                    sqlStmt = "ALTER TABLE " + tableName + " DROP COLUMN created_by_id";
                        command.CommandText = sqlStmt;

                        result = command.ExecuteNonQuery();

                        // Recreate the view
                        if (result == 0) {
                            logger.Error("drop_by_idColumns created_by_id - " + tableName);
                        }
                }
                catch (Exception ex) {
                    logger.ErrorFormat("drop_by_idColumns created_by_id - {0}", ex.Message);
                }

                // Changed_by_id
                try {
                    sqlStmt = "ALTER TABLE " + tableName + " DROP COLUMN changed_by_id";
                        command.CommandText = sqlStmt;

                        result = command.ExecuteNonQuery();

                    // Recreate the view
                    if (result == 0) {
                        logger.Error("drop_by_idColumns changed_by_id - " + tableName);
                    }
                }
                catch (Exception ex) {
                    logger.ErrorFormat("drop_by_idColumns changed_by_id - {0}", ex.Message);
                }
            }

            return result;
        }

        private int add_byColumns(String[] tableNames) {
            int result = 0;

            // Created_by_id
            String sqlStmt = null;
            command = connLocal.CreateCommand();


                foreach (String tableName in tableNames) {
                    try {
                        sqlStmt = "ALTER TABLE " + tableName + " ADD " +
                            "[created_by] [VARCHAR](50) NULL, " +
                            "[changed_by] [VARCHAR](50) NULL";
                        command.CommandText = sqlStmt;

                        result = (Int32) command.ExecuteNonQuery();

                        // Recreate the view
                        if (result == 0) {
                            logger.Error("add_byColumns - " + tableName);
                        }
                    }
                    catch (Exception ex) {
                        logger.ErrorFormat("add_byColumns - {0}", ex.Message);
                    }
                }

            return result;
        }

        private int updateTriggers() {
            int result = 0;

            result = updateShipsTriggers();
            result = updateAULTriggers();
            result = updateMSDSTriggers();
            result = updateMSSLTriggers();            

            return result;
        }

        private int updateShipsTriggers() {
            int result = 0;
            StringBuilder sb = new StringBuilder();

            command = connLocal.CreateCommand();

            // Delete
            try {
                // Drop trigger
                sb.Clear();
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_ships]')) ");
                sb.Append("DROP TRIGGER [dbo].[tr_delete_ships] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_delete_ships] ");
                sb.Append("on [dbo].[ships] for delete ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_ships (action, action_date, ship_id, current_status, name, uic, hull_type_id, hull_number,  ");
                sb.Append("	download_aul_date, download_msds_date, download_mssl_date,  ");
                sb.Append("	upload_aul_date, upload_msds_date, upload_mssl_date,  ");
                sb.Append("	created, created_by, changed, changed_by)  ");
                sb.Append("select 'DELETE', GETDATE(), ship_id, current_status, name, uic, hull_type_id, hull_number,  ");
                sb.Append("	download_aul_date, download_msds_date, download_mssl_date,  ");
                sb.Append("	upload_aul_date, upload_msds_date, upload_mssl_date,  ");
                sb.Append("	created, created_by, changed, changed_by  ");
                sb.Append("	from deleted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Delete - {0}", ex.Message);
            }

            // Insert
            try {
                // Drop trigger
                sb.Clear();
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_ships]')) ");
                sb.Append("DROP TRIGGER [dbo].[tr_insert_ships] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_insert_ships] ");
                sb.Append("on [dbo].[ships] for insert ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_ships (action, action_date, ship_id, current_status, name, uic, hull_type_id, hull_number,  ");
                sb.Append("	download_aul_date, download_msds_date, download_mssl_date,  ");
                sb.Append("	upload_aul_date, upload_msds_date, upload_mssl_date,  ");
                sb.Append("	created, created_by, changed, changed_by)  ");
                sb.Append("select 'INSERT', GETDATE(), ship_id, current_status, name, uic, hull_type_id, hull_number,  ");
                sb.Append("	download_aul_date, download_msds_date, download_mssl_date,  ");
                sb.Append("	upload_aul_date, upload_msds_date, upload_mssl_date,  ");
                sb.Append("	created, created_by, changed, changed_by  ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Insert - {0}", ex.Message);
            }

            // Delete
            try {
                // Drop trigger
                sb.Clear();
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_ships]')) ");
                sb.Append("DROP TRIGGER [dbo].[tr_update_ships] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_update_ships] ");
                sb.Append("on [dbo].[ships] for update ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_ships (action, action_date, ship_id, current_status, name, uic, hull_type_id, hull_number,  ");
                sb.Append("	download_aul_date, download_msds_date, download_mssl_date,  ");
                sb.Append("	upload_aul_date, upload_msds_date, upload_mssl_date,  ");
                sb.Append("	created, created_by, changed, changed_by)  ");
                sb.Append("select 'UPDATE', GETDATE(), ship_id, current_status, name, uic, hull_type_id, hull_number,  ");
                sb.Append("	download_aul_date, download_msds_date, download_mssl_date,  ");
                sb.Append("	upload_aul_date, upload_msds_date, upload_mssl_date,  ");
                sb.Append("	created, created_by, changed, changed_by  ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Update - {0}", ex.Message);
            }

            return result;
        }

        private int updateAULTriggers() {
            int result = 0;
            StringBuilder sb = new StringBuilder();

            command = connLocal.CreateCommand();

            // Delete
            try {
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_aul_inventory]')) ");
                sb.Append("DROP TRIGGER [dbo].[tr_delete_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_delete_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for delete ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'DELETE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from deleted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Delete - {0}", ex.Message);
            }

            // Insert
            try {
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_aul_inventory]'))");
                sb.Append(" DROP TRIGGER [dbo].[tr_insert_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_insert_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for insert ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'INSERT', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Insert - {0}", ex.Message);
            }

            // Delete
            try {
                // Drop trigger
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_aul_inventory]'))");
                sb.Append(" DROP TRIGGER [dbo].[tr_update_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_update_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for update ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'UPDATE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Update - {0}", ex.Message);
            }

            return result;
        }

        private int updateMSSLTriggers() {
            int result = 0;
            StringBuilder sb = new StringBuilder();

            command = connLocal.CreateCommand();

            // Delete
            try {
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_aul_inventory]')) ");
                sb.Append("DROP TRIGGER [dbo].[tr_delete_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_delete_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for delete ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'DELETE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from deleted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Delete - {0}", ex.Message);
            }

            // Insert
            try {
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_aul_inventory]'))");
                sb.Append(" DROP TRIGGER [dbo].[tr_insert_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_insert_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for insert ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'INSERT', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Insert - {0}", ex.Message);
            }

            // Delete
            try {
                // Drop trigger
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_aul_inventory]'))");
                sb.Append(" DROP TRIGGER [dbo].[tr_update_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_update_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for update ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'UPDATE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Update - {0}", ex.Message);
            }

            return result;
        }

        private int updateMSDSTriggers() {
            int result = 0;
            StringBuilder sb = new StringBuilder();

            command = connLocal.CreateCommand();

            // Delete
            try {
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_aul_inventory]')) ");
                sb.Append("DROP TRIGGER [dbo].[tr_delete_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_delete_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for delete ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'DELETE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from deleted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Delete - {0}", ex.Message);
            }

            // Insert
            try {
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_aul_inventory]'))");
                sb.Append(" DROP TRIGGER [dbo].[tr_insert_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_insert_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for insert ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'INSERT', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Insert - {0}", ex.Message);
            }

            // Delete
            try {
                // Drop trigger
                // Drop trigger
                sb.Append("IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_aul_inventory]'))");
                sb.Append(" DROP TRIGGER [dbo].[tr_update_aul_inventory] ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();

                // Create trigger
                sb.Clear();
                sb.Append("SET ANSI_NULLS ON ");
                sb.Append("GO ");
                sb.Append("SET QUOTED_IDENTIFIER ON ");
                sb.Append("GO ");
                sb.Append("CREATE Trigger [dbo].[tr_update_aul_inventory] ");
                sb.Append("on [dbo].[aul_inventory] for update ");
                sb.Append("as ");
                sb.Append("begin ");
                sb.Append("SET NOCOUNT ON; ");
                sb.Append("insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by) ");
                sb.Append("select 'UPDATE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, ");
                sb.Append("	manually_entered, cage, msds_num, created, created_by, changed, changed_by ");
                sb.Append("	from inserted; ");
                sb.Append("end ");

                command.CommandText = sb.ToString();
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                logger.ErrorFormat("updateAULTriggers - Update - {0}", ex.Message);
            }

            return result;
        }
    }
}