﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;
using HiltHub.DataObjects;

namespace HiltHub.Parsers {
    public class MSSLParser {
        public enum FILE_STATUS {
            Valid,
            Invalid_Type,
            Invalid_UIC,
            Invalid_Date,
            Invalid_UIC_Date
        };

        private DateTime file_date;
        public DateTime fileDate {
            get {
                return file_date;
            }
        }

        private string previousNiin;
        private string previousRow;

        public FILE_STATUS validateFile(StreamReader stream, String shipUIC, DateTime shipDate) {
            bool validUIC = false;
            bool validDate = false;
            bool validType = false;
            int nMax = 50;
            int nRow = 0;
            String searchFileType = "MASTER STOCK STATUS and LOCATOR LIST";
            String searchUIC = "UIC:\\s{0,10}" + shipUIC;
            String searchDate = "UIC:.+?(\\d{2}\\s\\w{3}\\s\\d{4})";

            // Process the first 50 rows looking for valid UIC and date
            string row = "";
            while (row != null && !stream.EndOfStream && nRow < nMax) {
                //get the next row from the reader
                row = stream.ReadLine();

                // Check for a valid file format
                if (row != null && Regex.IsMatch(row, searchFileType)) {
                    validType = true;
                }

                // Check the date
                if (row != null && Regex.IsMatch(row, searchDate)) {
                    Match match = Regex.Match(row, searchDate);
                    file_date = Convert.ToDateTime(match.Groups[1].Value);

                    if (file_date > shipDate)
                        validDate = true;                        
                }

                // look for a UIC: value in the file
                if (row != null && Regex.IsMatch(row, searchUIC)) {
                    validUIC = true;
                }

                if (validDate && validUIC && validType)
                    break;

                nRow++;
            }

            // Check the results
            FILE_STATUS status = FILE_STATUS.Valid;
            if (!validType) {
                status = FILE_STATUS.Invalid_Type;
            }
            else if (!validUIC && !validDate) {
                status = FILE_STATUS.Invalid_UIC_Date;
            }
            else if (!validUIC) {
                status = FILE_STATUS.Invalid_UIC;
            }
            else if (!validDate) {
                status = FILE_STATUS.Invalid_Date;
            }

            stream.Close();

            return status;
        }

        public MSSLData parse(StreamReader stream, String ship_UIC) {

            bool foundRowBefore = false;
            List<MsslInventory> inventoryList = new List<MsslInventory>();
            List<MsslSubs> subList = new List<MsslSubs>();
            MSSLData data = new MSSLData();

            // get headers
            string row = "";
            while (row != null && !stream.EndOfStream) {
                //get the next row from the reader
                row = stream.ReadLine();
                if (row == null) continue;

                //check if length is > 2
                if (row.Length <= 2) continue;
                string atc = row.Substring(0, 2).Trim();

                try {
                    //attempt to parse the value.  If the value is a number, we know the row is an inventory row
                    Int32.Parse(atc);

                    foundRowBefore = true;
                    //store the row so it can be checked to make sure we don't add subs for rows we're not interested in
                    previousRow = row;

                    // Check smcc and make sure it's acceptable - ignore for MSSL upload
                    if (checkSMCC(row.Substring(134, 6).Trim())) {
                        // Add to inventory
                        inventoryList.Add(getInventoryData(row));
                    }
                }
                catch (Exception ex) {
                    //check for subs - If the the row comes after an inventory row (subs must) and the 8 spaces before the row are blank, we assume these are subs
                    Debug.WriteLine("EXCEPTION - parse - " + ex.Message);

                    if (foundRowBefore && row.Substring(0, 8).Trim().Equals("") && checkSMCC(previousRow.Substring(134, 6).Trim())) {

                        string[] subNiins = row.Split(' ');

                        foreach (string s in subNiins) {
                            //store the sub niin values and associate them with the 'parent' niin
                            MsslSubs sub = new MsslSubs();

                            if (!s.Trim().Equals("")) {
                                sub.Niin = previousNiin;
                                sub.Sub_niin = s.Trim();
                                subList.Add(sub);
                            }


                            previousRow = "";
                        }
                    }

                    //wasn't the start of an inventory row
                    foundRowBefore = false;
                }
            }

            data.InventoryList = inventoryList;
            data.SubList = subList;

            stream.Close();

            return data;
        }

        private bool checkSMCC(string smcc) {

            bool accept = false;

            if (smcc.Equals("5") || smcc.Equals("6") || smcc.Equals("7") || smcc.Equals("8") || smcc.ToUpper().Equals("B") || smcc.ToUpper().Equals("C") ||
               smcc.ToUpper().Equals("D") || smcc.ToUpper().Equals("F") || smcc.ToUpper().Equals("G") || smcc.ToUpper().Equals("H") || smcc.ToUpper().Equals("I") ||
               smcc.ToUpper().Equals("J") || smcc.ToUpper().Equals("K") || smcc.ToUpper().Equals("L") || smcc.ToUpper().Equals("N") || smcc.ToUpper().Equals("O") ||
               smcc.ToUpper().Equals("P") || smcc.ToUpper().Equals("Q") || smcc.ToUpper().Equals("S") || smcc.ToUpper().Equals("T") || smcc.ToUpper().Equals("U") ||
               smcc.ToUpper().Equals("V") || smcc.ToUpper().Equals("W") || smcc.ToUpper().Equals("X") || smcc.ToUpper().Equals("Z")) {

                //row is acceptable
                accept = true;
                Debug.WriteLine(smcc);
            }

            return accept;

        }

        private MsslInventory getInventoryData(string row) {
            MsslInventory inventory = new MsslInventory();



            //check if the niin value is empty..if it is empty, assign it the same niin as the niin above it
            if (row.Substring(4, 10).Trim().Equals("")) {
                //niin is empty..assign it the niin above
                inventory.Niin = previousNiin;
            }
            else {
                //niin is not empty, set that value as the previous niin_value
                previousNiin = row.Substring(4, 10).Trim();
                inventory.Niin = previousNiin;
            }


            inventory.Atc = Int32.Parse(row.Substring(0, 2).Trim());
            inventory.Ati = row.Substring(14, 4).Trim();
            inventory.Cog = row.Substring(18, 7).Trim();
            inventory.Mcc = row.Substring(25, 4).Trim();
            inventory.Ui = row.Substring(29, 2).Trim();
            inventory.Up = Convert.ToDouble(row.Substring(31, 12).Trim());
            inventory.Netup = Convert.ToDouble(row.Substring(43, 12).Trim());
            inventory.Location = row.Substring(55, 15).Trim();
            inventory.Qty = Convert.ToInt32(row.Substring(70, 9).Trim());
            inventory.Lmc = row.Substring(79, 5).Trim();
            inventory.Irc = row.Substring(84, 4).Trim();
            inventory.Dues = Convert.ToInt32(row.Substring(88, 8).Trim());
            inventory.Ro = Convert.ToInt32(row.Substring(96, 8).Trim());
            inventory.Rp = Convert.ToInt32(row.Substring(104, 8).Trim());
            inventory.Amd = Convert.ToDouble(row.Substring(112, 8).Trim());
            inventory.Smic = row.Substring(120, 4).Trim();
            inventory.Slc = row.Substring(124, 5).Trim();
            inventory.Slac = row.Substring(129, 5).Trim();
            inventory.Smcc = row.Substring(134, 6).Trim();
            inventory.Nomenclature = row.Substring(140, 15);

            return inventory;
        }


        public int getTotalInventory(StreamReader stream) {

            int total = 0;

            string row = "";
            while (row != null && !stream.EndOfStream) {
                //get the next row from the reader
                row = stream.ReadLine();

                //check if length is > 2
                if (row == null || row.Length <= 2) continue;
                string atc = row.Substring(0, 2).Trim();

                try {
                    //attempt to parse the value.  If the value is a number, we know the row is an inventory row
                    Int32.Parse(atc);

                    total++;

                }
                catch (Exception ex) {
                    Debug.WriteLine("EXCEPTION - getTotalInventory - "
                                                       + ex.Message);
                }
            }

            stream.Close();

            return total;

        }
    }
}