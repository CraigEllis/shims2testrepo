// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments
#pragma warning disable 1591
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace HiltHub.Controllers {
    public partial class MsdsController {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public MsdsController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected MsdsController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result) {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.JsonResult GetExportList() {
            return new T4MVC_JsonResult(Area, Name, ActionNames.GetExportList);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult Export() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.Export);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult Index() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult Edit() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.Edit);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public MsdsController Actions { get { return MVC.Msds; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Msds";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass {
            public readonly string Clear = "Clear";
            public readonly string GetExportList = "GetExportList";
            public readonly string Export = "Export";
            public readonly string Index = "Index";
            public readonly string Edit = "Edit";
        }


        static readonly ViewNames s_views = new ViewNames();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewNames Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewNames {
            public readonly string Edit = "~/Views/msds/Edit.cshtml";
            public readonly string Export = "~/Views/msds/Export.cshtml";
            public readonly string Index = "~/Views/msds/Index.cshtml";
            public readonly string SearchFilters = "~/Views/msds/SearchFilters.cshtml";
            static readonly _edit s_edit = new _edit();
            public _edit edit { get { return s_edit; } }
            public partial class _edit{
                public readonly string Afjm = "~/Views/msds/edit/Afjm.cshtml";
                public readonly string AfjmDetail = "~/Views/msds/edit/AfjmDetail.cshtml";
                public readonly string Contractors = "~/Views/msds/edit/Contractors.cshtml";
                public readonly string Description = "~/Views/msds/edit/Description.cshtml";
                public readonly string Disposal = "~/Views/msds/edit/Disposal.cshtml";
                public readonly string Dot = "~/Views/msds/edit/Dot.cshtml";
                public readonly string Iata = "~/Views/msds/edit/Iata.cshtml";
                public readonly string Imo = "~/Views/msds/edit/Imo.cshtml";
                public readonly string Ingredients = "~/Views/msds/edit/Ingredients.cshtml";
                public readonly string Label = "~/Views/msds/edit/Label.cshtml";
                public readonly string Master = "~/Views/msds/edit/Master.cshtml";
                public readonly string PhysChem = "~/Views/msds/edit/PhysChem.cshtml";
                public readonly string Radiological = "~/Views/msds/edit/Radiological.cshtml";
                public readonly string Transportation = "~/Views/msds/edit/Transportation.cshtml";
            }
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public class T4MVC_MsdsController: HiltHub.Controllers.MsdsController {
        public T4MVC_MsdsController() : base(Dummy.Instance) { }

        public override System.Web.Mvc.ActionResult Clear() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Clear);
            return callInfo;
        }

        public override System.Web.Mvc.JsonResult GetExportList(string niin, string msdsSerNo, int? shipId, int? hullTypeId, string product, string manufacturer) {
            var callInfo = new T4MVC_JsonResult(Area, Name, ActionNames.GetExportList);
            callInfo.RouteValueDictionary.Add("niin", niin);
            callInfo.RouteValueDictionary.Add("msdsSerNo", msdsSerNo);
            callInfo.RouteValueDictionary.Add("shipId", shipId);
            callInfo.RouteValueDictionary.Add("hullTypeId", hullTypeId);
            callInfo.RouteValueDictionary.Add("product", product);
            callInfo.RouteValueDictionary.Add("manufacturer", manufacturer);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult Export(string niin, string msdsSerNo, int? shipId, int? hullTypeId, string product, string manufacturer) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Export);
            callInfo.RouteValueDictionary.Add("niin", niin);
            callInfo.RouteValueDictionary.Add("msdsSerNo", msdsSerNo);
            callInfo.RouteValueDictionary.Add("shipId", shipId);
            callInfo.RouteValueDictionary.Add("hullTypeId", hullTypeId);
            callInfo.RouteValueDictionary.Add("product", product);
            callInfo.RouteValueDictionary.Add("manufacturer", manufacturer);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult Index(int? page, MvcContrib.UI.Grid.GridSortOptions gridSortOptions, string niin, string msdsSerNo, int? shipId, int? hullTypeId, string product, string manufacturer) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Index);
            callInfo.RouteValueDictionary.Add("page", page);
            callInfo.RouteValueDictionary.Add("gridSortOptions", gridSortOptions);
            callInfo.RouteValueDictionary.Add("niin", niin);
            callInfo.RouteValueDictionary.Add("msdsSerNo", msdsSerNo);
            callInfo.RouteValueDictionary.Add("shipId", shipId);
            callInfo.RouteValueDictionary.Add("hullTypeId", hullTypeId);
            callInfo.RouteValueDictionary.Add("product", product);
            callInfo.RouteValueDictionary.Add("manufacturer", manufacturer);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult Edit(string id) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Edit);
            callInfo.RouteValueDictionary.Add("id", id);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult Edit(int id, HiltHub.Models.MsdsEditViewModel model) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Edit);
            callInfo.RouteValueDictionary.Add("id", id);
            callInfo.RouteValueDictionary.Add("model", model);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591
