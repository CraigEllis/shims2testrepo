// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments
#pragma warning disable 1591
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace HiltHub.Controllers {
    public partial class AulController {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public AulController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected AulController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result) {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult Index() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public System.Web.Mvc.ActionResult Details() {
            return new T4MVC_ActionResult(Area, Name, ActionNames.Details);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public AulController Actions { get { return MVC.Aul; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Aul";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass {
            public readonly string Clear = "Clear";
            public readonly string Index = "Index";
            public readonly string Details = "Details";
        }


        static readonly ViewNames s_views = new ViewNames();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewNames Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewNames {
            public readonly string Details = "~/Views/aul/Details.cshtml";
            public readonly string Index = "~/Views/aul/Index.cshtml";
            public readonly string SearchFilters = "~/Views/aul/SearchFilters.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public class T4MVC_AulController: HiltHub.Controllers.AulController {
        public T4MVC_AulController() : base(Dummy.Instance) { }

        public override System.Web.Mvc.ActionResult Clear() {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Clear);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult Index(int? page, MvcContrib.UI.Grid.GridSortOptions gridSortOptions, string niin, string description, string cage, int? shipId, int? catalogGroupId) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Index);
            callInfo.RouteValueDictionary.Add("page", page);
            callInfo.RouteValueDictionary.Add("gridSortOptions", gridSortOptions);
            callInfo.RouteValueDictionary.Add("niin", niin);
            callInfo.RouteValueDictionary.Add("description", description);
            callInfo.RouteValueDictionary.Add("cage", cage);
            callInfo.RouteValueDictionary.Add("shipId", shipId);
            callInfo.RouteValueDictionary.Add("catalogGroupId", catalogGroupId);
            return callInfo;
        }

        public override System.Web.Mvc.ActionResult Details(string id) {
            var callInfo = new T4MVC_ActionResult(Area, Name, ActionNames.Details);
            callInfo.RouteValueDictionary.Add("id", id);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591
