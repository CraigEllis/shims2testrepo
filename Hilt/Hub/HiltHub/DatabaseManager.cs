﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using Common.Logging;
using HiltHub.DatabaseUpdate;
using HiltHub.DataObjects;
using HiltHub.Models;
using System.Text;
using System.Data;

namespace HiltHub {
    public class DatabaseManager {
        public enum FILE_CHANGES {
            Initial_Load,
            Threshold_Exceeded,
            Acceptable_Changes
        };

        public enum FILE_TABLES {
            FileUpload,
            FileDownLoad
        };

        private static readonly ILog logger = LogManager.GetCurrentClassLogger();

        private static string MDF_CONNECTION;
        public static String DB_BACKUP_DIRECTORY;

    #region Constructors
        public DatabaseManager() {
            if (MDF_CONNECTION == null) {
                MDF_CONNECTION = Configuration.ConnectionInfo;
            }

            if (DB_BACKUP_DIRECTORY == null)
            DB_BACKUP_DIRECTORY = Configuration.HILT_HUB_DataFolder + "\\Backups";

            // Make sure the folder exists
            if (!Directory.Exists(DB_BACKUP_DIRECTORY)) {
                Directory.CreateDirectory(DB_BACKUP_DIRECTORY);
            }
        }

        public DatabaseManager(string connectionString) {
            if (MDF_CONNECTION == null) {
                MDF_CONNECTION = connectionString;
            }
        }
    #endregion

    #region User Management
        //**********************************************************************
        public static bool isAdmin(string username) {
            return isUserInRole(username, "ADMIN");
        }

        public static bool isSupplyOfficer(string username) {
            return isUserInRole(username, "SUPPLY_OFFICER");
        }

        public static bool isSupplyUser(string username) {
            return isUserInRole(username, "SUPPLY_USER");
        }

        public static bool isReports(string username) {
            return isUserInRole(username, "REPORTS");
        }

        public static bool isUserInRole(string username, string role) {
            bool isUserInRole = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            string stmt = "SELECT COUNT(*) FROM user_roles WHERE username=@username AND role=@role";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@username", dbNull(username));
            cmd.Parameters.AddWithValue("@role", dbNull(role));

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                isUserInRole = true;

            return isUserInRole;

        }
    #endregion

    #region Hull_Types
        //**********************************************************************
        // Hull_Types
        public bool checkHullTypeExists(int hull_type_id) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM hull_types WHERE hull_type_id = @hull_type_id";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);
            cmd.Parameters.AddWithValue("@hull_type_id", hull_type_id);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public bool checkHullTypeExists(String hull_type) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM hull_types WHERE hull_type = @hull_type";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);
            cmd.Parameters.AddWithValue("@hull_type", hull_type);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public long insertHull_Type(HullType hullType, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "INSERT INTO hull_Types (" +
                "hull_type, hull_description, active, parent_type_id " +
                "VALUES(" +
                "@hull_type, @hull_description, @active, @parent_type_id); " +
                "SELECT SCOPE_IDENTITY()";

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@hull_type", hullType.hull_type);
                insertCmd.Parameters.AddWithValue("@hull_description", dbNull(hullType.hull_description));
                insertCmd.Parameters.AddWithValue("@active", hullType.active);
                insertCmd.Parameters.AddWithValue("@parent_type_id", hullType.parent_type_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                insertCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Inserted HullType: {0}, {1} ID = {2}",
                    hullType.hull_type, hullType.hull_description, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error insertingHullType for: {0}, {1} - {2}", 
                    hullType.hull_type, hullType.hull_description, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateHull_Type(HullType hullType, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE hull_Types SET " +
                "hull_type = @hull_type, hull_description = @hull_description, " +
                "active = @active, parent_type_id = @parent_type_id " +
                "WHERE hull_type_id = @hull_type_id";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@hull_type_id", hullType.hull_type_id);
                updateCmd.Parameters.AddWithValue("@hull_type", hullType.hull_type);
                updateCmd.Parameters.AddWithValue("@hull_description", dbNull(hullType.hull_description));
                updateCmd.Parameters.AddWithValue("@active", hullType.active);
                updateCmd.Parameters.AddWithValue("@parent_type_id", hullType.parent_type_id);

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Updated HullType: ID = {0} to {1}, {2}, {3}, {4}",
                    hullType.hull_type_id, hullType.hull_type, hullType.hull_description,
                    hullType.parent_type_id, hullType.active);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updatingHullType for ID = {0}, {1} - {2}",
                    hullType.hull_type_id, hullType.hull_type, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long deleteHull_Type(HullType hullType, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM hull_Types " +
                "WHERE hull_type_id = @hull_type_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@hull_type_id", hullType.hull_type_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted HullType: ID = {0}, {1}",
                    hullType.hull_type_id, hullType.hull_type);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingHullType for ID = {0}, {1} - {2}",
                    hullType.hull_type_id, hullType.hull_type, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region Ship Status
        // Ship Statuses
        public bool checkShipExists(int ship_id) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM ships WHERE ship_id = @ship_id";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);
            cmd.Parameters.AddWithValue("@ship_id", ship_id);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public bool checkShipExists(String ship_name) {
            bool exists = false;
            String sqlStmt = "SELECT COUNT(*) FROM ships WHERE name = @ship_name";
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);
            cmd.Parameters.AddWithValue("@ship_name", ship_name);

            int count = (Int32) cmd.ExecuteScalar();

            cmd.Dispose();
            conn.Close();

            if (count > 0)
                exists = true;

            return exists;
        }

        public long insertShipStatus(String description, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "INSERT INTO ship_statuses (" +
                "description " +
                "VALUES(@description); " +
                "SELECT SCOPE_IDENTITY()";

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@description", dbNull(description));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                insertCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Inserted ShipStatus: {0} ID = {1}",
                    description, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error insertingShipStatus for: {0} - {1}",
                    description, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShipStatus(int status_id, String description, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ship_statuses SET " +
                "description = @description " +
                "WHERE status_id = @status_id";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@status_id", status_id);
                updateCmd.Parameters.AddWithValue("@description", dbNull(description));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Updated Ship Status: ID = {0} to {1}",
                    status_id, description);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updatingShipStatus ID = {0}, {1} - {2}",
                    status_id, description, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long deleteShipStatus(int status_id, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM ship_statuses " +
                "WHERE status_id = @status_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@status_id", status_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted ShipStatus: ID = {0}",
                    status_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingShipStatus ID = {0} - {1}",
                    status_id, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region Ships
        // Ships
        public Ship getShipRecord(int ship_id) {
            Ship record = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string sqlStmt = "SELECT * FROM v_Ships WHERE ship_id = @ship_id";

            try {
                SqlCommand cmd = new SqlCommand(sqlStmt, conn);

                //insert rows
                cmd.Parameters.AddWithValue("@ship_id", ship_id);

                using (SqlDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new Ship();
                        int temp_id = -1;
                        DateTime tempDate;

                        record.ship_id = ship_id;
                        int.TryParse(rdr["current_status"].ToString(), out temp_id);
                        record.current_status = temp_id;
                        record.ship_status = rdr["ship_status"].ToString();
                        record.uic = rdr["uic"].ToString();
                        record.hull_number = rdr["Hull_Number"].ToString();
                        record.name = rdr["Ship_Name"].ToString();
                        int.TryParse(rdr["hull_type_id"].ToString(), out temp_id);
                        record.hull_type_id = temp_id;
                        record.hull_type = rdr["Hull_Type"].ToString();
                        record.hull_description = rdr["Hull_Description"].ToString();
                        record.hull_active = Convert.ToBoolean(rdr["Hull_Active"]).ToString(CultureInfo.InvariantCulture);
                        int.TryParse(rdr["parent_type_id"].ToString(), out temp_id);
                        record.parent_type_id = temp_id;
                        record.parent_hull_type = rdr["Parent_Hull_Type"].ToString();
                        DateTime.TryParse(rdr["AUL_Download"].ToString(), out tempDate);
                        record.download_aul_date = tempDate;
                        DateTime.TryParse(rdr["MSDS_Download"].ToString(), out tempDate);
                        record.download_msds_date = tempDate;
                        DateTime.TryParse(rdr["MSSL_Download"].ToString(), out tempDate);
                        record.download_mssl_date = tempDate;
                        DateTime.TryParse(rdr["AUL_Upload"].ToString(), out tempDate);
                        record.upload_aul_date = tempDate;
                        DateTime.TryParse(rdr["MSDS_Upload"].ToString(), out tempDate);
                        record.upload_msds_date = tempDate;
                        DateTime.TryParse(rdr["MSSL_Upload"].ToString(), out tempDate);
                        record.upload_mssl_date = tempDate;
                        DateTime.TryParse(rdr["Created"].ToString(), out tempDate);
                        record.created = tempDate;
                        if (rdr["Created_By"] != DBNull.Value)
                            record.created_by = rdr["Created_By"].ToString();
                        DateTime.TryParse(rdr["Changed"].ToString(), out tempDate);
                        record.changed = tempDate;
                        if (rdr["Changed_By"] != DBNull.Value)
                            record.changed_by = rdr["Changed_by"].ToString();
                    }
                }

                cmd.Dispose();
            }
            finally {
                conn.Close();
            }

            return record;
        }

        public long insertShip(Ship ship, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "INSERT INTO ships (" +
                "current_status, name, uic, hull_type_id, hull_number, " +
                "created, created_by) VALUES(" +
                "@current_status, @name, @uic, @hull_type_id, @hull_number, @created, @created_by" +
                ");  SELECT SCOPE_IDENTITY()";

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@current_status", ship.current_status);
                insertCmd.Parameters.AddWithValue("@name", dbNull(ship.name));
                insertCmd.Parameters.AddWithValue("@uic", ship.uic);
                insertCmd.Parameters.AddWithValue("@hull_type_id", ship.hull_type_id);
                insertCmd.Parameters.AddWithValue("@hull_number", ship.hull_number);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", dbNull(userName));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                insertCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Inserted ShipRecord: {0}, {1}, {2}, {3} ID = {4}",
                    ship.hull_number, ship.name, ship.uic, ship.hull_type_id, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error insertingShipRecord: {0}, {1}, {2}, {3} - {4}",
                    ship.hull_number, ship.name, ship.uic, ship.hull_type_id, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShip(Ship ship, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ships SET " +
                "current_status = @current_status, name = @name, uic = @uic, " +
                "hull_type_id = @hull_type_id, hull_number = @hull_number, " +
                "changed = @changed, changed_by = @changed_by " +
                "WHERE ship_id = @ship_id";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@ship_id", ship.ship_id);
                updateCmd.Parameters.AddWithValue("@current_status", ship.current_status);
                updateCmd.Parameters.AddWithValue("@name", dbNull(ship.name));
                updateCmd.Parameters.AddWithValue("@uic", ship.uic);
                updateCmd.Parameters.AddWithValue("@hull_type_id", ship.hull_type_id);
                updateCmd.Parameters.AddWithValue("@hull_number", ship.hull_number);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", dbNull(userName));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Updated ShipRecord ID = {0}, {1}, {2}, {3}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updatingShipRecord ID = {0}, {1}, {2}, {3} - {4}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long deleteShip(Ship ship, String userName) {
            long result = 0;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM ships " +
                "WHERE ship_id = @ship_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@ship_id", ship.ship_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted ShipRecord ID = {0}, {1}, {2}, {3}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingShipRecord ID = {0}, {1}, {2}, {3} - {4}",
                    ship.ship_id, ship.hull_number, ship.name, ship.uic, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShipFileUpload(String uic, Configuration.UPLOAD_TYPES uploadType, 
            DateTime uploadDate, String userName) {
            long result = 0;

            // Set the upload date field based on the uploadType
            String fieldName = null;
            switch (uploadType) {
                case Configuration.UPLOAD_TYPES.AUL:
                    fieldName = "upload_aul_date";
                    break;
                case Configuration.UPLOAD_TYPES.MSDS:
                    fieldName = "upload_msds_date";
                    break;
                case Configuration.UPLOAD_TYPES.MSSL:
                    fieldName = "upload_mssl_date";
                    break;
                default:
                    break;
            }

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ships SET " +
                fieldName + " = @uploadDate, " +
                "changed = @changed, changed_by = @changed_by " +
                "WHERE uic = @uic";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@uic", uic);
                updateCmd.Parameters.AddWithValue("@uploadDate", uploadDate);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", dbNull(userName));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateShipFileDownload(String uic, Configuration.DOWNLOAD_TYPES downloadType,
            DateTime downloadDate, String userName) {
            long result = 0;

            // Set the upload date field based on the uploadType
            String fieldName = null;
            switch (downloadType) {
                case Configuration.DOWNLOAD_TYPES.AUL:
                    fieldName = "download_aul_date";
                    break;
                case Configuration.DOWNLOAD_TYPES.MSDS:
                    fieldName = "download_msds_date";
                    break;
                case Configuration.DOWNLOAD_TYPES.MSSL:
                    fieldName = "download_mssl_date";
                    break;
                default:
                    break;
            }

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "UPDATE ships SET " +
                fieldName + " = @downloadDate, " +
                "changed = @changed, changed_by = @changed_by " +
                "WHERE uic = @uic";

            try {
                SqlCommand updateCmd = new SqlCommand(sqlStmt, conn);
                updateCmd.Transaction = tran;

                //insert rows
                updateCmd.Parameters.AddWithValue("@uic", uic);
                updateCmd.Parameters.AddWithValue("@downloadDate", downloadDate);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", dbNull(userName));

                object o = updateCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                updateCmd.Parameters.Clear();

                tran.Commit();
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region MSSL
        public Hashtable getMSSLQuantities(String ship_UIC) {
            Hashtable ht = new Hashtable();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            String stmt = "SELECT niin, location, qty FROM mssl_inventory WHERE uic = @uic";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@uic", ship_UIC);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    int qty = 0;
                    int.TryParse(rdr["qty"].ToString(), out qty);
                    ht.Add(rdr["niin"] + "_" + rdr["location"], qty);
                }
            }

            cmd.Dispose();

            conn.Close();

            return ht;
        }

        public Hashtable getMSSLMasterNiins() {
            Hashtable ht = new Hashtable();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            String stmt = "SELECT niin FROM mssl_master";

            SqlCommand cmd = new SqlCommand(stmt, conn);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    int qty = 0;
                    ht.Add(rdr["niin"], qty);
                }
            }

            cmd.Dispose();

            conn.Close();

            return ht;
        }

        public FILE_CHANGES validateMSSLChanges(MSSLData data, String shipUIC) {
            FILE_CHANGES result = FILE_CHANGES.Acceptable_Changes;
            int changes = 0;
            int foundRecords = 0;

            // Check for existing inventory
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            try {
                conn.Open();

                String stmt = "SELECT COUNT(*) FROM mssl_inventory WHERE uic = @uic";

                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Parameters.AddWithValue("@uic", shipUIC);

                int invCount = (Int32) cmd.ExecuteScalar();
                int threshold = invCount / 4;

                if (invCount == 0) {
                    // New unit
                    data.MsslStats.NewRecords = data.InventoryList.Count;
                    result = FILE_CHANGES.Initial_Load;
                }
                else {
                    // Count changes from existing inventory
                    Hashtable htExisting = getMSSLQuantities(shipUIC);

                    // Go through the new data counting changes
                    foreach (MsslInventory record in data.InventoryList) {
                        String key = record.Niin + "_" + record.Location;
                        // See if the record exists
                        if (htExisting.ContainsKey(key)) {
                            foundRecords++;
                            // Exists - check qty
                            if ((int) htExisting[key] != record.Qty) {
                                changes++;
                                data.MsslStats.ChangedRecords++;
                            }
                        }
                        else {
                            // New niin
                            changes++;
                            data.MsslStats.NewRecords++;
                        }
                    }

                    changes += (invCount - foundRecords);

                    // check the threshold
                    if (changes > threshold) {
                        result = FILE_CHANGES.Threshold_Exceeded;
                    }
                }
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public bool processMSSLInventory(String uic, MSSLData data, String username) {
            bool result = false;
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            try {
                conn.Open();

                // Get the existing MSSL quantities and mssl_master niins
                Hashtable htExisting = getMSSLQuantities(uic);
                Hashtable htMaster = getMSSLMasterNiins();

                // Go through the new  inventory and update, or insert
                foreach (MsslInventory record in data.InventoryList) {
                    String key = record.Niin + "_" + record.Location;
                    // See if the record exists
                    if (htExisting.ContainsKey(key)) {                        
                        // Exists - update the qty
                        updateMSSLInventory(uic, record, conn, username);
                        htExisting[key] = 1;
                        data.MsslStats.ChangedRecords++;
                    }
                    else {
                        // New niin-location - insert the record
                        insertMSSLInventory(uic, record, conn, username);
                    }

                    // Now see if we need to add to the mssl_master;
                    if (!htMaster.ContainsKey(record.Niin)) {
                        insertMSSLMaster(uic, record, conn, username);
                        // Add the niin to the hashtable so we don't try to add duplicates
                        htMaster.Add(record.Niin, 0);
                        data.MsslStats.NewRecords++;
                    }
                }

                // Delete existing records we haven't updated
                char[] delimiter = {'_'};
                foreach (String key in htExisting.Keys) {
                    if ((Int32) htExisting[key] == 0) {
                        String[] niin_location = key.Split(delimiter);
                        this.deleteMSSLInventory(uic, niin_location[0], niin_location[1],
                            conn, username);
                        data.MsslStats.DeletedRecords++;
                    }
                }

                result = true;
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public bool updateMSSLInventory(String uic, MsslInventory record, SqlConnection conn,
            String username) {
            bool result = false;
            String stmt = "UPDATE mssl_inventory SET Qty = @Qty, changed = @changed, " +
                "changed_by = @changed_by WHERE uic = @uic AND niin = @niin AND location = @location";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@Qty", record.Qty);
                cmd.Parameters.AddWithValue("@changed", DateTime.Now);
                cmd.Parameters.AddWithValue("@changed_by", username);
                cmd.Parameters.AddWithValue("@uic", uic);
                cmd.Parameters.AddWithValue("@niin", record.Niin);
                cmd.Parameters.AddWithValue("@location", record.Location);

                cmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSSLInventory Updated: {0}, {1}, {2}, Qty = {3}",
                    uic, record.Niin, record.Location, record.Qty);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating MSSLInventory: {0}, {1}, {2} - {3}",
                    uic, record.Niin, record.Location, ex.Message);
                throw ex;
            }

            return result;
        }

        public long insertMSSLInventory(String uic, MsslInventory record, SqlConnection conn,
            String username) {
            long result = 0;
            String stmt = "INSERT INTO mssl_inventory (uic, niin, location, qty, created, created_by) " +
                "VALUES(@uic, @niin, @location, @Qty, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@Qty", record.Qty);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);
                insertCmd.Parameters.AddWithValue("@uic", uic);
                insertCmd.Parameters.AddWithValue("@niin", record.Niin);
                insertCmd.Parameters.AddWithValue("@location", record.Location);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("MSSLInventory Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.Niin, record.Location, record.Qty, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting MSSLInventory: {0}, {1}, {2} - {3}",
                    uic, record.Niin, record.Location, ex.Message);
                throw ex;
            }

            return result;
        }

        public bool deleteMSSLInventory(String uic, String niin, String location, 
            SqlConnection conn, String username) {
            bool result = false;
            String stmt = "DELETE FROM mssl_inventory WHERE " +
                "uic = @uic AND niin = @niin AND location = @location";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Transaction = tran;

                cmd.Parameters.AddWithValue("@uic", uic);
                cmd.Parameters.AddWithValue("@niin", niin);
                cmd.Parameters.AddWithValue("@location", location);

                cmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSSLInventory Deleted: {0}, {1}, {2}",
                    uic, niin, location);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleting MSSLInventory: {0}, {1}, {2} - {3}",
                    uic, niin, location, ex.Message);
                throw ex;
            }

            return result;
        }

        public long insertMSSLMaster(String uic, MsslInventory record, SqlConnection conn,
            String username) {
            long result = 0;
            String stmt = "INSERT INTO mssl_master (niin, ati, cog, mcc, ui, up, " +
                "netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, " +
                "data_source_cd, created, created_by) " +
                "VALUES(@niin, @ati, @cog, @mcc, @ui, @up, " +
                "@netup, @lmc, @irc, @dues, @ro, @rp, @amd, @smic, @slc, @slac, @smcc, @nomenclature, " +
                "@data_source_cd, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@niin", record.Niin);
                insertCmd.Parameters.AddWithValue("@ati", dbNull(record.Ati));
                insertCmd.Parameters.AddWithValue("@cog", dbNull(record.Cog));
                insertCmd.Parameters.AddWithValue("@mcc", dbNull(record.Mcc));
                insertCmd.Parameters.AddWithValue("@ui", dbNull(record.Ui));
                insertCmd.Parameters.AddWithValue("@up", dbNull(record.Up));
                insertCmd.Parameters.AddWithValue("@netup", dbNull(record.Netup));
                insertCmd.Parameters.AddWithValue("@lmc", dbNull(record.Lmc));
                insertCmd.Parameters.AddWithValue("@irc", dbNull(record.Irc));
                insertCmd.Parameters.AddWithValue("@dues", dbNull(record.Dues));
                insertCmd.Parameters.AddWithValue("@ro", dbNull(record.Ro));
                insertCmd.Parameters.AddWithValue("@rp", dbNull(record.Rp));
                insertCmd.Parameters.AddWithValue("@amd", dbNull(record.Amd));
                insertCmd.Parameters.AddWithValue("@smic", dbNull(record.Smic));
                insertCmd.Parameters.AddWithValue("@slc", dbNull(record.Slc));
                insertCmd.Parameters.AddWithValue("@slac", dbNull(record.Slac));
                insertCmd.Parameters.AddWithValue("@smcc", dbNull(record.Smcc));
                insertCmd.Parameters.AddWithValue("@nomenclature", dbNull(record.Nomenclature));
                insertCmd.Parameters.AddWithValue("@data_source_cd", uic);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("MSSLMaster Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.Niin, record.Nomenclature, record.Smcc, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting MSSLMaster: {0}, {1}, {2} - {3}",
                    uic, record.Niin, record.Location, ex.Message);
                throw ex;
            }

            return result;
        }
    #endregion

    #region Authorized Use List
        #region AUL Gets
        public AULRecord getAULMasterRecord(String niin, long hull_type_id, long aul_id = -1) {
            AULRecord record = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            StringBuilder sqlStmt = new StringBuilder("SELECT aul.*, cg.group_name AS Catalog_Group, ");
            sqlStmt.Append("cog.cog, ht.hull_type AS Hull_Type, ");
            sqlStmt.Append("slc.slc AS Shelf_Life_Code, slac.slac AS Shelf_Life_Action_Code, ");
            sqlStmt.Append("smcc.smcc, st.type AS Storage_Type, uc.category AS Usage_Category ");
            sqlStmt.Append("FROM auth_use_list aul ");
            sqlStmt.Append("LEFT JOIN catalog_groups cg ON cg.catalog_group_id = aul.catalog_group_id ");
            sqlStmt.Append("LEFT JOIN cog_codes cog ON cog.cog_id = aul.cog_id ");
            sqlStmt.Append("LEFT JOIN hull_types ht ON ht.hull_type_id = aul.hull_type_id ");
            sqlStmt.Append("LEFT JOIN shelf_life_code slc ON slc.shelf_life_code_id = aul.shelf_life_code_id ");
            sqlStmt.Append("LEFT JOIN shelf_life_action_code slac ON slac.shelf_life_action_code_id = aul.shelf_life_action_code_id ");
            sqlStmt.Append("LEFT JOIN smcc ON smcc.smcc_id = aul.smcc_id ");
            sqlStmt.Append("LEFT JOIN storage_type st ON st.storage_type_id = aul.storage_type_id ");
            sqlStmt.Append("LEFT JOIN usage_category uc ON uc.usage_category_id = aul.usage_category_id ");

            if (aul_id > 0) {
                sqlStmt.Append("WHERE aul.aul_id = @aul_id ");
            }
            else {
                sqlStmt.Append("WHERE aul.niin = @niin ");
            }

            sqlStmt.Append("AND aul.hull_type_id = @hull_type_id");

            try {
                SqlCommand cmd = new SqlCommand(sqlStmt.ToString(), conn);

                //get the row
                if (aul_id > 0) {
                    cmd.Parameters.AddWithValue("@aul_id", aul_id);
                }
                else {
                    cmd.Parameters.AddWithValue("@niin", niin);
                }

                cmd.Parameters.AddWithValue("@hull_type_id", hull_type_id);

                SqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read()) {
                    record = new AULRecord();
                    int temp_id = -1;
                    DateTime tempDate;

                    int.TryParse(rdr["aul_id"].ToString(), out temp_id);
                    record.aul_id = temp_id;
                    record.aac = rdr["aac"].ToString();
                    int.TryParse(rdr["allowance_qty"].ToString(), out temp_id);
                    record.allowance_qty = temp_id;
                    int.TryParse(rdr["aul_id"].ToString(), out temp_id);
                    record.aul_id = temp_id;
                    record.cage = rdr["cage"].ToString();
                    int.TryParse(rdr["catalog_group_id"].ToString(), out temp_id);
                    record.catalog_group_id = temp_id;
                    record.Catalog_Group = rdr["catalog_group"].ToString();
                    record.catalog_serial_number = rdr["catalog_serial_number"].ToString();
                    int.TryParse(rdr["cog_id"].ToString(), out temp_id);
                    record.cog_id = temp_id;
                    record.COG = rdr["COG"].ToString();
                    record.data_source_cd = rdr["data_source_cd"].ToString();
                    record.description = rdr["description"].ToString();
                    bool bTemp = false;
                    bool.TryParse(rdr["dropped_in_error"].ToString(), out bTemp); 
                    record.dropped_in_error = bTemp;
                    int.TryParse(rdr["fsc"].ToString(), out temp_id);
                    record.fsc = temp_id;
                    int.TryParse(rdr["hull_type_id"].ToString(), out temp_id);
                    record.hull_type_id = temp_id;
                    record.Hull_Type = rdr["hull_type"].ToString();
                    bool.TryParse(rdr["manually_entered"].ToString(), out bTemp);
                    record.manually_entered = bTemp;
                    record.manufacturer = rdr["manufacturer"].ToString();
                    record.msds_num = rdr["msds_num"].ToString();
                    record.nehc_rpt = rdr["nehc_rpt"].ToString();
                    record.niin = rdr["niin"].ToString();
                    record.price_per_ui = rdr["price_per_ui"].ToString();
                    record.qup = rdr["qup"].ToString();
                    record.remarks = rdr["remarks"].ToString();
                    int.TryParse(rdr["shelf_life_code_id"].ToString(), out temp_id);
                    record.shelf_life_code_id = temp_id;
                    record.Shelf_Life_Code = rdr["Shelf_Life_Code"].ToString();
                    int.TryParse(rdr["shelf_life_action_code_id"].ToString(), out temp_id);
                    record.shelf_life_action_code_id = temp_id;
                    record.Shelf_Life_Action_Code = rdr["Shelf_Life_Action_Code"].ToString();
                    int.TryParse(rdr["smcc_id"].ToString(), out temp_id);
                    record.smcc_id = temp_id;
                    record.SMCC = rdr["SMCC"].ToString();
                    record.smic = rdr["smic"].ToString();
                    record.specs = rdr["specs"].ToString();
                    record.spmig = rdr["spmig"].ToString();
                    int.TryParse(rdr["storage_type_id"].ToString(), out temp_id);
                    record.storage_type_id = temp_id;
                    record.Storage_Type = rdr["Storage_Type"].ToString();
                    record.ui = rdr["ui"].ToString();
                    record.um = rdr["um"].ToString();
                    int.TryParse(rdr["usage_category_id"].ToString(), out temp_id);
                    record.usage_category_id = temp_id;
                    record.Usage_Category = rdr["Usage_Category"].ToString();
                    DateTime.TryParse(rdr["created"].ToString(), out tempDate);
                    record.created = tempDate;
                    if (rdr["Created_By"] != DBNull.Value)
                        record.created_by = rdr["Created_By"].ToString();
                    DateTime.TryParse(rdr["changed"].ToString(), out tempDate);
                    record.changed = tempDate;
                    if (rdr["Changed_By"] != DBNull.Value)
                        record.changed_by = rdr["Changed_by"].ToString();

                    rdr.Close();
                    cmd.Dispose();
                }
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error getting AULMaster: {0}, {1}, {2} - {3}",
                    record.niin, record.hull_type_id, ex.Message);
            }
            finally {
                conn.Close();
            }

            return record;
        }

        public Hashtable getAULNIINsForHullTypeId(long hull_type_id) {
            Hashtable ht = new Hashtable();
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            conn.Open();

            String stmt = "SELECT niin, aul_id FROM auth_use_list WHERE hull_type_id = @hull_type_id";

            SqlCommand cmd = new SqlCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@hull_type_id", hull_type_id);

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read()) {
                ht.Add(rdr["niin"], rdr["aul_id"]);
            }

            cmd.Dispose();
            conn.Close();

            return ht;
        }

        public int countAULRecords(long hull_type_id) {
            int result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);

            try {
                conn.Open();

                String stmt = "SELECT COUNT(*) FROM auth_use_list WHERE hull_type_id = @hull_type_id";

                SqlCommand cmd = new SqlCommand(stmt, conn);
                cmd.Parameters.AddWithValue("@hull_type_id", hull_type_id);

                result = (Int32)cmd.ExecuteScalar();
            }
            finally {
                conn.Close();
            }

            return result;
        }

        #endregion

        #region AULInsert
        public long insertAULMasterRecord(String uic, AULRecord record, String username) {
            long result = 0;
            
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            try {
                conn.Open();
                result = insertAULMasterRecord(uic, record, conn, username);
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long insertAULMasterRecord(String uic, AULRecord record, SqlConnection conn, String username) {
            long result = 0;

            // Check to make sure we have the ID values - just in case
            if (record.hull_type_id == 0) {
                record.hull_type_id = getIDFromValue(
                    "Hull_Types", "hull_type_id", "hull_type", record.Hull_Type, conn);
            }
            if (record.usage_category_id == 0) {
                record.usage_category_id = getIDFromValue(
                    "Usage_Category", "usage_category_id", "category", record.Usage_Category, conn);
            }
            if (record.smcc_id == 0) {
                record.smcc_id = getIDFromValue(
                    "SMCC", "smcc_id", "smcc", record.SMCC, conn);
            }
            if (record.shelf_life_code_id == 0) {
                record.shelf_life_code_id = getIDFromValue(
                    "Shelf_Life_Code", "shelf_life_code_id", "slc", record.Shelf_Life_Code, conn);
            }
            if (record.shelf_life_action_code_id == 0) {
                record.shelf_life_action_code_id = getIDFromValue(
                    "Shelf_Life_Action_Code", "shelf_life_action_code_id", "slac", record.Shelf_Life_Action_Code, conn);
            }
            if (record.storage_type_id == 0) {
                record.storage_type_id = getIDFromValue(
                    "Storage_Type", "storage_type_id", "type", record.Storage_Type, conn);
            }
            if (record.cog_id == 0) {
                record.cog_id = getIDFromValue(
                    "COG_Codes", "cog_id", "cog", record.COG, conn);
            }
            if (record.catalog_group_id == 0) {
                record.catalog_group_id = getIDFromValue(
                    "Catalog_Groups", "catalog_group_id", "group_name", record.Catalog_Group, conn);
            }

            String stmt = "INSERT INTO auth_use_list (hull_type_id, fsc, niin, ui, um, usage_category_id, " +
                "description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, " +
                "remarks, storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, " +
                "catalog_serial_number, allowance_qty, manually_entered, dropped_in_error, " +
                "manufacturer, cage, msds_num, " +
                "aac, qup, price_per_ui, smic, " +
                "data_source_cd, created, created_by) " +
                "VALUES(@hull_type_id, @fsc, @niin, @ui, @um, @usage_category_id, " +
                "@description, @smcc_id, @specs, @shelf_life_code_id, @shelf_life_action_code_id, " +
                "@remarks, @storage_type_id, @cog_id, @spmig, @nehc_rpt, @catalog_group_id, " +
                "@catalog_serial_number, @allowance_qty, @manually_entered, @dropped_in_error, " +
                "@manufacturer, @cage, @msds_num, " + 
                "@aac, @qup, @price_per_ui, @smic, " +
                "@data_source_cd, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@hull_type_id", record.hull_type_id);
                insertCmd.Parameters.AddWithValue("@niin", record.niin);
                insertCmd.Parameters.AddWithValue("@fsc", dbNull(record.fsc));
                insertCmd.Parameters.AddWithValue("@ui", dbNull(record.ui));
                insertCmd.Parameters.AddWithValue("@um", dbNull(record.um));
                insertCmd.Parameters.AddWithValue("@usage_category_id", dbNull(record.usage_category_id));
                insertCmd.Parameters.AddWithValue("@description", dbNull(record.description));
                insertCmd.Parameters.AddWithValue("@smcc_id", dbNull(record.smcc_id));
                insertCmd.Parameters.AddWithValue("@specs", dbNull(record.specs));
                insertCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(record.shelf_life_code_id));
                insertCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(record.shelf_life_action_code_id));
                insertCmd.Parameters.AddWithValue("@remarks", dbNull(record.remarks));
                insertCmd.Parameters.AddWithValue("@storage_type_id", dbNull(record.storage_type_id));
                insertCmd.Parameters.AddWithValue("@cog_id", dbNull(record.cog_id));
                insertCmd.Parameters.AddWithValue("@spmig", dbNull(record.spmig));
                insertCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(record.nehc_rpt));
                insertCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(record.catalog_group_id));
                insertCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(record.catalog_serial_number));
                insertCmd.Parameters.AddWithValue("@allowance_qty", dbNull(record.allowance_qty));
                insertCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.manually_entered));
                insertCmd.Parameters.AddWithValue("@dropped_in_error", dbNull(record.dropped_in_error));
                insertCmd.Parameters.AddWithValue("@manufacturer", dbNull(record.manufacturer));
                insertCmd.Parameters.AddWithValue("@cage", dbNull(record.cage));
                insertCmd.Parameters.AddWithValue("@msds_num", dbNull(record.msds_num));
                insertCmd.Parameters.AddWithValue("@data_source_cd", uic);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);
                insertCmd.Parameters.AddWithValue("@aac", dbNull(record.aac));
                insertCmd.Parameters.AddWithValue("@qup", dbNull(record.qup));
                insertCmd.Parameters.AddWithValue("@price_per_ui", dbNull(record.price_per_ui));
                insertCmd.Parameters.AddWithValue("@smic", dbNull(record.smic));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("AULMaster Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.niin, record.description, record.hull_type_id, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting AULMaster: {0}, {1}, {2} - {3}",
                    uic, record.niin, record.hull_type_id, ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion

        #region AULDelete
        public long deleteAULMasterRecord(long aul_id, String userName) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            try {
                conn.Open();
                result = deleteAULMasterRecord(aul_id, conn, userName);
            }
            finally {
                conn.Close();
            }

            return result;
        }
        public long deleteAULMasterRecord(long aul_id, SqlConnection conn, String userName) {
            long result = 0;

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM auth_use_list " +
                "WHERE aul_id = @aul_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@aul_id", aul_id);

                result = deleteCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("Deleted AUL record for : ID = {0}",
                    aul_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deletingAULRecord for ID = {0} - {1}",
                    aul_id, ex.Message);
                throw ex;
            }

            return result;
        }

        public long deleteAULRecordsForHullType(long hull_type_id, String userName) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            string sqlStmt = "DELETE FROM auth_use_list " +
                "WHERE hull_type_id = @hull_type_id";

            try {
                SqlCommand deleteCmd = new SqlCommand(sqlStmt, conn);
                deleteCmd.Transaction = tran;

                //insert rows
                deleteCmd.Parameters.AddWithValue("@hull_type_id", hull_type_id);

                object o = deleteCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                deleteCmd.Parameters.Clear();

                tran.Commit();

                logger.InfoFormat("Deleted AUL records for HullType: ID = {0}",
                    hull_type_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleteAULRecordsForHullType for ID = {0} - {1}",
                    hull_type_id, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
        #endregion

        #region AUL Update
        public long updateAULMasterRecord(String uic, AULRecord record, String username) {
            long result = 0;
            
            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            try {
                conn.Open();
                result = updateAULMasterRecord(uic, record, conn, username);
            }
            finally {
                conn.Close();
            }

            return result;
        }

        public long updateAULMasterRecord(String uic, AULRecord record, SqlConnection conn,
            String username) {
            long result = 0;

            // Check to make sure we have the ID values - just in case
            if (record.hull_type_id == 0) {
                record.hull_type_id = getIDFromValue(
                    "Hull_Types", "hull_type_id", "hull_type", record.Hull_Type, conn);
            }
            if (record.usage_category_id == 0) {
                record.usage_category_id = getIDFromValue(
                    "Usage_Category", "usage_category_id", "category", record.Usage_Category, conn);
            }
            if (record.smcc_id == 0) {
                record.smcc_id = getIDFromValue(
                    "SMCC", "smcc_id", "smcc", record.SMCC, conn);
            }
            if (record.shelf_life_code_id == 0) {
                record.shelf_life_code_id = getIDFromValue(
                    "Shelf_Life_Code", "shelf_life_code_id", "slc", record.Shelf_Life_Code, conn);
            }
            if (record.shelf_life_action_code_id == 0) {
                record.shelf_life_action_code_id = getIDFromValue(
                    "Shelf_Life_Action_Code", "shelf_life_action_code_id", "slac", record.Shelf_Life_Action_Code, conn);
            }
            if (record.storage_type_id == 0) {
                record.storage_type_id = getIDFromValue(
                    "Storage_Type", "storage_type_id", "type", record.Storage_Type, conn);
            }
            if (record.cog_id == 0) {
                record.cog_id = getIDFromValue(
                    "COG_Codes", "cog_id", "cog", record.COG, conn);
            }
            if (record.catalog_group_id == 0) {
                record.catalog_group_id = getIDFromValue(
                    "Catalog_Groups", "catalog_group_id", "group_name", record.Catalog_Group, conn);
            }

            String stmt = "UPDATE auth_use_list SET hull_type_id = @hull_type_id, fsc = @fsc, niin = @niin, " +
                "ui = @ui, um = @um, usage_category_id = @usage_category_id, description = @description, " +
                "smcc_id = @smcc_id, specs = @specs, shelf_life_code_id = @shelf_life_code_id, " +
                "shelf_life_action_code_id = @shelf_life_action_code_id, " +
                "remarks = @remarks, storage_type_id = @storage_type_id, cog_id = @cog_id, spmig = @spmig, " +
                "nehc_rpt = @nehc_rpt, catalog_group_id = @catalog_group_id, " +
                "catalog_serial_number = @catalog_serial_number, allowance_qty = @allowance_qty, " +
                "manually_entered = @manually_entered, dropped_in_error = @dropped_in_error, " +
                "manufacturer = @manufacturer, cage = @cage, msds_num = @msds_num, " +
                "aac = @aac, qup = @qup, price_per_ui = @price_per_ui, smic = @smic, " +
                "data_source_cd = @data_source_cd, changed = @changed, changed_by = @changed_by " +
                "WHERE aul_id = @aul_id ";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@hull_type_id", record.hull_type_id);
                updateCmd.Parameters.AddWithValue("@niin", record.niin);
                updateCmd.Parameters.AddWithValue("@fsc", dbNull(record.fsc));
                updateCmd.Parameters.AddWithValue("@ui", dbNull(record.ui));
                updateCmd.Parameters.AddWithValue("@um", dbNull(record.um));
                updateCmd.Parameters.AddWithValue("@usage_category_id", dbNull(record.usage_category_id));
                updateCmd.Parameters.AddWithValue("@description", dbNull(record.description));
                updateCmd.Parameters.AddWithValue("@smcc_id", dbNull(record.smcc_id));
                updateCmd.Parameters.AddWithValue("@specs", dbNull(record.specs));
                updateCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(record.shelf_life_code_id));
                updateCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(record.shelf_life_action_code_id));
                updateCmd.Parameters.AddWithValue("@remarks", dbNull(record.remarks));
                updateCmd.Parameters.AddWithValue("@storage_type_id", dbNull(record.storage_type_id));
                updateCmd.Parameters.AddWithValue("@cog_id", dbNull(record.cog_id));
                updateCmd.Parameters.AddWithValue("@spmig", dbNull(record.spmig));
                updateCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(record.nehc_rpt));
                updateCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(record.catalog_group_id));
                updateCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(record.catalog_serial_number));
                updateCmd.Parameters.AddWithValue("@allowance_qty", dbNull(record.allowance_qty));
                updateCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.manually_entered));
                updateCmd.Parameters.AddWithValue("@dropped_in_error", dbNull(record.dropped_in_error));
                updateCmd.Parameters.AddWithValue("@manufacturer", dbNull(record.manufacturer));
                updateCmd.Parameters.AddWithValue("@cage", dbNull(record.cage));
                updateCmd.Parameters.AddWithValue("@msds_num", dbNull(record.msds_num));
                updateCmd.Parameters.AddWithValue("@data_source_cd", uic);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", username);
                updateCmd.Parameters.AddWithValue("@aul_id", record.aul_id);
                updateCmd.Parameters.AddWithValue("@aac", dbNull(record.aac));
                updateCmd.Parameters.AddWithValue("@qup", dbNull(record.qup));
                updateCmd.Parameters.AddWithValue("@price_per_ui", dbNull(record.price_per_ui));
                updateCmd.Parameters.AddWithValue("@smic", dbNull(record.smic));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("AULMaster updated: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.niin, record.description, record.hull_type_id, record.aul_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating AULMaster: {0}, {1}, {2} - {3}",
                    uic, record.niin, record.hull_type_id, ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion
    #endregion

    #region MSDS
        #region MSDS Gets
        public MSDSRecord getMSDSMasterRecord(long msds_id) {
            MSDSRecord record = null;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string sqlStmt = "SELECT m.*, hcc.HCC FROM msds_master m " +
                "JOIN hcc ON hcc.hcc_id = m.hcc_id " +
                "WHERE msds_id = @msds_id";

            try {
                SqlCommand cmd = new SqlCommand(sqlStmt, conn);

                //get the row
                cmd.Parameters.AddWithValue("@msds_id", msds_id);

                using (SqlDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDSRecord();
                        int temp_id = -1;
                        DateTime tempDate;

                        // Master data
                        record.msds_id = msds_id;
                        int.TryParse(rdr["hcc_id"].ToString(), out temp_id);
                        record.hcc_id = temp_id;
                        record.ARTICLE_IND = rdr["article_ind"].ToString();
                        record.CAGE = rdr["cage"].ToString();
                        record.data_source_cd = rdr["data_source_cd"].ToString();
                        record.DESCRIPTION = rdr["description"].ToString();
                        record.EMERGENCY_TEL = rdr["emergency_tel"].ToString();
                        record.END_COMP_IND = rdr["end_comp_ind"].ToString();
                        record.END_ITEM_IND = rdr["end_item_ind"].ToString();
                        record.file_name = rdr["file_name"].ToString();
                        record.HCC = rdr["hcc"].ToString();
                        record.KIT_IND = rdr["kit_ind"].ToString();
                        record.KIT_PART_IND = rdr["kit_part_ind"].ToString();
                        record.manually_entered = Convert.ToBoolean(rdr["manually_entered"].ToString());
                        record.MANUFACTURER = rdr["manufacturer"].ToString();
                        record.MANUFACTURER_MSDS_NO = rdr["manufacturer_msds_no"].ToString();
                        record.MIXTURE_IND = rdr["mixture_ind"].ToString();
                        record.MSDSSERNO = rdr["msdsserno"].ToString();
                        record.NIIN = rdr["niin"].ToString();
                        record.PARTNO = rdr["partno"].ToString();
                        record.PRODUCT_IDENTITY = rdr["product_identity"].ToString();
                        record.PRODUCT_IND = rdr["product_ind"].ToString();
                        record.PRODUCT_LANGUAGE = rdr["product_language"].ToString();
                        DateTime.TryParse(rdr["PRODUCT_LOAD_DATE"].ToString(), out tempDate);
                        record.PRODUCT_LOAD_DATE = tempDate;
                        record.PRODUCT_RECORD_STATUS = rdr["product_record_status"].ToString();
                        record.PRODUCT_REVISION_NO = rdr["product_revision_no"].ToString();
                        record.PROPRIETARY_IND = rdr["proprietary_ind"].ToString();
                        record.PUBLISHED_IND = rdr["published_ind"].ToString();
                        record.PURCHASED_PROD_IND = rdr["purchased_prod_ind"].ToString();
                        record.PURE_IND = rdr["pure_ind"].ToString();
                        record.RADIOACTIVE_IND = rdr["radioactive_ind"].ToString();
                        record.TRADE_SECRET_IND = rdr["trade_secret_ind"].ToString();
                        record.SERVICE_AGENCY_CODE = rdr["service_agency_code"].ToString();
                        record.TRADE_NAME = rdr["trade_name"].ToString();
                        record.TRADE_SECRET_IND = rdr["trade_secret_ind"].ToString();
                        DateTime.TryParse(rdr["created"].ToString(), out tempDate);
                        record.created = tempDate;
                        if (rdr["Created_By"] != DBNull.Value)
                            record.created_by = rdr["Created_By"].ToString();
                        DateTime.TryParse(rdr["changed"].ToString(), out tempDate);
                        record.changed = tempDate;
                        if (rdr["Changed_By"] != DBNull.Value)
                            record.changed_by = rdr["Changed_by"].ToString();

                        rdr.Close();

                        // Get the subtable data
                        populateMSDSSubTableData(record, conn);
                    }
                }

                cmd.Dispose();
            }
            finally {
                conn.Close();
            }

            return record;
        }

        private void populateMSDSSubTableData(MSDSRecord record, SqlConnection conn) {
            //MSDS CONTRACTOR LIST
            record.ContractorList = getMSDSContractorList(record.msds_id, conn);

            //MSDS DISPOSAL
            record.Disposal = getMSDSDisposalRecord(record.msds_id, conn);

            //MSDS DOC TYPES
            record.DocumentTypes = getMSDSDocumentTypesRecord(record.msds_id, conn);

            // MSDS INGREDIENTS
            record.IngredientsList = getMSDSIngredientsList(record.msds_id, conn);

            //MSDS ITEM DESCRIPTION
            record.ItemDescription = getMSDSItemDescriptionRecord(record.msds_id, conn);

            //MSDS LABEL INFO
            record.LabelInfo = getMSDSLabelInfoRecord(record.msds_id, conn);

            //MSDS PHYS CHEMICAL TABLE
            record.PhysChemical = getMSDSPhysChemicalRecord(record.msds_id, conn);

            //MSDS RADIOLOGICAL INFO
            record.RadiologicalInfoList = getMSDSRadiologicalInfoRecord(record.msds_id, conn);

            //MSDS TRANSPORTATION
            record.Transportation = getMSDSTransportationRecord(record.msds_id, conn);

            // AFJM_PSN
            record.Afjm_Psn = getMSDSAFJM_PSNRecord(record.msds_id, conn);

            //MSDS DOT PSN
            record.Dot_Psn = getMSDSDOT_PSNRecord(record.msds_id, conn);

            //MSDS IATA PSN
            record.Iata_Psn = getMSDSIATA_PSNRecord(record.msds_id, conn);

            //MSDS IMO PSN
            record.Imo_Psn = getMSDSIMO_PSNRecord(record.msds_id, conn);
        }

        private MSDS_AFJM_PSN getMSDSAFJM_PSNRecord(long msds_id, SqlConnection conn) {
            MSDS_AFJM_PSN record = null;

            string sqlStmt = "SELECT * FROM MSDS_AFJM_PSN WHERE afjm_psn_id = "
                + "(SELECT afjm_psn_id FROM msds_transportation WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDS_AFJM_PSN();

                    record.AFJM_HAZARD_CLASS = rdr["AFJM_HAZARD_CLASS"].ToString();
                    record.AFJM_PACK_GROUP = rdr["AFJM_PACK_GROUP"].ToString();
                    record.AFJM_PACK_PARAGRAPH = rdr["AFJM_PACK_PARAGRAPH"].ToString();
                    record.AFJM_PROP_SHIP_MODIFIER = rdr["AFJM_PROP_SHIP_MODIFIER"].ToString();
                    record.AFJM_PROP_SHIP_NAME = rdr["AFJM_PROP_SHIP_NAME"].ToString();
                    record.AFJM_PSN_CODE = rdr["AFJM_PSN_CODE"].ToString();
                    record.AFJM_SPECIAL_PROV = rdr["AFJM_SPECIAL_PROV"].ToString();
                    record.AFJM_SUBSIDIARY_RISK = rdr["AFJM_SUBSIDIARY_RISK"].ToString();
                    record.AFJM_SYMBOLS = rdr["AFJM_SYMBOLS"].ToString();
                    record.AFJM_UN_ID_NUMBER = rdr["AFJM_UN_ID_NUMBER"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private List<MSDSContractorInfo> getMSDSContractorList(long msds_id, SqlConnection conn) {
            List<MSDSContractorInfo> list = new List<MSDSContractorInfo>();

            string sqlStmt = "SELECT ct.*, con.* FROM msds_contracts ct "
                + "JOIN msds_contractor_info con ON con.contractor_id = ct.contractor_id "
                + "WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    // Create the record
                    MSDSContractorInfo record = new MSDSContractorInfo();

                    record.CT_CAGE = rdr["CT_CAGE"].ToString();
                    record.CT_ADDRESS_1 = rdr["CT_ADDRESS_!"].ToString();
                    record.CT_CITY = rdr["CT_CITY"].ToString();
                    record.CT_COMPANY_NAME = rdr["CT_COMPANY_NAME"].ToString();
                    record.CT_COUNTRY = rdr["CT_COUNTRY"].ToString();
                    record.CT_NUMBER = rdr["CT_NUMBER"].ToString();
                    record.CT_PHONE = rdr["CT_PHONE"].ToString();
                    record.CT_PO_BOX = rdr["CT_PO_BOX"].ToString();
                    record.CT_STATE = rdr["CT_STATE"].ToString();
                    record.CT_ZIP_CODE = rdr["ZIP_CODE"].ToString();
                    record.PURCHASE_ORDER_NO = rdr["PURCHASE_ORDER_NO"].ToString();

                    // Add it to the list
                    list.Add(record);
                }
            }

            cmd.Dispose();

            return list;
        }

        private MSDSDisposal getMSDSDisposalRecord(long msds_id, SqlConnection conn) {
            MSDSDisposal record = null;

            string sqlStmt = "SELECT * FROM msds_disposal WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDSDisposal();

                    record.DISPOSAL_ADD_INFO = rdr["DISPOSAL_ADD_INFO"].ToString();
                    record.EPA_HAZ_WASTE_CODE = rdr["EPA_HAZ_WASTE_CODE"].ToString();
                    record.EPA_HAZ_WASTE_IND = rdr["EPA_HAZ_WASTE_IND"].ToString();
                    record.EPA_HAZ_WASTE_NAME = rdr["EPA_HAZ_WASTE_NAME"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private MSDSDocumentTypes getMSDSDocumentTypesRecord(long msds_id, SqlConnection conn) {
            MSDSDocumentTypes record = null;

            string sqlStmt = "SELECT * FROM msds_document_types WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDSDocumentTypes();

                    record.manufacturer_label_filename = rdr["manufacturer_label_filename"].ToString();
                    record.msds_translated_filename = rdr["msds_translated_filename"].ToString();
                    record.neshap_comp_filename = rdr["neshap_comp_filename"].ToString();
                    record.other_docs_filename = rdr["other_docs_filename"].ToString();
                    record.product_sheet_filename = rdr["product_sheet_filename"].ToString();
                    record.transportation_cert_filename = rdr["transportation_cert_filename"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private MSDS_DOT_PSN getMSDSDOT_PSNRecord(long msds_id, SqlConnection conn) {
            MSDS_DOT_PSN record = null;

            string sqlStmt = "SELECT * FROM MSDS_DOT_PSN WHERE dot_psn_id = "
                + "(SELECT dot_psn_id FROM msds_transportation WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDS_DOT_PSN();

                    record.DOT_HAZARD_CLASS_DIV = rdr["DOT_HAZARD_CLASS_DIV"].ToString();
                    record.DOT_HAZARD_LABEL = rdr["DOT_HAZARD_LABEL"].ToString();
                    record.DOT_MAX_CARGO = rdr["DOT_MAX_CARGO"].ToString();
                    record.DOT_MAX_PASSENGER = rdr["DOT_MAX_PASSENGER"].ToString();
                    record.DOT_PACK_BULK = rdr["DOT_PACK_BULK"].ToString();
                    record.DOT_PACK_EXCEPTIONS = rdr["DOT_PACK_EXCEPTIONS"].ToString();
                    record.DOT_PACK_GROUP = rdr["DOT_PACK_GROUP"].ToString();
                    record.DOT_PACK_NONBULK = rdr["DOT_PACK_NONBULK"].ToString();
                    record.DOT_PROP_SHIP_MODIFIER = rdr["DOT_PROP_SHIP_MODIFIER"].ToString();
                    record.DOT_PROP_SHIP_NAME = rdr["DOT_PROP_SHIP_NAME"].ToString();
                    record.DOT_PSN_CODE = rdr["DOT_PSN_CODE"].ToString();
                    record.DOT_SPECIAL_PROVISION = rdr["DOT_SPECIAL_PROVISION"].ToString();
                    record.DOT_SYMBOLS = rdr["DOT_SYMBOLS"].ToString();
                    record.DOT_UN_ID_NUMBER = rdr["DOT_UN_ID_NUMBER"].ToString();
                    record.DOT_WATER_OTHER_REQ = rdr["DOT_WATER_OTHER_REQ"].ToString();
                    record.DOT_WATER_VESSEL_STOW = rdr["DOT_WATER_VESSEL_STOW"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private MSDS_IATA_PSN getMSDSIATA_PSNRecord(long msds_id, SqlConnection conn) {
            MSDS_IATA_PSN record = null;

            string sqlStmt = "SELECT * FROM MSDS_IATA_PSN WHERE iata_psn_id = "
                + "(SELECT iata_psn_id FROM msds_transportation WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDS_IATA_PSN();

                    record.IATA_CARGO_PACK_MAX_QTY = rdr["IATA_CARGO_PACK_MAX_QTY"].ToString();
                    record.IATA_CARGO_PACKING = rdr["IATA_CARGO_PACKING"].ToString();
                    record.IATA_HAZARD_CLASS = rdr["IATA_HAZARD_CLASS"].ToString();
                    record.IATA_HAZARD_LABEL = rdr["IATA_HAZARD_LABEL"].ToString();
                    record.IATA_PACK_GROUP = rdr["IATA_PACK_GROUP"].ToString();
                    record.IATA_PASS_AIR_MAX_QTY = rdr["IATA_PASS_AIR_MAX_QTY"].ToString();
                    record.IATA_PASS_AIR_PACK_LMT_INSTR = rdr["IATA_PASS_AIR_PACK_LMT_INSTR"].ToString();
                    record.IATA_PASS_AIR_PACK_LMT_PER_PKG = rdr["IATA_PASS_AIR_PACK_LMT_PER_PKG"].ToString();
                    record.IATA_PASS_AIR_PACK_NOTE = rdr["IATA_PASS_AIR_PACK_NOTE"].ToString();
                    record.IATA_PROP_SHIP_MODIFIER = rdr["IATA_PROP_SHIP_MODIFIER"].ToString();
                    record.IATA_PROP_SHIP_NAME = rdr["IATA_PROP_SHIP_NAME"].ToString();
                    record.IATA_PSN_CODE = rdr["IATA_PSN_CODE"].ToString();
                    record.IATA_SPECIAL_PROV = rdr["IATA_SPECIAL_PROV"].ToString();
                    record.IATA_SUBSIDIARY_RISK = rdr["IATA_SUBSIDIARY_RISK"].ToString();
                    record.IATA_UN_ID_NUMBER = rdr["IATA_UN_ID_NUMBER"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private MSDS_IMO_PSN getMSDSIMO_PSNRecord(long msds_id, SqlConnection conn) {
            MSDS_IMO_PSN record = null;

            string sqlStmt = "SELECT * FROM MSDS_IMO_PSN WHERE imo_psn_id = "
                + "(SELECT imo_psn_id FROM msds_transportation WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDS_IMO_PSN();

                    record.IMO_EMS_NO = rdr["IMO_EMS_NO"].ToString();
                    record.IMO_HAZARD_CLASS = rdr["IMO_HAZARD_CLASS"].ToString();
                    record.IMO_IBC_INSTR = rdr["IMO_IBC_INSTR"].ToString();
                    record.IMO_IBC_PROVISIONS = rdr["IMO_IBC_PROVISIONS"].ToString();
                    record.IMO_LIMITED_QTY = rdr["IMO_LIMITED_QTY"].ToString();
                    record.IMO_PACK_GROUP = rdr["IMO_PACK_GROUP"].ToString();
                    record.IMO_PACK_INSTRUCTIONS = rdr["IMO_PACK_INSTRUCTIONS"].ToString();
                    record.IMO_PACK_PROVISIONS = rdr["IMO_PACK_PROVISIONS"].ToString();
                    record.IMO_PROP_SHIP_MODIFIER = rdr["IMO_PROP_SHIP_MODIFIER"].ToString();
                    record.IMO_PROP_SHIP_NAME = rdr["IMO_PROP_SHIP_NAME"].ToString();
                    record.IMO_PSN_CODE = rdr["IMO_PSN_CODE"].ToString();
                    record.IMO_SPECIAL_PROV = rdr["IMO_SPECIAL_PROV"].ToString();
                    record.IMO_STOW_SEGR = rdr["IMO_STOW_SEGR"].ToString();
                    record.IMO_SUBSIDIARY_RISK = rdr["IMO_SUBSIDIARY_RISK"].ToString();
                    record.IMO_TANK_INSTR_IMO = rdr["IMO_TANK_INSTR_IMO"].ToString();
                    record.IMO_TANK_INSTR_PROV = rdr["IMO_TANK_INSTR_PROV"].ToString();
                    record.IMO_TANK_INSTR_UN = rdr["IMO_TANK_INSTR_UN"].ToString();
                    record.IMO_UN_NUMBER = rdr["IMO_UN_NUMBER"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private List<MSDSIngredient> getMSDSIngredientsList(long msds_id, SqlConnection conn) {
            List<MSDSIngredient> list = new List<MSDSIngredient>();

            string sqlStmt = "SELECT * FROM msds_ingredients WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    // Create the record
                    MSDSIngredient record = new MSDSIngredient();

                    record.ACGIH_STEL = rdr["ACGIH_STEL"].ToString();
                    record.ACGIH_TLV = rdr["ACGIH_TLV"].ToString();
                    record.CAS = rdr["CAS"].ToString();
                    record.CHEM_MFG_COMP_NAME = rdr["CHEM_MFG_COMP_NAME"].ToString();
                    record.DOT_REPORT_QTY = rdr["DOT_REPORT_QTY"].ToString();
                    record.EPA_REPORT_QTY = rdr["EPA_REPORT_QTY"].ToString();
                    record.INGREDIENT_NAME = rdr["INGREDIENT_NAME"].ToString();
                    record.ODS_IND = rdr["ODS_IND"].ToString();
                    record.OSHA_PEL = rdr["OSHA_PEL"].ToString();
                    record.OSHA_STEL = rdr["OSHA_STEL"].ToString();
                    record.OTHER_REC_LIMITS = rdr["OTHER_REC_LIMITS"].ToString();
                    record.PRCNT = rdr["PRCNT"].ToString();
                    record.PRCNT_VOL_VALUE = rdr["PRCNT_VOL_VALUE"].ToString();
                    record.PRCNT_VOL_WEIGHT = rdr["PRCNT_VOL_WEIGHT"].ToString();
                    record.RTECS_CODE = rdr["RTECS_CODE"].ToString();
                    record.RTECS_NUM = rdr["RTECS_NUM"].ToString();

                    // Add it to the list
                    list.Add(record);
                }
            }

            cmd.Dispose();

            return list;
        }

        private MSDSItemDescription getMSDSItemDescriptionRecord(long msds_id, SqlConnection conn) {
            MSDSItemDescription record = null;

            string sqlStmt = "SELECT * FROM msds_item_description WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using(SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDSItemDescription();
                    int temp_id = 0;

                    record.BATCH_NUMBER = rdr["BATCH_NUMBER"].ToString();
                    record.ITEM_MANAGER = rdr["ITEM_MANAGER"].ToString();
                    record.ITEM_NAME = rdr["ITEM_NAME"].ToString();
                    record.LOG_FLIS_NIIN_VER = rdr["LOG_FLIS_NIIN_VER"].ToString();
                    int.TryParse(rdr["LOG_FSC"].ToString(), out temp_id);
                    record.LOG_FSC = temp_id;
                    record.LOT_NUMBER = rdr["LOT_NUMBER"].ToString();
                    record.NET_UNIT_WEIGHT = rdr["NET_UNIT_WEIGHT"].ToString();
                    record.QUANTITATIVE_EXPRESSION = rdr["QUANTITATIVE_EXPRESSION"].ToString();
                    record.SHELF_LIFE_CODE = rdr["SHELF_LIFE_CODE"].ToString();
                    record.SPECIAL_EMP_CODE = rdr["SPECIAL_EMP_CODE"].ToString();
                    record.SPECIFICATION_NUMBER = rdr["SPECIFICATION_NUMBER"].ToString();
                    record.TYPE_GRADE_CLASS = rdr["TYPE_GRADE_CLASS"].ToString();
                    record.TYPE_OF_CONTAINER = rdr["TYPE_OF_CONTAINER"].ToString();
                    record.UI_CONTAINER_QTY = rdr["UI_CONTAINER_QTY"].ToString();
                    record.UN_NA_NUMBER = rdr["UN_NA_NUMBER"].ToString();
                    record.UNIT_OF_ISSUE = rdr["UNIT_OF_ISSUE"].ToString();
                    record.UPC_GTIN = rdr["UPC_GTIN"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private MSDSLabelInfo getMSDSLabelInfoRecord(long msds_id, SqlConnection conn) {
            MSDSLabelInfo record = null;

            string sqlStmt = "SELECT * FROM msds_label_info WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDSLabelInfo();

                    record.COMPANY_CAGE_RP = rdr["COMPANY_CAGE_RP"].ToString();
                    record.COMPANY_NAME_RP = rdr["COMPANY_NAME_RP"].ToString();
                    record.LABEL_EMERG_PHONE = rdr["LABEL_EMERG_PHONE"].ToString();
                    record.LABEL_ITEM_NAME = rdr["LABEL_ITEM_NAME"].ToString();
                    record.LABEL_PROC_YEAR = rdr["LABEL_PROC_YEAR"].ToString();
                    record.LABEL_PROD_IDENT = rdr["LABEL_PROD_IDENT"].ToString();
                    record.LABEL_PROD_SERIALNO = rdr["LABEL_PROD_SERIALNO"].ToString();
                	record.LABEL_SIGNAL_WORD_CODE = rdr["LABEL_SIGNAL_WORD_CODE"].ToString();
                    record.LABEL_STOCK_NO = rdr["LABEL_STOCK_NO"].ToString();
                    record.SPECIFIC_HAZARDS = rdr["SPECIFIC_HAZARDS"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private MSDSPhysChemical getMSDSPhysChemicalRecord(long msds_id, SqlConnection conn) {
            MSDSPhysChemical record = null;

            string sqlStmt = "SELECT * FROM msds_phys_chemical WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDSPhysChemical();

                    record.APP_ODOR = rdr["APP_ODOR"].ToString();
                    record.AUTOIGNITION_TEMP = rdr["AUTOIGNITION_TEMP"].ToString();
                    record.CARCINOGEN_IND = rdr["CARCINOGEN_IND"].ToString();
                    record.EPA_ACUTE = rdr["EPA_ACUTE"].ToString();
                    record.EPA_CHRONIC = rdr["EPA_CHRONIC"].ToString();
                    record.EPA_FIRE = rdr["EPA_FIRE"].ToString();
                    record.EPA_PRESSURE = rdr["EPA_PRESSURE"].ToString();
                    record.EPA_REACTIVITY = rdr["EPA_REACTIVITY"].ToString();
                    record.EVAP_RATE_REF = rdr["EVAP_RATE_REF"].ToString();
                    record.FLASH_PT_TEMP = rdr["FLASH_PT_TEMP"].ToString();
                    record.NEUT_AGENT = rdr["NEUT_AGENT"].ToString();
                    record.NFPA_FLAMMABILITY = rdr["NFPA_FLAMMABILITY"].ToString();
                    record.NFPA_HEALTH = rdr["NFPA_HEALTH"].ToString();
                    record.NFPA_REACTIVITY = rdr["NFPA_REACTIVITY"].ToString();
                    record.NFPA_SPECIAL = rdr["NFPA_SPECIAL"].ToString();
                    record.OSHA_CARCINOGENS = rdr["OSHA_CARCINOGENS"].ToString();
                    record.OSHA_COMB_LIQUID = rdr["OSHA_COMB_LIQUID"].ToString();
                    record.OSHA_COMP_GAS = rdr["OSHA_COMP_GAS"].ToString();
                    record.OSHA_CORROSIVE = rdr["OSHA_CORROSIVE"].ToString();
                    record.OSHA_EXPLOSIVE = rdr["OSHA_EXPLOSIVE"].ToString();
                    record.OSHA_FLAMMABLE = rdr["OSHA_FLAMMABLE"].ToString();
                    record.OSHA_HIGH_TOXIC = rdr["OSHA_HIGH_TOXIC"].ToString();
                    record.OSHA_IRRITANT = rdr["OSHA_IRRITANT"].ToString();
                    record.OSHA_ORG_PEROX = rdr["OSHA_ORG_PEROX"].ToString();
                    record.OSHA_OTHERLONGTERM = rdr["OSHA_OTHERLONGTERM"].ToString();
                    record.OSHA_OXIDIZER = rdr["OSHA_OXIDIZER"].ToString();
                    record.OSHA_PYRO = rdr["OSHA_PYRO"].ToString();
                    record.OSHA_SENSITIZER = rdr["OSHA_SENSITIZER"].ToString();
                    record.OSHA_TOXIC = rdr["OSHA_TOXIC"].ToString();
                    record.OSHA_UNST_REACT = rdr["OSHA_UNST_REACT"].ToString();
                    record.OSHA_WATER_REACTIVE = rdr["OSHA_WATER_REACTIVE"].ToString();
                    record.OTHER_SHORT_TERM = rdr["OTHER_SHORT_TERM"].ToString();
                    record.PERCENT_VOL_VOLUME = rdr["PERCENT_VOL_VOLUME"].ToString();
                    record.PH = rdr["PH"].ToString();
                    record.PHYS_STATE_CODE = rdr["PHYS_STATE_CODE"].ToString();
                    record.SOL_IN_WATER = rdr["SOL_IN_WATER"].ToString();
                    record.SPECIFIC_GRAV = rdr["SPECIFIC_GRAV"].ToString();
                    record.VAPOR_DENS = rdr["VAPOR_DENS"].ToString();
                    record.VAPOR_PRESS = rdr["VAPOR_PRESS"].ToString();
                    record.VISCOSITY = rdr["VISCOSITY"].ToString();
                    record.VOC_GRAMS_LITER = rdr["VOC_GRAMS_LITER"].ToString();
                    record.VOC_POUNDS_GALLON = rdr["VOC_POUNDS_GALLON"].ToString();
                    record.VOL_ORG_COMP_WT = rdr["VOL_ORG_COMP_WT"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        private List<MSDSRadiologicalInfo> getMSDSRadiologicalInfoRecord(long msds_id, SqlConnection conn) {
            List<MSDSRadiologicalInfo> list = new List<MSDSRadiologicalInfo>();
            MSDSRadiologicalInfo record = null;

            string sqlStmt = "SELECT * FROM msds_radiological_info WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    record = new MSDSRadiologicalInfo();

                    record.NRC_LP_NUM = rdr["NRC_LP_NUM"].ToString();
                    record.OPERATOR = rdr["OPERATOR"].ToString();
                    record.RAD_AMOUNT_MICRO = rdr["RAD_AMOUNT_MICRO"].ToString();
                    record.RAD_CAS = rdr["RAD_CAS"].ToString();
                    record.RAD_FORM = rdr["RAD_FORM"].ToString();
                    record.RAD_NAME = rdr["RAD_NAME"].ToString();
                    record.RAD_SYMBOL = rdr["RAD_SYMBOL"].ToString();
                    record.REP_NSN = rdr["REP_NSN"].ToString();
                    record.SEALED = rdr["SEALED"].ToString();

                    list.Add(record);
                }
            }

            cmd.Dispose();

            return list;
        }

        private MSDSTransportation getMSDSTransportationRecord(long msds_id, SqlConnection conn) {
            MSDSTransportation record = null;

            string sqlStmt = "SELECT * FROM msds_transportation WHERE msds_id = @msds_id";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msds_id", msds_id);

            using (SqlDataReader rdr = cmd.ExecuteReader()) {
                if (rdr.Read()) {
                    record = new MSDSTransportation();
                    int temp_id = -1;

                    record.AFJM_PSN_CODE = rdr["AFJM_PSN_CODE"].ToString();
                    record.DOT_PSN_CODE = rdr["DOT_PSN_CODE"].ToString();
                    record.IATA_PSN_CODE = rdr["IATA_PSN_CODE"].ToString();
                    record.IMO_PSN_CODE = rdr["IMO_PSN_CODE"].ToString();
                    record.AF_MMAC_CODE = rdr["AF_MMAC_CODE"].ToString();
                    record.CERTIFICATE_COE = rdr["CERTIFICATE_COE"].ToString();
                    record.COMPETENT_CAA = rdr["COMPETENT_CAA"].ToString();
                    record.DOD_ID_CODE = rdr["DOD_ID_CODE"].ToString();
                    record.DOT_EXEMPTION_NO = rdr["DOT_EXEMPTION_NO"].ToString();
                    record.DOT_RQ_IND = rdr["DOT_RQ_IND"].ToString();
                    record.EX_NO = rdr["EX_NO"].ToString();
                    int.TryParse(rdr["HIGH_EXPLOSIVE_WT"].ToString(), out temp_id);
                    record.HIGH_EXPLOSIVE_WT = temp_id;
                    record.LTD_QTY_IND = rdr["LTD_QTY_IND"].ToString();
                    record.MAGNETIC_IND = rdr["MAGNETIC_IND"].ToString();
                    record.MAGNETISM = rdr["MAGNETISM"].ToString();
                   record.MARINE_POLLUTANT_IND = rdr["MARINE_POLLUTANT_IND"].ToString();
                    int.TryParse(rdr["NET_EXP_QTY_DIST"].ToString(), out temp_id);
                    record.NET_EXP_QTY_DIST = temp_id;
                    record.NET_EXP_WEIGHT = rdr["NET_EXP_WEIGHT"].ToString();
                    record.NET_PROPELLANT_WT = rdr["NET_PROPELLANT_WT"].ToString();
                    record.NOS_TECHNICAL_SHIPPING_NAME = rdr["NOS_TECHNICAL_SHIPPING_NAME"].ToString();
                    record.TRANSPORTATION_ADDITIONAL_DATA = rdr["TRANSPORTATION_ADDITIONAL_DATA"].ToString();
                }
            }

            cmd.Dispose();

            return record;
        }

        public long getMSDSIdFromSerNoCage(String msdsSerNo, String cage) {
            long msds_id = -1;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            string sqlStmt = "SELECT msds_id FROM msds_master WHERE MSDSSERNO = @msdsSerNo " +
                "AND CAGE = @cage ORDER BY msds_id DESC";

            SqlCommand cmd = new SqlCommand(sqlStmt, conn);

            //get the row
            cmd.Parameters.AddWithValue("@msdsSerNo", msdsSerNo);
            cmd.Parameters.AddWithValue("@cage", cage);

            try {
                msds_id = (Int64)cmd.ExecuteScalar();
            }
            catch {
                msds_id = -1;
            }
            finally {
                conn.Close();
            }

            return msds_id;
        }
        #endregion

        #region MSDS Inserts
        public long insertMSDSMasterRecord(String uic, MsdsEditViewModel model, String username) {
            MSDSRecord record = new MSDSRecord(model);
            return insertMSDSMasterRecord(uic, record, username);
        }


        public long insertMSDSMasterRecord(String uic, MSDSRecord record, String username) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            // Check to make sure we have the ID values - just in case
            //if (record.hcc_id == 0) {
            //    record.hcc_id = getIDFromValue(
            //        "HCC", "hcc_id", "hcc", record.HCC, conn);
            //}

            String stmt = "INSERT INTO msds_master (MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, " +
                "NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, " +
                "DESCRIPTION, EMERGENCY_TEL, END_COMP_IND, END_ITEM_IND, " +
                "KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, " +
                "PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, " +
                "PRODUCT_REVISION_NO, PROPRIETARY_IND, PUBLISHED_IND, " +
                "PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY_CODE, " +
                "TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, " +
                "data_source_cd, created, created_by) " +
                "VALUES(@MSDSSERNO, @CAGE, @MANUFACTURER, @PARTNO, @FSC, " +
                "@NIIN, @hcc_id, @file_name, @manually_entered, @ARTICLE_IND, " +
                "@DESCRIPTION, @EMERGENCY_TEL, @END_COMP_IND, @END_ITEM_IND, " +
                "@KIT_IND, @KIT_PART_IND, @MANUFACTURER_MSDS_NO, @MIXTURE_IND, " +
                "@PRODUCT_IDENTITY, @PRODUCT_LOAD_DATE, @PRODUCT_RECORD_STATUS, " +
                "@PRODUCT_REVISION_NO, @PROPRIETARY_IND, @PUBLISHED_IND, " +
                "@PURCHASED_PROD_IND, @PURE_IND, @RADIOACTIVE_IND, @SERVICE_AGENCY_CODE, " +
                "@TRADE_NAME, @TRADE_SECRET_IND, @PRODUCT_IND, @PRODUCT_LANGUAGE, " +
                "@data_source_cd, @created, @created_by); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();
            SqlCommand insertCmd = null;

            try {
                insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@MSDSSERNO", record.MSDSSERNO);
                insertCmd.Parameters.AddWithValue("@CAGE", dbNull(record.CAGE));
                insertCmd.Parameters.AddWithValue("@MANUFACTURER", dbNull(record.MANUFACTURER));
                insertCmd.Parameters.AddWithValue("@PARTNO", dbNull(record.PARTNO));
                insertCmd.Parameters.AddWithValue("@FSC", dbNull(record.FSC));
                insertCmd.Parameters.AddWithValue("@NIIN", dbNull(record.NIIN));
                insertCmd.Parameters.AddWithValue("@hcc_id", dbNull(record.hcc_id));
                insertCmd.Parameters.AddWithValue("@file_name", dbNull(record.file_name));
                insertCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.manually_entered));
                insertCmd.Parameters.AddWithValue("@ARTICLE_IND", dbNull(record.ARTICLE_IND));
                insertCmd.Parameters.AddWithValue("@DESCRIPTION", dbNull(record.DESCRIPTION));
                insertCmd.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(record.EMERGENCY_TEL));
                insertCmd.Parameters.AddWithValue("@END_COMP_IND", dbNull(record.END_COMP_IND));
                insertCmd.Parameters.AddWithValue("@END_ITEM_IND", dbNull(record.END_ITEM_IND));
                insertCmd.Parameters.AddWithValue("@KIT_IND", dbNull(record.KIT_IND));
                insertCmd.Parameters.AddWithValue("@KIT_PART_IND", dbNull(record.KIT_PART_IND));
                insertCmd.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(record.MANUFACTURER_MSDS_NO));
                insertCmd.Parameters.AddWithValue("@MIXTURE_IND", dbNull(record.MIXTURE_IND));
                insertCmd.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(record.PRODUCT_IDENTITY));
                insertCmd.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(record.PRODUCT_LOAD_DATE));
                insertCmd.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(record.PRODUCT_RECORD_STATUS));
                insertCmd.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(record.PRODUCT_REVISION_NO));
                insertCmd.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(record.PROPRIETARY_IND));
                insertCmd.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(record.PUBLISHED_IND));
                insertCmd.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(record.PURCHASED_PROD_IND));
                insertCmd.Parameters.AddWithValue("@PURE_IND", dbNull(record.PURE_IND));
                insertCmd.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(record.RADIOACTIVE_IND));
                insertCmd.Parameters.AddWithValue("@SERVICE_AGENCY_CODE", dbNull(record.SERVICE_AGENCY_CODE));
                insertCmd.Parameters.AddWithValue("@TRADE_NAME", dbNull(record.TRADE_NAME));
                insertCmd.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(record.TRADE_SECRET_IND));
                insertCmd.Parameters.AddWithValue("@PRODUCT_IND", dbNull(record.PRODUCT_IND));
                insertCmd.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(record.PRODUCT_LANGUAGE));
                insertCmd.Parameters.AddWithValue("@data_source_cd", uic);
                insertCmd.Parameters.AddWithValue("@created", DateTime.Now);
                insertCmd.Parameters.AddWithValue("@created_by", username);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("MSDSMaster Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.MSDSSERNO, record.CAGE, record.PRODUCT_IDENTITY, result);

                // Process the sub table info
                long subresult = insertMSDSSubTables(result, record, conn);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting MSDSMaster: {0}, {1}, {2} - {3}",
                    uic, record.MSDSSERNO, record.CAGE, ex.Message);
                throw ex;
            }
            finally {
                insertCmd.Dispose();
                conn.Close();
            }

            return result;
        }

        private long insertMSDSSubTables(long msds_id, MSDSRecord record, SqlConnection conn) {
            long result = 0;
            //MSDS CONTRACTOR LIST
            if (record.ContractorList.Count > 0) {
                insertMSDSContractList(msds_id, record.ContractorList, conn);
            }
            //MSDS DISPOSAL
            if (record.Disposal != null) {
                insertMSDSDisposalRecord(msds_id, record.Disposal, conn);
            }
            //MSDS DOC TYPES
            if (record.DocumentTypes != null) {
                insertMSDSDocumentTypesRecord(msds_id, record.DocumentTypes, conn);
            }
            // MSDS INGREDIENTS
            if (record.IngredientsList.Count > 0) {
                insertMSDSIngredientsListRecord(msds_id, record.IngredientsList, conn);
            }
            //MSDS ITEM DESCRIPTION
            if (record.ItemDescription != null) {
                insertMSDSItemDescriptionRecord(msds_id, record.ItemDescription, conn);
            }
            //MSDS LABEL INFO
            if (record.LabelInfo != null) {
                insertMSDSLabelInfoRecord(msds_id, record.LabelInfo, conn);
            }
            //MSDS PHYS CHEMICAL TABLE
            if (record.PhysChemical != null) {
                insertMSDSPhysChemicalRecord(msds_id, record.PhysChemical, conn);
            }
            //MSDS RADIOLOGICAL INFO
            if (record.RadiologicalInfoList != null) {
                insertMSDSRadiologicalInfoListRecord(msds_id, record.RadiologicalInfoList, conn);
            }
            //MSDS TRANSPORTATION
            if (record.Transportation != null) {
                insertMSDSTransportationRecord(msds_id, record.Transportation, conn);
            }

            return result;
        }

        private long insertMSDSAFJM_PSNRecord(long msds_id, MSDS_AFJM_PSN record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO MSDS_AFJM_PSN (AFJM_HAZARD_CLASS, " +
                "AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP, AFJM_PROP_SHIP_NAME, " +
                "AFJM_PROP_SHIP_MODIFIER, AFJM_PSN_CODE, AFJM_SPECIAL_PROV, " +
                "AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS, AFJM_UN_ID_NUMBER) " +
                "VALUES(@AFJM_HAZARD_CLASS, @AFJM_PACK_PARAGRAPH, " +
                "@AFJM_PACK_GROUP, @AFJM_PROP_SHIP_NAME, @AFJM_PROP_SHIP_MODIFIER, " +
                "@AFJM_PSN_CODE, @AFJM_SPECIAL_PROV, @AFJM_SUBSIDIARY_RISK, " +
                "@AFJM_SYMBOLS, @AFJM_UN_ID_NUMBER); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                using (SqlCommand insertCmd = new SqlCommand(stmt, conn)) {
                    insertCmd.Transaction = tran;

                    insertCmd.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(record.AFJM_HAZARD_CLASS));
                    insertCmd.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(record.AFJM_PACK_PARAGRAPH));
                    insertCmd.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(record.AFJM_PACK_GROUP));
                    insertCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(record.AFJM_PROP_SHIP_NAME));
                    insertCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(record.AFJM_PROP_SHIP_MODIFIER));
                    insertCmd.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(record.AFJM_PSN_CODE));
                    insertCmd.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(record.AFJM_SPECIAL_PROV));
                    insertCmd.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(record.AFJM_SUBSIDIARY_RISK));
                    insertCmd.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(record.AFJM_SYMBOLS));
                    insertCmd.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(record.AFJM_UN_ID_NUMBER));

                    object o = insertCmd.ExecuteScalar();

                    result = Convert.ToInt32(o);

                    tran.Commit();

                            logger.InfoFormat("AFJM_PSN Inserted: {0}, {1}, {2} ID = {3}",
                                 record.AFJM_PSN_CODE, record.AFJM_HAZARD_CLASS, record.AFJM_UN_ID_NUMBER, result);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error inserting AFJM_PSN: {0}, {1} - {2}",
                        record.AFJM_PSN_CODE, record.AFJM_HAZARD_CLASS, ex.Message);
                    //throw ex;
            }

            return result;
        }

        //MSDS CONTRACT LIST
        private long insertMSDSContractList(long msds_id, List<MSDSContractorInfo> contractorList,
            SqlConnection conn) {
            long result = 0;

            long contractor_id = 0;

            String stmt = "INSERT INTO msds_contracts (CT_CONTRACT, contractor_id, " +
                "PURCHASE_ORDER_NO, msds_id) " +
                "VALUES(@CT_CONTRACT, @contractor_id, " +
                "@PURCHASE_ORDER_NO, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = null;

            SqlCommand insertCmd = new SqlCommand(stmt, conn);

            // Loop through the contractor records
            foreach (MSDSContractorInfo record in contractorList) {
                try {
                    tran = conn.BeginTransaction();
                    
                    // Clear the parameters
                    insertCmd.Parameters.Clear();
                    insertCmd.Transaction = tran;

                    // Get the contractor_id
                    contractor_id = getIDFromValue("msds_contractor_info", "contractor_id", 
                        "CT_CAGE", record.CT_CAGE);
                    if (contractor_id == 0)
                        contractor_id = insertMSDSContractorRecord(record);

                    insertCmd.Parameters.AddWithValue("@CT_CONTRACT", dbNull(record.CT_NUMBER));
                    insertCmd.Parameters.AddWithValue("@contractor_id", contractor_id);
                    insertCmd.Parameters.AddWithValue("@PURCHASE_ORDER_NO", dbNull(record.PURCHASE_ORDER_NO));
                    insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                    object o = insertCmd.ExecuteScalar();

                    result = Convert.ToInt32(o);

                    tran.Commit();

                    logger.InfoFormat("ContractorInfo Inserted: {0}, {1}, {2}, {3} ID = {4}",
                        msds_id, record.CT_CAGE, record.CT_NUMBER, record.PURCHASE_ORDER_NO, result);
                }
                catch (Exception ex) {
                    logger.ErrorFormat("Error inserting ContractorInfo: {0}, {1}, {2} - {3}",
                        msds_id, record.CT_CAGE, record.CT_NUMBER, ex.Message);
                }
            }

            return result;
        }

        protected internal long insertMSDSContractorRecord(MSDSContractorInfo record) {
            long result = 0;

            String stmt = "INSERT INTO msds_contractor_info (CT_CAGE, " +
                "CT_CITY, CT_COMPANY_NAME, CT_COUNTRY, CT_PO_BOX, CT_PHONE, " +
                "CT_ADDRESS_1, CT_STATE, CT_ZIP_CODE) " +
                "VALUES(@CT_CAGE, @CT_CITY, @CT_COMPANY_NAME, @CT_COUNTRY, " +
                "@CT_PO_BOX, @CT_PHONE, @CT_ADDRESS_1, @CT_STATE, @CT_ZIP_CODE); " +
                "SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

                SqlTransaction tran = conn.BeginTransaction();

                try {
                    using (SqlCommand insertCmd = new SqlCommand(stmt, conn)) {
                        insertCmd.Transaction = tran;

                    insertCmd.Parameters.AddWithValue("@CT_CAGE", dbNull(record.CT_CAGE));
                    insertCmd.Parameters.AddWithValue("@CT_CITY", dbNull(record.CT_CITY));
                    insertCmd.Parameters.AddWithValue("@CT_COMPANY_NAME", dbNull(record.CT_COMPANY_NAME));
                    insertCmd.Parameters.AddWithValue("@CT_COUNTRY", dbNull(record.CT_COUNTRY));
                    insertCmd.Parameters.AddWithValue("@CT_PO_BOX", dbNull(record.CT_PO_BOX));
                    insertCmd.Parameters.AddWithValue("@CT_PHONE", dbNull(record.CT_PHONE));
                        insertCmd.Parameters.AddWithValue("@CT_ADDRESS_1", dbNull(record.CT_ADDRESS_1));
                    insertCmd.Parameters.AddWithValue("@CT_STATE", dbNull(record.CT_STATE));
                        insertCmd.Parameters.AddWithValue("@CT_ZIP_CODE", dbNull(record.CT_ZIP_CODE));
                    insertCmd.Parameters.AddWithValue("@PURCHASE_ORDER_NO", dbNull(record.PURCHASE_ORDER_NO));

                    object o = insertCmd.ExecuteScalar();

                    result = Convert.ToInt32(o);

                    tran.Commit();

                        logger.InfoFormat("ContractorInfo Inserted: {0}, {1}, {2}} ID = {3}",
                            record.CT_CAGE, record.CT_COMPANY_NAME, result);
                    }
                }
                catch (Exception ex) {
                    logger.ErrorFormat("Error inserting ContractorInfo: {0}, {1} - {2}",
                        record.CT_CAGE, record.CT_COMPANY_NAME, ex.Message);
                }
           }

            return result;
        }

        private long getContractorIDFromCAGE(string cage) {
            long result = 0;

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

                string sql = "SELECT contractor_id FROM msds_contractor_info WHERE CT_CAGE = @cage";

                using (SqlCommand cmd = new SqlCommand(sql, conn)) {

                    //get the row
                    cmd.Parameters.AddWithValue("@cage", cage);

                    try {
                        object o = cmd.ExecuteScalar();

                        if (o != null)
                            long.TryParse(o.ToString(), out result);
                    }
                    catch (Exception ex) {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                }
            }

            return result;
        }

        //MSDS DISPOSAL
        private long insertMSDSDisposalRecord(long msds_id, MSDSDisposal record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_disposal (DISPOSAL_ADD_INFO, EPA_HAZ_WASTE_CODE, " +
                "EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME, msds_id) " +
                "VALUES(@DISPOSAL_ADD_INFO, @EPA_HAZ_WASTE_CODE, " +
                "@EPA_HAZ_WASTE_IND, @EPA_HAZ_WASTE_NAME, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(record.DISPOSAL_ADD_INFO));
                insertCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(record.EPA_HAZ_WASTE_CODE));
                insertCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(record.EPA_HAZ_WASTE_IND));
                insertCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(record.EPA_HAZ_WASTE_NAME));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("Disposal Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, record.EPA_HAZ_WASTE_IND, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting Disposal: {0}, {1}, {2} - {3}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOC TYPES
        private long insertMSDSDocumentTypesRecord(long msds_id, MSDSDocumentTypes record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_document_types (msds_id, " +
                "msds_translated_filename, neshap_comp_filename, other_docs_filename, " +
                "product_sheet_filename, transportation_cert_filename, manufacturer_label_filename) " +
                "VALUES(@msds_id, @msds_translated_filename, @neshap_comp_filename, " +
                "@other_docs_filename, @product_sheet_filename, @transportation_cert_filename, " + 
                "@manufacturer_label_filename); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);
                insertCmd.Parameters.AddWithValue("@msds_translated_filename", dbNull(record.msds_translated_filename));
                insertCmd.Parameters.AddWithValue("@neshap_comp_filename", dbNull(record.neshap_comp_filename));
                insertCmd.Parameters.AddWithValue("@other_docs_filename", dbNull(record.other_docs_filename));
                insertCmd.Parameters.AddWithValue("@product_sheet_filename", dbNull(record.product_sheet_filename));
                insertCmd.Parameters.AddWithValue("@transportation_cert_filename", dbNull(record.transportation_cert_filename));
                insertCmd.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(record.manufacturer_label_filename));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("DocumentTypes Inserted: {0}, {1}, {2} ID = {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting DocumentTypes: {0}, {1}, {2} - {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOT PSN
        protected internal long insertMSDSDOT_PSNRecord(MSDS_DOT_PSN record) {
            long result = 0;

            String stmt = "INSERT INTO MSDS_DOT_PSN (DOT_HAZARD_CLASS_DIV, DOT_HAZARD_LABEL, " +
                "DOT_MAX_CARGO, DOT_MAX_PASSENGER, DOT_PACK_BULK, DOT_PACK_EXCEPTIONS, " +
                "DOT_PACK_NONBULK, DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER, " +
                "DOT_PSN_CODE, DOT_SPECIAL_PROVISION, DOT_SYMBOLS, DOT_UN_ID_NUMBER, " +
                "DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW, DOT_PACK_GROUP) " +
                "VALUES(@DOT_HAZARD_CLASS_DIV, @DOT_HAZARD_LABEL, @DOT_MAX_CARGO, " +
                "@DOT_MAX_PASSENGER, @DOT_PACK_BULK, @DOT_PACK_EXCEPTIONS, " +
                "@DOT_PACK_NONBULK, @DOT_PROP_SHIP_NAME, @DOT_PROP_SHIP_MODIFIER, " +
                "@DOT_PSN_CODE, @DOT_SPECIAL_PROVISION, @DOT_SYMBOLS, @DOT_UN_ID_NUMBER, " +
                "@DOT_WATER_OTHER_REQ, @DOT_WATER_VESSEL_STOW, @DOT_PACK_GROUP); " +
                "SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand insertCmd = new SqlCommand(stmt, conn)) {
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(record.DOT_HAZARD_CLASS_DIV));
                insertCmd.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(record.DOT_HAZARD_LABEL));
                insertCmd.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(record.DOT_MAX_CARGO));
                insertCmd.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(record.DOT_MAX_PASSENGER));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(record.DOT_PACK_BULK));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(record.DOT_PACK_EXCEPTIONS));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(record.DOT_PACK_NONBULK));
                insertCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(record.DOT_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(record.DOT_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(record.DOT_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(record.DOT_SPECIAL_PROVISION));
                insertCmd.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(record.DOT_SYMBOLS));
                insertCmd.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(record.DOT_UN_ID_NUMBER));
                insertCmd.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(record.DOT_WATER_OTHER_REQ));
                insertCmd.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(record.DOT_WATER_VESSEL_STOW));
                insertCmd.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(record.DOT_PACK_GROUP));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                        logger.InfoFormat("DOT_PSN Inserted: {0}, {1}, {2} ID = {3}",
                            record.DOT_PSN_CODE, record.DOT_HAZARD_CLASS_DIV, record.DOT_HAZARD_LABEL, result);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error inserting DOT_PSN: {0}, {1} - {2}",
                        record.DOT_PSN_CODE, record.DOT_HAZARD_CLASS_DIV, ex.Message);
                    //throw ex;
                }
            }

            return result;
        }

        //MSDS IATA PSN
        protected internal long insertMSDSIATA_PSNRecord(MSDS_IATA_PSN record) {
            long result = 0;

            String stmt = "INSERT INTO MSDS_IATA_PSN (IATA_CARGO_PACKING, " +
                "IATA_HAZARD_CLASS, IATA_HAZARD_LABEL, IATA_PACK_GROUP, " +
                "IATA_PASS_AIR_PACK_LMT_INSTR, IATA_PASS_AIR_PACK_LMT_PER_PKG,  " +
                "IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME, IATA_PROP_SHIP_MODIFIER, " +
                "IATA_CARGO_PACK_MAX_QTY, IATA_PSN_CODE, IATA_PASS_AIR_MAX_QTY, " +
                "IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK, IATA_UN_ID_NUMBER) " +
                "VALUES(@IATA_CARGO_PACKING, @IATA_HAZARD_CLASS, @IATA_HAZARD_LABEL, " +
                "@IATA_PACK_GROUP, @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                "@IATA_PASS_AIR_PACK_LMT_PER_PKG, @IATA_PASS_AIR_PACK_NOTE, " +
                "@IATA_PROP_SHIP_NAME, @IATA_PROP_SHIP_MODIFIER, " +
                "@IATA_CARGO_PACK_MAX_QTY, @IATA_PSN_CODE, @IATA_PASS_AIR_MAX_QTY, " +
                "@IATA_SPECIAL_PROV, @IATA_SUBSIDIARY_RISK, @IATA_UN_ID_NUMBER); " +
                "SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand insertCmd = new SqlCommand(stmt, conn)) {
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(record.IATA_CARGO_PACKING));
                insertCmd.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(record.IATA_HAZARD_CLASS));
                insertCmd.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(record.IATA_HAZARD_LABEL));
                insertCmd.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(record.IATA_PACK_GROUP));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(record.IATA_PASS_AIR_PACK_LMT_INSTR));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(record.IATA_PASS_AIR_PACK_LMT_PER_PKG));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(record.IATA_PASS_AIR_PACK_NOTE));
                insertCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(record.IATA_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(record.IATA_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(record.IATA_CARGO_PACK_MAX_QTY));
                insertCmd.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(record.IATA_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(record.IATA_PASS_AIR_MAX_QTY));
                insertCmd.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(record.IATA_SPECIAL_PROV));
                insertCmd.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(record.IATA_SUBSIDIARY_RISK));
                insertCmd.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(record.IATA_UN_ID_NUMBER));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                        logger.InfoFormat("IATA_PSN Inserted: {0}, {1}, {2} ID = {3}",
                            record.IATA_PSN_CODE, record.IATA_HAZARD_CLASS, record.IATA_UN_ID_NUMBER, result);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error inserting IATA_PSN: {0}, {1} - {2}",
                        record.IATA_PSN_CODE, record.IATA_HAZARD_CLASS, ex.Message);
                    //throw ex;
                }
            }

            return result;
        }

        //MSDS IMO PSN
        protected internal long insertMSDSIMO_PSNRecord(MSDS_IMO_PSN record) {
            long result = 0;

            String stmt = "INSERT INTO MSDS_IMO_PSN (IMO_EMS_NO, IMO_HAZARD_CLASS, " +
                "IMO_IBC_INSTR, IMO_LIMITED_QTY, IMO_PACK_GROUP, IMO_PACK_INSTRUCTIONS, " +
                "IMO_PACK_PROVISIONS, IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER, " +
                "IMO_PSN_CODE, IMO_SPECIAL_PROV, IMO_STOW_SEGR, " +
                "IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO, IMO_TANK_INSTR_PROV, " +
                "IMO_TANK_INSTR_UN, IMO_UN_NUMBER, IMO_IBC_PROVISIONS) " +
                "VALUES(@IMO_EMS_NO, @IMO_HAZARD_CLASS, @IMO_IBC_INSTR, @IMO_LIMITED_QTY, " +
                "@IMO_PACK_GROUP, @IMO_PACK_INSTRUCTIONS, @IMO_PACK_PROVISIONS, " +
                "@IMO_PROP_SHIP_NAME, @IMO_PROP_SHIP_MODIFIER, @IMO_PSN_CODE, " +
                "@IMO_SPECIAL_PROV, @IMO_STOW_SEGR, @IMO_SUBSIDIARY_RISK, " +
                "@IMO_TANK_INSTR_IMO, @IMO_TANK_INSTR_PROV, @IMO_TANK_INSTR_UN, " +
                "@IMO_UN_NUMBER, @IMO_IBC_PROVISIONS); " +
                "SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand insertCmd = new SqlCommand(stmt, conn)) {
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(record.IMO_EMS_NO));
                insertCmd.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(record.IMO_HAZARD_CLASS));
                insertCmd.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(record.IMO_IBC_INSTR));
                insertCmd.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(record.IMO_LIMITED_QTY));
                insertCmd.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(record.IMO_PACK_GROUP));
                insertCmd.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(record.IMO_PACK_INSTRUCTIONS));
                insertCmd.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(record.IMO_PACK_PROVISIONS));
                insertCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(record.IMO_PROP_SHIP_NAME));
                insertCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(record.IMO_PROP_SHIP_MODIFIER));
                insertCmd.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(record.IMO_PSN_CODE));
                insertCmd.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(record.IMO_SPECIAL_PROV));
                insertCmd.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(record.IMO_STOW_SEGR));
                insertCmd.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(record.IMO_SUBSIDIARY_RISK));
                insertCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(record.IMO_TANK_INSTR_IMO));
                insertCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(record.IMO_TANK_INSTR_PROV));
                insertCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(record.IMO_TANK_INSTR_UN));
                insertCmd.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(record.IMO_UN_NUMBER));
                insertCmd.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(record.IMO_IBC_PROVISIONS));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                        logger.InfoFormat("IMO_PSN Inserted: {0}, {1}, {2} ID = {3}",
                            record.IMO_PSN_CODE, record.IMO_HAZARD_CLASS, record.IMO_UN_NUMBER, result);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error inserting IMO_PSN: {0}, {1} - {2}",
                        record.IMO_PSN_CODE, record.IMO_HAZARD_CLASS, ex.Message);
                    //throw ex;
                }
            }

            return result;
        }

        // MSDS INGREDIENTS
        private long insertMSDSIngredientsListRecord(long msds_id, List<MSDSIngredient> ingredientsList, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_ingredients (msds_id, CAS, RTECS_NUM, " +
                "RTECS_CODE, INGREDIENT_NAME, PRCNT, OSHA_PEL, OSHA_STEL, ACGIH_TLV, " +
                "ACGIH_STEL, EPA_REPORT_QTY, DOT_REPORT_QTY, PRCNT_VOL_VALUE, PRCNT_VOL_WEIGHT, " +
                "CHEM_MFG_COMP_NAME, ODS_IND, OTHER_REC_LIMITS) " +
                "VALUES(@msds_id, @CAS, @RTECS_NUM, @RTECS_CODE, @INGREDIENT_NAME, " +
                "@PRCNT,  @OSHA_PEL, @OSHA_STEL, @ACGIH_TLV, @ACGIH_STEL, @EPA_REPORT_QTY, " +
                "@DOT_REPORT_QTY, @PRCNT_VOL_VALUE, @PRCNT_VOL_WEIGHT,  @CHEM_MFG_COMP_NAME, " +
                "@ODS_IND, @OTHER_REC_LIMITS); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = null;

            SqlCommand insertCmd = new SqlCommand(stmt, conn);

            // Loop through the contracotr records
            foreach (MSDSIngredient record in ingredientsList) {
                try {
                    tran = conn.BeginTransaction();

                    // Clear the parameters
                    insertCmd.Parameters.Clear();
                    insertCmd.Transaction = tran;

                    insertCmd.Parameters.AddWithValue("@msds_id", msds_id);
                    insertCmd.Parameters.AddWithValue("@CAS", dbNull(record.CAS));
                    insertCmd.Parameters.AddWithValue("@RTECS_NUM", dbNull(record.RTECS_NUM));
                    insertCmd.Parameters.AddWithValue("@RTECS_CODE", dbNull(record.RTECS_CODE));
                    insertCmd.Parameters.AddWithValue("@INGREDIENT_NAME", dbNull(record.INGREDIENT_NAME));
                    insertCmd.Parameters.AddWithValue("@PRCNT", dbNull(record.PRCNT));
                    insertCmd.Parameters.AddWithValue("@OSHA_PEL", dbNull(record.OSHA_PEL));
                    insertCmd.Parameters.AddWithValue("@OSHA_STEL", dbNull(record.OSHA_STEL));
                    insertCmd.Parameters.AddWithValue("@ACGIH_TLV", dbNull(record.ACGIH_TLV));
                    insertCmd.Parameters.AddWithValue("@ACGIH_STEL", dbNull(record.ACGIH_STEL));
                    insertCmd.Parameters.AddWithValue("@EPA_REPORT_QTY", dbNull(record.EPA_REPORT_QTY));
                    insertCmd.Parameters.AddWithValue("@DOT_REPORT_QTY", dbNull(record.DOT_REPORT_QTY));
                    insertCmd.Parameters.AddWithValue("@PRCNT_VOL_VALUE", dbNull(record.PRCNT_VOL_VALUE));
                    insertCmd.Parameters.AddWithValue("@PRCNT_VOL_WEIGHT", dbNull(record.PRCNT_VOL_WEIGHT));
                    insertCmd.Parameters.AddWithValue("@CHEM_MFG_COMP_NAME", dbNull(record.CHEM_MFG_COMP_NAME));
                    insertCmd.Parameters.AddWithValue("@ODS_IND", dbNull(record.ODS_IND));
                    insertCmd.Parameters.AddWithValue("@OTHER_REC_LIMITS", dbNull(record.OTHER_REC_LIMITS));

                    object o = insertCmd.ExecuteScalar();

                    result = Convert.ToInt32(o);

                    tran.Commit();

                    logger.InfoFormat("MSDS Ingredient Inserted: {0}, {1}, {2}, {3} ID = {4}",
                        msds_id, record.INGREDIENT_NAME, record.CAS, record.CHEM_MFG_COMP_NAME, result);
                }
                catch (Exception ex) {
                    logger.ErrorFormat("Error inserting MSDS Ingredient: {0}, {1}, {2} - {3}",
                        msds_id, record.INGREDIENT_NAME, record.CAS, ex.Message);
                }
            }

            return result;
        }

        //MSDS ITEM DESCRIPTION
        private long insertMSDSItemDescriptionRecord(long msds_id, MSDSItemDescription record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_item_description (msds_id, ITEM_MANAGER, ITEM_NAME, " +
                "SPECIFICATION_NUMBER, TYPE_GRADE_CLASS, UNIT_OF_ISSUE, " +
                "QUANTITATIVE_EXPRESSION, UI_CONTAINER_QTY, TYPE_OF_CONTAINER, " +
                "BATCH_NUMBER, LOT_NUMBER, LOG_FLIS_NIIN_VER, LOG_FSC, " +
                "NET_UNIT_WEIGHT, SHELF_LIFE_CODE, SPECIAL_EMP_CODE, " +
                "UN_NA_NUMBER, UPC_GTIN) " +
                "VALUES(@msds_id, @ITEM_MANAGER, @ITEM_NAME, @SPECIFICATION_NUMBER, " +
                "@TYPE_GRADE_CLASS, @UNIT_OF_ISSUE, @QUANTITATIVE_EXPRESSION, " +
                "@UI_CONTAINER_QTY, @TYPE_OF_CONTAINER, @BATCH_NUMBER, " +
                "@LOT_NUMBER, @LOG_FLIS_NIIN_VER, @LOG_FSC, @NET_UNIT_WEIGHT, " +
                "@SHELF_LIFE_CODE, @SPECIAL_EMP_CODE, @UN_NA_NUMBER, @UPC_GTIN); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@msds_id", dbNull(msds_id));
                insertCmd.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(record.ITEM_MANAGER));
                insertCmd.Parameters.AddWithValue("@ITEM_NAME", dbNull(record.ITEM_NAME));
                insertCmd.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(record.SPECIFICATION_NUMBER));
                insertCmd.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(record.TYPE_GRADE_CLASS));
                insertCmd.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(record.UNIT_OF_ISSUE));
                insertCmd.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(record.QUANTITATIVE_EXPRESSION));
                insertCmd.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(record.UI_CONTAINER_QTY));
                insertCmd.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(record.TYPE_OF_CONTAINER));
                insertCmd.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(record.BATCH_NUMBER));
                insertCmd.Parameters.AddWithValue("@LOT_NUMBER", dbNull(record.LOT_NUMBER));
                insertCmd.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(record.LOG_FLIS_NIIN_VER));
                insertCmd.Parameters.AddWithValue("@LOG_FSC", dbNull(record.LOG_FSC));
                insertCmd.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(record.NET_UNIT_WEIGHT));
                insertCmd.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(record.SHELF_LIFE_CODE));
                insertCmd.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(record.SPECIAL_EMP_CODE));
                insertCmd.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(record.UN_NA_NUMBER));
                insertCmd.Parameters.AddWithValue("@UPC_GTIN", dbNull(record.UPC_GTIN));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("ItemDescription Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, record.UN_NA_NUMBER, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting ItemDescription: {0}, {1}, {2} - {3}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS LABEL INFO
        private long insertMSDSLabelInfoRecord(long msds_id, MSDSLabelInfo record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_label_info (COMPANY_CAGE_RP, " +
                "COMPANY_NAME_RP, LABEL_EMERG_PHONE, LABEL_ITEM_NAME, " +
                "LABEL_PROC_YEAR, LABEL_PROD_IDENT, LABEL_PROD_SERIALNO, " +
                "LABEL_SIGNAL_WORD, LABEL_STOCK_NO, SPECIFIC_HAZARDS, msds_id) " +
                "VALUES(@COMPANY_CAGE_RP, @COMPANY_NAME_RP, @LABEL_EMERG_PHONE, " +
                "@LABEL_ITEM_NAME, @LABEL_PROC_YEAR, @LABEL_PROD_IDENT, " +
                "@LABEL_PROD_SERIALNO, @LABEL_SIGNAL_WORD, @LABEL_STOCK_NO, " +
                "@SPECIFIC_HAZARDS, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(record.COMPANY_CAGE_RP));
                insertCmd.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(record.COMPANY_NAME_RP));
                insertCmd.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(record.LABEL_EMERG_PHONE));
                insertCmd.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(record.LABEL_ITEM_NAME));
                insertCmd.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(record.LABEL_PROC_YEAR));
                insertCmd.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(record.LABEL_PROD_IDENT));
                insertCmd.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(record.LABEL_PROD_SERIALNO));
                insertCmd.Parameters.AddWithValue("@LABEL_SIGNAL_WORD", dbNull(record.LABEL_SIGNAL_WORD_CODE));
                insertCmd.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(record.LABEL_STOCK_NO));
                insertCmd.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(record.SPECIFIC_HAZARDS));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("LabelInfo Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, record.COMPANY_NAME_RP, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting LabelInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS PHYS CHEMICAL TABLE
        private long insertMSDSPhysChemicalRecord(long msds_id, MSDSPhysChemical record, SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_phys_chemical (msds_id, " +
                "VAPOR_PRESS, VAPOR_DENS, SPECIFIC_GRAV, " +
                "VOC_POUNDS_GALLON, VOC_GRAMS_LITER, PH, VISCOSITY, " +
                "EVAP_RATE_REF, SOL_IN_WATER, APP_ODOR, " +
                "PERCENT_VOL_VOLUME, AUTOIGNITION_TEMP, CARCINOGEN_IND, " +
                "EPA_ACUTE, EPA_CHRONIC, EPA_FIRE, EPA_PRESSURE, EPA_REACTIVITY, " +
                "FLASH_PT_TEMP, NEUT_AGENT, " +
                "NFPA_FLAMMABILITY, NFPA_HEALTH, NFPA_REACTIVITY, NFPA_SPECIAL, " +
                "OSHA_CARCINOGENS, OSHA_COMB_LIQUID, OSHA_COMP_GAS, " +
                "OSHA_CORROSIVE, OSHA_EXPLOSIVE, OSHA_FLAMMABLE, " +
                "OSHA_HIGH_TOXIC, OSHA_IRRITANT, OSHA_ORG_PEROX, " +
                "OSHA_OTHERLONGTERM, OSHA_OXIDIZER, OSHA_PYRO, " +
                "OSHA_SENSITIZER, OSHA_TOXIC, OSHA_UNST_REACT, " +
                "OTHER_SHORT_TERM, PHYS_STATE_CODE, VOL_ORG_COMP_WT, OSHA_WATER_REACTIVE) " +
                "VALUES(@msds_id, @VAPOR_PRESS, @VAPOR_DENS, @SPECIFIC_GRAV, " +
                "@VOC_POUNDS_GALLON, @VOC_GRAMS_LITER, @PH, @VISCOSITY, " +
                "@EVAP_RATE_REF, @SOL_IN_WATER, @APP_ODOR, @PERCENT_VOL_VOLUME, " +
                "@AUTOIGNITION_TEMP, @CARCINOGEN_IND, " +
                "@EPA_ACUTE, @EPA_CHRONIC, @EPA_FIRE, @EPA_PRESSURE, @EPA_REACTIVITY, " +
                "@FLASH_PT_TEMP, @NEUT_AGENT, " +
                "@NFPA_FLAMMABILITY, @NFPA_HEALTH, @NFPA_REACTIVITY, " +
                "@NFPA_SPECIAL, @OSHA_CARCINOGENS, @OSHA_COMB_LIQUID, " +
                "@OSHA_COMP_GAS, @OSHA_CORROSIVE, @OSHA_EXPLOSIVE, " +
                "@OSHA_FLAMMABLE, @OSHA_HIGH_TOXIC, @OSHA_IRRITANT, " +
                "@OSHA_ORG_PEROX, @OSHA_OTHERLONGTERM, @OSHA_OXIDIZER, " +
                "@OSHA_PYRO, @OSHA_SENSITIZER, @OSHA_TOXIC, @OSHA_UNST_REACT, " +
                "@OTHER_SHORT_TERM, @PHYS_STATE_CODE, @VOL_ORG_COMP_WT, " +
                "@OSHA_WATER_REACTIVE); " +
                "SELECT SCOPE_IDENTITY()";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(stmt, conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);
                insertCmd.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(record.VAPOR_PRESS));
                insertCmd.Parameters.AddWithValue("@VAPOR_DENS", dbNull(record.VAPOR_DENS));
                insertCmd.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(record.SPECIFIC_GRAV));
                insertCmd.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(record.VOC_POUNDS_GALLON));
                insertCmd.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(record.VOC_GRAMS_LITER));
                insertCmd.Parameters.AddWithValue("@PH", dbNull(record.PH));
                insertCmd.Parameters.AddWithValue("@VISCOSITY", dbNull(record.VISCOSITY));
                insertCmd.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(record.EVAP_RATE_REF));
                insertCmd.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(record.SOL_IN_WATER));
                insertCmd.Parameters.AddWithValue("@APP_ODOR", dbNull(record.APP_ODOR));
                insertCmd.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(record.PERCENT_VOL_VOLUME));
                insertCmd.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(record.AUTOIGNITION_TEMP));
                insertCmd.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(record.CARCINOGEN_IND));
                insertCmd.Parameters.AddWithValue("@EPA_ACUTE", dbNull(record.EPA_ACUTE));
                insertCmd.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(record.EPA_CHRONIC));
                insertCmd.Parameters.AddWithValue("@EPA_FIRE", dbNull(record.EPA_FIRE));
                insertCmd.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(record.EPA_PRESSURE));
                insertCmd.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(record.EPA_REACTIVITY));
                insertCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                insertCmd.Parameters.AddWithValue("@NEUT_AGENT", dbNull(record.NEUT_AGENT));
                insertCmd.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(record.NFPA_FLAMMABILITY));
                insertCmd.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(record.NFPA_HEALTH));
                insertCmd.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(record.NFPA_REACTIVITY));
                insertCmd.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(record.NFPA_SPECIAL));
                insertCmd.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(record.OSHA_CARCINOGENS));
                insertCmd.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(record.OSHA_COMB_LIQUID));
                insertCmd.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(record.OSHA_COMP_GAS));
                insertCmd.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(record.OSHA_CORROSIVE));
                insertCmd.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(record.OSHA_EXPLOSIVE));
                insertCmd.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(record.OSHA_FLAMMABLE));
                insertCmd.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(record.OSHA_HIGH_TOXIC));
                insertCmd.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(record.OSHA_IRRITANT));
                insertCmd.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(record.OSHA_ORG_PEROX));
                insertCmd.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(record.OSHA_OTHERLONGTERM));
                insertCmd.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(record.OSHA_OXIDIZER));
                insertCmd.Parameters.AddWithValue("@OSHA_PYRO", dbNull(record.OSHA_PYRO));
                insertCmd.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(record.OSHA_SENSITIZER));
                insertCmd.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(record.OSHA_TOXIC));
                insertCmd.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(record.OSHA_UNST_REACT));
                insertCmd.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(record.OTHER_SHORT_TERM));
                insertCmd.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(record.PHYS_STATE_CODE));
                insertCmd.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(record.VOL_ORG_COMP_WT));
                insertCmd.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(record.OSHA_WATER_REACTIVE));

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("PhysChemical Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, record.APP_ODOR, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting PhysChemical: {0}, {1}, {2} - {3}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS RADIOLOGICAL INFO
        private long insertMSDSRadiologicalInfoListRecord(long msds_id, List<MSDSRadiologicalInfo> records, 
                SqlConnection conn) {
            long result = 0;

            String stmt = "INSERT INTO msds_radiological_info (NRC_LP_NUM, " +
                "OPERATOR, RAD_AMOUNT_MICRO, RAD_FORM, RAD_CAS, " +
                "RAD_NAME, RAD_SYMBOL, REP_NSN, SEALED, msds_id) " +
                "VALUES(@NRC_LP_NUM, @OPERATOR, @RAD_AMOUNT_MICRO, @RAD_FORM, " +
                "@RAD_CAS, @RAD_NAME, @RAD_SYMBOL, @REP_NSN, @SEALED, @msds_id); " +
                "SELECT SCOPE_IDENTITY()";

            using(SqlCommand insertCmd = new SqlCommand(stmt, conn)) {
                foreach (MSDSRadiologicalInfo record in records) {
                    try {
                                SqlTransaction tran = conn.BeginTransaction();
                        insertCmd.Transaction = tran;
                                insertCmd.Parameters.Clear();

                        insertCmd.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(record.NRC_LP_NUM));
                        insertCmd.Parameters.AddWithValue("@OPERATOR", dbNull(record.OPERATOR));
                        insertCmd.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(record.RAD_AMOUNT_MICRO));
                        insertCmd.Parameters.AddWithValue("@RAD_FORM", dbNull(record.RAD_FORM));
                        insertCmd.Parameters.AddWithValue("@RAD_CAS", dbNull(record.RAD_CAS));
                        insertCmd.Parameters.AddWithValue("@RAD_NAME", dbNull(record.RAD_NAME));
                        insertCmd.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(record.RAD_SYMBOL));
                        insertCmd.Parameters.AddWithValue("@REP_NSN", dbNull(record.REP_NSN));
                        insertCmd.Parameters.AddWithValue("@SEALED", dbNull(record.SEALED));
                        insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                        object o = insertCmd.ExecuteScalar();

                        result = Convert.ToInt32(o);

                        tran.Commit();

                        logger.InfoFormat("RadiologicalInfo Inserted: {0}, {1}, {2}, {3} ID = {4}",
                            msds_id, record.RAD_CAS, record.RAD_SYMBOL, record.RAD_FORM, result);
                    }
                    catch (Exception ex) {
                        logger.ErrorFormat("Error inserting RadiologicalInfo: {0}, {1}, {2} - {3}",
                            msds_id, record.RAD_CAS, record.RAD_SYMBOL, ex.Message);
                            }
                }
            }

            return result;
        }

        //MSDS TRANSPORTATION
        private long insertMSDSTransportationRecord(long msds_id, MSDSTransportation record, SqlConnection conn) {
            long result = 0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("INSERT INTO msds_transportation (afjm_psn_id, dot_psn_id, iata_psn_id, imo_psn_id, ");
            sb.AppendLine("AF_MMAC_CODE, CERTIFICATE_COE, ");
            sb.AppendLine("COMPETENT_CAA, DOD_ID_CODE, DOT_EXEMPTION_NO, DOT_RQ_IND, ");
            sb.AppendLine("EX_NO, HIGH_EXPLOSIVE_WT, ");
            sb.AppendLine("LTD_QTY_IND, MAGNETIC_IND, MAGNETISM, MARINE_POLLUTANT_IND, ");
            sb.AppendLine("NET_EXP_QTY_DIST, NET_EXP_WEIGHT, NET_PROPELLANT_WT, ");
            sb.AppendLine("NOS_TECHNICAL_SHIPPING_NAME, TRANSPORTATION_ADDITIONAL_DATA, msds_id) ");
            sb.AppendLine("VALUES(@afjm_psn_id, @dot_psn_id, @iata_psn_id, @imo_psn_id, ");
            sb.AppendLine("@AF_MMAC_CODE, @CERTIFICATE_COE, @COMPETENT_CAA, @DOD_ID_CODE, ");
            sb.AppendLine("@DOT_EXEMPTION_NO, @DOT_RQ_IND, @EX_NO, ");
            sb.AppendLine("@HIGH_EXPLOSIVE_WT, @LTD_QTY_IND, @MAGNETIC_IND, ");
            sb.AppendLine("@MAGNETISM, @MARINE_POLLUTANT_IND, @NET_EXP_QTY_DIST, ");
            sb.AppendLine("@NET_EXP_WEIGHT, @NET_PROPELLANT_WT, @NOS_TECHNICAL_SHIPPING_NAME, ");
            sb.AppendLine("@TRANSPORTATION_ADDITIONAL_DATA, @msds_id); ");
            sb.AppendLine("SELECT SCOPE_IDENTITY()");

            SqlTransaction tran = conn.BeginTransaction();

            try {
                // Get the PSN ids
                long afjm_psn_id = getIDFromValue("MSDS_AFJM_PSN", "afjm_psn_id",
                    "AFJM_PSN_CODE", record.AFJM_PSN_CODE);
                long dot_psn_id = getIDFromValue("MSDS_DOT_PSN", "dot_psn_id",
                    "DOT_PSN_CODE", record.DOT_PSN_CODE);
                long iata_psn_id = getIDFromValue("MSDS_IATA_PSN", "iata_psn_id",
                    "IATA_PSN_CODE", record.IATA_PSN_CODE);
                long imo_psn_id = getIDFromValue("MSDS_IMO_PSN", "imo_psn_id",
                    "IMO_PSN_CODE", record.IMO_PSN_CODE);


                SqlCommand insertCmd = new SqlCommand(sb.ToString(), conn);
                insertCmd.Transaction = tran;

                insertCmd.Parameters.AddWithValue("@afjm_psn_id", afjm_psn_id);
                insertCmd.Parameters.AddWithValue("@dot_psn_id", dot_psn_id);
                insertCmd.Parameters.AddWithValue("@iata_psn_id", iata_psn_id);
                insertCmd.Parameters.AddWithValue("@imo_psn_id", imo_psn_id);

                insertCmd.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(record.AF_MMAC_CODE));
                insertCmd.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(record.CERTIFICATE_COE));
                insertCmd.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(record.COMPETENT_CAA));
                insertCmd.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(record.DOD_ID_CODE));
                insertCmd.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(record.DOT_EXEMPTION_NO));
                insertCmd.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(record.DOT_RQ_IND));
                insertCmd.Parameters.AddWithValue("@EX_NO", dbNull(record.EX_NO));
                insertCmd.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(record.HIGH_EXPLOSIVE_WT));
                insertCmd.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(record.LTD_QTY_IND));
                insertCmd.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(record.MAGNETIC_IND));
                insertCmd.Parameters.AddWithValue("@MAGNETISM", dbNull(record.MAGNETISM));
                insertCmd.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(record.MARINE_POLLUTANT_IND));
                insertCmd.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(record.NET_EXP_QTY_DIST));
                insertCmd.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(record.NET_EXP_WEIGHT));
                insertCmd.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(record.NET_PROPELLANT_WT));
                insertCmd.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(record.NOS_TECHNICAL_SHIPPING_NAME));
                insertCmd.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(record.TRANSPORTATION_ADDITIONAL_DATA));
                insertCmd.Parameters.AddWithValue("@msds_id", msds_id);

                object o = insertCmd.ExecuteScalar();

                result = Convert.ToInt32(o);

                tran.Commit();

                logger.InfoFormat("Transportation Inserted: {0}, {1}, {2}, {3} ID = {4}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, record.MARINE_POLLUTANT_IND, result);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting Transportation: {0}, {1}, {2} - {3}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion

        #region MSDS Update
        public long updateMSDSMasterRecord(String uic, MsdsEditViewModel model, String username) {
            MSDSRecord record = new MSDSRecord(model);
            return updateMSDSMasterRecord(uic, record, username);
        }

        public long updateMSDSMasterRecord(String uic, MSDSRecord record, String username) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            // Check to make sure we have the ID values - just in case it's coming from HMIRS
            if (record.msds_id == 0) {
                record.msds_id = getIDFromValue(
                    "msds_master", "msds_id", "MSDSSERNO", record.MSDSSERNO, conn);
            }
            //if (record.hcc_id == 0) {
            //    record.hcc_id = getIDFromValue(
            //        "HCC", "hcc_id", "hcc", record.HCC, conn);
            //}

            String stmt = "UPDATE msds_master SET MSDSSERNO = @MSDSSERNO, " +
                "CAGE = @CAGE, MANUFACTURER = @MANUFACTURER, PARTNO = @PARTNO, " +
                "FSC = @FSC, NIIN = @NIIN, hcc_id = @hcc_id, file_name = @file_name, " +
                "manually_entered = @manually_entered, ARTICLE_IND = @ARTICLE_IND, " +
                "DESCRIPTION = @DESCRIPTION, EMERGENCY_TEL = @EMERGENCY_TEL, " +
                "END_COMP_IND = @END_COMP_IND, END_ITEM_IND = @END_ITEM_IND, " +
                "KIT_IND = @KIT_IND, KIT_PART_IND = @KIT_PART_IND, " +
                "MANUFACTURER_MSDS_NO = @MANUFACTURER_MSDS_NO, MIXTURE_IND = @MIXTURE_IND, " +
                "PRODUCT_IDENTITY = @PRODUCT_IDENTITY, PRODUCT_LOAD_DATE = @PRODUCT_LOAD_DATE, " +
                "PRODUCT_RECORD_STATUS = @PRODUCT_RECORD_STATUS, " +
                "PRODUCT_REVISION_NO = @PRODUCT_REVISION_NO, PROPRIETARY_IND = @PROPRIETARY_IND, " +
                "PUBLISHED_IND = @PUBLISHED_IND, PURCHASED_PROD_IND = @PURCHASED_PROD_IND, " +
                "PURE_IND = @PURE_IND, RADIOACTIVE_IND = @RADIOACTIVE_IND, " +
                "SERVICE_AGENCY_CODE = @SERVICE_AGENCY_CODE, TRADE_NAME = @TRADE_NAME, " +
                "TRADE_SECRET_IND = @TRADE_SECRET_IND, PRODUCT_IND = @PRODUCT_IND, " +
                "PRODUCT_LANGUAGE = @PRODUCT_LANGUAGE, " +
                "data_source_cd = @data_source_cd, changed = @changed, changed_by = @changed_by " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();
            SqlCommand updateCmd = null;
            try {
                updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@MSDSSERNO", dbNull(record.MSDSSERNO));
                updateCmd.Parameters.AddWithValue("@CAGE", dbNull(record.CAGE));
                updateCmd.Parameters.AddWithValue("@MANUFACTURER", dbNull(record.MANUFACTURER));
                updateCmd.Parameters.AddWithValue("@PARTNO", dbNull(record.PARTNO));
                updateCmd.Parameters.AddWithValue("@FSC", dbNull(record.FSC));
                updateCmd.Parameters.AddWithValue("@NIIN", dbNull(record.NIIN));
                updateCmd.Parameters.AddWithValue("@hcc_id", dbNull(record.hcc_id));
                updateCmd.Parameters.AddWithValue("@file_name", dbNull(record.file_name));
                updateCmd.Parameters.AddWithValue("@manually_entered", dbNull(record.manually_entered));
                updateCmd.Parameters.AddWithValue("@ARTICLE_IND", dbNull(record.ARTICLE_IND));
                updateCmd.Parameters.AddWithValue("@DESCRIPTION", dbNull(record.DESCRIPTION));
                updateCmd.Parameters.AddWithValue("@EMERGENCY_TEL", dbNull(record.EMERGENCY_TEL));
                updateCmd.Parameters.AddWithValue("@END_COMP_IND", dbNull(record.END_COMP_IND));
                updateCmd.Parameters.AddWithValue("@END_ITEM_IND", dbNull(record.END_ITEM_IND));
                updateCmd.Parameters.AddWithValue("@KIT_IND", dbNull(record.KIT_IND));
                updateCmd.Parameters.AddWithValue("@KIT_PART_IND", dbNull(record.KIT_PART_IND));
                updateCmd.Parameters.AddWithValue("@MANUFACTURER_MSDS_NO", dbNull(record.MANUFACTURER_MSDS_NO));
                updateCmd.Parameters.AddWithValue("@MIXTURE_IND", dbNull(record.MIXTURE_IND));
                updateCmd.Parameters.AddWithValue("@PRODUCT_IDENTITY", dbNull(record.PRODUCT_IDENTITY));
                updateCmd.Parameters.AddWithValue("@PRODUCT_LOAD_DATE", dbNull(record.PRODUCT_LOAD_DATE));
                updateCmd.Parameters.AddWithValue("@PRODUCT_RECORD_STATUS", dbNull(record.PRODUCT_RECORD_STATUS));
                updateCmd.Parameters.AddWithValue("@PRODUCT_REVISION_NO", dbNull(record.PRODUCT_REVISION_NO));
                updateCmd.Parameters.AddWithValue("@PROPRIETARY_IND", dbNull(record.PROPRIETARY_IND));
                updateCmd.Parameters.AddWithValue("@PUBLISHED_IND", dbNull(record.PUBLISHED_IND));
                updateCmd.Parameters.AddWithValue("@PURCHASED_PROD_IND", dbNull(record.PURCHASED_PROD_IND));
                updateCmd.Parameters.AddWithValue("@PURE_IND", dbNull(record.PURE_IND));
                updateCmd.Parameters.AddWithValue("@RADIOACTIVE_IND", dbNull(record.RADIOACTIVE_IND));
                updateCmd.Parameters.AddWithValue("@SERVICE_AGENCY_CODE", dbNull(record.SERVICE_AGENCY_CODE));
                updateCmd.Parameters.AddWithValue("@TRADE_NAME", dbNull(record.TRADE_NAME));
                updateCmd.Parameters.AddWithValue("@TRADE_SECRET_IND", dbNull(record.TRADE_SECRET_IND));
                updateCmd.Parameters.AddWithValue("@PRODUCT_IND", dbNull(record.PRODUCT_IND));
                updateCmd.Parameters.AddWithValue("@PRODUCT_LANGUAGE", dbNull(record.PRODUCT_LANGUAGE));
                updateCmd.Parameters.AddWithValue("@data_source_cd", uic);
                updateCmd.Parameters.AddWithValue("@changed", DateTime.Now);
                updateCmd.Parameters.AddWithValue("@changed_by", username);
                updateCmd.Parameters.AddWithValue("@msds_id", record.msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSDSMaster Updated: {0}, {1}, {2}, {3} ID = {4}",
                    uic, record.MSDSSERNO, record.CAGE, record.PRODUCT_IDENTITY, record.msds_id);

                // Process the sub table info
                long subresult = updateMSDSSubTables(record, conn);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating MSDSMaster: {0}, {1}, {2} - {3}",
                    uic, record.MSDSSERNO, record.CAGE, ex.Message);
                throw ex;
            }
            finally {
                updateCmd.Dispose();
                conn.Close();
            }

            return result;
        }

        private long updateMSDSSubTables(MSDSRecord record, SqlConnection conn) {
            long result = 0;

            //MSDS CONTRACTOR LIST
            if (record.ContractorList != null && record.ContractorList.Count > 0) {
                updateMSDSContractList(record.msds_id, record.ContractorList, conn);
            }
            //MSDS DISPOSAL
            if (record.Disposal != null) {
                updateMSDSDisposalRecord(record.msds_id, record.Disposal, conn);
            }
            //MSDS DOC TYPES
            if (record.DocumentTypes != null) {
                updateMSDSDocumentTypesRecord(record.msds_id, record.DocumentTypes, conn);
            }
            // MSDS INGREDIENTS
            if (record.IngredientsList != null && record.IngredientsList.Count > 0) {
                updateMSDSIngredientsListRecord(record.msds_id, record.IngredientsList, conn);
            }
            //MSDS ITEM DESCRIPTION
            if (record.ItemDescription != null) {
                updateMSDSItemDescriptionRecord(record.msds_id, record.ItemDescription, conn);
            }
            //MSDS LABEL INFO
            if (record.LabelInfo != null) {
                updateMSDSLabelInfoRecord(record.msds_id, record.LabelInfo, conn);
            }
            //MSDS PHYS CHEMICAL TABLE
            if (record.PhysChemical != null) {
                updateMSDSPhysChemicalRecord(record.msds_id, record.PhysChemical, conn);
            }
            //MSDS RADIOLOGICAL INFO
            if (record.RadiologicalInfoList != null && record.RadiologicalInfoList.Count > 0) {
                updateMSDSRadiologicalInfoList(record.msds_id, record.RadiologicalInfoList, conn);
            }
            //MSDS TRANSPORTATION
            if (record.Transportation != null) {
                //updateMSDSTransportationRecord(record.msds_id, record.Transportation, conn);
            }

            return result;
        }

        // AFJM_PSN
        protected internal long updateMSDSAFJM_PSNRecord(MSDS_AFJM_PSN record) {
            long result = 0;

            String stmt = "UPDATE MSDS_AFJM_PSN SET AFJM_HAZARD_CLASS = @AFJM_HAZARD_CLASS, " +
                "AFJM_PACK_PARAGRAPH = @AFJM_PACK_PARAGRAPH, AFJM_PACK_GROUP = @AFJM_PACK_GROUP, " +
                "AFJM_PROP_SHIP_NAME = @AFJM_PROP_SHIP_NAME, " +
                "AFJM_PROP_SHIP_MODIFIER = @AFJM_PROP_SHIP_MODIFIER, " +
                "AFJM_SPECIAL_PROV = @AFJM_SPECIAL_PROV, " +
                "AFJM_SUBSIDIARY_RISK = @AFJM_SUBSIDIARY_RISK, AFJM_SYMBOLS = @AFJM_SYMBOLS, " +
                "AFJM_UN_ID_NUMBER = @AFJM_UN_ID_NUMBER " +
                "WHERE AFJM_PSN_CODE = @AFJM_PSN_CODE";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@AFJM_HAZARD_CLASS", dbNull(record.AFJM_HAZARD_CLASS));
                updateCmd.Parameters.AddWithValue("@AFJM_PACK_PARAGRAPH", dbNull(record.AFJM_PACK_PARAGRAPH));
                updateCmd.Parameters.AddWithValue("@AFJM_PACK_GROUP", dbNull(record.AFJM_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_NAME", dbNull(record.AFJM_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@AFJM_PROP_SHIP_MODIFIER", dbNull(record.AFJM_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@AFJM_PSN_CODE", dbNull(record.AFJM_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@AFJM_SPECIAL_PROV", dbNull(record.AFJM_SPECIAL_PROV));
                updateCmd.Parameters.AddWithValue("@AFJM_SUBSIDIARY_RISK", dbNull(record.AFJM_SUBSIDIARY_RISK));
                updateCmd.Parameters.AddWithValue("@AFJM_SYMBOLS", dbNull(record.AFJM_SYMBOLS));
                updateCmd.Parameters.AddWithValue("@AFJM_UN_ID_NUMBER", dbNull(record.AFJM_UN_ID_NUMBER));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                        logger.InfoFormat("AFJM_PSN Updated: {0}, {1}, {2}",
                            record.AFJM_PSN_CODE, record.AFJM_HAZARD_CLASS, record.AFJM_UN_ID_NUMBER);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error updating AFJM_PSN: {0}, {1} - {2}",
                        record.AFJM_PSN_CODE, record.AFJM_HAZARD_CLASS, ex.Message);
                    //throw ex;
            }
            }

            return result;
        }

        //MSDS CONTRACTOR LIST
        private long updateMSDSContractList(long msds_id, List<MSDSContractorInfo> contractorList,
            SqlConnection conn) {
            long result = 0;

            // Delete any existing contractor records for this msds_id
            result = deleteMSDSSubTableRecords(msds_id, "msds_contracts", conn);

            // Insert the new list
            result = insertMSDSContractList(msds_id, contractorList, conn);

            return result;
        }

        protected internal long updateMSDSContractorRecord(MSDSContractorInfo record) {
            long result = 0;

            String stmt = "UPDATE msds_contractor_info SET CT_CITY = @CT_CITY, " +
                "CT_COMPANY_NAME = @CT_COMPANY_NAME, CT_COUNTRY = @CT_COUNTRY, " +
                "CT_PHONE = @CT_PHONE, CT_PO_BOX = @CT_PO_BOX, " +
                "CT_ADDRESS_1 = @CT_ADDRESS_1, CT_STATE = @CT_STATE, " +
                "CT_ZIP_CODE = @CT_ZIP_CODE " +
                "WHERE CT_CAGE = @CT_CAGE";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();
                SqlTransaction tran = conn.BeginTransaction();

                try {
                    using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {
                        updateCmd.Transaction = tran;

                        updateCmd.Parameters.AddWithValue("@CT_CAGE", dbNull(record.CT_CAGE));
                        updateCmd.Parameters.AddWithValue("@CT_CITY", dbNull(record.CT_CITY));
                        updateCmd.Parameters.AddWithValue("@CT_COMPANY_NAME", dbNull(record.CT_COMPANY_NAME));
                        updateCmd.Parameters.AddWithValue("@CT_COUNTRY", dbNull(record.CT_COUNTRY));
                        updateCmd.Parameters.AddWithValue("@CT_PHONE", dbNull(record.CT_PHONE));
                        updateCmd.Parameters.AddWithValue("@CT_PO_BOX", dbNull(record.CT_PO_BOX));
                        updateCmd.Parameters.AddWithValue("@CT_ADDRESS_1", dbNull(record.CT_ADDRESS_1));
                        updateCmd.Parameters.AddWithValue("@CT_STATE", dbNull(record.CT_STATE));
                        updateCmd.Parameters.AddWithValue("@CT_ZIP_CODE", dbNull(record.CT_ZIP_CODE));

                        result = updateCmd.ExecuteNonQuery();

                        tran.Commit();

                        logger.InfoFormat("DOT_PSN updated: {0}, {1}",
                            record.CT_CAGE, record.CT_COMPANY_NAME);
                    }
                }
                catch (Exception ex) {
                    logger.ErrorFormat("Error updating DOT_PSN: {0}, {1} - {2}",
                        record.CT_CAGE, record.CT_COMPANY_NAME, ex.Message);
                    //throw ex;
                }
            }

            return result;
        }

        //MSDS DISPOSAL
        private long updateMSDSDisposalRecord(long msds_id, MSDSDisposal record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_disposal SET DISPOSAL_ADD_INFO = @DISPOSAL_ADD_INFO, " +
                "EPA_HAZ_WASTE_CODE = @EPA_HAZ_WASTE_CODE, " +
                "EPA_HAZ_WASTE_IND = @EPA_HAZ_WASTE_IND, EPA_HAZ_WASTE_NAME = @EPA_HAZ_WASTE_NAME " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@DISPOSAL_ADD_INFO", dbNull(record.DISPOSAL_ADD_INFO));
                updateCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_CODE", dbNull(record.EPA_HAZ_WASTE_CODE));
                updateCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_IND", dbNull(record.EPA_HAZ_WASTE_IND));
                updateCmd.Parameters.AddWithValue("@EPA_HAZ_WASTE_NAME", dbNull(record.EPA_HAZ_WASTE_NAME));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("Disposal updated: {0}, {1}, {2}, {3}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, record.EPA_HAZ_WASTE_IND);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating Disposal: {0}, {1}, {2} - {3}",
                    msds_id, record.EPA_HAZ_WASTE_CODE, record.EPA_HAZ_WASTE_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS DOC TYPES
        private long updateMSDSDocumentTypesRecord(long msds_id, MSDSDocumentTypes record, SqlConnection conn) {
            long result = 0;

            bool haveFiles = false;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("UPDATE msds_document_types SET ");
            if (record.msds_translated_filename != null && record.msds_translated_filename.Length > 0) {
                sb.AppendLine("msds_translated_filename = @msds_translated_filename,");
                haveFiles = true;
            }
            if (record.neshap_comp_filename != null && record.neshap_comp_filename.Length > 0) {
                sb.AppendLine("neshap_comp_filename = @neshap_comp_filename,");
                haveFiles = true;
            }
            if (record.other_docs_filename != null && record.other_docs_filename.Length > 0) {
                sb.AppendLine("other_docs_filename = @other_docs_filename,");
                haveFiles = true;
            }
            if (record.product_sheet_filename != null && record.product_sheet_filename.Length > 0) {
                sb.AppendLine("product_sheet_filename = @product_sheet_filename,");
                haveFiles = true;
            }
            if (record.transportation_cert_filename != null && record.transportation_cert_filename.Length > 0) {
                sb.AppendLine("transportation_cert_filename = @transportation_cert_filename,");
                haveFiles = true;
            }
            if (record.manufacturer_label_filename != null && record.manufacturer_label_filename.Length > 0) {
                sb.AppendLine("manufacturer_label_filename = @manufacturer_label_filename");
                haveFiles = true;
            }

            if (haveFiles) {
                // Remove the trailing comma in the set list
                if (sb.ToString().EndsWith(",\r\n"))
                    sb.Remove(sb.Length - 3, 1);

                sb.AppendLine(" WHERE msds_id = @msds_id");

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    SqlCommand updateCmd = new SqlCommand(sb.ToString(), conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);
                updateCmd.Parameters.AddWithValue("@msds_translated_filename", dbNull(record.msds_translated_filename));
                updateCmd.Parameters.AddWithValue("@neshap_comp_filename", dbNull(record.neshap_comp_filename));
                updateCmd.Parameters.AddWithValue("@other_docs_filename", dbNull(record.other_docs_filename));
                updateCmd.Parameters.AddWithValue("@product_sheet_filename", dbNull(record.product_sheet_filename));
                updateCmd.Parameters.AddWithValue("@transportation_cert_filename", dbNull(record.transportation_cert_filename));
                updateCmd.Parameters.AddWithValue("@manufacturer_label_filename", dbNull(record.manufacturer_label_filename));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("DocumentTypes updated: {0}, {1}, {2}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating DocumentTypes: {0}, {1}, {2} - {3}",
                    msds_id, record.product_sheet_filename, record.manufacturer_label_filename, ex.Message);
                throw ex;
            }
            }

            return result;
        }

        //MSDS DOT PSN
        protected internal long updateMSDSDOT_PSNRecord(MSDS_DOT_PSN record) {
            long result = 0;

            String stmt = "UPDATE MSDS_DOT_PSN SET DOT_HAZARD_CLASS_DIV = @DOT_HAZARD_CLASS_DIV, " +
                "DOT_HAZARD_LABEL = @DOT_HAZARD_LABEL, DOT_MAX_CARGO = @DOT_MAX_CARGO, " +
                "DOT_MAX_PASSENGER = @DOT_MAX_PASSENGER, DOT_PACK_BULK = @DOT_PACK_BULK, " +
                "DOT_PACK_EXCEPTIONS = @DOT_PACK_EXCEPTIONS, DOT_PACK_NONBULK = @DOT_PACK_NONBULK, " +
                "DOT_PROP_SHIP_NAME = @DOT_PROP_SHIP_NAME, DOT_PROP_SHIP_MODIFIER = @DOT_PROP_SHIP_MODIFIER, " +
                "DOT_SPECIAL_PROVISION = @DOT_SPECIAL_PROVISION, " +
                "DOT_SYMBOLS = @DOT_SYMBOLS, DOT_UN_ID_NUMBER = @DOT_UN_ID_NUMBER, " +
                "DOT_WATER_OTHER_REQ = @DOT_WATER_OTHER_REQ, DOT_WATER_VESSEL_STOW = @DOT_WATER_VESSEL_STOW " +
                "WHERE DOT_PSN_CODE = @DOT_PSN_CODE";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@DOT_HAZARD_CLASS_DIV", dbNull(record.DOT_HAZARD_CLASS_DIV));
                updateCmd.Parameters.AddWithValue("@DOT_HAZARD_LABEL", dbNull(record.DOT_HAZARD_LABEL));
                updateCmd.Parameters.AddWithValue("@DOT_MAX_CARGO", dbNull(record.DOT_MAX_CARGO));
                updateCmd.Parameters.AddWithValue("@DOT_MAX_PASSENGER", dbNull(record.DOT_MAX_PASSENGER));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_BULK", dbNull(record.DOT_PACK_BULK));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_EXCEPTIONS", dbNull(record.DOT_PACK_EXCEPTIONS));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_NONBULK", dbNull(record.DOT_PACK_NONBULK));
                updateCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_NAME", dbNull(record.DOT_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@DOT_PROP_SHIP_MODIFIER", dbNull(record.DOT_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@DOT_PSN_CODE", dbNull(record.DOT_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@DOT_SPECIAL_PROVISION", dbNull(record.DOT_SPECIAL_PROVISION));
                updateCmd.Parameters.AddWithValue("@DOT_SYMBOLS", dbNull(record.DOT_SYMBOLS));
                updateCmd.Parameters.AddWithValue("@DOT_UN_ID_NUMBER", dbNull(record.DOT_UN_ID_NUMBER));
                updateCmd.Parameters.AddWithValue("@DOT_WATER_OTHER_REQ", dbNull(record.DOT_WATER_OTHER_REQ));
                updateCmd.Parameters.AddWithValue("@DOT_WATER_VESSEL_STOW", dbNull(record.DOT_WATER_VESSEL_STOW));
                updateCmd.Parameters.AddWithValue("@DOT_PACK_GROUP", dbNull(record.DOT_PACK_GROUP));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                        logger.InfoFormat("DOT_PSN updated: {0}, {1}, {2}",
                            record.DOT_PSN_CODE, record.DOT_HAZARD_CLASS_DIV, record.DOT_HAZARD_LABEL);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error updating DOT_PSN: {0}, {1} - {2}",
                        record.DOT_PSN_CODE, record.DOT_HAZARD_CLASS_DIV, ex.Message);
                    //throw ex;
            }
            }

            return result;
        }

        //MSDS IATA PSN
        protected internal long updateMSDSIATA_PSNRecord(MSDS_IATA_PSN record) {
            long result = 0;

            String stmt = "UPDATE MSDS_IATA_PSN SET IATA_CARGO_PACKING = @IATA_CARGO_PACKING, " +
                "IATA_HAZARD_CLASS = @IATA_HAZARD_CLASS, IATA_HAZARD_LABEL = @IATA_HAZARD_LABEL, " +
                "IATA_PACK_GROUP = @IATA_PACK_GROUP, IATA_PASS_AIR_PACK_LMT_INSTR = @IATA_PASS_AIR_PACK_LMT_INSTR, " +
                "IATA_PASS_AIR_PACK_LMT_PER_PKG = @IATA_PASS_AIR_PACK_LMT_PER_PKG,  " +
                "IATA_PASS_AIR_PACK_NOTE = @IATA_PASS_AIR_PACK_NOTE, IATA_PROP_SHIP_NAME = @IATA_PROP_SHIP_NAME, " +
                "IATA_PROP_SHIP_MODIFIER = @IATA_PROP_SHIP_MODIFIER, IATA_CARGO_PACK_MAX_QTY = @IATA_CARGO_PACK_MAX_QTY, " +
                "IATA_PASS_AIR_MAX_QTY = @IATA_PASS_AIR_MAX_QTY, " +
                "IATA_SPECIAL_PROV = @IATA_SPECIAL_PROV, IATA_SUBSIDIARY_RISK = @IATA_SUBSIDIARY_RISK, " +
                "IATA_UN_ID_NUMBER = @IATA_UN_ID_NUMBER " +
                "WHERE IATA_PSN_CODE = @IATA_PSN_CODE";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@IATA_CARGO_PACKING", dbNull(record.IATA_CARGO_PACKING));
                updateCmd.Parameters.AddWithValue("@IATA_HAZARD_CLASS", dbNull(record.IATA_HAZARD_CLASS));
                updateCmd.Parameters.AddWithValue("@IATA_HAZARD_LABEL", dbNull(record.IATA_HAZARD_LABEL));
                updateCmd.Parameters.AddWithValue("@IATA_PACK_GROUP", dbNull(record.IATA_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_INSTR", dbNull(record.IATA_PASS_AIR_PACK_LMT_INSTR));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_LMT_PER_PKG", dbNull(record.IATA_PASS_AIR_PACK_LMT_PER_PKG));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_PACK_NOTE", dbNull(record.IATA_PASS_AIR_PACK_NOTE));
                updateCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_NAME", dbNull(record.IATA_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@IATA_PROP_SHIP_MODIFIER", dbNull(record.IATA_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@IATA_CARGO_PACK_MAX_QTY", dbNull(record.IATA_CARGO_PACK_MAX_QTY));
                updateCmd.Parameters.AddWithValue("@IATA_PSN_CODE", dbNull(record.IATA_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@IATA_PASS_AIR_MAX_QTY", dbNull(record.IATA_PASS_AIR_MAX_QTY));
                updateCmd.Parameters.AddWithValue("@IATA_SPECIAL_PROV", dbNull(record.IATA_SPECIAL_PROV));
                updateCmd.Parameters.AddWithValue("@IATA_SUBSIDIARY_RISK", dbNull(record.IATA_SUBSIDIARY_RISK));
                updateCmd.Parameters.AddWithValue("@IATA_UN_ID_NUMBER", dbNull(record.IATA_UN_ID_NUMBER));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                        logger.InfoFormat("IATA_PSN updated: {0}, {1}, {2}",
                            record.IATA_PSN_CODE, record.IATA_HAZARD_CLASS, record.IATA_UN_ID_NUMBER);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error updating IATA_PSN: {0}, {1} - {2}",
                        record.IATA_PSN_CODE, record.IATA_HAZARD_CLASS, ex.Message);
                    //throw ex;
                }
            }

            return result;
        }

        //MSDS IMO PSN
        private long updateMSDSIMO_PSNRecord(long msds_id, MSDS_IMO_PSN record) {
            long result = 0;

            String stmt = "UPDATE MSDS_IMO_PSN SET IMO_EMS_NO = @IMO_EMS_NO, IMO_HAZARD_CLASS = @IMO_HAZARD_CLASS, " +
                "IMO_IBC_INSTR = @IMO_IBC_INSTR, IMO_LIMITED_QTY = @IMO_LIMITED_QTY, IMO_PACK_GROUP = @IMO_PACK_GROUP, " +
                "IMO_PACK_INSTRUCTIONS = @IMO_PACK_INSTRUCTIONS, IMO_PACK_PROVISIONS = @IMO_PACK_PROVISIONS, " +
                "IMO_PROP_SHIP_NAME = @IMO_PROP_SHIP_NAME, IMO_PROP_SHIP_MODIFIER = @IMO_PROP_SHIP_MODIFIER, " +
                "IMO_SPECIAL_PROV = @IMO_SPECIAL_PROV, IMO_STOW_SEGR = @IMO_STOW_SEGR, " +
                "IMO_SUBSIDIARY_RISK = @IMO_SUBSIDIARY_RISK, IMO_TANK_INSTR_IMO = @IMO_TANK_INSTR_IMO, " +
                "IMO_TANK_INSTR_PROV = @IMO_TANK_INSTR_PROV, IMO_TANK_INSTR_UN = @IMO_TANK_INSTR_UN, " +
                "IMO_UN_NUMBER = @IMO_UN_NUMBER, IMO_IBC_PROVISIONS = @IMO_IBC_PROVISIONS " +
                "WHERE IMO_PSN_CODE = @IMO_PSN_CODE";

            using (SqlConnection conn = new SqlConnection(MDF_CONNECTION)) {
                conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                    using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@IMO_EMS_NO", dbNull(record.IMO_EMS_NO));
                updateCmd.Parameters.AddWithValue("@IMO_HAZARD_CLASS", dbNull(record.IMO_HAZARD_CLASS));
                updateCmd.Parameters.AddWithValue("@IMO_IBC_INSTR", dbNull(record.IMO_IBC_INSTR));
                updateCmd.Parameters.AddWithValue("@IMO_LIMITED_QTY", dbNull(record.IMO_LIMITED_QTY));
                updateCmd.Parameters.AddWithValue("@IMO_PACK_GROUP", dbNull(record.IMO_PACK_GROUP));
                updateCmd.Parameters.AddWithValue("@IMO_PACK_INSTRUCTIONS", dbNull(record.IMO_PACK_INSTRUCTIONS));
                updateCmd.Parameters.AddWithValue("@IMO_PACK_PROVISIONS", dbNull(record.IMO_PACK_PROVISIONS));
                updateCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_NAME", dbNull(record.IMO_PROP_SHIP_NAME));
                updateCmd.Parameters.AddWithValue("@IMO_PROP_SHIP_MODIFIER", dbNull(record.IMO_PROP_SHIP_MODIFIER));
                updateCmd.Parameters.AddWithValue("@IMO_PSN_CODE", dbNull(record.IMO_PSN_CODE));
                updateCmd.Parameters.AddWithValue("@IMO_SPECIAL_PROV", dbNull(record.IMO_SPECIAL_PROV));
                updateCmd.Parameters.AddWithValue("@IMO_STOW_SEGR", dbNull(record.IMO_STOW_SEGR));
                updateCmd.Parameters.AddWithValue("@IMO_SUBSIDIARY_RISK", dbNull(record.IMO_SUBSIDIARY_RISK));
                updateCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_IMO", dbNull(record.IMO_TANK_INSTR_IMO));
                updateCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_PROV", dbNull(record.IMO_TANK_INSTR_PROV));
                updateCmd.Parameters.AddWithValue("@IMO_TANK_INSTR_UN", dbNull(record.IMO_TANK_INSTR_UN));
                updateCmd.Parameters.AddWithValue("@IMO_UN_NUMBER", dbNull(record.IMO_UN_NUMBER));
                updateCmd.Parameters.AddWithValue("@IMO_IBC_PROVISIONS", dbNull(record.IMO_IBC_PROVISIONS));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                        logger.InfoFormat("IMO_PSN updated: {0}, {1}, {2}",
                            record.IMO_PSN_CODE, record.IMO_HAZARD_CLASS, record.IMO_UN_NUMBER);
                    }
            }
            catch (Exception ex) {
                    logger.ErrorFormat("Error updating IMO_PSN: {0}, {1} - {2}",
                        record.IMO_PSN_CODE, record.IMO_HAZARD_CLASS, ex.Message);
                    //throw ex;
                }
            }

            return result;
        }

        // MSDS INGREDIENTS
        private long updateMSDSIngredientsListRecord(long msds_id, List<MSDSIngredient> ingredientsList, SqlConnection conn) {
            long result = 0;

            // Delete any existing ingredients for this msds_id
            result = deleteMSDSSubTableRecords(msds_id, "msds_ingredients", conn);

            // Insert the new list
            result = insertMSDSIngredientsListRecord(msds_id, ingredientsList, conn);

            return result;
        }

        //MSDS ITEM DESCRIPTION
        private long updateMSDSItemDescriptionRecord(long msds_id, MSDSItemDescription record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_item_description SET ITEM_MANAGER = @ITEM_MANAGER, ITEM_NAME = @ITEM_NAME, " +
                "SPECIFICATION_NUMBER = @SPECIFICATION_NUMBER, TYPE_GRADE_CLASS = @TYPE_GRADE_CLASS, " +
                "UNIT_OF_ISSUE = @UNIT_OF_ISSUE, QUANTITATIVE_EXPRESSION = @QUANTITATIVE_EXPRESSION, " +
                "UI_CONTAINER_QTY = @UI_CONTAINER_QTY, TYPE_OF_CONTAINER = @TYPE_OF_CONTAINER, " +
                "BATCH_NUMBER = @BATCH_NUMBER, LOT_NUMBER = @LOT_NUMBER, " +
                "LOG_FLIS_NIIN_VER = @LOG_FLIS_NIIN_VER, LOG_FSC = @LOG_FSC, " +
                "NET_UNIT_WEIGHT = @NET_UNIT_WEIGHT, SHELF_LIFE_CODE = @SHELF_LIFE_CODE, " +
                "SPECIAL_EMP_CODE = @SPECIAL_EMP_CODE, UN_NA_NUMBER = @UN_NA_NUMBER, UPC_GTIN = @UPC_GTIN " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@msds_id", dbNull(msds_id));
                updateCmd.Parameters.AddWithValue("@ITEM_MANAGER", dbNull(record.ITEM_MANAGER));
                updateCmd.Parameters.AddWithValue("@ITEM_NAME", dbNull(record.ITEM_NAME));
                updateCmd.Parameters.AddWithValue("@SPECIFICATION_NUMBER", dbNull(record.SPECIFICATION_NUMBER));
                updateCmd.Parameters.AddWithValue("@TYPE_GRADE_CLASS", dbNull(record.TYPE_GRADE_CLASS));
                updateCmd.Parameters.AddWithValue("@UNIT_OF_ISSUE", dbNull(record.UNIT_OF_ISSUE));
                updateCmd.Parameters.AddWithValue("@QUANTITATIVE_EXPRESSION", dbNull(record.QUANTITATIVE_EXPRESSION));
                updateCmd.Parameters.AddWithValue("@UI_CONTAINER_QTY", dbNull(record.UI_CONTAINER_QTY));
                updateCmd.Parameters.AddWithValue("@TYPE_OF_CONTAINER", dbNull(record.TYPE_OF_CONTAINER));
                updateCmd.Parameters.AddWithValue("@BATCH_NUMBER", dbNull(record.BATCH_NUMBER));
                updateCmd.Parameters.AddWithValue("@LOT_NUMBER", dbNull(record.LOT_NUMBER));
                updateCmd.Parameters.AddWithValue("@LOG_FLIS_NIIN_VER", dbNull(record.LOG_FLIS_NIIN_VER));
                updateCmd.Parameters.AddWithValue("@LOG_FSC", dbNull(record.LOG_FSC));
                updateCmd.Parameters.AddWithValue("@NET_UNIT_WEIGHT", dbNull(record.NET_UNIT_WEIGHT));
                updateCmd.Parameters.AddWithValue("@SHELF_LIFE_CODE", dbNull(record.SHELF_LIFE_CODE));
                updateCmd.Parameters.AddWithValue("@SPECIAL_EMP_CODE", dbNull(record.SPECIAL_EMP_CODE));
                updateCmd.Parameters.AddWithValue("@UN_NA_NUMBER", dbNull(record.UN_NA_NUMBER));
                updateCmd.Parameters.AddWithValue("@UPC_GTIN", dbNull(record.UPC_GTIN));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("ItemDescription updated: {0}, {1}, {2}, {3}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, record.UN_NA_NUMBER);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating ItemDescription: {0}, {1}, {2} - {3}",
                    msds_id, record.ITEM_NAME, record.SPECIFICATION_NUMBER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS LABEL INFO
        private long updateMSDSLabelInfoRecord(long msds_id, MSDSLabelInfo record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_label_info SET COMPANY_CAGE_RP = @COMPANY_CAGE_RP, " +
                "COMPANY_NAME_RP = @COMPANY_NAME_RP, LABEL_EMERG_PHONE = @LABEL_EMERG_PHONE, " +
                "LABEL_ITEM_NAME = @LABEL_ITEM_NAME, LABEL_PROC_YEAR = @LABEL_PROC_YEAR, " +
                "LABEL_PROD_IDENT = @LABEL_PROD_IDENT, LABEL_PROD_SERIALNO = @LABEL_PROD_SERIALNO, " +
                "LABEL_SIGNAL_WORD_CODE = @LABEL_SIGNAL_WORD_CODE, LABEL_STOCK_NO = @LABEL_STOCK_NO, " +
                "SPECIFIC_HAZARDS = @SPECIFIC_HAZARDS " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@COMPANY_CAGE_RP", dbNull(record.COMPANY_CAGE_RP));
                updateCmd.Parameters.AddWithValue("@COMPANY_NAME_RP", dbNull(record.COMPANY_NAME_RP));
                updateCmd.Parameters.AddWithValue("@LABEL_EMERG_PHONE", dbNull(record.LABEL_EMERG_PHONE));
                updateCmd.Parameters.AddWithValue("@LABEL_ITEM_NAME", dbNull(record.LABEL_ITEM_NAME));
                updateCmd.Parameters.AddWithValue("@LABEL_PROC_YEAR", dbNull(record.LABEL_PROC_YEAR));
                updateCmd.Parameters.AddWithValue("@LABEL_PROD_IDENT", dbNull(record.LABEL_PROD_IDENT));
                updateCmd.Parameters.AddWithValue("@LABEL_PROD_SERIALNO", dbNull(record.LABEL_PROD_SERIALNO));
                updateCmd.Parameters.AddWithValue("@LABEL_SIGNAL_WORD_CODE", dbNull(record.LABEL_SIGNAL_WORD_CODE));
                updateCmd.Parameters.AddWithValue("@LABEL_STOCK_NO", dbNull(record.LABEL_STOCK_NO));
                updateCmd.Parameters.AddWithValue("@SPECIFIC_HAZARDS", dbNull(record.SPECIFIC_HAZARDS));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();


                tran.Commit();

                logger.InfoFormat("LabelInfo updated: {0}, {1}, {2}, {3}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, record.COMPANY_NAME_RP);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating LabelInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.LABEL_PROD_IDENT, record.LABEL_ITEM_NAME, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS PHYS CHEMICAL TABLE
        private long updateMSDSPhysChemicalRecord(long msds_id, MSDSPhysChemical record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_phys_chemical SET " +
                "VAPOR_PRESS = @VAPOR_PRESS, VAPOR_DENS = @VAPOR_DENS, SPECIFIC_GRAV = @SPECIFIC_GRAV, " +
                "VOC_POUNDS_GALLON = @VOC_POUNDS_GALLON, VOC_GRAMS_LITER = @VOC_GRAMS_LITER, PH = @PH, VISCOSITY = @VISCOSITY, " +
                "EVAP_RATE_REF = @EVAP_RATE_REF, SOL_IN_WATER = @SOL_IN_WATER, APP_ODOR = @APP_ODOR, " +
                "PERCENT_VOL_VOLUME = @PERCENT_VOL_VOLUME, AUTOIGNITION_TEMP = @AUTOIGNITION_TEMP, CARCINOGEN_IND = @CARCINOGEN_IND, " +
                "EPA_ACUTE = @EPA_ACUTE, EPA_CHRONIC = @EPA_CHRONIC, EPA_FIRE = @EPA_FIRE, EPA_PRESSURE = @EPA_PRESSURE, " +
                "EPA_REACTIVITY = @EPA_REACTIVITY, FLASH_PT_TEMP = @FLASH_PT_TEMP, NEUT_AGENT = @NEUT_AGENT, " +
                "NFPA_FLAMMABILITY = @NFPA_FLAMMABILITY, NFPA_HEALTH = @NFPA_HEALTH, NFPA_REACTIVITY = @NFPA_REACTIVITY, " +
                "NFPA_SPECIAL = @NFPA_SPECIAL, " +
                "OSHA_CARCINOGENS = @OSHA_CARCINOGENS, OSHA_COMB_LIQUID = @OSHA_COMB_LIQUID, OSHA_COMP_GAS = @OSHA_COMP_GAS, " +
                "OSHA_CORROSIVE = @OSHA_CORROSIVE, OSHA_EXPLOSIVE = @OSHA_EXPLOSIVE, OSHA_FLAMMABLE = @OSHA_FLAMMABLE, " +
                "OSHA_HIGH_TOXIC = @OSHA_HIGH_TOXIC, OSHA_IRRITANT = @OSHA_IRRITANT, OSHA_ORG_PEROX = @OSHA_ORG_PEROX, " +
                "OSHA_OTHERLONGTERM = @OSHA_OTHERLONGTERM, OSHA_OXIDIZER = @OSHA_OXIDIZER, OSHA_PYRO = @OSHA_PYRO, " +
                "OSHA_SENSITIZER = @OSHA_SENSITIZER, OSHA_TOXIC = @OSHA_TOXIC, OSHA_UNST_REACT = @OSHA_UNST_REACT, " +
                "OTHER_SHORT_TERM = @OTHER_SHORT_TERM, PHYS_STATE_CODE = @PHYS_STATE_CODE, VOL_ORG_COMP_WT = @VOL_ORG_COMP_WT, " +
                "OSHA_WATER_REACTIVE = @OSHA_WATER_REACTIVE " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand updateCmd = new SqlCommand(stmt, conn);
                updateCmd.Transaction = tran;

                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);
                updateCmd.Parameters.AddWithValue("@VAPOR_PRESS", dbNull(record.VAPOR_PRESS));
                updateCmd.Parameters.AddWithValue("@VAPOR_DENS", dbNull(record.VAPOR_DENS));
                updateCmd.Parameters.AddWithValue("@SPECIFIC_GRAV", dbNull(record.SPECIFIC_GRAV));
                updateCmd.Parameters.AddWithValue("@VOC_POUNDS_GALLON", dbNull(record.VOC_POUNDS_GALLON));
                updateCmd.Parameters.AddWithValue("@VOC_GRAMS_LITER", dbNull(record.VOC_GRAMS_LITER));
                updateCmd.Parameters.AddWithValue("@PH", dbNull(record.PH));
                updateCmd.Parameters.AddWithValue("@VISCOSITY", dbNull(record.VISCOSITY));
                updateCmd.Parameters.AddWithValue("@EVAP_RATE_REF", dbNull(record.EVAP_RATE_REF));
                updateCmd.Parameters.AddWithValue("@SOL_IN_WATER", dbNull(record.SOL_IN_WATER));
                updateCmd.Parameters.AddWithValue("@APP_ODOR", dbNull(record.APP_ODOR));
                updateCmd.Parameters.AddWithValue("@PERCENT_VOL_VOLUME", dbNull(record.PERCENT_VOL_VOLUME));
                updateCmd.Parameters.AddWithValue("@AUTOIGNITION_TEMP", dbNull(record.AUTOIGNITION_TEMP));
                updateCmd.Parameters.AddWithValue("@CARCINOGEN_IND", dbNull(record.CARCINOGEN_IND));
                updateCmd.Parameters.AddWithValue("@EPA_ACUTE", dbNull(record.EPA_ACUTE));
                updateCmd.Parameters.AddWithValue("@EPA_CHRONIC", dbNull(record.EPA_CHRONIC));
                updateCmd.Parameters.AddWithValue("@EPA_FIRE", dbNull(record.EPA_FIRE));
                updateCmd.Parameters.AddWithValue("@EPA_PRESSURE", dbNull(record.EPA_PRESSURE));
                updateCmd.Parameters.AddWithValue("@EPA_REACTIVITY", dbNull(record.EPA_REACTIVITY));
                updateCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                updateCmd.Parameters.AddWithValue("@NEUT_AGENT", dbNull(record.NEUT_AGENT));
                updateCmd.Parameters.AddWithValue("@NFPA_FLAMMABILITY", dbNull(record.NFPA_FLAMMABILITY));
                updateCmd.Parameters.AddWithValue("@NFPA_HEALTH", dbNull(record.NFPA_HEALTH));
                updateCmd.Parameters.AddWithValue("@NFPA_REACTIVITY", dbNull(record.NFPA_REACTIVITY));
                updateCmd.Parameters.AddWithValue("@NFPA_SPECIAL", dbNull(record.NFPA_SPECIAL));
                updateCmd.Parameters.AddWithValue("@OSHA_CARCINOGENS", dbNull(record.OSHA_CARCINOGENS));
                updateCmd.Parameters.AddWithValue("@OSHA_COMB_LIQUID", dbNull(record.OSHA_COMB_LIQUID));
                updateCmd.Parameters.AddWithValue("@OSHA_COMP_GAS", dbNull(record.OSHA_COMP_GAS));
                updateCmd.Parameters.AddWithValue("@OSHA_CORROSIVE", dbNull(record.OSHA_CORROSIVE));
                updateCmd.Parameters.AddWithValue("@OSHA_EXPLOSIVE", dbNull(record.OSHA_EXPLOSIVE));
                updateCmd.Parameters.AddWithValue("@OSHA_FLAMMABLE", dbNull(record.OSHA_FLAMMABLE));
                updateCmd.Parameters.AddWithValue("@OSHA_HIGH_TOXIC", dbNull(record.OSHA_HIGH_TOXIC));
                updateCmd.Parameters.AddWithValue("@OSHA_IRRITANT", dbNull(record.OSHA_IRRITANT));
                updateCmd.Parameters.AddWithValue("@OSHA_ORG_PEROX", dbNull(record.OSHA_ORG_PEROX));
                updateCmd.Parameters.AddWithValue("@OSHA_OTHERLONGTERM", dbNull(record.OSHA_OTHERLONGTERM));
                updateCmd.Parameters.AddWithValue("@OSHA_OXIDIZER", dbNull(record.OSHA_OXIDIZER));
                updateCmd.Parameters.AddWithValue("@OSHA_PYRO", dbNull(record.OSHA_PYRO));
                updateCmd.Parameters.AddWithValue("@OSHA_SENSITIZER", dbNull(record.OSHA_SENSITIZER));
                updateCmd.Parameters.AddWithValue("@OSHA_TOXIC", dbNull(record.OSHA_TOXIC));
                updateCmd.Parameters.AddWithValue("@OSHA_UNST_REACT", dbNull(record.OSHA_UNST_REACT));
                updateCmd.Parameters.AddWithValue("@OTHER_SHORT_TERM", dbNull(record.OTHER_SHORT_TERM));
                updateCmd.Parameters.AddWithValue("@PHYS_STATE_CODE", dbNull(record.PHYS_STATE_CODE));
                updateCmd.Parameters.AddWithValue("@VOL_ORG_COMP_WT", dbNull(record.VOL_ORG_COMP_WT));
                updateCmd.Parameters.AddWithValue("@OSHA_WATER_REACTIVE", dbNull(record.OSHA_WATER_REACTIVE));

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("PhysChemical updated: {0}, {1}, {2}, {3}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, record.APP_ODOR);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating PhysChemical: {0}, {1}, {2} - {3}",
                    msds_id, record.SPECIFIC_GRAV, record.SOL_IN_WATER, ex.Message);
                throw ex;
            }

            return result;
        }

        //MSDS RADIOLOGICAL INFO
        private long updateMSDSRadiologicalInfoList(long msds_id, List<MSDSRadiologicalInfo> records, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_radiological_info SET NRC_LP_NUM = @NRC_LP_NUM, " +
                "OPERATOR = @OPERATOR, RAD_AMOUNT_MICRO = @RAD_AMOUNT_MICRO, RAD_FORM = @RAD_FORM, RAD_CAS = @RAD_CAS, " +
                "RAD_NAME = @RAD_NAME, REP_NSN = @REP_NSN, SEALED = @SEALED " +
                "WHERE msds_id = @msds_id AND RAD_SYMBOL = @RAD_SYMBOL";

            SqlTransaction tran = conn.BeginTransaction();

            using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {
                foreach (MSDSRadiologicalInfo record in records) {

            try {
                updateCmd.Transaction = tran;
                        updateCmd.Parameters.Clear();

                updateCmd.Parameters.AddWithValue("@NRC_LP_NUM", dbNull(record.NRC_LP_NUM));
                updateCmd.Parameters.AddWithValue("@OPERATOR", dbNull(record.OPERATOR));
                updateCmd.Parameters.AddWithValue("@RAD_AMOUNT_MICRO", dbNull(record.RAD_AMOUNT_MICRO));
                updateCmd.Parameters.AddWithValue("@RAD_FORM", dbNull(record.RAD_FORM));
                updateCmd.Parameters.AddWithValue("@RAD_CAS", dbNull(record.RAD_CAS));
                updateCmd.Parameters.AddWithValue("@RAD_NAME", dbNull(record.RAD_NAME));
                updateCmd.Parameters.AddWithValue("@RAD_SYMBOL", dbNull(record.RAD_SYMBOL));
                updateCmd.Parameters.AddWithValue("@REP_NSN", dbNull(record.REP_NSN));
                updateCmd.Parameters.AddWithValue("@SEALED", dbNull(record.SEALED));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                        logger.InfoFormat("RadiologicalInfo updated: {0}, {1}, {2}, {3}",
                    msds_id, record.RAD_CAS, record.RAD_SYMBOL, record.RAD_FORM);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updateing RadiologicalInfo: {0}, {1}, {2} - {3}",
                    msds_id, record.RAD_CAS, record.RAD_SYMBOL, ex.Message);
                throw ex;
            }
                }
            }

            return result;
        }

        //MSDS TRANSPORTATION
        private long updateMSDSTransportationRecord(long msds_id, MSDSTransportation record, SqlConnection conn) {
            long result = 0;

            String stmt = "UPDATE msds_transportation SET " +
                "afjm_psn_id = @afjm_psn_id, dot_psn_id = @dot_psn_id, " +
                "iata_psn_id = @iata_psn_id, imo_psn_id = @imo_psn_id, " +
                "AF_MMAC_CODE = @AF_MMAC_CODE, CERTIFICATE_COE = @CERTIFICATE_COE, " +
                "COMPETENT_CAA = @COMPETENT_CAA, DOD_ID_CODE = @DOD_ID_CODE, DOT_EXEMPTION_NO = @DOT_EXEMPTION_NO, " +
                "DOT_RQ_IND = @DOT_RQ_IND, EX_NO = @EX_NO, " +
                "HIGH_EXPLOSIVE_WT = @HIGH_EXPLOSIVE_WT, LTD_QTY_IND = @LTD_QTY_IND, " +
                "MAGNETIC_IND = @MAGNETIC_IND, MAGNETISM = @MAGNETISM, MARINE_POLLUTANT_IND = @MARINE_POLLUTANT_IND, " +
                "NET_EXP_QTY_DIST = @NET_EXP_QTY_DIST, NET_EXP_WEIGHT = @NET_EXP_WEIGHT, NET_PROPELLANT_WT = @NET_PROPELLANT_WT, " +
                "NOS_TECHNICAL_SHIPPING_NAME = @NOS_TECHNICAL_SHIPPING_NAME, " +
                "TRANSPORTATION_ADDITIONAL_DATA = @TRANSPORTATION_ADDITIONAL_DATA " +
                "WHERE msds_id = @msds_id";

            SqlTransaction tran = conn.BeginTransaction();

            using (SqlCommand updateCmd = new SqlCommand(stmt, conn)) {

            try {
                updateCmd.Transaction = tran;
                    updateCmd.Parameters.Clear();

                    // Get the PSN ids
                    long afjm_psn_id = getIDFromValue("MSDS_AFJM_PSN", "afjm_psn_id",
                        "AFJM_PSN_CODE", record.AFJM_PSN_CODE);
                    long dot_psn_id = getIDFromValue("MSDS_DOT_PSN", "dot_psn_id",
                        "DOT_PSN_CODE", record.DOT_PSN_CODE);
                    long iata_psn_id = getIDFromValue("MSDS_IATA_PSN", "iata_psn_id",
                        "IATA_PSN_CODE", record.IATA_PSN_CODE);
                    long imo_psn_id = getIDFromValue("MSDS_IMO_PSN", "imo_psn_id",
                        "IMO_PSN_CODE", record.IMO_PSN_CODE);

                    updateCmd.Parameters.AddWithValue("@afjm_psn_id", afjm_psn_id);
                    updateCmd.Parameters.AddWithValue("@dot_psn_id", dot_psn_id);
                    updateCmd.Parameters.AddWithValue("@iata_psn_id", iata_psn_id);
                    updateCmd.Parameters.AddWithValue("@imo_psn_id", imo_psn_id);

                updateCmd.Parameters.AddWithValue("@AF_MMAC_CODE", dbNull(record.AF_MMAC_CODE));
                updateCmd.Parameters.AddWithValue("@CERTIFICATE_COE", dbNull(record.CERTIFICATE_COE));
                updateCmd.Parameters.AddWithValue("@COMPETENT_CAA", dbNull(record.COMPETENT_CAA));
                updateCmd.Parameters.AddWithValue("@DOD_ID_CODE", dbNull(record.DOD_ID_CODE));
                updateCmd.Parameters.AddWithValue("@DOT_EXEMPTION_NO", dbNull(record.DOT_EXEMPTION_NO));
                updateCmd.Parameters.AddWithValue("@DOT_RQ_IND", dbNull(record.DOT_RQ_IND));
                updateCmd.Parameters.AddWithValue("@EX_NO", dbNull(record.EX_NO));
                    //updateCmd.Parameters.AddWithValue("@FLASH_PT_TEMP", dbNull(record.FLASH_PT_TEMP));
                    //updateCmd.Parameters.AddWithValue("@HCC", dbNull(record.HCC));
                updateCmd.Parameters.AddWithValue("@HIGH_EXPLOSIVE_WT", dbNull(record.HIGH_EXPLOSIVE_WT));
                updateCmd.Parameters.AddWithValue("@LTD_QTY_IND", dbNull(record.LTD_QTY_IND));
                updateCmd.Parameters.AddWithValue("@MAGNETIC_IND", dbNull(record.MAGNETIC_IND));
                updateCmd.Parameters.AddWithValue("@MAGNETISM", dbNull(record.MAGNETISM));
                updateCmd.Parameters.AddWithValue("@MARINE_POLLUTANT_IND", dbNull(record.MARINE_POLLUTANT_IND));
                updateCmd.Parameters.AddWithValue("@NET_EXP_QTY_DIST", dbNull(record.NET_EXP_QTY_DIST));
                updateCmd.Parameters.AddWithValue("@NET_EXP_WEIGHT", dbNull(record.NET_EXP_WEIGHT));
                updateCmd.Parameters.AddWithValue("@NET_PROPELLANT_WT", dbNull(record.NET_PROPELLANT_WT));
                updateCmd.Parameters.AddWithValue("@NOS_TECHNICAL_SHIPPING_NAME", dbNull(record.NOS_TECHNICAL_SHIPPING_NAME));
                updateCmd.Parameters.AddWithValue("@TRANSPORTATION_ADDITIONAL_DATA", dbNull(record.TRANSPORTATION_ADDITIONAL_DATA));
                updateCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = updateCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("Transportation updated: {0}, {1}, {2}, {3}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, record.MARINE_POLLUTANT_IND);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error updating Transportation: {0}, {1}, {2} - {3}",
                    msds_id, record.DOT_RQ_IND, record.LTD_QTY_IND, ex.Message);
                throw ex;
            }
            }

            return result;
        }
        #endregion

        #region MSDS Deletes
        public long deleteMSDSMasterRecord(String uic, MSDSRecord record, String userName) {
            long result = 0;

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            String stmt = "DELETE FROM msds_master WHERE msds_id = @msds_id";

            SqlTransaction tran = null;

            SqlCommand deleteCmd = new SqlCommand(stmt, conn);

            try {

                // Subtable data will be deleted automatically from FK CASCADE

                tran = conn.BeginTransaction();

                deleteCmd.Transaction = tran;

                deleteCmd.Parameters.AddWithValue("@msds_id", record.msds_id);

                result = deleteCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSDS Record deleted: {0} ID = {1}",
                    result, record.msds_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleting MSDS Record: {0} - {1}",
                    record.msds_id, ex.Message);
            }
            finally {
                deleteCmd.Dispose();
                conn.Close();
            }

            return result;
        }

        private long deleteMSDSSubTables(MSDSRecord record, SqlConnection conn) {
            long result = 0;

            //MSDS CONTRACTOR LIST
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_contractor_info", conn);

            //MSDS DISPOSAL
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_disposal", conn);

            //MSDS DOC TYPES
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_document_types", conn);

            // MSDS INGREDIENTS
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_ingredients", conn);

            //MSDS ITEM DESCRIPTION
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_item_description", conn);

            //MSDS LABEL INFO
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_label_info", conn);

            //MSDS PHYS CHEMICAL TABLE
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_phys_chemical", conn);

            //MSDS RADIOLOGICAL INFO
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_radiological_info", conn);

            //MSDS TRANSPORTATION
            result = deleteMSDSSubTableRecords(record.msds_id, "msds_transportation", conn);

            return result;
        }

        private long deleteMSDSSubTableRecords(long msds_id, String tableName, SqlConnection conn) {
            long result = 0;

            String stmt = "DELETE FROM " + tableName + " WHERE msds_id = @msds_id";

            SqlTransaction tran = null;

            SqlCommand deleteCmd = new SqlCommand(stmt, conn);

            try {
                tran = conn.BeginTransaction();

                deleteCmd.Transaction = tran;

                deleteCmd.Parameters.AddWithValue("@msds_id", msds_id);

                result = deleteCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("MSDS {0} data deleted: {1} ID = {2}",
                    tableName, result, msds_id);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error deleting MSDS {0} data: {1} - {2}",
                    tableName, msds_id, ex.Message);
            }

            return result;
        }
        #endregion
    #endregion

    #region File Tables
        public bool updateFileTable(FILE_TABLES fileTable, String uic, String fileName, String fileType,
            DateTime fileDate, DateTime processedDate, String username, 
            int records_added, int records_changed = 0, int records_deleted = 0) {
            bool result = false;
            string sqlStmt = "INSERT INTO file_uploads (" +
                "file_name, file_type, file_date, upload_date, uploaded_by, uic, " +
                "records_added, records_changed, records_deleted) " +
                "VALUES(@file_name, @file_type, @file_date, @process_date, @processed_by, " +
                "@uic , @records_added, @records_changed, @records_deleted)";
            
            if (fileTable == FILE_TABLES.FileDownLoad)
                sqlStmt = "INSERT INTO file_downloads (" +
                    "file_name, file_type, file_date, download_date, downloaded_by, " +
                    "uic, records_added) " +
                    "VALUES(@file_name, @file_type, @file_date, @process_date, @processed_by, " +
                    "@uic, @records_added)";

            SqlConnection conn = new SqlConnection(MDF_CONNECTION);
            conn.Open();

            SqlTransaction tran = conn.BeginTransaction();

            try {
                SqlCommand insertCmd = new SqlCommand(sqlStmt, conn);
                insertCmd.Transaction = tran;

                //insert rows
                insertCmd.Parameters.AddWithValue("@file_name", fileName);
                insertCmd.Parameters.AddWithValue("@file_type", fileType);
                insertCmd.Parameters.AddWithValue("@file_date", fileDate);
                insertCmd.Parameters.AddWithValue("@process_date", processedDate);
                insertCmd.Parameters.AddWithValue("@processed_by", username);
                insertCmd.Parameters.AddWithValue("@uic", uic);
                insertCmd.Parameters.AddWithValue("@records_added", records_added);
                insertCmd.Parameters.AddWithValue("@records_changed", records_changed);
                insertCmd.Parameters.AddWithValue("@records_deleted", records_deleted);

                insertCmd.ExecuteNonQuery();

                tran.Commit();

                logger.InfoFormat("FileRecord Inserted to: {0} - {1}, {2}, {3}, {4}",
                    fileTable.ToString(), uic, fileType, fileName, fileDate);
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error inserting FileRecord to: {0} - {1}, {2}, {3}, {4} - {5}",
                    fileTable.ToString(), uic, fileType, fileName, fileDate, ex.Message);
                throw ex;
            }
            finally {
                conn.Close();
            }

            return result;
        }
    #endregion

    #region Utilities
        public long getIDFromValue(String tableName, String idField,
                String descField, String value) {
            return getIDFromValue(tableName, idField,
            descField, value, null);
        }
        private long getIDFromValue(String tableName, String idField,
            String descField, String value, SqlConnection conn) {
            long result = 0;
            bool bNewConn = false;

            // Kick out if we have a null value 
            if (value == null)
                return result;

            // Create a connection if we don't have one - used to avoid having connections in UI
            if (conn == null) {
                conn = new SqlConnection(MDF_CONNECTION);
                bNewConn = true;
            }
            if (conn.State != ConnectionState.Open)
                conn.Open();

            String stmt = "SELECT " + idField + " FROM " + tableName + " WHERE " + descField + " = @value";
            SqlCommand cmd = null;

            try {
                cmd = new SqlCommand(stmt, conn);
                cmd.Parameters.AddWithValue("@value", value);

                Object o = cmd.ExecuteScalar();

                result = (Int64)o;
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error in getIDFromValue: {0}, {1}, {2}, - {3}",
                    tableName, descField, value, ex.Message);
            }
            finally {
                cmd.Dispose();
            }

            return result;
        }

        public void checkForUpdates() {
            DatabaseUpdater databaseUpdater = new DatabaseUpdater();
            SqlConnection connection = null;

            try {
                if (MDF_CONNECTION != null) {
                    connection = new SqlConnection(MDF_CONNECTION);

                    databaseUpdater.update(connection);
                }
                else
                    logger.Error("No HAZMAT ConnectionString");
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error occured while applying database updates - {0}",
                    ex.Message + ex.StackTrace);
            }
            finally {
                if (connection != null)
                    connection.Close();
            }

            // Make sure we have backup directory
            checkForBackupFolder();
        }

        private void checkForBackupFolder() {
            try {
                // See if the backup folder exists
                if (!Directory.Exists(DB_BACKUP_DIRECTORY)) {
                    // Create the folder
                    Directory.CreateDirectory(DB_BACKUP_DIRECTORY);
                    logger.InfoFormat("Created backup directory - {0}", DB_BACKUP_DIRECTORY);
                }
            }
            catch (Exception ex) {
                logger.ErrorFormat("Error creating backup directory - {0}", ex.Message);
            }
        }

        private static object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            }
            else {
                return DBNull.Value;
            }
        }

    #endregion
    }
}