﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AESCommon.DatabaseUpdate;

namespace DatabaseUpdate {
    public class DBUpdate20110906 : AESCommon.DatabaseUpdate.IDatabaseUpdate {
        SqlConnection connection = null;
        SqlCommand command = null;
        public int update(SqlConnection connection) {
            int result = 0;

            // Set the connection and command
            this.connection = connection;
            command = connection.CreateCommand();

            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            // Set the database
            //command.CommandText = "USE HILT_HUB";
            //result = command.ExecuteNonQuery();

            // Create the views
            // AUL Inventory
            result = createAULInventoryDetailViews();
            // MSDS Inventory
            result = createMSDSInventoryDetailViews();
            // MSSL Inventory
            result = createMSSLInventoryDetailViews();

            return result;
        }

        private int createAULInventoryDetailViews() {
                    int result = 0;
            StringBuilder sb = new StringBuilder();

            // Drop the existing view
            sb.Append("-- AUL Inventory Detail record\n");
            sb.Append("IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS\n");
            sb.Append("        WHERE TABLE_NAME = 'v_AUL_Inventory_Detail')\n");
            sb.Append("    DROP VIEW v_AUL_Inventory_Detail\n");

            command.CommandText = sb.ToString();
            try {
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                Console.WriteLine("\tException dropping v_AUL_Inventory_Detail - "
                    + ex.Message);
            }

            // Recreate the view
            if (result != 0) {
                sb.Clear();
                sb.Append("CREATE VIEW v_AUL_Inventory_Detail AS\n");
                sb.Append("	SELECT \n");
                sb.Append("		inv.uic AS UIC,\n");
                sb.Append("		inv.niin AS NIIN,\n");
                sb.Append("		inv.qty AS QTY,\n");
                sb.Append("		inv.allowance_qty AS Allowance_QTY,\n");
                sb.Append("		COALESCE(inv.manually_entered, '0') AS Manually_Entered,\n");
                sb.Append("		COALESCE(inv.cage, '') AS CAGE,\n");
                sb.Append("		COALESCE(inv.msds_num, '') AS MSDS_Num,\n");
                sb.Append("		inv.created AS Created,\n");
                sb.Append("		COALESCE(mstr.ui, '') AS UI,\n");
                sb.Append("		COALESCE(mstr.um, '') AS UM,\n");
                sb.Append("		sh.hull_type AS Hull_Type,\n");
                sb.Append("		sh.name AS Ship_Name,\n");
                sb.Append("		sh.hull_number AS Ship_Number,\n");
                sb.Append("		mstr.usage_category_id,\n");
                sb.Append("		COALESCE(uc.category, '') AS Usage_Category,\n");
                sb.Append("		COALESCE(uc.description, '') AS Usage_Category_Description,\n");
                sb.Append("		mstr.description AS Description,\n");
                sb.Append("		mstr.smcc_id,\n");
                sb.Append("		COALESCE(smcc.smcc, '') AS SMCC,\n");
                sb.Append("		COALESCE(smcc.description, '') AS SMCC_Description,\n");
                sb.Append("		mstr.specs AS Specs,\n");
                sb.Append("		mstr.shelf_life_code_id,\n");
                sb.Append("		COALESCE(slc.slc, '') AS Shelf_Life_Code,\n");
                sb.Append("		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,\n");
                sb.Append("		COALESCE(slc.description, '') AS Shelf_Life_Description,\n");
                sb.Append("		COALESCE(slc.type, '') AS Shelf_Life_Type,\n");
                sb.Append("		mstr.shelf_life_action_code_id,\n");
                sb.Append("		COALESCE(slac.slac, '') AS Shelf_Life_Action_Code,\n");
                sb.Append("		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,\n");
                sb.Append("		CONVERT(NVARCHAR(2000), mstr.remarks) AS Remarks,\n");
                sb.Append("		mstr.storage_type_id,\n");
                sb.Append("		COALESCE(stor.type, '') AS Storage_Type,\n");
                sb.Append("		COALESCE(stor.description, '') AS Storage_Type_Description,\n");
                sb.Append("		mstr.cog_id,\n");
                sb.Append("		COALESCE(cog.cog, '') AS COG,\n");
                sb.Append("		COALESCE(cog.description, '') AS COG_Description,\n");
                sb.Append("		mstr.spmig AS SPMIG,\n");
                sb.Append("		mstr.nehc_rpt AS NEHC_Report,\n");
                sb.Append("		mstr.catalog_group_id,\n");
                sb.Append("		mstr.catalog_serial_number AS Catalog_Serial_Number,\n");
                sb.Append("		mstr.dropped_in_error AS Dropped_In_Error,\n");
                sb.Append("		mstr.manufacturer AS Manufacturer,\n");
                sb.Append("		mstr.data_source_cd AS Data_Source_CD		\n");
                sb.Append("	FROM AUL_Inventory inv\n");
                sb.Append("	INNER JOIN Ships sh on sh.uic = inv.uic\n");
                sb.Append("	INNER JOIN Auth_Use_List mstr ON mstr.niin = inv.niin and mstr.hull_type = sh.hull_type\n");
                sb.Append("	LEFT OUTER JOIN Usage_Category uc ON uc.usage_category_id = mstr.usage_category_id\n");
                sb.Append("	LEFT OUTER JOIN SMCC smcc ON smcc.smcc_id = mstr.smcc_id\n");
                sb.Append("	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.shelf_life_code_id = mstr.shelf_life_code_id\n");
                sb.Append("	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.shelf_life_action_code_id = mstr.shelf_life_action_code_id\n");
                sb.Append("	LEFT OUTER JOIN Storage_Type stor ON stor.storage_type_id = mstr.storage_type_id\n");
                sb.Append("	LEFT OUTER JOIN Cog_Codes cog ON cog.cog_id = mstr.cog_id;\n");

                command.CommandText = sb.ToString();

                try {
                    result = command.ExecuteNonQuery();
                }
                catch (Exception ex) {
                    Console.WriteLine("\tException creating v_AUL_Inventory_Detail - " 
                        + ex.Message);
                }
            }

            return result;
        }

        private int createMSDSInventoryDetailViews() {
            int result = 0;
            StringBuilder sb = new StringBuilder();

            // Drop the existing view
            sb.Append("-- MSDS Inventory details record\n");
            sb.Append("IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS\n");
            sb.Append("        WHERE TABLE_NAME = 'v_MSDS_Inventory_Detail')\n");
            sb.Append("    DROP VIEW v_MSDS_Inventory_Detail\n");

            command.CommandText = sb.ToString();
            try {
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                Console.WriteLine("\tException dropping v_MSDS_Inventory_Detail - "
                    + ex.Message);
            }

            // Recreate the view
            if (result != 0) {
                sb.Clear();
                sb.Append("CREATE VIEW v_MSDS_Inventory_Detail AS\n");
                sb.Append("	SELECT \n");
                sb.Append("		inv.uic AS UIC,\n");
                sb.Append("		inv.niin AS NIIN,\n");
                sb.Append("		inv.MSDSSERNO,\n");
                sb.Append("		inv.CAGE,\n");
                sb.Append("		inv.manually_entered AS Manually_Entered,\n");
                sb.Append("		inv.created AS Created,\n");
                sb.Append("		sh.hull_type AS Hull_Type,\n");
                sb.Append("		sh.name AS Ship_Name,\n");
                sb.Append("		sh.hull_number AS Hull_Number,\n");
                sb.Append("		mstr.manually_entered AS Master_Manually_Entered,\n");
                sb.Append("		mstr.MSDSSERNO AS Master_MSDSSERNO,\n");
                sb.Append("		mstr.CAGE AS Master_CAGE,\n");
                sb.Append("		mstr.MANUFACTURER AS Manufacturer,\n");
                sb.Append("		mstr.PARTNO AS Part_NO,\n");
                sb.Append("		mstr.FSC,\n");
                sb.Append("		mstr.hcc_id,\n");
                sb.Append("		hcc.hcc AS HCC,\n");
                sb.Append("		hcc.description AS HCC_Description,\n");
                sb.Append("		mstr.file_name AS File_Name,\n");
                sb.Append("		mstr.ARTICLE_IND AS Article_IND,\n");
                sb.Append("		COALESCE(CONVERT(NVARCHAR(2000),mstr.DESCRIPTION), mstr.PRODUCT_IDENTITY) AS Description,\n");
                sb.Append("		mstr.EMERGENCY_TEL AS Emergency_Tel,\n");
                sb.Append("		mstr.END_COMP_IND AS End_Comp_IND,\n");
                sb.Append("		mstr.END_ITEM_IND AS End_Item_IND,\n");
                sb.Append("		mstr.KIT_IND AS Kit_IND,\n");
                sb.Append("		mstr.KIT_PART_IND AS Kit_Part_IND,\n");
                sb.Append("		mstr.MANUFACTURER_MSDS_NO AS Mfg_MSDS_NO,\n");
                sb.Append("		mstr.MIXTURE_IND AS Mixture_IND,\n");
                sb.Append("		mstr.PRODUCT_IDENTITY AS Product_Identity,\n");
                sb.Append("		mstr.PRODUCT_LOAD_DATE AS Product_Load_Date,\n");
                sb.Append("		mstr.PRODUCT_RECORD_STATUS AS Product_Record_Status,\n");
                sb.Append("		mstr.PRODUCT_REVISION_NO AS Product_Rev_NO,\n");
                sb.Append("		mstr.PROPRIETARY_IND AS Proprietary_IND,\n");
                sb.Append("		mstr.PUBLISHED_IND AS Published_IND,\n");
                sb.Append("		mstr.PURCHASED_PROD_IND AS Purchased_Prod_IND,\n");
                sb.Append("		mstr.PURE_IND AS Pure_IND,\n");
                sb.Append("		mstr.RADIOACTIVE_IND AS Radioactive_IND,\n");
                sb.Append("		mstr.SERVICE_AGENCY AS Service_Agency,\n");
                sb.Append("		mstr.TRADE_NAME AS Trade_Name,\n");
                sb.Append("		mstr.TRADE_SECRET_IND AS Trade_Secret_IND,\n");
                sb.Append("		mstr.PRODUCT_IND AS Product_IND,\n");
                sb.Append("		mstr.PRODUCT_LANGUAGE AS Product_Language,\n");
                sb.Append("		mstr.data_source_cd AS Data_Source_CD\n");
                sb.Append("	FROM MSDS_Inventory inv\n");
                sb.Append("	INNER JOIN Ships sh on sh.uic = inv.uic\n");
                sb.Append("	INNER JOIN MSDS_Master mstr ON mstr.msdsserno = inv.msdsserno AND mstr.cage = inv.cage\n");
                sb.Append("		AND mstr.msds_id = (SELECT MAX(msds_id) FROM MSDS_Master WHERE msdsserno = inv.msdsserno and cage = inv.cage)\n");
                sb.Append("	LEFT OUTER JOIN hcc ON hcc.hcc_id = inv.hcc_id;");

                command.CommandText = sb.ToString();

                try {
                    result = command.ExecuteNonQuery();
                }
                catch (Exception ex) {
                    Console.WriteLine("\tException creating v_MSDS_Inventory_Detail - "
                        + ex.Message);
                }
            }

            return result;
        }

        private int createMSSLInventoryDetailViews() {
            int result = 0;
            StringBuilder sb = new StringBuilder();

            // Drop the existing view
            sb.Append("-- MSSL Inventory details record\n");
            sb.Append("IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS\n");
            sb.Append("        WHERE TABLE_NAME = 'v_MSSL_Inventory_Detail')\n");
            sb.Append("    DROP VIEW v_MSSL_Inventory_Detail\n");

            command.CommandText = sb.ToString();
            try {
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex) {
                Console.WriteLine("\tException dropping v_MSSL_Inventory_Detail - "
                    + ex.Message);
            }

            // Recreate the view
            if (result != 0) {
                sb.Clear();
                sb.Append("CREATE VIEW v_MSSL_Inventory_Detail AS\n");
                sb.Append("	SELECT \n");
                sb.Append("		inv.uic AS UIC,\n");
                sb.Append("		inv.niin AS NIIN,\n");
                sb.Append("		inv.location,\n");
                sb.Append("		inv.qty AS QTY,\n");
                sb.Append("		inv.created AS Created,\n");
                sb.Append("		sh.hull_type AS Hull_Type,\n");
                sb.Append("		sh.name AS Ship_Name,\n");
                sb.Append("		sh.hull_number AS Hull_Number,\n");
                sb.Append("		mstr.ati AS ATI,\n");
                sb.Append("		COALESCE(mstr.cog, '') AS COG,\n");
                sb.Append("		COALESCE(cog.description, '') AS Cog_Description,\n");
                sb.Append("		mstr.mcc AS MCC,\n");
                sb.Append("		mstr.ui AS UI,\n");
                sb.Append("		mstr.up AS UP,\n");
                sb.Append("		mstr.netup AS NetUp,\n");
                sb.Append("		mstr.lmc AS LMC,\n");
                sb.Append("		mstr.irc AS IRC,\n");
                sb.Append("		mstr.dues AS Dues,\n");
                sb.Append("		mstr.ro AS RO,\n");
                sb.Append("		mstr.rp AS RP,\n");
                sb.Append("		mstr.amd AS AMD,\n");
                sb.Append("		mstr.smic AS SMIC,\n");
                sb.Append("		COALESCE(mstr.slc, '') AS Shelf_Life_Code,\n");
                sb.Append("		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,\n");
                sb.Append("		COALESCE(slc.description, '') AS Shelf_Life_Description,\n");
                sb.Append("		COALESCE(slc.type, '') AS Shelf_Life_Type,\n");
                sb.Append("		COALESCE(mstr.slac, '') AS Shelf_Life_Action_Code,\n");
                sb.Append("		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,		\n");
                sb.Append("		COALESCE(mstr.smcc, '') AS SMCC,\n");
                sb.Append("		COALESCE(smcc.description, '') AS SMCC_Description,\n");
                sb.Append("		mstr.nomenclature AS Nomenclature,\n");
                sb.Append("		mstr.data_source_cd AS Data_Source_CD\n");
                sb.Append("	FROM mssl_Inventory inv\n");
                sb.Append("	INNER JOIN Ships sh on sh.uic = inv.uic\n");
                sb.Append("	INNER JOIN mssl_Master mstr ON mstr.niin = inv.niin\n");
                sb.Append("	LEFT OUTER JOIN SMCC smcc ON smcc.smcc = mstr.smcc\n");
                sb.Append("	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.slc = mstr.slc\n");
                sb.Append("	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.slac = mstr.slac\n");
                sb.Append("	LEFT OUTER JOIN Cog_Codes cog ON cog.cog = mstr.cog;\n");

                command.CommandText = sb.ToString();

                try {
                    result = command.ExecuteNonQuery();
                }
                catch (Exception ex) {
                    Console.WriteLine("\tException creating v_MSSL_Inventory_Detail - "
                        + ex.Message);
                }
            }

            return result;
        }

        private String createViews() {
            StringBuilder sb = new StringBuilder();

            // Recreate the views
            sb.Append("-- AUL Inventory Detail record\n");
            sb.Append("IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS\n");
            sb.Append("        WHERE TABLE_NAME = 'v_AUL_Inventory_Detail')\n");
            sb.Append("    DROP VIEW v_AUL_Inventory_Detail\n");
            sb.Append("\n");
            sb.Append("CREATE VIEW v_AUL_Inventory_Detail AS\n");
            sb.Append("	SELECT \n");
            sb.Append("		inv.uic AS UIC,\n");
            sb.Append("		inv.niin AS NIIN,\n");
            sb.Append("		inv.qty AS QTY,\n");
            sb.Append("		inv.allowance_qty AS Allowance_QTY,\n");
            sb.Append("		COALESCE(inv.manually_entered, '0') AS Manually_Entered,\n");
            sb.Append("		COALESCE(inv.cage, '') AS CAGE,\n");
            sb.Append("		COALESCE(inv.msds_num, '') AS MSDS_Num,\n");
            sb.Append("		inv.created AS Created,\n");
            sb.Append("		COALESCE(mstr.ui, '') AS UI,\n");
            sb.Append("		COALESCE(mstr.um, '') AS UM,\n");
            sb.Append("		sh.hull_type AS Hull_Type,\n");
            sb.Append("		sh.name AS Ship_Name,\n");
            sb.Append("		sh.hull_number AS Ship_Number,\n");
            sb.Append("		mstr.usage_category_id,\n");
            sb.Append("		COALESCE(uc.category, '') AS Usage_Category,\n");
            sb.Append("		COALESCE(uc.description, '') AS Usage_Category_Description,\n");
            sb.Append("		mstr.description AS Description,\n");
            sb.Append("		mstr.smcc_id,\n");
            sb.Append("		COALESCE(smcc.smcc, '') AS SMCC,\n");
            sb.Append("		COALESCE(smcc.description, '') AS SMCC_Description,\n");
            sb.Append("		mstr.specs AS Specs,\n");
            sb.Append("		mstr.shelf_life_code_id,\n");
            sb.Append("		COALESCE(slc.slc, '') AS Shelf_Life_Code,\n");
            sb.Append("		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,\n");
            sb.Append("		COALESCE(slc.description, '') AS Shelf_Life_Description,\n");
            sb.Append("		COALESCE(slc.type, '') AS Shelf_Life_Type,\n");
            sb.Append("		mstr.shelf_life_action_code_id,\n");
            sb.Append("		COALESCE(slac.slac, '') AS Shelf_Life_Action_Code,\n");
            sb.Append("		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,\n");
            sb.Append("		CONVERT(NVARCHAR(2000), mstr.remarks) AS Remarks,\n");
            sb.Append("		mstr.storage_type_id,\n");
            sb.Append("		COALESCE(stor.type, '') AS Storage_Type,\n");
            sb.Append("		COALESCE(stor.description, '') AS Storage_Type_Description,\n");
            sb.Append("		mstr.cog_id,\n");
            sb.Append("		COALESCE(cog.cog, '') AS COG,\n");
            sb.Append("		COALESCE(cog.description, '') AS COG_Description,\n");
            sb.Append("		mstr.spmig AS SPMIG,\n");
            sb.Append("		mstr.nehc_rpt AS NEHC_Report,\n");
            sb.Append("		mstr.catalog_group_id,\n");
            sb.Append("		mstr.catalog_serial_number AS Catalog_Serial_Number,\n");
            sb.Append("		mstr.dropped_in_error AS Dropped_In_Error,\n");
            sb.Append("		mstr.manufacturer AS Manufacturer,\n");
            sb.Append("		mstr.data_source_cd AS Data_Source_CD		\n");
            sb.Append("	FROM AUL_Inventory inv\n");
            sb.Append("	INNER JOIN Ships sh on sh.uic = inv.uic\n");
            sb.Append("	INNER JOIN Auth_Use_List mstr ON mstr.niin = inv.niin and mstr.hull_type = sh.hull_type\n");
            sb.Append("	LEFT OUTER JOIN Usage_Category uc ON uc.usage_category_id = mstr.usage_category_id\n");
            sb.Append("	LEFT OUTER JOIN SMCC smcc ON smcc.smcc_id = mstr.smcc_id\n");
            sb.Append("	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.shelf_life_code_id = mstr.shelf_life_code_id\n");
            sb.Append("	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.shelf_life_action_code_id = mstr.shelf_life_action_code_id\n");
            sb.Append("	LEFT OUTER JOIN Storage_Type stor ON stor.storage_type_id = mstr.storage_type_id\n");
            sb.Append("	LEFT OUTER JOIN Cog_Codes cog ON cog.cog_id = mstr.cog_id;\n");
            sb.Append("\n");
            sb.Append("-- MSSL Inventory details record\n");
            sb.Append("IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS\n");
            sb.Append("        WHERE TABLE_NAME = 'v_MSSL_Inventory_Detail')\n");
            sb.Append("    DROP VIEW v_MSSL_Inventory_Detail\n");
            sb.Append("\n");
            sb.Append("CREATE VIEW v_MSSL_Inventory_Detail AS\n");
            sb.Append("	SELECT \n");
            sb.Append("		inv.uic AS UIC,\n");
            sb.Append("		inv.niin AS NIIN,\n");
            sb.Append("		inv.location,\n");
            sb.Append("		inv.qty AS QTY,\n");
            sb.Append("		inv.created AS Created,\n");
            sb.Append("		sh.hull_type AS Hull_Type,\n");
            sb.Append("		sh.name AS Ship_Name,\n");
            sb.Append("		sh.hull_number AS Hull_Number,\n");
            sb.Append("		mstr.ati AS ATI,\n");
            sb.Append("		COALESCE(mstr.cog, '') AS COG,\n");
            sb.Append("		COALESCE(cog.description, '') AS Cog_Description,\n");
            sb.Append("		mstr.mcc AS MCC,\n");
            sb.Append("		mstr.ui AS UI,\n");
            sb.Append("		mstr.up AS UP,\n");
            sb.Append("		mstr.netup AS NetUp,\n");
            sb.Append("		mstr.lmc AS LMC,\n");
            sb.Append("		mstr.irc AS IRC,\n");
            sb.Append("		mstr.dues AS Dues,\n");
            sb.Append("		mstr.ro AS RO,\n");
            sb.Append("		mstr.rp AS RP,\n");
            sb.Append("		mstr.amd AS AMD,\n");
            sb.Append("		mstr.smic, AS SMIC,\n");
            sb.Append("		COALESCE(mstr.slc, '') AS Shelf_Life_Code,\n");
            sb.Append("		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,\n");
            sb.Append("		COALESCE(slc.description, '') AS Shelf_Life_Description,\n");
            sb.Append("		COALESCE(slc.type, '') AS Shelf_Life_Type,\n");
            sb.Append("		COALESCE(mstr.slac, '') AS Shelf_Life_Action_Code,\n");
            sb.Append("		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,		\n");
            sb.Append("		COALESCE(mstr.smcc, '') AS SMCC,\n");
            sb.Append("		COALESCE(smcc.description, '') AS SMCC_Description,\n");
            sb.Append("		mstr.nomenclature AS Nomenclature,\n");
            sb.Append("		mstr.data_source_cd AS Data_Source_CD\n");
            sb.Append("	FROM mssl_Inventory inv\n");
            sb.Append("	INNER JOIN Ships sh on sh.uic = inv.uic\n");
            sb.Append("	INNER JOIN mssl_Master mstr ON mstr.niin = inv.niin\n");
            sb.Append("	LEFT OUTER JOIN SMCC smcc ON smcc.smcc = mstr.smcc\n");
            sb.Append("	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.slc = mstr.slc\n");
            sb.Append("	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.slac = mstr.slac\n");
            sb.Append("	LEFT OUTER JOIN Cog_Codes cog ON cog.cog = mstr.cog;\n");
            sb.Append("\n");
            sb.Append("-- MSDS Inventory details record\n");
            sb.Append("IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS\n");
            sb.Append("        WHERE TABLE_NAME = 'v_MSDS_Inventory_Detail')\n");
            sb.Append("    DROP VIEW v_MSDS_Inventory_Detail\n");
            sb.Append("\n");
            sb.Append("CREATE VIEW v_MSDS_Inventory_Detail AS\n");
            sb.Append("	SELECT \n");
            sb.Append("		inv.uic AS UIC,\n");
            sb.Append("		inv.niin AS NIIN,\n");
            sb.Append("		inv.MSDSSERNO,\n");
            sb.Append("		inv.CAGE,\n");
            sb.Append("		inv.manually_entered AS Manually_Entered,\n");
            sb.Append("		inv.created AS Created,\n");
            sb.Append("		sh.hull_type AS Hull_Type,\n");
            sb.Append("		sh.name AS Ship_Name,\n");
            sb.Append("		sh.hull_number AS Hull_Number,\n");
            sb.Append("		mstr.manually_entered AS Master_Manually_Entered,\n");
            sb.Append("		mstr.MSDSSERNO AS Master_MSDSSERNO,\n");
            sb.Append("		mstr.CAGE AS Master_CAGE,\n");
            sb.Append("		mstr.MANUFACTURER AS Manufacturer,\n");
            sb.Append("		mstr.PARTNO AS Part_NO,\n");
            sb.Append("		mstr.FSC,\n");
            sb.Append("		mstr.hcc_id,\n");
            sb.Append("		hcc.hcc AS HCC,\n");
            sb.Append("		hcc.description AS HCC_Description,\n");
            sb.Append("		mstr.file_name AS File_Name,\n");
            sb.Append("		mstr.ARTICLE_IND AS Article_IND,\n");
            sb.Append("		COALESCE(CONVERT(NVARCHAR(2000),mstr.DESCRIPTION), mstr.PRODUCT_IDENTITY) AS Description,\n");
            sb.Append("		mstr.EMERGENCY_TEL AS Emergency_Tel,\n");
            sb.Append("		mstr.END_COMP_IND AS End_Comp_IND,\n");
            sb.Append("		mstr.END_ITEM_IND AS End_Item_IND,\n");
            sb.Append("		mstr.KIT_IND AS Kit_IND,\n");
            sb.Append("		mstr.KIT_PART_IND AS Kit_Part_IND,\n");
            sb.Append("		mstr.MANUFACTURER_MSDS_NO AS Mfg_MSDS_NO,\n");
            sb.Append("		mstr.MIXTURE_IND AS Mixture_IND,\n");
            sb.Append("		mstr.PRODUCT_IDENTITY AS Product_Identity,\n");
            sb.Append("		mstr.PRODUCT_LOAD_DATE AS Product_Load_Date,\n");
            sb.Append("		mstr.PRODUCT_RECORD_STATUS AS Product_Record_Status,\n");
            sb.Append("		mstr.PRODUCT_REVISION_NO AS Product_Rev_NO,\n");
            sb.Append("		mstr.PROPRIETARY_IND AS Proprietary_IND,\n");
            sb.Append("		mstr.PUBLISHED_IND AS Published_IND,\n");
            sb.Append("		mstr.PURCHASED_PROD_IND AS Purchased_Prod_IND,\n");
            sb.Append("		mstr.PURE_IND AS Pure_IND,\n");
            sb.Append("		mstr.RADIOACTIVE_IND AS Radioactive_IND,\n");
            sb.Append("		mstr.SERVICE_AGENCY AS Service_Agency,\n");
            sb.Append("		mstr.TRADE_NAME AS Trade_Name,\n");
            sb.Append("		mstr.TRADE_SECRET_IND AS Trade_Secret_IND,\n");
            sb.Append("		mstr.PRODUCT_IND AS Product_IND,\n");
            sb.Append("		mstr.PRODUCT_LANGUAGE AS Product_Language,\n");
            sb.Append("		mstr.data_source_cd AS Data_Source_CD\n");
            sb.Append("	FROM MSDS_Inventory inv\n");
            sb.Append("	INNER JOIN Ships sh on sh.uic = inv.uic\n");
            sb.Append("	INNER JOIN MSDS_Master mstr ON mstr.msdsserno = inv.msdsserno AND mstr.cage = inv.cage\n");
            sb.Append("		AND mstr.msds_id = (SELECT MAX(msds_id) FROM MSDS_Master WHERE msdsserno = inv.msdsserno and cage = inv.cage)\n");
            sb.Append("	LEFT OUTER JOIN hcc ON hcc.hcc_id = inv.hcc_id;");

            return sb.ToString();
        }
    }
}