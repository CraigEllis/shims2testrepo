﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AESCommon.DatabaseUpdate;

namespace DatabaseUpdate {

    public class DatabaseUpdater : AESCommon.DatabaseUpdate.DatabaseUpdater {
        // AppDBVersion 
        override public int AppDBVersion {
            get {
                return 20110906;
            }
        }

        // Update method
        public override int update(System.Data.SqlClient.SqlConnection connection) {
            int version = 0;

            Console.WriteLine("**** TestDatabaseUpdater update started " +
                connection.State.ToString());
            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            // Check to see if the database is current
            DatabaseStatus status = checkDatabaseStatus(connection);
            Console.WriteLine("\tstatus = " + status.Status.ToString());
            if (status.Status == DatabaseStatus.DBStatus.Current) {
                connection.Close();
                Console.WriteLine("**** TestDatabaseUpdater database is current - version = " +
                    status.Version);
                return 0;
            }

            Console.WriteLine("\tbuilding update list");
            // Build the list of updates to apply based on the version returned
            // from the checkDatabaseStatus call
            List<IDatabaseUpdate> commands = new List<IDatabaseUpdate>();
            if (version < 20110906) {
                commands.Add(new DBUpdate20110906());
            }

            Console.WriteLine("\tupdates in list = " + commands.Count);
            // Apply the updates
            foreach (IDatabaseUpdate command in commands) {
                Console.WriteLine("\tupdate = " + command.ToString());
                command.update(connection);
            }

            // Update the database with the current AppDBVersion
            Console.WriteLine("\tupdating DB Version to " + AppDBVersion);
            updateDBVersion(AppDBVersion, connection);

            connection.Close();

            return commands.Count;
        }
    }
}
