﻿using System.Configuration;

namespace HiltHub {
    public class Configuration {

        public enum UPLOAD_TYPES {
            AUL,
            MSDS,
            MSSL
        };

        public enum DOWNLOAD_TYPES {
            AUL,
            MSDS,
            MSSL
        };

        public static string ConnectionInfo {
            [CoverageExclude]
            get {
                return ConfigurationManager.ConnectionStrings["hilt_hubConnectionString"].ConnectionString;
            }
        }

        public static string HILT_HUB_DataFolder {
            [CoverageExclude]
            get {
                return ConfigurationManager.AppSettings["HILT_HUB_DataFolder"];
            }
        }
    }
}