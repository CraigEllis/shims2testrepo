﻿using System;
using System.Linq;
using System.Web.Mvc;
using HiltHub.Models;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;

namespace HiltHub.Controllers
{
    public partial class ShipController : Controller
    {
        //
        // GET: /ship/
        public virtual ActionResult Clear()
        {
            return RedirectToAction("Index");
        }


        public virtual ActionResult Index(int? page,
                                  GridSortOptions gridSortOptions,
                                  string name,
                                  string uic,
                                  string hullNumber,
                                  int? hullTypeId)
        {
            ViewBag.section = "ship";
            int? pageSize = null;
            if (Request.Cookies["pageSize"] != null)
                pageSize = Convert.ToInt16(Request.Cookies["pageSize"].Value);

            var dc = new hubDataClassesDataContext();
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "Ship_Name";
            }
            var model = new ShipListContainerViewModel();

            IQueryable<v_Ship> shipList = dc.v_Ships;
            // filters
            if (!String.IsNullOrEmpty(name))
                shipList = shipList.Where(x => x.Ship_Name.Contains(name));

            if (!String.IsNullOrEmpty(uic))
                shipList = shipList.Where(x => x.UIC.Contains(uic));

            if (!String.IsNullOrEmpty(hullNumber))
                shipList = shipList.Where(x => x.Hull_Number.Equals(hullNumber));

            if (hullTypeId >= 0)
                shipList = shipList.Where(
                    (x => x.hull_type_id.Equals(hullTypeId) || x.Parent_Type_ID.Equals(hullTypeId))
                    );


            model.ShipPagedList = shipList
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                .AsPagination(page ?? 1, pageSize ?? 10);
            model.GridSortOptions = gridSortOptions;
            model.ShipFilterView = new ShipFilterViewModel
                                       {
                                           HullNumber = null,
                                           Name = name,
                                           HullTypeID = hullTypeId ?? -1,
                                           Uic = null,
                                           HullTypes =
                                               new SelectList(dc.hull_types.Where(x => x.active).AsEnumerable(),
                                                              "hull_type_id", "hull_description"),
                                       };
            return View(model);
        }

        //
        // GET: /ship/Create

        public virtual ActionResult Create()
        {
            ViewBag.Title = "New Ship";
            return View(new ShipEditViewModel {Ship = new ship()});
        }

        [HttpPost]
        public virtual ActionResult Create(ShipEditViewModel model)
        {
            var dc = new hubDataClassesDataContext();
            ViewBag.Title = "New Ship";
            model.Ship.hull_type_id = model.HullTypeID;
            model.Ship.created = DateTime.Now;
            model.Ship.changed = model.Ship.created;
            model.Ship.created_by = User.Identity.Name;
            model.Ship.changed_by = model.Ship.created_by;
            try
            {
                dc.ships.InsertOnSubmit(model.Ship);
                dc.SubmitChanges();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("UNQ__Ships__UIC"))
                    ViewBag.ErrorText = "UIC already exists for another ship.";
                else if (ex.Message.Contains("UNQ__Ships__HT_HN"))
                    ViewBag.ErrorText = "Hull number already exists for another ship.";
                else
                    ViewBag.ErrorText = ex.Message;

                return View(model);
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /ship/Edit/5

        public virtual ActionResult Edit(int id)
        {
            var dc = new hubDataClassesDataContext();
            ship ship = dc.ships.First(x => x.ship_id == id);
            ViewBag.Title = "Edit Ship: " + ship.name;
            return View(new ShipEditViewModel {Ship = ship});
        }


        //
        // POST: /ship/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(int id, ShipEditViewModel model)
        {
            var dc = new hubDataClassesDataContext();
            ship Ship = dc.ships.First(x => x.ship_id == id);
            Ship.hull_type_id = model.HullTypeID; // TODO: Why is this here?
            Ship.changed = DateTime.Now;
            Ship.changed_by = User.Identity.Name;
            UpdateModel(Ship, "Ship");
            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("UNQ__Ships__UIC"))
                    ViewBag.ErrorText = "UIC already exists for another ship.";
                else if (ex.Message.Contains("UNQ__Ships__HT_HN"))
                    ViewBag.ErrorText = "Hull number already exists for another ship.";
                else
                    ViewBag.ErrorText = ex.Message;

                return View(model);
            }
        }

        /*
                //
                // GET: /ship/Delete/5
 
                public ActionResult Delete(int id)
                {
                    return View();
                }

                //
                // POST: /ship/Delete/5

                [HttpPost]
                public ActionResult Delete(int id, FormCollection collection)
                {
                    try
                    {
                        // TODO: Add delete logic here
 
                        return RedirectToAction("Index");
                    }
                    catch
                    {
                        return View();
                    }
                }
            }
         */
    }
}
