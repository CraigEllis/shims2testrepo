﻿using System.Web.Mvc;

namespace HiltHub.Controllers
{
    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            return RedirectToAction("Index", "Dashboard");
        }
    }
}
