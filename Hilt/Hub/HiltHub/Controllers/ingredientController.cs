﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HiltHub.Models;

namespace HiltHub.Controllers
{
    public partial class ingredientController : Controller
    {
        //
        // GET: /ingredient/

        public virtual ActionResult Index(int id)
        {
            var dc = new hubDataClassesDataContext();

            IEnumerable<msds_ingredient> model = dc.msds_ingredients.Where(x => x.msds_id == id);
            msds_master master = dc.msds_masters.First(x => x.msds_id == id);
            ViewBag.msdsserno = master.MSDSSERNO;
            ViewBag.id = id;
            return View(model);
        }

        //
        // GET: /ingredient/Create

        public virtual ActionResult Create(int id)
        {
            var dc = new hubDataClassesDataContext();
            msds_ingredient model = new msds_ingredient {msds_id = id, INGREDIENT_NAME = "New Ingredient"};
            dc.msds_ingredients.InsertOnSubmit(model);
            dc.SubmitChanges();
            return RedirectToAction("Edit", new { id = model.ingredients_id });
        } 

        
        //
        // GET: /ingredient/Edit/5

        public virtual ActionResult Edit(int id)
        {
            var dc = new hubDataClassesDataContext();

            return View(dc.msds_ingredients.First(x => x.ingredients_id == id));
        }

        //
        // POST: /ingredient/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(int id, msds_ingredient model)
        {
            var dc = new hubDataClassesDataContext();
            msds_ingredient rec = dc.msds_ingredients.First(x => x.ingredients_id == id);
            try
            {
                UpdateModel(rec);
                dc.SubmitChanges();
                return RedirectToAction("Index", new { id = model.msds_id });
            }
            catch
            {
                return View(model);
            }
        }

        //
        // GET: /ingredient/Delete/5

        public virtual ActionResult Delete(int id)
        {
            var dc = new hubDataClassesDataContext();
            msds_ingredient model = dc.msds_ingredients.First(x => x.ingredients_id == id);
            var msds_id = model.msds_id;
            dc.msds_ingredients.DeleteOnSubmit(model);
            dc.SubmitChanges();
            return RedirectToAction("Index", new { id = msds_id });
        }

    }
}
