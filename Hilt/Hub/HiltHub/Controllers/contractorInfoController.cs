﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HiltHub.Models;

namespace HiltHub.Controllers
{
    public partial class contractorInfoController : Controller
    {
        //
        // GET: /contractorInfo/

        public virtual ActionResult Index(int id)
        {
            var dc = new hubDataClassesDataContext();

            IEnumerable<msds_contractor_info> model = dc.msds_contractor_infos.Where(x => x.contractor_id == id);
            msds_master master = dc.msds_masters.First(x => x.msds_id == id);
            ViewBag.msdsserno = master.MSDSSERNO;
            ViewBag.id = id;
            return View(model);
        }

        //
        // GET: /contractorInfo/Create

        public virtual ActionResult Create(int id)
        {
            var dc = new hubDataClassesDataContext();
            msds_contractor_info model = new msds_contractor_info {contractor_id = id, CT_COMPANY_NAME = "New Company"};
            dc.msds_contractor_infos.InsertOnSubmit(model);
            dc.SubmitChanges();
            return RedirectToAction("Edit", new {id = model.contractor_id});
        } 
        
        //
        // GET: /contractorInfo/Edit/5

        public virtual ActionResult Edit(int id)
        {
            var dc = new hubDataClassesDataContext();

            return View(dc.msds_contractor_infos.First(x => x.contractor_id == id));
        }

        //
        // POST: /contractorInfo/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(int id, msds_contractor_info model)
        {
            var dc = new hubDataClassesDataContext();
            msds_contractor_info rec = dc.msds_contractor_infos.First(x => x.contractor_id == id);
            try
            {
                UpdateModel(rec);
                dc.SubmitChanges();
                return RedirectToAction("Index", new {id = model.contractor_id});
            }
            catch
            {
                return View(model);
            }
        }

        //
        // GET: /contractorInfo/Delete/5

        public virtual ActionResult Delete(int id)
        {
            var dc = new hubDataClassesDataContext();
            msds_contractor_info model = dc.msds_contractor_infos.First(x => x.contractor_id == id);
            var msds_id = model.contractor_id;
            dc.msds_contractor_infos.DeleteOnSubmit(model);
            dc.SubmitChanges();
            return RedirectToAction("Index", new { id = msds_id });
        }
    }
}
