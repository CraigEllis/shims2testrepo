﻿using System;
using System.Linq;
using System.Web.Mvc;
using MvcContrib.UI.Grid;
using MvcContrib.Pagination;
using MvcContrib.Sorting; 
using HiltHub.Models;
using System.Web;
using System.IO;
using MvcContrib.Filters;
using HiltHub.ImportExport;

namespace HiltHub.Controllers
{
    [ModelStateToTempData]
    public partial class fileImportController : Controller
    {
        //
        // GET: /fileImport/

        public virtual ActionResult Clear()
        {
            return RedirectToAction("Index");
        }

        public virtual ActionResult Index(int? page, 
                GridSortOptions gridSortOptions, 
				int? selectedShipId,
				int? selectedHullTypeId)
        {
            ViewBag.section = "import";
            // Create the model
            fileImportContainerViewModel model = getModel();
 
            // Fill the lists
            populateListContainerModel(model, page, gridSortOptions,
                selectedShipId, selectedHullTypeId, null, null);

            return View(model);
        }

       // GET: /fileImport/msslUpload/
        [HttpPost]
        public virtual ActionResult processFileImport(HttpPostedFileBase importFile, FormCollection collection)
        {
            String saveFilePath = null;

            fileImportContainerViewModel model = getModel();


            int importId;
            Int32.TryParse(collection.Get("selectedImportFileTypeId"), out importId);
            model.selectedImportFileTypeId = importId;
            Int32.TryParse(collection.Get("selectedImportShipId"), out importId);
            model.selectedImportShipId = importId;
            String tempFilePath = collection.Get("fileTempName");
            model.fileImportName = collection.Get("fileImportName");

            // Check for a valid file upload
            // Make sure we have a file name and that we aren't overriding a warning
            if ((importFile == null || importFile.FileName.Length == 0) && tempFilePath.Length == 0) {
                model.fileImportMsg = "Please select a file to import into HILT Hub";
            }
            else if (tempFilePath.Length > 0 && importFile == null) {
                // User clicked the Upload button after a warning
                // See if its the same file
                saveFilePath = tempFilePath;
                model.ignoreChanges = true;
            }
            else {
                // New file upload
                if (importFile != null && importFile.ContentLength > 0) {
                    saveFilePath = Path.GetTempFileName();
                    model.fileImportName = importFile.FileName;
                    // Create a temp file on the server
                    importFile.SaveAs(saveFilePath);
                    model.fileTempName = saveFilePath;
                }
            }

            if (saveFilePath != null) {
                // process the file upload
                String importMsg = doFileImport(saveFilePath, model.fileImportName,
                    model.selectedImportFileTypeId, model.selectedImportShipId,
                    User.Identity.Name, model.ignoreChanges);

                // Funky way to check for changes - MVC3 doesn't support postbacks
                if (importMsg.StartsWith("ERROR")) {
                    model.fileImportMsg = importMsg;
                    model.fileImportName = null;
                    model.fileTempName = null;
                    model.ignoreChanges = false;
                }
                else if (importMsg.StartsWith("WARN")) {
                    model.fileImportMsg = importMsg;
                    model.ignoreChanges = true;
                }
                else {
                    model.fileImportMsg = "File " + model.fileImportName + 
                        "successfully imported.\n" + importMsg;
                    model.fileImportName = null;
                    model.fileTempName = null;
                    model.ignoreChanges = false;
                }
            }

            // Stuff the model into TempData then redirect to the Index event to redraw
            // the page 
            TempData.Add("FileImportModel", model);

            return RedirectToAction("Index","FileImport");
        }

        //
        // GET: /fileImport/Details

        public virtual ActionResult Details(int id)
        {
            var dc = new hubDataClassesDataContext();
            v_Ship model = dc.v_Ships.First(x => x.ship_id == id);
            ViewBag.Title = "File Imports for: " + model.Ship_Name;

            return PartialView("FileImportDetails");
        }


        private fileImportContainerViewModel getModel() {
            // Create a blank model
            var model = new fileImportContainerViewModel();

            // See if we have an existing model from the file upload event
            if (TempData["FileImportModel"] != null) 
                model = (fileImportContainerViewModel) TempData["FileImportModel"];

            return model;
        }

        private static void populateListContainerModel(fileImportContainerViewModel model,
                int? page,
                GridSortOptions gridSortOptions,
                int? selectedShipId,
                int? selectedHullTypeId,
                int? selectedImportFileTypeId, 
                int? selectedImportShipId) {
            var dc = new hubDataClassesDataContext();
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column)) {
                gridSortOptions.Column = "Ship_Name";
            }

            IQueryable<v_Ship> shipList = dc.v_Ships;

            if (selectedShipId >= 0) {
                shipList = shipList.Where(x => x.ship_id.Equals(selectedShipId));
                model.selectedShipId = (int) selectedShipId;
            }

            if (selectedHullTypeId >= 0)
                shipList = shipList.Where(x => x.hull_type_id.Equals(selectedHullTypeId));

            if (selectedImportShipId >= 0) {
                model.selectedImportShipId = (int) selectedImportShipId;
            }

            if (selectedImportFileTypeId >= 0) {
                model.selectedImportFileTypeId = (int) selectedImportFileTypeId;
            }

            model.FileImportPagedList = shipList
                                    .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                                    .AsPagination(page ?? 1, 10);
            model.GridSortOptions = gridSortOptions;
            model.FileImportFilterViewModel = new fileImportFilterViewModel {
                SelectedHullTypeId = selectedHullTypeId ?? -1,
                SelectedShipId = selectedShipId ?? -1
            };
            model.FileImportFilterViewModel.Fill();
        }

        private static String doFileImport(String importFileName, String fileName,
                    int fileType, int shipID, String userName, bool ignoreChanges) {
            String importMsg = "";

            // Call the appropriate file importer
            switch ((Configuration.UPLOAD_TYPES) fileType) {
                case Configuration.UPLOAD_TYPES.AUL:
                    // Call the AUL importer
                    break;
                case Configuration.UPLOAD_TYPES.MSDS:
                    // Call the MSDS importer
                    break;
                case Configuration.UPLOAD_TYPES.MSSL:
                    // Call the MSSL importer
                    MSSLImporter importer = new MSSLImporter();
                    importMsg = importer.import(importFileName, fileName, shipID, userName,
                        ignoreChanges);
                    break;
            }

            return importMsg;
        }
    }
}
