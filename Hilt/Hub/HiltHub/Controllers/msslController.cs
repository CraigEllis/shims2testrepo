﻿using System;
using System.Linq;
using System.Web.Mvc;
using MvcContrib.UI.Grid;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using HiltHub.Models;
using ExtensionMethods;

namespace HiltHub.Controllers
{
    public partial class MsslController : Controller
    {
        public virtual ActionResult Clear()
        {
            return RedirectToAction("Index");
        }

        public virtual ActionResult Index(int? page, 
				GridSortOptions gridSortOptions, 
				string niin,
                string product,
                string shelfLife,
				int? shipId)
        {
            ViewBag.section = "mssl";
            int? pageSize = null;
            if (Request.Cookies["pageSize"] != null)
                pageSize = Convert.ToInt16(Request.Cookies["pageSize"].Value);

            var dc = new hubDataClassesDataContext();
            var model = new MsslListContainerViewModel();

            // get list
            IQueryable<v_MSSL_Inventory_Detail> msslList = dc.v_MSSL_Inventory_Details;

            // filter
            if (!String.IsNullOrEmpty(niin))
                msslList = msslList.Where(x => x.NIIN.Contains(niin));

            if (!String.IsNullOrEmpty(product))
                msslList = msslList.Where(x => x.Nomenclature.Contains(product));

            if (!String.IsNullOrEmpty(shelfLife))
                msslList = msslList.Where(x => x.Shelf_Life_Code.Equals(shelfLife));

            if (shipId >= 0)
                msslList = msslList.Where(x => x.ship_id.Equals(shipId));

            msslList = msslList.GroupBy(x=>x.NIIN).Select(x=>x.First()).OrderBy(e=>e.NIIN);

            model.MsslPagedList = msslList
                                      .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                                    .AsPagination(page ?? 1, pageSize ?? 10);
            model.GridSortOptions = gridSortOptions;

            IOrderedQueryable<v_MSSL_Inventory_Detail> shelfLives = dc.v_MSSL_Inventory_Details.GroupBy(x=>x.Shelf_Life_Description)
                                .Select(x=>x.First()).OrderBy(e => e.Shelf_Life_Description);
            SelectList shelfLifeList = 
                new SelectList(shelfLives, "Shelf_Life_Code", "Shelf_Life_Description");

            model.MsslFilterViewModel = new MsslFilterViewModel
                                           {
                                               NIIN = niin,
                                               Product = product,
                                               ShelfLife = shelfLifeList,
                                               ShipId = shipId ?? -1,
                                               Ships = new SelectList(dc.ships.AsEnumerable(), "ship_id", "name"),
                                           };            
            return View(model);
        }

        //
        // GET: /mssl/Details/5

        public virtual ActionResult Details(string id)
        {
            var dc = new hubDataClassesDataContext();
            v_MSSL_Inventory_Detail model = dc.v_MSSL_Inventory_Details.First(x => x.NIIN == id);
            return View(model);
        }
    }
}
