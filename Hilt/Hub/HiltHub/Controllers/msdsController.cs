﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Data.OleDb;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using HiltHub.Models;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;
using ExtensionMethods;

namespace HiltHub.Controllers
{
    public partial class MsdsController : Controller
    {
        //
        // GET: /msds/

        public virtual ActionResult Clear()
        {
            return RedirectToAction("Index");
        }

        private static void ExplodeContractorTable()
        {
            var dc = new hubDataClassesDataContext();
            IEnumerable<msds_contractor_info> csContractorList =
                dc.msds_contractor_infos.OrderBy("contractor_id", SortDirection.Ascending).Where(x => x.CT_CAGE.Contains(",")).AsEnumerable();
            if (csContractorList == null) return;
            foreach (msds_contractor_info csContractor in csContractorList)
            {
                if (csContractor.CT_CAGE == null
                    || csContractor.CT_CITY == null
                    || csContractor.CT_COMPANY_NAME == null
                    || csContractor.CT_COUNTRY == null
                    //|| csContractor.CT_NUMBER == null
                    || csContractor.CT_PHONE == null
                    || csContractor.CT_PO_BOX == null
                    //|| csContractor.PURCHASE_ORDER_NO == null
                    || csContractor.CT_STATE == null) continue;
                string[] ct_cages = csContractor.CT_CAGE.Split(',');
                if (ct_cages.Count() < 2) continue;
                string[] ct_cities = csContractor.CT_CITY.Split(',');
                string[] ct_company_names = csContractor.CT_COMPANY_NAME.Split(',');
                string[] ct_countries = csContractor.CT_COUNTRY.Split(',');
                //string[] ct_numbers = csContractor.CT_NUMBER.Split(',');
                string[] ct_phones = csContractor.CT_PHONE.Split(',');
                string[] ct_po_boxes = csContractor.CT_PO_BOX.Split(',');
                //string[] purchase_order_nos = csContractor.PURCHASE_ORDER_NO.Split(',');
                string[] ct_states = csContractor.CT_STATE.Split(',');
                for (int i = 0; i < ct_cages.Count(); i++)
                {
                    msds_contractor_info splitContractorI =
                        new msds_contractor_info
                        {
                            CT_CAGE = ct_cages[i],
                            CT_CITY = ct_cities[i],
                            CT_COMPANY_NAME = ct_company_names[i],
                            CT_COUNTRY = ct_countries[i],
                            //CT_NUMBER = ct_numbers[i],
                            CT_PHONE = ct_phones[i],
                            CT_PO_BOX = ct_po_boxes[i],
                            CT_STATE = ct_states[i],
                            //PURCHASE_ORDER_NO = purchase_order_nos[i],
                            contractor_id = csContractor.contractor_id,
                            //msds_id = csContractor.msds_id
                        };
                    dc.msds_contractor_infos.InsertOnSubmit(splitContractorI);
                    dc.SubmitChanges();
                }
                dc.msds_contractor_infos.DeleteOnSubmit(csContractor);
                dc.SubmitChanges();
            }
        }
        
        private static void ExplodeIngredientsTable()
        {
            var dc = new hubDataClassesDataContext();
            
            IEnumerable<msds_ingredient> csIngredientsList =
                dc.msds_ingredients.OrderBy("ingredients_id", SortDirection.Ascending).Where(x => x.CAS.Contains(",")).AsEnumerable();
            if (csIngredientsList == null) return;
            foreach (msds_ingredient csIngredient in csIngredientsList)
            {
                if (csIngredient.ACGIH_STEL == null
                    || csIngredient.ACGIH_TLV == null
                    || csIngredient.CAS == null
                    || csIngredient.CHEM_MFG_COMP_NAME == null
                    || csIngredient.DOT_REPORT_QTY == null
                    || csIngredient.EPA_REPORT_QTY == null
                    || csIngredient.INGREDIENT_NAME == null
                    || csIngredient.ODS_IND == null
                    || csIngredient.OSHA_PEL == null
                    || csIngredient.OSHA_STEL == null
                    || csIngredient.OTHER_REC_LIMITS == null
                    || csIngredient.PRCNT == null
                    || csIngredient.PRCNT_VOL_VALUE == null
                    || csIngredient.PRCNT_VOL_WEIGHT == null
                    || csIngredient.RTECS_CODE == null
                    || csIngredient.RTECS_NUM == null) continue;
                
                string[] acgih_tlvs = csIngredient.ACGIH_TLV.Split(',');
                if (acgih_tlvs.Count() < 2) continue;
                string[] acgih_stels = csIngredient.ACGIH_STEL.Split(',');
                string[] cass = csIngredient.CAS.Split(',');
                string[] chem_mfg_comp_names = csIngredient.CHEM_MFG_COMP_NAME.Split(',');
                string[] dot_report_qtys = csIngredient.DOT_REPORT_QTY.Split(',');
                string[] epa_report_qtys = csIngredient.EPA_REPORT_QTY.Split(',');
                string[] ingredient_names = csIngredient.INGREDIENT_NAME.Split(',');
                string[] ods_inds = csIngredient.ODS_IND.Split(',');
                string[] osha_pels = csIngredient.OSHA_PEL.Split(',');
                string[] osha_stels = csIngredient.OSHA_STEL.Split(',');
                string[] other_rec_limits = csIngredient.OTHER_REC_LIMITS.Split(',');
                string[] prcnts = csIngredient.PRCNT.Split(',');
                string[] prcnt_vol_values = csIngredient.PRCNT_VOL_VALUE.Split(',');
                string[] prcnt_vol_weights = csIngredient.PRCNT_VOL_WEIGHT.Split(',');
                string[] rtecs_codes = csIngredient.RTECS_CODE.Split(',');
                string[] rtecs_nums = csIngredient.RTECS_NUM.Split(',');
                try
                {
                    for (int i = 0; i < acgih_tlvs.Count(); i++)
                    {
                        msds_ingredient splitIngredientI =
                            new msds_ingredient
                                {
                                    ACGIH_STEL = acgih_stels[i],
                                    ACGIH_TLV = acgih_tlvs[i],
                                    CAS = cass[i],
                                    CHEM_MFG_COMP_NAME = chem_mfg_comp_names[i],
                                    DOT_REPORT_QTY = dot_report_qtys[i],
                                    EPA_REPORT_QTY = epa_report_qtys[i],
                                    INGREDIENT_NAME = ingredient_names[i],
                                    ODS_IND = ods_inds[i],
                                    OSHA_PEL = osha_pels[i],
                                    OSHA_STEL = osha_stels[i],
                                    OTHER_REC_LIMITS = other_rec_limits[i],
                                    PRCNT = prcnts[i],
                                    PRCNT_VOL_VALUE = prcnt_vol_values[i],
                                    PRCNT_VOL_WEIGHT = prcnt_vol_weights[i],
                                    RTECS_CODE = rtecs_codes[i],
                                    RTECS_NUM = rtecs_nums[i],
                                    msds_id = csIngredient.msds_id,
                                    ingredients_id = csIngredient.ingredients_id
                                };
                        dc.msds_ingredients.InsertOnSubmit(splitIngredientI);
                        dc.SubmitChanges();
                    }
                    dc.msds_ingredients.DeleteOnSubmit(csIngredient);
                    dc.SubmitChanges();
                }
                catch (Exception)
                {
                }
            }
        }

        // Some of the Contractor and Ingredients data comes to us in comma-separated records.
        // We have a much easier time handling these if they're split into individual records.
        public static void ExplodeContractorAndIngredientsTables()
        {
            ExplodeIngredientsTable();
            ExplodeContractorTable();
        }

        [HttpGet]
        public virtual JsonResult GetExportList(string niin,
                                  string msdsSerNo,
                                  int? shipId,
                                  int? hullTypeId,
                                  string product,
                                  string manufacturer)
        {
            List<string> msdsList = GetMsdsList(niin, msdsSerNo, shipId, hullTypeId, product, manufacturer).Select(x => x.Master_MSDSSERNO).ToList();
            return Json(msdsList, JsonRequestBehavior.AllowGet);
        }

        public virtual void ExportSingle(string msdsSerNo, int? hullTypeId)
        {
            Export(null, msdsSerNo, null, hullTypeId, null, null);
        }

        public virtual ActionResult Export(string niin,
                                  string msdsSerNo,
                                  int? shipId,
                                  int? hullTypeId,
                                  string product,
                                  string manufacturer)
        {
            IEnumerable<v_MSDS_HullType_Detail> msdsList = GetMsdsList(niin, msdsSerNo, shipId, hullTypeId, product, manufacturer).Take(10);
            var dc = new hubDataClassesDataContext();
            string DocumentsSource = Configuration.HILT_HUB_DataFolder + @"\MSDS_Docs";

            // init folders, etc
            string FolderName = "All";
            if (hullTypeId > -1)
            {
                FolderName = dc.hull_types.First(x => x.hull_type_id == hullTypeId).hull_description;
            }

            // Add datestamp and avoid overwriting files which might not have handles properly released
            //FolderName += " " + DateTime.Now.ToString().Replace('/', '-').Replace(':','.'); 

            if (!System.IO.File.Exists(DocumentsSource))
                Directory.CreateDirectory(DocumentsSource);
            
            string ExportToFolder = DocumentsSource + @"\" + FolderName;
            if (!System.IO.File.Exists(ExportToFolder))
                Directory.CreateDirectory(ExportToFolder);

            string DocumentsFolder = ExportToFolder + @"\Documents";
            if (!Directory.Exists(DocumentsFolder))
                Directory.CreateDirectory(DocumentsFolder);

            string ValueAddFolder = ExportToFolder + @"\ValueAdded";
            if (!Directory.Exists(ValueAddFolder))
                Directory.CreateDirectory(ValueAddFolder);            

            // Create list of MSDS's
            string exportListSourceFile = Request.PhysicalApplicationPath + @"\ImportExport\MsdsList.xls";
            string exportListDestFile = ExportToFolder + @"\MSDS List.xls";
            if (!System.IO.File.Exists(exportListDestFile))
                System.IO.File.Copy(exportListSourceFile, exportListDestFile, true); 
            
            OleDbConnection con =
                 new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;"
                    + @"Data Source=" + exportListDestFile + ";"
                    + @"Extended Properties=""Excel 8.0;HDR=Yes;""");
            con.Open();

            OleDbCommand cmd = 
                new OleDbCommand
                {
                    Connection = con,
                    Transaction = con.BeginTransaction(),
                    CommandText = "insert into [MsdsList$] VALUES(@msds, @document, @valueadded)"
                };

            DirectoryInfo dir = new DirectoryInfo(DocumentsSource);
            foreach (v_MSDS_HullType_Detail msds in msdsList)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@msds", msds.Master_MSDSSERNO);
                cmd.Parameters.AddWithValue("@valueadded",
                            @"=HYPERLINK(""ValueAdded\" + msds.Master_MSDSSERNO +
                            @".xls"", ""Value Added"")");
                FileInfo[] files = dir.GetFiles(msds.Master_MSDSSERNO + "*");
                if (!files.Any())
                {
                    cmd.Parameters.AddWithValue("@document","No files available.");
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    foreach (FileInfo f in files)
                    {
                        OleDbParameter odbp = cmd.Parameters.AddWithValue("@document",
                                                    @"=HYPERLINK(""Documents\" + f.Name + @""", """ + f.Name + @""")");
                        System.IO.File.Copy(Path.Combine(DocumentsSource, f.Name), Path.Combine(DocumentsFolder, f.Name), true);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Remove(odbp);
                    }
                }
                
                ExportValueAdded(msds, ValueAddFolder);
            }

            cmd.Transaction.Commit();
            con.Close();


            // Create excel file for value-added data
            foreach (v_MSDS_HullType_Detail msds in msdsList)
            {
                ExportValueAdded(msds, ValueAddFolder);
            }

            // Copy PDF and HTML files


            // Show results
            ViewBag.exportfolder = ExportToFolder;
            ViewBag.count = msdsList.Count();
            return View();
        }

        private void ExportValueAdded(v_MSDS_HullType_Detail msdsInventoryDetail, string ValueAddFolder)
        {
            var dc = new hubDataClassesDataContext();
            string exportValueAddSourceFile = Request.PhysicalApplicationPath + @"\ImportExport\MsdsValueAdded.xls";
            string exportValueAddDestFile = ValueAddFolder + @"\" + msdsInventoryDetail.Master_MSDSSERNO + ".xls";

            if (System.IO.File.Exists(exportValueAddDestFile))
                return; //todo: this is a workaround for file still in use
            try
            {
                System.IO.File.Copy(exportValueAddSourceFile, exportValueAddDestFile, true);
            }
            catch
            {
                return;
            }

            OleDbConnection con =
                 new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;"
                    + @"Data Source=" + exportValueAddDestFile + ";"
                    + @"Extended Properties=""Excel 8.0;HDR=Yes;""");

            long msds_id = msdsInventoryDetail.msds_id;
            msds_master master = dc.msds_masters.FirstOrDefault(x => x.msds_id == msds_id);
            ExportMsdsSection(master, "Header Data", con);
            ExportMsdsSection(dc.msds_disposals.FirstOrDefault(x => x.msds_id == msds_id), "Disposal", con);
            ExportMsdsSection(dc.msds_item_descriptions.FirstOrDefault(x => x.msds_id == msds_id), "Item Description", con);
            ExportMsdsSection(dc.msds_label_infos.FirstOrDefault(x => x.msds_id == msds_id), "Label", con);
            ExportMsdsSection(dc.msds_phys_chemicals.FirstOrDefault(x => x.msds_id == msds_id), "Physical/Chemical", con);
            ExportMsdsSection(dc.msds_radiological_infos.FirstOrDefault(x => x.msds_id == msds_id), "Radiological", con);

            msds_transportation transportation = dc.msds_transportations.FirstOrDefault(x => x.msds_id == msds_id);
            if (transportation != null)
            {
                ExportMsdsSection(transportation, "Transportation", con);
                ExportMsdsSection(dc.msds_dot_psns.FirstOrDefault(x => x.dot_psn_id == transportation.dot_psn_id), "DOT PSN", con);
                ExportMsdsSection(dc.msds_iata_psns.FirstOrDefault(x => x.iata_psn_id == transportation.iata_psn_id), "IATA PSN", con);
                ExportMsdsSection(dc.msds_imo_psns.FirstOrDefault(x => x.imo_psn_id == transportation.imo_psn_id), "IMO PSN", con);
                ExportMsdsSection(dc.msds_afjm_psns.FirstOrDefault(x => x.afjm_psn_id == transportation.afjm_psn_id), "AFJM PSN", con);
            }

            ExportMsdsContracts(dc.v_MSDS_Contract_Details.Where(x => x.msds_id == msds_id), con);
            //TODO: Radiological and Ingredients lists
            con.Close();
            con.Dispose();
        }

        private static void ExportMsdsSection(object section, string sectionName, OleDbConnection con)
        {
            if (section == null)
                return;
            
            con.Open();
            
            // write section head
            OleDbCommand cmd = 
                new OleDbCommand
                {
                    Connection = con,
                    CommandText = "insert into [MsdsValueAddedData$] VALUES(@section, ' ', ' ')"
                };

            cmd.Transaction = con.BeginTransaction();
            cmd.Parameters.AddWithValue("@section", sectionName);
            cmd.ExecuteNonQuery();
            cmd.Transaction.Commit();

            // write name/value pairs
            cmd.CommandText = "insert into [MsdsValueAddedData$] VALUES(' ', @name, @value)";
            var type = section.GetType();
            PropertyInfo[] pis = type.GetProperties();
            foreach (PropertyInfo pi in pis)
            {
                if (pi.Name == "msds_master" || pi.Name.IndexOf("_id", StringComparison.Ordinal) == pi.Name.Length - 3)
                    continue;
                var value = pi.GetValue(section, null);
                string strVal = " ";
                if (value != null)
                {
                    strVal = value.ToString();

                    if (strVal.Length > 255)
                        strVal = strVal.Substring(0, 255);
                }
                
                if (strVal.Contains("System.Data.Linq.EntitySet") || strVal.Contains("HiltHub.Models"))
                    continue;
                cmd.Transaction = con.BeginTransaction();
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@name", pi.Name.Replace('_',' ').TitleCase());
                cmd.Parameters.AddWithValue("@value", strVal);
                cmd.ExecuteNonQuery();
                cmd.Transaction.Commit();
            }            
            con.Close();
        }

        private static void ExportMsdsContracts(IEnumerable<v_MSDS_Contract_Detail> contracts, OleDbConnection con)
        {
            con.Open();
            
            OleDbCommand cmd = 
                new OleDbCommand
                {
                    Connection = con,
                    CommandText = "insert into [Contractors$] VALUES(@Contract, @PurchaseOrder, @CAGE, @CompanyName, @POBox, @Address, @City, @State, @ZipCode, @Country, @Phone)"
                };

            foreach (v_MSDS_Contract_Detail contract in contracts)
            {
                cmd.Transaction = con.BeginTransaction();
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Contract", contract.CT_CONTRACT ?? "");
                cmd.Parameters.AddWithValue("@PurchaseOrder", contract.PURCHASE_ORDER_NO ?? "");
                cmd.Parameters.AddWithValue("@CAGE", contract.CT_CAGE ?? "");
                cmd.Parameters.AddWithValue("@CompanyName", contract.CT_COMPANY_NAME ?? "");
                cmd.Parameters.AddWithValue("@POBox", contract.CT_PO_BOX ?? "");
                cmd.Parameters.AddWithValue("@Address", contract.CT_ADDRESS_1 ?? "");
                cmd.Parameters.AddWithValue("@City", contract.CT_CITY ?? "");
                cmd.Parameters.AddWithValue("@State", contract.CT_STATE ?? "");
                cmd.Parameters.AddWithValue("@ZipCode", contract.CT_ZIP_CODE ?? "");
                cmd.Parameters.AddWithValue("@Country", contract.CT_COUNTRY ?? "");
                cmd.Parameters.AddWithValue("@Phone", contract.CT_PHONE ?? "");
                cmd.ExecuteNonQuery();
                cmd.Transaction.Commit();
            }

            con.Close();
        }

        public virtual ActionResult Index(int? page,
                                  GridSortOptions gridSortOptions,
                                  string niin,
                                  string msdsSerNo,
                                  int? shipId,
                                  int? hullTypeId,
                                  string product,
                                  string manufacturer)
        {
            ViewBag.section = "msds";

            int? pageSize = null;
            if (Request.Cookies["pageSize"] != null)
                pageSize = Convert.ToInt16(Request.Cookies["pageSize"].Value);

            // Uncomment next line to clean up the MSDS data tables
            //ExplodeContractorAndIngredientsTables();
            var dc = new hubDataClassesDataContext();
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "Master_MSDSSERNO";
            }
            var model = new MsdsListContainerViewModel();

            IQueryable<v_MSDS_HullType_Detail> msdsList = GetMsdsList(niin, msdsSerNo, shipId, hullTypeId, product, manufacturer);
            model.MsdsPagedList = msdsList
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                .AsPagination(page ?? 1, pageSize ?? 10);
            model.GridSortOptions = gridSortOptions;
            model.MsdsFilterViewModel = new MsdsFilterViewModel
                                            {
                                                niin = niin,
                                                Master_MSDSSERNO = msdsSerNo,
                                                ShipId = shipId ?? -1,
                                                Ships = new SelectList(dc.ships.AsEnumerable(), "ship_id", "name"),
                                                HullTypes = new SelectList(dc.hull_types.Where(x => x.parent_type_id > 0).AsEnumerable(), "hull_type_id", "hull_description"),
                                                HullTypeId = hullTypeId ?? -1,
                                                Manufacturer = manufacturer,
                                                Product = product
                                            };

            return View(model);
        }

        class MsdsComparer : IEqualityComparer<v_MSDS_HullType_Detail>
        {
            public bool Equals(v_MSDS_HullType_Detail a, v_MSDS_HullType_Detail b)
            {
                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(a, b)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(a, null) || Object.ReferenceEquals(b, null))
                    return false;

                return a.Master_MSDSSERNO == b.Master_MSDSSERNO;
            }

            public int GetHashCode(v_MSDS_HullType_Detail a)
            {
                return a.Master_MSDSSERNO.GetHashCode();
            }
        }

        private static IQueryable<v_MSDS_HullType_Detail> GetMsdsList(                                  
                                      string niin,
                                      string msdsSerNo,
                                      int? shipId,
                                      int? hullTypeId,
                                      string product,
                                      string manufacturer)
        {
            var dc = new hubDataClassesDataContext();
            // Get the list
            IQueryable<v_MSDS_HullType_Detail> msdsList = dc.v_MSDS_HullType_Details.Where(x => x.niin != "UNKNOWN");

            // Apply the filter
            if (!String.IsNullOrEmpty(niin))
                msdsList = msdsList.Where(x => x.niin.Contains(niin));

            /* TODO: if (shipId >= 0)
                msdsList = msdsList.Where(x => x.ship.ship_id.Equals(shipId)); */

            if (hullTypeId >= 0)
                msdsList = msdsList.Where(x => x.hull_type_id.Equals(hullTypeId));

            if (!String.IsNullOrEmpty(msdsSerNo))
                msdsList = msdsList.Where(x => x.Master_MSDSSERNO.Contains(msdsSerNo));

            if (!String.IsNullOrEmpty(manufacturer))
                msdsList = msdsList.Where(x => x.Manufacturer.Contains(manufacturer));

            if (!String.IsNullOrEmpty(product))
                msdsList = msdsList.Where(x => x.Product_Identity.Contains(product));

            IEnumerable<v_MSDS_HullType_Detail> nodupes = msdsList.AsEnumerable();
            nodupes = nodupes.Distinct(new MsdsComparer());            
            return nodupes.AsQueryable();
        }

        //
        // GET: /msds/Edit/5
        public virtual ActionResult Edit(string id)
        {
            return View(GetMsdsModel(id));
        }

        private string GetSerializedMsdsAuditRecord(string id)
        {
            var dc = new hubDataClassesDataContext();
            MsdsAuditModel model = new MsdsAuditModel();
            Int64 i = Convert.ToInt64(id);
            DateTime? maxActionDate =
                dc.AUDIT_msds_masters.Where(x => x.msds_id == i).Where(x => x.action == "INSERT").Max(
                    x => x.action_date);
            if (maxActionDate == null) return(null);
            model.MsdsMaster = 
                dc.AUDIT_msds_masters
                .Where(x => x.msds_id == i)
                .Where(x => x.action == "INSERT")
                .First(x => x.action_date == maxActionDate);

            // enumerate all the members of the original Msds Record and serialized into a JSON string
            return new JavaScriptSerializer().Serialize(model);
        }

        private static MsdsEditViewModel GetMsdsModel(string id)
        {
            Int64 i = Convert.ToInt64(id);
            var dc = new hubDataClassesDataContext();

            MsdsEditViewModel model = 
                new MsdsEditViewModel
                    {
                        MsdsOriginalValues = "{}", //GetSerializedMsdsAuditRecord(id),
                        MsdsMaster = dc.msds_masters.First(x => x.msds_id == i),
                        MsdsIngredientList = dc.msds_ingredients.Where(x => x.msds_id == i).ToList(),
                        MsdsItemDescription = dc.msds_item_descriptions.FirstOrDefault(x => x.msds_id == i),
                        MsdsLabelInfo = dc.msds_label_infos.First(x => x.msds_id == i),
                        MsdsPhysChemical = dc.msds_phys_chemicals.First(x => x.msds_id == i),
                        MsdsRadiologicalInfo = dc.msds_radiological_infos.Where(x => x.msds_id == i).ToList(),
                        MsdsTransportation = dc.msds_transportations.FirstOrDefault(x => x.msds_id == i),
                        MsdsDisposal = dc.msds_disposals.FirstOrDefault(x => x.msds_id == i)
                        //MsdsContractorList = dc.msds_contractor_infos.Where(x => x.msds_id == i).ToList()
                    };

            if (model.MsdsTransportation != null)
            {
                if (model.MsdsTransportation.dot_psn_id != null)
                    model.MsdsDotPsn =
                        dc.msds_dot_psns.FirstOrDefault(x => x.dot_psn_id == model.MsdsTransportation.dot_psn_id);

                if (model.MsdsTransportation.iata_psn_id != null)
                    model.MsdsIataPsn =
                        dc.msds_iata_psns.FirstOrDefault(x => x.iata_psn_id == model.MsdsTransportation.iata_psn_id);

                if (model.MsdsTransportation.imo_psn_id != null)
                    model.MsdsImoPsn = dc.msds_imo_psns.FirstOrDefault(x => x.imo_psn_id == model.MsdsTransportation.imo_psn_id);

                if (model.MsdsTransportation.afjm_psn_id != null)
                    model.MsdsAfjmPsn =
                        dc.msds_afjm_psns.FirstOrDefault(x => x.afjm_psn_id == model.MsdsTransportation.afjm_psn_id);
            }

            return model;
        }

        //
        // POST: /msds/Edit/5
        // TODO: Only allow POST from user with allowed role(s)
        [HttpPost]
        public virtual ActionResult Edit(int id, MsdsEditViewModel model)
        {

            if (!ModelState.IsValid) return View(model);

            try
            {
                var dm = new DatabaseManager();
                dm.updateMSDSMasterRecord("HILT HUB", model, User.Identity.Name); 

                return RedirectToAction("Index");
            }
            catch (FormatException)
            {
                return View(model);
            }
        }
    }
}