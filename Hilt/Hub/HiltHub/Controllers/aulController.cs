﻿using System;
using System.Linq;
using System.Web.Mvc;
using MvcContrib.UI.Grid;
using MvcContrib.Pagination;
using MvcContrib.Sorting; 
using HiltHub.Models;

namespace HiltHub.Controllers
{
    public partial class AulController : Controller
    {
        //
        // GET: /aul/

        public virtual ActionResult Clear()
        {
            return RedirectToAction("Index");
        }

        public virtual ActionResult Index(int? page, 
                                    GridSortOptions gridSortOptions, 
                                    string niin, 
                                    string description,
                                    string cage,
									int? shipId,
                                    int? catalogGroupId)
        {
            ViewBag.section = "aul";
            int? pageSize = null;
            if (Request.Cookies["pageSize"] != null)
                pageSize = Convert.ToInt16(Request.Cookies["pageSize"].Value);

            var dc = new hubDataClassesDataContext();
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column)) { gridSortOptions.Column = "description"; }
            var model = new AulListContainerViewModel();

            IQueryable<v_AUL_HullType_Detail> aulList = dc.v_AUL_HullType_Details.Where(x => x.niin != "UNKNOWN");
            
            if (!String.IsNullOrEmpty(niin))
                aulList = aulList.Where(x => x.niin.Contains(niin));
/*
            if (shipId >= 0)
                aulList = aulList.Where(x => x..Equals(shipId));
*/
            if (!String.IsNullOrEmpty(description))
                aulList = aulList.Where(x => x.description.Contains(description));

            if (catalogGroupId >= 0)
                aulList = aulList.Where(x => x.catalog_group_id == catalogGroupId);

            model.AulPagedList = aulList
                                     .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                                    .AsPagination(page ?? 1, pageSize ?? 10);
            model.GridSortOptions = gridSortOptions;
            model.AulFilterViewModel = new AulFilterViewModel
                                           {
                                               NIIN = niin,
                                               Description = description,
                                               CatalogGroupId = catalogGroupId ?? -1,
                                               CatalogGroups = new SelectList(dc.catalog_groups.AsEnumerable(), "catalog_group_id", "group_name"),
                                               ShipId = shipId ?? -1,
                                               Ships = new SelectList(dc.ships.AsEnumerable(), "ship_id", "name"),
                                           };
            return View(model);
        }


        //
        // GET: /aul/Details/5
        public virtual ActionResult Details(string id)
        {
            var dc = new hubDataClassesDataContext();
            v_AUL_HullType_Detail model = dc.v_AUL_HullType_Details.First(x => x.niin == id);
            return View(model);
        }
    }
}
