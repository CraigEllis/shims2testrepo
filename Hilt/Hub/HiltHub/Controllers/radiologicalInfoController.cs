﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HiltHub.Models;

namespace HiltHub.Controllers
{
    public partial class radiologicalInfoController : Controller
    {
        //
        // GET: /ingredient/

        public virtual ActionResult Index(int id)
        {
            var dc = new hubDataClassesDataContext();

            IEnumerable<msds_radiological_info> model = dc.msds_radiological_infos.Where(x => x.msds_id == id);
            msds_master master = dc.msds_masters.First(x => x.msds_id == id);
            ViewBag.msdsserno = master.MSDSSERNO;
            ViewBag.id = id;
            return View(model);
        }

        //
        // GET: /ingredient/Create

        public virtual ActionResult Create(int id)
        {
            var dc = new hubDataClassesDataContext();
            msds_radiological_info model = new msds_radiological_info { msds_id = id, RAD_NAME = "New Radiological Info" };
            dc.msds_radiological_infos.InsertOnSubmit(model);
            dc.SubmitChanges();
            return RedirectToAction("Edit", new { id = model.rad_info_id });
        } 

        
        //
        // GET: /ingredient/Edit/5

        public virtual ActionResult Edit(int id)
        {
            var dc = new hubDataClassesDataContext();

            return View(dc.msds_radiological_infos.First(x => x.rad_info_id == id));
        }

        //
        // POST: /ingredient/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(int id, msds_ingredient model)
        {
            var dc = new hubDataClassesDataContext();
            msds_radiological_info rec = dc.msds_radiological_infos.First(x => x.rad_info_id == id);
            try
            {
                UpdateModel(rec);
                dc.SubmitChanges();
                return RedirectToAction("Index", new { id = model.msds_id });
            }
            catch
            {
                return View(model);
            }
        }

        //
        // GET: /ingredient/Delete/5

        public virtual ActionResult Delete(int id)
        {
            var dc = new hubDataClassesDataContext();
            msds_radiological_info model = dc.msds_radiological_infos.First(x => x.rad_info_id == id);
            var msds_id = model.msds_id;
            dc.msds_radiological_infos.DeleteOnSubmit(model);
            dc.SubmitChanges();
            return RedirectToAction("Index", new { id = msds_id });
        }

    }
}
