﻿using System.Linq;
using System.Web.Mvc;
using HiltHub.Models;

// using HiltHub.DatabaseUpdate;

namespace HiltHub.Controllers
{
    public partial class DashboardController : Controller
    {
        //
        // GET: /dashboard/

        public virtual ActionResult Index()
        {
            ViewBag.section = "dashboard";
            hubDataClassesDataContext dc = new hubDataClassesDataContext();

            IQueryable<v_AUL_Inventory_Detail> aul = dc.v_AUL_Inventory_Details;
            ViewBag.TotalAul = aul.Count();

            IQueryable<v_MSDS_HullType_Detail> msds = dc.v_MSDS_HullType_Details;
            ViewBag.TotalMsds = msds.Count();

            IQueryable<v_MSSL_Inventory_Detail> mssl = dc.v_MSSL_Inventory_Details;
            ViewBag.TotalMssl = mssl.Count();

            IQueryable<v_Ship> ships = dc.v_Ships;
            ViewBag.TotalShips = ships.Count();

            return View();
        }

        protected override void Initialize(
                System.Web.Routing.RequestContext rc) {
            base.Initialize(rc);

/*            DatabaseUpdater databaseUpdater = new DatabaseUpdater();
            SqlConnection connection = null;

            try {
                String connectionString =
                        System.Configuration.ConfigurationManager.ConnectionStrings[
                        "hilt_hubConnectionString"].ConnectionString;

                if (connectionString != null) {
                    Console.WriteLine("HILT_HUB ConnectionString = \"{0}\"",
                        connectionString);

                    connection = new SqlConnection(connectionString);
                    databaseUpdater.update(connection);
                }
                else
                    Console.WriteLine("No HILT_HUB ConnectionString");
            }
            catch (Exception ex) {
                Console.WriteLine("Error in Initialize - " + ex.Message);
            }
*/
        }
    }
}
