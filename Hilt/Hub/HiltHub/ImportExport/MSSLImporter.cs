﻿using System;
using System.IO;
using HiltHub.DataObjects;
using HiltHub.Parsers;

namespace HiltHub.ImportExport {
    public class MSSLImporter {
        public String import(String tempFileName, String fileName, int shipID, String userName,
            bool ignoreChanges) {
            String returnMsg = "";
            bool noChanges = true;

            try {
                DatabaseManager manager = new DatabaseManager();

                // Get the ship record
                Ship ship = manager.getShipRecord(shipID);

                StreamReader reader = new StreamReader(tempFileName);
                MSSLParser m = new MSSLParser();

                // Validate the vessel's UIC and upload date against the file UIC & date
                MSSLParser.FILE_STATUS status = m.validateFile(reader, ship.uic, ship.upload_mssl_date);
                if (status == MSSLParser.FILE_STATUS.Valid) {
                    // Parse the file and get the msslInventory data
                    reader = new StreamReader(tempFileName);
                    MSSLData data = m.parse(reader, ship.uic);
                    DateTime fileDate = m.fileDate;

                    // Check the inventory list for add, delete, change
                    DatabaseManager.FILE_CHANGES changes =
                        manager.validateMSSLChanges(data, ship.uic);
                    if (!ignoreChanges) {
                        switch (changes) {
                            case DatabaseManager.FILE_CHANGES.Initial_Load:
                                returnMsg = "WARNING - Initial MSSL load for this unit. " +
                                    "\nClick the Upload button to continue loading this file: " +
                                    fileName;
                                noChanges = false;
                                break;
                            case DatabaseManager.FILE_CHANGES.Threshold_Exceeded:
                                returnMsg =
                                    "WARNING - More than 25% of the MSSL records have changed." +
                                    "\nClick the Upload button to continue loading this file: " +
                                    fileName;
                                noChanges = false;
                                break;
                        }
                    }

                    if (noChanges) {
                        DateTime uploadDate = DateTime.Now;
                        // Process the MSSL data into the database
                        bool processed = manager.processMSSLInventory(ship.uic, data,
                            userName);

                        if (processed) {
                            // Update the ships table mssl upload field
                            manager.updateShipFileUpload(ship.uic, Configuration.UPLOAD_TYPES.MSSL,
                                uploadDate, userName);

                            // Insert a record into the file uploads table
                            manager.updateFileTable(DatabaseManager.FILE_TABLES.FileUpload,
                                ship.uic, fileName, Configuration.UPLOAD_TYPES.MSSL.ToString(),
                                fileDate, uploadDate, userName, data.MsslStats.NewRecords,
                                data.MsslStats.ChangedRecords, data.MsslStats.DeletedRecords);

                            returnMsg = data.InventoryList.Count + " MSSL records processed. " +
                                data.MsslStats.NewRecords + " new records;  " + 
                                data.MsslStats.ChangedRecords +" changed records; " +
                                data.MsslStats.DeletedRecords + " deleted records. ";

                        }
                        else {
                            returnMsg = "ERROR - processing MSSL file import: " +
                                fileName + " - See log file for details";
                        }
                    }
                }
                else {
                    switch (status) {
                        case MSSLParser.FILE_STATUS.Invalid_Type:
                            returnMsg = "ERROR - " + fileName +
                                " - The file is not a valid MSSL file." +
                                "\nPlease select another file to import";
                            break;
                        case MSSLParser.FILE_STATUS.Invalid_Date:
                            returnMsg = "ERROR - " + fileName + 
                                " - The file date is older than the last MSSL upload date." +
                                "\nPlease select another file to import";
                            break;
                        case MSSLParser.FILE_STATUS.Invalid_UIC:
                            returnMsg = "ERROR - " + fileName +
                                " The UIC in the file does not match the selected unit's UIC." +
                                "\nPlease select another file to import.";
                            break;
                        case MSSLParser.FILE_STATUS.Invalid_UIC_Date:
                            returnMsg = "ERROR - " + fileName +
                                " The UIC in the file does not match the selected unit's UIC " +
                                "and the file date is older than the last MSSL upload date." +
                                "\nPlease select another file to import";
                            break;
                    }
                }

            }
            catch (Exception ex) {
                returnMsg = "ERROR - processing MSSL file import: " + fileName + 
                    " - " + ex.Message +
                    " - See log file for details";
            }

            return returnMsg;
        }
    }
}