﻿using System;
using System.Collections.Generic;

namespace HiltHub.DataObjects {
    /// <summary>
    /// Container for all MSDS components of an MSDS record
    /// Basic msds info from the msds_master table is implemented as properties of
    /// the MSDSRecord. Data from MSDS sub tables are implemented as sub-objects of
    /// the MSDS Record
    /// </summary>
    public class MSDSRecord {
        #region Constructors
        // Default constructor
        public MSDSRecord() {
        }

        // Constructor using view models as input
        public MSDSRecord(Models.MsdsEditViewModel model) {
            this.ARTICLE_IND = model.MsdsMaster.ARTICLE_IND;
            this.CAGE = model.MsdsMaster.CAGE;
            this.DESCRIPTION = model.MsdsMaster.DESCRIPTION;
            this.EMERGENCY_TEL = model.MsdsMaster.EMERGENCY_TEL;
            this.END_COMP_IND = model.MsdsMaster.END_COMP_IND;
            this.END_ITEM_IND = model.MsdsMaster.END_ITEM_IND;
            this.FSC = model.MsdsMaster.FSC;
            this.KIT_IND = model.MsdsMaster.KIT_IND;
            this.KIT_PART_IND = model.MsdsMaster.KIT_PART_IND;
            this.MANUFACTURER = model.MsdsMaster.MANUFACTURER;
            this.MANUFACTURER_MSDS_NO = model.MsdsMaster.MANUFACTURER_MSDS_NO;
            this.MIXTURE_IND = model.MsdsMaster.MIXTURE_IND;
            this.MSDSSERNO = model.MsdsMaster.MSDSSERNO;
            this.NIIN = model.MsdsMaster.NIIN;
            this.PARTNO = model.MsdsMaster.PARTNO;
            this.PRODUCT_IDENTITY = model.MsdsMaster.PRODUCT_IDENTITY;
            this.PRODUCT_IND = model.MsdsMaster.PRODUCT_IND;
            this.PRODUCT_LANGUAGE = model.MsdsMaster.PRODUCT_LANGUAGE;
            this.PRODUCT_LOAD_DATE = (DateTime) model.MsdsMaster.PRODUCT_LOAD_DATE;
            this.PRODUCT_RECORD_STATUS = model.MsdsMaster.PRODUCT_RECORD_STATUS;
            this.PRODUCT_REVISION_NO = model.MsdsMaster.PRODUCT_REVISION_NO;
            this.PROPRIETARY_IND = model.MsdsMaster.PROPRIETARY_IND.ToString();
            this.PUBLISHED_IND = model.MsdsMaster.PUBLISHED_IND.ToString();
            this.PURCHASED_PROD_IND = model.MsdsMaster.PURCHASED_PROD_IND.ToString();
            this.PURE_IND = model.MsdsMaster.PURE_IND.ToString();
            this.RADIOACTIVE_IND = model.MsdsMaster.RADIOACTIVE_IND.ToString();
            this.SERVICE_AGENCY_CODE = model.MsdsMaster.SERVICE_AGENCY_CODE;
            this.TRADE_NAME = model.MsdsMaster.TRADE_NAME;
            this.TRADE_SECRET_IND = model.MsdsMaster.TRADE_SECRET_IND.ToString();

            this.Disposal = new MSDSDisposal(model.MsdsDisposal);
            this.DocumentTypes = model.MsdsDocumentTypes == null ? null : new MSDSDocumentTypes(model.MsdsDocumentTypes);
            this.ItemDescription = new MSDSItemDescription(model.MsdsItemDescription);
            this.LabelInfo = new MSDSLabelInfo(model.MsdsLabelInfo);
            this.PhysChemical = new MSDSPhysChemical(model.MsdsPhysChemical);
            this.Transportation = model.MsdsTransportation == null ? null : new MSDSTransportation(model.MsdsTransportation);

            this.ContractorList       = model.MsdsContractorList   == null ? null : buildContractListFromModel(model.MsdsContractorList);
            this.IngredientsList      = model.MsdsIngredientList   == null ? null : buildIngredientsListFromModel(model.MsdsIngredientList);
            this.RadiologicalInfoList = model.MsdsRadiologicalInfo == null ? null : buildRadiologicalInfoListFromModel(model.MsdsRadiologicalInfo);

            this.Afjm_Psn = new MSDS_AFJM_PSN(model.MsdsAfjmPsn);
            this.Dot_Psn = new MSDS_DOT_PSN(model.MsdsDotPsn);
            this.Iata_Psn = new MSDS_IATA_PSN(model.MsdsIataPsn);
            this.Imo_Psn = new MSDS_IMO_PSN(model.MsdsImoPsn);

            this.msds_id = model.MsdsMaster.msds_id;
            this.hcc_id = (long)model.MsdsMaster.hcc_id;
            this.file_name = model.MsdsMaster.file_name;
            this.manually_entered = (bool)model.MsdsMaster.manually_entered;
            this.changed = Convert.ToDateTime(model.MsdsMaster.changed);
            this.changed_by = model.MsdsMaster.changed_by;
            this.created = Convert.ToDateTime(model.MsdsMaster.created);
            this.created_by = model.MsdsMaster.created_by;
            this.data_source_cd = model.MsdsMaster.data_source_cd;            
        }

        private List<MSDSContractorInfo> buildContractListFromModel(
                List<Models.msds_contractor_info> modelList) {
            List<MSDSContractorInfo> list = new List<MSDSContractorInfo>();

            foreach (Models.msds_contractor_info item in modelList) {
                list.Add(new MSDSContractorInfo(item));
            }

            return list;
        }

        private List<MSDSIngredient> buildIngredientsListFromModel(
                List<Models.msds_ingredient> modelList) {
            List<MSDSIngredient> list = new List<MSDSIngredient>();

            foreach (Models.msds_ingredient item in modelList) {
                list.Add(new MSDSIngredient(item));
            }

            return list;
        }

        private List<MSDSRadiologicalInfo> buildRadiologicalInfoListFromModel(
                List<Models.msds_radiological_info> modelList) {
            List<MSDSRadiologicalInfo> list = new List<MSDSRadiologicalInfo>();

            foreach (Models.msds_radiological_info item in modelList) {
                list.Add(new MSDSRadiologicalInfo(item));
            }

            return list;
        }
        #endregion

        // MSDS MASTER TABLE
        #region MSDS TABLE
        // MSDS ID
        public long msds_id {
            get;
            set;
        }
        //PRODUCT SERIAL NUMBER
        public String MSDSSERNO {
            get;
            set;
        }
        //RESPONSIBLE PARTY CAGE.
        public String CAGE {
            get;
            set;
        }
        //RESPONSIBLE PARTY COMPANY NAME
        public String MANUFACTURER {
            get;
            set;
        }
        //PART NUMBER
        public String PARTNO {
            get;
            set;
        }
        //FSC
        public String FSC {
            get;
            set;
        }
        //NIIN
        public String NIIN {
            get;
            set;
        }
        //HCC ID
        public long hcc_id {
            get;
            set;
        }
        //HCC
        public String HCC {
            get;
            set;
        }
        // MSDS FILE NAME
        public String file_name {
            get;
            set;
        }
        //ARTICLE INDEX.
        public String ARTICLE_IND {
            get;
            set;
        }
        //DESCRIPTION
        public String DESCRIPTION {
            get;
            set;
        }
        //EMERGENCY TELEPHONE RESPONSIBLE PARTY
        public String EMERGENCY_TEL {
            get;
            set;
        }
        //END ITEM COMPONENT INDICATOR.
        public String END_COMP_IND {
            get;
            set;
        }
        //END ITEM INDICATOR.
        public String END_ITEM_IND {
            get;
            set;
        }
        //KIT INDICATOR.
        public String KIT_IND {
            get;
            set;
        }
        //KIT PART INDICATOR.
        public String KIT_PART_IND {
            get;
            set;
        }
        //MANUFACTURER MSDS NUMBER
        public String MANUFACTURER_MSDS_NO {
            get;
            set;
        }
        //MIXTURE INDICATOR.
        public String MIXTURE_IND {
            get;
            set;
        }
        //PRODUCT IDENTITY
        public String PRODUCT_IDENTITY {
            get;
            set;
        }
        //PRODUCT INDICATOR.
        public String PRODUCT_IND {
            get;
            set;
        }
        //PRODUCT LANGUAGE
        public String PRODUCT_LANGUAGE {
            get;
            set;
        }
        //PRODUCT LOAD DATE.
        public DateTime PRODUCT_LOAD_DATE {
            get;
            set;
        }
        //PRODUCT RECORD STATUS.
        public String PRODUCT_RECORD_STATUS {
            get;
            set;
        }
        //PRODUCT REVISION NUMBER.
        public String PRODUCT_REVISION_NO {
            get;
            set;
        }
        //PROPRIETARY INDICATOR
        public String PROPRIETARY_IND {
            get;
            set;
        }
        //PUBLISHED INDICATOR
        public String PUBLISHED_IND {
            get;
            set;
        }
        //PURCHASED PRODUCT INDICATOR
        public String PURCHASED_PROD_IND {
            get;
            set;
        }
        //PURE INDICATOR
        public String PURE_IND {
            get;
            set;
        }
        //RADIOACTIVE INDICATOR
        public String RADIOACTIVE_IND {
            get;
            set;
        }
        //SERVICE AGENCY
        public String SERVICE_AGENCY_CODE {
            get;
            set;
        }
        //TRADE NAME
        public String TRADE_NAME {
            get;
            set;
        }
        //TRADE SECRET INDICATOR
        public String TRADE_SECRET_IND {
            get;
            set;
        }
        // MANUALLY ENTERED
        public bool manually_entered {
            get;
            set;
        }
        // DATA SOURCE CD
        public String data_source_cd {
            get;
            set;
        }
        // CREATED DATETIME
        public DateTime created {
            get;
            set;
        }
        // CREATED BY
        public String created_by {
            get;
            set;
        }
        // CHANGED DATETIME
        public DateTime changed {
            get;
            set;
        }
        // CHANGED BY
        public String changed_by {
            get;
            set;
        }
        #endregion

        // Sub Table Objects
        #region SUB_TABLE OBJECTS
        //MSDS AFJM PSN
        public MSDS_AFJM_PSN Afjm_Psn {
            get;
            set;
        }
        //MSDS CONTRACTOR LIST
        public List<MSDSContractorInfo> ContractorList { 
            get; 
            set; 
        }
        //MSDS DISPOSAL
        public MSDSDisposal Disposal {
            get;
            set;
        }
        //MSDS DOC TYPES
        public MSDSDocumentTypes DocumentTypes { 
            get; 
            set; 
        }
        //MSDS DOT PSN
        public MSDS_DOT_PSN Dot_Psn { 
            get; 
            set; 
        }
        //MSDS IATA PSN
        public MSDS_IATA_PSN Iata_Psn { 
            get; 
            set; 
        }
        //MSDS IMO PSN
        public MSDS_IMO_PSN Imo_Psn { 
            get; 
            set; 
        } 
        // MSDS INGREDIENTS
        public List<MSDSIngredient> IngredientsList { 
            get; 
            set; 
        }
        //MSDS ITEM DESCRIPTION
        public MSDSItemDescription ItemDescription { 
            get; 
            set; 
        }
        //MSDS LABEL INFO
        public MSDSLabelInfo LabelInfo { 
            get; 
            set; 
        }
        //MSDS PHYS CHEMICAL TABLE
        public MSDSPhysChemical PhysChemical { 
            get; 
            set; 
        }
        //MSDS RADIOLOGICAL INFO
        public List<MSDSRadiologicalInfo> RadiologicalInfoList { 
            get; 
            set; 
        }
        //MSDS TRANSPORTATION
        public MSDSTransportation Transportation {
            get;
            set;
        }
        #endregion

        #region HMIRS ID Fields
        public long doc_index_id {
            get;
            set;
    }
        public long doc_data_id {
            get;
            set;
        }
        #endregion
    }

    //MSDS AFJM PSN
    // The database view v_MSDS_AFJM_PSN_Detail genereates this data
    public class MSDS_AFJM_PSN {
        public MSDS_AFJM_PSN() { 
        }

        public MSDS_AFJM_PSN(Models.msds_afjm_psn MsdsAfjmPsn) {
            if (MsdsAfjmPsn == null)
                return;
            this.afjm_psn_id = MsdsAfjmPsn.afjm_psn_id;
            this.AFJM_HAZARD_CLASS = MsdsAfjmPsn.AFJM_HAZARD_CLASS;
            this.AFJM_PACK_GROUP = MsdsAfjmPsn.AFJM_PACK_GROUP;
            this.AFJM_PACK_PARAGRAPH = MsdsAfjmPsn.AFJM_PACK_PARAGRAPH;
            this.AFJM_PROP_SHIP_MODIFIER = MsdsAfjmPsn.AFJM_PROP_SHIP_MODIFIER;
            this.AFJM_PROP_SHIP_NAME = MsdsAfjmPsn.AFJM_PROP_SHIP_NAME;
            this.AFJM_PSN_CODE = MsdsAfjmPsn.AFJM_PSN_CODE;
            this.AFJM_SPECIAL_PROV = MsdsAfjmPsn.AFJM_SPECIAL_PROV;
            this.AFJM_SUBSIDIARY_RISK = MsdsAfjmPsn.AFJM_SUBSIDIARY_RISK;
            this.AFJM_SYMBOLS = MsdsAfjmPsn.AFJM_SYMBOLS;
            this.AFJM_UN_ID_NUMBER = MsdsAfjmPsn.AFJM_UN_ID_NUMBER;
        }

        //AFJM PSN ID
        public long afjm_psn_id {
            get;
            set;
        }
        //AFJM HAZARD CLASS/DIV
        public String AFJM_HAZARD_CLASS {
            get;
            set;
        }
        //AFJM PACKAGING PARAGRAPH
        public String AFJM_PACK_PARAGRAPH {
            get;
            set;
        }
        //AFJM PACKING GROUP
        public String AFJM_PACK_GROUP {
            get;
            set;
        }
        //AFJM PROPER SHIPPING NAME
        public String AFJM_PROP_SHIP_NAME {
            get;
            set;
        }
        //AFJM PROPER SHIPPING NAME MODIFIER
        public String AFJM_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //AFJM PSN CODE
        public String AFJM_PSN_CODE {
            get;
            set;
        }
        //AFJM SPECIAL PROVISIONS
        public String AFJM_SPECIAL_PROV {
            get;
            set;
        }
        //AFJM SUBSIDIARY RISK
        public String AFJM_SUBSIDIARY_RISK {
            get;
            set;
        }
        //AFJM SYMBOLS
        public String AFJM_SYMBOLS {
            get;
            set;
        }
        //AFJM UN ID NUMBER
        public String AFJM_UN_ID_NUMBER {
            get;
            set;
        }
    }

    //MSDS CONTRACTOR INFO
    // The database view v_MSDS_Contract_Detail genereates this data
    public class MSDSContractorInfo {
        public MSDSContractorInfo() {
        }

        public MSDSContractorInfo(Models.msds_contractor_info MsdsInfo) {
            this.contractor_id = MsdsInfo.contractor_id;
            this.CT_ADDRESS_1 = MsdsInfo.CT_ADDRESS_1;
            this.CT_CAGE = MsdsInfo.CT_CAGE;
            this.CT_CITY = MsdsInfo.CT_CITY;
            this.CT_COMPANY_NAME = MsdsInfo.CT_COMPANY_NAME;
            this.CT_COUNTRY = MsdsInfo.CT_COUNTRY;
            //this.CT_NUMBER = MsdsInfo.CT_NUMBER;
            this.CT_PHONE = MsdsInfo.CT_PHONE;
            this.CT_PO_BOX = MsdsInfo.CT_PO_BOX;
            this.CT_STATE = MsdsInfo.CT_STATE;
            this.CT_ZIP_CODE = MsdsInfo.CT_ZIP_CODE;
            //this.PURCHASE_ORDER_NO = MsdsInfo.PURCHASE_ORDER_NO;
        }

        //CONTRACTOR ID
        public long contractor_id {
            get;
            set;
        }
        //CONTRACT NUMBER
        public string CT_NUMBER {
            get;
            set;
        }
        //CONTRACTOR CAGE
        public string CT_CAGE {
            get;
            set;
        }
        //CONTRACTOR CITY
        public string CT_CITY {
            get;
            set;
        }
        //CONTRACTOR COMPANY NAME
        public string CT_COMPANY_NAME {
            get;
            set;
        }
        //CONTRACTOR COUNTRY
        public string CT_COUNTRY {
            get;
            set;
        }
        //CONTRACTOR PO BOX
        public string CT_PO_BOX {
            get;
            set;
        }
        //CONTRACTOR STATE
        public string CT_STATE {
            get;
            set;
        }
        //CONTRACTOR TELE NUMBER
        public string CT_PHONE {
            get;
            set;
        }
        //PURCHASE ORDER NUMBER
        public string PURCHASE_ORDER_NO {
            get;
            set;
        }
        //Addess 1
        public string CT_ADDRESS_1 {
            get;
            set;
        }
        //Zip Code
        public string CT_ZIP_CODE {
            get;
            set;
        }
    }

    //MSDS DISPOSAL
    public class MSDSDisposal {
        public MSDSDisposal() {
        }

        public MSDSDisposal(Models.msds_disposal MsdsDisposal) {
            this.DISPOSAL_ADD_INFO = MsdsDisposal.DISPOSAL_ADD_INFO;
            this.EPA_HAZ_WASTE_CODE = MsdsDisposal.EPA_HAZ_WASTE_CODE;
            this.EPA_HAZ_WASTE_IND = MsdsDisposal.EPA_HAZ_WASTE_IND.ToString();
            this.EPA_HAZ_WASTE_NAME = MsdsDisposal.EPA_HAZ_WASTE_NAME;
        }

        //DISPOSAL ADDITIONAL INFO
        public String DISPOSAL_ADD_INFO {
            get;
            set;
        }
        //EPA HAZARDOUS WASTE CODE
        public String EPA_HAZ_WASTE_CODE {
            get;
            set;
        }
        //EPA HAZARDOUS WAST INDICATOR
        public String EPA_HAZ_WASTE_IND {
            get;
            set;
        }
        //EPA HAZARDOUS WASTE NAME
        public String EPA_HAZ_WASTE_NAME {
            get;
            set;
        }
    }

    //MSDS DOC TYPES
    public class MSDSDocumentTypes {
        public MSDSDocumentTypes() {
        }

        public MSDSDocumentTypes(Models.msds_document_type MsdsDocType) {
            this.manufacturer_label_filename = MsdsDocType.manufacturer_label_filename;
            this.msds_translated_filename = MsdsDocType.msds_translated_filename;
            this.neshap_comp_filename = MsdsDocType.neshap_comp_filename;
            this.other_docs_filename = MsdsDocType.other_docs_filename;
            this.product_sheet_filename = MsdsDocType.product_sheet_filename;
            this.transportation_cert_filename = MsdsDocType.transportation_cert_filename;
        }

        //MSDS - SEE MSDS TABLE.
        //MSDS MANUFACTURER LABEL
        public String manufacturer_label_filename {
            get;
            set;
        }
        //MSDS TRANSLATED
        public String msds_translated_filename {
            get;
            set;
        }
        //NESHAP COMPLIANCE CERTIFICATE
        public String neshap_comp_filename {
            get;
            set;
        }
        //OTHER DOCS
        public String other_docs_filename {
            get;
            set;
        }
        //PRODUCT SHEET
        public String product_sheet_filename {
            get;
            set;
        }
        //TRANSPORTATION CERT
        public String transportation_cert_filename {
            get;
            set;
        }
    }

    //MSDS DOT PSN
    // The database view v_MSDS_DOT_PSN_Detail genereates this data
    public class MSDS_DOT_PSN {
        public MSDS_DOT_PSN() {
        }

        public MSDS_DOT_PSN(Models.msds_dot_psn MsdsDotPsn) {
            if (MsdsDotPsn == null) return;
            this.dot_psn_id = MsdsDotPsn.dot_psn_id;
            this.DOT_HAZARD_CLASS_DIV = MsdsDotPsn.DOT_HAZARD_CLASS_DIV;
            this.DOT_HAZARD_LABEL = MsdsDotPsn.DOT_HAZARD_LABEL;
            this.DOT_MAX_CARGO = MsdsDotPsn.DOT_MAX_CARGO;
            this.DOT_MAX_PASSENGER = MsdsDotPsn.DOT_MAX_PASSENGER;
            this.DOT_PACK_BULK = MsdsDotPsn.DOT_PACK_BULK;
            this.DOT_PACK_EXCEPTIONS = MsdsDotPsn.DOT_PACK_EXCEPTIONS;
            this.DOT_PACK_GROUP = MsdsDotPsn.DOT_PACK_GROUP;
            this.DOT_PACK_NONBULK = MsdsDotPsn.DOT_PACK_NONBULK;
            this.DOT_PROP_SHIP_MODIFIER = MsdsDotPsn.DOT_PROP_SHIP_MODIFIER;
            this.DOT_PROP_SHIP_NAME = MsdsDotPsn.DOT_PROP_SHIP_NAME;
            this.DOT_PSN_CODE = MsdsDotPsn.DOT_PSN_CODE;
            this.DOT_SPECIAL_PROVISION = MsdsDotPsn.DOT_SPECIAL_PROVISION;
            this.DOT_SYMBOLS = MsdsDotPsn.DOT_SYMBOLS;
            this.DOT_UN_ID_NUMBER = MsdsDotPsn.DOT_UN_ID_NUMBER;
            this.DOT_WATER_OTHER_REQ = MsdsDotPsn.DOT_WATER_OTHER_REQ;
            this.DOT_WATER_VESSEL_STOW = MsdsDotPsn.DOT_WATER_VESSEL_STOW;
        }

        //DOT PSN ID
        public long dot_psn_id {
            get;
            set;
        }
        //DOT HAZARD CLASS/DIV
        public String DOT_HAZARD_CLASS_DIV {
            get;
            set;
        }
        //DOT HAZARD LABEL
        public String DOT_HAZARD_LABEL {
            get;
            set;
        }
        //DOT MAX QUANTITY: CARGO AIRCRAFT ONLY
        public String DOT_MAX_CARGO {
            get;
            set;
        }
        //DOT MAX QUANTITY: PASSENGER AIRCRAFT/RAIL
        public String DOT_MAX_PASSENGER {
            get;
            set;
        }
        //DOT PACKAGING BULK
        public String DOT_PACK_BULK {
            get;
            set;
        }
        //DOT PACKAGING EXCEPTIONS
        public String DOT_PACK_EXCEPTIONS {
            get;
            set;
        }
        //DOT PACKAGING NON BULK
        public String DOT_PACK_NONBULK {
            get;
            set;
        }
        //DOT PACKING GROUP
        public String DOT_PACK_GROUP {
            get;
            set;
        }
        //DOT PROPER SHIPPING NAME
        public String DOT_PROP_SHIP_NAME {
            get;
            set;
        }
        //DOT PROPER SHIPPING NAME MODIFIER
        public String DOT_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //DOT PSN CODE
        public String DOT_PSN_CODE {
            get;
            set;
        }
        //DOT SPECIAL PROVISION
        public String DOT_SPECIAL_PROVISION {
            get;
            set;
        }
        //DOT SYMBOLS
        public String DOT_SYMBOLS {
            get;
            set;
        }
        //DOT UN ID NUMBER
        public String DOT_UN_ID_NUMBER {
            get;
            set;
        }
        //DOT WATER SHIPMENT OTHER REQS
        public String DOT_WATER_OTHER_REQ {
            get;
            set;
        }
        //DOT WATER SHIPMENT VESSEL STOWAGE
        public String DOT_WATER_VESSEL_STOW {
            get;
            set;
        }
    }

    //MSDS IATA PSN
    // The database view v_MSDS_IATA_PSN_Detail genereates this data
    public class MSDS_IATA_PSN {
        public MSDS_IATA_PSN() {
        }

        public MSDS_IATA_PSN(Models.msds_iata_psn MsdsIataPsn) {
            if (MsdsIataPsn == null) return;
            this.iata_psn_id = MsdsIataPsn.iata_psn_id;
            this.IATA_CARGO_PACK_MAX_QTY = MsdsIataPsn.IATA_CARGO_PACK_MAX_QTY;
            this.IATA_CARGO_PACKING = MsdsIataPsn.IATA_CARGO_PACKING;
            this.IATA_HAZARD_CLASS = MsdsIataPsn.IATA_HAZARD_CLASS;
            this.IATA_HAZARD_LABEL = MsdsIataPsn.IATA_HAZARD_LABEL;
            this.IATA_PACK_GROUP = MsdsIataPsn.IATA_PACK_GROUP;
            this.IATA_PASS_AIR_MAX_QTY = MsdsIataPsn.IATA_PASS_AIR_MAX_QTY;
            this.IATA_PASS_AIR_PACK_LMT_INSTR = MsdsIataPsn.IATA_PASS_AIR_PACK_LMT_INSTR;
            this.IATA_PASS_AIR_PACK_LMT_PER_PKG = MsdsIataPsn.IATA_PASS_AIR_PACK_LMT_PER_PKG;
            this.IATA_PASS_AIR_PACK_NOTE = MsdsIataPsn.IATA_PASS_AIR_PACK_NOTE;
            this.IATA_PROP_SHIP_MODIFIER = MsdsIataPsn.IATA_PROP_SHIP_MODIFIER;
            this.IATA_PROP_SHIP_NAME = MsdsIataPsn.IATA_PROP_SHIP_NAME;
            this.IATA_PSN_CODE = MsdsIataPsn.IATA_PSN_CODE;
            this.IATA_SPECIAL_PROV = MsdsIataPsn.IATA_SPECIAL_PROV;
            this.IATA_SUBSIDIARY_RISK = MsdsIataPsn.IATA_SUBSIDIARY_RISK;
            this.IATA_UN_ID_NUMBER = MsdsIataPsn.IATA_UN_ID_NUMBER;
        }

        //IATA PSN ID
        public long iata_psn_id {
            get;
            set;
        }
        //IATA CARGO PACKING: NOTE CARGO AIRCRAFT PACKING INSTRUCTIONS
        public String IATA_CARGO_PACKING {
            get;
            set;
        }
        //IATA HAZARD CLASS/DIV
        public String IATA_HAZARD_CLASS {
            get;
            set;
        }
        //IATA HAZARD LABEL
        public String IATA_HAZARD_LABEL {
            get;
            set;
        }
        //IATA PACKING GROUP
        public String IATA_PACK_GROUP {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: LMT QTY PKG INSTR
        public String IATA_PASS_AIR_PACK_LMT_INSTR {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: LMT QTY MAX QTY PER PKG
        public String IATA_PASS_AIR_PACK_LMT_PER_PKG {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING MAX QUANTITY
        public String IATA_PASS_AIR_MAX_QTY {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: NOTE PASSENGER AIR PACKING INSTR
        public String IATA_PASS_AIR_PACK_NOTE {
            get;
            set;
        }
        //IATA PROPER SHIPPING NAME
        public String IATA_PROP_SHIP_NAME {
            get;
            set;
        }
        //IATA PROPER SHIPPING NAME MODIFIER
        public String IATA_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //IATA CARGO PACKING: MAX QUANTITY
        public String IATA_CARGO_PACK_MAX_QTY {
            get;
            set;
        }
        //IATA PSN CODE
        public String IATA_PSN_CODE {
            get;
            set;
        }
        //IATA SPECIAL PROVISIONS
        public String IATA_SPECIAL_PROV {
            get;
            set;
        }
        //IATA SUBSIDIARY RISK
        public String IATA_SUBSIDIARY_RISK {
            get;
            set;
        }
        //IATA UN ID NUMBER
        public String IATA_UN_ID_NUMBER {
            get;
            set;
        }
    }

    //MSDS IMO PSN
    // The database view v_MSDS_IMO_PSN_Detail genereates this data
    public class MSDS_IMO_PSN {
        public MSDS_IMO_PSN() {
        }

        public MSDS_IMO_PSN(Models.msds_imo_psn MsdsImoPsn) {
            if (MsdsImoPsn == null) return;
            this.imo_psn_id = MsdsImoPsn.imo_psn_id;
            this.IMO_EMS_NO = MsdsImoPsn.IMO_EMS_NO;
            this.IMO_HAZARD_CLASS = MsdsImoPsn.IMO_HAZARD_CLASS;
            this.IMO_IBC_INSTR = MsdsImoPsn.IMO_IBC_INSTR;
            this.IMO_IBC_PROVISIONS = MsdsImoPsn.IMO_IBC_PROVISIONS;
            this.IMO_LIMITED_QTY = MsdsImoPsn.IMO_LIMITED_QTY;
            this.IMO_PACK_GROUP = MsdsImoPsn.IMO_PACK_GROUP;
            this.IMO_PACK_INSTRUCTIONS = MsdsImoPsn.IMO_PACK_INSTRUCTIONS;
            this.IMO_PACK_PROVISIONS = MsdsImoPsn.IMO_PACK_PROVISIONS;
            this.IMO_PROP_SHIP_MODIFIER = MsdsImoPsn.IMO_PROP_SHIP_MODIFIER;
            this.IMO_PROP_SHIP_NAME = MsdsImoPsn.IMO_PROP_SHIP_NAME;
            this.IMO_PSN_CODE = MsdsImoPsn.IMO_PSN_CODE;
            this.IMO_SPECIAL_PROV = MsdsImoPsn.IMO_SPECIAL_PROV;
            this.IMO_STOW_SEGR = MsdsImoPsn.IMO_STOW_SEGR;
            this.IMO_SUBSIDIARY_RISK = MsdsImoPsn.IMO_SUBSIDIARY_RISK;
            this.IMO_TANK_INSTR_IMO = MsdsImoPsn.IMO_TANK_INSTR_IMO;
            this.IMO_TANK_INSTR_PROV = MsdsImoPsn.IMO_TANK_INSTR_PROV;
            this.IMO_TANK_INSTR_UN = MsdsImoPsn.IMO_TANK_INSTR_UN;
            this.IMO_UN_NUMBER = MsdsImoPsn.IMO_UN_NUMBER;
        }

        //IMO PSN ID
        public long imo_psn_id {
            get;
            set;
        }
        //IMO EMS NUMBER
        public String IMO_EMS_NO {
            get;
            set;
        }
        //IMO HAZARD CLASS/DIV
        public String IMO_HAZARD_CLASS {
            get;
            set;
        }
        //IMO IBC INSTRUCTIONS
        public String IMO_IBC_INSTR {
            get;
            set;
        }
        //IMO IBC PROVISIONS
        public String IMO_IBC_PROVISIONS {
            get;
            set;
        }
        //IMO LIMITED QUANTITY
        public String IMO_LIMITED_QTY {
            get;
            set;
        }
        //IMO PACKING GROUP
        public String IMO_PACK_GROUP {
            get;
            set;
        }
        //IMO PACKING INSTRUCTIONS
        public String IMO_PACK_INSTRUCTIONS {
            get;
            set;
        }
        //IMO PACKING PROVISIONS
        public String IMO_PACK_PROVISIONS {
            get;
            set;
        }
        //IMO PROPER SHIPPING NAME
        public String IMO_PROP_SHIP_NAME {
            get;
            set;
        }
        //IMO PROPER SHIPPING NAME MODIFIER
        public String IMO_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //IMO PSN CODE
        public String IMO_PSN_CODE {
            get;
            set;
        }
        //IMO SPECIAL PROVISIONS
        public String IMO_SPECIAL_PROV {
            get;
            set;
        }
        //IMO STOWAGE AND SEGREGATION
        public String IMO_STOW_SEGR {
            get;
            set;
        }
        //IMO SUBSIDIARY RISK LABEL
        public String IMO_SUBSIDIARY_RISK {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, IMO
        public String IMO_TANK_INSTR_IMO {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, PROVISIONS
        public String IMO_TANK_INSTR_PROV {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, UN
        public String IMO_TANK_INSTR_UN {
            get;
            set;
        }
        //IMO UN NUMBER
        public String IMO_UN_NUMBER {
            get;
            set;
        }
    }

    //MSDS INGREDIENTS
    public class MSDSIngredient {
        public MSDSIngredient() {
        }

        public MSDSIngredient(Models.msds_ingredient MsdsIngred) {
            if (MsdsIngred == null) return;
            this.ACGIH_STEL = MsdsIngred.ACGIH_STEL;
            this.ACGIH_TLV = MsdsIngred.ACGIH_TLV;
            this.CAS = MsdsIngred.CAS;
            this.CHEM_MFG_COMP_NAME = MsdsIngred.CHEM_MFG_COMP_NAME;
            this.DOT_REPORT_QTY = MsdsIngred.DOT_REPORT_QTY;
            this.EPA_REPORT_QTY = MsdsIngred.EPA_REPORT_QTY;
            this.INGREDIENT_NAME = MsdsIngred.INGREDIENT_NAME;
            this.ODS_IND = MsdsIngred.ODS_IND;
            this.OSHA_PEL = MsdsIngred.OSHA_PEL;
            this.OSHA_STEL = MsdsIngred.OSHA_STEL;
            this.OTHER_REC_LIMITS = MsdsIngred.OTHER_REC_LIMITS;
            this.PRCNT = MsdsIngred.PRCNT;
            this.PRCNT_VOL_VALUE = MsdsIngred.PRCNT_VOL_VALUE;
            this.PRCNT_VOL_WEIGHT = MsdsIngred.PRCNT_VOL_WEIGHT;
            this.RTECS_CODE = MsdsIngred.RTECS_CODE;
            this.RTECS_NUM = MsdsIngred.RTECS_NUM;
        }

        //PERCENT TEXT value
        public string PRCNT {
            get;
            set;
        }
        //PERCENT VOLUME value
        public string PRCNT_VOL_VALUE {
            get;
            set;
        }
        //PERCENT WEIGHT value
        public string PRCNT_VOL_WEIGHT {
            get;
            set;
        }
        //ACGIH STEL
        public string ACGIH_STEL {
            get;
            set;
        }
        //ACGIH TLV
        public string ACGIH_TLV {
            get;
            set;
        }
        //CAS NUMBER
        public string CAS {
            get;
            set;
        }
        //CHEMICAL MANUFACTURER COMPANY NAME
        public string CHEM_MFG_COMP_NAME {
            get;
            set;
        }
        //COMPONENT INGREDIENT NAME
        public string INGREDIENT_NAME {
            get;
            set;
        }
        //DOT RQ
        public string DOT_REPORT_QTY {
            get;
            set;
        }
        //EPA RQ
        public string EPA_REPORT_QTY {
            get;
            set;
        }
        //ODS INDICATOR
        public string ODS_IND {
            get;
            set;
        }
        //OSHA PEL
        public string OSHA_PEL {
            get;
            set;
        }
        //OSHA STEL
        public string OSHA_STEL {
            get;
            set;
        }
        //OTHER RECORDED LIMITS
        public string OTHER_REC_LIMITS {
            get;
            set;
        }
        //RTECS NUMBER
        public string RTECS_NUM {
            get;
            set;
        }
        //RTECS CODE
        public string RTECS_CODE {
            get;
            set;
        }
    }

    //MSDS ITEM DESCRIPTION
    public class MSDSItemDescription {
        public MSDSItemDescription() {
        }

        public MSDSItemDescription(Models.msds_item_description MsdsItemDesc) {
            this.BATCH_NUMBER = MsdsItemDesc.BATCH_NUMBER;
            this.ITEM_MANAGER = MsdsItemDesc.ITEM_MANAGER;
            this.ITEM_NAME = MsdsItemDesc.ITEM_NAME;
            this.LOG_FLIS_NIIN_VER = MsdsItemDesc.LOG_FLIS_NIIN_VER.ToString();
            this.LOG_FSC = (int) MsdsItemDesc.LOG_FSC;
            this.LOT_NUMBER = MsdsItemDesc.LOT_NUMBER;
            this.NET_UNIT_WEIGHT = MsdsItemDesc.NET_UNIT_WEIGHT;
            this.QUANTITATIVE_EXPRESSION = MsdsItemDesc.QUANTITATIVE_EXPRESSION;
            this.SHELF_LIFE_CODE = MsdsItemDesc.SHELF_LIFE_CODE;
            this.SPECIAL_EMP_CODE = MsdsItemDesc.SPECIAL_EMP_CODE;
            this.SPECIFICATION_NUMBER = MsdsItemDesc.SPECIFICATION_NUMBER;
            this.TYPE_GRADE_CLASS = MsdsItemDesc.TYPE_GRADE_CLASS;
            this.TYPE_OF_CONTAINER = MsdsItemDesc.TYPE_OF_CONTAINER;
            this.UI_CONTAINER_QTY = MsdsItemDesc.UI_CONTAINER_QTY;
            this.UN_NA_NUMBER = MsdsItemDesc.UN_NA_NUMBER;
            this.UNIT_OF_ISSUE = MsdsItemDesc.UNIT_OF_ISSUE;
            this.UPC_GTIN = MsdsItemDesc.UPC_GTIN;
        }


        //BATCH NUMBER
        public String BATCH_NUMBER {
            get;
            set;
        }
        //LOT NUMBER
        public String LOT_NUMBER {
            get;
            set;
        }
        //ITEM MANAGER
        public String ITEM_MANAGER {
            get;
            set;
        }
        //ITEM NAME
        public String ITEM_NAME {
            get;
            set;
        }
        //LOGISTICS FLSI NIIN VERIFIED
        public String LOG_FLIS_NIIN_VER {
            get;
            set;
        }
        //LOGISTICS FSC
        public int LOG_FSC {
            get;
            set;
        }
        //NET UNIT WEIGHT
        public String NET_UNIT_WEIGHT {
            get;
            set;
        }
        //QUANTITATIVE EXPRESSION EXP
        public String QUANTITATIVE_EXPRESSION {
            get;
            set;
        }
        //SHELF LIFE CODE
        public String SHELF_LIFE_CODE {
            get;
            set;
        }
        //SPECIAL EMPHASIS CODE
        public String SPECIAL_EMP_CODE {
            get;
            set;
        }
        //SPECIFICATION NUMBER
        public String SPECIFICATION_NUMBER {
            get;
            set;
        }
        //SPECIFICATION TYPE/GRADE/CLASS
        public String TYPE_GRADE_CLASS {
            get;
            set;
        }
        //TYPE OF CONTAINER
        public String TYPE_OF_CONTAINER {
            get;
            set;
        }
        //UN/NA NUMBER
        public String UN_NA_NUMBER {
            get;
            set;
        }
        //UNIT OF ISSUE
        public String UNIT_OF_ISSUE {
            get;
            set;
        }
        //UNIT OF ISSUE CONTAINER QUANTITY
        public String UI_CONTAINER_QTY {
            get;
            set;
        }
        //UPC/GTIN
        public String UPC_GTIN {
            get;
            set;
        }
    }

    //MSDS LABEL INFO
    public class MSDSLabelInfo {
        public MSDSLabelInfo() {
        }

        public MSDSLabelInfo(Models.msds_label_info MsdsLabel) {
            this.COMPANY_CAGE_RP = MsdsLabel.COMPANY_CAGE_RP;
            this.COMPANY_NAME_RP = MsdsLabel.COMPANY_NAME_RP;
            this.LABEL_EMERG_PHONE = MsdsLabel.LABEL_EMERG_PHONE;
            this.LABEL_ITEM_NAME = MsdsLabel.LABEL_ITEM_NAME;
            this.LABEL_PROC_YEAR = MsdsLabel.LABEL_PROC_YEAR;
            this.LABEL_PROD_IDENT = MsdsLabel.LABEL_PROD_IDENT;
            this.LABEL_PROD_SERIALNO = MsdsLabel.LABEL_PROD_SERIALNO;
            this.LABEL_SIGNAL_WORD_CODE = MsdsLabel.LABEL_SIGNAL_WORD_CODE;
            this.LABEL_STOCK_NO = MsdsLabel.LABEL_STOCK_NO;
            this.SPECIFIC_HAZARDS = MsdsLabel.SPECIFIC_HAZARDS;
        }

        //COMPANY CAGE(RESPONSIBLE PARTY)
        public String COMPANY_CAGE_RP {
            get;
            set;
        }
        //COMPANY NAME(RESPONSIBLE PARTY)
        public String COMPANY_NAME_RP {
            get;
            set;
        }
        //LABEL EMERGENCY TELE NUMBER
        public String LABEL_EMERG_PHONE {
            get;
            set;
        }
        //LABEL ITEM NAME
        public String LABEL_ITEM_NAME {
            get;
            set;
        }
        //LABEL PROCUREMENT YEAR
        public String LABEL_PROC_YEAR {
            get;
            set;
        }
        //LABEL PRODUCT IDENTITY
        public String LABEL_PROD_IDENT {
            get;
            set;
        }
        //LABEL PRODUCT SERIAL NUMBER
        public String LABEL_PROD_SERIALNO {
            get;
            set;
        }
        //LABEL SIGNAL WORD
        public String LABEL_SIGNAL_WORD_CODE {
            get;
            set;
        }
        //LABEL STOCK NUMBER
        public String LABEL_STOCK_NO {
            get;
            set;
        }
        //SPECIFIC HAZARDS
        public String SPECIFIC_HAZARDS {
            get;
            set;
        }
    }

    //MSDS PHYS CHEMICAL TABLE
    public class MSDSPhysChemical {
        public MSDSPhysChemical() {
        }

        public MSDSPhysChemical(Models.msds_phys_chemical MsdsPhysChem) {
            this.APP_ODOR = MsdsPhysChem.APP_ODOR;
            this.AUTOIGNITION_TEMP = MsdsPhysChem.AUTOIGNITION_TEMP;
            this.CARCINOGEN_IND = MsdsPhysChem.CARCINOGEN_IND.ToString();
            this.EPA_ACUTE = MsdsPhysChem.EPA_ACUTE.ToString();
            this.EPA_CHRONIC = MsdsPhysChem.EPA_CHRONIC.ToString();
            this.EPA_FIRE = MsdsPhysChem.EPA_FIRE.ToString();
            this.EPA_PRESSURE = MsdsPhysChem.EPA_PRESSURE.ToString();
            this.EPA_REACTIVITY = MsdsPhysChem.EPA_REACTIVITY.ToString();
            this.EVAP_RATE_REF = MsdsPhysChem.EVAP_RATE_REF;
            this.FLASH_PT_TEMP = MsdsPhysChem.FLASH_PT_TEMP;
            this.NEUT_AGENT = MsdsPhysChem.NEUT_AGENT;
            this.NFPA_FLAMMABILITY = MsdsPhysChem.NFPA_FLAMMABILITY;
            this.NFPA_HEALTH = MsdsPhysChem.NFPA_HEALTH;
            this.NFPA_REACTIVITY = MsdsPhysChem.NFPA_REACTIVITY;
            this.NFPA_SPECIAL = MsdsPhysChem.NFPA_SPECIAL;
            this.OSHA_CARCINOGENS = MsdsPhysChem.OSHA_CARCINOGENS.ToString();
            this.OSHA_COMB_LIQUID = MsdsPhysChem.OSHA_COMB_LIQUID.ToString();
            this.OSHA_COMP_GAS = MsdsPhysChem.OSHA_COMP_GAS.ToString();
            this.OSHA_CORROSIVE = MsdsPhysChem.OSHA_CORROSIVE.ToString();
            this.OSHA_EXPLOSIVE = MsdsPhysChem.OSHA_EXPLOSIVE.ToString();
            this.OSHA_FLAMMABLE = MsdsPhysChem.OSHA_FLAMMABLE.ToString();
            this.OSHA_HIGH_TOXIC = MsdsPhysChem.OSHA_HIGH_TOXIC.ToString();
            this.OSHA_IRRITANT = MsdsPhysChem.OSHA_IRRITANT.ToString();
            this.OSHA_ORG_PEROX = MsdsPhysChem.OSHA_ORG_PEROX.ToString();
            this.OSHA_OTHERLONGTERM = MsdsPhysChem.OSHA_OTHERLONGTERM.ToString();
            this.OSHA_OXIDIZER = MsdsPhysChem.OSHA_OXIDIZER.ToString();
            this.OSHA_PYRO = MsdsPhysChem.OSHA_PYRO.ToString();
            this.OSHA_SENSITIZER = MsdsPhysChem.OSHA_SENSITIZER.ToString();
            this.OSHA_TOXIC = MsdsPhysChem.OSHA_TOXIC.ToString();
            this.OSHA_UNST_REACT = MsdsPhysChem.OSHA_UNST_REACT.ToString();
            this.OSHA_WATER_REACTIVE = MsdsPhysChem.OSHA_WATER_REACTIVE.ToString();
            this.OTHER_SHORT_TERM = MsdsPhysChem.OTHER_SHORT_TERM;
            this.PERCENT_VOL_VOLUME = MsdsPhysChem.PERCENT_VOL_VOLUME;
            this.PH = MsdsPhysChem.PH;
            this.PHYS_STATE_CODE = MsdsPhysChem.PHYS_STATE_CODE;
            this.SOL_IN_WATER = MsdsPhysChem.SOL_IN_WATER;
            this.SPECIFIC_GRAV = MsdsPhysChem.SPECIFIC_GRAV;
            this.VAPOR_DENS = MsdsPhysChem.VAPOR_DENS;
            this.VAPOR_PRESS = MsdsPhysChem.VAPOR_PRESS;
            this.VISCOSITY = MsdsPhysChem.VISCOSITY;
            this.VOC_GRAMS_LITER = MsdsPhysChem.VOC_GRAMS_LITER;
            this.VOC_POUNDS_GALLON = MsdsPhysChem.VOC_POUNDS_GALLON;
            this.VOL_ORG_COMP_WT = MsdsPhysChem.VOL_ORG_COMP_WT;
        }

        //OSHA_WATER_REACTIVE
        public String OSHA_WATER_REACTIVE {
            get;
            set;
        }
        //PERCENT VOLATILES BY VOLUME
        public String PERCENT_VOL_VOLUME {
            get;
            set;
        }
        //APPEARANCE ODOR TEXT
        public String APP_ODOR {
            get;
            set;
        }
        //AUTOIGNITION TEMP. (C)
        public String AUTOIGNITION_TEMP {
            get;
            set;
        }
        //CARCINOGEN INDICATOR
        public String CARCINOGEN_IND {
            get;
            set;
        }
        //EPA ACUTE
        public String EPA_ACUTE {
            get;
            set;
        }
        //EPA CHRONIC
        public String EPA_CHRONIC {
            get;
            set;
        }
        //EPA FIRE
        public String EPA_FIRE {
            get;
            set;
        }
        //EPA PRESSURE
        public String EPA_PRESSURE {
            get;
            set;
        }
        //EPA REACTIVITY
        public String EPA_REACTIVITY {
            get;
            set;
        }
        //EVAPORATION RATE
        public String EVAP_RATE_REF {
            get;
            set;
        }
        //FLASH POint TEMP (C)
        public String FLASH_PT_TEMP {
            get;
            set;
        }
        //NEUTRALIZING AGENT TEXT
        public String NEUT_AGENT {
            get;
            set;
        }
        //NFPA FLAMMABILITY
        public String NFPA_FLAMMABILITY {
            get;
            set;
        }
        //NFPA HEALTH
        public String NFPA_HEALTH {
            get;
            set;
        }
        //NFPA REACTIVITY
        public String NFPA_REACTIVITY {
            get;
            set;
        }
        //NFPA SPECIAL
        public String NFPA_SPECIAL {
            get;
            set;
        }
        //OSHA CARCINOGENS
        public String OSHA_CARCINOGENS {
            get;
            set;
        }
        //OSHA COMBUSTION LIQUID
        public String OSHA_COMB_LIQUID {
            get;
            set;
        }
        //OSHA COMPRESSED GAS
        public String OSHA_COMP_GAS {
            get;
            set;
        }
        //OSHA CORROSIVE
        public String OSHA_CORROSIVE {
            get;
            set;
        }
        //OSHA EXPLOSIVE
        public String OSHA_EXPLOSIVE {
            get;
            set;
        }
        //OSHA FLAMMABLE
        public String OSHA_FLAMMABLE {
            get;
            set;
        }
        //OSHA HIGHLY TOXIC
        public String OSHA_HIGH_TOXIC {
            get;
            set;
        }
        //OSHA IRRITANT
        public String OSHA_IRRITANT {
            get;
            set;
        }
        //OSHA ORGANIC PEROXIDE
        public String OSHA_ORG_PEROX {
            get;
            set;
        }
        //OSHA OTHER/LONG TERM
        public String OSHA_OTHERLONGTERM {
            get;
            set;
        }
        //OSHA OXIDIZER
        public String OSHA_OXIDIZER {
            get;
            set;
        }
        //OSHA PYROPHORIC
        public String OSHA_PYRO {
            get;
            set;
        }
        //OSHA SENSITIZER
        public String OSHA_SENSITIZER {
            get;
            set;
        }
        //OSHA TOXIC
        public String OSHA_TOXIC {
            get;
            set;
        }
        //OSHA UNSTABLE REACTIVE
        public String OSHA_UNST_REACT {
            get;
            set;
        }

        //OTHER/SHORT TERM
        public String OTHER_SHORT_TERM {
            get;
            set;
        }
        //PH
        public String PH {
            get;
            set;
        }
        //PHYSICAL STATE CODE
        public String PHYS_STATE_CODE {
            get;
            set;
        }
        //SOLUBILITY IN WATER
        public String SOL_IN_WATER {
            get;
            set;
        }
        //SPECIFIC GRAVITY
        public String SPECIFIC_GRAV {
            get;
            set;
        }
        //VAPOR DENSITY
        public String VAPOR_DENS {
            get;
            set;
        }
        //VAPOR PRESSURE
        public String VAPOR_PRESS {
            get;
            set;
        }
        //VISCOSITY
        public String VISCOSITY {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (GM/L)
        public String VOC_GRAMS_LITER {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (LB/G)
        public String VOC_POUNDS_GALLON {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (WT%)
        public String VOL_ORG_COMP_WT {
            get;
            set;
        }
    }

    //MSDS RADIOLOGICAL INFO
    public class MSDSRadiologicalInfo {
        public MSDSRadiologicalInfo() {
        }

        public MSDSRadiologicalInfo(Models.msds_radiological_info MsdsRad) {
            this.NRC_LP_NUM = MsdsRad.NRC_LP_NUM;
            this.OPERATOR = MsdsRad.OPERATOR;
            this.RAD_AMOUNT_MICRO = MsdsRad.RAD_AMOUNT_MICRO;
            this.RAD_CAS = MsdsRad.RAD_CAS;
            this.RAD_FORM = MsdsRad.RAD_FORM;
            this.RAD_NAME = MsdsRad.RAD_NAME;
            this.RAD_SYMBOL = MsdsRad.RAD_SYMBOL;
            this.REP_NSN = MsdsRad.REP_NSN;
            this.SEALED = MsdsRad.SEALED.ToString();
        }
        
        //NRC LICENSE/PERMIT NUMBER
        public String NRC_LP_NUM {
            get;
            set;
        }
        //OPERATOR
        public String OPERATOR {
            get;
            set;
        }
        //RADIOACTIVE AMOUNT (MICROCURIES)
        public String RAD_AMOUNT_MICRO {
            get;
            set;
        }
        //RADIOACTIVE FORM
        public String RAD_FORM {
            get;
            set;
        }
        //RADIOISOTOPE CAS
        public String RAD_CAS {
            get;
            set;
        }
        //RADIOISOTOPE NAME
        public String RAD_NAME {
            get;
            set;
        }
        //RADIOISOTOPE SYMBOL
        public String RAD_SYMBOL {
            get;
            set;
        }
        //REPLACEMENT NSN
        public String REP_NSN {
            get;
            set;
        }
        //SEALED
        public String SEALED {
            get;
            set;
        }
    }

    //MSDS TRANSPORTATION
    public class MSDSTransportation {
        public MSDSTransportation() {
        }

        public MSDSTransportation(Models.msds_transportation MSdsTran) {
            this.afjm_psn_id = MSdsTran.afjm_psn_id;
            this.dot_psn_id = MSdsTran.dot_psn_id;
            this.iata_psn_id = MSdsTran.iata_psn_id;
            this.imo_psn_id = MSdsTran.imo_psn_id;
            this.AF_MMAC_CODE = MSdsTran.AF_MMAC_CODE;
            this.CERTIFICATE_COE = MSdsTran.CERTIFICATE_COE;
            this.COMPETENT_CAA = MSdsTran.COMPETENT_CAA;
            this.DOD_ID_CODE = MSdsTran.DOD_ID_CODE;
            this.DOT_EXEMPTION_NO = MSdsTran.DOT_EXEMPTION_NO;
            this.DOT_RQ_IND = MSdsTran.DOT_RQ_IND.ToString();
            this.EX_NO = MSdsTran.EX_NO;
            this.HIGH_EXPLOSIVE_WT = Convert.ToInt32(MSdsTran.HIGH_EXPLOSIVE_WT);
            this.LTD_QTY_IND = MSdsTran.LTD_QTY_IND.ToString();
            this.MAGNETIC_IND = MSdsTran.MAGNETIC_IND.ToString();
            this.MAGNETISM = MSdsTran.MAGNETISM;
            this.MARINE_POLLUTANT_IND = MSdsTran.MARINE_POLLUTANT_IND.ToString();
            this.NET_EXP_QTY_DIST = Convert.ToInt32(MSdsTran.NET_EXP_QTY_DIST);
            this.NET_EXP_WEIGHT = MSdsTran.NET_EXP_WEIGHT;
            this.NET_PROPELLANT_WT = MSdsTran.NET_PROPELLANT_WT;
            this.NOS_TECHNICAL_SHIPPING_NAME = MSdsTran.NOS_TECHNICAL_SHIPPING_NAME;
            this.TRANSPORTATION_ADDITIONAL_DATA = MSdsTran.TRANSPORTATION_ADDITIONAL_DATA;
        }

        //AFJM PSN ID
        public long? afjm_psn_id {
            get;
            set;
        }
        //DOT PSN ID
        public long? dot_psn_id {
            get;
            set;
        }
        //IATA PSN ID
        public long? iata_psn_id {
            get;
            set;
        }
        //IMO PSN ID
        public long? imo_psn_id {
            get;
            set;
        }
        //AF MMAC CODE
        public String AF_MMAC_CODE {
            get;
            set;
        }
        //CERTIFICATE OF EQUIVALENCY
        public String CERTIFICATE_COE {
            get;
            set;
        }
        //COMPETENT AUTHORITY APPROVAL
        public String COMPETENT_CAA {
            get;
            set;
        }
        //DOD ID CODE
        public String DOD_ID_CODE {
            get;
            set;
        }
        //DOT EXEMPTION NUMBER
        public String DOT_EXEMPTION_NO {
            get;
            set;
        }
        //DOT RQ INDICATOR
        public String DOT_RQ_IND {
            get;
            set;
        }
        //EX NUMBER
        public String EX_NO {
            get;
            set;
        }
        //FLASH PT TEMP (C)
        public int FLASH_PT_TEMP {
            get;
            set;
        }
        //HCC
        public String HCC {
            get;
            set;
        }
        //HIGH EXPLOSIVE WEIGHT
        public int? HIGH_EXPLOSIVE_WT {
            get;
            set;
        }
        //LTD QTY INDICATOR
        public String LTD_QTY_IND {
            get;
            set;
        }
        //MAGNETIC INDICATOR
        public String MAGNETIC_IND {
            get;
            set;
        }
        //MAGNETISM (MAGNETIC STRENGTH)
        public String MAGNETISM {
            get;
            set;
        }
        //MARINE POLLUTANT INDICATOR
        public String MARINE_POLLUTANT_IND {
            get;
            set;
        }
        //NET EXPLOSIVE QTY. DISTANCE WEIGHT
        public int NET_EXP_QTY_DIST {
            get;
            set;
        }
        //NET EXPLOSIVE WEIGHT (KG)
        public String NET_EXP_WEIGHT {
            get;
            set;
        }
        //NET PROPELLANT WEIGHT (KG)
        public String NET_PROPELLANT_WT {
            get;
            set;
        }
        //NOS TECHNICAL SHIPPING NAME
        public String NOS_TECHNICAL_SHIPPING_NAME {
            get;
            set;
        }
        //TRANSPORTATION ADDITIONAL DATA
        public String TRANSPORTATION_ADDITIONAL_DATA {
            get;
            set;
        }
        public string AFJM_PSN_CODE {
            get;
            set;
        }
        public string DOT_PSN_CODE {
            get;
            set;
        }
        public string IATA_PSN_CODE {
            get;
            set;
        }
        public string IMO_PSN_CODE {
            get;
            set;
        }
    }
}