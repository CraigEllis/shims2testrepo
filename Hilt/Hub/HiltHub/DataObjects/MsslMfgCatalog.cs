﻿
namespace HiltHub.DataObjects {
    public class MsslMfgCatalog {
        public string Cage { get; set; }

        public string Manufacturer { get; set; }

        public int Hazard_id { get; set; }
    }
}