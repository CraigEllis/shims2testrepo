﻿using System;

namespace HiltHub.DataObjects {
    public class Ship {
        #region Constructors
        // Default constructor
        public Ship() {
        }

        // Constructor using view models as input
        public Ship(Models.ship ship) {
            ship_id = ship.ship_id;
            if (ship.current_status != null) 
                current_status = (long) ship.current_status;
            name = ship.name;
            hull_type_id = ship.hull_type_id;
            uic = ship.uic;
            hull_number = ship.hull_number;
            if (ship.download_aul_date != null)
                download_aul_date = (DateTime) ship.download_aul_date;
            if (ship.download_msds_date != null)
                download_msds_date = (DateTime)ship.download_msds_date;
            if (ship.download_mssl_date != null)
                download_mssl_date = (DateTime)ship.download_mssl_date;
            if (ship.upload_aul_date != null)
                upload_aul_date = (DateTime)ship.upload_aul_date;
            if (ship.upload_msds_date != null)
                upload_msds_date = (DateTime)ship.upload_msds_date;
            if (ship.upload_mssl_date != null)
                upload_mssl_date = (DateTime)ship.upload_mssl_date;

            if (ship.created != null)
                created = (DateTime) ship.created;
            created_by = ship.created_by;
            if (ship.changed != null)
                changed = (DateTime)ship.changed;
            changed_by = ship.changed_by;
        }
        #endregion

        #region Properties
        public long	ship_id {
	        get;
	        set;
        }
        public long	current_status {
	        get;
	        set;
        }
        public String name {
            get;
            set;
        }
        public String ship_status {
            get;
            set;
        }
        public String hull_type {
            get;
            set;
        }
        public String uic {
	        get;
	        set;
        }
        public long hull_type_id {
            get;
            set;
        }
        public long parent_type_id {
            get;
            set;
        }
        public String hull_active {
            get;
            set;
        }
        public String hull_description {
            get;
            set;
        }
        public String parent_hull_type {
            get;
            set;
        }
        public String hull_number {
	        get;
	        set;
        }
        public DateTime	download_aul_date {
	        get;
	        set;
        }
        public DateTime	download_msds_date {
	        get;
	        set;
        }
        public DateTime	download_mssl_date {
	        get;
	        set;
        }
        public DateTime	upload_aul_date {
	        get;
	        set;
        }
        public DateTime	upload_msds_date {
	        get;
	        set;
        }
        public DateTime	upload_mssl_date {
	        get;
	        set;
        }
        public DateTime	created {
	        get;
	        set;
        }
        public String	created_by {
	        get;
	        set;
        }
        public DateTime	changed {
	        get;
	        set;
        }
        public String changed_by {
            get;
            set;
        }
        #endregion
    }
}