﻿
namespace HiltHub.DataObjects {
    public class MSSLStats {
        public int NewRecords { get; set; }

        public int ChangedRecords { get; set; }

        public int DeletedRecords { get; set; }

        public MSSLStats()
        {
            DeletedRecords = 0;
            ChangedRecords = 0;
        }

        public int TotalRecords { get; set; }
    }
}