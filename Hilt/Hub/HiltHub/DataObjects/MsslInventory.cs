using System;
using System.Collections.Generic;

namespace HiltHub.DataObjects {
    public class MsslInventory {
        private DateTime shelf_life_expiration_date;

        private List<MsslSubs> subs = new List<MsslSubs>();
        private int atc;
        public string Niin { get; set; }

        public string Cosal { get; set; }

        public string Cog { get; set; }

        public string Mcc { get; set; }

		public string Ui { get; set; }

        public double Up { get; set; }

        public string Location { get; set; }

        public string Lmc { get; set; }

        public string Irc { get; set; }

        public int Dues { get; set; }

        public int Ro { get; set; }

        public int Rp { get; set; }

        public double Amd { get; set; }

        public string Smic { get; set; }

        public string Slc { get; set; }

        public string Slac { get; set; }

        public string Smcc { get; set; }

        public string Nomenclature { get; set; }

        public double Netup { get; set; }

        public string Arrc { get; set; }

        public string LimitFlag { get; set; }

        public string NoDropInd { get; set; }

        public int Atc { get; set; }

        public List<MsslSubs> Subs { get; set; }

        public DateTime Shelf_life_expiration_date { get; set; }

        public int Location_id { get; set; }

        public int Qty { get; set; }

        public string Serial_number { get; set; }

        public bool Bulk_item { get; set; }

        public MsslMfgCatalog Mfg_catalog { get; set; }

        public DateTime Manufacturer_date { get; set; }

        public string Ati { get; set; }

        public DateTime	created { get; set; }
        public String	created_by { get; set; }
        public DateTime	changed { get; set; }
        public String   changed_by { get; set; }
    }
}
