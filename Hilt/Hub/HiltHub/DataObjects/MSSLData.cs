﻿using System.Collections.Generic;

namespace HiltHub.DataObjects {
    public class MSSLData {
        public List<MsslInventory> InventoryList { get; set; }

        public List<MsslSubs> SubList { get; set; }

        public string ErrorMsg { get; set; }

        private MSSLStats msslStats;

        public MSSLStats MsslStats {
            get { return msslStats ?? (msslStats = new MSSLStats()); }
            set {
                msslStats = value;
            }
        }


    }
}