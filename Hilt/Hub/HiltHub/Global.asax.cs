﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Web.UI;
using System.Reflection;
using System.Data.Linq.Mapping;
using System.Web.UI.WebControls.Expressions;

namespace HiltHub
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication {
        public static string version = "";
        public const string TBDMessage = "Feature not yet available.";
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute
            {
                ExceptionType = typeof(Exception),
                View = "Error",
                Order = 2
            }); 

            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Contractors List", // Route name
                "contractorInfo/{id}", // URL with parameters
                new
                {
                    controller = "contractorInfo",
                    action = "Index"
                } // Parameter defaults
            );


            routes.MapRoute(
                "Ingredients List", // Route name
                "Ingredient/{id}", // URL with parameters
                new
                {
                    controller = "Ingredient",
                    action = "Index"
                } // Parameter defaults
            );

            routes.MapRoute(
                "Create Ship", // Route name
                "Ship/Create", // URL with parameters
                new
                {
                    controller = "Ship",
                    action = "Create"
                } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                } // Parameter defaults
            );


        }

        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            version = System.Diagnostics.FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location)
                .ProductVersion;

            // Check for database updates
            DatabaseManager dbManager = new DatabaseManager();
            dbManager.checkForUpdates();
        }

        protected void Application_Error(object sender, EventArgs e) {
            // Code that runs when an unhandled <b style="color: black; background-color: rgb(153, 255, 153);">error</b> occurs    
            Exception ex = Server.GetLastError().GetBaseException();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<br/><h1><b>HILT Hub encountered an unexpected problem</b></h1><br/><h1>What to do: </h1> <br><b>Close your browser and try the attempted action again. If the problem persists please contact your system administrator. </b><br/><br/><hr/><br/>");
            sb.AppendLine("<h1>More info: </h1><br><h3><b>Message</b>:</h3><br/> <b>" + ex.Message + "</b><br/><br/>");
            sb.AppendLine("<h3><b>Exception</b>:</h3><br/> <b>" + ex.GetType() + "</b><br/><br/>");
            if (ex.TargetSite != null)
                sb.AppendLine("<h3><b>Targetsite</b>:</h3><br/> " + ex.TargetSite + "<br/><br/>");
            sb.AppendLine("<h3><b>Source</b>:</h3><br/>  " + ex.Source + "<br/><br/>");
            sb.AppendLine("<h3><b>StackTrace</b></h3><br/> " + ex.StackTrace.
                                          Replace(Environment.NewLine, "<br/>") + "<br/><br/>");
            sb.AppendLine("<h3><b>Data count</b>:</h3><br/> " + ex.Data.Count.ToString(CultureInfo.InvariantCulture) + " ");
            if (ex.Data.Count > 0) {
                HtmlTable tbl = new HtmlTable {Border = 1};
                HtmlTableRow htr = new HtmlTableRow();
                HtmlTableCell htc1 = new HtmlTableCell();
                HtmlTableCell htc2 = new HtmlTableCell();
                HtmlTableCell htc3 = new HtmlTableCell();
                HtmlTableCell htc4 = new HtmlTableCell();
                htc1.InnerHtml = "<b>Key</b>";
                htc2.InnerHtml = "<b>Value</b>";
                htc3.InnerHtml = "Key Type";
                htc4.InnerHtml = "Value Type";

                htr.Cells.Add(htc1);
                htr.Cells.Add(htc2);
                htr.Cells.Add(htc3);
                htr.Cells.Add(htc4);
                tbl.Rows.Add(htr);

                foreach (DictionaryEntry de in ex.Data) {
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell tc1 = new HtmlTableCell();
                    HtmlTableCell tc2 = new HtmlTableCell();
                    HtmlTableCell tc3 = new HtmlTableCell();
                    HtmlTableCell tc4 = new HtmlTableCell();
                    tc1.InnerHtml = "<b>" + de.Key + "</b>";
                    tc2.InnerHtml = "<b>" + de.Value + "</b>";
                    tc3.InnerHtml = de.Key.GetType().Name;
                    tc4.InnerHtml = de.Value.GetType().Name;
                    tc3.Align = "center";
                    tc4.Align = "center";

                    tr.Cells.Add(tc1);
                    tr.Cells.Add(tc2);
                    tr.Cells.Add(tc3);
                    tr.Cells.Add(tc4);
                    tbl.Rows.Add(tr);
                }
                StringBuilder tblSb = new StringBuilder();
                System.IO.StringWriter sw = new System.IO.StringWriter(tblSb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                tbl.RenderControl(htw);
                sb.AppendLine(tblSb.ToString());
            }
            sb.AppendLine("<br/><br/>");
            sb.AppendLine("<h3><b>Exception</b>:</h3><br/> " + ex.ToString().
                                          Replace(Environment.NewLine, "<br/>") + "");
            Session["Exception"] = sb.ToString();
            Server.ClearError();

            Session["ExPath"] = Request.Path;
            if (ex.InnerException != null) {
                Session["ExTitle"] = ex.InnerException.Message;
            }
            else
                Session["ExTitle"] = null;
            Session["ExMess"] = ex.ToString();
            Session["ExTrace"] = ex.StackTrace;
        }
    }
}

namespace ExtensionMethods
{
    public static class MyExtensions
    {
        // Add a custom method to the String class to truncate strings
        public static string Truncate(this String str, int i)
        {
            if (str == null) return "";
            if (str.Length <= i) return str;
            
            return "<span title=\"" + HttpUtility.HtmlEncode(str) + "\">" +
                        HttpUtility.HtmlEncode(str.Substring(0, i).TrimEnd()) + "\u2026" + // << That's an Ellipsis character '...'
                   "</span>"; 
        }

        public static string TitleCase(this string str)
        {
            TextInfo textInfo = new CultureInfo("en-US",false).TextInfo;

            return textInfo.ToTitleCase(str.ToLower()).Replace("&A", "&a").Replace(" And ", " and ").Replace("&Q", "&q");
        }

        public static MvcHtmlString LimitedEditorFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression)
        {
            string S = htmlHelper.EditorFor(expression).ToString().Replace("/>", "MAXLENGTH=\"" + GetLengthLimit(expression) + "\"/>");

            return new MvcHtmlString(S);
        }

        public static MvcHtmlString YesNoRadioFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression)
        {
            Func<TModel, TValue> F = expression.Compile();
            string R = "N";
            var FRes = F(htmlHelper.ViewData.Model);
            if (FRes != null) 
                R = FRes.ToString();
            bool isYes = R.StartsWith("Y");

            return new MvcHtmlString(htmlHelper.RadioButton(ExpressionHelper.GetExpressionText(expression), "Y", isYes).ToString() + " Yes " + 
                    htmlHelper.RadioButton(ExpressionHelper.GetExpressionText(expression), "N", !isYes).ToString() + " No") ;
        }


        private static object GetValue(ConstantExpression expression)
        {
            Expression conversion = Expression.Convert(expression, typeof(object));
            var getterLambda = Expression.Lambda<Func<object>>(conversion);
     
            var getter = getterLambda.Compile();
     
            return getter();
        }


        private static int GetLengthLimit(Expression expression)
        {
            MemberExpression ME = GetMemberInfo(expression);
            Type type = ME.Member.ReflectedType;
            string field = ME.Member.Name;
            PropertyInfo prop = type.GetProperty(field);
            // Find the Linq 'Column' attribute
            // e.g. [Column(Storage="_FileName", DbType="NChar(256) NOT NULL", CanBeNull=false)]
            object[] info = prop.GetCustomAttributes(typeof(ColumnAttribute), true);
            if (info.Length != 1) return 0;   // Assume there is just one

            ColumnAttribute ca = (ColumnAttribute)info[0];
            string dbtype = ca.DbType;
            if (!dbtype.StartsWith("NChar") && !dbtype.StartsWith("NVarChar")) return 0;

            Regex pattern = new Regex("\\((.+)\\)");
            Match m = pattern.Match(dbtype);
            return !m.Success ? 0 : Convert.ToInt16(m.Groups[1].Value);
        }

        private static MemberExpression GetMemberInfo(Expression method)
        {
            LambdaExpression lambda = method as LambdaExpression; 
            if (lambda == null) throw new ArgumentNullException("method"); 
            MemberExpression memberExpr = null; 
            switch (lambda.Body.NodeType)
            {
                case ExpressionType.Convert:
                    memberExpr = ((UnaryExpression)lambda.Body).Operand as MemberExpression;
                    break;
                case ExpressionType.MemberAccess:
                    memberExpr = lambda.Body as MemberExpression;
                    break;
            } 
            
            if (memberExpr == null) throw new ArgumentException("method"); 
            return memberExpr;
        }
    }
}


