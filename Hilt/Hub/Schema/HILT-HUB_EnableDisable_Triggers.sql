-- Script to enable/disable triggers on HILT-HUB database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved

USE HILT_HUB;
GO

-- Disable Triggers
-- AUL
DISABLE TRIGGER tr_delete_auth_use_list ON auth_use_list;
DISABLE TRIGGER tr_insert_auth_use_list ON auth_use_list;
DISABLE TRIGGER tr_update_auth_use_list ON auth_use_list;
DISABLE TRIGGER tr_delete_aul_inventory ON aul_inventory;
DISABLE TRIGGER tr_insert_aul_inventory ON aul_inventory;
DISABLE TRIGGER tr_update_aul_inventory ON aul_inventory;
-- MSDS
DISABLE TRIGGER tr_delete_msds_master ON msds_master;
DISABLE TRIGGER tr_insert_msds_master ON msds_master;
DISABLE TRIGGER tr_update_msds_master ON msds_master;
DISABLE TRIGGER tr_delete_msds_inventory ON msds_inventory;
DISABLE TRIGGER tr_insert_msds_inventory ON msds_inventory;
DISABLE TRIGGER tr_update_msds_inventory ON msds_inventory;
-- MSSL
DISABLE TRIGGER tr_delete_mssl_master ON mssl_master;
DISABLE TRIGGER tr_insert_mssl_master ON mssl_master;
DISABLE TRIGGER tr_update_mssl_master ON mssl_master;
DISABLE TRIGGER tr_delete_mssl_inventory ON mssl_inventory;
DISABLE TRIGGER tr_insert_mssl_inventory ON mssl_inventory;
DISABLE TRIGGER tr_update_mssl_inventory ON mssl_inventory;

-- Enable Triggers
-- AUL
ENABLE TRIGGER tr_delete_auth_use_list ON auth_use_list;
ENABLE TRIGGER tr_insert_auth_use_list ON auth_use_list;
ENABLE TRIGGER tr_update_auth_use_list ON auth_use_list;
ENABLE TRIGGER tr_delete_aul_inventory ON aul_inventory;
ENABLE TRIGGER tr_insert_aul_inventory ON aul_inventory;
ENABLE TRIGGER tr_update_aul_inventory ON aul_inventory;
-- MSDS
ENABLE TRIGGER tr_delete_msds_master ON msds_master;
ENABLE TRIGGER tr_insert_msds_master ON msds_master;
ENABLE TRIGGER tr_update_msds_master ON msds_master;
ENABLE TRIGGER tr_delete_msds_inventory ON msds_inventory;
ENABLE TRIGGER tr_insert_msds_inventory ON msds_inventory;
ENABLE TRIGGER tr_update_msds_inventory ON msds_inventory;
-- MSSL
ENABLE TRIGGER tr_delete_mssl_master ON mssl_master;
ENABLE TRIGGER tr_insert_mssl_master ON mssl_master;
ENABLE TRIGGER tr_update_mssl_master ON mssl_master;
ENABLE TRIGGER tr_delete_mssl_inventory ON mssl_inventory;
ENABLE TRIGGER tr_insert_mssl_inventory ON mssl_inventory;
ENABLE TRIGGER tr_update_mssl_inventory ON mssl_inventory;
