USE HILT_HUB;
GO 

/****** Object:  Table [dbo].[storage_type]    Script Date: 09/01/2011 13:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[storage_type](
	[storage_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](5) NULL,
	[description] [text] NULL,
 CONSTRAINT [PK_storage_type] PRIMARY KEY CLUSTERED 
(
	[storage_type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

-- Storage_Type
set identity_insert storage_type on;
insert into storage_type (storage_type_id,type,description) 
	select storage_type_id,type,description from hazmat.dbo.storage_type;
set identity_insert storage_type off;

GO
