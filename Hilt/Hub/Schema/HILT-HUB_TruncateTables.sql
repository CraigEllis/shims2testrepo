-- Script to delete table data in the HILT-HUB database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved

USE hilt_hub;
GO

-- Audit tables
-- AUL
TRUNCATE TABLE AUDIT_auth_use_list;
TRUNCATE TABLE AUDIT_aul_inventory;
-- MSDS
TRUNCATE TABLE AUDIT_msds_master;
TRUNCATE TABLE AUDIT_msds_inventory;
-- MSSL
TRUNCATE TABLE AUDIT_mssl_master;
TRUNCATE TABLE AUDIT_mssl_inventory;

-- Inventory Tables
-- AUL
TRUNCATE TABLE aul_inventory;
-- MSDS
TRUNCATE TABLE msds_inventory;
-- MSSL
TRUNCATE TABLE mssl_inventory;

-- Master Tables
-- AUL
TRUNCATE TABLE auth_use_list;
-- MSSL
TRUNCATE TABLE mssl_master;

-- MSDS - because of FK you need to delete the child tables first before deleting msds_master
TRUNCATE TABLE msds_afjm_psn;
TRUNCATE TABLE msds_contractor_info;
TRUNCATE TABLE msds_disposal;
TRUNCATE TABLE msds_document_types;
TRUNCATE TABLE msds_dot_psn;
TRUNCATE TABLE msds_iata_psn;
TRUNCATE TABLE msds_imo_psn;
TRUNCATE TABLE msds_ingredients;
TRUNCATE TABLE msds_item_description;
TRUNCATE TABLE msds_label_info;
TRUNCATE TABLE msds_phys_chemical;
TRUNCATE TABLE msds_radiological_info;
TRUNCATE TABLE msds_transportation;
TRUNCATE TABLE msds_master;

-- Support Tables - only delete after master & inventory records have been deleted
TRUNCATE TABLE data_sources;
TRUNCATE TABLE hazards;
TRUNCATE TABLE hcc;
TRUNCATE TABLE mfg_catalog;
TRUNCATE TABLE ships;
TRUNCATE TABLE user_roles;
