USE [hilt_hub]
GO

/****** Object:  Index [uc_aulinventoryniin]    Script Date: 09/09/2011 07:59:45 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aul_inventory]') AND name = N'uc_aulinventoryniin')
ALTER TABLE [dbo].[aul_inventory] DROP CONSTRAINT [uc_aulinventoryniin]
GO
