-- Create the triggers for HILT-HUB database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved

USE [hilt_hub]
GO

-- MSSL
/****** Object:  Trigger [dbo].[tr_delete_mssl_master]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_mssl_master]
on [dbo].[mssl_master] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_master (action, action_date, mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, created) 
select 'DELETE', GETDATE(), mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, created 
	from deleted;

end

GO

/****** Object:  Trigger [dbo].[tr_insert_mssl_master]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_insert_mssl_master]
on [dbo].[mssl_master] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_master (action, action_date, mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, created) 
select 'INSERT', GETDATE(), mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_update_mssl_master]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_update_mssl_master]
on [dbo].[mssl_master] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_master (action, action_date, mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, created) 
select 'UPDATE', GETDATE(), mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, created 
	from inserted;

end

GO


/****** Object:  Trigger [dbo].[tr_delete_mssl_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_mssl_inventory]
on [dbo].[mssl_inventory] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_inventory (action, action_date, mssl_inventory_id, uic, niin, location, qty, created) 
select 'DELETE', GETDATE(), mssl_inventory_id, uic, niin, location, qty, created 
	from deleted;

end

GO

/****** Object:  Trigger [dbo].[tr_insert_mssl_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_insert_mssl_inventory]
on [dbo].[mssl_inventory] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_inventory (action, action_date, mssl_inventory_id, uic, niin, location, qty, created) 
select 'INSERT', GETDATE(), mssl_inventory_id, uic, niin, location, qty, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_update_mssl_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_update_mssl_inventory]
on [dbo].[mssl_inventory] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_inventory (action, action_date, mssl_inventory_id, uic, niin, location, qty, created) 
select 'UPDATE', GETDATE(), mssl_inventory_id, uic, niin, location, qty, created 
	from inserted;

end

GO

-- AUL
/****** Object:  Trigger [dbo].[tr_delete_auth_use_list]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_auth_use_list]
on [dbo].[auth_use_list] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_auth_use_list (action, action_date, aul_id, hull_type, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, created) 
select 'DELETE', GETDATE() , aul_id, hull_type, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, created 
	from deleted;

end

GO

/****** Object:  Trigger [dbo].[tr_insert_auth_use_list]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_insert_auth_use_list]
on [dbo].[auth_use_list] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_auth_use_list (action, action_date, aul_id, hull_type, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, created) 
select 'INSERT', GETDATE() , aul_id, hull_type, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_update_auth_use_list]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_update_auth_use_list]
on [dbo].[auth_use_list] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_auth_use_list (action, action_date, aul_id, hull_type, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, created) 
select 'UPDATE', GETDATE() , aul_id, hull_type, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, created 
	from inserted;

end


GO

/****** Object:  Trigger [dbo].[tr_delete_aul_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_aul_inventory]
on [dbo].[aul_inventory] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created) 
select 'DELETE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created 
	from deleted;

end

GO

/****** Object:  Trigger [dbo].[tr_insert_aul_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_insert_aul_inventory]
on [dbo].[aul_inventory] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created) 
select 'INSERT', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_update_aul_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_update_aul_inventory]
on [dbo].[aul_inventory] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created) 
select 'UPDATE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created 
	from inserted;

end


GO

-- MSDS
/****** Object:  Trigger [dbo].[tr_delete_msds_master]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_msds_master]
on [dbo].[msds_master] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_master (action, action_date, msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, created) 
select 'DELETE', GETDATE(), msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, created 
	from deleted;

end

GO

/****** Object:  Trigger [dbo].[tr_insert_msds_master]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_insert_msds_master]
on [dbo].[msds_master] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_master (action, action_date, msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, created) 
select 'INSERT', GETDATE(), msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_update_msds_master]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_update_msds_master]
on [dbo].[msds_master] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_master (action, action_date, msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, created) 
select 'UPDATE', GETDATE(), msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_delete_msds_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_msds_inventory]
on [dbo].[msds_inventory] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_inventory (action, action_date, msds_inventory_id, MSDSSERNO, CAGE, uic, NIIN, 
	manually_entered, created) 
select 'DELETE', GETDATE(), msds_inventory_id, MSDSSERNO, CAGE, uic, NIIN, manually_entered, created 
	from deleted;

end

GO

/****** Object:  Trigger [dbo].[tr_insert_msds_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_insert_msds_inventory]
on [dbo].[msds_inventory] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_inventory (action, action_date, msds_inventory_id, MSDSSERNO, CAGE, uic, NIIN, 
	manually_entered, created) 
select 'INSERT', GETDATE(), msds_inventory_id, MSDSSERNO, CAGE, uic, NIIN, manually_entered, created 
	from inserted;

end

GO

/****** Object:  Trigger [dbo].[tr_update_msds_inventory]    Script Date: 08/18/2011 13:24:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_update_msds_inventory]
on [dbo].[msds_inventory] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_inventory (action, action_date, msds_inventory_id, MSDSSERNO, CAGE, uic, NIIN, 
	manually_entered, created) 
select 'UPDATE', GETDATE(), msds_inventory_id, MSDSSERNO, CAGE, uic, NIIN, manually_entered, created 
	from inserted;

end

GO

