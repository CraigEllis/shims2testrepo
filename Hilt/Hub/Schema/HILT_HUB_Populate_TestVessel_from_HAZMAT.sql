-- Scripts to populate HILT_HUB database with a test vessel using data from the Hazmat database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved
USE HILT_HUB;

-- Ships
insert into ships (current_status,name,uic,hull_type,hull_number,
	POC_Name,POC_Telephone,POC_Email) 
	VALUES(3,'Test Vessel','TEST','SSN','Unk','','','');

insert into data_sources (data_source_cd,name) VALUES('TEST', 'Test Vessel');

-- AUL Inventory
insert into aul_inventory (uic,hull_type,
	fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,
	shelf_life_code_id,shelf_life_action_code_id,remarks,storage_type_id,
	cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,
	qty,allowance_qty,manually_entered,dropped_in_error,manufacturer,
	cage,msds_num,data_source_cd,created) 
	select 'TEST','SSN',
		fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,
		shelf_life_code_id,shelf_life_action_code_id,remarks,storage_type_id,
		cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,
		1,allowance_qty,manually_entered,dropped_in_error,manufacturer,
		cage,msds_num,'TEST',created from hazmat.dbo.niin_catalog;
-- update with quantities
begin tran;
Update aul_inventory
	SET aul_inventory.allowance_qty = (SELECT allowance_qty
	FROM auth_use_list WHERE niin = aul_inventory.niin)
	WHERE uic = 'TEST'; 
commit tran;

-- MSSL Inventory
insert into mssl_inventory (uic,niin,
	ati,cog,mcc,ui,up,netup,location,qty,lmc,irc,dues,
	ro,rp,amd,smic,slc,slac,smcc,nomenclature,
	data_source_cd,created) 
	select 'TEST',niin,
	ati,cog,mcc,ui,up,netup,location,1,lmc,irc,dues,
	ro,rp,amd,smic,slc,slac,smcc,nomenclature,
	'TEST',date_inserted from hazmat.dbo.mssl_inventory;

--MSDS Inventory
insert into msds_inventory (MSDSSERNO,CAGE,uic,
	MANUFACTURER,PARTNO,FSC,NIIN,hcc_id,file_name,manually_entered,
	ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,END_COMP_IND,END_ITEM_IND,
	KIT_IND,KIT_PART_IND,MANUFACTURER_MSDS_NO,MIXTURE_IND,
	PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,
	PROPRIETARY_IND,PUBLISHED_IND,PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,
	SERVICE_AGENCY,TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,
	data_source_cd,created) 
	select MSDSSERNO,CAGE,'TEST',
	MANUFACTURER,PARTNO,FSC,NIIN,hcc_id,file_name,manually_entered,
	ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,END_COMP_IND,END_ITEM_IND,
	KIT_IND,KIT_PART_IND,MANUFACTURER_MSDS_NO,MIXTURE_IND,
	PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,
	PROPRIETARY_IND,PUBLISHED_IND,PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,
	SERVICE_AGENCY,TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,
	'TEST', GETDATE() from hazmat.dbo.msds
