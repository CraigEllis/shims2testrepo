-- Create the HILT-HUB views
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved

USE hilt_hub;
GO

-- AUL Inventory Detail record
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'v_AUL_Inventory_Detail')
    DROP VIEW v_AUL_Inventory_Detail
GO
CREATE VIEW v_AUL_Inventory_Detail AS
	SELECT 
		inv.uic AS UIC,
		inv.niin AS NIIN,
		inv.qty AS QTY,
		inv.allowance_qty AS Allowance_QTY,
		COALESCE(inv.manually_entered, '0') AS Manually_Entered,
		COALESCE(inv.cage, '') AS CAGE,
		COALESCE(inv.msds_num, '') AS MSDS_Num,
		inv.created AS Created,
		COALESCE(mstr.ui, '') AS UI,
		COALESCE(mstr.um, '') AS UM,
		sh.ship_id,
		sh.hull_type AS Hull_Type,
		sh.name AS Ship_Name,
		sh.hull_number AS Ship_Number,
		mstr.usage_category_id,
		COALESCE(uc.category, '') AS Usage_Category,
		COALESCE(uc.description, '') AS Usage_Category_Description,
		mstr.description AS Description,
		mstr.smcc_id,
		COALESCE(smcc.smcc, '') AS SMCC,
		COALESCE(smcc.description, '') AS SMCC_Description,
		mstr.specs AS Specs,
		mstr.shelf_life_code_id,
		COALESCE(slc.slc, '') AS Shelf_Life_Code,
		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,
		COALESCE(slc.description, '') AS Shelf_Life_Description,
		COALESCE(slc.type, '') AS Shelf_Life_Type,
		mstr.shelf_life_action_code_id,
		COALESCE(slac.slac, '') AS Shelf_Life_Action_Code,
		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,
		CONVERT(NVARCHAR(2000), mstr.remarks) AS Remarks,
		mstr.storage_type_id,
		COALESCE(stor.type, '') AS Storage_Type,
		COALESCE(stor.description, '') AS Storage_Type_Description,
		mstr.cog_id,
		COALESCE(cog.cog, '') AS COG,
		COALESCE(cog.description, '') AS COG_Description,
		mstr.spmig AS SPMIG,
		mstr.nehc_rpt AS NEHC_Report,
		mstr.catalog_group_id,
		mstr.catalog_serial_number AS Catalog_Serial_Number,
		mstr.dropped_in_error AS Dropped_In_Error,
		mstr.manufacturer AS Manufacturer,
		mstr.data_source_cd AS Data_Source_CD		
	FROM AUL_Inventory inv
	INNER JOIN Ships sh on sh.uic = inv.uic
	INNER JOIN Auth_Use_List mstr ON mstr.niin = inv.niin and mstr.hull_type = sh.hull_type
	LEFT OUTER JOIN Usage_Category uc ON uc.usage_category_id = mstr.usage_category_id
	LEFT OUTER JOIN SMCC smcc ON smcc.smcc_id = mstr.smcc_id
	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.shelf_life_code_id = mstr.shelf_life_code_id
	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.shelf_life_action_code_id = mstr.shelf_life_action_code_id
	LEFT OUTER JOIN Storage_Type stor ON stor.storage_type_id = mstr.storage_type_id
	LEFT OUTER JOIN Cog_Codes cog ON cog.cog_id = mstr.cog_id;
	
GO

-- MSSL Inventory details record
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'v_MSSL_Inventory_Detail')
    DROP VIEW v_MSSL_Inventory_Detail
GO
CREATE VIEW v_MSSL_Inventory_Detail AS
	SELECT 
		inv.uic AS UIC,
		inv.niin AS NIIN,
		inv.location,
		inv.qty AS QTY,
		inv.created AS Created,
		sh.ship_id,
		sh.hull_type AS Hull_Type,
		sh.name AS Ship_Name,
		sh.hull_number AS Hull_Number,
		mstr.ati AS ATI,
		COALESCE(mstr.cog, '') AS COG,
		COALESCE(cog.description, '') AS Cog_Description,
		mstr.mcc AS MCC,
		mstr.ui AS UI,
		mstr.up AS UP,
		mstr.netup AS NetUp,
		mstr.lmc AS LMC,
		mstr.irc AS IRC,
		mstr.dues AS Dues,
		mstr.ro AS RO,
		mstr.rp AS RP,
		mstr.amd AS AMD,
		mstr.smic AS SMIC,
		COALESCE(mstr.slc, '') AS Shelf_Life_Code,
		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,
		COALESCE(slc.description, '') AS Shelf_Life_Description,
		COALESCE(slc.type, '') AS Shelf_Life_Type,
		COALESCE(mstr.slac, '') AS Shelf_Life_Action_Code,
		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,		
		COALESCE(mstr.smcc, '') AS SMCC,
		COALESCE(smcc.description, '') AS SMCC_Description,
		mstr.nomenclature AS Nomenclature,
		mstr.data_source_cd AS Data_Source_CD
	FROM mssl_Inventory inv
	INNER JOIN Ships sh on sh.uic = inv.uic
	INNER JOIN mssl_Master mstr ON mstr.niin = inv.niin
	LEFT OUTER JOIN SMCC smcc ON smcc.smcc = mstr.smcc
	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.slc = mstr.slc
	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.slac = mstr.slac
	LEFT OUTER JOIN Cog_Codes cog ON cog.cog = mstr.cog;
	
GO

-- MSDS Inventory details record
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
        WHERE TABLE_NAME = 'v_MSDS_Inventory_Detail')
    DROP VIEW v_MSDS_Inventory_Detail
GO
CREATE VIEW v_MSDS_Inventory_Detail AS
	SELECT 
		inv.uic AS UIC,
		inv.niin AS NIIN,
		inv.MSDSSERNO,
		inv.CAGE,
		inv.manually_entered AS Manually_Entered,
		inv.created AS Created,
		sh.ship_id,
		sh.hull_type AS Hull_Type,
		sh.name AS Ship_Name,
		sh.hull_number AS Hull_Number,
		mstr.manually_entered AS Master_Manually_Entered,
		mstr.MSDSSERNO AS Master_MSDSSERNO,
		mstr.CAGE AS Master_CAGE,
		mstr.MANUFACTURER AS Manufacturer,
		mstr.PARTNO AS Part_NO,
		mstr.FSC,
		mstr.hcc_id,
		hcc.hcc AS HCC,
		hcc.description AS HCC_Description,
		mstr.file_name AS File_Name,
		mstr.ARTICLE_IND AS Article_IND,
		COALESCE(CONVERT(NVARCHAR(2000),mstr.DESCRIPTION), mstr.PRODUCT_IDENTITY) AS Description,
		mstr.EMERGENCY_TEL AS Emergency_Tel,
		mstr.END_COMP_IND AS End_Comp_IND,
		mstr.END_ITEM_IND AS End_Item_IND,
		mstr.KIT_IND AS Kit_IND,
		mstr.KIT_PART_IND AS Kit_Part_IND,
		mstr.MANUFACTURER_MSDS_NO AS Mfg_MSDS_NO,
		mstr.MIXTURE_IND AS Mixture_IND,
		mstr.PRODUCT_IDENTITY AS Product_Identity,
		mstr.PRODUCT_LOAD_DATE AS Product_Load_Date,
		mstr.PRODUCT_RECORD_STATUS AS Product_Record_Status,
		mstr.PRODUCT_REVISION_NO AS Product_Rev_NO,
		mstr.PROPRIETARY_IND AS Proprietary_IND,
		mstr.PUBLISHED_IND AS Published_IND,
		mstr.PURCHASED_PROD_IND AS Purchased_Prod_IND,
		mstr.PURE_IND AS Pure_IND,
		mstr.RADIOACTIVE_IND AS Radioactive_IND,
		mstr.SERVICE_AGENCY AS Service_Agency,
		mstr.TRADE_NAME AS Trade_Name,
		mstr.TRADE_SECRET_IND AS Trade_Secret_IND,
		mstr.PRODUCT_IND AS Product_IND,
		mstr.PRODUCT_LANGUAGE AS Product_Language,
		mstr.data_source_cd AS Data_Source_CD
	FROM MSDS_Inventory inv
	INNER JOIN Ships sh on sh.uic = inv.uic
	INNER JOIN MSDS_Master mstr ON mstr.msdsserno = inv.msdsserno AND mstr.cage = inv.cage
		AND mstr.msds_id = (SELECT MAX(msds_id) FROM MSDS_Master WHERE msdsserno = inv.msdsserno and cage = inv.cage)
	LEFT OUTER JOIN hcc ON hcc.hcc_id = inv.hcc_id;
	
GO
