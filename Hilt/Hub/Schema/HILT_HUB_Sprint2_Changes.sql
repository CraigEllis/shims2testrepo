USE [hilt_hub]
GO

-- Tweak the DB_Properties table
ALTER TABLE DB_Properties
	ADD DB_Version INT NOT NULL DEFAULT 0;
ALTER TABLE DB_Properties
	DROP COLUMN DBVersion;


-- Hazardous_HCC table
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hazardous_hcc](
	[hazardous_hcc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hazard_id] [bigint] NULL,
	[hcc] [nvarchar](50) NULL,
 CONSTRAINT [PK_hazardous_hcc] PRIMARY KEY CLUSTERED 
(
	[hazardous_hcc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[hazardous_hcc]  WITH NOCHECK ADD  CONSTRAINT [FK_hazardous_hcc_hazards] FOREIGN KEY([hazard_id])
REFERENCES [dbo].[hazards] ([hazard_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[hazardous_hcc] CHECK CONSTRAINT [FK_hazardous_hcc_hazards]
GO


------------- Ships changes -------------
-- Hull_types table
/****** Object:  Table [dbo].[hull_types]    Script Date: 11/02/2011 08:25:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[hull_types](
	[hull_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hull_type] [nvarchar](10) NOT NULL,
	[hull_description] [varchar](100) NULL,
	[active] [bit] NOT NULL,
	[parent_type_id] [bigint] NOT NULL,
 CONSTRAINT [PK_hull_types] PRIMARY KEY CLUSTERED 
(
	[hull_type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[hull_types] ADD  DEFAULT ((1)) FOR [active]
GO

ALTER TABLE [dbo].[hull_types] ADD  DEFAULT ((0)) FOR [parent_type_id]
GO

-- Seed it with some rows
INSERT INTO hull_types VALUES('SUBS', 'SUBMARINES', 1, 0);
INSERT INTO hull_types VALUES('SURFACE', 'SURFACE SHIPS', 1, 0);
INSERT INTO hull_types VALUES('SSN', 'Attack Submarine (Nuclear Powered)', 1, 
	(SELECT hull_type_id FROM hull_types WHERE hull_type = 'SUBS'));
INSERT INTO hull_types VALUES('SSBN', 'Ballistic Missile Submarine (Nuclear-Powered)', 1,  
	(SELECT hull_type_id FROM hull_types WHERE hull_type = 'SUBS'));
GO


-- AUDIT_ships table
/****** Object:  Table [dbo].[AUDIT_ships]    Script Date: 11/02/2011 08:28:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_ships](
	[ship_id] [bigint] NOT NULL,
	[current_status] [bigint] NULL,
	[name] [nvarchar](50) NOT NULL,
	[uic] [nvarchar](10) NOT NULL,
	[hull_type_id] [bigint] NOT NULL,
	[hull_number] [nvarchar](10) NOT NULL,
	[download_aul_date] [datetime] NULL,
	[download_msds_date] [datetime] NULL,
	[download_mssl_date] [datetime] NULL,
	[upload_aul_date] [datetime] NULL,
	[upload_msds_date] [datetime] NULL,
	[upload_mssl_date] [datetime] NULL,
	[created] [datetime] NULL,
	[created_by_id] [bigint] NULL,
	[changed] [datetime] NULL,
	[changed_by_id] [bigint] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO


-- Ships table
-- copy out the existing records
select * into #temp_ships from ships;
GO


-- DROP the table and recreate it
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ships_hull_types]') AND parent_object_id = OBJECT_ID(N'[dbo].[ships]'))
ALTER TABLE [dbo].[ships] DROP CONSTRAINT [FK_ships_hull_types]
GO

USE [hilt_hub]
GO

/****** Object:  Table [dbo].[ships]    Script Date: 11/02/2011 08:31:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ships]') AND type in (N'U'))
DROP TABLE [dbo].[ships]
GO

USE [hilt_hub]
GO

/****** Object:  Table [dbo].[ships]    Script Date: 11/02/2011 08:31:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ships](
	[ship_id] [bigint] IDENTITY(1,1) NOT NULL,
	[current_status] [bigint] NULL,
	[name] [nvarchar](50) NOT NULL,
	[uic] [nvarchar](10) NOT NULL,
	[hull_type_id] [bigint] NOT NULL,
	[hull_number] [nvarchar](10) NOT NULL,
	[download_aul_date] [datetime] NULL,
	[download_msds_date] [datetime] NULL,
	[download_mssl_date] [datetime] NULL,
	[upload_aul_date] [datetime] NULL,
	[upload_msds_date] [datetime] NULL,
	[upload_mssl_date] [datetime] NULL,
	[created] [datetime] NULL,
	[created_by_id] [bigint] NULL,
	[changed] [datetime] NULL,
	[changed_by_id] [bigint] NULL,
 CONSTRAINT [PK_ships] PRIMARY KEY CLUSTERED 
(
	[ship_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ships]  WITH NOCHECK ADD  CONSTRAINT [FK_ships_hull_types] FOREIGN KEY([hull_type_id])
REFERENCES [dbo].[hull_types] ([hull_type_id])
GO

ALTER TABLE [dbo].[ships] CHECK CONSTRAINT [FK_ships_hull_types]
GO


-- insert the old records
insert into ships (current_status, name, uic, hull_type_id, hull_number)
select current_status, name, uic, 3, hull_number from #temp_ships;
GO

DROP TABLE #temp_ships;
GO

-- Create the Ships table triggers
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_ships]
on [dbo].[ships] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_ships (action, action_date, ship_id, current_status, name, uic, hull_type_id, hull_number, 
	download_aul_date, download_msds_date, download_mssl_date, 
	upload_aul_date, upload_msds_date, upload_mssl_date, 
	created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE(), ship_id, current_status, name, uic, hull_type_id, hull_number, 
	download_aul_date, download_msds_date, download_mssl_date, 
	upload_aul_date, upload_msds_date, upload_mssl_date, 
	created, created_by_id, changed, changed_by_id 
	from deleted;
end

GO

CREATE Trigger [dbo].[tr_insert_ships]
on [dbo].[ships] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_ships (action, action_date, ship_id, current_status, name, uic, hull_type_id, hull_number, 
	download_aul_date, download_msds_date, download_mssl_date, 
	upload_aul_date, upload_msds_date, upload_mssl_date, 
	created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE(), ship_id, current_status, name, uic, hull_type_id, hull_number, 
	download_aul_date, download_msds_date, download_mssl_date, 
	upload_aul_date, upload_msds_date, upload_mssl_date, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end

GO

CREATE Trigger [dbo].[tr_update_ships]
on [dbo].[ships] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_ships (action, action_date, ship_id, current_status, name, uic, hull_type_id, hull_number, 
	download_aul_date, download_msds_date, download_mssl_date, 
	upload_aul_date, upload_msds_date, upload_mssl_date, 
	created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE(), ship_id, current_status, name, uic, hull_type_id, hull_number, 
	download_aul_date, download_msds_date, download_mssl_date, 
	upload_aul_date, upload_msds_date, upload_mssl_date, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end

GO

-- Views
USE [hilt_hub]
GO

/****** Object:  View [dbo].[v_Ships]    Script Date: 11/02/2011 10:34:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Ships]
AS
SELECT     dbo.ships.*, dbo.hull_types.hull_type, dbo.hull_types.hull_description, dbo.hull_types.active, dbo.ship_statuses.description
FROM         dbo.ships INNER JOIN
                      dbo.hull_types ON dbo.ships.hull_type_id = dbo.hull_types.hull_type_id INNER JOIN
                      dbo.ship_statuses ON dbo.ships.current_status = dbo.ship_statuses.status_id

GO


------------- Auth Use List changes -------------
-- AUDIT tables changes
ALTER Table [dbo].[AUDIT_auth_use_list]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL,
			[hull_type_id] [bigint] NULL;
GO

ALTER Table [dbo].[AUDIT_auth_use_list]
	DROP COLUMN [hull_type];
GO

ALTER Table [dbo].[AUDIT_aul_inventory]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

-- Master table changes
ALTER Table [dbo].[auth_use_list]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL,
			[hull_type_id] [bigint] NOT NULL DEFAULT 0;
GO

ALTER Table [dbo].[auth_use_list]
	DROP COLUMN [hull_type];
GO

-- Triggers
/****** Object:  Trigger [tr_delete_auth_use_list]    Script Date: 11/02/2011 09:25:56 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_auth_use_list]'))
DROP TRIGGER [dbo].[tr_delete_auth_use_list]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_auth_use_list]'))
DROP TRIGGER [dbo].[tr_insert_auth_use_list]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_auth_use_list]'))
DROP TRIGGER [dbo].[tr_update_auth_use_list]
GO

USE [hilt_hub]
GO

/****** Object:  Trigger [dbo].[tr_delete_auth_use_list]    Script Date: 11/02/2011 09:25:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_auth_use_list]
on [dbo].[auth_use_list] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_auth_use_list (action, action_date, aul_id, hull_type_id, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE() , aul_id, hull_type_id, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from deleted;
end
GO

CREATE Trigger [dbo].[tr_insert_auth_use_list]
on [dbo].[auth_use_list] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_auth_use_list (action, action_date, aul_id, hull_type_id, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE() , aul_id, hull_type_id, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

CREATE Trigger [dbo].[tr_update_auth_use_list]
on [dbo].[auth_use_list] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_auth_use_list (action, action_date, aul_id, hull_type_id, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE() , aul_id, hull_type_id, fsc, niin, ui, um, 
	usage_category_id, description, smcc_id, specs, shelf_life_code_id, shelf_life_action_code_id, 
	storage_type_id, cog_id, spmig, nehc_rpt, catalog_group_id, catalog_serial_number, 
	allowance_qty, manually_entered, dropped_in_error, manufacturer, cage, msds_num, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

-- Inventory table changes
ALTER Table [dbo].[aul_inventory]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

ALTER Table [dbo].[aul_inventory]
	DROP COLUMN	[hull_type],
		[fsc],
		[ui],
		[um],
		[usage_category_id],
		[description],
		[smcc_id],
		[specs],
		[shelf_life_code_id],
		[shelf_life_action_code_id],
		[remarks],
		[storage_type_id],
		[cog_id],
		[spmig],
		[nehc_rpt],
		[catalog_group_id],
		[catalog_serial_number],
		[dropped_in_error],
		[manufacturer],
		[data_source_cd]
GO
ALTER TABLE [dbo].[aul_inventory] REBUILD
GO


-- Triggers
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_aul_inventory]'))
DROP TRIGGER [dbo].[tr_delete_aul_inventory]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_aul_inventory]'))
DROP TRIGGER [dbo].[tr_insert_aul_inventory]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_aul_inventory]'))
DROP TRIGGER [dbo].[tr_update_aul_inventory]
GO

USE [hilt_hub]
GO

/****** Object:  Trigger [dbo].[tr_delete_aul_inventory]    Script Date: 11/02/2011 09:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_aul_inventory]
on [dbo].[aul_inventory] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created, created_by_id, changed, changed_by_id 
	from deleted;
end
GO

CREATE Trigger [dbo].[tr_insert_aul_inventory]
on [dbo].[aul_inventory] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

CREATE Trigger [dbo].[tr_update_aul_inventory]
on [dbo].[aul_inventory] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_aul_inventory (action, action_date, aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE() , aul_inventory_id, uic, niin, qty, allowance_qty, 
	manually_entered, cage, msds_num, created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

-- Views
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_AUL_Inventory_Detail]'))
DROP VIEW [dbo].[v_AUL_Inventory_Detail]
GO

USE [hilt_hub]
GO

/****** Object:  View [dbo].[v_AUL_Inventory_Detail]    Script Date: 11/02/2011 11:32:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_AUL_Inventory_Detail] AS
	SELECT 
		inv.uic AS UIC,
		inv.niin AS NIIN,
		inv.qty AS QTY,
		inv.allowance_qty AS Allowance_QTY,
		COALESCE(inv.manually_entered, '0') AS Manually_Entered,
		COALESCE(inv.cage, '') AS CAGE,
		COALESCE(inv.msds_num, '') AS MSDS_Num,
		inv.created AS Created,
		COALESCE(mstr.ui, '') AS UI,
		COALESCE(mstr.um, '') AS UM,
		sh.ship_id,
		sh.name AS Ship_Name,
		sh.hull_number AS Ship_Number,
		ht.hull_type AS Hull_Type,
		ht.hull_description AS Hull_Description,
		ht.active AS Hull_Active,
		ht.parent_type_id,
		mstr.usage_category_id,
		COALESCE(uc.category, '') AS Usage_Category,
		COALESCE(uc.description, '') AS Usage_Category_Description,
		mstr.description AS Description,
		mstr.smcc_id,
		COALESCE(smcc.smcc, '') AS SMCC,
		COALESCE(smcc.description, '') AS SMCC_Description,
		mstr.specs AS Specs,
		mstr.shelf_life_code_id,
		COALESCE(slc.slc, '') AS Shelf_Life_Code,
		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,
		COALESCE(slc.description, '') AS Shelf_Life_Description,
		COALESCE(slc.type, '') AS Shelf_Life_Type,
		mstr.shelf_life_action_code_id,
		COALESCE(slac.slac, '') AS Shelf_Life_Action_Code,
		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,
		CONVERT(NVARCHAR(2000), mstr.remarks) AS Remarks,
		mstr.storage_type_id,
		COALESCE(stor.type, '') AS Storage_Type,
		COALESCE(stor.description, '') AS Storage_Type_Description,
		mstr.cog_id,
		COALESCE(cog.cog, '') AS COG,
		COALESCE(cog.description, '') AS COG_Description,
		mstr.spmig AS SPMIG,
		mstr.nehc_rpt AS NEHC_Report,
		mstr.catalog_group_id,
		mstr.catalog_serial_number AS Catalog_Serial_Number,
		mstr.dropped_in_error AS Dropped_In_Error,
		mstr.manufacturer AS Manufacturer,
		mstr.data_source_cd AS Data_Source_CD		
	FROM AUL_Inventory inv
	INNER JOIN Ships sh on sh.uic = inv.uic
	INNER JOIN hull_types ht on ht.hull_type_id = sh.hull_type_id
	INNER JOIN Auth_Use_List mstr ON mstr.niin = inv.niin and mstr.hull_type_id = ht.parent_type_id
	LEFT OUTER JOIN Usage_Category uc ON uc.usage_category_id = mstr.usage_category_id
	LEFT OUTER JOIN SMCC smcc ON smcc.smcc_id = mstr.smcc_id
	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.shelf_life_code_id = mstr.shelf_life_code_id
	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.shelf_life_action_code_id = mstr.shelf_life_action_code_id
	LEFT OUTER JOIN Storage_Type stor ON stor.storage_type_id = mstr.storage_type_id
	LEFT OUTER JOIN Cog_Codes cog ON cog.cog_id = mstr.cog_id;
GO


------------- MSDS changes -------------
-- AUDIT tables changes
ALTER Table [dbo].[AUDIT_msds_master]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

ALTER Table [dbo].[AUDIT_msds_inventory]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

-- Master table changes
ALTER Table [dbo].[msds_master]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

-- Triggers
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_msds_master]'))
DROP TRIGGER [dbo].[tr_delete_msds_master]
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_msds_master]'))
DROP TRIGGER [dbo].[tr_insert_msds_master]
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_msds_master]'))
DROP TRIGGER [dbo].[tr_update_msds_master]
GO

USE [hilt_hub]
GO

/****** Object:  Trigger [dbo].[tr_delete_msds_master]    Script Date: 11/02/2011 09:39:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_msds_master]
on [dbo].[msds_master] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_master (action, action_date, msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE(), msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from deleted;
end
GO

CREATE Trigger [dbo].[tr_insert_msds_master]
on [dbo].[msds_master] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_master (action, action_date, msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE(), msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

CREATE Trigger [dbo].[tr_update_msds_master]
on [dbo].[msds_master] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_master (action, action_date, msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE(), msds_id, MSDSSERNO, CAGE, MANUFACTURER, PARTNO, FSC, 
	NIIN, hcc_id, file_name, manually_entered, ARTICLE_IND, EMERGENCY_TEL, 
	END_COMP_IND, END_ITEM_IND, KIT_IND, KIT_PART_IND, MANUFACTURER_MSDS_NO, MIXTURE_IND, 
	PRODUCT_IDENTITY, PRODUCT_LOAD_DATE, PRODUCT_RECORD_STATUS, PRODUCT_REVISION_NO, PROPRIETARY_IND, 
	PUBLISHED_IND, PURCHASED_PROD_IND, PURE_IND, RADIOACTIVE_IND, SERVICE_AGENCY, 
	TRADE_NAME, TRADE_SECRET_IND, PRODUCT_IND, PRODUCT_LANGUAGE, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

-- Inventory table changes
ALTER Table [dbo].[msds_inventory]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_msds_inventory_hcc]') AND parent_object_id = OBJECT_ID(N'[dbo].[msds_inventory]'))
ALTER TABLE [dbo].[msds_inventory] DROP CONSTRAINT [FK_msds_inventory_hcc]
GO

ALTER Table [dbo].[msds_inventory]
	DROP COLUMN		[MANUFACTURER],
		[PARTNO],
		[FSC],
		[hcc_id],
		[file_name],
		[ARTICLE_IND],
		[DESCRIPTION],
		[EMERGENCY_TEL],
		[END_COMP_IND],
		[END_ITEM_IND],
		[KIT_IND],
		[KIT_PART_IND],
		[MANUFACTURER_MSDS_NO],
		[MIXTURE_IND],
		[PRODUCT_IDENTITY],
		[PRODUCT_LOAD_DATE],
		[PRODUCT_RECORD_STATUS],
		[PRODUCT_REVISION_NO],
		[PROPRIETARY_IND],
		[PUBLISHED_IND],
		[PURCHASED_PROD_IND],
		[PURE_IND],
		[RADIOACTIVE_IND],
		[SERVICE_AGENCY],
		[TRADE_NAME],
		[TRADE_SECRET_IND],
		[PRODUCT_IND],
		[PRODUCT_LANGUAGE],
		[data_source_cd]
GO
ALTER TABLE [dbo].[msds_inventory] REBUILD
GO

-- Triggers
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_msds_inventory]
on [dbo].[msds_inventory] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_inventory (action, action_date, msds_inventory_id, MSDSSERNO, CAGE, uic, 
	NIIN, manually_entered, 
	created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE(), msds_inventory_id, MSDSSERNO, CAGE, uic, 
	NIIN, manually_entered, 
	created, created_by_id, changed, changed_by_id 
	from deleted;
end
GO

CREATE Trigger [dbo].[tr_insert_msds_inventory]
on [dbo].[msds_inventory] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_inventory (action, action_date, msds_inventory_id, MSDSSERNO, CAGE, uic, 
	NIIN, manually_entered, 
	created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE(), msds_inventory_id, MSDSSERNO, CAGE, uic, 
	NIIN, manually_entered, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

CREATE Trigger [dbo].[tr_update_msds_inventory]
on [dbo].[msds_inventory] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_msds_inventory (action, action_date, msds_inventory_id, MSDSSERNO, CAGE, uic, 
	NIIN, manually_entered, 
	created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE(), msds_inventory_id, MSDSSERNO, CAGE, uic, 
	NIIN, manually_entered, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

-- Views
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_MSDS_Inventory_Detail]'))
DROP VIEW [dbo].[v_MSDS_Inventory_Detail]
GO

USE [hilt_hub]
GO

/****** Object:  View [dbo].[v_MSDS_Inventory_Detail]    Script Date: 11/02/2011 11:37:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_MSDS_Inventory_Detail] AS
	SELECT 
		inv.uic AS UIC,
		inv.niin AS NIIN,
		inv.MSDSSERNO,
		inv.CAGE,
		inv.manually_entered AS Manually_Entered,
		inv.created AS Created,
		sh.ship_id,
		sh.name AS Ship_Name,
		sh.hull_number AS Hull_Number,
		ht.hull_type AS Hull_Type,
		ht.hull_description AS Hull_Description,
		ht.active AS Hull_Active,
		mstr.manually_entered AS Master_Manually_Entered,
		mstr.MSDSSERNO AS Master_MSDSSERNO,
		mstr.CAGE AS Master_CAGE,
		mstr.MANUFACTURER AS Manufacturer,
		mstr.PARTNO AS Part_NO,
		mstr.FSC,
		mstr.hcc_id,
		hcc.hcc AS HCC,
		hcc.description AS HCC_Description,
		mstr.file_name AS File_Name,
		mstr.ARTICLE_IND AS Article_IND,
		COALESCE(CONVERT(NVARCHAR(2000),mstr.DESCRIPTION), mstr.PRODUCT_IDENTITY) AS Description,
		mstr.EMERGENCY_TEL AS Emergency_Tel,
		mstr.END_COMP_IND AS End_Comp_IND,
		mstr.END_ITEM_IND AS End_Item_IND,
		mstr.KIT_IND AS Kit_IND,
		mstr.KIT_PART_IND AS Kit_Part_IND,
		mstr.MANUFACTURER_MSDS_NO AS Mfg_MSDS_NO,
		mstr.MIXTURE_IND AS Mixture_IND,
		mstr.PRODUCT_IDENTITY AS Product_Identity,
		mstr.PRODUCT_LOAD_DATE AS Product_Load_Date,
		mstr.PRODUCT_RECORD_STATUS AS Product_Record_Status,
		mstr.PRODUCT_REVISION_NO AS Product_Rev_NO,
		mstr.PROPRIETARY_IND AS Proprietary_IND,
		mstr.PUBLISHED_IND AS Published_IND,
		mstr.PURCHASED_PROD_IND AS Purchased_Prod_IND,
		mstr.PURE_IND AS Pure_IND,
		mstr.RADIOACTIVE_IND AS Radioactive_IND,
		mstr.SERVICE_AGENCY AS Service_Agency,
		mstr.TRADE_NAME AS Trade_Name,
		mstr.TRADE_SECRET_IND AS Trade_Secret_IND,
		mstr.PRODUCT_IND AS Product_IND,
		mstr.PRODUCT_LANGUAGE AS Product_Language,
		mstr.data_source_cd AS Data_Source_CD
	FROM MSDS_Inventory inv
	INNER JOIN Ships sh on sh.uic = inv.uic
	INNER JOIN Hull_Types ht on ht.hull_type_id = sh.hull_type_id
	INNER JOIN MSDS_Master mstr ON mstr.msdsserno = inv.msdsserno AND mstr.cage = inv.cage
		AND mstr.msds_id = (SELECT MAX(msds_id) FROM MSDS_Master WHERE msdsserno = inv.msdsserno and cage = inv.cage)
	LEFT OUTER JOIN hcc ON hcc.hcc_id = mstr.hcc_id;
GO


------------- MSSL changes -------------
-- AUDIT tables changes
ALTER Table [dbo].[AUDIT_mssl_master]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

ALTER Table [dbo].[AUDIT_mssl_inventory]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

-- Master table changes
ALTER Table [dbo].[mssl_master]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

-- Triggers
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_mssl_master]'))
DROP TRIGGER [dbo].[tr_delete_mssl_master]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_mssl_master]'))
DROP TRIGGER [dbo].[tr_insert_mssl_master]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_mssl_master]'))
DROP TRIGGER [dbo].[tr_update_mssl_master]
GO

USE [hilt_hub]
GO

/****** Object:  Trigger [dbo].[tr_delete_mssl_master]    Script Date: 11/02/2011 09:54:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_mssl_master]
on [dbo].[mssl_master] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_master (action, action_date, mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE(), mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from deleted;
end
GO

CREATE Trigger [dbo].[tr_insert_mssl_master]
on [dbo].[mssl_master] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_master (action, action_date, mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE(), mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

CREATE Trigger [dbo].[tr_update_mssl_master]
on [dbo].[mssl_master] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_master (action, action_date, mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, 
	created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE(), mssl_master_id, niin, ati, cog, mcc, ui, up, 
	netup, lmc, irc, dues, ro, rp, amd, smic, slc, slac, smcc, nomenclature, data_source_cd, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

-- Inventory table changes
ALTER Table [dbo].[mssl_inventory]
	ADD [created_by_id] [bigint] NULL,
			[changed] [datetime] NULL,
			[changed_by_id] [bigint] NULL;
GO

ALTER Table [dbo].[mssl_inventory]
	DROP COLUMN	[ati],
		[cog],
		[mcc],
		[ui],
		[up],
		[netup],
		[lmc],
		[irc],
		[dues],
		[ro],
		[rp],
		[amd],
		[smic],
		[slc],
		[slac],
		[smcc],
		[nomenclature],
		[data_source_cd];
GO
ALTER TABLE [dbo].[mssl_inventory] REBUILD
GO

-- Triggers
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_delete_mssl_inventory]'))
DROP TRIGGER [dbo].[tr_delete_mssl_inventory]
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_mssl_inventory]'))
DROP TRIGGER [dbo].[tr_insert_mssl_inventory]
GO
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_mssl_inventory]'))
DROP TRIGGER [dbo].[tr_update_mssl_inventory]
GO

USE [hilt_hub]
GO

/****** Object:  Trigger [dbo].[tr_delete_mssl_inventory]    Script Date: 11/02/2011 09:58:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Snydsman>
-- Create date: <11/18/2011>
-- Description:	<Description,,>
-- =============================================
CREATE Trigger [dbo].[tr_delete_mssl_inventory]
on [dbo].[mssl_inventory] for delete
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_inventory (action, action_date, mssl_inventory_id, uic, niin, location, qty, 
	created, created_by_id, changed, changed_by_id) 
select 'DELETE', GETDATE(), mssl_inventory_id, uic, niin, location, qty, 
	created, created_by_id, changed, changed_by_id 
	from deleted;
end
GO

CREATE Trigger [dbo].[tr_insert_mssl_inventory]
on [dbo].[mssl_inventory] for insert
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_inventory (action, action_date, mssl_inventory_id, uic, niin, location, qty, 
	created, created_by_id, changed, changed_by_id) 
select 'INSERT', GETDATE(), mssl_inventory_id, uic, niin, location, qty, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

CREATE Trigger [dbo].[tr_update_mssl_inventory]
on [dbo].[mssl_inventory] for update
as
begin

SET NOCOUNT ON;

insert into AUDIT_mssl_inventory (action, action_date, mssl_inventory_id, uic, niin, location, qty, 
	created, created_by_id, changed, changed_by_id) 
select 'UPDATE', GETDATE(), mssl_inventory_id, uic, niin, location, qty, 
	created, created_by_id, changed, changed_by_id 
	from inserted;
end
GO

-- Views
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_MSSL_Inventory_Detail]'))
DROP VIEW [dbo].[v_MSSL_Inventory_Detail]
GO

USE [hilt_hub]
GO

/****** Object:  View [dbo].[v_MSSL_Inventory_Detail]    Script Date: 11/02/2011 11:39:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_MSSL_Inventory_Detail] AS
	SELECT 
		inv.uic AS UIC,
		inv.niin AS NIIN,
		inv.location,
		inv.qty AS QTY,
		inv.created AS Created,
		sh.ship_id,
		sh.name AS Ship_Name,
		sh.hull_number AS Hull_Number,
		ht.hull_type AS Hull_Type,
		ht.hull_description AS Hull_Description,
		ht.active AS Hull_Active,
		mstr.ati AS ATI,
		COALESCE(mstr.cog, '') AS COG,
		COALESCE(cog.description, '') AS Cog_Description,
		mstr.mcc AS MCC,
		mstr.ui AS UI,
		mstr.up AS UP,
		mstr.netup AS NetUp,
		mstr.lmc AS LMC,
		mstr.irc AS IRC,
		mstr.dues AS Dues,
		mstr.ro AS RO,
		mstr.rp AS RP,
		mstr.amd AS AMD,
		mstr.smic AS SMIC,
		COALESCE(mstr.slc, '') AS Shelf_Life_Code,
		COALESCE(slc.time_in_months, '') AS Shelf_Life_Months,
		COALESCE(slc.description, '') AS Shelf_Life_Description,
		COALESCE(slc.type, '') AS Shelf_Life_Type,
		COALESCE(mstr.slac, '') AS Shelf_Life_Action_Code,
		COALESCE(slac.description, '') AS Shelf_Life_Action_Description,		
		COALESCE(mstr.smcc, '') AS SMCC,
		COALESCE(smcc.description, '') AS SMCC_Description,
		mstr.nomenclature AS Nomenclature,
		mstr.data_source_cd AS Data_Source_CD
	FROM mssl_Inventory inv
	INNER JOIN Ships sh on sh.uic = inv.uic
	INNER JOIN hull_types ht on ht.hull_type_id = sh.hull_type_id
	INNER JOIN mssl_Master mstr ON mstr.niin = inv.niin
	LEFT OUTER JOIN SMCC smcc ON smcc.smcc = mstr.smcc
	LEFT OUTER JOIN Shelf_Life_Code slc ON slc.slc = mstr.slc
	LEFT OUTER JOIN Shelf_Life_Action_Code slac ON slac.slac = mstr.slac
	LEFT OUTER JOIN Cog_Codes cog ON cog.cog = mstr.cog;	
GO
