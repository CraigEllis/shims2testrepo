-- Create the HILT-HUB database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved

USE hilt_hub
GO

-- Insert AUL record
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
        WHERE ROUTINE_NAME = 'InsertAULRecord')
    DROP PROCEDURE InsertAULRecord
GO
CREATE PROCEDURE InsertAULRecord(
	@uic NVARCHAR(10),
	@niin NVARCHAR(9),
	@qty INT,
	@allowance_qty int,
	@manually_entered bit,
	@cage nvarchar(255),
	@msds_num nvarchar(255),	
	@hull_type nvarchar(10),
	@fsc int,
	@ui nvarchar(50),
	@um nvarchar(50),
	@usage_category_id bigint,
	@description nvarchar(1024),
	@smcc_id bigint,
	@specs nvarchar(50),
	@shelf_life_code_id bigint,
	@shelf_life_action_code_id bigint,
	@remarks text,
	@storage_type_id bigint,
	@cog_id bigint,
	@spmig nvarchar(255),
	@nehc_rpt nvarchar(10),
	@catalog_group_id bigint,
	@catalog_serial_number nvarchar(255),
	@dropped_in_error bit,
	@manufacturer nvarchar(255))
AS 
BEGIN
	/* Inserts a new AUL Inventory record into the aul_inventory tablea
		Also checks to see if the record already exists in the auth_use_list table - if
		it doesn't, a new master record is inserted.

		Modified:
			08/23/2011	WES - created
	*/
	
	DECLARE @created DateTime
	SET @created = GETDATE()
	
	-- Do the inventory insert
	INSERT INTO aul_inventory (uic,niin,qty,allowance_qty,
		manually_entered,cage,msds_num,created) 
		VALUES(@uic,@niin,@qty,@allowance_qty,
		@manually_entered,@cage,@msds_num,@created)
		
	-- Check to see if we have the master auth_use_list WHERE niin = @niin)
	IF NOT EXISTS(SELECT * FROM auth_use_list WHERE niin = @niin AND hull_type = @hull_type)
		BEGIN
		-- insert the inventory data as a master record
			INSERT INTO auth_use_list (niin,allowance_qty,manually_entered,
					cage,msds_num,hull_type,fsc,ui,um,
					usage_category_id,description,smcc_id,specs,
					shelf_life_code_id,shelf_life_action_code_id,
					remarks,storage_type_id,cog_id,spmig,
					nehc_rpt,catalog_group_id,catalog_serial_number,
					dropped_in_error,manufacturer,data_source_cd,created) 
				VALUES(@niin, @allowance_qty,@manually_entered,
					@cage,@msds_num,@hull_type,@fsc,@ui,@um,
					@usage_category_id,@description,@smcc_id,@specs,
					@shelf_life_code_id,@shelf_life_action_code_id,
					@remarks,@storage_type_id,@cog_id,@spmig,
					@nehc_rpt,@catalog_group_id,@catalog_serial_number,
					@dropped_in_error,@manufacturer,@uic,@created)
		END
END
GO

-- Update AUL record
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
        WHERE ROUTINE_NAME = 'UpdateAULRecord')
    DROP PROCEDURE UpdateAULRecord
GO
CREATE PROCEDURE UpdateAULRecord(
	@uic NVARCHAR(10),
	@niin NVARCHAR(9),
	@qty INT,
	@allowance_qty int,
	@manually_entered bit,
	@cage nvarchar(255),
	@msds_num nvarchar(255),	
	@hull_type nvarchar(10),
	@fsc int,
	@ui nvarchar(50),
	@um nvarchar(50),
	@usage_category_id bigint,
	@description nvarchar(1024),
	@smcc_id bigint,
	@specs nvarchar(50),
	@shelf_life_code_id bigint,
	@shelf_life_action_code_id bigint,
	@remarks text,
	@storage_type_id bigint,
	@cog_id bigint,
	@spmig nvarchar(255),
	@nehc_rpt nvarchar(10),
	@catalog_group_id bigint,
	@catalog_serial_number nvarchar(255),
	@dropped_in_error bit,
	@manufacturer nvarchar(255))
AS 
BEGIN
	/* Updates an existing AUL Inventory record in the aul_inventory table. and auth_use_list table 
		Also updates the auth_use_list table master record if it's values have changed.

		Modified:
			08/30/2011	WES - created
	*/
	
	DECLARE @created DateTime
	DECLARE @sql NVARCHAR(2000)
	
	SET @created = GETDATE()
	
	-- Do the inventory insert
	SET @sql = 'UPDATE aul_inventory SET' +
		' qty=' + CONVERT(NVARCHAR(10), @qty) + ',' +
		' allowance_qty=' + CONVERT(NVARCHAR(10), @allowance_qty) + ',' +
		' manually_entered=' + CONVERT(NVARCHAR(1), @manually_entered) + ',' +
		' cage=' + @cage + ',' +
		' msds_num=' + @msds_num + ',' +
		' created=' + @created +
		' WHERE uic = ' + @uic + ' AND niin=' +	@niin
	EXEC (@sql)
		
	-- Check to see if we have the master auth_use_list WHERE niin = @niin)
	IF NOT EXISTS(SELECT * FROM auth_use_list WHERE niin = @niin AND hull_type = @hull_type)
		BEGIN
		-- insert the inventory data as a master record
			INSERT INTO auth_use_list (niin,allowance_qty,manually_entered,
					cage,msds_num,hull_type,fsc,ui,um,
					usage_category_id,description,smcc_id,specs,
					shelf_life_code_id,shelf_life_action_code_id,
					remarks,storage_type_id,cog_id,spmig,
					nehc_rpt,catalog_group_id,catalog_serial_number,
					dropped_in_error,manufacturer,data_source_cd,created) 
				VALUES(@niin,	@allowance_qty,@manually_entered,
					@cage,@msds_num,@hull_type,@fsc,@ui,@um,
					@usage_category_id,@description,@smcc_id,@specs,
					@shelf_life_code_id,@shelf_life_action_code_id,
					@remarks,@storage_type_id,@cog_id,@spmig,
					@nehc_rpt,@catalog_group_id,@catalog_serial_number,
					@dropped_in_error,@manufacturer,@uic,@created)
		END
	ELSE
		BEGIN
		-- update the existing master record
			SET @sql = 'UPDATE auth_use_list SET' +
				' qty=' + CONVERT(NVARCHAR(10), @qty) + ',' +
				' allowance_qty=' + CONVERT(NVARCHAR(10), @allowance_qty) + ',' +
				' manually_entered=' + CONVERT(NVARCHAR(1), @manually_entered) + ',' +
				' cage=' + @cage + ',' +
				' msds_num=' + @msds_num + ',' +
				' hull_type=' + @hull_type + ',' +
				' fsc=' + CONVERT(NVARCHAR(10), @fsc) + ',' +
				' ui=' + @ui + ',' +
				' um=' + @um + ',' +
				' usage_category_id=' + CONVERT(NVARCHAR(10), @usage_category_id) + ',' +
				' description=' + @description + ',' +
				' smcc_id=' + CONVERT(NVARCHAR(10), @smcc_id) + ',' +
				' specs=' + @specs + ',' +
				' shelf_life_code_id=' + CONVERT(NVARCHAR(10), @shelf_life_code_id) + ',' +
				' shelf_life_action_code_id=' + CONVERT(NVARCHAR(10), @shelf_life_action_code_id) + ',' +
				' remarks=' + CONVERT(NVARCHAR(2000), @remarks) + ',' +
				' storage_type_id=' + CONVERT(NVARCHAR(10), @storage_type_id) + ',' +
				' cog_id=' + CONVERT(NVARCHAR(10), @cog_id) + ',' +
				' spmig=' + @spmig + ',' +
				' nehc_rpt=' + @nehc_rpt + ',' +
				' catalog_group_id=' + CONVERT(NVARCHAR(10), @catalog_group_id) + ',' +
				' catalog_serial_number=' + @catalog_serial_number + ',' +
				' dropped_in_error=' + CONVERT(NVARCHAR(1), @dropped_in_error) + ',' +
				' manufacturer=' + @manufacturer + ',' +
				' data_source_cd=' + @uic + ',' +
				' created=' + @created +
				' WHERE niin=' + @niin + ' AND hull_type=' + @hull_type
			EXEC (@sql)
		END
END
GO

-- Insert MSDS record
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
        WHERE ROUTINE_NAME = 'InsertMSDSRecord')
    DROP PROCEDURE InsertMSDSRecord
GO
CREATE PROCEDURE InsertMSDSRecord(
	@uic nvarchar(10),
	@NIIN nvarchar(9),
	@manually_entered bit,
	@MSDSSERNO nvarchar(10),
	@CAGE nvarchar(5),
	@MANUFACTURER nvarchar(255),
	@PARTNO nvarchar(100),
	@FSC nvarchar(18),
	@hcc_id bigint,
	@msds_file image,
	@file_name nvarchar(255),
	@ARTICLE_IND nchar(10),
	@DESCRIPTION ntext,
	@EMERGENCY_TEL nvarchar(50),
	@END_COMP_IND nchar(10),
	@END_ITEM_IND nchar(10),
	@KIT_IND nchar(10),
	@KIT_PART_IND nchar(10),
	@MANUFACTURER_MSDS_NO nvarchar(50),
	@MIXTURE_IND nchar(10),
	@PRODUCT_IDENTITY nvarchar(255),
	@PRODUCT_LOAD_DATE smalldatetime,
	@PRODUCT_RECORD_STATUS nchar(10),
	@PRODUCT_REVISION_NO nvarchar(18),
	@PROPRIETARY_IND nchar(1),
	@PUBLISHED_IND nchar(1),
	@PURCHASED_PROD_IND nchar(1),
	@PURE_IND nchar(1),
	@RADIOACTIVE_IND nchar(1),
	@SERVICE_AGENCY nvarchar(10),
	@TRADE_NAME nvarchar(255),
	@TRADE_SECRET_IND nchar(1),
	@PRODUCT_IND nvarchar(1),
	@PRODUCT_LANGUAGE nvarchar(10))
 
AS 
BEGIN
	/* Inserts a new MSDS Inventory record into the msds_inventory table
		Also checks to see if the record already exists in the msds_master table - if
		it doesn't, a new master record is inserted.

		Modified:
			08/23/2011	WES - created
	*/
	
	DECLARE @created DateTime
	SET @created = GETDATE()
	
	-- Do the inventory insert
	INSERT INTO msds_inventory (uic,niin,MSDSSERNO,CAGE,
		manually_entered,created) 
		VALUES(@uic,@niin,@MSDSSERNO,@CAGE,
		@manually_entered,@created)
		
	-- Check to see if we have the master record
	IF NOT EXISTS(SELECT * FROM msds_master WHERE niin = @niin AND data_source_cd = @uic)
		BEGIN
		-- insert the inventory data as a master record
			INSERT INTO msds_master (NIIN,manually_entered,MSDSSERNO,CAGE,
					MANUFACTURER,PARTNO,FSC,hcc_id,msds_file,file_name,
					ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,
					END_COMP_IND,END_ITEM_IND,KIT_IND,KIT_PART_IND,
					MANUFACTURER_MSDS_NO,MIXTURE_IND,PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,
					PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,PROPRIETARY_IND,PUBLISHED_IND,
					PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,SERVICE_AGENCY,
					TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,data_source_cd,created) 
				VALUES(@NIIN,@manually_entered,@MSDSSERNO,@CAGE,
					@MANUFACTURER,@PARTNO,@FSC,@hcc_id,@msds_file,@file_name,
					@ARTICLE_IND,@DESCRIPTION,@EMERGENCY_TEL,
					@END_COMP_IND,@END_ITEM_IND,@KIT_IND,@KIT_PART_IND,
					@MANUFACTURER_MSDS_NO,@MIXTURE_IND,@PRODUCT_IDENTITY,@PRODUCT_LOAD_DATE,
					@PRODUCT_RECORD_STATUS,@PRODUCT_REVISION_NO,@PROPRIETARY_IND,@PUBLISHED_IND,
					@PURCHASED_PROD_IND,@PURE_IND,@RADIOACTIVE_IND,@SERVICE_AGENCY,
					@TRADE_NAME,@TRADE_SECRET_IND,@PRODUCT_IND,@PRODUCT_LANGUAGE,
					@uic,@created)
		END
END
GO

-- Insert MSSL record
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES
        WHERE ROUTINE_NAME = 'InsertMSSLRecord')
    DROP PROCEDURE InsertMSSLRecord
GO
CREATE PROCEDURE InsertMSSLRecord(
	@uic NVARCHAR(10),
	@niin NVARCHAR(50),
	@ati NVARCHAR(50),
	@cog NVARCHAR(50),
	@mcc NVARCHAR(50),
	@ui NVARCHAR(50),
	@up DECIMAL(18,2),
	@netup DECIMAL(18,2),
	@location NVARCHAR(50),
	@qty INT,
	@lmc  NVARCHAR(50),
	@irc NVARCHAR(50),
	@dues INT,
	@ro INT,
	@rp INT,
	@amd DECIMAL(18,2),
	@smic NVARCHAR(50),
	@slc NVARCHAR(50),
	@slac NVARCHAR(50),
	@smcc NVARCHAR(50),
	@nomenclature NVARCHAR(50))
AS 
BEGIN
	/* Inserts a new MSSL Inventory record into the mssl_inventory table
		Also checks to see if the NIIN already exists in the mssl_master table - if
		it doesn't, a new master record is inserted.

		Modified:
			08/23/2011	WES - created
	*/
	
	DECLARE @created DateTime
	SET @created = GETDATE()
	
	-- Do the inventory insert
	INSERT INTO mssl_inventory (uic,niin,location,qty,created) 
		VALUES(@uic,@niin,@location,@qty,@created)
		
	-- Check to see if we have the master record
	IF NOT EXISTS(SELECT * FROM mssl_master WHERE niin = @niin)
		BEGIN
		-- insert the inventory data as a master record
			INSERT INTO mssl_master (niin,ati,cog,mcc,ui,up,netup,
					lmc,irc,dues,ro,rp,amd,smic,slc,slac,smcc,
					nomenclature,data_source_cd,created) 
				VALUES(@niin,@ati,@cog,@mcc,@ui,@up,@netup,
					@lmc,@irc,@dues,@ro,@rp,@amd,@smic,@slc,@slac,@smcc,
					@nomenclature,@uic,@created)
		END
END
GO



