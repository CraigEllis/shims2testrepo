-- Scripts to populate HILT_HUB database with test data from the Hazmat database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved
-- Atmosphere_Control
set identity_insert atmosphere_control on;
insert into atmosphere_control (atmosphere_control_id,atmosphere_control_number,
	cage,niin,msds_serial_number,shelf_life) 
	select atmosphere_control_id,atmosphere_control_number,
	cage,niin,msds_serial_number,shelf_life from hazmat.dbo.atmosphere_control;
set identity_insert atmosphere_control off;

-- Catalog_Groups
set identity_insert catalog_groups on;
insert into catalog_groups (catalog_group_id,group_name) 
	select catalog_group_id,group_name from hazmat.dbo.catalog_groups;
set identity_insert catalog_groups off;

-- COG_Codes
set identity_insert cog_codes on;
insert into cog_codes (cog_id,cog,description) 
	select cog_id,cog,description from hazmat.dbo.cog_codes;
set identity_insert cog_codes off;

-- COSAL_Niin_Allowances
set identity_insert cosal_niin_allowances on;
insert into cosal_niin_allowances (cosal_niin_id,COSAL,NIIN,
	[AT],ALLOWANCE_QTY) 
	select cosal_niin_id,COSAL,NIIN,
	[AT],ALLOWANCE_QTY from hazmat.dbo.cosal_niin_allowances;
set identity_insert cosal_niin_allowances off;

-- Hazard_Warnings
set identity_insert hazard_warnings on;
insert into hazard_warnings (hazard_warning_id,hazard_id_1,
	hazard_id_2,warning_level) 
	select hazard_warning_id,hazard_id_1,
	hazard_id_2,warning_level from hazmat.dbo.hazard_warnings;
set identity_insert hazard_warnings off;

-- Shelf_Life_Action_Code
set identity_insert shelf_life_action_code on;
insert into shelf_life_action_code (shelf_life_action_code_id,
	slac,description) 
	select shelf_life_action_code_id,
	slac,description from hazmat.dbo.shelf_life_action_code;
set identity_insert shelf_life_action_code off;

-- Shelf_Life_Code
set identity_insert shelf_life_code on;
insert into shelf_life_code (shelf_life_code_id,slc,
	time_in_months,description,type) 
	select shelf_life_code_id,slc,
	time_in_months,description,type from hazmat.dbo.shelf_life_code;
set identity_insert shelf_life_code off;

-- Ship_Statuses
set identity_insert ship_statuses on;
insert into ship_statuses (status_id,description) 
	select status_id,description from hazmat.dbo.ship_statuses;
set identity_insert ship_statuses off;

-- SMCC
set identity_insert smcc on;
insert into smcc (smcc_id,smcc,description) 
	select smcc_id,smcc,description from hazmat.dbo.smcc;
set identity_insert smcc off;

-- Storage_Type
set identity_insert storage_type on;
insert into storage_type (storage_type_id,type,description) 
	select storage_type_id,type,description from hazmat.dbo.storage_type;
set identity_insert storage_type off;

-- Usage_Category
set identity_insert usage_category on;
insert into usage_category (usage_category_id,category,description) 
	select usage_category_id,category,description from hazmat.dbo.usage_category;
set identity_insert usage_category off;

-- Ships
set identity_insert ships on;
insert into ships (ship_id,current_status,name,uic,hull_type,hull_number,
	POC_Name,POC_Telephone,POC_Email) 
	select ship_id,current_status,name,uic,hull_type,hull_number,
		POC_Name,POC_Telephone,POC_Email from hazmat.dbo.ships;
set identity_insert ships off;

-- HCC
set identity_insert hcc on;
insert into hcc (hcc_id,hcc,description) 
	select hcc_id,hcc,description from hazmat.dbo.hcc;
set identity_insert hcc off;

-- Hazards
set identity_insert hazards on;
insert into hazards (hazard_id,hazard_name) 
	select hazard_id,hazard_name from hazmat.dbo.hazards;
set identity_insert hazards off;

-- User_Roles
set identity_insert user_roles on;
insert into user_roles (user_role_id,username,role) 
	select user_role_id,username,role from hazmat.dbo.user_roles;
set identity_insert user_roles off;

-- Data Sources (Manually created for HMIRS and California)
insert into data_sources (data_source_cd,name) VALUES('HMIRS', 'HMIRS');
insert into data_sources (data_source_cd,name) VALUES('N20079', 'PCU CALIFORNIA (SSN 781)');

-- AUL Master
set identity_insert auth_use_list on;
insert into auth_use_list (aul_id,hull_type,fsc,niin,
	ui,um,usage_category_id,description,smcc_id,
	specs,shelf_life_code_id,shelf_life_action_code_id,remarks,storage_type_id,
	cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,allowance_qty,
	manually_entered,dropped_in_error,manufacturer,cage,msds_num,
	data_source_cd,created) 
	select niin_catalog_id,'SSN',fsc,niin,
	ui,um,usage_category_id,description,smcc_id,
	specs,shelf_life_code_id,shelf_life_action_code_id,remarks,storage_type_id,
	cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,allowance_qty,
	manually_entered,dropped_in_error,manufacturer,cage,msds_num,
	'N20079', GETDATE() from hazmat.dbo.niin_catalog
set identity_insert auth_use_list off;
-- Add allowed Qty
select niin, min(allowance_qty) as qty into #niin_qty 
	from hazmat.dbo.cosal_niin_allowances group by niin
begin tran;
Update auth_use_list
	SET auth_use_list.allowance_qty = #niin_qty.qty
	FROM #niin_qty
	WHERE auth_use_list.niin = #niin_qty.niin; 
commit tran;

-- AUL Inventory
set identity_insert aul_inventory on;
insert into aul_inventory (aul_inventory_id,uic,hull_type,
	fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,
	shelf_life_code_id,shelf_life_action_code_id,remarks,storage_type_id,
	cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,
	qty,allowance_qty,manually_entered,dropped_in_error,manufacturer,
	cage,msds_num,data_source_cd,created) 
	select niin_catalog_id, 'N20079','SSN',
		fsc,niin,ui,um,usage_category_id,description,smcc_id,specs,
		shelf_life_code_id,shelf_life_action_code_id,remarks,storage_type_id,
		cog_id,spmig,nehc_rpt,catalog_group_id,catalog_serial_number,
		0,allowance_qty,manually_entered,dropped_in_error,manufacturer,
		cage,msds_num,'N20079',created from hazmat.dbo.niin_catalog;
set identity_insert aul_inventory off;
-- update with quantities
begin tran;
Update aul_inventory
	SET aul_inventory.allowance_qty = #niin_qty.qty
	FROM #niin_qty
	WHERE aul_inventory.niin = #niin_qty.niin; 
commit tran;

drop table #niin_qty

-- MSSL Master
set identity_insert mssl_master on;
insert into mssl_master (mssl_master_id,niin,
	ati,cog,mcc,ui,up,netup,lmc,irc,dues,
	ro,rp,amd,smic,slc,slac,smcc,nomenclature,
	data_source_cd,created) 
	select mssl_inventory_id,niin,
	ati,cog,mcc,ui,up,netup,lmc,irc,dues,
	ro,rp,amd,smic,slc,slac,smcc,nomenclature,
	'N20079',date_inserted from hazmat.dbo.mssl_inventory;
set identity_insert mssl_master off;

-- MSSL Inventory
set identity_insert mssl_inventory on;
insert into mssl_inventory (mssl_inventory_id,uic,niin,
	ati,cog,mcc,ui,up,netup,location,qty,lmc,irc,dues,
	ro,rp,amd,smic,slc,slac,smcc,nomenclature,
	data_source_cd,created) 
	select mssl_inventory_id,'N20079',niin,
	ati,cog,mcc,ui,up,netup,location,qty,lmc,irc,dues,
	ro,rp,amd,smic,slc,slac,smcc,nomenclature,
	'N20079',date_inserted from hazmat.dbo.mssl_inventory;
set identity_insert mssl_inventory off;

--MSDS Inventory
set identity_insert msds_inventory on;
insert into msds_inventory (msds_inventory_id,MSDSSERNO,CAGE,uic,
	MANUFACTURER,PARTNO,FSC,NIIN,hcc_id,file_name,manually_entered,
	ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,END_COMP_IND,END_ITEM_IND,
	KIT_IND,KIT_PART_IND,MANUFACTURER_MSDS_NO,MIXTURE_IND,
	PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,
	PROPRIETARY_IND,PUBLISHED_IND,PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,
	SERVICE_AGENCY,TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,
	data_source_cd,created) 
	select msds_id,MSDSSERNO,CAGE,'N20079',
	MANUFACTURER,PARTNO,FSC,NIIN,hcc_id,file_name,manually_entered,
	ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,END_COMP_IND,END_ITEM_IND,
	KIT_IND,KIT_PART_IND,MANUFACTURER_MSDS_NO,MIXTURE_IND,
	PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,
	PROPRIETARY_IND,PUBLISHED_IND,PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,
	SERVICE_AGENCY,TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,
	'N20079', GETDATE() from hazmat.dbo.msds
set identity_insert msds_inventory off;

-- MSDS Master
set identity_insert msds_master on;
insert into msds_master (msds_id,MSDSSERNO,CAGE,
	MANUFACTURER,PARTNO,FSC,NIIN,hcc_id,msds_file,file_name,manually_entered,
	ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,END_COMP_IND,END_ITEM_IND,
	KIT_IND,KIT_PART_IND,MANUFACTURER_MSDS_NO,MIXTURE_IND,
	PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,
	PROPRIETARY_IND,PUBLISHED_IND,PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,
	SERVICE_AGENCY,TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,
	data_source_cd,created) 
	select msds_id,MSDSSERNO,CAGE,
	MANUFACTURER,PARTNO,FSC,NIIN,hcc_id,msds_file,file_name,manually_entered,
	ARTICLE_IND,DESCRIPTION,EMERGENCY_TEL,END_COMP_IND,END_ITEM_IND,
	KIT_IND,KIT_PART_IND,MANUFACTURER_MSDS_NO,MIXTURE_IND,
	PRODUCT_IDENTITY,PRODUCT_LOAD_DATE,PRODUCT_RECORD_STATUS,PRODUCT_REVISION_NO,
	PROPRIETARY_IND,PUBLISHED_IND,PURCHASED_PROD_IND,PURE_IND,RADIOACTIVE_IND,
	SERVICE_AGENCY,TRADE_NAME,TRADE_SECRET_IND,PRODUCT_IND,PRODUCT_LANGUAGE,
	'N20079', GETDATE() from hazmat.dbo.msds
set identity_insert msds_master off;

-- MSDS Support Tables
set identity_insert msds_afjm_psn on;
insert into msds_afjm_psn (afjm_psn_id,AFJM_HAZARD_CLASS,AFJM_PACK_PARAGRAPH,
AFJM_PACK_GROUP,AFJM_PROP_SHIP_NAME,AFJM_PROP_SHIP_MODIFIER,AFJM_PSN_CODE,
AFJM_SPECIAL_PROV,AFJM_SUBSIDIARY_RISK,AFJM_SYMBOLS,AFJM_UN_ID_NUMBER,msds_id)
	select afjm_psn_id,AFJM_HAZARD_CLASS,AFJM_PACK_PARAGRAPH,
	AFJM_PACK_GROUP,AFJM_PROP_SHIP_NAME,AFJM_PROP_SHIP_MODIFIER,AFJM_PSN_CODE,
	AFJM_SPECIAL_PROV,AFJM_SUBSIDIARY_RISK,AFJM_SYMBOLS,AFJM_UN_ID_NUMBER,msds_id 
	from hazmat.dbo.msds_afjm_psn;
set identity_insert msds_afjm_psn off;

set identity_insert msds_contractor_info on;
insert into msds_contractor_info (contractor_id,CT_NUMBER,CT_CAGE,CT_CITY,
	CT_COMPANY_NAME,CT_COUNTRY,CT_PO_BOX,CT_PHONE,PURCHASE_ORDER_NO,
	msds_id,CT_STATE)
	select contractor_id,CT_NUMBER,CT_CAGE,CT_CITY,
	CT_COMPANY_NAME,CT_COUNTRY,CT_PO_BOX,CT_PHONE,PURCHASE_ORDER_NO,
	msds_id,CT_STATE 
	from hazmat.dbo.msds_contractor_info;
set identity_insert msds_contractor_info off;

set identity_insert msds_disposal on;
insert into msds_disposal (disp_info_id,DISPOSAL_ADD_INFO,
	EPA_HAZ_WASTE_CODE,EPA_HAZ_WASTE_IND,EPA_HAZ_WASTE_NAME,msds_id)
	select disp_info_id,DISPOSAL_ADD_INFO,
	EPA_HAZ_WASTE_CODE,EPA_HAZ_WASTE_IND,EPA_HAZ_WASTE_NAME,msds_id 
	from hazmat.dbo.msds_disposal;
set identity_insert msds_disposal off;

set identity_insert msds_document_types on;
insert into msds_document_types (doc_type_id,msds_id,MSDS_TRANSLATED,
	NESHAP_COMP_CERT,OTHER_DOCS,PRODUCT_SHEET,TRANSPORTATION_CERT,
	msds_translated_filename,neshap_comp_filename,other_docs_filename,
	product_sheet_filename,transportation_cert_filename,
	MANUFACTURER_LABEL,manufacturer_label_filename)
	select doc_type_id,msds_id,MSDS_TRANSLATED,
	NESHAP_COMP_CERT,OTHER_DOCS,PRODUCT_SHEET,TRANSPORTATION_CERT,
	msds_translated_filename,neshap_comp_filename,other_docs_filename,
	product_sheet_filename,transportation_cert_filename,
	MANUFACTURER_LABEL,manufacturer_label_filename 
	from hazmat.dbo.msds_document_types;
set identity_insert msds_document_types off;

set identity_insert msds_document_types on;
insert into msds_document_types (doc_type_id,msds_id,MSDS_TRANSLATED,
	NESHAP_COMP_CERT,OTHER_DOCS,PRODUCT_SHEET,TRANSPORTATION_CERT,
	msds_translated_filename,neshap_comp_filename,other_docs_filename,
	product_sheet_filename,transportation_cert_filename,
	MANUFACTURER_LABEL,manufacturer_label_filename)
	select doc_type_id,msds_id,MSDS_TRANSLATED,
	NESHAP_COMP_CERT,OTHER_DOCS,PRODUCT_SHEET,TRANSPORTATION_CERT,
	msds_translated_filename,neshap_comp_filename,other_docs_filename,
	product_sheet_filename,transportation_cert_filename,
	MANUFACTURER_LABEL,manufacturer_label_filename 
	from hazmat.dbo.msds_document_types;
set identity_insert msds_document_types off;

set identity_insert msds_dot_psn on;
insert into msds_dot_psn (dot_psn_id,DOT_HAZARD_CLASS_DIV,DOT_HAZARD_LABEL,
	DOT_MAX_CARGO,DOT_MAX_PASSENGER,DOT_PACK_BULK,DOT_PACK_EXCEPTIONS,DOT_PACK_NONBULK,
	DOT_PROP_SHIP_NAME,DOT_PROP_SHIP_MODIFIER,DOT_PSN_CODE,DOT_SPECIAL_PROVISION,
	DOT_SYMBOLS,DOT_UN_ID_NUMBER,DOT_WATER_OTHER_REQ,DOT_WATER_VESSEL_STOW,
	msds_id,DOT_PACK_GROUP)
	select dot_psn_id,DOT_HAZARD_CLASS_DIV,DOT_HAZARD_LABEL,
	DOT_MAX_CARGO,DOT_MAX_PASSENGER,DOT_PACK_BULK,DOT_PACK_EXCEPTIONS,DOT_PACK_NONBULK,
	DOT_PROP_SHIP_NAME,DOT_PROP_SHIP_MODIFIER,DOT_PSN_CODE,DOT_SPECIAL_PROVISION,
	DOT_SYMBOLS,DOT_UN_ID_NUMBER,DOT_WATER_OTHER_REQ,DOT_WATER_VESSEL_STOW,
	msds_id,DOT_PACK_GROUP 
	from hazmat.dbo.msds_dot_psn;
set identity_insert msds_dot_psn off;

set identity_insert msds_iata_psn on;
insert into msds_iata_psn (iata_psn_id,IATA_CARGO_PACKING,IATA_HAZARD_CLASS,
	IATA_HAZARD_LABEL,IATA_PACK_GROUP,IATA_PASS_AIR_PACK_LMT_INSTR,
	IATA_PASS_AIR_PACK_LMT_PER_PKG,IATA_PASS_AIR_PACK_NOTE,IATA_PROP_SHIP_NAME,
	IATA_PROP_SHIP_MODIFIER,IATA_CARGO_PACK_MAX_QTY,IATA_PSN_CODE,IATA_PASS_AIR_MAX_QTY,
	IATA_SPECIAL_PROV,IATA_SUBSIDIARY_RISK,IATA_UN_ID_NUMBER,msds_id)
	select iata_psn_id,IATA_CARGO_PACKING,IATA_HAZARD_CLASS,
	IATA_HAZARD_LABEL,IATA_PACK_GROUP,IATA_PASS_AIR_PACK_LMT_INSTR,
	IATA_PASS_AIR_PACK_LMT_PER_PKG,IATA_PASS_AIR_PACK_NOTE,IATA_PROP_SHIP_NAME,
	IATA_PROP_SHIP_MODIFIER,IATA_CARGO_PACK_MAX_QTY,IATA_PSN_CODE,IATA_PASS_AIR_MAX_QTY,
	IATA_SPECIAL_PROV,IATA_SUBSIDIARY_RISK,IATA_UN_ID_NUMBER,msds_id 
	from hazmat.dbo.msds_iata_psn;
set identity_insert msds_iata_psn off;

set identity_insert msds_imo_psn on;
insert into msds_imo_psn (imo_psn_id,IMO_EMS_NO,IMO_HAZARD_CLASS,IMO_IBC_INSTR,
	IMO_LIMITED_QTY,IMO_PACK_GROUP,IMO_PACK_INSTRUCTIONS,IMO_PACK_PROVISIONS,
	IMO_PROP_SHIP_NAME,IMO_PROP_SHIP_MODIFIER,IMO_PSN_CODE,IMO_SPECIAL_PROV,
	IMO_STOW_SEGR,IMO_SUBSIDIARY_RISK,IMO_TANK_INSTR_IMO,IMO_TANK_INSTR_PROV,
	IMO_TANK_INSTR_UN,IMO_UN_NUMBER,msds_id,IMO_IBC_PROVISIONS)
	select imo_psn_id,IMO_EMS_NO,IMO_HAZARD_CLASS,IMO_IBC_INSTR,
	IMO_LIMITED_QTY,IMO_PACK_GROUP,IMO_PACK_INSTRUCTIONS,IMO_PACK_PROVISIONS,
	IMO_PROP_SHIP_NAME,IMO_PROP_SHIP_MODIFIER,IMO_PSN_CODE,IMO_SPECIAL_PROV,
	IMO_STOW_SEGR,IMO_SUBSIDIARY_RISK,IMO_TANK_INSTR_IMO,IMO_TANK_INSTR_PROV,
	IMO_TANK_INSTR_UN,IMO_UN_NUMBER,msds_id,IMO_IBC_PROVISIONS 
	from hazmat.dbo.msds_imo_psn;
set identity_insert msds_imo_psn off;

set identity_insert msds_ingredients on;
insert into msds_ingredients (ingredients_id,msds_id,CAS,RTECS_NUM,RTECS_CODE,
	INGREDIENT_NAME,PRCNT,OSHA_PEL,OSHA_STEL,ACGIH_TLV,ACGIH_STEL,
	EPA_REPORT_QTY,DOT_REPORT_QTY,PRCNT_VOL_VALUE,PRCNT_VOL_WEIGHT,
	CHEM_MFG_COMP_NAME,ODS_IND,OTHER_REC_LIMITS)
	select ingredients_id,msds_id,CAS,RTECS_NUM,RTECS_CODE,
	INGREDIENT_NAME,PRCNT,OSHA_PEL,OSHA_STEL,ACGIH_TLV,ACGIH_STEL,
	EPA_REPORT_QTY,DOT_REPORT_QTY,PRCNT_VOL_VALUE,PRCNT_VOL_WEIGHT,
	CHEM_MFG_COMP_NAME,ODS_IND,OTHER_REC_LIMITS 
	from hazmat.dbo.msds_ingredients;
set identity_insert msds_ingredients off;

set identity_insert msds_item_description on;
insert into msds_item_description (item_description_id,msds_id,ITEM_MANAGER,
	ITEM_NAME,SPECIFICATION_NUMBER,TYPE_GRADE_CLASS,UNIT_OF_ISSUE,
	QUANTITATIVE_EXPRESSION,UI_CONTAINER_QTY,TYPE_OF_CONTAINER,BATCH_NUMBER,
	LOT_NUMBER,LOG_FLIS_NIIN_VER,LOG_FSC,NET_UNIT_WEIGHT,SHELF_LIFE_CODE,
	SPECIAL_EMP_CODE,UN_NA_NUMBER,UPC_GTIN)
	select item_description_id,msds_id,ITEM_MANAGER,
	ITEM_NAME,SPECIFICATION_NUMBER,TYPE_GRADE_CLASS,UNIT_OF_ISSUE,
	QUANTITATIVE_EXPRESSION,UI_CONTAINER_QTY,TYPE_OF_CONTAINER,BATCH_NUMBER,
	LOT_NUMBER,LOG_FLIS_NIIN_VER,LOG_FSC,NET_UNIT_WEIGHT,SHELF_LIFE_CODE,
	SPECIAL_EMP_CODE,UN_NA_NUMBER,UPC_GTIN 
	from hazmat.dbo.msds_item_description;
set identity_insert msds_item_description off;

set identity_insert msds_label_info on;
insert into msds_label_info (label_info_id,COMPANY_CAGE_RP,COMPANY_NAME_RP,
	LABEL_EMERG_PHONE,LABEL_ITEM_NAME,LABEL_PROC_YEAR,LABEL_PROD_IDENT,
	LABEL_PROD_SERIALNO,LABEL_SIGNAL_WORD,LABEL_STOCK_NO,SPECIFIC_HAZARDS,msds_id)
	select label_info_id,COMPANY_CAGE_RP,COMPANY_NAME_RP,
	LABEL_EMERG_PHONE,LABEL_ITEM_NAME,LABEL_PROC_YEAR,LABEL_PROD_IDENT,
	LABEL_PROD_SERIALNO,LABEL_SIGNAL_WORD,LABEL_STOCK_NO,SPECIFIC_HAZARDS,msds_id 
	from hazmat.dbo.msds_label_info;
set identity_insert msds_label_info off;

set identity_insert msds_phys_chemical on;
insert into msds_phys_chemical (phys_chemical_id,msds_id,VAPOR_PRESS,VAPOR_DENS,
	SPECIFIC_GRAV,VOC_POUNDS_GALLON,VOC_GRAMS_LITER,PH,VISCOSITY,EVAP_RATE_REF,
	SOL_IN_WATER,APP_ODOR,PERCENT_VOL_VOLUME,AUTOIGNITION_TEMP,CARCINOGEN_IND,
	EPA_ACUTE,EPA_CHRONIC,EPA_FIRE,EPA_PRESSURE,EPA_REACTIVITY,FLASH_PT_TEMP,
	NEUT_AGENT,NFPA_FLAMMABILITY,NFPA_HEALTH,NFPA_REACTIVITY,NFPA_SPECIAL,
	OSHA_CARCINOGENS,OSHA_COMB_LIQUID,OSHA_COMP_GAS,OSHA_CORROSIVE,OSHA_EXPLOSIVE,
	OSHA_FLAMMABLE,OSHA_HIGH_TOXIC,OSHA_IRRITANT,OSHA_ORG_PEROX,OSHA_OTHERLONGTERM,
	OSHA_OXIDIZER,OSHA_PYRO,OSHA_SENSITIZER,OSHA_TOXIC,OSHA_UNST_REACT,
	OTHER_SHORT_TERM,PHYS_STATE_CODE,VOL_ORG_COMP_WT,OSHA_WATER_REACTIVE)
	select phys_chemical_id,msds_id,VAPOR_PRESS,VAPOR_DENS,
	SPECIFIC_GRAV,VOC_POUNDS_GALLON,VOC_GRAMS_LITER,PH,VISCOSITY,EVAP_RATE_REF,
	SOL_IN_WATER,APP_ODOR,PERCENT_VOL_VOLUME,AUTOIGNITION_TEMP,CARCINOGEN_IND,
	EPA_ACUTE,EPA_CHRONIC,EPA_FIRE,EPA_PRESSURE,EPA_REACTIVITY,FLASH_PT_TEMP,
	NEUT_AGENT,NFPA_FLAMMABILITY,NFPA_HEALTH,NFPA_REACTIVITY,NFPA_SPECIAL,
	OSHA_CARCINOGENS,OSHA_COMB_LIQUID,OSHA_COMP_GAS,OSHA_CORROSIVE,OSHA_EXPLOSIVE,
	OSHA_FLAMMABLE,OSHA_HIGH_TOXIC,OSHA_IRRITANT,OSHA_ORG_PEROX,OSHA_OTHERLONGTERM,
	OSHA_OXIDIZER,OSHA_PYRO,OSHA_SENSITIZER,OSHA_TOXIC,OSHA_UNST_REACT,
	OTHER_SHORT_TERM,PHYS_STATE_CODE,VOL_ORG_COMP_WT,OSHA_WATER_REACTIVE 
	from hazmat.dbo.msds_phys_chemical;
set identity_insert msds_phys_chemical off;

set identity_insert msds_radiological_info on;
insert into msds_radiological_info (rad_info_id,NRC_LP_NUM,OPERATOR,
	RAD_AMOUNT_MICRO,RAD_FORM,RAD_CAS,RAD_NAME,RAD_SYMBOL,REP_NSN,
	SEALED,msds_id)
	select rad_info_id,NRC_LP_NUM,OPERATOR,
	RAD_AMOUNT_MICRO,RAD_FORM,RAD_CAS,RAD_NAME,RAD_SYMBOL,REP_NSN,
	SEALED,msds_id 
	from hazmat.dbo.msds_radiological_info;
set identity_insert msds_radiological_info off;

set identity_insert msds_transportation on;
insert into msds_transportation (transportation_id,AF_MMAC_CODE,CERTIFICATE_COE,
	COMPETENT_CAA,DOD_ID_CODE,DOT_EXEMPTION_NO,DOT_RQ_IND,EX_NO,FLASH_PT_TEMP,
	HCC,HIGH_EXPLOSIVE_WT,LTD_QTY_IND,MAGNETIC_IND,MAGNETISM,MARINE_POLLUTANT_IND,
	NET_EXP_QTY_DIST,NET_EXP_WEIGHT,NET_PROPELLANT_WT,NOS_TECHNICAL_SHIPPING_NAME,
	TRANSPORTATION_ADDITIONAL_DATA,msds_id)
	select transportation_id,AF_MMAC_CODE,CERTIFICATE_COE,
	COMPETENT_CAA,DOD_ID_CODE,DOT_EXEMPTION_NO,DOT_RQ_IND,EX_NO,FLASH_PT_TEMP,
	HCC,HIGH_EXPLOSIVE_WT,LTD_QTY_IND,MAGNETIC_IND,MAGNETISM,MARINE_POLLUTANT_IND,
	NET_EXP_QTY_DIST,NET_EXP_WEIGHT,NET_PROPELLANT_WT,NOS_TECHNICAL_SHIPPING_NAME,
	TRANSPORTATION_ADDITIONAL_DATA,msds_id 
	from hazmat.dbo.msds_transportation;
set identity_insert msds_transportation off;

-- MFG Catalog
set identity_insert mfg_catalog on;
insert into mfg_catalog (mfg_catalog_id,cage,manufacturer,aul_id,
	hazard_id,PRODUCT_IDENTITY)
	select mfg_catalog_id,cage,manufacturer,niin_catalog_id,
	hazard_id,PRODUCT_IDENTITY 
	from hazmat.dbo.mfg_catalog;
set identity_insert mfg_catalog off;

-- SFR
set identity_insert sfr on;
insert into sfr (sfr_id,aul_id,sfr_type,action_complete,username,
	POC_Name,POC_Telephone,POC_Email,completed_flag,	fsc,niin,ui,um,usage_category_id,
	description,smcc_id,specs,shelf_life_code_id,shelf_life_action_code_id,
	storage_type_id,cog_id,spmig,nehc_rpt,catalog_group_id,	catalog_serial_number,
	allowance_qty,ship_id,remarks,manufacturer,cage,msds_num,part_no,
	poc_city,poc_address,poc_state,poc_zip,system_equipment_material,method_of_application,
	proposed_usage,negative_impact,special_training,precautions,properties,comments,advantages)
	select sfr_id,niin_catalog_id,sfr_type,action_complete,username,
	POC_Name,POC_Telephone,POC_Email,completed_flag,	fsc,niin,ui,um,usage_category_id,
	description,smcc_id,specs,shelf_life_code_id,shelf_life_action_code_id,
	storage_type_id,cog_id,spmig,nehc_rpt,catalog_group_id,	catalog_serial_number,
	allowance_qty,ship_id,remarks,manufacturer,cage,msds_num,part_no,
	poc_city,poc_address,poc_state,poc_zip,system_equipment_material,method_of_application,
	proposed_usage,negative_impact,special_training,precautions,properties,comments,advantages
	from hazmat.dbo.sfr;
set identity_insert sfr off;
