-- Create the tables for HILT-HUB database
-- Copyright 2011 Applied Enterprise Solutions, All rights reserved

/****** 
	NOTE: this script is intended only for use with the HILT_HUB prototype
	The inventory tables contain the same fields as the master record tables - they 
	will be pared down for the production version
	
	The HILT_HUB_Populate_from_HAZMAT.sql script will populate these tables from the
	current Hazmat database instance.
 ******/
 
-- Table definitions start here
USE [hilt_hub]
GO

-- Utility tables
/****** Object:  Table [dbo].[db_properties]    Script Date: 08/31/2011 14:35:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[db_properties](
	[dbversion] [bigint] NOT NULL
) ON [PRIMARY]

GO

-- Insert the default version - original creation date
INSERT INTO [dbo].[db_properties] VALUES(20110831);
GO

/****** Object:  Table [dbo].[data_sources]    Script Date: 08/17/2011 14:35:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[data_sources](
	[data_source_cd] [nvarchar](10) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_data_sources] PRIMARY KEY CLUSTERED 
(
	[data_source_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- Add Insert statements here for HMIRS, ...
INSERT INTO [dbo].[data_sources] VALUES('HMIRS', 'HMIRS');
GO

-- Support Tables
/****** Object:  Table [dbo].[atmosphere_control]    Script Date: 09/01/2011 08:53:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[atmosphere_control](
	[atmosphere_control_id] [bigint] IDENTITY(1,1) NOT NULL,
	[atmosphere_control_number] [bigint] NOT NULL,
	[cage] [nvarchar](5) NULL,
	[niin] [nvarchar](9) NULL,
	[msds_serial_number] [nvarchar](10) NULL,
	[shelf_life] [datetime] NULL,
 CONSTRAINT [PK_atmosphere_control] PRIMARY KEY CLUSTERED 
(
	[atmosphere_control_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_atm_control_num] UNIQUE NONCLUSTERED 
(
	[atmosphere_control_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[catalog_groups]    Script Date: 09/01/2011 08:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_groups](
	[catalog_group_id] [bigint] IDENTITY(1,1) NOT NULL,
	[group_name] [nvarchar](255) NULL,
 CONSTRAINT [PK_catalog_groups] PRIMARY KEY CLUSTERED 
(
	[catalog_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[cog_codes]    Script Date: 09/01/2011 08:56:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cog_codes](
	[cog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cog] [nvarchar](5) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_cog_codes] PRIMARY KEY CLUSTERED 
(
	[cog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[cosal_niin_allowances]    Script Date: 09/01/2011 08:58:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cosal_niin_allowances](
	[cosal_niin_id] [bigint] IDENTITY(1,1) NOT NULL,
	[COSAL] [nvarchar](5) NULL,
	[NIIN] [nvarchar](9) NULL,
	[AT] [int] NULL,
	[ALLOWANCE_QTY] [int] NULL,
 CONSTRAINT [PK_cosal_niin_allowances] PRIMARY KEY CLUSTERED 
(
	[cosal_niin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_cosal_niin] UNIQUE NONCLUSTERED 
(
	[COSAL] ASC,
	[NIIN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hazard_warnings]    Script Date: 09/01/2011 08:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hazard_warnings](
	[hazard_warning_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hazard_id_1] [bigint] NULL,
	[hazard_id_2] [bigint] NULL,
	[warning_level] [nvarchar](50) NULL,
 CONSTRAINT [PK_hazard_warnings] PRIMARY KEY CLUSTERED 
(
	[hazard_warning_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hazards]    Script Date: 08/18/2011 11:17:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hazards](
	[hazard_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hazard_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_hazards] PRIMARY KEY CLUSTERED 
(
	[hazard_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[hcc]    Script Date: 08/18/2011 10:33:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hcc](
	[hcc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hcc] [nvarchar](5) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_hcc] PRIMARY KEY CLUSTERED 
(
	[hcc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[shelf_life_action_code]    Script Date: 09/01/2011 08:41:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[shelf_life_action_code](
	[shelf_life_action_code_id] [bigint] IDENTITY(1,1) NOT NULL,
	[slac] [nvarchar](2) NULL,
	[description] [text] NULL,
 CONSTRAINT [PK_shelf_life_action_code] PRIMARY KEY CLUSTERED 
(
	[shelf_life_action_code_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[shelf_life_code]    Script Date: 09/01/2011 08:43:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[shelf_life_code](
	[shelf_life_code_id] [bigint] IDENTITY(1,1) NOT NULL,
	[slc] [nvarchar](1) NULL,
	[time_in_months] [int] NULL,
	[description] [nvarchar](50) NULL,
	[type] [nvarchar](50) NULL,
 CONSTRAINT [PK_shelf_life_code] PRIMARY KEY CLUSTERED 
(
	[shelf_life_code_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[ship_statuses]    Script Date: 09/01/2011 08:43:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ship_statuses](
	[status_id] [bigint] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](20) NULL,
 CONSTRAINT [PK_ship_statuses] PRIMARY KEY CLUSTERED 
(
	[status_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[smcc]    Script Date: 09/01/2011 08:45:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[smcc](
	[smcc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[smcc] [nvarchar](5) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_smcc] PRIMARY KEY CLUSTERED 
(
	[smcc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[storage_type]    Script Date: 09/01/2011 13:10:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[storage_type](
	[storage_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](5) NULL,
	[description] [text] NULL,
 CONSTRAINT [PK_storage_type] PRIMARY KEY CLUSTERED 
(
	[storage_type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[usage_category]    Script Date: 09/01/2011 08:48:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[usage_category](
	[usage_category_id] [bigint] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](5) NULL,
	[description] [nvarchar](50) NULL,
 CONSTRAINT [PK_usage_category] PRIMARY KEY CLUSTERED 
(
	[usage_category_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


-- Ships tables
/****** Object:  Table [dbo].[ships]    Script Date: 08/17/2011 14:35:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ships](
	[ship_id] [bigint] IDENTITY(1,1) NOT NULL,
	[current_status] [bigint] NULL,
	[name] [nvarchar](50) NULL,
	[uic] [nvarchar](10) NULL,
	[hull_type] [nvarchar](10) NULL,
	[hull_number] [nvarchar](10) NULL,
	[POC_Name] [nvarchar](50) NULL,
	[POC_Telephone] [nvarchar](50) NULL,
	[POC_Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_ships] PRIMARY KEY CLUSTERED 
(
	[ship_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- MSSL tables
/****** Object:  Table [dbo].[mssl_master]    Script Date: 08/17/2011 14:26:08 
 Holds the set of valid NIIN reocrds - hopefully we will get this from Rsupply, but may need to build it
 as we go along ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mssl_master](
	[mssl_master_id] [bigint] IDENTITY(1,1) NOT NULL,
	[niin] [nvarchar](50) NULL,
	[ati] [nvarchar](50) NULL,
	[cog] [nvarchar](50) NULL,
	[mcc] [nvarchar](50) NULL,
	[ui] [nvarchar](50) NULL,
	[up] [decimal](18, 2) NULL,
	[netup] [decimal](18, 2) NULL,
	[lmc] [nvarchar](50) NULL,
	[irc] [nvarchar](50) NULL,
	[dues] [bigint] NULL,
	[ro] [bigint] NULL,
	[rp] [bigint] NULL,
	[amd] [decimal](18, 2) NULL,
	[smic] [nvarchar](50) NULL,
	[slc] [nvarchar](50) NULL,
	[slac] [nvarchar](50) NULL,
	[smcc] [nvarchar](50) NULL,
	[nomenclature] [nvarchar](50) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_mssl_master] PRIMARY KEY CLUSTERED 
(
	[mssl_master_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[mssl_inventory]    Script Date: 08/17/2011 14:26:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mssl_inventory](
	[mssl_inventory_id] [bigint] IDENTITY(1,1) NOT NULL,
	[uic] [nvarchar](10) NULL,
	[niin] [nvarchar](50) NULL,
	[ati] [nvarchar](50) NULL,
	[cog] [nvarchar](50) NULL,
	[mcc] [nvarchar](50) NULL,
	[ui] [nvarchar](50) NULL,
	[up] [decimal](18, 2) NULL,
	[netup] [decimal](18, 2) NULL,
	[location] [nvarchar](50) NULL,
	[qty] [bigint] NULL,
	[lmc] [nvarchar](50) NULL,
	[irc] [nvarchar](50) NULL,
	[dues] [bigint] NULL,
	[ro] [bigint] NULL,
	[rp] [bigint] NULL,
	[amd] [decimal](18, 2) NULL,
	[smic] [nvarchar](50) NULL,
	[slc] [nvarchar](50) NULL,
	[slac] [nvarchar](50) NULL,
	[smcc] [nvarchar](50) NULL,
	[nomenclature] [nvarchar](50) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_mssl_inventory] PRIMARY KEY CLUSTERED 
(
	[mssl_inventory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- AUL tables
/****** Object:  Table [dbo].[auth_use_list]    Script Date: 08/18/2011 08:36:05 Called niin_catalog in hazmat ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[auth_use_list](
	[aul_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hull_type] [nvarchar](10) NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](9) NULL,
	[ui] [nvarchar](50) NULL,
	[um] [nvarchar](50) NULL,
	[usage_category_id] [bigint] NULL,
	[description] [nvarchar](1024) NULL,
	[smcc_id] [bigint] NULL,
	[specs] [nvarchar](50) NULL,
	[shelf_life_code_id] [bigint] NULL,
	[shelf_life_action_code_id] [bigint] NULL,
	[remarks] [text] NULL,
	[storage_type_id] [bigint] NULL,
	[cog_id] [bigint] NULL,
	[spmig] [nvarchar](255) NULL,
	[nehc_rpt] [nvarchar](10) NULL,
	[catalog_group_id] [bigint] NULL,
	[catalog_serial_number] [nvarchar](255) NULL,
	[allowance_qty] [int] NULL,
	[manually_entered] [bit] NULL,
	[dropped_in_error] [bit] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](255) NULL,
	[msds_num] [nvarchar](255) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_aul_id] PRIMARY KEY CLUSTERED 
(
	[aul_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_aulniin] UNIQUE NONCLUSTERED 
(
	[niin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'federal supply class - 4 digit number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'auth_use_list', @level2type=N'COLUMN',@level2name=N'fsc'
GO

/****** Object:  Table [dbo].[aul_inventory]    Script Date: 08/18/2011 08:36:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aul_inventory](
	[aul_inventory_id] [bigint] IDENTITY(1,1) NOT NULL,
	[uic] [nvarchar](10) NULL,
	[hull_type] [nvarchar](10) NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](9) NULL,
	[ui] [nvarchar](50) NULL,
	[um] [nvarchar](50) NULL,
	[usage_category_id] [bigint] NULL,
	[description] [nvarchar](1024) NULL,
	[smcc_id] [bigint] NULL,
	[specs] [nvarchar](50) NULL,
	[shelf_life_code_id] [bigint] NULL,
	[shelf_life_action_code_id] [bigint] NULL,
	[remarks] [text] NULL,
	[storage_type_id] [bigint] NULL,
	[cog_id] [bigint] NULL,
	[spmig] [nvarchar](255) NULL,
	[nehc_rpt] [nvarchar](10) NULL,
	[catalog_group_id] [bigint] NULL,
	[catalog_serial_number] [nvarchar](255) NULL,
	[qty] [int] NULL,
	[allowance_qty] [int] NULL,
	[manually_entered] [bit] NULL,
	[dropped_in_error] [bit] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](255) NULL,
	[msds_num] [nvarchar](255) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_aul_inventory] PRIMARY KEY CLUSTERED 
(
	[aul_inventory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- MSDS tables
/****** Object:  Table [dbo].[msds_master]    Script Date: 08/18/2011 10:31:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_master](
	[msds_id] [bigint] IDENTITY(1,1) NOT NULL,
	[MSDSSERNO] [nvarchar](10) NULL,
	[CAGE] [nvarchar](5) NULL,
	[MANUFACTURER] [nvarchar](255) NULL,
	[PARTNO] [nvarchar](100) NULL,
	[FSC] [nvarchar](18) NULL,
	[NIIN] [nvarchar](9) NULL,
	[hcc_id] [bigint] NULL,
	[msds_file] [image] NULL,
	[file_name] [nvarchar](255) NULL,
	[manually_entered] [bit] NULL,
	[ARTICLE_IND] [nchar](10) NULL,
	[DESCRIPTION] [ntext] NULL,
	[EMERGENCY_TEL] [nvarchar](50) NULL,
	[END_COMP_IND] [nchar](10) NULL,
	[END_ITEM_IND] [nchar](10) NULL,
	[KIT_IND] [nchar](10) NULL,
	[KIT_PART_IND] [nchar](10) NULL,
	[MANUFACTURER_MSDS_NO] [nvarchar](50) NULL,
	[MIXTURE_IND] [nchar](10) NULL,
	[PRODUCT_IDENTITY] [nvarchar](255) NULL,
	[PRODUCT_LOAD_DATE] [smalldatetime] NULL,
	[PRODUCT_RECORD_STATUS] [nchar](10) NULL,
	[PRODUCT_REVISION_NO] [nvarchar](18) NULL,
	[PROPRIETARY_IND] [nchar](1) NULL,
	[PUBLISHED_IND] [nchar](1) NULL,
	[PURCHASED_PROD_IND] [nchar](1) NULL,
	[PURE_IND] [nchar](1) NULL,
	[RADIOACTIVE_IND] [nchar](1) NULL,
	[SERVICE_AGENCY] [nvarchar](10) NULL,
	[TRADE_NAME] [nvarchar](255) NULL,
	[TRADE_SECRET_IND] [nchar](1) NULL,
	[PRODUCT_IND] [nvarchar](1) NULL,
	[PRODUCT_LANGUAGE] [nvarchar](10) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_msds] PRIMARY KEY CLUSTERED 
(
	[msds_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_master]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_hcc] FOREIGN KEY([hcc_id])
REFERENCES [dbo].[hcc] ([hcc_id])
GO

ALTER TABLE [dbo].[msds_master] CHECK CONSTRAINT [FK_msds_hcc]
GO

/****** Object:  Table [dbo].[msds_inventory]    Script Date: 08/18/2011 10:31:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_inventory](
	[msds_inventory_id] [bigint] IDENTITY(1,1) NOT NULL,
	[MSDSSERNO] [nvarchar](10) NULL,
	[CAGE] [nvarchar](5) NULL,
	[uic] [nvarchar](10) NULL,
	[MANUFACTURER] [nvarchar](255) NULL,
	[PARTNO] [nvarchar](100) NULL,
	[FSC] [nvarchar](18) NULL,
	[NIIN] [nvarchar](9) NULL,
	[hcc_id] [bigint] NULL,
	[file_name] [nvarchar](255) NULL,
	[manually_entered] [bit] NULL,
	[ARTICLE_IND] [nchar](10) NULL,
	[DESCRIPTION] [ntext] NULL,
	[EMERGENCY_TEL] [nvarchar](50) NULL,
	[END_COMP_IND] [nchar](10) NULL,
	[END_ITEM_IND] [nchar](10) NULL,
	[KIT_IND] [nchar](10) NULL,
	[KIT_PART_IND] [nchar](10) NULL,
	[MANUFACTURER_MSDS_NO] [nvarchar](50) NULL,
	[MIXTURE_IND] [nchar](10) NULL,
	[PRODUCT_IDENTITY] [nvarchar](255) NULL,
	[PRODUCT_LOAD_DATE] [smalldatetime] NULL,
	[PRODUCT_RECORD_STATUS] [nchar](10) NULL,
	[PRODUCT_REVISION_NO] [nvarchar](18) NULL,
	[PROPRIETARY_IND] [nchar](1) NULL,
	[PUBLISHED_IND] [nchar](1) NULL,
	[PURCHASED_PROD_IND] [nchar](1) NULL,
	[PURE_IND] [nchar](1) NULL,
	[RADIOACTIVE_IND] [nchar](1) NULL,
	[SERVICE_AGENCY] [nvarchar](10) NULL,
	[TRADE_NAME] [nvarchar](255) NULL,
	[TRADE_SECRET_IND] [nchar](1) NULL,
	[PRODUCT_IND] [nvarchar](1) NULL,
	[PRODUCT_LANGUAGE] [nvarchar](10) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
 CONSTRAINT [PK_msds_inventory_id] PRIMARY KEY CLUSTERED 
(
	[msds_inventory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


GO

ALTER TABLE [dbo].[msds_inventory]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_inventory_hcc] FOREIGN KEY([hcc_id])
REFERENCES [dbo].[hcc] ([hcc_id])
GO

ALTER TABLE [dbo].[msds_inventory] CHECK CONSTRAINT [FK_msds_inventory_hcc]
GO

/****** Object:  Table [dbo].[msds_afjm_psn]    Script Date: 08/18/2011 10:51:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_afjm_psn](
	[afjm_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AFJM_HAZARD_CLASS] [nvarchar](10) NULL,
	[AFJM_PACK_PARAGRAPH] [nvarchar](30) NULL,
	[AFJM_PACK_GROUP] [nvarchar](10) NULL,
	[AFJM_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[AFJM_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[AFJM_PSN_CODE] [nvarchar](10) NULL,
	[AFJM_SPECIAL_PROV] [nvarchar](100) NULL,
	[AFJM_SUBSIDIARY_RISK] [nvarchar](10) NULL,
	[AFJM_SYMBOLS] [nvarchar](10) NULL,
	[AFJM_UN_ID_NUMBER] [nvarchar](10) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_afjm_psn] PRIMARY KEY CLUSTERED 
(
	[afjm_psn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_afjm_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_afjm_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_afjm_psn] CHECK CONSTRAINT [FK_msds_afjm_psn_msds]
GO

/****** Object:  Table [dbo].[msds_contractor_info]    Script Date: 08/18/2011 10:52:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_contractor_info](
	[contractor_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CT_NUMBER] [ntext] NULL,
	[CT_CAGE] [ntext] NULL,
	[CT_CITY] [ntext] NULL,
	[CT_COMPANY_NAME] [ntext] NULL,
	[CT_COUNTRY] [nvarchar](255) NULL,
	[CT_PO_BOX] [nvarchar](255) NULL,
	[CT_PHONE] [ntext] NULL,
	[PURCHASE_ORDER_NO] [nvarchar](255) NULL,
	[msds_id] [bigint] NULL,
	[CT_STATE] [nvarchar](255) NULL,
 CONSTRAINT [PK_msds_contractor_info] PRIMARY KEY CLUSTERED 
(
	[contractor_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_contractor_info]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_contractor_info_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_contractor_info] CHECK CONSTRAINT [FK_msds_contractor_info_msds]
GO

/****** Object:  Table [dbo].[msds_disposal]    Script Date: 08/18/2011 10:54:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_disposal](
	[disp_info_id] [bigint] IDENTITY(1,1) NOT NULL,
	[DISPOSAL_ADD_INFO] [ntext] NULL,
	[EPA_HAZ_WASTE_CODE] [nvarchar](10) NULL,
	[EPA_HAZ_WASTE_IND] [nchar](1) NULL,
	[EPA_HAZ_WASTE_NAME] [nvarchar](100) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_disposal] PRIMARY KEY CLUSTERED 
(
	[disp_info_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_disposal]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_disposal_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_disposal] CHECK CONSTRAINT [FK_msds_disposal_msds]
GO

/****** Object:  Table [dbo].[msds_document_types]    Script Date: 08/18/2011 10:54:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_document_types](
	[doc_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[MSDS_TRANSLATED] [image] NULL,
	[NESHAP_COMP_CERT] [image] NULL,
	[OTHER_DOCS] [image] NULL,
	[PRODUCT_SHEET] [image] NULL,
	[TRANSPORTATION_CERT] [image] NULL,
	[msds_translated_filename] [nvarchar](255) NULL,
	[neshap_comp_filename] [nvarchar](255) NULL,
	[other_docs_filename] [nvarchar](255) NULL,
	[product_sheet_filename] [nvarchar](255) NULL,
	[transportation_cert_filename] [nvarchar](255) NULL,
	[MANUFACTURER_LABEL] [image] NULL,
	[manufacturer_label_filename] [nvarchar](255) NULL,
 CONSTRAINT [PK_msds_document_types] PRIMARY KEY CLUSTERED 
(
	[doc_type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_document_types]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_document_types_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_document_types] CHECK CONSTRAINT [FK_msds_document_types_msds]
GO

/****** Object:  Table [dbo].[msds_dot_psn]    Script Date: 08/18/2011 10:54:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_dot_psn](
	[dot_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[DOT_HAZARD_CLASS_DIV] [nvarchar](10) NULL,
	[DOT_HAZARD_LABEL] [nvarchar](100) NULL,
	[DOT_MAX_CARGO] [nvarchar](20) NULL,
	[DOT_MAX_PASSENGER] [nvarchar](20) NULL,
	[DOT_PACK_BULK] [nvarchar](20) NULL,
	[DOT_PACK_EXCEPTIONS] [nvarchar](30) NULL,
	[DOT_PACK_NONBULK] [nvarchar](20) NULL,
	[DOT_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[DOT_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[DOT_PSN_CODE] [nvarchar](10) NULL,
	[DOT_SPECIAL_PROVISION] [nvarchar](100) NULL,
	[DOT_SYMBOLS] [nvarchar](10) NULL,
	[DOT_UN_ID_NUMBER] [nvarchar](10) NULL,
	[DOT_WATER_OTHER_REQ] [nvarchar](30) NULL,
	[DOT_WATER_VESSEL_STOW] [nvarchar](20) NULL,
	[msds_id] [bigint] NULL,
	[DOT_PACK_GROUP] [nvarchar](10) NULL,
 CONSTRAINT [PK_msds_dot_psn] PRIMARY KEY CLUSTERED 
(
	[dot_psn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_dot_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_dot_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_dot_psn] CHECK CONSTRAINT [FK_msds_dot_psn_msds]
GO

/****** Object:  Table [dbo].[msds_iata_psn]    Script Date: 08/18/2011 10:55:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_iata_psn](
	[iata_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[IATA_CARGO_PACKING] [nvarchar](20) NULL,
	[IATA_HAZARD_CLASS] [nvarchar](10) NULL,
	[IATA_HAZARD_LABEL] [nvarchar](100) NULL,
	[IATA_PACK_GROUP] [nvarchar](10) NULL,
	[IATA_PASS_AIR_PACK_LMT_INSTR] [nvarchar](30) NULL,
	[IATA_PASS_AIR_PACK_LMT_PER_PKG] [nvarchar](20) NULL,
	[IATA_PASS_AIR_PACK_NOTE] [nvarchar](20) NULL,
	[IATA_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[IATA_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[IATA_CARGO_PACK_MAX_QTY] [nvarchar](20) NULL,
	[IATA_PSN_CODE] [nvarchar](20) NULL,
	[IATA_PASS_AIR_MAX_QTY] [nvarchar](20) NULL,
	[IATA_SPECIAL_PROV] [nvarchar](30) NULL,
	[IATA_SUBSIDIARY_RISK] [nvarchar](50) NULL,
	[IATA_UN_ID_NUMBER] [nvarchar](50) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_iata_psn] PRIMARY KEY CLUSTERED 
(
	[iata_psn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_iata_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_iata_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_iata_psn] CHECK CONSTRAINT [FK_msds_iata_psn_msds]
GO

/****** Object:  Table [dbo].[msds_imo_psn]    Script Date: 08/18/2011 10:55:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_imo_psn](
	[imo_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[IMO_EMS_NO] [nvarchar](30) NULL,
	[IMO_HAZARD_CLASS] [nvarchar](10) NULL,
	[IMO_IBC_INSTR] [nvarchar](10) NULL,
	[IMO_LIMITED_QTY] [nvarchar](10) NULL,
	[IMO_PACK_GROUP] [nvarchar](10) NULL,
	[IMO_PACK_INSTRUCTIONS] [nvarchar](10) NULL,
	[IMO_PACK_PROVISIONS] [nvarchar](30) NULL,
	[IMO_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[IMO_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[IMO_PSN_CODE] [nvarchar](10) NULL,
	[IMO_SPECIAL_PROV] [nvarchar](100) NULL,
	[IMO_STOW_SEGR] [nvarchar](15) NULL,
	[IMO_SUBSIDIARY_RISK] [nvarchar](30) NULL,
	[IMO_TANK_INSTR_IMO] [nvarchar](10) NULL,
	[IMO_TANK_INSTR_PROV] [nvarchar](30) NULL,
	[IMO_TANK_INSTR_UN] [nvarchar](10) NULL,
	[IMO_UN_NUMBER] [nvarchar](10) NULL,
	[msds_id] [bigint] NULL,
	[IMO_IBC_PROVISIONS] [nvarchar](30) NULL,
 CONSTRAINT [PK_msds_imo_psn] PRIMARY KEY CLUSTERED 
(
	[imo_psn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_imo_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_imo_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_imo_psn] CHECK CONSTRAINT [FK_msds_imo_psn_msds]
GO

/****** Object:  Table [dbo].[msds_ingredients]    Script Date: 08/18/2011 10:55:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_ingredients](
	[ingredients_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[CAS] [nvarchar](255) NULL,
	[RTECS_NUM] [nvarchar](255) NULL,
	[RTECS_CODE] [nvarchar](255) NULL,
	[INGREDIENT_NAME] [ntext] NULL,
	[PRCNT] [nvarchar](255) NULL,
	[OSHA_PEL] [nvarchar](255) NULL,
	[OSHA_STEL] [nvarchar](255) NULL,
	[ACGIH_TLV] [nvarchar](255) NULL,
	[ACGIH_STEL] [nvarchar](255) NULL,
	[EPA_REPORT_QTY] [nvarchar](255) NULL,
	[DOT_REPORT_QTY] [nvarchar](255) NULL,
	[PRCNT_VOL_VALUE] [nvarchar](255) NULL,
	[PRCNT_VOL_WEIGHT] [nvarchar](255) NULL,
	[CHEM_MFG_COMP_NAME] [nvarchar](255) NULL,
	[ODS_IND] [nvarchar](255) NULL,
	[OTHER_REC_LIMITS] [nvarchar](255) NULL,
 CONSTRAINT [PK_msds_ingredients] PRIMARY KEY CLUSTERED 
(
	[ingredients_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_ingredients]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_ingredients_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_ingredients] CHECK CONSTRAINT [FK_msds_ingredients_msds]
GO

/****** Object:  Table [dbo].[msds_item_description]    Script Date: 08/18/2011 10:56:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_item_description](
	[item_description_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[ITEM_MANAGER] [nvarchar](10) NULL,
	[ITEM_NAME] [nvarchar](100) NULL,
	[SPECIFICATION_NUMBER] [nvarchar](20) NULL,
	[TYPE_GRADE_CLASS] [nvarchar](20) NULL,
	[UNIT_OF_ISSUE] [nvarchar](10) NULL,
	[QUANTITATIVE_EXPRESSION] [nvarchar](15) NULL,
	[UI_CONTAINER_QTY] [nvarchar](15) NULL,
	[TYPE_OF_CONTAINER] [nvarchar](15) NULL,
	[BATCH_NUMBER] [nvarchar](50) NULL,
	[LOT_NUMBER] [nvarchar](50) NULL,
	[LOG_FLIS_NIIN_VER] [nchar](1) NULL,
	[LOG_FSC] [numeric](18, 0) NULL,
	[NET_UNIT_WEIGHT] [nvarchar](15) NULL,
	[SHELF_LIFE_CODE] [nvarchar](10) NULL,
	[SPECIAL_EMP_CODE] [nvarchar](10) NULL,
	[UN_NA_NUMBER] [nvarchar](10) NULL,
	[UPC_GTIN] [nvarchar](15) NULL,
 CONSTRAINT [PK_msds_item_description] PRIMARY KEY CLUSTERED 
(
	[item_description_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_item_description]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_item_description_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_item_description] CHECK CONSTRAINT [FK_msds_item_description_msds]
GO

/****** Object:  Table [dbo].[msds_label_info]    Script Date: 08/18/2011 10:56:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_label_info](
	[label_info_id] [bigint] IDENTITY(1,1) NOT NULL,
	[COMPANY_CAGE_RP] [nvarchar](10) NULL,
	[COMPANY_NAME_RP] [nvarchar](255) NULL,
	[LABEL_EMERG_PHONE] [nvarchar](50) NULL,
	[LABEL_ITEM_NAME] [nvarchar](100) NULL,
	[LABEL_PROC_YEAR] [nvarchar](10) NULL,
	[LABEL_PROD_IDENT] [nvarchar](100) NULL,
	[LABEL_PROD_SERIALNO] [nvarchar](10) NULL,
	[LABEL_SIGNAL_WORD] [nvarchar](10) NULL,
	[LABEL_STOCK_NO] [nvarchar](15) NULL,
	[SPECIFIC_HAZARDS] [ntext] NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_label_info] PRIMARY KEY CLUSTERED 
(
	[label_info_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_label_info]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_label_info_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_label_info] CHECK CONSTRAINT [FK_msds_label_info_msds]
GO

/****** Object:  Table [dbo].[msds_phys_chemical]    Script Date: 08/18/2011 10:56:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_phys_chemical](
	[phys_chemical_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[VAPOR_PRESS] [nvarchar](30) NULL,
	[VAPOR_DENS] [nvarchar](10) NULL,
	[SPECIFIC_GRAV] [nvarchar](30) NULL,
	[VOC_POUNDS_GALLON] [nvarchar](50) NULL,
	[VOC_GRAMS_LITER] [nvarchar](50) NULL,
	[PH] [nvarchar](15) NULL,
	[VISCOSITY] [nvarchar](30) NULL,
	[EVAP_RATE_REF] [nvarchar](30) NULL,
	[SOL_IN_WATER] [nvarchar](30) NULL,
	[APP_ODOR] [nvarchar](255) NULL,
	[PERCENT_VOL_VOLUME] [nvarchar](15) NULL,
	[AUTOIGNITION_TEMP] [nvarchar](18) NULL,
	[CARCINOGEN_IND] [nchar](1) NULL,
	[EPA_ACUTE] [nchar](1) NULL,
	[EPA_CHRONIC] [nchar](1) NULL,
	[EPA_FIRE] [nchar](1) NULL,
	[EPA_PRESSURE] [nchar](1) NULL,
	[EPA_REACTIVITY] [nchar](1) NULL,
	[FLASH_PT_TEMP] [nvarchar](18) NULL,
	[NEUT_AGENT] [nvarchar](255) NULL,
	[NFPA_FLAMMABILITY] [nvarchar](10) NULL,
	[NFPA_HEALTH] [nvarchar](50) NULL,
	[NFPA_REACTIVITY] [nvarchar](50) NULL,
	[NFPA_SPECIAL] [nvarchar](50) NULL,
	[OSHA_CARCINOGENS] [nchar](1) NULL,
	[OSHA_COMB_LIQUID] [nchar](1) NULL,
	[OSHA_COMP_GAS] [nchar](1) NULL,
	[OSHA_CORROSIVE] [nchar](1) NULL,
	[OSHA_EXPLOSIVE] [nchar](1) NULL,
	[OSHA_FLAMMABLE] [nchar](1) NULL,
	[OSHA_HIGH_TOXIC] [nchar](1) NULL,
	[OSHA_IRRITANT] [nchar](1) NULL,
	[OSHA_ORG_PEROX] [nchar](1) NULL,
	[OSHA_OTHERLONGTERM] [nchar](1) NULL,
	[OSHA_OXIDIZER] [nchar](1) NULL,
	[OSHA_PYRO] [nchar](1) NULL,
	[OSHA_SENSITIZER] [nchar](1) NULL,
	[OSHA_TOXIC] [nchar](1) NULL,
	[OSHA_UNST_REACT] [nchar](1) NULL,
	[OTHER_SHORT_TERM] [nvarchar](20) NULL,
	[PHYS_STATE_CODE] [nchar](10) NULL,
	[VOL_ORG_COMP_WT] [nvarchar](18) NULL,
	[OSHA_WATER_REACTIVE] [nchar](1) NULL,
 CONSTRAINT [PK_msds_phys_chemical] PRIMARY KEY CLUSTERED 
(
	[phys_chemical_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_phys_chemical]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_phys_chemical_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_phys_chemical] CHECK CONSTRAINT [FK_msds_phys_chemical_msds]
GO

/****** Object:  Table [dbo].[msds_radiological_info]    Script Date: 08/18/2011 10:57:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_radiological_info](
	[rad_info_id] [bigint] IDENTITY(1,1) NOT NULL,
	[NRC_LP_NUM] [nvarchar](30) NULL,
	[OPERATOR] [nvarchar](10) NULL,
	[RAD_AMOUNT_MICRO] [nvarchar](22) NULL,
	[RAD_FORM] [nvarchar](50) NULL,
	[RAD_CAS] [nvarchar](20) NULL,
	[RAD_NAME] [nvarchar](255) NULL,
	[RAD_SYMBOL] [nvarchar](10) NULL,
	[REP_NSN] [nvarchar](15) NULL,
	[SEALED] [nchar](1) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_radiological_info] PRIMARY KEY CLUSTERED 
(
	[rad_info_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_radiological_info]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_radiological_info_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_radiological_info] CHECK CONSTRAINT [FK_msds_radiological_info_msds]
GO

/****** Object:  Table [dbo].[msds_transportation]    Script Date: 08/18/2011 10:57:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[msds_transportation](
	[transportation_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AF_MMAC_CODE] [nvarchar](10) NULL,
	[CERTIFICATE_COE] [nvarchar](15) NULL,
	[COMPETENT_CAA] [nvarchar](15) NULL,
	[DOD_ID_CODE] [nvarchar](30) NULL,
	[DOT_EXEMPTION_NO] [nvarchar](15) NULL,
	[DOT_RQ_IND] [nchar](1) NULL,
	[EX_NO] [nvarchar](15) NULL,
	[FLASH_PT_TEMP] [numeric](18, 0) NULL,
	[HCC] [nvarchar](10) NULL,
	[HIGH_EXPLOSIVE_WT] [numeric](18, 0) NULL,
	[LTD_QTY_IND] [nchar](1) NULL,
	[MAGNETIC_IND] [nchar](1) NULL,
	[MAGNETISM] [nvarchar](50) NULL,
	[MARINE_POLLUTANT_IND] [nchar](1) NULL,
	[NET_EXP_QTY_DIST] [numeric](18, 0) NULL,
	[NET_EXP_WEIGHT] [nvarchar](30) NULL,
	[NET_PROPELLANT_WT] [nvarchar](30) NULL,
	[NOS_TECHNICAL_SHIPPING_NAME] [nvarchar](255) NULL,
	[TRANSPORTATION_ADDITIONAL_DATA] [ntext] NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_transportation] PRIMARY KEY CLUSTERED 
(
	[transportation_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[msds_transportation]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_transportation_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds_master] ([msds_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[msds_transportation] CHECK CONSTRAINT [FK_msds_transportation_msds]
GO

/****** Object:  Table [dbo].[mfg_catalog]    Script Date: 08/18/2011 10:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[mfg_catalog](
	[mfg_catalog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cage] [nvarchar](5) NULL,
	[manufacturer] [nvarchar](255) NULL,
	[aul_id] [bigint] NULL,
	[hazard_id] [bigint] NULL,
	[PRODUCT_IDENTITY] [nvarchar](255) NULL,
 CONSTRAINT [PK_mfg_catalog] PRIMARY KEY CLUSTERED 
(
	[mfg_catalog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[mfg_catalog]  WITH NOCHECK ADD  CONSTRAINT [FK_mfg_catalog_hazards] FOREIGN KEY([hazard_id])
REFERENCES [dbo].[hazards] ([hazard_id])
GO

ALTER TABLE [dbo].[mfg_catalog] CHECK CONSTRAINT [FK_mfg_catalog_hazards]
GO

ALTER TABLE [dbo].[mfg_catalog]  WITH NOCHECK ADD  CONSTRAINT [FK_mfg_catalog_auth_use_list] FOREIGN KEY([aul_id])
REFERENCES [dbo].[auth_use_list] ([aul_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[mfg_catalog] CHECK CONSTRAINT [FK_mfg_catalog_auth_use_list]
GO

-- ATM tables
/****** Object:  Table [dbo].[atmosphere_control]    Script Date: 08/18/2011 10:58:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[atmosphere_control](
	[atmosphere_control_id] [bigint] IDENTITY(1,1) NOT NULL,
	[atmosphere_control_number] [bigint] NOT NULL,
	[cage] [nvarchar](5) NULL,
	[niin] [nvarchar](9) NULL,
	[msds_serial_number] [nvarchar](10) NULL,
	[shelf_life] [datetime] NULL,
 CONSTRAINT [PK_atmosphere_control] PRIMARY KEY CLUSTERED 
(
	[atmosphere_control_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_atm_control_num] UNIQUE NONCLUSTERED 
(
	[atmosphere_control_number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


-- User tables
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 08/18/2011 12:28:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) NOT NULL,
	[LoweredApplicationName] [nvarchar](256) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK__aspnet_A__C93A4C98031C6FA4] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__aspnet_A__17477DE405F8DC4F] UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__aspnet_A__3091033108D548FA] UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aspnet_Applications] ADD  CONSTRAINT [DF__aspnet_Ap__Appli__0ABD916C]  DEFAULT (newid()) FOR [ApplicationId]
GO

/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 08/18/2011 12:26:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_U__1788CC4D0D99FE17] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[aspnet_Users]  WITH NOCHECK ADD  CONSTRAINT [FK__aspnet_Us__Appli__0F824689] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO

ALTER TABLE [dbo].[aspnet_Users] CHECK CONSTRAINT [FK__aspnet_Us__Appli__0F824689]
GO

ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__UserI__10766AC2]  DEFAULT (newid()) FOR [UserId]
GO

ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__Mobil__116A8EFB]  DEFAULT (null) FOR [MobileAlias]
GO

ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__IsAno__125EB334]  DEFAULT (0) FOR [IsAnonymous]
GO

/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 08/18/2011 12:30:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[MobilePIN] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[LoweredEmail] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] NULL,
 CONSTRAINT [PK__aspnet_M__1788CC4D21A0F6C4] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[aspnet_Membership]  WITH NOCHECK ADD  CONSTRAINT [FK__aspnet_Me__Appli__23893F36] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO

ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__Appli__23893F36]
GO

ALTER TABLE [dbo].[aspnet_Membership]  WITH NOCHECK ADD  CONSTRAINT [FK__aspnet_Me__UserI__247D636F] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__UserI__247D636F]
GO

ALTER TABLE [dbo].[aspnet_Membership] ADD  CONSTRAINT [DF__aspnet_Me__Passw__257187A8]  DEFAULT (0) FOR [PasswordFormat]
GO

/****** Object:  Table [dbo].[user_roles]    Script Date: 08/18/2011 12:24:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[user_roles](
	[user_role_id] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[role] [nvarchar](50) NULL,
 CONSTRAINT [PK_user_roles] PRIMARY KEY CLUSTERED 
(
	[user_role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[authorized_users]    Script Date: 08/18/2011 12:21:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[authorized_users](
	[authorized_user_id] [bigint] IDENTITY(1,1) NOT NULL,
	[workcenter_id] [bigint] NULL,
	[firstname] [nvarchar](50) NULL,
	[lastname] [nvarchar](50) NULL,
	[deleted] [bit] NULL,
	[username] [nvarchar](255) NULL,
 CONSTRAINT [PK_authorized_users] PRIMARY KEY CLUSTERED 
(
	[authorized_user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[authorized_users]  WITH NOCHECK ADD  CONSTRAINT [FK_authorized_users_workcenter] FOREIGN KEY([workcenter_id])
REFERENCES [dbo].[workcenter] ([workcenter_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[authorized_users] CHECK CONSTRAINT [FK_authorized_users_workcenter]
GO

-- History Tables
-- MSSL
/****** Object:  Table [dbo].[AUDIT_mssl_master]    Script Date: 08/17/2011 14:26:08 
 Holds the set of valid NIIN reocrds - hopefully we will get this from Rsupply, but may need to build it
 as we go along ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_mssl_master](
	[mssl_master_id] [bigint] NOT NULL,
	[niin] [nvarchar](50) NULL,
	[ati] [nvarchar](50) NULL,
	[cog] [nvarchar](50) NULL,
	[mcc] [nvarchar](50) NULL,
	[ui] [nvarchar](50) NULL,
	[up] [decimal](18, 2) NULL,
	[netup] [decimal](18, 2) NULL,
	[lmc] [nvarchar](50) NULL,
	[irc] [nvarchar](50) NULL,
	[dues] [bigint] NULL,
	[ro] [bigint] NULL,
	[rp] [bigint] NULL,
	[amd] [decimal](18, 2) NULL,
	[smic] [nvarchar](50) NULL,
	[slc] [nvarchar](50) NULL,
	[slac] [nvarchar](50) NULL,
	[smcc] [nvarchar](50) NULL,
	[nomenclature] [nvarchar](50) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AUDIT_mssl_inventory]    Script Date: 08/17/2011 14:26:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_mssl_inventory](
	[mssl_inventory_id] [bigint] NOT NULL,
	[uic] [nvarchar](10) NULL,
	[niin] [nvarchar](50) NULL,
	[location] [nvarchar](50) NULL,
	[qty] [bigint] NULL,
	[created] [datetime] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO


-- AUL
/****** Object:  Table [dbo].[AUDIT_auth_use_list]    Script Date: 08/18/2011 13:25:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_auth_use_list](
	[aul_id] [bigint] NOT NULL,
	[hull_type] [nvarchar](10) NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](9) NULL,
	[ui] [nvarchar](50) NULL,
	[um] [nvarchar](50) NULL,
	[usage_category_id] [bigint] NULL,
	[description] [nvarchar](1024) NULL,
	[smcc_id] [bigint] NULL,
	[specs] [nvarchar](50) NULL,
	[shelf_life_code_id] [bigint] NULL,
	[shelf_life_action_code_id] [bigint] NULL,
	[remarks] [text] NULL,
	[storage_type_id] [bigint] NULL,
	[cog_id] [bigint] NULL,
	[spmig] [nvarchar](255) NULL,
	[nehc_rpt] [nvarchar](10) NULL,
	[catalog_group_id] [bigint] NULL,
	[catalog_serial_number] [nvarchar](255) NULL,
	[allowance_qty] [int] NULL,
	[manually_entered] [bit] NULL,
	[dropped_in_error] [bit] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](255) NULL,
	[msds_num] [nvarchar](255) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AUDIT_aul_inventory]    Script Date: 08/18/2011 08:36:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_aul_inventory](
	[aul_inventory_id] [bigint] NOT NULL,
	[uic] [nvarchar](10) NULL,
	[niin] [nvarchar](9) NULL,
	[qty] [int] NULL,
	[allowance_qty] [int] NULL,
	[manually_entered] [bit] NULL,
	[cage] [nvarchar](255) NULL,
	[msds_num] [nvarchar](255) NULL,
	[created] [datetime] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

-- MSDS
/****** Object:  Table [dbo].[AUDIT_msds_master]    Script Date: 08/18/2011 10:31:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_msds_master](
	[msds_id] [bigint] NOT NULL,
	[MSDSSERNO] [nvarchar](10) NULL,
	[CAGE] [nvarchar](5) NULL,
	[MANUFACTURER] [nvarchar](255) NULL,
	[PARTNO] [nvarchar](100) NULL,
	[FSC] [nvarchar](18) NULL,
	[NIIN] [nvarchar](9) NULL,
	[hcc_id] [bigint] NULL,
	[msds_file] [image] NULL,
	[file_name] [nvarchar](255) NULL,
	[manually_entered] [bit] NULL,
	[ARTICLE_IND] [nchar](10) NULL,
	[DESCRIPTION] [varchar](1000) NULL,
	[EMERGENCY_TEL] [nvarchar](50) NULL,
	[END_COMP_IND] [nchar](10) NULL,
	[END_ITEM_IND] [nchar](10) NULL,
	[KIT_IND] [nchar](10) NULL,
	[KIT_PART_IND] [nchar](10) NULL,
	[MANUFACTURER_MSDS_NO] [nvarchar](50) NULL,
	[MIXTURE_IND] [nchar](10) NULL,
	[PRODUCT_IDENTITY] [nvarchar](255) NULL,
	[PRODUCT_LOAD_DATE] [smalldatetime] NULL,
	[PRODUCT_RECORD_STATUS] [nchar](10) NULL,
	[PRODUCT_REVISION_NO] [nvarchar](18) NULL,
	[PROPRIETARY_IND] [nchar](1) NULL,
	[PUBLISHED_IND] [nchar](1) NULL,
	[PURCHASED_PROD_IND] [nchar](1) NULL,
	[PURE_IND] [nchar](1) NULL,
	[RADIOACTIVE_IND] [nchar](1) NULL,
	[SERVICE_AGENCY] [nvarchar](10) NULL,
	[TRADE_NAME] [nvarchar](255) NULL,
	[TRADE_SECRET_IND] [nchar](1) NULL,
	[PRODUCT_IND] [nvarchar](1) NULL,
	[PRODUCT_LANGUAGE] [nvarchar](10) NULL,
	[data_source_cd] [nvarchar](10) NULL,
	[created] [datetime] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AUDIT_msds_inventory]    Script Date: 08/18/2011 10:31:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AUDIT_msds_inventory](
	[msds_inventory_id] [bigint] NOT NULL,
	[MSDSSERNO] [nvarchar](10) NULL,
	[CAGE] [nvarchar](5) NULL,
	[uic] [nvarchar](10) NULL,
	[NIIN] [nvarchar](9) NULL,
	[manually_entered] [bit] NULL,
	[created] [datetime] NULL,
	[action] [nvarchar](50) NULL,
	[action_date] [datetime] NULL,
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

-- SFR
/****** Object:  Table [dbo].[sfr]    Script Date: 09/01/2011 08:50:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sfr](
	[sfr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[aul_id] [bigint] NULL,
	[sfr_type] [nvarchar](10) NULL,
	[action_complete] [datetime] NULL,
	[username] [varchar](90) NULL,
	[POC_Name] [varchar](90) NULL,
	[POC_Telephone] [varchar](90) NULL,
	[POC_Email] [varchar](90) NULL,
	[completed_flag] [tinyint] NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](9) NULL,
	[ui] [nvarchar](50) NULL,
	[um] [nvarchar](50) NULL,
	[usage_category_id] [bigint] NULL,
	[description] [varchar](1024) NULL,
	[smcc_id] [bigint] NULL,
	[specs] [nvarchar](50) NULL,
	[shelf_life_code_id] [bigint] NULL,
	[shelf_life_action_code_id] [bigint] NULL,
	[storage_type_id] [bigint] NULL,
	[cog_id] [bigint] NULL,
	[spmig] [nvarchar](255) NULL,
	[nehc_rpt] [nvarchar](10) NULL,
	[catalog_group_id] [bigint] NULL,
	[catalog_serial_number] [nvarchar](255) NULL,
	[allowance_qty] [int] NULL,
	[ship_id] [bigint] NULL,
	[remarks] [text] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](5) NULL,
	[msds_num] [nvarchar](255) NULL,
	[part_no] [nvarchar](255) NULL,
	[poc_city] [nvarchar](255) NULL,
	[poc_address] [nvarchar](255) NULL,
	[poc_state] [nvarchar](20) NULL,
	[poc_zip] [nvarchar](10) NULL,
	[system_equipment_material] [varchar](255) NULL,
	[method_of_application] [varchar](255) NULL,
	[proposed_usage] [varchar](255) NULL,
	[negative_impact] [varchar](255) NULL,
	[special_training] [varchar](255) NULL,
	[precautions] [varchar](255) NULL,
	[properties] [varchar](255) NULL,
	[comments] [varchar](255) NULL,
	[advantages] [varchar](255) NULL,
 CONSTRAINT [PK_sfr] PRIMARY KEY CLUSTERED 
(
	[sfr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[sfr]  WITH NOCHECK ADD  CONSTRAINT [FK_sfr_aul] FOREIGN KEY([aul_id])
REFERENCES [dbo].[auth_use_list] ([aul_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[sfr] CHECK CONSTRAINT [FK_sfr_aul]
GO




