﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HiltHub.Controllers;

namespace tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            if (result != null) Assert.AreEqual<string>("Welcome to ASP.NET MVC!", result.ViewBag.Message);
        }
    }
}
