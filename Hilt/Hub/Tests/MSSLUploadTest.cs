﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HiltHub;
using System.IO;
using HiltHub.Parsers;
using HiltHub.DataObjects;

namespace tests {
    [TestClass]
    public class MSSLUploadTest {
        private const string TestFilesFolder = "..\\..\\files\\";

        [TestMethod]
        public void uploadShipMismatchTest() {
            // Test to see if the file's UIC code matches the selected ship
            String fileNameGood = TestFilesFolder + "MSSL_NOV_9_09Good.txt";
            String fileNameBadUIC = TestFilesFolder + "MSSL_NOV_9_09BadUIC.txt";
            String fileNameBadDate = TestFilesFolder + "MSSL_NOV_9_09BadDate.txt";
            String fileNameBadBoth = TestFilesFolder + "MSSL_NOV_9_09BadBoth.txt";
            String uic = "N20079";
            DateTime shipDate = new DateTime(2009, 10, 10);
            MSSLParser.FILE_STATUS status = MSSLParser.FILE_STATUS.Valid;

            // Start reading the file - the good one
            try {
                StreamReader reader = new StreamReader(fileNameGood);
                MSSLParser m = new MSSLParser();

                // Validate the UIC in the file
                status = m.validateFile(reader, uic, shipDate);

            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
            Assert.AreEqual(status, MSSLParser.FILE_STATUS.Valid);

            // Start reading the file - bad UIC
            try {
                StreamReader reader = new StreamReader(fileNameBadUIC);
                MSSLParser m = new MSSLParser();

                // Validate the UIC in the file
                status = m.validateFile(reader, uic, shipDate);
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
            Assert.AreEqual(status, MSSLParser.FILE_STATUS.Invalid_UIC);

            // Start reading the file - bad Date
            try {
                StreamReader reader = new StreamReader(fileNameBadDate);
                MSSLParser m = new MSSLParser();

                // Validate the UIC in the file
                status = m.validateFile(reader, uic, shipDate);
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
            Assert.AreEqual(status, MSSLParser.FILE_STATUS.Invalid_Date);

            // Start reading the file - bad Both
            try {
                StreamReader reader = new StreamReader(fileNameBadBoth);
                MSSLParser m = new MSSLParser();

                // Validate the UIC in the file
                status = m.validateFile(reader, uic, shipDate);
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
            Assert.AreEqual(status, MSSLParser.FILE_STATUS.Invalid_UIC_Date);
        }

        [TestMethod]
        public void uploadGoodMSSLFileTest() {
            String fileNameGood = TestFilesFolder + "MSSL_NOV_9_09Short.txt";
            //String fileNameGood = "C:\\temp\\HILT_HUB_Data\\TestData\\MSSL_NOV_9_09Good.txt";
            String shipUIC = "N20079";
            DateTime shipDate = new DateTime(2009, 10, 10);
            DateTime fileDate;
            //bool isValid = false;

            MSSLData data = new MSSLData();
            
            try {
                string filename = fileNameGood;

                StreamReader reader = new StreamReader(filename);
                MSSLParser m = new MSSLParser();

                // Validate the vessel's UIC and upload date against the file UIC & date
                MSSLParser.FILE_STATUS status = m.validateFile(reader, shipUIC, shipDate);
                if (status == MSSLParser.FILE_STATUS.Valid) {
                    // Parse the file and get the msslInventory
                    reader = new StreamReader(filename);
                    data = m.parse(reader, shipUIC);
                    fileDate = m.fileDate;

                    // Insert data from inventoryList into the HILT database
                    DatabaseManager manager = new DatabaseManager();

                    // Check the inventory list for add, delete, change
                    String changeMessage = "";
                    DatabaseManager.FILE_CHANGES changes = 
                        manager.validateMSSLChanges(data, shipUIC);
                    switch (changes) {
                        case DatabaseManager.FILE_CHANGES.Initial_Load:
                            changeMessage = "Initial MSSL load for this unit";
                            break;
                        case DatabaseManager.FILE_CHANGES.Threshold_Exceeded:
                            changeMessage = 
                                "More than 25% of the MSSL records have changed." +
                                "\nDo you want to continue loading this file?";
                            break;
                    }

                    // Display message if we have changes
                    System.Windows.Forms.DialogResult response = System.Windows.Forms.DialogResult.Yes;
                    if (changeMessage.Length > 0) {
                        response = System.Windows.Forms.MessageBox.Show(changeMessage, "MSSL Upload",
                            System.Windows.Forms.MessageBoxButtons.YesNo);
                    }

                    if (response == System.Windows.Forms.DialogResult.Yes) {
                        // Load the data into the database
                        manager.processMSSLInventory(shipUIC, data, "TEST");

                        // Update the ships table mssl upload field
                        DateTime uploadDate = DateTime.Now;
                        manager.updateShipFileUpload(shipUIC, Configuration.UPLOAD_TYPES.MSSL, 
                            uploadDate, "TEST");
                        // Insert a record into the file uploads table

                        manager.updateFileTable(DatabaseManager.FILE_TABLES.FileUpload,
                            shipUIC, filename, Configuration.UPLOAD_TYPES.MSSL.ToString(),
                            fileDate, uploadDate, "TEST", data.MsslStats.NewRecords,
                            data.MsslStats.ChangedRecords, data.MsslStats.DeletedRecords);
                     }
                }
                else {
                    switch (status) {
                        case MSSLParser.FILE_STATUS.Invalid_Date:
                            data.ErrorMsg = "The file date is older than the last MSSL upload date";
                            break;
                        case MSSLParser.FILE_STATUS.Invalid_UIC:
                            data.ErrorMsg = "The UIC in the file does not match the selected unit's UIC";
                            break;
                        case MSSLParser.FILE_STATUS.Invalid_UIC_Date:
                            data.ErrorMsg = "The UIC in the file does not match the selected unit's UIC" +
                                "\n and the file date is older than the last MSSL upload date";
                            break;
                    }
                }

            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }

            Assert.IsTrue((data.ErrorMsg == null), data.ErrorMsg);
        }

        [TestMethod]
        public void uploadBadMSSLFileTest() {
            // Test to see if the file's UIC code matches the selected ship
            String fileNameBadData = TestFilesFolder + "MSSL_NOV_9_09Good.txt";
            String fileNameBadUIC = TestFilesFolder + "MSSL_NOV_9_09BadUIC.txt";
            String uic = "N20079";
            MSSLData data = null;

            try {
                // Start reading the file - bad UIC
                StreamReader reader = new StreamReader(fileNameBadUIC);
                MSSLParser m = new MSSLParser();

                //// Parse the file
                data = m.parse(reader, uic);
            }
            catch (Exception ex) {
                Assert.Fail(ex.Message);
            }
            /*

            Assert.IsTrue((data.ErrorMsg != null), data.ErrorMsg);
            //Start reading the file - too many bad records
            try {
                StreamReader reader = new StreamReader(fileNameBadData);
                MSSLParser m = new MSSLParser();

                // Validate the UIC in the file
                bool isValid = m.validateFileUIC(reader, uic);


                Assert.IsTrue(isValid);
            }
            catch (Exception ex) {

            }
             */
        }

    }
}
