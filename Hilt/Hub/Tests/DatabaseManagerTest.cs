﻿using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HiltHub;
using HiltHub.DataObjects;
using HiltHub.Models;
using System.Collections.Generic;
using System.Diagnostics;

namespace tests {
    [TestClass]
    public class DatabaseManagerTest {

        [TestMethod]
        public void checkHullTypeExistsTest() {
            // DatabaseManager
            DatabaseManager mgr = new DatabaseManager();

            // Valid hull_type_id
            int hull_type_id = 1;

            bool exists = mgr.checkHullTypeExists(hull_type_id);

            Assert.IsTrue(exists, "Valid hull_type_id");

            // Invalid hull_type_id
            hull_type_id = -10;

            exists = mgr.checkHullTypeExists(hull_type_id);

            Assert.IsFalse(exists, "Invalid hull_type_id");

            // Valid hull_type
            String hull_type = "SSN";

            exists = mgr.checkHullTypeExists(hull_type);

            Assert.IsTrue(exists, "Valid hull_type");

            // Invalid hull_type
            hull_type = "Zeppelin";

            exists = mgr.checkHullTypeExists(hull_type);

            Assert.IsFalse(exists, "Invalid hull_type");
        }

        [TestMethod]
        public void checkShipExistsTest() {
            // DatabaseManager
            DatabaseManager mgr = new DatabaseManager();

            // Valid ship_id
            int ship_id = 1;

            bool exists = mgr.checkShipExists(ship_id);

            Assert.IsTrue(exists, "Valid ship_id");

            // Invalid ship_id
            ship_id = -10;

            exists = mgr.checkShipExists(ship_id);

            Assert.IsFalse(exists, "Invalid ship_id");

            // Valid ship name
            String ship_name = "Test Vessel";

            exists = mgr.checkShipExists(ship_name);

            Assert.IsTrue(exists, "Valid ship_name");

            // Invalid ship name
            ship_name = "Zeppelin";

            exists = mgr.checkShipExists(ship_name);

            Assert.IsFalse(exists, "Invalid ship_name");
        }

        [TestMethod]
        public void getShipTest() {
            // DatabaseManager
            DatabaseManager mgr = new DatabaseManager();

            // Valid ship_id
            int ship_id = 1;
            String hull_number = "781";

            Ship shipRecord = mgr.getShipRecord(ship_id);

            Assert.AreEqual(shipRecord.hull_number, hull_number);
        }

        [TestMethod]
        public void getMSSLQuantitiesTest() {
            // DatabaseManager
            DatabaseManager mgr = new DatabaseManager();
            String ship_UIC = "N20079";
            int expectedCount = 175;

            Hashtable ht = mgr.getMSSLQuantities(ship_UIC);

            Assert.AreEqual(ht.Count, expectedCount);
        }

        // MSDS Tests
        [TestMethod]
        public void insertMSDSTest() {
            // DatabaseManager
            DatabaseManager mgr = new DatabaseManager();

            // Get the MSDS test record
            MSDSRecord record = createMSDSTestRecord();

            // See if the record already exists
            long nStart = Environment.TickCount;
            long msds_id = mgr.getMSDSIdFromSerNoCage(record.MSDSSERNO, record.CAGE);
            long nEnd = Environment.TickCount;
            Debug.WriteLine("***** insertMSDSTest getMSDSIdFromSerNoCage: " + (nEnd - nStart).ToString());

            if (msds_id > 0) {
                // Delete the record
                record.msds_id = msds_id;
                nStart = Environment.TickCount;
                long result = mgr.deleteMSDSMasterRecord("TEST", record, "TEST");
                nEnd = Environment.TickCount;
                Debug.WriteLine("***** insertMSDSTest deleteMSDSMasterRecord: " + (nEnd - nStart).ToString());
            }

            // Do the insert
            nStart = Environment.TickCount;
            long new_id = mgr.insertMSDSMasterRecord("Test", record, "Test");
            nEnd = Environment.TickCount;
            Debug.WriteLine("***** insertMSDSTest insertMSDSMasterRecord: " + (nEnd - nStart).ToString());

            // Get the record and check the results
            nStart = Environment.TickCount;
            MSDSRecord newRecord = mgr.getMSDSMasterRecord(new_id);
            nEnd = Environment.TickCount;
            Debug.WriteLine("***** insertMSDSTest getMSDSMasterRecord: " + (nEnd - nStart).ToString());

            // Master record
            Assert.AreEqual(record.MSDSSERNO, newRecord.MSDSSERNO);
            Assert.AreEqual(record.CAGE, newRecord.CAGE);
            Assert.AreEqual(record.NIIN, newRecord.NIIN);
            Assert.AreEqual(34, newRecord.hcc_id);

            // AFJM_PSN
            Assert.AreEqual(record.Afjm_Psn.AFJM_PSN_CODE, newRecord.Afjm_Psn.AFJM_PSN_CODE);
            // Contractor List
            Assert.AreEqual(record.ContractorList.Count, newRecord.ContractorList.Count);
            Assert.AreEqual(record.ContractorList[0].CT_STATE, newRecord.ContractorList[0].CT_STATE);
            // Disposal
            Assert.AreEqual(record.Disposal.EPA_HAZ_WASTE_CODE, newRecord.Disposal.EPA_HAZ_WASTE_CODE);
            // Document Types
            Assert.AreEqual(record.DocumentTypes.manufacturer_label_filename, newRecord.DocumentTypes.manufacturer_label_filename);
            // DOT_PSN
            Assert.AreEqual(record.Dot_Psn.DOT_HAZARD_LABEL, newRecord.Dot_Psn.DOT_HAZARD_LABEL);
            // IATA_PSN
            Assert.AreEqual(record.Iata_Psn.IATA_HAZARD_LABEL, newRecord.Iata_Psn.IATA_HAZARD_LABEL);
            // IMO_PSN
            Assert.AreEqual(record.Imo_Psn.IMO_HAZARD_CLASS, newRecord.Imo_Psn.IMO_HAZARD_CLASS);
            // Ingredients
            Assert.AreEqual(record.IngredientsList.Count, newRecord.IngredientsList.Count);
            Assert.AreEqual(record.IngredientsList[0].INGREDIENT_NAME, newRecord.IngredientsList[0].INGREDIENT_NAME);
            // Item Description
            Assert.AreEqual(record.ItemDescription.ITEM_NAME, newRecord.ItemDescription.ITEM_NAME);
            // Label Info
            Assert.AreEqual(record.LabelInfo.LABEL_ITEM_NAME, newRecord.LabelInfo.LABEL_ITEM_NAME);
            // Phys Chemical
            Assert.AreEqual(record.PhysChemical.APP_ODOR, newRecord.PhysChemical.APP_ODOR);
            // Radiological Info
            Assert.AreEqual(record.RadiologicalInfoList[0].RAD_SYMBOL, newRecord.RadiologicalInfoList[0].RAD_SYMBOL);
            // Transportation
            Assert.AreEqual(record.Transportation.MARINE_POLLUTANT_IND, newRecord.Transportation.MARINE_POLLUTANT_IND);
        }

        // MSDS Tests
        [TestMethod]
        public void updateMSDSTest() {
            // DatabaseManager
            DatabaseManager mgr = new DatabaseManager();

            // Get the MSDS test record 
            MSDSRecord record = createMSDSTestRecord();

            // See if it already exists in the database - if it doesn't, create it
            long nStart = Environment.TickCount;
            long msds_id = mgr.getMSDSIdFromSerNoCage(record.MSDSSERNO, record.CAGE);
            long nEnd = Environment.TickCount;
            Debug.WriteLine("***** updateMSDSTest getMSDSIdFromSerNoCage: " + (nEnd - nStart).ToString());
            if (msds_id <= 0) {
                nStart = Environment.TickCount;
                msds_id = mgr.insertMSDSMasterRecord("TEST", record, "TEST");
                nEnd = Environment.TickCount;
                Debug.WriteLine("***** updateMSDSTest insertMSDSMasterRecord: " + (nEnd - nStart).ToString());
            }
            record.msds_id = msds_id;

            // Modify the record
            modifyMSDSTestRecord(record);
            // Do the update
            nStart = Environment.TickCount;
            mgr.updateMSDSMasterRecord("test", record, "test");
            nEnd = Environment.TickCount;
            Debug.WriteLine("***** updateMSDSTest updateMSDSMasterRecord: " + (nEnd - nStart).ToString());

            // Compare the results
            nStart = Environment.TickCount;
            MSDSRecord newRecord = mgr.getMSDSMasterRecord(record.msds_id);
            nEnd = Environment.TickCount;
            Debug.WriteLine("***** updateMSDSTest getMSDSMasterRecord: " + (nEnd - nStart).ToString());

            // Master record
            Assert.AreEqual(record.MSDSSERNO, newRecord.MSDSSERNO);
            Assert.AreEqual(record.CAGE, newRecord.CAGE);
            Assert.AreEqual(record.NIIN, newRecord.NIIN);
            Assert.AreEqual(33, newRecord.hcc_id);
            Assert.AreEqual(record.RADIOACTIVE_IND, newRecord.RADIOACTIVE_IND);

            // AFJM_PSN
            Assert.AreEqual(record.Afjm_Psn.AFJM_PSN_CODE, newRecord.Afjm_Psn.AFJM_PSN_CODE);
            // Contractor List
            Assert.AreEqual(record.ContractorList.Count, newRecord.ContractorList.Count);
            Assert.AreEqual(record.ContractorList[0].CT_STATE, newRecord.ContractorList[0].CT_STATE);
            // Disposal
            Assert.AreEqual(record.Disposal.EPA_HAZ_WASTE_CODE, newRecord.Disposal.EPA_HAZ_WASTE_CODE);
            // Document Types
            Assert.AreEqual(record.DocumentTypes.manufacturer_label_filename, newRecord.DocumentTypes.manufacturer_label_filename);
            // DOT_PSN
            Assert.AreEqual(record.Dot_Psn.DOT_HAZARD_LABEL, newRecord.Dot_Psn.DOT_HAZARD_LABEL);
            // IATA_PSN
            Assert.AreEqual(record.Iata_Psn.IATA_HAZARD_LABEL, newRecord.Iata_Psn.IATA_HAZARD_LABEL);
            // IMO_PSN
            Assert.AreEqual(record.Imo_Psn.IMO_HAZARD_CLASS, newRecord.Imo_Psn.IMO_HAZARD_CLASS);
            // Ingredients
            Assert.AreEqual(record.IngredientsList.Count, newRecord.IngredientsList.Count);
            Assert.AreEqual(record.IngredientsList[0].INGREDIENT_NAME, newRecord.IngredientsList[0].INGREDIENT_NAME);
            // Item Description
            Assert.AreEqual(record.ItemDescription.ITEM_NAME, newRecord.ItemDescription.ITEM_NAME);
            // Label Info
            Assert.AreEqual(record.LabelInfo.LABEL_ITEM_NAME, newRecord.LabelInfo.LABEL_ITEM_NAME);
            // Phys Chemical
            Assert.AreEqual(record.PhysChemical.APP_ODOR, newRecord.PhysChemical.APP_ODOR);
            // Radiological Info
            Assert.AreEqual(record.RadiologicalInfoList[0].RAD_SYMBOL, newRecord.RadiologicalInfoList[0].RAD_SYMBOL);
            // Transportation
            Assert.AreEqual(record.Transportation.MARINE_POLLUTANT_IND, newRecord.Transportation.MARINE_POLLUTANT_IND);
        }

        // MSDS Test Record
        private MSDSRecord createMSDSTestRecord() {
            MSDSRecord record = new MSDSRecord();

            // Populate the data
            record.CAGE = "ZZZZZ";
            record.data_source_cd = "TEST";
            record.DESCRIPTION = "Test New MSDS Functionality";
            record.EMERGENCY_TEL = "911";
            record.file_name = "ZZTOP_MSDS.pdf";
            record.FSC = "9999";
            record.hcc_id = 34;
            record.manually_entered = false;
            record.MANUFACTURER = "ZZ Industries";
            record.MANUFACTURER_MSDS_NO = "0091";
            record.MSDSSERNO = "ZZTOP";
            record.NIIN = "999999999";
            record.PARTNO = "AA-ROCK";
            record.PRODUCT_IDENTITY = "Old Time Rockers";
            record.PRODUCT_IND = "Y";
            record.PRODUCT_LOAD_DATE = new DateTime(2011, 10, 4);
            record.PRODUCT_REVISION_NO = "2";
            record.RADIOACTIVE_IND = "N";

            // Now populate the 13 sub table objects
            record.Afjm_Psn = new MSDS_AFJM_PSN();
            record.Afjm_Psn.AFJM_HAZARD_CLASS = "4.1";
            record.Afjm_Psn.AFJM_PSN_CODE = "PTH";
            record.Afjm_Psn.AFJM_PROP_SHIP_NAME = "CORROSIVE LIQUID, FLAMMABLE, N.O.S.";

            MSDSContractorInfo contractor = new MSDSContractorInfo();
            record.ContractorList = new List<MSDSContractorInfo>();

            contractor.CT_CAGE = "9100";
            contractor.CT_COMPANY_NAME = "ZZ Industries";
            contractor.CT_STATE = "AL";
            record.ContractorList.Add(contractor);

            record.Disposal = new MSDSDisposal();
            record.Disposal.EPA_HAZ_WASTE_CODE = "U069";
            record.Disposal.EPA_HAZ_WASTE_NAME = "WASTE DIBUTYL PHTHALATE";

            record.DocumentTypes = new MSDSDocumentTypes();
            record.DocumentTypes.manufacturer_label_filename = "TestLabel31.png";
            record.DocumentTypes.product_sheet_filename = "ZZ Top on display.doc";
            record.DocumentTypes.transportation_cert_filename = "NoShip.pdf";

            record.Dot_Psn = new MSDS_DOT_PSN();
            record.Dot_Psn.DOT_HAZARD_LABEL = "Play at your own risk";
            record.Dot_Psn.DOT_PSN_CODE = "XXX";

            record.Iata_Psn = new MSDS_IATA_PSN();
            record.Iata_Psn.IATA_HAZARD_CLASS = "9";
            record.Iata_Psn.IATA_HAZARD_LABEL = "Boogie";

            record.Imo_Psn = new MSDS_IMO_PSN();
            record.Imo_Psn.IMO_HAZARD_CLASS = "No puke";
            record.Imo_Psn.IMO_PSN_CODE = "XXX";

            MSDSIngredient ingredient = new MSDSIngredient();
            record.IngredientsList = new List<MSDSIngredient>();
            ingredient.INGREDIENT_NAME = "Sulfur";
            ingredient.PRCNT = "40";
            record.IngredientsList.Add(ingredient);

            record.ItemDescription = new MSDSItemDescription();
            record.ItemDescription.ITEM_NAME = "Best of ZZ Top";
            record.ItemDescription.ITEM_MANAGER = "I69";

            record.LabelInfo = new MSDSLabelInfo();
            record.LabelInfo.COMPANY_NAME_RP = "ZZ Industries";
            record.LabelInfo.LABEL_ITEM_NAME = "Album Cover";

            record.PhysChemical = new MSDSPhysChemical();
            record.PhysChemical.APP_ODOR = "Dead Skunk";
            record.PhysChemical.AUTOIGNITION_TEMP = "451";
            record.PhysChemical.EPA_CHRONIC = "Y";

            MSDSRadiologicalInfo rad = new MSDSRadiologicalInfo();
            record.RadiologicalInfoList = new List<MSDSRadiologicalInfo>();
            rad.RAD_FORM = "NORM-SOL";
            rad.RAD_SYMBOL = "TH 232";
            record.RadiologicalInfoList.Add(rad);

            record.Transportation = new MSDSTransportation();
            record.Transportation.AF_MMAC_CODE = "Test";
            record.Transportation.MARINE_POLLUTANT_IND = "Y";

            return record;
        }

        // MSDS Test Record - modified
        private void modifyMSDSTestRecord(MSDSRecord record) {
            // Modify the data
            record.DESCRIPTION = "Test Update MSDS Functionality";
            record.EMERGENCY_TEL = "867-5309";
            record.file_name = "ZZTOP_MSDS.pdf";
            record.FSC = "9999";
            record.hcc_id = 33;
            record.manually_entered = false;
            record.MANUFACTURER = "ZZ Industries";
            record.MANUFACTURER_MSDS_NO = "0091";
            record.MSDSSERNO = "ZZTOP";
            record.NIIN = "999999999";
            record.PARTNO = "AA-ROCK";
            record.PRODUCT_IDENTITY = "Old Time Rockers";
            record.PRODUCT_IND = "Y";
            record.PRODUCT_REVISION_NO = "2";
            record.RADIOACTIVE_IND = "Y";

            // Now populate the 13 sub table objects
            record.Afjm_Psn.AFJM_HAZARD_CLASS = "4.1";
            record.Afjm_Psn.AFJM_PSN_CODE = "ZTH";
            record.Afjm_Psn.AFJM_PROP_SHIP_NAME = "CORROSIVE LIQUID, FLAMMABLE, N.O.S.";

            record.ContractorList[0].CT_STATE = "CA";

            MSDSContractorInfo contractor = new MSDSContractorInfo();
            contractor.CT_CAGE = "9200";
            contractor.CT_COMPANY_NAME = "ZZ Industries, LLC";
            contractor.CT_STATE = "AL";
            record.ContractorList.Add(contractor);

            record.Disposal.EPA_HAZ_WASTE_CODE = "Z069";
            record.Disposal.EPA_HAZ_WASTE_NAME = "WASTED LIFE";

            record.DocumentTypes.manufacturer_label_filename = "TestLabel33.png";
            record.DocumentTypes.product_sheet_filename = "ZZ Top on display.doc";
            record.DocumentTypes.transportation_cert_filename = "NoShip.pdf";

            record.Dot_Psn.DOT_HAZARD_LABEL = "Back Again";
            record.Dot_Psn.DOT_PSN_CODE = "XXX";

            record.Iata_Psn.IATA_HAZARD_CLASS = "9";
            record.Iata_Psn.IATA_HAZARD_LABEL = "Good To Go";

            record.Imo_Psn.IMO_HAZARD_CLASS = "No puking";
            record.Imo_Psn.IMO_PSN_CODE = "XXX";

            record.IngredientsList[0].INGREDIENT_NAME = "Fish Eye";

            MSDSIngredient ingredient = new MSDSIngredient();
            ingredient.INGREDIENT_NAME = "Tail of Newt";
            ingredient.PRCNT = "60";
            record.IngredientsList.Add(ingredient);

            record.ItemDescription.ITEM_NAME = "Best of ZZ Top II";
            record.ItemDescription.ITEM_MANAGER = "I69";

            record.LabelInfo.COMPANY_NAME_RP = "ZZ Industries";
            record.LabelInfo.LABEL_ITEM_NAME = "Album Liner";

            record.PhysChemical.APP_ODOR = "Dead Skunk in the Middle of the Road";
            record.PhysChemical.AUTOIGNITION_TEMP = "451";
            record.PhysChemical.EPA_CHRONIC = "Y";

            record.RadiologicalInfoList[0].RAD_FORM = "NORM-SOL";
            record.RadiologicalInfoList[0].RAD_SYMBOL = "KR 85";

            record.Transportation.AF_MMAC_CODE = "Test";
            record.Transportation.MARINE_POLLUTANT_IND = "N";
        }
    }
}
