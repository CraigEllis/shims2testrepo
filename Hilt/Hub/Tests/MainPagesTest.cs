﻿using System.IO;
using System.Web;
using System.Web.UI;
using HiltHub.Controllers;
using HiltHub.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using MvcContrib.UI.Grid;
using System.Web.Mvc;

namespace tests
{
    
    
    /// <summary>
    ///This is a test class for AulControllerTest and is intended
    ///to contain all AulControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MainPagesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        [HostType("ASP.NET")]
        public void AulIndexTest()
        {
            AulController target = new AulController();
            ViewResult actual = target.Index(1, new GridSortOptions(), null, null, null, null, null) as ViewResult;
            Assert.IsNotNull(actual);
            if (actual != null) {
                AulListContainerViewModel model = actual.Model as AulListContainerViewModel;
                Assert.IsNotNull(model);
                if (model != null)
                    Assert.IsTrue(model.AulPagedList.TotalItems > 0);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        public void MsslIndexTest()
        {
            MsslController target = new MsslController();
            ViewResult actual = target.Index(1, new GridSortOptions(), null, null, null, null) as ViewResult;
            Assert.IsNotNull(actual);
            if (actual != null) {
                MsslListContainerViewModel model = actual.Model as MsslListContainerViewModel;
                Assert.IsNotNull(model);
                if (model != null)
                    Assert.IsTrue(model.MsslPagedList.TotalItems > 0);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        public void MsdsIndexTest()
        {
            MsdsController target = new MsdsController();
            ViewResult actual = target.Index(1, new GridSortOptions(), null, null, null, null, null, null) as ViewResult;
            Assert.IsNotNull(actual);
            if (actual != null) {
                MsdsListContainerViewModel model = actual.Model as MsdsListContainerViewModel;
                Assert.IsNotNull(model);
                if (model != null)
                    Assert.IsTrue(model.MsdsPagedList.TotalItems > 0);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        public void ShipIndexTest()
        {
            ShipController target = new ShipController();
            ViewResult actual = target.Index(1, new GridSortOptions(), null, null, null, null) as ViewResult;
            Assert.IsNotNull(actual);
            if (actual != null) {
                ShipListContainerViewModel model = actual.Model as ShipListContainerViewModel;
                Assert.IsNotNull(model);
                if (model != null)
                    Assert.IsTrue(model.ShipPagedList.TotalItems > 0);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        public void DashboardTest()
        {
            DashboardController target = new DashboardController();
            ActionResult actual = target.Index();
            Assert.IsNotNull(actual);
        }

    }
}
