﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace HILT_IMPORTER {
    public partial class MSDSImportForm : HILT_IMPORTER.BaseImportForm {
        DateTime m_lastUpdate;
        
        public enum DOCUMENT_TYPES {
            HTML,
            PDF,
            RTF,
            NONE
        };

        public MSDSImportForm() {
            InitializeComponent();

            // Set the label
            this.Title = "Manufacturer Safety Data Sheets Import";
            this.fileLlabel.Text = "MSDS Folder:";
            Text = "MSDS Import";
            messageRTBox.Text = "";
            this.fileTextBox.Text = Configuration.CDROM_DataFolder;
        }

        private void MSDSImportForm_Shown(object sender, EventArgs e) {
            importButton.Focus();
            Application.DoEvents();

            // Set the rich text box tab size 
            TabSizeTwips = 360;
            previewData(false);
        }

        long m_ProgressBarMaxValue = 100;
        public long ProgressBarMaxValue {
            get {
                return m_ProgressBarMaxValue;
            }
            set {
                m_ProgressBarMaxValue = value;
            }
        }

        protected override void processCancelButton_Click(object sender, EventArgs e) {
            if (m_processing) {
                // Make sure the user really wants to cancel
                DialogResult response = MessageBox.Show(this,
                    "This will cancel the Hub database MSDS update"
                    + "\nAre you sure you want to cancel the update?",
                    "MSDS Update Cancellation Pending", MessageBoxButtons.YesNo);

                if (response == DialogResult.Yes)
                    // Cancel the background task
                    msdsBackgroundWorker.CancelAsync();
            }
            else {
                // Clear the data and reset the status message 
                clearData();

                messageRTBox.Text = "";
                m_parentMDI.StatusBarLabel_Value = "Ready";
                m_parentMDI.progressBar.Value = 0;
                setCursors(Cursors.Default);
            }
        }

        protected override void processFileButton_Click(object sender, EventArgs e) {
            // Display the directory dialog to select the directory containing the MSDS CD image
            FolderBrowserDialog browseDialog = new FolderBrowserDialog();

            if (browseDialog.ShowDialog() == DialogResult.OK) {
                fileTextBox.Text = browseDialog.SelectedPath;
            }
        }

        protected override void processImportButton_Click(object sender, EventArgs e) {
            m_parentMDI.StatusBarCount_Visible = true;
            m_parentMDI.ProgressBar_Visible = true;
            setCursors(Cursors.WaitCursor);

            DateTime dtStart = DateTime.Now;
            long nStart = Environment.TickCount;

            // Execute import as a background thread
            // Clear things out
            m_parentMDI.StatusBarLabel_Value = "Processing Data...";
            m_parentMDI.StatusBarProgress_Value = 0;
            resetMessageBox();

            m_processing = true;
            msdsBackgroundWorker.RunWorkerAsync(processAllCheckBox.Checked);

            while (msdsBackgroundWorker.IsBusy) {
                Application.DoEvents();
            }
        }

        private void previewData(bool processAll) {
            int percentComplete = 0;
            m_lastUpdate = DateTime.MinValue;

            m_parentMDI.StatusBarCount_Visible = true;
            m_parentMDI.ProgressBar_Visible = true;
            setCursors(Cursors.WaitCursor);

            try {
                long nStart = Environment.TickCount;
                m_parentMDI.StatusBarLabel_Value = "Analyzing Data...";
                resetMessageBox();
                Application.DoEvents();

                StringBuilder sb = new StringBuilder();
                DatabaseManager manager = new DatabaseManager();

                // Generate the preview info
                sb.AppendLine("***** HILT HUB Database *****");

                // SQL Server MSDS Statistics
                int count = manager.getTableCount("MSDS_Master");
                sb.AppendLine("\tMSDS Records: " + formatNumberThousands(count));

                percentComplete += 10;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // MSDS Documents
                int[] fileCounts = getFileCount();
                sb.Clear();
                sb.AppendLine("Documents:");
                sb.AppendLine("\tMSDS: " + formatNumberThousands(fileCounts[0]));
                sb.AppendLine("\tProd Sheets: " + formatNumberThousands(fileCounts[1]));
                sb.AppendLine("\tMfg Labels : " + formatNumberThousands(fileCounts[2]));
                sb.AppendLine("\tTrans Cert : " + formatNumberThousands(fileCounts[3]));
                //sb.AppendLine("\tMSDS, Trans: " + fileCounts[4].ToString());
                sb.AppendLine("\tOther Docs : " + formatNumberThousands(fileCounts[5]));

                percentComplete += 10;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // Last MSDS load date
                sb.Clear();
                if (processAll) {
                    sb.AppendLine("\nReprocess all HMIRS Data");
                    m_lastUpdate = DateTime.MinValue;
                }
                else {
                    m_lastUpdate = manager.getLastHMIRSUpdate();
                    sb.AppendLine("\nLast HMIRS Update: "
                        + String.Format("{0:MM/dd/yyyy}", m_lastUpdate));
                }
                percentComplete += 5;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // HMIRS Statistics
                HMIRSDatabaseManager hmirsMgr = new HMIRSDatabaseManager();
                sb.Clear();
                sb.AppendLine("\n***** HMIRS Database *****");
                // Get the number of new records
                count = hmirsMgr.getNewRevisionCount(m_lastUpdate);
                sb.AppendLine("\tNew/Updated MSDS Records: " + formatNumberThousands(count));
                percentComplete += 20;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // MSDS Documents
                fileCounts = hmirsMgr.getRevisionDocTypeCounts(m_lastUpdate);
                sb.Clear();
                sb.AppendLine("Documents:");
                sb.AppendLine("\tMSDS: " + formatNumberThousands(fileCounts[0]));
                sb.AppendLine("\tProd Sheets: " + formatNumberThousands(fileCounts[1]));
                sb.AppendLine("\tMfg Labels : " + formatNumberThousands(fileCounts[2]));
                sb.AppendLine("\tTrans Cert : " + formatNumberThousands(fileCounts[3]));
                //sb.AppendLine("\tMSDS, Trans: " + fileCounts[4].ToString());
                sb.AppendLine("\tOther Docs : " + formatNumberThousands(fileCounts[5]));
                percentComplete += 20;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                // Support tables
                fileCounts = hmirsMgr.getSupportDataRevisionCounts(m_lastUpdate);
                sb.Clear();
                sb.AppendLine("Support Tables:");
                sb.AppendLine("\tContractors: " + formatNumberThousands(fileCounts[0]));
                sb.AppendLine("\tTrans Sheets: "
                    + formatNumberThousands(fileCounts[1] + fileCounts[2] + fileCounts[3] + fileCounts[4]));
                percentComplete = 100;
                messageRTBox.AppendText(sb.ToString());
                m_parentMDI.StatusBarProgress_Value = percentComplete;
                Application.DoEvents();

                messageRTBox.Text += "\nEstimated Processing time: "
                    + ((int)(count * .005) + 15).ToString() + " (minutes)";

                long nEnd = Environment.TickCount;
                //messageRTBox.Text += "\nPreview time: "
                //    + (((double)(nEnd - nStart)) / 1000).ToString("#.0") + " (seconds)";
            }
            catch (Exception ex) {
                messageRTBox.Text = "ERROR previewing MSDS data - check the configuration settings in Tools > Setup" + ex.ToString();
            }
            m_parentMDI.StatusBarLabel_Value = "Ready";
            m_parentMDI.progressBar.Value = 0;
            setCursors(Cursors.Default);
            Application.DoEvents();
        }

        private void writeFileTypeFound(String fileType, long rowNum, String source) {
            Debug.WriteLine("Found:\t" + fileType + "\t" + rowNum +"\t" + source);
        }

        private void clearData() {
        }

        //**********************************************************************
        // BackgroundWorker Import Stuff
        private void msdsBackgroundWorker_DoWork(object sender, DoWorkEventArgs e) {
            // Run the import
            try {
                HMIRSImporter importer = new HMIRSImporter(this, msdsBackgroundWorker);

                // Update the database
                bool result = importer.import(m_lastUpdate);
            }
            //TODO clean this up to use the messageBox
            //catch (ImportException ex) {
            //    MessageBox.Show(ex.Message, "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Unexpected Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void msdsBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Update the value of the ProgressBar and the count label to the BackgroundWorker progress.
            m_parentMDI.StatusBarCount_Value = e.ProgressPercentage;

            // change the count to a percentage for the progress bar
            double percent = (e.ProgressPercentage * 100.0) / m_ProgressBarMaxValue;
            m_parentMDI.StatusBarProgress_Value = (int)percent;

            if (e.UserState != null) {
                String msg = e.UserState.ToString();
                // See whether we write this to the message text box, or the status bar
                if (msg.StartsWith("SB_")) {
                    m_parentMDI.StatusBarLabel_Value = msg.Substring(3);
                }
                else {
                    messageRTBox.AppendText(msg);
                }
            }

            Application.DoEvents();
        }

        private void msdsBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // Clean things up
            m_processing = false;
            m_parentMDI.StatusBarLabel_Value = "Ready";
            m_parentMDI.progressBar.Value = 0;
            setCursors(Cursors.Default);
        }

        private int[] getFileCount() {
            // Initialize the document count array
            // Order of types is: MSDS, Mfg Label, 
            int[] result = {0, 0, 0, 0, 0, 0};

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(
                Configuration.MSDS_DocumentFolder);
            // Loop through the files to count by type
            foreach (FileInfo file in dir.GetFiles()) {
                if (file.Name.Contains("_MSDS_"))
                    result[0]++;
                else if (file.Name.Contains("_PROD SHEET_"))
                    result[1]++;
                else if (file.Name.Contains("_MFR. LABEL_"))
                    result[2]++;
                else if (file.Name.Contains("_TRANS CERT_"))
                    result[3]++;
                else if (file.Name.Contains("_MSDS, TRAN_"))
                    result[4]++;
                else if (file.Name.Contains("_OTHER DOCS_"))
                    result[5]++;
            }

            return result;
        }

        private void processAllCheckBox_CheckedChanged(object sender, EventArgs e) {
            if (processAllCheckBox.Checked) {
                // Rerun the preview to show them what they are in for
                previewData(true);
            }
            else {
                previewData(false);
            }
        }
    }
}
