﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.IO;
using HILT_IMPORTER.DataObjects;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("HILT_Importer_Test")]

namespace HILT_IMPORTER {
    public class HMIRSDatabaseManager {
        public const string UNKNOWN_HCC = "H1";

        private string base_ConnectionString = "DRIVER=Adaptive Server Anywhere 8.0;"
                + "Start=XXXXXX\\RTENG8.exe -r -c 50M -x none -gp 32768 -n UTIL;"
                + "UID=dba;"
                + "PWD=sql;"
                + "Description=HMIRS CD DB v8.0;"
                + "DatabaseFile=XXXXXX\\CDROM.CDB;"
                + "AutoStop=Yes;";

        private string m_connString = "";
        private static OdbcConnection m_conn = null;

        public HMIRSDatabaseManager() {
            // Call Replace twice since .NET doesn't have a ReplaceAll method
            m_connString = base_ConnectionString.Replace("XXXXXX",
                Configuration.CDROM_DataFolder);
            m_connString = base_ConnectionString.Replace("XXXXXX",
                Configuration.CDROM_DataFolder);
            // Create new connection on initialization, but do not open, to execute the START command
            // in DSN, so it is available by time we need RTENG8.EXE
            openConnection();
        }

        public HMIRSDatabaseManager(string filePath) {
            // Call Replace twice since .NET doesn't have a ReplaceAll method
            m_connString = base_ConnectionString.Replace("XXXXXX",
                filePath);
            m_connString = base_ConnectionString.Replace("XXXXXX",
                filePath);
            openConnection();
        }

        public void disposeConnection() {
            m_conn.Close();
            m_conn.Dispose();
        }

        protected internal void openConnection()
        {
            m_conn = new OdbcConnection(m_connString);
        }

        protected internal OdbcConnection getConnection() {
            if (m_conn == null)
                openConnection();
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            return m_conn;
        }

        #region MSDS Record
        public MSDSRecord getMSDSMasterRecord(string msdsSerNo, DateTime lastUpdate) {
            MSDSRecord record = null;
            long doc_index_id = 0;

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            //TODO - replace doc_vaut_agg with individual tables?
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT ag.*, ");
            sb.AppendLine("di.Mix_Ind, di.Published_Ind, di.Purchased_Product_Ind, ");
            sb.AppendLine("di.Pure_Ind, di.Trade_Secret_Ind, di.tmsp_last_updt AS di_updt, ");
            sb.AppendLine("dd.tmsp_last_updt AS dd_updt, ");
            sb.AppendLine("lo.Description_Text, lo.Emergency_Phone_Number ");
            sb.AppendLine("FROM msdsonline_dbo.Doc_Vault_Agg ag ");
            sb.AppendLine("JOIN msdsonline_dbo.Document_Index di ON di.doc_index_id = ag.doc_index_id ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.Document_Data dd ON dd.doc_data_id = ag.doc_data_id ");
            sb.AppendLine("JOIN msdsonline_dbo.Logistics lo ON lo.doc_index_id = ag.doc_index_id ");
            sb.AppendLine("WHERE ag.Doc_Group_Num = ? ");
            sb.AppendLine("ORDER BY ag.Revision_Date DESC;");
//select di.*, lo.*, co.company_name
//from msdsonline_dbo.document_index di
//join msdsonline_dbo.logistics lo on lo.doc_index_id = di.doc_index_id
//join msdsonline_dbo.company co on co.company_code = di.company_code

            try {
                using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                    //get the rows
                    cmd.Parameters.AddWithValue("@msdsSerNo", msdsSerNo);
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);

                    using(OdbcDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            DateTime tempDate;

                            if (doc_index_id == 0) {
                                record = new MSDSRecord();

                                // Stuff the document types object into the record
                                record.DocumentTypes = new MSDSDocumentTypes();

                                // Get the HMIRS ID values
                                long.TryParse(rdr["doc_index_id"].ToString(), out doc_index_id);
                                record.doc_index_id = doc_index_id;
                                long.TryParse(rdr["doc_data_id"].ToString(), out doc_index_id);
                                record.doc_data_id = doc_index_id;

                                // Set the Hub msds_id to zero
                                record.msds_id = 0;
                                // Ignore the hcc_id on this end
                                //int.TryParse(rdr["hcc_id"].ToString(), out temp_id);
                                record.hcc_id = 0;
                                record.ARTICLE_IND = rdr["article_ind"].ToString();
                                record.CAGE = rdr["company_code"].ToString();
                                record.data_source_cd = "HMIRS";
                                record.DESCRIPTION = rdr["description_text"].ToString();
                                record.EMERGENCY_TEL = rdr["emergency_phone_number"].ToString();
                                record.END_COMP_IND = rdr["end_item_comp_ind"].ToString();
                                record.END_ITEM_IND = rdr["end_item_ind"].ToString();
                                record.HCC = rdr["hcc"].ToString();
                                if (record.HCC.Length == 0)
                                    record.HCC = UNKNOWN_HCC;
                                record.KIT_IND = rdr["kit_ind"].ToString();
                                record.KIT_PART_IND = rdr["kit_part_ind"].ToString();
                                record.MANUFACTURER = rdr["company_name"].ToString();
                                record.MANUFACTURER_MSDS_NO = rdr["manufacturer_serial_num"].ToString();
                                record.MIXTURE_IND = rdr["mix_ind"].ToString();
                                record.MSDSSERNO = rdr["doc_group_num"].ToString();
                                record.FSC = rdr["fsc"].ToString();
                                record.NIIN = rdr["iin"].ToString();
                                record.PARTNO = rdr["part_num"].ToString();
                                record.PRODUCT_IDENTITY = rdr["product_identity"].ToString();
                                record.PRODUCT_IND = "Y"; 
                                if (record.ARTICLE_IND.Equals("Y")) 
                                    record.PRODUCT_IND = "N";
                                record.PRODUCT_LANGUAGE = rdr["product_language_code"].ToString();
                                DateTime.TryParse(rdr["LOAD_DATE"].ToString(), out tempDate);
                                record.PRODUCT_LOAD_DATE =  tempDate;
                                record.PRODUCT_RECORD_STATUS = rdr["product_doc_status"].ToString();
                                record.PRODUCT_REVISION_NO = rdr["product_revision_num"].ToString();
                                record.PROPRIETARY_IND = rdr["proprietary_ind"].ToString();
                                record.PUBLISHED_IND = rdr["published_ind"].ToString();
                                record.PURCHASED_PROD_IND = rdr["purchased_product_ind"].ToString();
                                record.PURE_IND = rdr["pure_ind"].ToString();
                                record.RADIOACTIVE_IND = rdr["radioactive_ind"].ToString();
                                record.SERVICE_AGENCY_CODE = rdr["service_agency_code"].ToString();
                                record.TRADE_NAME = rdr["doc_trade_name"].ToString();
                                record.TRADE_SECRET_IND = rdr["trade_secret_ind"].ToString();
                            }

                            // Find the latest update
                            DateTime.TryParse(rdr["revision_date"].ToString(), out tempDate);
                            DateTime revision_date = tempDate;
                            DateTime.TryParse(rdr["di_updt"].ToString(), out tempDate);
                            DateTime di_updt = tempDate;
                            DateTime.TryParse(rdr["dd_updt"].ToString(), out tempDate);
                            DateTime dd_updt = tempDate;

                            record.last_update = new DateTime(
                                Math.Max(Math.Max(di_updt.Ticks, dd_updt.Ticks), revision_date.Ticks));  

                            // Build the doc file name
                            // Format: MSDSSERNO_DocType_RevisionDate_Country_Language_DocStatusCode.DocFormat
                            // Note this is differnet from the HILT file format -makes it easier to find the file
                            string country_code = rdr["country_code"].ToString();
                            string language = rdr["document_language_code"].ToString();
                            string doc_status = rdr["doc_status_code"].ToString();
                            string doc_type = rdr["document_type"].ToString();
                            string doc_file_format = rdr["doc_file_format"].ToString();

                            StringBuilder sbTemp = new StringBuilder(record.MSDSSERNO + "_");
                            sbTemp.Append(doc_type + "_");
                            sbTemp.Append(String.Format("{0:yyyyMMdd_}", revision_date));
                            sbTemp.Append(country_code.ToString() + "_");
                            sbTemp.Append(language + "_");
                            sbTemp.Append(doc_status + ".");
                            sbTemp.Append(doc_file_format);
                            long doc_data_id = 0;
                            long.TryParse(rdr["doc_data_id"].ToString(), out doc_data_id);

                            // Now determine where to stuff it
                            // Only keep the first MSDS document as the file name
                            if (doc_type.Equals("MSDS") && record.file_name == null) {
                                record.file_name = sbTemp.ToString();
                                record.doc_data_id = doc_data_id;
                            }
                            else if (doc_type.Equals("PROD SHEET")) {
                                record.DocumentTypes.product_sheet_filename = 
                                    sbTemp.ToString();
                                record.DocumentTypes.product_sheet_id = doc_data_id;
                            }
                            else if (doc_type.Equals("MFR. LABEL")) {
                                    record.DocumentTypes.manufacturer_label_filename =
                                    sbTemp.ToString();
                                record.DocumentTypes.manufacturer_label_id = doc_data_id;
                            }
                            else if (doc_type.Equals("TRANS CERT")) {
                                record.DocumentTypes.transportation_cert_filename = 
                                    sbTemp.ToString();
                                record.DocumentTypes.transportation_cert_id = doc_data_id;
                            }
                            else if (doc_type.Equals("MSDS, TRAN")) {
                                record.DocumentTypes.msds_translated_filename = 
                                    sbTemp.ToString();
                                record.DocumentTypes.msds_translated_id = doc_data_id;
                            }
                            else if (doc_type.Equals("OTHER DOCS")) {
                                record.DocumentTypes.other_docs_filename =
                                    sbTemp.ToString();
                                record.DocumentTypes.other_docs_id = doc_data_id;
                            }
                            else { // stuff it into the neshap column
                                record.DocumentTypes.neshap_comp_filename =
                                    sbTemp.ToString();
                                record.DocumentTypes.neshap_comp_id = doc_data_id;
                            }
                        }
                    }
                }

                // Get the subtable data
                if (record != null)
                    populateMSDSSubTableData(doc_index_id, lastUpdate, record, m_conn);
            }
            catch (Exception ex) {
                // Log error message
                System.Diagnostics.Debug.WriteLine("***** ERROR - " + msdsSerNo
                    + " - " + doc_index_id + " : " +  ex.Message);
            }

            return record;
        }

        private void populateMSDSSubTableData(long doc_index_id, DateTime lastUpdate,
                MSDSRecord record, OdbcConnection conn) {
            //MSDS CONTRACTOR LIST
            record.ContractorList = getMSDSContractorList(doc_index_id, m_conn);

            //MSDS DISPOSAL
            record.Disposal = getMSDSDisposalRecord(doc_index_id, m_conn);

            //MSDS DOC TYPES - done as part of the get master record method
            //record.DocumentTypes = getMSDSDocumentTypesRecord(doc_index_id, 
            //    record.MSDSSERNO, lastUpdate, m_conn);

            // MSDS INGREDIENTS
            record.IngredientsList = getMSDSIngredientsList(doc_index_id, m_conn);

            //MSDS ITEM DESCRIPTION
            record.ItemDescription = getMSDSItemDescriptionRecord(doc_index_id, m_conn);

            //MSDS LABEL INFO
            record.LabelInfo = getMSDSLabelInfoRecord(doc_index_id, m_conn);

            //MSDS PHYS CHEMICAL TABLE
            record.PhysChemical = getMSDSPhysChemicalRecord(doc_index_id, m_conn);

            //MSDS RADIOLOGICAL INFO
            record.RadiologicalInfoList = getMSDSRadiologicalInfoRecord(doc_index_id, m_conn);

            //MSDS TRANSPORTATION
            record.Transportation = getMSDSTransportationRecord(doc_index_id, m_conn);

            if (record.Transportation != null) {
                // AFJM_PSN
                if (record.Transportation.AFJM_PSN_CODE != null)
                    record.Afjm_Psn = getMSDSAFJM_PSNRecord(record.Transportation.AFJM_PSN_CODE, m_conn);

                //MSDS DOT PSN
                if (record.Transportation.DOT_PSN_CODE != null)
                    record.Dot_Psn = getMSDSDOT_PSNRecord(record.Transportation.DOT_PSN_CODE, m_conn);

                //MSDS IATA PSN
                if (record.Transportation.IATA_PSN_CODE != null)
                    record.Iata_Psn = getMSDSIATA_PSNRecord(record.Transportation.IATA_PSN_CODE, m_conn);

                //MSDS IMO PSN
                if (record.Transportation.IMO_PSN_CODE != null)
                    record.Imo_Psn = getMSDSIMO_PSNRecord(record.Transportation.IMO_PSN_CODE, m_conn);
            }
        }

        private MSDS_AFJM_PSN getMSDSAFJM_PSNRecord(string psnCode, OdbcConnection conn) {
            MSDS_AFJM_PSN record = null;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.ref_afjm_transportation WHERE psn_code = ?";

            using(OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@psnCode", psnCode);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDS_AFJM_PSN();

                        record.AFJM_HAZARD_CLASS = rdr["UN_CLASS_CODE"].ToString();
                        record.AFJM_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.AFJM_PACK_PARAGRAPH = rdr["BASIC_PACK_REFERENCE"].ToString();
                        record.AFJM_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.AFJM_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.AFJM_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.AFJM_SPECIAL_PROV = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.AFJM_SUBSIDIARY_RISK = rdr["SUBSIDIARY_RISK"].ToString();
                        record.AFJM_SYMBOLS = rdr["SYMBOLS"].ToString();
                        record.AFJM_UN_ID_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                    }
                }
            }

            return record;
        }

        private List<MSDSContractorInfo> getMSDSContractorList(long doc_index_id, OdbcConnection conn) {
            List<MSDSContractorInfo> list = new List<MSDSContractorInfo>();

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SELECT ot.*, ");
            sb.AppendLine("comp.po_box, comp.address_line_1, comp.city, comp.state_code, ");
            sb.AppendLine("comp.zip_code_5, comp.country_code, comp.phone_number ");
            sb.AppendLine("FROM msdsonline_dbo.order_tracking ot ");
            sb.AppendLine("JOIN msdsonline_dbo.company comp ON comp.company_code = ot.company_code ");
            sb.AppendLine("WHERE ot.doc_index_id = ?; ");

            using(OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        // Create the record
                        MSDSContractorInfo record = new MSDSContractorInfo();

                        record.CT_CAGE = rdr["COMPANY_CODE"].ToString();
                        record.CT_CITY = rdr["CITY"].ToString();
                        record.CT_COMPANY_NAME = rdr["COMPANY_NAME"].ToString();
                        record.CT_COUNTRY = rdr["COUNTRY_CODE"].ToString();
                        record.CT_NUMBER = rdr["CONTRACT_NUM"].ToString();
                        record.CT_PHONE = rdr["PHONE_NUMBER"].ToString();
                        record.CT_PO_BOX = rdr["PO_BOX"].ToString();
                        record.CT_ADDRESS_1 = rdr["ADDRESS_LINE_1"].ToString();
                        record.CT_STATE = rdr["STATE_CODE"].ToString();
                        record.CT_ZIP_CODE = rdr["ZIP_CODE_5"].ToString();
                        record.PURCHASE_ORDER_NO = rdr["PURCHASE_ORDER_NUM"].ToString();

                        // Add it to the list
                        list.Add(record);
                    }
                }
            }

            return list;
        }

        private MSDSDisposal getMSDSDisposalRecord(long doc_index_id, OdbcConnection conn) {
            MSDSDisposal record = null;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT di.*,ref.EPA_HAZARDOUS_WASTE_NAME ");
            sb.AppendLine("FROM msdsonline_dbo.disposal di ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.ref_disposal ref ");
            sb.AppendLine("  ON ref.EPA_HAZARDOUS_WASTE_CODE = di.EPA_HAZARDOUS_WASTE_CODE ");
            sb.AppendLine("WHERE di.doc_index_id = ?");

            using(OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using(OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDSDisposal();

                        record.DISPOSAL_ADD_INFO = rdr["SUPPLEMENTAL_DATA_TEXT"].ToString();
                        record.EPA_HAZ_WASTE_CODE = rdr["EPA_HAZARDOUS_WASTE_CODE"].ToString();
                        record.EPA_HAZ_WASTE_IND = rdr["EPA_HAZARDOUS_WASTE_IND"].ToString();
                        record.EPA_HAZ_WASTE_NAME = rdr["EPA_HAZARDOUS_WASTE_NAME"].ToString();
                    }
                }
            }

            return record;
        }

        // MSDSDocumentTypes are extracted as part of the get master record

        private MSDS_DOT_PSN getMSDSDOT_PSNRecord(string psnCode, OdbcConnection conn) {
            MSDS_DOT_PSN record = null;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.ref_dot_transportation WHERE psn_code = ?";

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@psnCode", psnCode);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDS_DOT_PSN();

                        record.DOT_HAZARD_CLASS_DIV = rdr["UN_HAZARD_CLASS"].ToString();
                        record.DOT_HAZARD_LABEL = rdr["LABEL"].ToString();
                        record.DOT_MAX_CARGO = rdr["MAX_QTY_CARGO"].ToString();
                        record.DOT_MAX_PASSENGER = rdr["MAX_QTY_PASSENGER"].ToString();
                        record.DOT_PACK_BULK = rdr["BULK_PACKAGING"].ToString();
                        record.DOT_PACK_EXCEPTIONS = rdr["PACKAGING_EXCEPTIONS"].ToString();
                        record.DOT_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.DOT_PACK_NONBULK = rdr["NON_BULK_PACKAGING"].ToString();
                        record.DOT_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.DOT_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.DOT_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.DOT_SPECIAL_PROVISION = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.DOT_SYMBOLS = rdr["SYMBOLS"].ToString();
                        record.DOT_UN_ID_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                        record.DOT_WATER_OTHER_REQ = rdr["WATER_SHIP_OTHER_REQ"].ToString();
                        record.DOT_WATER_VESSEL_STOW = rdr["VESSEL_STOWAGE_REQ"].ToString();
                   }
                }
            }

            return record;
        }

        private MSDS_IATA_PSN getMSDSIATA_PSNRecord(string psnCode, OdbcConnection conn) {
            MSDS_IATA_PSN record = null;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.ref_iata_transportation WHERE psn_code = ?";

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@psnCode", psnCode);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDS_IATA_PSN();

                        record.IATA_CARGO_PACK_MAX_QTY = rdr["MAX_QTY_CARGO"].ToString();
                        record.IATA_CARGO_PACKING = rdr["PACKING_NOTE_CARGO"].ToString();
                        record.IATA_HAZARD_CLASS = rdr["UN_HAZARD_CLASS"].ToString();
                        record.IATA_HAZARD_LABEL = rdr["LABEL"].ToString();
                        record.IATA_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.IATA_PASS_AIR_MAX_QTY = rdr["MAX_QTY_PASSENGER"].ToString();
                        record.IATA_PASS_AIR_PACK_LMT_INSTR = rdr["AIR_LIM_QTY_PACKING_INST"].ToString();
                        record.IATA_PASS_AIR_PACK_LMT_PER_PKG = rdr["AIR_LIM_QTY_PACKING_MAX"].ToString();
                        record.IATA_PASS_AIR_PACK_NOTE = rdr["AIR_PACKING_NOTE_INSTRUCTIONS"].ToString();
                        record.IATA_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.IATA_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.IATA_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.IATA_SPECIAL_PROV = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.IATA_SUBSIDIARY_RISK = rdr["SUBSIDIARY_RISK_CLASS"].ToString();
                        record.IATA_UN_ID_NUMBER = rdr["UN_ID_NUMBER"].ToString();

                        rdr.Close();
                    }
                }
            }

            return record;
        }

        private MSDS_IMO_PSN getMSDSIMO_PSNRecord(string psnCode, OdbcConnection conn) {
            MSDS_IMO_PSN record = null;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.ref_imo_transportation WHERE psn_code = ?";

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@psnCode", psnCode);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDS_IMO_PSN();

                        record.IMO_EMS_NO = rdr["EMS_NUMBER"].ToString();
                        record.IMO_HAZARD_CLASS = rdr["UN_HAZARD_CLASS"].ToString();
                        record.IMO_IBC_INSTR = rdr["IBC_INSTRUCTIONS"].ToString();
                        record.IMO_IBC_PROVISIONS = rdr["IBC_PROVISIONS"].ToString();
                        record.IMO_LIMITED_QTY = rdr["LIMITED_QUANTITY"].ToString();
                        record.IMO_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.IMO_PACK_INSTRUCTIONS = rdr["PACKING_INSTRUCTIONS"].ToString();
                        record.IMO_PACK_PROVISIONS = rdr["PACKING_PROVISIONS"].ToString();
                        record.IMO_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.IMO_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.IMO_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.IMO_SPECIAL_PROV = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.IMO_STOW_SEGR = rdr["STOWAGE_AND_SEGREGATION"].ToString();
                        record.IMO_SUBSIDIARY_RISK = rdr["SUBSIDIARY_RISK_CLASS"].ToString();
                        record.IMO_TANK_INSTR_IMO = rdr["TANK_INSTRUCTIONS"].ToString();
                        record.IMO_TANK_INSTR_PROV = rdr["TANK_INSTRUCTIONS_PROVISIONS"].ToString();
                        record.IMO_TANK_INSTR_UN = rdr["TANK_INSTRUCTIONS_UN"].ToString();
                        record.IMO_UN_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                    }
                }
            }

            return record;
        }

        private List<MSDSIngredient> getMSDSIngredientsList(long doc_index_id, OdbcConnection conn) {
            List<MSDSIngredient> list = new List<MSDSIngredient>();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT comp.*, ing.ACGIH_STEL, ing.ACGIH_TLV, ");
            sb.AppendLine("ing.DOT_REPORTABLE_QTY, ing.EPA_REPORTABLE_QTY, ");
            sb.AppendLine("ing.CHEMICAL_NAME, ing.OZONE_DEPLETING_SUBSTANCE_CODE, ");
            sb.AppendLine("ing.OSHA_PERMISSIBLE_EXP_LIMIT, ing.OSHA_SHORT_TERM_EXP_LIMIT ");
            sb.AppendLine("FROM msdsonline_dbo.composition comp ");
            sb.AppendLine("JOIN msdsonline_dbo.ref_ingredients ing ON ing.CAS_NUMBER = comp.CAS_NUMBER ");
            sb.AppendLine("WHERE comp.doc_index_id = ?;"); 


            using(OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        // Create the record
                        MSDSIngredient record = new MSDSIngredient();

                        record.ACGIH_STEL = rdr["ACGIH_STEL"].ToString();
                        record.ACGIH_TLV = rdr["ACGIH_TLV"].ToString();
                        record.CAS = rdr["CAS_NUMBER"].ToString();
                        record.CHEM_MFG_COMP_NAME = rdr["COMPANY_NAME"].ToString();
                        record.DOT_REPORT_QTY = rdr["DOT_REPORTABLE_QTY"].ToString();
                        record.EPA_REPORT_QTY = rdr["EPA_REPORTABLE_QTY"].ToString();
                        record.INGREDIENT_NAME = rdr["CHEMICAL_NAME"].ToString();
                        record.ODS_IND = rdr["OZONE_DEPLETING_SUBSTANCE_CODE"].ToString();
                        record.OSHA_PEL = rdr["OSHA_PERMISSIBLE_EXP_LIMIT"].ToString();
                        record.OSHA_STEL = rdr["OSHA_SHORT_TERM_EXP_LIMIT"].ToString();
                        record.OTHER_REC_LIMITS = rdr["OTHER_RECORDED_LIMITS"].ToString();
                        record.PRCNT = rdr["PERCENT_Text"].ToString();
                        record.RTECS_CODE = rdr["RTECS_CODE"].ToString();
                        record.RTECS_NUM = rdr["RTECS_NUMBER"].ToString();

                        // Build the weight & vol ranges
                        string sLow = "L: ";
                        string sHigh = "; H: ";

                        double num = 0;
                        string sLowVal = rdr["PERCENT_LOW_RANGE_VOLUME"].ToString();
                        if (double.TryParse(sLowVal, out num)) 
                            sLowVal = String.Format("{0:N2}", num);
                        string sHighVal = rdr["PERCENT_HIGH_RANGE_VOLUME"].ToString();
                        if (double.TryParse(sHighVal, out num)) 
                            sHighVal = String.Format("{0:N2}", num);

                        record.PRCNT_VOL_VALUE = sLow + sLowVal + sHigh + sHighVal;
                        
                        sLowVal = rdr["PERCENT_LOW_RANGE_WEIGHT"].ToString();
                        if (double.TryParse(sLowVal, out num)) 
                            sLowVal = String.Format("{0:N2}", num);
                        sHighVal = rdr["PERCENT_HIGH_RANGE_WEIGHT"].ToString();
                        if (double.TryParse(sHighVal, out num)) 
                            sHighVal = String.Format("{0:N2}", num);

                        record.PRCNT_VOL_WEIGHT = sLow + sLowVal + sHigh + sHighVal;

                        // Add it to the list
                        list.Add(record);
                    }
                }
            }

            return list;
        }

        private MSDSItemDescription getMSDSItemDescriptionRecord(long doc_index_id, OdbcConnection conn) {
            MSDSItemDescription record = null;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT lo.*, "); 
            sb.AppendLine("flis.ITEM_MANAGER, flis.ITEM_NAME, flis.FLIS_VERIFIED_IND, ");
            sb.AppendLine("flis.QUANTITATIVE_EXPRESSION, flis.SHELF_LIFE_CODE, ");
            sb.AppendLine("flis.SPECIFICATION_NUM, flis.UNIT_OF_ISSUE_CODE, ");
            sb.AppendLine("upc.BATCH_NUM, upc.LOT_NUM, upc.UPC, ");
            sb.AppendLine("se.SPECIAL_EMPHASIS_CODE ");
            sb.AppendLine("FROM msdsonline_dbo.logistics lo ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.flis_logistics flis ON flis.iin = lo.iin ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.upc_lot_batch_number upc ");
            sb.AppendLine("  ON upc.doc_index_id = lo.doc_index_id ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.special_emphasis se ");
            sb.AppendLine("  ON se.doc_index_id = lo.doc_index_id ");
            sb.AppendLine("WHERE lo.doc_index_id = ?;");

            using(OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDSItemDescription();
                        int temp_id = 0;

                        record.BATCH_NUMBER = rdr["BATCH_NUM"].ToString();
                        record.ITEM_MANAGER = rdr["ITEM_MANAGER"].ToString();
                        record.ITEM_NAME = rdr["ITEM_NAME"].ToString();
                        record.LOG_FLIS_NIIN_VER = rdr["FLIS_VERIFIED_IND"].ToString();
                        int.TryParse(rdr["FSC"].ToString(), out temp_id);
                        record.LOG_FSC = temp_id;
                        record.LOT_NUMBER = rdr["LOT_NUM"].ToString();
                        record.NET_UNIT_WEIGHT = rdr["NET_UNIT_WEIGHT"].ToString();
                        record.QUANTITATIVE_EXPRESSION = rdr["QUANTITATIVE_EXPRESSION"].ToString();
                        record.SHELF_LIFE_CODE = rdr["SHELF_LIFE_CODE"].ToString();
                        record.SPECIAL_EMP_CODE = rdr["SPECIAL_EMPHASIS_CODE"].ToString();
                        record.SPECIFICATION_NUMBER = rdr["SPECIFICATION_NUM"].ToString();
                        record.TYPE_GRADE_CLASS = rdr["SPECIFICATION_TYPE_GRADE_CLASS"].ToString();
                        record.TYPE_OF_CONTAINER = rdr["TYPE_CONTAINER"].ToString();
                        record.UI_CONTAINER_QTY = rdr["UNIT_OF_ISSUE_CONTAINER_QTY"].ToString();
                        record.UN_NA_NUMBER = rdr["UN_NA_NUMBER"].ToString();
                        record.UNIT_OF_ISSUE = rdr["UNIT_OF_ISSUE_CODE"].ToString();
                        record.UPC_GTIN = rdr["UPC"].ToString();
                    }
                }
            }

            return record;
        }

        protected internal MSDSLabelInfo getMSDSLabelInfoRecord(long doc_index_id, OdbcConnection conn) {
            MSDSLabelInfo record = null;

            StringBuilder sb = new StringBuilder();
            //NOTE: The doc_index_agg table has mis-matches between the Specification_Num
            // Code below will work, but to be safe we'll use the JOIN query
            //sb.AppendLine("SELECT lab.*, di.company_code, di.company_name, di.item_name, ");
            //sb.AppendLine("di.product_identity, di.doc_group_num, di.stock_number ");
            //sb.AppendLine("FROM msdsonline_dbo.labeling lab ");
            //sb.AppendLine("JOIN msdsonline_dbo.doc_index_agg di ON di.doc_index_id = lab.doc_index_id ");
            //sb.AppendLine("WHERE lab.doc_index_id = ?;");
            sb.AppendLine("SELECT lab.*, di.company_code, co.company_name, di.doc_group_num, ");
            sb.AppendLine("lo.product_identity, lo.stock_number, fl.item_name ");
            sb.AppendLine("FROM msdsonline_dbo.document_index di ");
            sb.AppendLine("JOIN msdsonline_dbo.logistics lo ON lo.doc_index_id = di.doc_index_id ");
            sb.AppendLine("JOIN msdsonline_dbo.flis_logistics fl ON fl.iin = lo.iin AND fl.iin_type = lo.iin_type");
            sb.AppendLine("JOIN msdsonline_dbo.company co ON co.company_code = di.company_code ");
            sb.AppendLine("JOIN msdsonline_dbo.labeling lab ON lab.doc_index_id = di.doc_index_id ");
            sb.AppendLine("WHERE lab.doc_index_id = ?;");

            using(OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDSLabelInfo();

                        record.COMPANY_CAGE_RP = rdr["COMPANY_CODE"].ToString();
                        record.COMPANY_NAME_RP = rdr["COMPANY_NAME"].ToString();
                        record.LABEL_EMERG_PHONE = rdr["HEALTH_EMERGENCY_PHONE_NUM"].ToString();
                        record.LABEL_ITEM_NAME = rdr["ITEM_NAME"].ToString();
                        record.LABEL_PROC_YEAR = rdr["YEAR_PROCURED"].ToString();
                        record.LABEL_PROD_IDENT = rdr["PRODUCT_IDENTITY"].ToString();
                        record.LABEL_PROD_SERIALNO = rdr["DOC_GROUP_NUM"].ToString();
                        record.LABEL_SIGNAL_WORD_CODE = rdr["SIGNAL_WORD_CODE"].ToString();
                        record.LABEL_STOCK_NO = rdr["STOCK_NUMBER"].ToString();
                        record.SPECIFIC_HAZARDS = rdr["SPECIFIC_HAZARDS_TEXT"].ToString();
                    }
                }
            }

            return record;
        }

        private MSDSPhysChemical getMSDSPhysChemicalRecord(long doc_index_id, OdbcConnection conn) {
            MSDSPhysChemical record = null;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.phys_chem WHERE doc_index_id = ?";

            using(OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDSPhysChemical();

                        record.APP_ODOR = rdr["APPEARANCE_ODOR_TEXT"].ToString();
                        record.AUTOIGNITION_TEMP = rdr["AUTOIGNITION_TEMP"].ToString();
                        record.CARCINOGEN_IND = rdr["CARCINOGEN_IND"].ToString();
                        record.EPA_ACUTE = rdr["EPA_IMMEDIATE_ACUTE"].ToString();
                        record.EPA_CHRONIC = rdr["EPA_DELAYED_CHRONIC"].ToString();
                        record.EPA_FIRE = rdr["EPA_FIRE"].ToString();
                        record.EPA_PRESSURE = rdr["EPA_SUDDEN_RELEASE_OF_PRESSURE"].ToString();
                        record.EPA_REACTIVITY = rdr["EPA_REACTIVE"].ToString();
                        record.EVAP_RATE_REF = rdr["EVAPORATION_RATE"].ToString();
                        record.FLASH_PT_TEMP = rdr["FLASH_POINT_TEMP"].ToString();
                        record.NEUT_AGENT = rdr["NEUTRALIZING_AGENT_TEXT"].ToString();
                        record.NFPA_FLAMMABILITY = rdr["NFPA_FLAMMABILITY"].ToString();
                        record.NFPA_HEALTH = rdr["NFPA_HEALTH"].ToString();
                        record.NFPA_REACTIVITY = rdr["NFPA_REACTIVITY"].ToString();
                        record.NFPA_SPECIAL = rdr["NFPA_SPECIAL"].ToString();
                        record.OSHA_CARCINOGENS = rdr["OSHA_CARCINOGENS"].ToString();
                        record.OSHA_COMB_LIQUID = rdr["OSHA_COMBUSTION_LIQUID"].ToString();
                        record.OSHA_COMP_GAS = rdr["OSHA_COMPRESSED_GAS"].ToString();
                        record.OSHA_CORROSIVE = rdr["OSHA_CORROSIVE"].ToString();
                        record.OSHA_EXPLOSIVE = rdr["OSHA_EXPLOSIVE"].ToString();
                        record.OSHA_FLAMMABLE = rdr["OSHA_FLAMMABLE"].ToString();
                        record.OSHA_HIGH_TOXIC = rdr["OSHA_HIGHLY_TOXIC"].ToString();
                        record.OSHA_IRRITANT = rdr["OSHA_IRRITANT"].ToString();
                        record.OSHA_ORG_PEROX = rdr["OSHA_ORGANIC_PEROXIDE"].ToString();
                        record.OSHA_OTHERLONGTERM = rdr["OSHA_OTHER_LT"].ToString();
                        record.OSHA_OXIDIZER = rdr["OSHA_OXIDIZER"].ToString();
                        record.OSHA_PYRO = rdr["OSHA_PYROPHORIC"].ToString();
                        record.OSHA_SENSITIZER = rdr["OSHA_SENSITIZER"].ToString();
                        record.OSHA_TOXIC = rdr["OSHA_TOXIC"].ToString();
                        record.OSHA_UNST_REACT = rdr["OSHA_UNSTABLE_REACTIVE"].ToString();
                        record.OSHA_WATER_REACTIVE = rdr["OSHA_WATER_REACTIVE"].ToString();
                        record.OTHER_SHORT_TERM = rdr["OSHA_OTHER_ST"].ToString();
                        record.PERCENT_VOL_VOLUME = rdr["PERCENT_VOLATILES_BY_VOLUME"].ToString();
                        record.PH = rdr["PH"].ToString();
                        record.PHYS_STATE_CODE = rdr["PHYSICAL_STATE_CODE"].ToString();
                        record.SOL_IN_WATER = rdr["SOLUBILITY_IN_WATER"].ToString();
                        record.SPECIFIC_GRAV = rdr["SPECIFIC_GRAVITY"].ToString();
                        record.VAPOR_DENS = rdr["VAPOR_DENSITY"].ToString();
                        record.VAPOR_PRESS = rdr["VAPOR_PRESSURE"].ToString();
                        record.VISCOSITY = rdr["VISCOSITY"].ToString();
                        record.VOC_GRAMS_LITER = rdr["VOC_GRAMS_PER_LITER"].ToString();
                        record.VOC_POUNDS_GALLON = rdr["VOC_POUNDS_PER_GALLON"].ToString();
                        record.VOL_ORG_COMP_WT = rdr["VOC_WEIGHT_PERCENTAGE"].ToString();
                    }
                }
            }

            return record;
        }

        private List<MSDSRadiologicalInfo> getMSDSRadiologicalInfoRecord(long doc_index_id, OdbcConnection conn) {
            List<MSDSRadiologicalInfo> list = new List<MSDSRadiologicalInfo>();

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SELECT rad.*, nuc.license_number, ");
            sb.AppendLine("lo.form, lo.sealed, flis.replacement_nsn ");
            sb.AppendLine("FROM msdsonline_dbo.radioactivity rad ");
            sb.AppendLine("JOIN msdsonline_dbo.logistics lo ON lo.doc_index_id = rad.doc_index_id ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.flis_logistics flis ON flis.iin = lo.iin ");
            sb.AppendLine("LEFT JOIN msdsonline_dbo.nuclear_reg_comm_licenses nuc ON nuc.doc_index_id = rad.doc_index_id ");
            sb.AppendLine("WHERE rad.doc_index_id = ?; ");

            using(OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        MSDSRadiologicalInfo record = new MSDSRadiologicalInfo();

                        record.NRC_LP_NUM = rdr["LICENSE_NUMBER"].ToString();
                        record.OPERATOR = rdr["OPERATOR"].ToString();
                        record.RAD_AMOUNT_MICRO = rdr["AMOUNT"].ToString();
                        record.RAD_CAS = rdr["RADIOISOTOPE_CAS"].ToString();
                        record.RAD_FORM = rdr["FORM"].ToString();
                        record.RAD_NAME = rdr["NAME"].ToString();
                        record.RAD_SYMBOL = rdr["SYMBOL"].ToString();
                        record.REP_NSN = rdr["REPLACEMENT_NSN"].ToString();
                        record.SEALED = rdr["SEALED"].ToString();

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        private MSDSTransportation getMSDSTransportationRecord(long doc_index_id, OdbcConnection conn) {
            MSDSTransportation record = null;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.transportation WHERE doc_index_id = ?";

            using(OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@doc_index_id", doc_index_id);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    if (rdr.Read()) {
                        record = new MSDSTransportation();
                        int temp_id = -1;

                        record.AFJM_PSN_CODE = rdr["AFJM_PSN_CODE"].ToString();
                        record.DOT_PSN_CODE = rdr["DOT_PSN_CODE"].ToString();
                        record.IATA_PSN_CODE = rdr["IATA_PSN_CODE"].ToString();
                        record.IMO_PSN_CODE = rdr["IMO_PSN_CODE"].ToString();
                        record.AF_MMAC_CODE = rdr["AF_MMAC_CODE"].ToString();
                        record.CERTIFICATE_COE = rdr["COE"].ToString();
                        record.COMPETENT_CAA = rdr["CAA"].ToString();
                        record.DOD_ID_CODE = rdr["DOD_ID_CODE"].ToString();
                        record.DOT_EXEMPTION_NO = rdr["DOT_EXEMPTION_NUM"].ToString();
                        record.DOT_RQ_IND = rdr["DOT_REPORTABLE_QTY_IND"].ToString();
                        record.EX_NO = rdr["EX_NUM"].ToString();
                        int.TryParse(rdr["HIGH_EXPLOSIVE_WEIGHT"].ToString(), out temp_id);
                        record.HIGH_EXPLOSIVE_WT = temp_id;
                        record.LTD_QTY_IND = rdr["LIMITED_QTY_IND"].ToString();
                        record.MAGNETIC_IND = rdr["MAGNETIC_IND"].ToString();
                        record.MAGNETISM = rdr["MAGNETISM"].ToString();
                        record.MARINE_POLLUTANT_IND = rdr["MARINE_POLLUTANT_IND"].ToString();
                        int.TryParse(rdr["NET_EXPLOSIVE_QTY_DIST_WEIGHT"].ToString(), out temp_id);
                        record.NET_EXP_QTY_DIST = temp_id;
                        record.NET_EXP_WEIGHT = rdr["NET_EXPLOSIVE_WEIGHT"].ToString();
                        record.NET_PROPELLANT_WT = rdr["NET_PROPELLANT_WEIGHT"].ToString();
                        record.NOS_TECHNICAL_SHIPPING_NAME = rdr["TECH_ENTRY_NOS_SHIPPING_NAME"].ToString();
                        record.TRANSPORTATION_ADDITIONAL_DATA = rdr["ADDITIONAL_DATA"].ToString();
                    }
                }
            }

            return record;
        }

        public Dictionary<string, string> getMSDSDoc_IDMapping() {
            Dictionary<string, string> list = new Dictionary<string, string>();

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT di.doc_group_num, dd.document_type,");
            sb.AppendLine("dd.doc_data_id, dd.revision_date");
            sb.AppendLine("FROM msdsonline_dbo.document_data dd");
            sb.AppendLine("JOIN msdsonline_dbo.document_product_mapping dpm ON dpm.doc_data_id = dd.doc_data_id");
            sb.AppendLine("JOIN msdsonline_dbo.document_index di ON di.doc_index_id = dpm.doc_index_id");
            sb.AppendLine("ORDER BY dd.doc_data_id;");

            StringBuilder doc_key = new StringBuilder();
            StringBuilder data_key = new StringBuilder();

            using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        doc_key.Clear();
                        data_key.Clear();

                        long data_id = 0;
                        long.TryParse(rdr["doc_data_id"].ToString(), out data_id);

                        string msdsSerNo = rdr["doc_group_num"].ToString();
                        string doc_type = rdr["document_type"].ToString();

                        DateTime rev_date;
                        DateTime.TryParse(rdr["revision_date"].ToString(), out rev_date);
                        string revDate = String.Format("{0:yyyyMMdd_}", rev_date);

                        doc_key.Append(msdsSerNo + "_");
                        doc_key.Append(doc_type + "_");
                        doc_key.Append(revDate);

                        data_key.Append(data_id.ToString() + "_");
                        data_key.Append(doc_type + "_");
                        data_key.Append(revDate);

                        list.Add(doc_key.ToString(), data_key.ToString());
                    }
                }
            }

            return list;
        }

        public List<HMIRSDocumentRecord> getDocDataMapping(int diskNumber) {
            List<HMIRSDocumentRecord> list = new List<HMIRSDocumentRecord>();

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT dd.*, dv.doc_file_format");
            sb.AppendLine("FROM msdsonline_dbo.document_data dd");
            sb.AppendLine("JOIN msdsonline_dbo.document_vault dv ON dv.doc_data_id = dd.doc_data_id");
            sb.AppendLine("JOIN msdsonline_dbo.diskmapping dm ON dv.doc_data_id");
            sb.AppendLine("  BETWEEN dm.begin_doc_number AND dm.end_doc_number");
            sb.AppendLine("WHERE dm.disk_number = ?");
            sb.AppendLine("ORDER BY dd.doc_data_id");

            StringBuilder sbFile = new StringBuilder();

            using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                cmd.Parameters.AddWithValue("@diskNumber", diskNumber);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        HMIRSDocumentRecord record = new HMIRSDocumentRecord();
                        long data_id = 0;
                        long.TryParse(rdr["doc_data_id"].ToString(), out data_id);
                        record.doc_data_id = data_id;
                        DateTime rev_date;
                        DateTime.TryParse(rdr["revision_date"].ToString(), out rev_date);
                        record.revision_date = rev_date;
                        record.document_type = rdr["document_type"].ToString();
                        record.country_code = rdr["country_code"].ToString();
                        record.language_code = rdr["language_code"].ToString();
                        record.doc_status_code = rdr["doc_status_code"].ToString();
                        record.doc_format = rdr["doc_file_format"].ToString();
                        int temp_id = 0;
                        int.TryParse(rdr["revision_num"].ToString(), out temp_id);
                        record.revision_num = temp_id;
                        record.proprietary_ind = rdr["proprietary_ind"].ToString();
                        record.manufacturer_serial_num = rdr["manufacturer_serial_num"].ToString();
                        int.TryParse(rdr["manufacturer_revision_num"].ToString(), out temp_id);
                        record.manufacturer_revision_num = temp_id;

                        sbFile.Clear();
                        sbFile.Append(data_id.ToString() + "_");
                        sbFile.Append(record.document_type + "_");
                        sbFile.Append(String.Format("{0:yyyyMMdd_}", rev_date));
                        sbFile.Append(record.country_code + "_");
                        sbFile.Append(record.language_code + "_");
                        sbFile.Append(record.doc_status_code + ".");
                        sbFile.Append(record.doc_format);

                        record.doc_file_name = sbFile.ToString();

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        //TODO - may not need this w/new logic in getNewDocumentList
        public List<HMIRSDocumentRecord> getDocIDsForSerialNum(string serialNum) {
            List<HMIRSDocumentRecord> list = new List<HMIRSDocumentRecord>();

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            //TODO - replace doc_vaut_agg with individual tables
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT dd.*, dv.doc_file_format");
            sb.AppendLine("FROM msdsonline_dbo.document_data dd");
            sb.AppendLine("JOIN msdsonline_dbo.document_vault dv ON dv.doc_data_id = dd.doc_data_id");
            sb.AppendLine("JOIN msdsonline_dbo.doc_vault_agg agg ON agg.doc_data_id = dd.doc_data_id");
            sb.AppendLine("WHERE agg.doc_group_num = ?");

            StringBuilder sbFile = new StringBuilder();

            using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                cmd.Parameters.AddWithValue("@serialNum", serialNum);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {

                    while (rdr.Read()) {
                        HMIRSDocumentRecord record = new HMIRSDocumentRecord();
                        long data_id = 0;
                        long.TryParse(rdr["doc_data_id"].ToString(), out data_id);
                        record.doc_data_id = data_id;
                        DateTime rev_date;
                        DateTime.TryParse(rdr["revision_date"].ToString(), out rev_date);
                        record.revision_date = rev_date;
                        record.document_type = rdr["document_type"].ToString();
                        record.country_code = rdr["country_code"].ToString();
                        record.language_code = rdr["language_code"].ToString();
                        record.doc_status_code = rdr["doc_status_code"].ToString();
                        record.doc_format = rdr["doc_file_format"].ToString();

                        sbFile.Clear();
                        sbFile.Append(data_id.ToString() + "_");
                        sbFile.Append(record.document_type + "_");
                        sbFile.Append(String.Format("{0:yyyyMMdd_}", rev_date));
                        sbFile.Append(record.country_code + "_");
                        sbFile.Append(record.language_code + "_");
                        sbFile.Append(record.doc_status_code + ".");
                        sbFile.Append(record.doc_format);

                        record.doc_file_name = sbFile.ToString();

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        public List<int> getCDNumsToExtract(DateTime lastUpdate) {
            List<int> list = new List<int>();

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            //TODO - Replace call to doc_vault_agg with document_data - that's where the rev date really resides
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT DISTINCT dm.disk_number ");
            sb.AppendLine("FROM msdsonline_dbo.diskmapping dm, msdsonline_dbo.doc_vault_agg agg");
            sb.AppendLine("where agg.doc_data_id >= dm.begin_doc_number"); 
            sb.AppendLine("and agg.doc_data_id <= dm.end_doc_number");
            sb.AppendLine("and agg.revision_date > ?");
            sb.AppendLine("order by dm.disk_number");

            StringBuilder sbFile = new StringBuilder();

            using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                cmd.Parameters.AddWithValue("@revision_date", lastUpdate);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {

                    while (rdr.Read()) {
                        int disk_num = 0;
                        int.TryParse(rdr["disk_number"].ToString(), out disk_num);

                        list.Add(disk_num);
                    }
                }
            }

            return list;
        }
        #endregion

        #region HMIRS Support Table Import Processing
        protected internal List<MSDSContractorInfo> getUpdatedContractorInfo(DateTime startDate) {
            List<MSDSContractorInfo> list = new List<MSDSContractorInfo>(); ;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.Company WHERE Tmsp_Last_Updt > ?";

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@startDate", startDate);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        MSDSContractorInfo record = new MSDSContractorInfo();

                        record.CT_CAGE = rdr["COMPANY_CODE"].ToString();
                        record.CT_CITY = rdr["CITY"].ToString();
                        record.CT_COMPANY_NAME = rdr["COMPANY_NAME"].ToString();
                        record.CT_COUNTRY = rdr["COUNTRY_CODE"].ToString();
                        record.CT_ADDRESS_1 = rdr["ADDRESS_LINE_1"].ToString();
                        record.CT_PHONE = rdr["PHONE_NUMBER"].ToString();
                        record.CT_PO_BOX = rdr["PO_BOX"].ToString();
                        record.CT_STATE = rdr["STATE_CODE"].ToString();
                        record.CT_ZIP_CODE = rdr["ZIP_CODE_5"].ToString();
                        DateTime temp;
                        DateTime.TryParse(rdr["TMSP_LAST_UPDT"].ToString(), out temp);
                        record.TMSP_LAST_UPDT = temp;

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        protected internal List<MSDS_AFJM_PSN> getUpdatedAFJMRecords(DateTime startDate) {
            List<MSDS_AFJM_PSN> list = new List<MSDS_AFJM_PSN>(); ;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.Ref_AFJM_Transportation WHERE Tmsp_Last_Updt > ?";

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@startDate", startDate);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        MSDS_AFJM_PSN record = new MSDS_AFJM_PSN();

                        record.AFJM_HAZARD_CLASS = rdr["UN_CLASS_CODE"].ToString();
                        record.AFJM_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.AFJM_PACK_PARAGRAPH = rdr["BASIC_PACK_REFERENCE"].ToString();
                        record.AFJM_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.AFJM_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.AFJM_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.AFJM_SPECIAL_PROV = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.AFJM_SUBSIDIARY_RISK = rdr["SUBSIDIARY_RISK"].ToString();
                        record.AFJM_SYMBOLS = rdr["SYMBOLS"].ToString();
                        record.AFJM_UN_ID_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                        DateTime temp;
                        DateTime.TryParse(rdr["TMSP_LAST_UPDT"].ToString(), out temp);
                        record.TMSP_LAST_UPDT = temp;

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        protected internal List<MSDS_DOT_PSN> getUpdatedDOTRecords(DateTime startDate) {
            List<MSDS_DOT_PSN> list = new List<MSDS_DOT_PSN>(); ;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.Ref_DOT_Transportation WHERE Tmsp_Last_Updt > ?";

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@startDate", startDate);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        MSDS_DOT_PSN record = new MSDS_DOT_PSN();

                        record.DOT_HAZARD_CLASS_DIV = rdr["UN_HAZARD_CLASS"].ToString();
                        record.DOT_HAZARD_LABEL = rdr["LABEL"].ToString();
                        record.DOT_MAX_CARGO = rdr["MAX_QTY_CARGO"].ToString();
                        record.DOT_MAX_PASSENGER = rdr["MAX_QTY_PASSENGER"].ToString();
                        record.DOT_PACK_BULK = rdr["BULK_PACKAGING"].ToString();
                        record.DOT_PACK_EXCEPTIONS = rdr["PACKAGING_EXCEPTIONS"].ToString();
                        record.DOT_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.DOT_PACK_NONBULK = rdr["NON_BULK_PACKAGING"].ToString();
                        record.DOT_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.DOT_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.DOT_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.DOT_SPECIAL_PROVISION = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.DOT_SYMBOLS = rdr["SYMBOLS"].ToString();
                        record.DOT_UN_ID_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                        record.DOT_WATER_OTHER_REQ = rdr["WATER_SHIP_OTHER_REQ"].ToString();
                        record.DOT_WATER_VESSEL_STOW = rdr["VESSEL_STOWAGE_REQ"].ToString();
                        DateTime temp;
                        DateTime.TryParse(rdr["TMSP_LAST_UPDT"].ToString(), out temp);
                        record.TMSP_LAST_UPDT = temp;

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        protected internal List<MSDS_IATA_PSN> getUpdatedIATARecords(DateTime startDate) {
            List<MSDS_IATA_PSN> list = new List<MSDS_IATA_PSN>(); ;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.Ref_IATA_Transportation WHERE Tmsp_Last_Updt > ?";

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@startDate", startDate);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        MSDS_IATA_PSN record = new MSDS_IATA_PSN();

                        record.IATA_CARGO_PACK_MAX_QTY = rdr["MAX_QTY_CARGO"].ToString();
                        record.IATA_CARGO_PACKING = rdr["PACKING_NOTE_CARGO"].ToString();
                        record.IATA_HAZARD_CLASS = rdr["UN_HAZARD_CLASS"].ToString();
                        record.IATA_HAZARD_LABEL = rdr["LABEL"].ToString();
                        record.IATA_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.IATA_PASS_AIR_MAX_QTY = rdr["MAX_QTY_PASSENGER"].ToString();
                        record.IATA_PASS_AIR_PACK_LMT_INSTR = rdr["AIR_LIM_QTY_PACKING_INST"].ToString();
                        record.IATA_PASS_AIR_PACK_LMT_PER_PKG = rdr["AIR_LIM_QTY_PACKING_MAX"].ToString();
                        record.IATA_PASS_AIR_PACK_NOTE = rdr["AIR_PACKING_NOTE_INSTRUCTIONS"].ToString();
                        record.IATA_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.IATA_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.IATA_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.IATA_SPECIAL_PROV = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.IATA_SUBSIDIARY_RISK = rdr["SUBSIDIARY_RISK_CLASS"].ToString();
                        record.IATA_UN_ID_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                        DateTime temp;
                        DateTime.TryParse(rdr["TMSP_LAST_UPDT"].ToString(), out temp);
                        record.TMSP_LAST_UPDT = temp;

                        list.Add(record);
                    }
                }
            }

            return list;
        }

        protected internal List<MSDS_IMO_PSN> getUpdatedIMORecords(DateTime startDate) {
            List<MSDS_IMO_PSN> list = new List<MSDS_IMO_PSN>(); ;

            string sqlStmt = "SELECT * FROM msdsonline_dbo.Ref_IMO_Transportation WHERE Tmsp_Last_Updt > ?";

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            using (OdbcCommand cmd = new OdbcCommand(sqlStmt, m_conn)) {
                //get the row
                cmd.Parameters.AddWithValue("@startDate", startDate);

                using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                    while (rdr.Read()) {
                        MSDS_IMO_PSN record = new MSDS_IMO_PSN();

                        record.IMO_EMS_NO = rdr["EMS_NUMBER"].ToString();
                        record.IMO_HAZARD_CLASS = rdr["UN_HAZARD_CLASS"].ToString();
                        record.IMO_IBC_INSTR = rdr["IBC_INSTRUCTIONS"].ToString();
                        record.IMO_IBC_PROVISIONS = rdr["IBC_PROVISIONS"].ToString();
                        record.IMO_LIMITED_QTY = rdr["LIMITED_QUANTITY"].ToString();
                        record.IMO_PACK_GROUP = rdr["UN_PACKING_GROUP"].ToString();
                        record.IMO_PACK_INSTRUCTIONS = rdr["PACKING_INSTRUCTIONS"].ToString();
                        record.IMO_PACK_PROVISIONS = rdr["PACKING_PROVISIONS"].ToString();
                        record.IMO_PROP_SHIP_MODIFIER = rdr["PSN_MODIFIER"].ToString();
                        record.IMO_PROP_SHIP_NAME = rdr["PSN_NAME"].ToString();
                        record.IMO_PSN_CODE = rdr["PSN_CODE"].ToString();
                        record.IMO_SPECIAL_PROV = rdr["SPECIAL_PROVISIONS"].ToString();
                        record.IMO_STOW_SEGR = rdr["STOWAGE_AND_SEGREGATION"].ToString();
                        record.IMO_SUBSIDIARY_RISK = rdr["SUBSIDIARY_RISK_CLASS"].ToString();
                        record.IMO_TANK_INSTR_IMO = rdr["TANK_INSTRUCTIONS"].ToString();
                        record.IMO_TANK_INSTR_PROV = rdr["TANK_INSTRUCTIONS_PROVISIONS"].ToString();
                        record.IMO_TANK_INSTR_UN = rdr["TANK_INSTRUCTIONS_UN"].ToString();
                        record.IMO_UN_NUMBER = rdr["UN_ID_NUMBER"].ToString();
                        DateTime temp;
                        DateTime.TryParse(rdr["TMSP_LAST_UPDT"].ToString(), out temp);
                        record.TMSP_LAST_UPDT = temp;

                        list.Add(record);
                    }
                }
            }

            return list;
        }
        #endregion


        #region Utilities
        public int getTableCount(String tableName) {
            int result = 0;

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();
            try {
                String stmt = "SELECT COUNT(*) FROM msdsonline_dbo." + tableName;

                using (OdbcCommand cmd = new OdbcCommand(stmt, m_conn)) {
                    result = (Int32)cmd.ExecuteScalar();
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getTableCount - " 
                    + tableName
                    + "\n\t" + ex.Message);
            }

            return result;
        }

        public int getDocumentCount() {
            int result = 0;

            //TODO - replace doc_vaut_agg with individual tables
            String stmt = "SELECT COUNT(*) FROM MSDSONLINE_DBO.DOC_VAULT_AGG";

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();
            try {
                using (OdbcCommand cmd = new OdbcCommand(stmt, m_conn)) {
                    result = (Int32)cmd.ExecuteScalar();
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getDocumentCount - "
                    + "\n\t" + ex.Message);
            }

            return result;
        }

        public int getNewRevisionCount(DateTime lastUpdate) {
            int result = 0;

            //TODO - replace doc_vaut_agg with individual tables
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT COUNT(DISTINCT Doc_Group_Num) ");
            sb.AppendLine("FROM MSDSONLINE_DBO.DOC_VAULT_AGG ");
            sb.AppendLine("WHERE doc_index_id in ");
            sb.AppendLine("  (SELECT doc_index_id FROM msdsonline_dbo.document_index ");
            sb.AppendLine("  WHERE tmsp_last_updt > ?)");
            sb.AppendLine("OR doc_data_id in ");
            sb.AppendLine("  (SELECT doc_data_id FROM msdsonline_dbo.document_data ");
            sb.AppendLine("  WHERE tmsp_last_updt > ?)");
            sb.AppendLine("OR Revision_Date > ?");
            sb.AppendLine("AND Document_Language_Code = 'E'");
 
            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            try {
                using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                    cmd.Parameters.AddWithValue("@di_updt", lastUpdate);
                    cmd.Parameters.AddWithValue("@dd_updt", lastUpdate);
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);

                    result = (Int32)cmd.ExecuteScalar();
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getNewRevisionCount - " 
                    + lastUpdate.ToShortDateString()
                    + "\n\t" + ex.Message);
            }
 
            return result;
        }

        public int[] getRevisionDocTypeCounts(DateTime lastUpdate) {
            int[] result = {0, 0, 0, 0, 0, 0};

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT Document_Type, COUNT(*) AS Items ");
            sb.AppendLine("FROM MSDSONLINE_DBO.DOCUMENT_DATA ");
            sb.AppendLine("WHERE Revision_Date > ? ");
            sb.AppendLine("AND Language_Code = 'E' ");
            sb.AppendLine("GROUP BY Document_Type");

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            try {
                using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);

                    using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            int count = 0;
                            string docType = rdr[0].ToString();
                            int.TryParse(rdr[1].ToString(), out count);

                            if (docType.Equals("MSDS"))
                                result[0] = count;
                            else if (docType.Equals("PROD SHEET"))
                                result[1] = count;
                            else if (docType.Equals("MFR. LABEL"))
                                result[2] = count;
                            else if (docType.Equals("TRANS CERT"))
                                result[3] = count;
                            else if (docType.Equals("MSDS, TRAN"))
                                result[4] = count;
                            else if (docType.Equals("OTHER DOCS"))
                                result[5] = count;
                        }
                    }
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getRevisionDocTypeCounts - "
                    + lastUpdate.ToShortDateString()
                    + "\n\t" + ex.Message);
            }

            return result;
        }

        public int[] getSupportDataRevisionCounts(DateTime lastUpdate) {
            int[] result = { 0, 0, 0, 0, 0, 0 };

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT COUNT(*) AS comp, ");
            sb.AppendLine("(select COUNT(*) FROM msdsonline_dbo.Ref_Afjm_Transportation WHERE tmsp_last_updt > ?) as afjmdot, ");
            sb.AppendLine("(select COUNT(*) FROM msdsonline_dbo.Ref_Dot_Transportation WHERE tmsp_last_updt > ?) as dot, ");
            sb.AppendLine("(select COUNT(*) FROM msdsonline_dbo.Ref_Iata_Transportation WHERE tmsp_last_updt > ?) as iata, ");
            sb.AppendLine("(select COUNT(*) FROM msdsonline_dbo.Ref_Imo_Transportation WHERE tmsp_last_updt > ?) as imo ");
            sb.AppendLine("FROM msdsonline_dbo.Company WHERE tmsp_last_updt > ?");

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            try {
                using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);
                    cmd.Parameters.AddWithValue("@lastUpdate", lastUpdate);

                    using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                        if (rdr.Read()) {
                            int count = 0;
                            int.TryParse(rdr[0].ToString(), out count);
                            result[0] = count;
                            int.TryParse(rdr[1].ToString(), out count);
                            result[1] = count;
                            int.TryParse(rdr[2].ToString(), out count);
                            result[2] = count;
                            int.TryParse(rdr[3].ToString(), out count);
                            result[3] = count;
                            int.TryParse(rdr[4].ToString(), out count);
                            result[4] = count;
                        }
                    }
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getRevisionDocTypeCounts - "
                    + lastUpdate.ToShortDateString()
                    + "\n\t" + ex.Message);
            }

            return result;
        }

        //TODO - look into SELECT logic - don't think we need Revision_Date or Doc Language Code
        public List<string> getNewMSDSList(DateTime startDate) {
            List<string> list = new List<string>();

            //TODO - replace doc_vaut_agg with individual tables
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT DISTINCT Doc_Group_Num ");
            sb.AppendLine("FROM MSDSONLINE_DBO.DOC_VAULT_AGG ");
            sb.AppendLine("WHERE doc_index_id in ");
            sb.AppendLine("  (SELECT doc_index_id FROM msdsonline_dbo.document_index ");
            sb.AppendLine("  WHERE tmsp_last_updt > ?)");
            sb.AppendLine("OR doc_data_id in ");
            sb.AppendLine("  (SELECT doc_data_id FROM msdsonline_dbo.document_data ");
            sb.AppendLine("  WHERE tmsp_last_updt > ?)");
            sb.AppendLine("OR Revision_Date > ?");
            sb.AppendLine("AND Document_Language_Code = 'E'");

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            try {
                using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                    cmd.Parameters.AddWithValue("@di_updt", startDate);
                    cmd.Parameters.AddWithValue("@dd_updt", startDate);
                    cmd.Parameters.AddWithValue("@startDate", startDate);

                    using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            list.Add(rdr[0].ToString());
                        }
                    }
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getNewMSDSList - "
                    + startDate.ToShortDateString()
                    + "\n\t" + ex.Message);
            }

            return list;
        }

        public List<HMIRSDocumentRecord> getNewDocumentList(DateTime startDate) {
            List<HMIRSDocumentRecord> list = new List<HMIRSDocumentRecord>();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT dd.*, di.doc_group_num ");
            sb.AppendLine("FROM msdsonline_dbo.document_data dd ");
            sb.AppendLine("JOIN msdsonline_dbo.document_product_mapping mp on mp.doc_data_id = dd.doc_data_id");
            sb.AppendLine("JOIN msdsonline_dbo.document_index di ON di.doc_index_id = mp.doc_index_id");
            sb.AppendLine("WHERE dd.Revision_Date > ?");

            if (m_conn == null)
                m_conn = new OdbcConnection(m_connString);
            if (m_conn.State != System.Data.ConnectionState.Open)
                m_conn.Open();

            try {
                using (OdbcCommand cmd = new OdbcCommand(sb.ToString(), m_conn)) {
                    cmd.Parameters.AddWithValue("@startDate", startDate);

                    using (OdbcDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            HMIRSDocumentRecord record = new HMIRSDocumentRecord();
                            long data_id = 0;
                            long.TryParse(rdr["doc_data_id"].ToString(), out data_id);
                            record.doc_data_id = data_id;
                            DateTime rev_date;
                            DateTime.TryParse(rdr["revision_date"].ToString(), out rev_date);
                            record.revision_date = rev_date;
                            record.document_type = rdr["document_type"].ToString();
                            record.country_code = rdr["country_code"].ToString();
                            record.language_code = rdr["language_code"].ToString();
                            record.doc_status_code = rdr["doc_status_code"].ToString();
                            record.doc_format = rdr["doc_file_format"].ToString();
                            int temp_id = 0;
                            int.TryParse(rdr["revision_num"].ToString(), out temp_id);
                            record.revision_num = temp_id;
                            record.proprietary_ind = rdr["proprietary_ind"].ToString();
                            record.manufacturer_serial_num = rdr["manufacturer_serial_num"].ToString();
                            int.TryParse(rdr["manufacturer_revision_num"].ToString(), out temp_id);
                            record.manufacturer_revision_num = temp_id;
                            record.doc_group_num = rdr["doc_group_num"].ToString();

                            list.Add(record);
                        }
                    }
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR - getNewDocumentList - "
                    + startDate.ToShortDateString()
                    + "\n\t" + ex.Message);
            }

            return list;
        }
        #endregion

    }
}
