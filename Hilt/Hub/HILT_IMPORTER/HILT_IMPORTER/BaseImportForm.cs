﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HILT_IMPORTER {
    public partial class BaseImportForm : Form {
        protected HiltImportMDIForm m_parentMDI = null;
        protected bool m_processing = false;
        protected int m_tabTwips = 720;

        public BaseImportForm() {
            InitializeComponent();
        }

        // Form Properties
        protected String Title {
            get {
                return this.titleLabel.Text;
            }
            set {
                this.titleLabel.Text = value;
            }
        }

        protected int TabSizeTwips {
            get {
                return m_tabTwips;
            }
            set {
                m_tabTwips = value;
            }
        }

        private void fileButton_Click(object sender, EventArgs e) {
            // Call the processFileButton method to process the click event
            processFileButton_Click(sender, e);
        }

        private void importButton_Click(object sender, EventArgs e) {
            // Call the processSaveButton method to process the click event
            processImportButton_Click(sender, e);
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            // Call the processCancelButton method to process the click event
            processCancelButton_Click(sender, e);
        }

        protected virtual void processFileButton_Click(object sender, EventArgs e) {
            MessageBox.Show("Please implement the processFileButton_Click method in your child form");
        }

        protected virtual void processImportButton_Click(object sender, EventArgs e) {
            MessageBox.Show("Please implement the processSaveButton_Click method in your child form");
        }

        protected virtual void processCancelButton_Click(object sender, EventArgs e) {
            // Default behavior - override this method if you want special handling on Cancel
        }

        private void hiltOpenFileDialog_File(object sender, CancelEventArgs e) {

        }

        private void BaseImportForm_Shown(object sender, EventArgs e) {
             m_parentMDI = (HiltImportMDIForm)MdiParent;
        }

        protected void setCursors(Cursor cursor) {
            Cursor = cursor;
            messageRTBox.Cursor = cursor;
        }

        private void messageRTBox_TextChanged(object sender, EventArgs e) {
            if (messageRTBox.Text.Length < messageRTBox.MaxLength) {
                messageRTBox.SelectionStart = messageRTBox.Text.Length;
                messageRTBox.ScrollToCaret();
            }
        }

        #region utilities
        // Clears the RTF message box and resets the default tab size
        protected void resetMessageBox() {
            messageRTBox.Clear();
            messageRTBox.Rtf = messageRTBox.Rtf.Replace("deflang1033",
                "deflang1033\\deftab" + m_tabTwips.ToString());
        }

        static protected internal string formatNumberThousands(int num) {
            return String.Format("{0:#,##0}", num);
        }
        #endregion
    }
}
