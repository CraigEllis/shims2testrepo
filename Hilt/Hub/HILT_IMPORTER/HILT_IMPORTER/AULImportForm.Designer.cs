﻿namespace HILT_IMPORTER {
    partial class AULImportForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.previewButton = new System.Windows.Forms.Button();
            this.aulFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.aulBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // fileTextBox
            // 
            this.fileTextBox.TabIndex = 1;
            // 
            // messageRTBox
            // 
            this.messageRTBox.ReadOnly = true;
            this.messageRTBox.TabIndex = 7;
            // 
            // importButton
            // 
            this.importButton.TabIndex = 5;
            // 
            // cancelButton
            // 
            this.cancelButton.TabIndex = 6;
            // 
            // fileButton
            // 
            this.fileButton.TabIndex = 2;
            this.fileButton.Click += new System.EventHandler(this.fileButton_Click);
            // 
            // previewButton
            // 
            this.previewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.previewButton.Location = new System.Drawing.Point(16, 397);
            this.previewButton.Name = "previewButton";
            this.previewButton.Size = new System.Drawing.Size(75, 23);
            this.previewButton.TabIndex = 4;
            this.previewButton.Text = "Preview";
            this.previewButton.UseVisualStyleBackColor = true;
            this.previewButton.Click += new System.EventHandler(this.previewButton_Click);
            // 
            // aulFileDialog
            // 
            this.aulFileDialog.Filter = "(Excel files|*.xls;*.xlsx|All files|*.*";
            // 
            // aulBackgroundWorker
            // 
            this.aulBackgroundWorker.WorkerReportsProgress = true;
            this.aulBackgroundWorker.WorkerSupportsCancellation = true;
            this.aulBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.aulBackgroundWorker_DoWork);
            this.aulBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.aulBackgroundWorker_ProgressChanged_1);
            this.aulBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.aulBackgroundWorker_RunWorkerCompleted);
            // 
            // AULImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(668, 432);
            this.Controls.Add(this.previewButton);
            this.Name = "AULImportForm";
            this.Text = "Authorized Use List Import";
            this.Controls.SetChildIndex(this.fileLlabel, 0);
            this.Controls.SetChildIndex(this.fileButton, 0);
            this.Controls.SetChildIndex(this.importButton, 0);
            this.Controls.SetChildIndex(this.cancelButton, 0);
            this.Controls.SetChildIndex(this.fileTextBox, 0);
            this.Controls.SetChildIndex(this.messageRTBox, 0);
            this.Controls.SetChildIndex(this.previewButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void fileButton_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button previewButton;
        private System.Windows.Forms.OpenFileDialog aulFileDialog;
        private System.ComponentModel.BackgroundWorker aulBackgroundWorker;
    }
}
