﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILT_IMPORTER
{
    public class Record
    {

        //Use this to track the 
        //File name in which the record lives.
        string parsedFileName = null;

        public string ParsedFileName
        {
            get { return parsedFileName; }
            set { parsedFileName = value; }
        }

        //MSDS Table Information.
        //***********************
        #region MSDS Table
        //Article Index.
        private char article_ind = ' ';

        public char Article_ind
        {
            get { return article_ind; }
            set { article_ind = value; }
        }
        //Responsible Party Cage.
        private string rpcage = null;

        public string Rpcage
        {
            get { return rpcage; }
            set { rpcage = value; }
        }
        //Responsible Party Company Name
        private string manufacturer = null;

        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }
        //Description
        private string description = null;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        //Emergency Telephone Responsible Party
        private string emergency_tel = null;

        public string Emergency_tel
        {
            get { return emergency_tel; }
            set { emergency_tel = value; }
        }
        //End Item Component Indicator.
        private char end_comp_ind = ' ';

        public char End_comp_ind
        {
            get { return end_comp_ind; }
            set { end_comp_ind = value; }
        }
        //End Item Indicator.
        private char end_item_ind = ' ';

        public char End_item_ind
        {
            get { return end_item_ind; }
            set { end_item_ind = value; }
        }
        //FSC
        private string fsc = null;

        public string Fsc
        {
            get { return fsc; }
            set { fsc = value; }
        }
        //Kit Indicator.
        private char kit_ind = ' ';

        public char Kit_ind
        {
            get { return kit_ind; }
            set { kit_ind = value; }
        }
        //Kit Part Indicator.
        private char kit_part_ind = ' ';

        public char Kit_part_ind
        {
            get { return kit_part_ind; }
            set { kit_part_ind = value; }
        }
        //Manufacturer MSDS Number
        private string manufacturer_msds_no = null;

        public string Manufacturer_msds_no
        {
            get { return manufacturer_msds_no; }
            set { manufacturer_msds_no = value; }
        }
        //Mixture Indicator.
        private char mixture_ind = ' ';

        public char Mixture_ind
        {
            get { return mixture_ind; }
            set { mixture_ind = value; }
        }
        //NIIN
        private string niin = null;

        public string Niin
        {
            get { return niin; }
            set { niin = value; }
        }
        //Part Number
        private string partNo = null;

        public string PartNo
        {
            get { return partNo; }
            set { partNo = value; }
        }
        //Product Identity
        private string product_identity = null;

        public string Product_identity
        {
            get { return product_identity; }
            set { product_identity = value; }
        }
        //Product Indicator.
        private char product_ind = ' ';

        public char Product_ind
        {
            get { return product_ind; }
            set { product_ind = value; }
        }
        //Product Language
        private string product_language = null;

        public string Product_language
        {
            get { return product_language; }
            set { product_language = value; }
        }
        //Product Load Date.
        private DateTime? product_load_date = null;

        public DateTime? Product_load_date
        {
            get { return product_load_date; }
            set { product_load_date = value; }
        }
        //Product Record Status.
        private char product_record_status = ' ';

        public char Product_record_status
        {
            get { return product_record_status; }
            set { product_record_status = value; }
        }
        //Product Revision Number.
        private string product_revision_no = null;

        public string Product_revision_no
        {
            get { return product_revision_no; }
            set { product_revision_no = value; }
        }
        //Product Serial Number
        private string msdsSerNo = null;

        public string MsdsSerNo
        {
            get { return msdsSerNo; }
            set { msdsSerNo = value; }
        }
        //Published Indicator
        private char published_ind = ' ';

        private char proprietary_ind = ' ';

        public char Proprietary_ind
        {
            get { return proprietary_ind; }
            set { proprietary_ind = value; }
        }

        public char Published_ind
        {
            get { return published_ind; }
            set { published_ind = value; }
        }
        //Purchased Product Indicator
        private char purchased_prod_ind = ' ';

        public char Purchased_prod_ind
        {
            get { return purchased_prod_ind; }
            set { purchased_prod_ind = value; }
        }
        //Pure Indicator
        private char pure_ind = ' ';

        public char Pure_ind
        {
            get { return pure_ind; }
            set { pure_ind = value; }
        }
        //Radioactive Indicator
        private char radioactive_ind = ' ';

        public char Radioactive_ind
        {
            get { return radioactive_ind; }
            set { radioactive_ind = value; }
        }
        //Service Agency
        private string service_agency_code = null;

        public string Service_agency_code
        {
            get { return service_agency_code; }
            set { service_agency_code = value; }
        }
        //Trade Name
        private string trade_name = null;

        public string Trade_name
        {
            get { return trade_name; }
            set { trade_name = value; }
        }
        //Trade Secret Indicator
        private char trade_secret_ind = ' ';

        public char Trade_secret_ind
        {
            get { return trade_secret_ind; }
            set { trade_secret_ind = value; }
        }
        //MSDS File Path
        private string msdsFilePath = null;

        public string MsdsFilePath
        {
            get { return msdsFilePath; }
            set { msdsFilePath = value; }
        }

        private int? hcc_id = null;

        public int? Hcc_id
        {
            get { return hcc_id; }
            set { hcc_id = value; }
        }
#endregion

        //MSDS Phys Chemical Table
        #region MSDS Phys Chem Table
        //Percent Volatiles by Volume
        private string percent_vol_volume = null;

        public string Percent_vol_volume
        {
            get { return percent_vol_volume; }
            set { percent_vol_volume = value; }
        }
        //Appearance Odor Text
        private string app_odor = null;

        public string App_odor
        {
            get { return app_odor; }
            set { app_odor = value; }
        }
        //Autoignition Temp. (C)
        private string autoignition_temp = null;

        public string Autoignition_temp
        {
            get { return autoignition_temp; }
            set { autoignition_temp = value; }
        }
        //Carcinogen Indicator
        private char carcinogen_ind = ' ';

        public char Carcinogen_ind
        {
            get { return carcinogen_ind; }
            set { carcinogen_ind = value; }
        }
        //EPA Acute
        private char epa_acute = ' ';

        public char Epa_acute
        {
            get { return epa_acute; }
            set { epa_acute = value; }
        }
        //EPA Chronic
        private char epa_chronic = ' ';

        public char Epa_chronic
        {
            get { return epa_chronic; }
            set { epa_chronic = value; }
        }
        //EPA Fire
        private char epa_fire = ' ';

        public char Epa_fire
        {
            get { return epa_fire; }
            set { epa_fire = value; }
        }
        //EPA Pressure
        private char epa_pressure = ' ';

        public char Epa_pressure
        {
            get { return epa_pressure; }
            set { epa_pressure = value; }
        }
        //EPA Reactivity
        private char epa_reactivity = ' ';

        public char Epa_reactivity
        {
            get { return epa_reactivity; }
            set { epa_reactivity = value; }
        }
        //Evaporation Rate
        private string evap_rate_ref = null;

        public string Evap_rate_ref
        {
            get { return evap_rate_ref; }
            set { evap_rate_ref = value; }
        }
        //Flash Point Temp (C)
        private string flash_point_temp = null;

        public string Flash_point_temp
        {
            get { return flash_point_temp; }
            set { flash_point_temp = value; }
        }
        //Neutralizing Agent Text
        private string neut_agent = null;

        public string Neut_agent
        {
            get { return neut_agent; }
            set { neut_agent = value; }
        }
        //NFPA Flammability
        private string nfpa_flammability = null;

        public string Nfpa_flammability
        {
            get { return nfpa_flammability; }
            set { nfpa_flammability = value; }
        }
        //NFPA Health
        private string nfpa_health = null;

        public string Nfpa_health
        {
            get { return nfpa_health; }
            set { nfpa_health = value; }
        }
        //NFPA Reactivity
        private string nfpa_reactivity = null;

        public string Nfpa_reactivity
        {
            get { return nfpa_reactivity; }
            set { nfpa_reactivity = value; }
        }
        //NFPA Special
        private string nfpa_special = null;

        public string Nfpa_special
        {
            get { return nfpa_special; }
            set { nfpa_special = value; }
        }
        //OSHA Carcinogens
        private char osha_carcinogens = ' ';

        public char Osha_carcinogens
        {
            get { return osha_carcinogens; }
            set { osha_carcinogens = value; }
        }
        //OSHA Combustion Liquid
        private char osha_comb_liquid = ' ';

        public char Osha_comb_liquid
        {
            get { return osha_comb_liquid; }
            set { osha_comb_liquid = value; }
        }
        //OSHA Compressed Gas
        private char osha_comp_gas = ' ';

        public char Osha_comp_gas
        {
            get { return osha_comp_gas; }
            set { osha_comp_gas = value; }
        }
        //OSHA Corrosive
        private char osha_corrosive = ' ';

        public char Osha_corrosive
        {
            get { return osha_corrosive; }
            set { osha_corrosive = value; }
        }
        //OSHA Explosive
        private char osha_explosive = ' ';

        public char Osha_explosive
        {
            get { return osha_explosive; }
            set { osha_explosive = value; }
        }
        //OSHA Flammable
        private char osha_flammable = ' ';

        public char Osha_flammable
        {
            get { return osha_flammable; }
            set { osha_flammable = value; }
        }
        //OSHA Highly Toxic
        private char osha_high_toxic = ' ';

        public char Osha_high_toxic
        {
            get { return osha_high_toxic; }
            set { osha_high_toxic = value; }
        }
        //OSHA Irritant
        private char osha_irritant = ' ';

        public char Osha_irritant
        {
            get { return osha_irritant; }
            set { osha_irritant = value; }
        }
        //OSHA Organic Peroxide
        private char osha_org_perox = ' ';

        public char Osha_org_perox
        {
            get { return osha_org_perox; }
            set { osha_org_perox = value; }
        }
        //OSHA Other/Long Term
        private char osha_otherlongterm = ' ';

        public char Osha_otherlongterm
        {
            get { return osha_otherlongterm; }
            set { osha_otherlongterm = value; }
        }
        //OSHA Oxidizer
        private char osha_oxidizer = ' ';

        public char Osha_oxidizer
        {
            get { return osha_oxidizer; }
            set { osha_oxidizer = value; }
        }
        //OSHA Pyrophoric
        private char osha_pyro = ' ';

        public char Osha_pyro
        {
            get { return osha_pyro; }
            set { osha_pyro = value; }
        }
        //OSHA Sensitizer
        private char osha_sensitizer = ' ';

        public char Osha_sensitizer
        {
            get { return osha_sensitizer; }
            set { osha_sensitizer = value; }
        }
        //OSHA Toxic
        private char osha_toxic = ' ';

        public char Osha_toxic
        {
            get { return osha_toxic; }
            set { osha_toxic = value; }
        }
        //OSHA Unstable Reactive
        private char osha_unst_react = ' ';

        public char Osha_unst_react
        {
            get { return osha_unst_react; }
            set { osha_unst_react = value; }
        }
        //OSHA Water Reactive
        private char osha_water_reac = ' ';

        public char Osha_water_reac
        {
            get { return osha_water_reac; }
            set { osha_water_reac = value; }
        }
        //Other/Short Term
        private string other_short_term = null;

        public string Other_short_term
        {
            get { return other_short_term; }
            set { other_short_term = value; }
        }
        //pH
        private string ph = null;

        public string Ph
        {
            get { return ph; }
            set { ph = value; }
        }
        //Physical State Code
        private string phys_state_code = null;

        public string Phys_state_code
        {
            get { return phys_state_code; }
            set { phys_state_code = value; }
        }
        //Solubility in Water
        private string sol_in_water = null;

        public string Sol_in_water
        {
            get { return sol_in_water; }
            set { sol_in_water = value; }
        }
        //Specific Gravity
        private string specific_grav = null;

        public string Specific_grav
        {
            get { return specific_grav; }
            set { specific_grav = value; }
        }
        //Vapor Density
        private string vapor_dens = null;

        public string Vapor_dens
        {
            get { return vapor_dens; }
            set { vapor_dens = value; }
        }
        //Vapor Pressure
        private string vapor_press = null;

        public string Vapor_press
        {
            get { return vapor_press; }
            set { vapor_press = value; }
        }
        //Viscosity
        private string viscosity = null;

        public string Viscosity
        {
            get { return viscosity; }
            set { viscosity = value; }
        }
        //Volatile Organic Compound (gm/L)
        private string voc_grams_liter = null;

        public string Voc_grams_liter
        {
            get { return voc_grams_liter; }
            set { voc_grams_liter = value; }
        }
        //Volatile Organic Compound (lb/g)
        private string voc_pounds_gallon = null;

        public string Voc_pounds_gallon
        {
            get { return voc_pounds_gallon; }
            set { voc_pounds_gallon = value; }
        }
        //Volatile Organic Compound (wt%)
        private string vol_org_comp_wt = null;

        public string Vol_org_comp_wt
        {
            get { return vol_org_comp_wt; }
            set { vol_org_comp_wt = value; }
        }
        #endregion

        //MSDS Ingredients
        #region MSDS Ingredients
        //Percent Text Value
        private string prcnt = null;

        public string Prcnt
        {
            get { return prcnt; }
            set { prcnt = value; }
        }
        //Percent Volume Value
        private string prcnt_vol_value = null;

        public string Prcnt_vol_value
        {
            get { return prcnt_vol_value; }
            set { prcnt_vol_value = value; }
        }
        //Percent Weight Value
        private string prcnt_vol_weight = null;

        public string Prcnt_vol_weight
        {
            get { return prcnt_vol_weight; }
            set { prcnt_vol_weight = value; }
        }
        //ACGIH Stel
        private string acgih_stel = null;

        public string Acgih_stel
        {
            get { return acgih_stel; }
            set { acgih_stel = value; }
        }
        //ACGIH TLV
        private string acgih_tlv = null;

        public string Acgih_tlv
        {
            get { return acgih_tlv; }
            set { acgih_tlv = value; }
        }
        //CAS Number
        private string cas = null;

        public string Cas
        {
            get { return cas; }
            set { cas = value; }
        }
        //Chemical Manufacturer Company Name
        private string chem_mfg_comp_name = null;

        public string Chem_mfg_comp_name
        {
            get { return chem_mfg_comp_name; }
            set { chem_mfg_comp_name = value; }
        }
        //Component Ingredient Name
        private string ingredient_name = null;

        public string Ingredient_name
        {
            get { return ingredient_name; }
            set { ingredient_name = value; }
        }
        //DOT RQ
        private string dot_report_qty = null;

        public string Dot_report_qty
        {
            get { return dot_report_qty; }
            set { dot_report_qty = value; }
        }
        //EPA RQ
        private string epa_report_qty = null;

        public string Epa_report_qty
        {
            get { return epa_report_qty; }
            set { epa_report_qty = value; }
        }
        //ODS Indicator
        private string ods_ind = null;

        public string Ods_ind
        {
            get { return ods_ind; }
            set { ods_ind = value; }
        }
        //OSHA PEL
        private string osha_pel = null;

        public string Osha_pel
        {
            get { return osha_pel; }
            set { osha_pel = value; }
        }
        //OSHA STEL
        private string osha_stel = null;

        public string Osha_stel
        {
            get { return osha_stel; }
            set { osha_stel = value; }
        }
        //Other Recorded Limits
        private string other_rec_limits = null;

        public string Other_rec_limits
        {
            get { return other_rec_limits; }
            set { other_rec_limits = value; }
        }
        //RTECS Number
        private string rtecs_num = null;

        public string Rtecs_num
        {
            get { return rtecs_num; }
            set { rtecs_num = value; }
        }
        //RTECS Code
        private string rtecs_code = null;

        public string Rtecs_code
        {
            get { return rtecs_code; }
            set { rtecs_code = value; }
        }
#endregion

        //MSDS Contractor Info
        #region MSDS Contractor Info
        //Contract Number
        private string ct_number = null;

        public string Ct_number
        {
            get { return ct_number; }
            set { ct_number = value; }
        }
        //Contractor CAGE
        private string ct_cage = null;

        public string Ct_cage
        {
            get { return ct_cage; }
            set { ct_cage = value; }
        }
        //Contractor City
        private string ct_city = null;

        public string Ct_city
        {
            get { return ct_city; }
            set { ct_city = value; }
        }
        //Contractor Company Name
        private string ct_company_name = null;

        public string Ct_company_name
        {
            get { return ct_company_name; }
            set { ct_company_name = value; }
        }
        //Contractor Country
        private string ct_country = null;

        public string Ct_country
        {
            get { return ct_country; }
            set { ct_country = value; }
        }
        //Contractor PO Box
        private string ct_po_box = null;

        public string Ct_po_box
        {
            get { return ct_po_box; }
            set { ct_po_box = value; }
        }
        //Contractor State
        private string ct_state = null;

        public string Ct_state
        {
            get { return ct_state; }
            set { ct_state = value; }
        }
        //Contractor Tele Number
        private string ct_phone = null;

        public string Ct_phone
        {
            get { return ct_phone; }
            set { ct_phone = value; }
        }
        //Purchase Order Number
        private string purchase_order_no = null;

        public string Purchase_order_no
        {
            get { return purchase_order_no; }
            set { purchase_order_no = value; }
        }
#endregion

        //MSDS Radiological Info
        #region MSDS Radiological Info
        //NRC License/Permit Number
        private string nrc_lp_num = null;

        public string Nrc_lp_num
        {
            get { return nrc_lp_num; }
            set { nrc_lp_num = value; }
        }
        //Operator
        private string theOperator = null;

        public string TheOperator
        {
            get { return theOperator; }
            set { theOperator = value; }
        }
        //Radioactive Amount (Microcuries)
        private string rad_amount_micro = null;

        public string Rad_amount_micro
        {
            get { return rad_amount_micro; }
            set { rad_amount_micro = value; }
        }
        //Radioactive Form
        private string rad_form = null;

        public string Rad_form
        {
            get { return rad_form; }
            set { rad_form = value; }
        }
        //Radioisotope CAS
        private string rad_cas = null;

        public string Rad_cas
        {
            get { return rad_cas; }
            set { rad_cas = value; }
        }
        //Radioisotope Name
        private string rad_name = null;

        public string Rad_name
        {
            get { return rad_name; }
            set { rad_name = value; }
        }
        //Radioisotope Symbol
        private string rad_symbol = null;

        public string Rad_symbol
        {
            get { return rad_symbol; }
            set { rad_symbol = value; }
        }
        //Replacement NSN
        private string rep_nsn = null;

        public string Rep_nsn
        {
            get { return rep_nsn; }
            set { rep_nsn = value; }
        }
        //Sealed
        private char isSealed = ' ';

        public char IsSealed
        {
            get { return isSealed; }
            set { isSealed = value; }
        }
#endregion

        //MSDS Transportation
        #region MSDS Transportation
        //AF MMAC Code
        private string af_mmac_code = null;

        public string Af_mmac_code
        {
            get { return af_mmac_code; }
            set { af_mmac_code = value; }
        }
        //Certificate of Equivalency
        private string certificate_coe = null;

        public string Certificate_coe
        {
            get { return certificate_coe; }
            set { certificate_coe = value; }
        }
        //Competent Authority Approval
        private string competent_caa = null;

        public string Competent_caa
        {
            get { return competent_caa; }
            set { competent_caa = value; }
        }
        //DoD ID Code
        private string dod_id_code = null;

        public string Dod_id_code
        {
            get { return dod_id_code; }
            set { dod_id_code = value; }
        }
        //DOT Exemption Number
        private string dot_exemption_no = null;

        public string Dot_exemption_no
        {
            get { return dot_exemption_no; }
            set { dot_exemption_no = value; }
        }
        //DOT RQ Indicator
        private char dot_rq_ind = ' ';

        public char Dot_rq_ind
        {
            get { return dot_rq_ind; }
            set { dot_rq_ind = value; }
        }
        //EX Number
        private string ex_no = null;

        public string Ex_no
        {
            get { return ex_no; }
            set { ex_no = value; }
        }
        //Flash Pt Temp (C)
        private double? flash_pt_temp = null;

        public double? Flash_pt_temp
        {
            get { return flash_pt_temp; }
            set { flash_pt_temp = value; }
        }
        //HCC
        private string hcc = null;

        public string Hcc
        {
            get { return hcc; }
            set { hcc = value; }
        }

        //High Explosive Weight
        private double? high_explosive_wt = null;

        public double? High_explosive_wt
        {
            get { return high_explosive_wt; }
            set { high_explosive_wt = value; }
        }
        //Ltd Qty Indicator
        private char ltd_qty_ind = ' ';

        public char Ltd_qty_ind
        {
            get { return ltd_qty_ind; }
            set { ltd_qty_ind = value; }
        }
        //Magnetic Indicator
        private char magnetic_ind = ' ';

        public char Magnetic_ind
        {
            get { return magnetic_ind; }
            set { magnetic_ind = value; }
        }
        //Magnetism (Magnetic Strength)
        private string magnetism = null;

        public string Magnetism
        {
            get { return magnetism; }
            set { magnetism = value; }
        }
        //Marine Pollutant Indicator
        private char marine_pollutant_ind = ' ';

        public char Marine_pollutant_ind
        {
            get { return marine_pollutant_ind; }
            set { marine_pollutant_ind = value; }
        }
        //Net Explosive Qty. Distance Weight
        private double? net_exp_qty_dist = null;

        public double? Net_exp_qty_dist
        {
            get { return net_exp_qty_dist; }
            set { net_exp_qty_dist = value; }
        }
        //Net Explosive Weight (kg)
        private string net_exp_weight = null;

        public string Net_exp_weight
        {
            get { return net_exp_weight; }
            set { net_exp_weight = value; }
        }
        //Net Propellant Weight (kg)
        private string net_propellant_wt = null;

        public string Net_propellant_wt
        {
            get { return net_propellant_wt; }
            set { net_propellant_wt = value; }
        }
        //NOS Technical Shipping Name
        private string nos_technical_shipping_name = null;

        public string Nos_technical_shipping_name
        {
            get { return nos_technical_shipping_name; }
            set { nos_technical_shipping_name = value; }
        }
        //Transportation Additional Data
        private string transportation_additional_data = null;

        public string Transportation_additional_data
        {
            get { return transportation_additional_data; }
            set { transportation_additional_data = value; }
        }
#endregion

        //MSDS DOT PSN
        #region MSDS DOT PSN
        //DOT Hazard Class/Div
        private string dot_hazard_class_div = null;

        public string Dot_hazard_class_div
        {
            get { return dot_hazard_class_div; }
            set { dot_hazard_class_div = value; }
        }
        //DOT Hazard Label
        private string dot_hazard_label = null;

        public string Dot_hazard_label
        {
            get { return dot_hazard_label; }
            set { dot_hazard_label = value; }
        }
        //DOT Max Quantity: CARGO AIRCRAFT ONLY
        private string dot_max_cargo = null;

        public string Dot_max_cargo
        {
            get { return dot_max_cargo; }
            set { dot_max_cargo = value; }
        }
        //DOT MAX QUANTITY: PASSENGER AIRCRaFT/RAIL
        private string dot_max_passenger = null;

        public string Dot_max_passenger
        {
            get { return dot_max_passenger; }
            set { dot_max_passenger = value; }
        }
        //DOT PACKAGING BULK
        private string dot_pack_bulk = null;

        public string Dot_pack_bulk
        {
            get { return dot_pack_bulk; }
            set { dot_pack_bulk = value; }
        }
        //DOT PACKAGING EXCEPTIONS
        private string dot_pack_exceptions = null;

        public string Dot_pack_exceptions
        {
            get { return dot_pack_exceptions; }
            set { dot_pack_exceptions = value; }
        }
        //DOT PACKAGING NON BULK
        private string dot_pack_nonbulk = null;

        public string Dot_pack_nonbulk
        {
            get { return dot_pack_nonbulk; }
            set { dot_pack_nonbulk = value; }
        }
        //DOT PACKING GROUP
        private string dot_pack_group = null;

        public string Dot_pack_group
        {
            get { return dot_pack_group; }
            set { dot_pack_group = value; }
        }
        //DOT PROPER SHIPPING NAME
        private string dot_prop_ship_name = null;

        public string Dot_prop_ship_name
        {
            get { return dot_prop_ship_name; }
            set { dot_prop_ship_name = value; }
        }
        //DOT PROPER SHIPPING NAME MODIFIER
        private string dot_prop_ship_modifier = null;

        public string Dot_prop_ship_modifier
        {
            get { return dot_prop_ship_modifier; }
            set { dot_prop_ship_modifier = value; }
        }
        //DOT PSN CODE
        private string dot_psn_code = null;

        public string Dot_psn_code
        {
            get { return dot_psn_code; }
            set { dot_psn_code = value; }
        }
        //DOT SPECIAL PROVISION
        private string dot_special_provision = null;

        public string Dot_special_provision
        {
            get { return dot_special_provision; }
            set { dot_special_provision = value; }
        }
        //DOT SYMBOLS
        private string dot_symbols = null;

        public string Dot_symbols
        {
            get { return dot_symbols; }
            set { dot_symbols = value; }
        }
        //DOT UN ID NUMBER
        private string dot_un_id_number = null;

        public string Dot_un_id_number
        {
            get { return dot_un_id_number; }
            set { dot_un_id_number = value; }
        }
        //DOT WATER SHIPMENT OTHER REQS
        private string dot_water_other_req = null;

        public string Dot_water_other_req
        {
            get { return dot_water_other_req; }
            set { dot_water_other_req = value; }
        }
        //DOT WATER SHIPMENT VESSEL STOWAGE
        private string dot_water_vessel_stow = null;

        public string Dot_water_vessel_stow
        {
            get { return dot_water_vessel_stow; }
            set { dot_water_vessel_stow = value; }
        }
#endregion

        //MSDS AFJM PSN
        #region MSDS AFJM PSN
        //AFJM HAZARD CLASS/DIV
        private string afjm_hazard_class = null;

        public string Afjm_hazard_class
        {
            get { return afjm_hazard_class; }
            set { afjm_hazard_class = value; }
        }
        //AFJM PACKAGING PARAGRAPH
        private string afjm_pack_paragraph = null;

        public string Afjm_pack_paragraph
        {
            get { return afjm_pack_paragraph; }
            set { afjm_pack_paragraph = value; }
        }
        //AFJM PACKING GROUP
        private string afjm_pack_group = null;

        public string Afjm_pack_group
        {
            get { return afjm_pack_group; }
            set { afjm_pack_group = value; }
        }
        //AFJM PROPER SHIPPING NAME
        private string afjm_proper_ship_name = null;

        public string Afjm_proper_ship_name
        {
            get { return afjm_proper_ship_name; }
            set { afjm_proper_ship_name = value; }
        }
        //AFJM PROPER SHIPPING NAME MODIFIER
        private string afjm_proper_ship_modifier = null;

        public string Afjm_proper_ship_modifier
        {
            get { return afjm_proper_ship_modifier; }
            set { afjm_proper_ship_modifier = value; }
        }
        //AFJM PSN CODE
        private string afjm_psn_code = null;

        public string Afjm_psn_code
        {
            get { return afjm_psn_code; }
            set { afjm_psn_code = value; }
        }
        //AFJM SPECIAL PROVISIONS
        private string afjm_special_prov = null;

        public string Afjm_special_prov
        {
            get { return afjm_special_prov; }
            set { afjm_special_prov = value; }
        }
        //AFJM SUBSIDIARY RISK
        private string afjm_subsidiary_risk = null;

        public string Afjm_subsidiary_risk
        {
            get { return afjm_subsidiary_risk; }
            set { afjm_subsidiary_risk = value; }
        }
        //AFJM SYMBOLS
        private string afjm_symbols = null;

        public string Afjm_symbols
        {
            get { return afjm_symbols; }
            set { afjm_symbols = value; }
        }
        //AFJM UN ID NUMBER
        private string afjm_un_id_number = null;

        public string Afjm_un_id_number
        {
            get { return afjm_un_id_number; }
            set { afjm_un_id_number = value; }
        }
#endregion

        //MSDS IATA PSN
        #region MSDS IATA PSN
        //IATA CARGO PACKING: NOTE CARGO AIRCRAfT PACKING INSTRUCTIONS
        private string iata_cargo_packing = null;

        public string Iata_cargo_packing
        {
            get { return iata_cargo_packing; }
            set { iata_cargo_packing = value; }
        }
        //IATA HAZARD CLASS/DIV
        private string iata_hazard_class = null;

        public string Iata_hazard_class
        {
            get { return iata_hazard_class; }
            set { iata_hazard_class = value; }
        }
        //IATA HAZARD LABEL
        private string iata_hazard_label = null;

        public string Iata_hazard_label
        {
            get { return iata_hazard_label; }
            set { iata_hazard_label = value; }
        }
        //IATA PACKING GROUP
        private string iata_packing_group = null;

        public string Iata_packing_group
        {
            get { return iata_packing_group; }
            set { iata_packing_group = value; }
        }
        //IATA PASSENGER AIR PACKING: LMT QTY PKG INSTR
        private string iata_pass_air_pack_lmt_instr = null;

        public string Iata_pass_air_pack_lmt_instr
        {
            get { return iata_pass_air_pack_lmt_instr; }
            set { iata_pass_air_pack_lmt_instr = value; }
        }
        //IATA PASSENGER AIR PACKING: LMT QTY MAX QTY PER PKG
        private string iata_pass_air_pack_lmt_per_pkg = null;

        public string Iata_pass_air_pack_lmt_per_pkg
        {
            get { return iata_pass_air_pack_lmt_per_pkg; }
            set { iata_pass_air_pack_lmt_per_pkg = value; }
        }
        //IATA PASSENGER AIR PACKING MAX QUANTITY
        private string iata_pass_air_max_qty = null;

        public string Iata_pass_air_max_qty
        {
            get { return iata_pass_air_max_qty; }
            set { iata_pass_air_max_qty = value; }
        }
        //IATA PASSENGER AIR PACKING: NOTE PASSENGER AIR PACKING INSTR
        private string iata_pass_air_pack_note = null;

        public string Iata_pass_air_pack_note
        {
            get { return iata_pass_air_pack_note; }
            set { iata_pass_air_pack_note = value; }
        }
        //IATA PROPER SHIPPING NAME
        private string iata_prop_ship_name = null;

        public string Iata_prop_ship_name
        {
            get { return iata_prop_ship_name; }
            set { iata_prop_ship_name = value; }
        }
        //IATA PROPER SHIPPING NAME MODIFIER
        private string iata_prop_ship_modifier = null;

        public string Iata_prop_ship_modifier
        {
            get { return iata_prop_ship_modifier; }
            set { iata_prop_ship_modifier = value; }
        }
        //IATA CARGO PACKING: MAX QUANTITY
        private string iata_pass_cargo_pack_max_qty = null;

        public string Iata_pass_cargo_pack_max_qty
        {
            get { return iata_pass_cargo_pack_max_qty; }
            set { iata_pass_cargo_pack_max_qty = value; }
        }
        //IATA PSN CODE
        private string iata_psn_code = null;

        public string Iata_psn_code
        {
            get { return iata_psn_code; }
            set { iata_psn_code = value; }
        }
        //IATA SPECIAL PROVISIONS
        private string iata_special_prov = null;

        public string Iata_special_prov
        {
            get { return iata_special_prov; }
            set { iata_special_prov = value; }
        }
        //IATA SUBSIDIARY RISK
        private string iata_subsidiary_risk = null;

        public string Iata_subsidiary_risk
        {
            get { return iata_subsidiary_risk; }
            set { iata_subsidiary_risk = value; }
        }
        //IATA UN ID NUMBER
        private string iata_un_id_number = null;

        public string Iata_un_id_number
        {
            get { return iata_un_id_number; }
            set { iata_un_id_number = value; }
        }
#endregion

        //MSDS IMO PSN
        #region MSDS IMO PSN
        //IMO EMS NUMBER
        private string imo_ems_no = null;

        public string Imo_ems_no
        {
            get { return imo_ems_no; }
            set { imo_ems_no = value; }
        }
        //IMO HAZARD CLASS/DIV
        private string imo_hazard_class = null;

        public string Imo_hazard_class
        {
            get { return imo_hazard_class; }
            set { imo_hazard_class = value; }
        }
        //IMO IBC INSTRUCTIONS
        private string imo_ibc_instr = null;

        public string Imo_ibc_instr
        {
            get { return imo_ibc_instr; }
            set { imo_ibc_instr = value; }
        }
        //IMO IBC PROVISIONS
        private string imo_ibc_provisions = null;

        public string Imo_ibc_provisions
        {
            get { return imo_ibc_provisions; }
            set { imo_ibc_provisions = value; }
        }
        //IMO LIMITED QUANTITY
        private string imo_limited_qty = null;

        public string Imo_limited_qty
        {
            get { return imo_limited_qty; }
            set { imo_limited_qty = value; }
        }
        //IMO PACKING GROUP
        private string imo_pack_group = null;

        public string Imo_pack_group
        {
            get { return imo_pack_group; }
            set { imo_pack_group = value; }
        }
        //IMO PACKING INSTRUCTIONS
        private string imo_pack_instructions = null;

        public string Imo_pack_instructions
        {
            get { return imo_pack_instructions; }
            set { imo_pack_instructions = value; }
        }
        //IMO PACKING PROVISIONS
        private string imo_pack_provisions = null;

        public string Imo_pack_provisions
        {
            get { return imo_pack_provisions; }
            set { imo_pack_provisions = value; }
        }
        //IMO PROPER SHIPPING NAME
        private string imo_prop_ship_name = null;

        public string Imo_prop_ship_name
        {
            get { return imo_prop_ship_name; }
            set { imo_prop_ship_name = value; }
        }
        //IMO PROPER SHIPPING NAME MODIFIER
        private string imo_prop_ship_modifier = null;

        public string Imo_prop_ship_modifier
        {
            get { return imo_prop_ship_modifier; }
            set { imo_prop_ship_modifier = value; }
        }
        //IMO PSN CODE
        private string imo_psn_code = null;

        public string Imo_psn_code
        {
            get { return imo_psn_code; }
            set { imo_psn_code = value; }
        }
        //IMO SPECIAL PROVISIONS
        private string imo_special_prov = null;

        public string Imo_special_prov
        {
            get { return imo_special_prov; }
            set { imo_special_prov = value; }
        }
        //IMO STOWAGE AND SEGREGATION
        private string imo_stow_segr = null;

        public string Imo_stow_segr
        {
            get { return imo_stow_segr; }
            set { imo_stow_segr = value; }
        }
        //IMO SUBSIDIARY RISK LABEL
        private string imo_subsidiary_risk = null;

        public string Imo_subsidiary_risk
        {
            get { return imo_subsidiary_risk; }
            set { imo_subsidiary_risk = value; }
        }
        //IMO TANK INSTRUCTIONS, IMO
        private string imo_tank_instr_imo = null;

        public string Imo_tank_instr_imo
        {
            get { return imo_tank_instr_imo; }
            set { imo_tank_instr_imo = value; }
        }
        //IMO TANK INSTRUCTIONS, PROVISIONS
        private string imo_tank_instr_provisions = null;

        public string Imo_tank_instr_provisions
        {
            get { return imo_tank_instr_provisions; }
            set { imo_tank_instr_provisions = value; }
        }
        //IMO TANK INSTRUCTIONS, UN
        private string imo_tank_instr_un = null;

        public string Imo_tank_instr_un
        {
            get { return imo_tank_instr_un; }
            set { imo_tank_instr_un = value; }
        }
        //IMO UN NUMBER
        private string imo_un_number = null;

        public string Imo_un_number
        {
            get { return imo_un_number; }
            set { imo_un_number = value; }
        }
#endregion

        //MSDS ITEM DESCRIPTION
        #region MSDS ITEM DESC
        //BATCH NUMBER
        private string batch_number = null;

        public string Batch_number
        {
            get { return batch_number; }
            set { batch_number = value; }
        }
        //LOT NUMBER
        private string lot_number = null;

        public string Lot_number
        {
            get { return lot_number; }
            set { lot_number = value; }
        }

        //ITEM MANAGER
        private string item_manager = null;

        public string Item_manager
        {
            get { return item_manager; }
            set { item_manager = value; }
        }
        //ITEM NAME
        private string item_name = null;

        public string Item_name
        {
            get { return item_name; }
            set { item_name = value; }
        }
        //LOGISTICS FLSI NIIN VERIFIED
        private char log_flis_niin_ver = ' ';

        public char Log_flis_niin_ver
        {
            get { return log_flis_niin_ver; }
            set { log_flis_niin_ver = value; }
        }
        //LOGISTICS FSC
        private double? log_fsc = null;

        public double? Log_fsc
        {
            get { return log_fsc; }
            set { log_fsc = value; }
        }
        //NET UNIT WEIGHT
        private string net_unit_weight = null;

        public string Net_unit_weight
        {
            get { return net_unit_weight; }
            set { net_unit_weight = value; }
        }
        //QUANTITATIVE EXPRESSION EXP
        private string quantitative_expression = null;

        public string Quantitative_expression
        {
            get { return quantitative_expression; }
            set { quantitative_expression = value; }
        }
        //SHELF LIFE CODE
        private string shelf_life_code = null;

        public string Shelf_life_code
        {
            get { return shelf_life_code; }
            set { shelf_life_code = value; }
        }
        //SPECIAL EMPHASIS CODE
        private string special_emp_code = null;

        public string Special_emp_code
        {
            get { return special_emp_code; }
            set { special_emp_code = value; }
        }
        //SPECIFICATION NUMBER
        private string specification_number = null;

        public string Specification_number
        {
            get { return specification_number; }
            set { specification_number = value; }
        }
        //SPECIFICATION TYPE/GRADE/CLASS
        private string type_grade_class = null;

        public string Type_grade_class
        {
            get { return type_grade_class; }
            set { type_grade_class = value; }
        }
        //TYPE OF CONTAINER
        private string type_of_container = null;

        public string Type_of_container
        {
            get { return type_of_container; }
            set { type_of_container = value; }
        }
        //UN/NA NUMBER
        private string un_na_number = null;

        public string Un_na_number
        {
            get { return un_na_number; }
            set { un_na_number = value; }
        }
        //UNIT OF ISSUE
        private string unit_of_issue = null;

        public string Unit_of_issue
        {
            get { return unit_of_issue; }
            set { unit_of_issue = value; }
        }
        //UNIT OF ISSUE CONTAINER QUANTITY
        private string ui_container_qty = null;

        public string Ui_container_qty
        {
            get { return ui_container_qty; }
            set { ui_container_qty = value; }
        }
        //UPC/GTIN
        private string upc_gtin = null;

        public string Upc_gtin
        {
            get { return upc_gtin; }
            set { upc_gtin = value; }
        }
#endregion

        //MSDS LABEL INFO
        #region MSDS Label Info
        //COMPANY CAGE(RESPONSIBLE PARTY)
        private string company_cage_rp = null;

        public string Company_cage_rp
        {
            get { return company_cage_rp; }
            set { company_cage_rp = value; }
        }
        //COMPANY NAME(RESPONSIBLE PARTY)
        private string company_name_rp = null;

        public string Company_name_rp
        {
            get { return company_name_rp; }
            set { company_name_rp = value; }
        }
        //LABEL EMERGENCY TELE NUMBER
        private string label_emerg_phone = null;

        public string Label_emerg_phone
        {
            get { return label_emerg_phone; }
            set { label_emerg_phone = value; }
        }
        //LABEL ITEM NAME
        private string label_item_name = null;

        public string Label_item_name
        {
            get { return label_item_name; }
            set { label_item_name = value; }
        }
        //LABEL PROCUREMENT YEAR
        private string label_proc_year = null;

        public string Label_proc_year
        {
            get { return label_proc_year; }
            set { label_proc_year = value; }
        }
        //LABEL PRODUCT IDENTITY
        private string label_prod_ident = null;

        public string Label_prod_ident
        {
            get { return label_prod_ident; }
            set { label_prod_ident = value; }
        }
        //LABEL PRODUCT SERIAL NUMBER
        private string label_prod_serialno = null;

        public string Label_prod_serialno
        {
            get { return label_prod_serialno; }
            set { label_prod_serialno = value; }
        }
        //LABEL SIGNAL WORD
        private string label_signal_word = null;

        public string Label_signal_word
        {
            get { return label_signal_word; }
            set { label_signal_word = value; }
        }
        //LABEL STOCK NUMBER
        private string label_stock_no = null;

        public string Label_stock_no
        {
            get { return label_stock_no; }
            set { label_stock_no = value; }
        }
        //SPECIFIC HAZARDS
        private string specific_hazards = null;

        public string Specific_hazards
        {
            get { return specific_hazards; }
            set { specific_hazards = value; }
        }
#endregion

        //MSDS DISPOSAL
        #region MSDS Disposal
        //DISPOSAL ADDITIONAL INFO
        private string disposal_add_info = null;

        public string Disposal_add_info
        {
            get { return disposal_add_info; }
            set { disposal_add_info = value; }
        }
        //EPA HAZARDOUS WASTE CODE
        private string epa_haz_waste_code = null;

        public string Epa_haz_waste_code
        {
            get { return epa_haz_waste_code; }
            set { epa_haz_waste_code = value; }
        }
        //EPA HAZARDOUS WAST INDICATOR
        private char epa_haz_waste_ind = ' ';

        public char Epa_haz_waste_ind
        {
            get { return epa_haz_waste_ind; }
            set { epa_haz_waste_ind = value; }
        }
        //EPA HAZARDOUS WASTE NAME
        private string epa_haz_waste_name = null;

        public string Epa_haz_waste_name
        {
            get { return epa_haz_waste_name; }
            set { epa_haz_waste_name = value; }
        }
#endregion

        //MSDS DOC TYPES
        //These are all simply file paths, actual
        //files will be obtained and inserted at HILT Insertion time.
        #region MSDS DOCS
        //MSDS - See MSDS Table.
        //MSDS MANUFACTURER LABEL
        private string manufacturer_label = null;

        public string Manufacturer_label
        {
            get { return manufacturer_label; }
            set { manufacturer_label = value; }
        }
        //MSDS TRANSLATED 
        private string msds_translated = null;

        public string Msds_translated
        {
            get { return msds_translated; }
            set { msds_translated = value; }
        }
        //NESHAP COMPLIANCE CERTIFICATE
        private string neshap_compliance_cert = null;

        public string Neshap_compliance_cert
        {
            get { return neshap_compliance_cert; }
            set { neshap_compliance_cert = value; }
        }
        //OTHER DOCS
        private string other_docs = null;

        public string Other_docs
        {
            get { return other_docs; }
            set { other_docs = value; }
        }
        //PRODUCT SHEET
        private string product_sheet = null;

        public string Product_sheet
        {
            get { return product_sheet; }
            set { product_sheet = value; }
        }
        //TRANSPORTATION CERT
        private string transportation_cert = null;

        public string Transportation_cert
        {
            get { return transportation_cert; }
            set { transportation_cert = value; }
        }
#endregion
     
    }
}
