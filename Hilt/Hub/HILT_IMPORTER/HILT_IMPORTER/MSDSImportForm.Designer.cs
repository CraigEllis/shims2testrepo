﻿namespace HILT_IMPORTER {
    partial class MSDSImportForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.msdsBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.processAllCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // fileTextBox
            // 
            this.fileTextBox.ReadOnly = true;
            this.fileTextBox.TabIndex = 2;
            // 
            // messageRTBox
            // 
            this.messageRTBox.TabIndex = 4;
            // 
            // fileButton
            // 
            this.fileButton.TabIndex = 3;
            this.fileButton.Visible = false;
            // 
            // fileLlabel
            // 
            this.fileLlabel.TabIndex = 1;
            // 
            // msdsBackgroundWorker
            // 
            this.msdsBackgroundWorker.WorkerReportsProgress = true;
            this.msdsBackgroundWorker.WorkerSupportsCancellation = true;
            this.msdsBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.msdsBackgroundWorker_DoWork);
            this.msdsBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.msdsBackgroundWorker_ProgressChanged);
            this.msdsBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.msdsBackgroundWorker_RunWorkerCompleted);
            // 
            // processAllCheckBox
            // 
            this.processAllCheckBox.AutoSize = true;
            this.processAllCheckBox.Location = new System.Drawing.Point(499, 89);
            this.processAllCheckBox.Name = "processAllCheckBox";
            this.processAllCheckBox.Size = new System.Drawing.Size(155, 17);
            this.processAllCheckBox.TabIndex = 5;
            this.processAllCheckBox.Text = "Reprocess All HMIRS Data";
            this.processAllCheckBox.UseVisualStyleBackColor = true;
            this.processAllCheckBox.CheckedChanged += new System.EventHandler(this.processAllCheckBox_CheckedChanged);
            // 
            // MSDSImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(668, 432);
            this.Controls.Add(this.processAllCheckBox);
            this.Name = "MSDSImportForm";
            this.Text = "MSDS Import";
            this.Shown += new System.EventHandler(this.MSDSImportForm_Shown);
            this.Controls.SetChildIndex(this.processAllCheckBox, 0);
            this.Controls.SetChildIndex(this.fileLlabel, 0);
            this.Controls.SetChildIndex(this.fileButton, 0);
            this.Controls.SetChildIndex(this.importButton, 0);
            this.Controls.SetChildIndex(this.cancelButton, 0);
            this.Controls.SetChildIndex(this.fileTextBox, 0);
            this.Controls.SetChildIndex(this.messageRTBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker msdsBackgroundWorker;
        private System.Windows.Forms.CheckBox processAllCheckBox;
    }
}
