using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class HullType {
        
        /// <summary>
        /// Unique identifier for a hull type
        /// </summary>
        public int	hull_type_id {
            get;
            set;
        }

        /// <summary>
        /// The hull classification symbol
        /// </summary>
        public String hull_type {
            get;
            set;
        }

        /// <summary>
        /// Description of the hull type
        /// </summary>
        public String hull_description {
            get;
            set;
        }

        /// <summary>
        /// Denotes whether the hull type is currently active
        /// </summary>
        public bool active {
            get;
            set;
        }

        /// <summary>
        /// The parent hull type id for this hull type
        /// </summary>
        public int parent_type_id {
            get;
            set;
        }
        /// <summary>
        /// The hull classification symbol
        /// </summary>
        public String parent_hull_type {
            get;
            set;
        }

    }
}
