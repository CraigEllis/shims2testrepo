﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class Ship {
        #region Constructors
        // Default constructor
        public Ship() {
        }
        #endregion

        #region Properties
        public long	ship_id {
	        get;
	        set;
        }
        public long	current_status {
	        get;
	        set;
        }
        public String name {
            get;
            set;
        }
        public String ship_status {
            get;
            set;
        }
        public String hull_type {
            get;
            set;
        }
        public String uic {
	        get;
	        set;
        }
        public long hull_type_id {
            get;
            set;
        }
        public long parent_type_id {
            get;
            set;
        }
        public String hull_active {
            get;
            set;
        }
        public String hull_description {
            get;
            set;
        }
        public String parent_hull_type {
            get;
            set;
        }
        public String hull_number {
	        get;
	        set;
        }
        public DateTime	download_aul_date {
	        get;
	        set;
        }
        public DateTime	download_msds_date {
	        get;
	        set;
        }
        public DateTime	download_mssl_date {
	        get;
	        set;
        }
        public DateTime	upload_aul_date {
	        get;
	        set;
        }
        public DateTime	upload_msds_date {
	        get;
	        set;
        }
        public DateTime	upload_mssl_date {
	        get;
	        set;
        }
        public DateTime	created {
	        get;
	        set;
        }
        public String	created_by {
	        get;
	        set;
        }
        public DateTime	changed {
	        get;
	        set;
        }
        public String changed_by {
            get;
            set;
        }
        #endregion
    }
}