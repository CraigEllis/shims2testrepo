﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class MSSLStats {
        private int newRecords = 0;

        public int NewRecords {
            get {
                return newRecords;
            }
            set {
                newRecords = value;
            }
        }
        private int changedRecords = 0;

        public int ChangedRecords {
            get {
                return changedRecords;
            }
            set {
                changedRecords = value;
            }
        }
        private int deletedRecords = 0;

        public int DeletedRecords {
            get {
                return deletedRecords;
            }
            set {
                deletedRecords = value;
            }
        }
        private int totalRecords = 0;

        public int TotalRecords {
            get {
                return totalRecords;
            }
            set {
                totalRecords = value;
            }
        }

    }
}