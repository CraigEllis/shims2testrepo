﻿using System;
using System.Collections.Generic;
using System.Web;

namespace HILT_IMPORTER.DataObjects {
    public class MSSLData {

        private List<MsslInventory> inventoryList;

        public List<MsslInventory> InventoryList {
            get {
                return inventoryList;
            }
            set {
                inventoryList = value;
            }
        }
        private List<MsslSubs> subList;

        public List<MsslSubs> SubList {
            get {
                return subList;
            }
            set {
                subList = value;
            }
        }

        private String errorMsg;
        public String ErrorMsg {
            get { 
                return errorMsg;
            }
            set {
                errorMsg = value;
            }
        }

        private MSSLStats msslStats;

        public MSSLStats MsslStats {
            get {
                if (msslStats == null) {
                    msslStats = new MSSLStats();
                }
                return msslStats;
            }
            set {
                msslStats = value;
            }
        }


    }
}