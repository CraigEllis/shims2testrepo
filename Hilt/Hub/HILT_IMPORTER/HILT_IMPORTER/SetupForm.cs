﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace HILT_IMPORTER
{
    public partial class SetupForm : Form
    {
        //Default Constructor
        public SetupForm()
        {
            InitializeComponent();

            // Load the properties
            loadProperties();
        }

        // App properties
        private void loadProperties() {
            this.txtOnlineCDPath.Text = Properties.Settings.Default.CDFolderName;
            this.txtMSDSFilesPath.Text = Properties.Settings.Default.MSDSFolderName;
            this.txtAULFilePath.Text = Properties.Settings.Default.AULFolderName;
            this.txtServerBox.Text = Properties.Settings.Default.ServerName;
            if (Properties.Settings.Default.ServerPort == 0) {
                chkLocalMachine.Checked = true;
            }
            else {
                this.txtPortBox.Text = Properties.Settings.Default.ServerPort.ToString();
            }
            this.txtDatabase.Text = Properties.Settings.Default.Database;
            this.txtUsernameBox.Text = Properties.Settings.Default.UserName;
            this.txtPasswordBox.Text = Properties.Settings.Default.UserPassword;
            this.txtHubDataPath.Text = Properties.Settings.Default.HILT_HUB_DataFolder;
        }

        private Boolean saveProperties() {
            Boolean saved = true;

            try {
                Properties.Settings.Default.CDFolderName = this.txtOnlineCDPath.Text;
                Properties.Settings.Default.MSDSFolderName = this.txtMSDSFilesPath.Text;
                Properties.Settings.Default.AULFolderName = this.txtAULFilePath.Text;
                Properties.Settings.Default.ServerName = this.txtServerBox.Text;
                int nPort = 0;
                int.TryParse(this.txtPortBox.Text, out nPort);
                Properties.Settings.Default.ServerPort = nPort;
                Properties.Settings.Default.Database = this.txtDatabase.Text;
                Properties.Settings.Default.UserName = this.txtUsernameBox.Text;
                Properties.Settings.Default.UserPassword = this.txtPasswordBox.Text;
                Properties.Settings.Default.HILT_HUB_DataFolder = this.txtHubDataPath.Text;

                // Build a new ConnectionString
                Properties.Settings.Default.ConnectionInfo = buildConnectionString();

                Properties.Settings.Default.Save();
            }
            catch (Exception ex) {
                MessageBox.Show("Error saving HILT Importer setup parameters\n\n" + ex.Message,
                    "HILT Importer");
                saved = false;
            }
            return saved;
        }

        //Browse Button actions
        private void btnBrowseCDOnline_Click(object sender, EventArgs e)
        {
            this.folderBrowserDialog1.SelectedPath = txtOnlineCDPath.Text;
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.txtOnlineCDPath.Text = this.folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnBrowseMSDSFiles_Click(object sender, EventArgs e) {
            this.folderBrowserDialog1.SelectedPath = txtMSDSFilesPath.Text;
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                this.txtMSDSFilesPath.Text = this.folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnBrowseAULFiles_Click(object sender, EventArgs e) {
            this.folderBrowserDialog1.SelectedPath = txtAULFilePath.Text;
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                this.txtAULFilePath.Text = this.folderBrowserDialog1.SelectedPath;
            }
        }

        //Show Password box dialog.
        private void chkPassword_CheckedChanged(object sender, EventArgs e)
        {
            this.txtPasswordBox.UseSystemPasswordChar = !this.chkPassword.Checked;
        }

        //Cancel Button dialog
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Begins the HMIRS Importer process, if all user specified data is valid.
        private void btnSave_Click(object sender, EventArgs e)
        {
            // validate input
            string validationMessage = "";

            // MSDS
            if (this.txtOnlineCDPath.Text == "" || this.txtMSDSFilesPath.Text == "") {
                validationMessage += "\nMSDS Settings\n";
                if (this.txtOnlineCDPath.Text == "") {
                    validationMessage += "\tPlease specify the path to the CD Online application folder\n";
                }

                if (this.txtMSDSFilesPath.Text == "") {
                    validationMessage += "\tPlease specify the path to the HILT HUB MSDS Files folder\n";
                }
            }

            // AUL
            if (this.txtAULFilePath.Text == "") {
                validationMessage += "\nAUL Settings\n\tPlease specify the path to the AUL Files folder\n";
            }

            // Server
            string serverSettings = "\nHILT HUB Database Settings\n";
            if (this.txtPasswordBox.Text == "") {
                if (!validationMessage.Contains(serverSettings))
                    validationMessage += serverSettings;
                validationMessage += "\nPlease specify the HILT database password\n";
            }

            if (chkLocalMachine.Checked) {
                // Allow zero port for local machines
                txtPortBox.Text = "";
            }
            else {
                if (this.txtPortBox.Text == "") {
                    if (!validationMessage.Contains(serverSettings))
                        validationMessage += serverSettings;
                    validationMessage += "Please specify the HILT database port\n";
                }

                // Ensure the port is a positive int
                else {
                    int port = 1;
                    try {
                        port = int.Parse(this.txtPortBox.Text);

                        if (port <= 0) {
                            if (!validationMessage.Contains(serverSettings))
                                validationMessage += serverSettings;
                            validationMessage += "The port must be a positive integer greater than 0.\n";
                        }
                    }
                    catch (Exception) {
                        if (!validationMessage.Contains(serverSettings))
                            validationMessage += serverSettings;
                        validationMessage += "The port must be a positive integer greater than 0.\n";
                    }
                }
            }

            if (this.txtServerBox.Text == "")
            {
                if (!validationMessage.Contains(serverSettings))
                    validationMessage += serverSettings;
                validationMessage += "Please specify the HILT database server\n";
            }

            if (this.txtUsernameBox.Text == "")
            {
                if (!validationMessage.Contains(serverSettings))
                    validationMessage += serverSettings;
                validationMessage += "Please specify the HILT database user\n";
            }

            if (validationMessage != "")
            {
                MessageBox.Show(validationMessage, "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // ensure the online cd path is actually a directory
            if(!Directory.Exists(this.txtOnlineCDPath.Text)) {
                MessageBox.Show(this.txtOnlineCDPath.Text + " does not exist or is not a directory", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Save the properties
            Boolean saved = saveProperties();

            // Close the form
            this.Close();
        }

        private void btnTestConnection_Click(object sender, EventArgs e) {
            String connectMsg = null;

            // Build the connection string
            String connString = buildConnectionString();

            DatabaseManager mgr = new DatabaseManager(connString);

            connectMsg = mgr.testConnection(connString);

            MessageBox.Show(connString + "\n\n" + connectMsg, "Test Connection");
        }

        private String buildConnectionString() {
            // Build the connection string
            StringBuilder sb = new StringBuilder();
            sb.Append("Data Source=" + txtServerBox.Text);
            if (txtPortBox.Text.Length > 0)
                sb.Append("," + txtPortBox.Text);
            sb.Append(";Initial Catalog=" + txtDatabase.Text);
            sb.Append(";Persist Security Info=False;uid=" + txtUsernameBox.Text);
            sb.Append(";pwd=" + txtPasswordBox.Text);

            return sb.ToString();
        }

        private void chkLocalMachine_CheckedChanged(object sender, EventArgs e) {
            if (chkLocalMachine.Checked) {
                txtPortBox.Text = "";
            }
        }
    }
}
