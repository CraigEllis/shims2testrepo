﻿namespace HILT_IMPORTER
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOnlineCDPath = new System.Windows.Forms.TextBox();
            this.btnBrowseCDOnline = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.msdsGroupBox = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMSDSFilesPath = new System.Windows.Forms.TextBox();
            this.btnBrowseMSDSFiles = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtHubDataPath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkPassword = new System.Windows.Forms.CheckBox();
            this.txtPasswordBox = new System.Windows.Forms.TextBox();
            this.txtUsernameBox = new System.Windows.Forms.TextBox();
            this.txtPortBox = new System.Windows.Forms.TextBox();
            this.txtServerBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtAULFilePath = new System.Windows.Forms.TextBox();
            this.aulGroupBox = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnBrowseAULFiles = new System.Windows.Forms.Button();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.chkLocalMachine = new System.Windows.Forms.CheckBox();
            this.msdsGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.aulGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Default CD Folder:";
            // 
            // txtOnlineCDPath
            // 
            this.txtOnlineCDPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOnlineCDPath.Location = new System.Drawing.Point(110, 35);
            this.txtOnlineCDPath.Name = "txtOnlineCDPath";
            this.txtOnlineCDPath.Size = new System.Drawing.Size(292, 20);
            this.txtOnlineCDPath.TabIndex = 2;
            this.txtOnlineCDPath.Text = "C:\\HMIRS Importer";
            this.toolTip1.SetToolTip(this.txtOnlineCDPath, "The folder where the HMIRS OnlineCd application and data are located");
            // 
            // btnBrowseCDOnline
            // 
            this.btnBrowseCDOnline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseCDOnline.Location = new System.Drawing.Point(408, 33);
            this.btnBrowseCDOnline.Name = "btnBrowseCDOnline";
            this.btnBrowseCDOnline.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseCDOnline.TabIndex = 3;
            this.btnBrowseCDOnline.Text = "Browse...";
            this.btnBrowseCDOnline.UseVisualStyleBackColor = true;
            this.btnBrowseCDOnline.Click += new System.EventHandler(this.btnBrowseCDOnline_Click);
            // 
            // msdsGroupBox
            // 
            this.msdsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.msdsGroupBox.Controls.Add(this.label8);
            this.msdsGroupBox.Controls.Add(this.label7);
            this.msdsGroupBox.Controls.Add(this.label6);
            this.msdsGroupBox.Controls.Add(this.txtMSDSFilesPath);
            this.msdsGroupBox.Controls.Add(this.btnBrowseMSDSFiles);
            this.msdsGroupBox.Controls.Add(this.label1);
            this.msdsGroupBox.Controls.Add(this.txtOnlineCDPath);
            this.msdsGroupBox.Controls.Add(this.btnBrowseCDOnline);
            this.msdsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.msdsGroupBox.Name = "msdsGroupBox";
            this.msdsGroupBox.Size = new System.Drawing.Size(494, 122);
            this.msdsGroupBox.TabIndex = 0;
            this.msdsGroupBox.TabStop = false;
            this.msdsGroupBox.Text = "MSDS Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Hilt Hub Import";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "HMIRS Extract";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "MSDS Files Folder:";
            // 
            // txtMSDSFilesPath
            // 
            this.txtMSDSFilesPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMSDSFilesPath.Location = new System.Drawing.Point(110, 87);
            this.txtMSDSFilesPath.Name = "txtMSDSFilesPath";
            this.txtMSDSFilesPath.Size = new System.Drawing.Size(292, 20);
            this.txtMSDSFilesPath.TabIndex = 6;
            this.txtMSDSFilesPath.Text = "C:\\HILT_HUB\\MSDS_Files";
            this.toolTip1.SetToolTip(this.txtMSDSFilesPath, "The folder in the HILT_HUB application where the MSDS files will be stored");
            // 
            // btnBrowseMSDSFiles
            // 
            this.btnBrowseMSDSFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseMSDSFiles.Location = new System.Drawing.Point(408, 85);
            this.btnBrowseMSDSFiles.Name = "btnBrowseMSDSFiles";
            this.btnBrowseMSDSFiles.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseMSDSFiles.TabIndex = 7;
            this.btnBrowseMSDSFiles.Text = "Browse...";
            this.btnBrowseMSDSFiles.UseVisualStyleBackColor = true;
            this.btnBrowseMSDSFiles.Click += new System.EventHandler(this.btnBrowseMSDSFiles_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkLocalMachine);
            this.groupBox2.Controls.Add(this.btnTestConnection);
            this.groupBox2.Controls.Add(this.txtHubDataPath);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtDatabase);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.chkPassword);
            this.groupBox2.Controls.Add(this.txtPasswordBox);
            this.groupBox2.Controls.Add(this.txtUsernameBox);
            this.groupBox2.Controls.Add(this.txtPortBox);
            this.groupBox2.Controls.Add(this.txtServerBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 231);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 218);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "HILT HUB Database Settings";
            // 
            // txtHubDataPath
            // 
            this.txtHubDataPath.Location = new System.Drawing.Point(110, 180);
            this.txtHubDataPath.Name = "txtHubDataPath";
            this.txtHubDataPath.Size = new System.Drawing.Size(251, 20);
            this.txtHubDataPath.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 183);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "HUB Data Folder:";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(110, 74);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(251, 20);
            this.txtDatabase.TabIndex = 6;
            this.txtDatabase.Text = "hilt_hub";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Database:";
            // 
            // chkPassword
            // 
            this.chkPassword.AutoSize = true;
            this.chkPassword.Location = new System.Drawing.Point(110, 146);
            this.chkPassword.Name = "chkPassword";
            this.chkPassword.Size = new System.Drawing.Size(102, 17);
            this.chkPassword.TabIndex = 11;
            this.chkPassword.Text = "Show Password";
            this.chkPassword.UseVisualStyleBackColor = true;
            this.chkPassword.CheckedChanged += new System.EventHandler(this.chkPassword_CheckedChanged);
            // 
            // txtPasswordBox
            // 
            this.txtPasswordBox.Location = new System.Drawing.Point(110, 123);
            this.txtPasswordBox.Name = "txtPasswordBox";
            this.txtPasswordBox.Size = new System.Drawing.Size(251, 20);
            this.txtPasswordBox.TabIndex = 10;
            this.txtPasswordBox.Text = "ashore";
            this.txtPasswordBox.UseSystemPasswordChar = true;
            // 
            // txtUsernameBox
            // 
            this.txtUsernameBox.Location = new System.Drawing.Point(110, 100);
            this.txtUsernameBox.Name = "txtUsernameBox";
            this.txtUsernameBox.Size = new System.Drawing.Size(251, 20);
            this.txtUsernameBox.TabIndex = 8;
            this.txtUsernameBox.Text = "ashore";
            // 
            // txtPortBox
            // 
            this.txtPortBox.Location = new System.Drawing.Point(110, 48);
            this.txtPortBox.Name = "txtPortBox";
            this.txtPortBox.Size = new System.Drawing.Size(89, 20);
            this.txtPortBox.TabIndex = 3;
            this.txtPortBox.Text = "1434";
            // 
            // txtServerBox
            // 
            this.txtServerBox.Location = new System.Drawing.Point(110, 25);
            this.txtServerBox.Name = "txtServerBox";
            this.txtServerBox.Size = new System.Drawing.Size(251, 20);
            this.txtServerBox.TabIndex = 1;
            this.txtServerBox.Text = ".\\SQLEXPRESS2008";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Username:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Port:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Server Name:";
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.Location = new System.Drawing.Point(302, 464);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(87, 23);
            this.saveBtn.TabIndex = 3;
            this.saveBtn.Text = "&Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.Location = new System.Drawing.Point(395, 464);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 23);
            this.cancelBtn.TabIndex = 4;
            this.cancelBtn.Text = "&Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtAULFilePath
            // 
            this.txtAULFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAULFilePath.Location = new System.Drawing.Point(110, 22);
            this.txtAULFilePath.Name = "txtAULFilePath";
            this.txtAULFilePath.Size = new System.Drawing.Size(292, 20);
            this.txtAULFilePath.TabIndex = 1;
            this.txtAULFilePath.Text = "C:\\Import\\AUL";
            this.toolTip1.SetToolTip(this.txtAULFilePath, "The folder where the AUL (SMCL, SHML) files to import are located");
            // 
            // aulGroupBox
            // 
            this.aulGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aulGroupBox.Controls.Add(this.label12);
            this.aulGroupBox.Controls.Add(this.txtAULFilePath);
            this.aulGroupBox.Controls.Add(this.btnBrowseAULFiles);
            this.aulGroupBox.Location = new System.Drawing.Point(12, 147);
            this.aulGroupBox.Name = "aulGroupBox";
            this.aulGroupBox.Size = new System.Drawing.Size(494, 67);
            this.aulGroupBox.TabIndex = 1;
            this.aulGroupBox.TabStop = false;
            this.aulGroupBox.Text = "AUL Settings";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Default AUL Folder:";
            // 
            // btnBrowseAULFiles
            // 
            this.btnBrowseAULFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseAULFiles.Location = new System.Drawing.Point(408, 20);
            this.btnBrowseAULFiles.Name = "btnBrowseAULFiles";
            this.btnBrowseAULFiles.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseAULFiles.TabIndex = 2;
            this.btnBrowseAULFiles.Text = "Browse...";
            this.btnBrowseAULFiles.UseVisualStyleBackColor = true;
            this.btnBrowseAULFiles.Click += new System.EventHandler(this.btnBrowseAULFiles_Click);
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestConnection.Location = new System.Drawing.Point(383, 25);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(100, 23);
            this.btnTestConnection.TabIndex = 13;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // chkLocalMachine
            // 
            this.chkLocalMachine.AutoSize = true;
            this.chkLocalMachine.Location = new System.Drawing.Point(221, 50);
            this.chkLocalMachine.Name = "chkLocalMachine";
            this.chkLocalMachine.Size = new System.Drawing.Size(77, 17);
            this.chkLocalMachine.TabIndex = 4;
            this.chkLocalMachine.Text = "Local Host";
            this.chkLocalMachine.UseVisualStyleBackColor = true;
            this.chkLocalMachine.CheckedChanged += new System.EventHandler(this.chkLocalMachine_CheckedChanged);
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 496);
            this.Controls.Add(this.aulGroupBox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.msdsGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.Text = "HILT Import Setup";
            this.msdsGroupBox.ResumeLayout(false);
            this.msdsGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.aulGroupBox.ResumeLayout(false);
            this.aulGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOnlineCDPath;
        private System.Windows.Forms.Button btnBrowseCDOnline;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox msdsGroupBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPasswordBox;
        private System.Windows.Forms.TextBox txtUsernameBox;
        private System.Windows.Forms.TextBox txtPortBox;
        private System.Windows.Forms.TextBox txtServerBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkPassword;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMSDSFilesPath;
        private System.Windows.Forms.Button btnBrowseMSDSFiles;
        private System.Windows.Forms.GroupBox aulGroupBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAULFilePath;
        private System.Windows.Forms.Button btnBrowseAULFiles;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtHubDataPath;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.CheckBox chkLocalMachine;
    }
}

