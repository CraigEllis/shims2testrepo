﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace HILT_IMPORTER
{
    public class SMCLImporter
    {
        CSVParser parser = new CSVParser();
        List<string[]> niinList = new List<string[]>();

        //Initialize variables
        private string serverPath;
        private string username;
        private string password;
        private string uploadFilePath;
        private int port;

        //Initialize Log File
        FileInfo niinLog;

        private SqlConnection conn;

       /* Method used to insert records into the NIIN Catalog.  These
        * records come from a tab delimited file that is uploaded and
        * parsed using the CSVParser.  The parser returns a List of String 
        * arrays that are then used to create valid data records for the niin-catalog.
        */
        public void populateNiinCatalog()
        {
            #region HILT DB Connection Test
            try {
                conn = createConnection(this.serverPath, this.port, this.username, this.password);
                conn.Open();
                conn.Close();
            } catch (Exception) {
                throw new ImportException("Could not connect to the HILT database with the specified host: " + this.ServerPath + " port: " + 
                                            this.Port + " Username: " + this.Username + " Password: " + this.Password);                
            }
            #endregion

            try{
                Stream fileStream = new FileStream(uploadFilePath, FileMode.Open, FileAccess.Read);
                niinList = parser.parseCSV(fileStream);
            } catch (Exception ex) {
                throw new ImportException("Error reading from the specified file.  Returned the following error " + ex.Message);
            }

            if (niinList.Count != 0)
            {
                this.updateSMCLFromList(niinList);
            }
            else
            {
                throw new ImportException("Specified file is empty, stopping process.");
            }
        }


         /* Method used to insert records into the NIIN Catalog.  These
        * records come from a tab delimited file that is uploaded and
        * parsed using the CSVParser.  The parser returns a List of String 
        * arrays that are then used to create valid data records for the niin-catalog.
        */
        public void updateSMCLFromList(List<string[]> itemList)
        {

            #region Variable Declaration
            //Initialize variables.
            Dictionary<string, int> niinMap = this.getNIINCatalogMap();
            Dictionary<string, int> niinATCMap = this.getNiinATCMap();
            Dictionary<string, int> usageCategoryMap = this.getUsageCategoryMap();
            Dictionary<string, int> smccMap = this.getSmccMap();
            Dictionary<string, int> slcMap = this.getSlcMap();
            Dictionary<string, int> slacMap = this.getSlacMap();
            Dictionary<string, int> storageTypeMap = this.getStorageTypeMap();
            Dictionary<string, int> cogMap = this.getCogMap();
            Dictionary<string, int> catalogGroupMap = this.getCatalogGroupMap();
            
            //Used to track total number of records, inserted records, and skipped.
            int inserted = 0, skipped = 0;

            //Opening the connection.
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();

            //Create SQL Commands for use in inserting and updating.
            string insertStmt = "INSERT INTO AUTH_USE_LIST ( fsc, niin, ui, um, usage_category_id, description, smcc_id, specs," +
                                    "shelf_life_code_id, shelf_life_action_code_id, remarks, storage_type_id, cog_id, spmig, nehc_rpt," +
                                    "catalog_group_id, catalog_serial_number, allowance_qty, ship_id, created, manually_entered)"
                                 + "VALUES( @fsc, @niin, @ui, @um, @usage_category_id, @description, @smcc_id, @specs," +
                                    "@shelf_life_code_id, @shelf_life_action_code_id, @remarks, @storage_type_id, @cog_id, @spmig, @nehc_rpt," +
                                    "@catalog_group_id, @catalog_serial_number, @allowance_qty, @ship_id, @created, @manually_entered )";


            SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
            insertCmd.Transaction = tran;

            //Create a log file to which to write.
            this.createLogFile();
            #endregion

            /* First create a list of column headers
             * since we know the first row is always headers.
             */
            string[] headers = niinList[0];

            foreach (string[] row in niinList)  {

                //Initialize variables.
                int atc = 0, fsc = 0, usage_category_id = 0, smcc_id = 0, shelf_life_code_id = 0, shelf_life_action_code_id = 0, storage_type_id = 0;
                int catalog_group_id = 0, cog_id = 0, allowance_qty = 0, manually_entered = 0;
                int ship_id = 1;  //TEMPORARY TO DO
                string niin = "", ui = "", um = "", description = "", specs = "", remarks = "", spmig = "", nehc_rpt = "", catalog_serial_number = "", cosal = "";
                DateTime created = DateTime.Now;

                //The first row of the list will always be HEADERS.
                if (niinList[0] != row)  {

                    for (int i = 0; i < row.Length; i++)
                    {
                        //Capture the ATC value.
                        if (headers[i].ToUpper().Equals("ATC"))
                        {
                            try
                            {
                                atc = Int32.Parse(row[i]);

                                //Using the ACT value, update the COSAL value.
                                if (atc == 1)
                                {
                                    cosal = "HME";
                                }
                                else if (atc == 2)
                                {
                                    cosal = "Q";
                                }
                                else if (atc == 8)
                                {
                                    cosal = "OSI";
                                }
                            }
                            catch { System.Diagnostics.Debug.WriteLine("ATC"); }
                        }
                        //Capture the FSC value.
                        else if (headers[i].ToUpper().Equals("FSC"))
                        {
                            try
                            {
                                fsc = Int32.Parse(row[i]);
                            }
                            catch { System.Diagnostics.Debug.WriteLine("FSC"); }
                        }
                        //Capture the NIIN value.  
                        //Now that we now NIIN and Cosal, verify that this record is unique.
                        else if (headers[i].ToUpper().Equals("NIIN"))
                        {
                            niin = row[i];
                        }
                        //Capture the Usage Category value.
                        else if (headers[i].ToUpper().Equals("USAGE_CATEGORY"))
                        {
                            if (usageCategoryMap.ContainsKey(row[i]))
                                usage_category_id = usageCategoryMap[row[i]];
                        }
                        //Capture the Description value.
                        else if (headers[i].ToUpper().Equals("DESCRIPTION"))
                        {
                            description = row[i];
                        }
                        //Capture the UI value.
                        else if (headers[i].ToUpper().Equals("UI"))
                        {
                            ui = row[i];
                        }
                        //Capture the SMCC value.
                        else if (headers[i].ToUpper().Equals("SMCC"))
                        {
                            if (smccMap.ContainsKey(row[i]))
                                smcc_id = smccMap[row[i]];
                        }
                        //Capture the UM value.
                        else if (headers[i].ToUpper().Equals("UM"))
                        {
                            um = row[i];
                        }
                        //Capture the Specs value.
                        else if (headers[i].ToUpper().Equals("SPECS"))
                        {
                            specs = row[i];
                        }
                        //Capture the Storage Type value.
                        else if (headers[i].ToUpper().Equals("STORAGE_TYPE"))
                        {
                            if (storageTypeMap.ContainsKey(row[i]))
                                storage_type_id = storageTypeMap[row[i]];
                        }
                        //Capture the COG value.
                        else if (headers[i].ToUpper().Equals("COG"))
                        {
                            if (cogMap.ContainsKey(row[i]))
                                cog_id = cogMap[row[i]];
                        }
                        //Capture the Shelf Life Code ID.
                        else if (headers[i].ToUpper().Equals("SHELF_LIFE_CODE"))
                        {
                            if (slcMap.ContainsKey(row[i]))
                                shelf_life_code_id = slcMap[row[i]];
                        }
                        //Capture the Shelf Life Action Code ID.
                        else if (headers[i].ToUpper().Equals("SHELF_LIFE_ACTION_CODE"))
                        {
                            if (slacMap.ContainsKey(row[i]))
                                shelf_life_action_code_id = slacMap[row[i]];
                        }
                        //Capture the REMARKS value.
                        else if (headers[i].ToUpper().Equals("REMARKS"))
                        {
                            remarks = row[i];
                        }
                        //Capture the SPMIG value.
                        else if (headers[i].ToUpper().Equals("SPMIG"))
                        {
                            spmig = row[i];
                        }
                        //Capture the NEHC_RPT value.
                        else if (headers[i].ToUpper().Equals("NEHC_RPT"))
                        {
                            nehc_rpt = row[i];
                        }
                        //Capture the CATALOG_GROUP value.
                        else if (headers[i].ToUpper().Equals("CATALOG_GROUP"))
                        {
                            if (catalogGroupMap.ContainsKey(row[i]))
                                catalog_group_id = catalogGroupMap[row[i]];
                        }
                        //Capture the Catalog Serial Number value.
                        else if (headers[i].ToUpper().Equals("CATALOG_SERIAL_NUMBER"))
                        {
                            catalog_serial_number = row[i];
                        }
                        //Capture the Allowance Qty value
                        else if (headers[i].ToUpper().Equals("ALLOWANCE_QTY"))
                        {
                            allowance_qty = Int32.Parse(row[i]);
                        }

                    }  //End of parsing the row for values.

                    //insertCmd.Parameters.AddWithValue("@atc", dbNull(atc));
                    insertCmd.Parameters.AddWithValue("@fsc", dbNull(fsc));
                    insertCmd.Parameters.AddWithValue("@niin", dbNull(niin));
                    insertCmd.Parameters.AddWithValue("@ui", dbNull(ui));
                    insertCmd.Parameters.AddWithValue("@um", dbNull(um));
                    insertCmd.Parameters.AddWithValue("@usage_category_id", dbNull(usage_category_id));
                    insertCmd.Parameters.AddWithValue("@description", dbNull(description));
                    insertCmd.Parameters.AddWithValue("@smcc_id", dbNull(smcc_id));
                    insertCmd.Parameters.AddWithValue("@specs", dbNull(specs));
                    insertCmd.Parameters.AddWithValue("@shelf_life_code_id", dbNull(shelf_life_code_id));
                    insertCmd.Parameters.AddWithValue("@shelf_life_action_code_id", dbNull(shelf_life_action_code_id));
                    insertCmd.Parameters.AddWithValue("@remarks", dbNull(remarks));
                    insertCmd.Parameters.AddWithValue("@storage_type_id", dbNull(storage_type_id));
                    insertCmd.Parameters.AddWithValue("@cog_id", dbNull(cog_id));
                    insertCmd.Parameters.AddWithValue("@spmig", dbNull(spmig));
                    insertCmd.Parameters.AddWithValue("@nehc_rpt", dbNull(nehc_rpt));
                    insertCmd.Parameters.AddWithValue("@catalog_group_id", dbNull(catalog_group_id));
                    insertCmd.Parameters.AddWithValue("@catalog_serial_number", dbNull(catalog_serial_number));
                    insertCmd.Parameters.AddWithValue("@allowance_qty", dbNull(allowance_qty));
                    insertCmd.Parameters.AddWithValue("@ship_id", dbNull(ship_id));
                    insertCmd.Parameters.AddWithValue("@created", dbNull(DateTime.Now));
                    insertCmd.Parameters.AddWithValue("@manually_entered", dbNull(manually_entered));
                    //insertCmd.Parameters.AddWithValue("@cosal", dbNull(cosal));

                    try
                    {
                        insertCmd.ExecuteNonQuery();
                        inserted++;
                    }
                    catch (Exception ex) {
                        //Duplicates
                        this.writeToErrorLog(this.niinLog, "NIIN: " + niin + "\tATC: " + atc + "\tError: " + ex.Message);
                        skipped++;
                    }

                    insertCmd.Parameters.Clear();

                } //End of looping through for each row.
            }
              
            try
            {
                tran.Commit();
                System.Diagnostics.Debug.WriteLine("RECORDS INSERTED:  " + inserted + "   RECORDS SKIPPED:  " + skipped);
            }
            catch (Exception ex) {                
                throw new ImportException("Error committing transaction!  Error: " + ex.Message); 
            }

            conn.Close();

            DialogResult reply = MessageBox.Show("NIIN Catalog has been populated.  Records Inserted:  " + inserted + 
                                        "  Duplicates/Skipped Records:  " + skipped, "Populate NIIN Catalog", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        #region LOGGING
        // Method for storing errors encountered during the File Parsing process.
        private void writeToErrorLog(FileInfo file, String errMsg)
        {
            String date = "" + System.DateTime.Now;
            StreamWriter sw = File.AppendText(file.FullName);
            sw.WriteLine(date + "\t" + errMsg);
            sw.Close();
        }

        private void createLogFile()
        {
            String path = "";
            string currentDate = System.DateTime.Now.ToString("MM-dd-yyyy");

            try
            {
                //Grab the path to the Application Data folder for this user.
                path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path += "\\HILT";

                //If the defined path does not exist, create it.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                //Stamp the folder with the current date.
                path += "\\niin_catalog_import_" + currentDate;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                path += "\\logs";

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);                    
                }

                this.niinLog = new FileInfo(path + @"\populateNiinErrorLog.txt");

            }
            catch (Exception ex)
            {
                throw new ImportException("Error occurred when creating log file:  " + ex.Message);
            }
        }
        #endregion

        // Methods for creating Dictionary Mapping Tables.
        #region Mapping Tables
         public Dictionary<string, int> getMap(String selectStmt)
        {
            Dictionary<string, int> map = new Dictionary<string, int>();

            conn.Open();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                if (!map.ContainsKey("" + reader.GetValue(0)))
                    map.Add("" + reader.GetValue(0), Int32.Parse(""+reader.GetValue(1)));
            }

            reader.Dispose();
            reader.Close();

            conn.Close();

            return map;

        }

        public Dictionary<string, int> getMapforDuplicates(String selectStmt)
        {
            //Initialize Variables.
            Dictionary<string, int> map = new Dictionary<string, int>();

            //Open connection for use.
            conn.Open();

            //Initialize some SQL variables for reading data.
            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);
            SqlDataReader reader = cmdRead.ExecuteReader();

            while (reader.Read()) {
                if (!map.ContainsKey("" + reader.GetValue(0) + reader.GetValue(1)))
                    map.Add("" + reader.GetValue(0) + reader.GetValue(1), Int32.Parse("" + reader.GetValue(2)));
            }

            //Closing variables.
            reader.Dispose();
            reader.Close();
            conn.Close();

            return map;
        }
       
        public Dictionary<string, int> getNiinATCMap()
        {
            string selectStmt = "Select NIIN, aul_id from auth_use_list";
            return getMapforDuplicates(selectStmt);
        }

        public Dictionary<string, int> getNIINCatalogMap()
        {
            string selectStmt = "Select NIIN, aul_id from auth_use_list";                        
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getUsageCategoryMap()
        {
            string selectStmt = "Select category, usage_category_id from usage_category";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSmccMap()
        {
            string selectStmt = "Select smcc, smcc_id from smcc";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlcMap()
        {
            string selectStmt = "Select slc, shelf_life_code_id from shelf_life_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlacMap()
        {
            string selectStmt = "Select slac, shelf_life_action_code_id from shelf_life_action_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getStorageTypeMap()
        {
            string selectStmt = "Select type, storage_type_id from storage_type";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getCogMap()
        {
            string selectStmt = "Select cog, cog_id from cog_codes";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getCatalogGroupMap()
        {
            string selectStmt = "Select group_name, catalog_group_id from catalog_groups";
            return getMap(selectStmt);
        }
        #endregion

        /* Methods for connecting to and a db value insertion
         * validation methods.
         */
        #region Database Methods
        protected string getConnectionString(string host, int port, string user, string pass) {
            return "Data Source=" + host + ";Initial Catalog=ashore;Integrated Security=False;User Id=" + user + ";PWD= " + pass + ";Min pool size=5;Max pool size=100;connection timeout=15;pooling=yes";
        }

        protected SqlConnection createConnection(string host, int port, string user, string pass)  {
            SqlConnection conn = new SqlConnection(this.getConnectionString(host, port, user, pass));
            return conn;
        }

        /* Method created by Adam Hale to check a value before db insertion.
         * If this object is blank or null it replaces it with a DBNull value.
         */
        private object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            } else {
                return DBNull.Value;
            }
        }
        #endregion

        //Getters and Setters.
        #region Getters/Setters
        public string UploadFilePath
        {
            get { return uploadFilePath; }
            set { uploadFilePath = value; }
        }

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string ServerPath
        {
            get { return serverPath; }
            set { serverPath = value; }
        }
        #endregion

    } //End of class smclImporter.
}