﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.ComponentModel;
//using ICSharpCode.SharpZipLib.Core;
//using ICSharpCode.SharpZipLib.GZip;
//using ICSharpCode.SharpZipLib.Tar;
using Ionic.Zip;
//using System.IO.Compression;
using HILT_IMPORTER.DataObjects;


namespace HILT_IMPORTER.ImportExport {
    /// <summary>
    /// Extracts the documents from the HMIRS data CDs
    /// Process is basically:
    /// 1. check that the CD data file exists
    /// 2. get the list of document data for that CD (doc_data_id, type, format, ...
    /// 3. open the dat file and start reading the data
    ///     a. if the line contains the magic bytes:
    ///         1. get the starting position of the file in the dat file
    ///         2. extract the file using DotNetZip gzipstream saving it
    ///             to a file using the doc-data_id from HMIRS as the key value
    ///     b. repeat step a for all lines/files in the dat file. 
    /// </summary>
    class HMIRSDocProcessor {
        public enum DOC_FORMATS {
            HTML,
            PDF,
            RTF,
            UNK
        };
        public enum DOC_TYPES {
            MSDS,
            MSDS_TRAN,
            MFG_LABEL,
            PROD_SHT,
            TRANS_CERT,
            NESHAP,
            OTHER
        };

        private MSDSImportForm m_form = null;
        private BackgroundWorker m_worker = null;

        // Byte arrays for finding the gzip magic bytes
        // magicBytesStart is used at the start of the document
        private byte[] magicBytesStart = { 0x1f, 0x8b, 0x08, 0x00 };
        // magicBytesSearch is used after the first gzip file was found 
        //    (includes the null char at the end of the previous file)
        private byte[] magicBytesSearch = { 0x00, 0x1f, 0x8b, 0x08, 0x00 };

        public HMIRSDocProcessor(MSDSImportForm form, BackgroundWorker worker) {
            m_form = form;
            m_worker = worker;
        }        


        public Dictionary<DOC_FORMATS, int> getDocumentFormatCounts(string diskFileName) {
            int[] counts = { 0, 0, 0, 0 };

            Dictionary<DOC_FORMATS, int> htType = new Dictionary<DOC_FORMATS, int>();
            htType.Add(DOC_FORMATS.HTML, 0);
            htType.Add(DOC_FORMATS.PDF, 0);
            htType.Add(DOC_FORMATS.RTF, 0);
            htType.Add(DOC_FORMATS.UNK, 0);

            String sHTML = "<HTML>";
            //String sHTML_END = "</HTML>";
            String sPDF = "%PDF";
            //String sPDF_END1 = "%%EO\\x00";
            //String sPDF_END2 = "%%EOF\\x00";
            String sRTF = "{\\\\rtf";
            //String sRTF_END = "\\\\par}}";

            DateTime dtStart = DateTime.Now;
            long nStart = Environment.TickCount;

            // Open the file and count the different file types

            using(StreamReader reader = new StreamReader(diskFileName)) {
                //bool bFound = false;
                long nCount = 0;
                string row = "";
                string rowSub = "";
                try {
                    while (reader.Peek() >= 0) {
                        //Application.DoEvents();
                        row = reader.ReadLine();
                        rowSub = row.Replace("\x00", ""); // Ignore nulls
                        if (row.Length > 30) {
                            rowSub = row.Substring(0, 30);
                        }
                        nCount++;

                        if (nCount % 1000 == 0) {
                            if (m_form != null)
                            ((HiltImportMDIForm)m_form.MdiParent).StatusBarCount_Value = nCount;
                        }

                        int length = row.Length;
                        if (length > 20)
                            length = 20;
                        // 3 variations of HTML documents
                        if (Regex.IsMatch(row, "DOCTYPE HTML", RegexOptions.IgnoreCase)) {
                            htType[DOC_FORMATS.HTML]++;
                        }
                        if (Regex.IsMatch(row, "<HTML ", RegexOptions.IgnoreCase)) {
                            htType[DOC_FORMATS.HTML]++;
                        }
                        else if (Regex.IsMatch(row, sHTML, RegexOptions.IgnoreCase)) {
                            // Check for a <BODY or <HR> on the the next line
                            row = reader.ReadLine();
                            if (row.StartsWith("<BODY") || row.StartsWith("<HR>")) {
                                htType[DOC_FORMATS.HTML]++;
                            }
                            nCount++;
                        }
                        // PDF document
                        else if (Regex.IsMatch(row, sPDF, RegexOptions.IgnoreCase)) {
                            htType[DOC_FORMATS.PDF]++;
                        }
                        // RTF document
                        else if (Regex.IsMatch(row, sRTF, RegexOptions.IgnoreCase)) {
                            htType[DOC_FORMATS.RTF]++;
                        }
                    }

                    DateTime dtEnd = DateTime.Now;
                    TimeSpan ts = dtEnd.Subtract(dtStart);
                    long nEnd = Environment.TickCount;
                    long tsMilliseconds = nEnd - nStart;

                    StringBuilder sb = new StringBuilder("\n" + diskFileName + " processed in ");
                    if (tsMilliseconds < 1000) {
                        sb.Append(tsMilliseconds.ToString() + " (ms)");
                    }
                    else {
                        sb.AppendLine((tsMilliseconds / 1000) + "seconds");
                    }
                    sb.AppendLine("\tLines read: " + nCount.ToString());
                    sb.AppendLine("\tHTML Files: " + htType[DOC_FORMATS.HTML].ToString());
                    sb.AppendLine("\tPDF Files: " + htType[DOC_FORMATS.PDF].ToString());
                    sb.AppendLine("\tRTF Files: " + htType[DOC_FORMATS.RTF].ToString());
                    sb.AppendLine("Start Time: " + dtStart.ToLongTimeString());
                    sb.Append("\tEnd Time: " + dtEnd.ToLongTimeString());
                    if (m_worker != null)
                        m_worker.ReportProgress(100, sb.ToString());
                }
                catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine("ERROR - getDocumentFormatCounts: " + ex.Message);
                }
            }

            return htType;
        }

        public int countDiskDocuments(string zipFileName) {
            bool result = true;
            bool bStart = true;
            int nCount = 0;
            long chunkSize = 1024;
            byte[] magicBytes = magicBytesStart;

            // Open the document and start reading
            byte[] readBuffer = new byte[chunkSize];
            int startIndex = 0;
            long moveBack = 0;

            using (Stream inStream = File.OpenRead(zipFileName)) {
                int bytesRead = 0;
                long startPosition = 0;
                bool firstDoc = true;

                while ((bytesRead = inStream.Read(readBuffer, 0, readBuffer.Length)) != 0) {
                    if (!bStart)
                        magicBytes = magicBytesSearch;

                    startIndex = searchForBytePattern(readBuffer, magicBytes);
                    bStart = false;

                    if (startIndex >= 0) {
                        if (!firstDoc)
                            startIndex++; // Move the start index up to avoid the null char

                        // Increment the document list count
                        nCount++;
                        firstDoc = false;

                        // Print out the found bytes
                        using (Stream tempStream = File.OpenRead(zipFileName)) {
                            long readPos = startPosition + startIndex - 1;
                            int readSize = 11;
                            if (readPos < 0) {
                                readPos = 0;
                                readSize = 10;
                            }
                            tempStream.Position = readPos;

                            byte[] temp = new byte[readSize];
                            int nRead = tempStream.Read(temp, 0, readSize);

                            System.Diagnostics.Debug.WriteLine(nCount + ": " + (startPosition
                                + startIndex).ToString() + ": " + BitConverter.ToString(temp));
                        }
                    }
                    else {
                        // No match found - back up the length of the pattern & read some more
                        if (inStream.Length == inStream.Position) break;

                        moveBack = inStream.Position - (long)magicBytes.Length - 1;
                        inStream.Position = moveBack;
                    }

                    startPosition = inStream.Position;
                }

            }

            System.Diagnostics.Debug.WriteLine("***** Documents found = " + nCount.ToString());

            return nCount;
        }

        /// <summary>
        /// Takes the HMIRS document tar file (concatenated-gzipped documents)
        /// and separates it into individual gzip files; the gzip file is then
        /// unpacked and saved to the outPath using the doc_data_id as the 
        /// beginning of the filename (i.e. doc_data_id_*.doc_file_format)'
        /// </summary>
        /// <param name="zipFileName"></param>
        /// <returns></returns>
        public int unpackDocumentFiles(string zipFileName, string outPath, 
                List<HMIRSDocumentRecord> docData) {
            bool result = true;
            bool bStart = true;
            int nDocCount = 0;  // Offset into the docData list
            int nFileCount = 0; // Number of files extracted from the tar file
            long chunkSize = 1024;
            byte[] magicBytes = magicBytesStart;

            // Get the missing document IDS
        // Several of the doc_vault records for disks 2 & 3 don't have documents on the CDs
        // We want to skip these during processing to keep things in order
            Dictionary<long, string> badDocIDs = getBadDocIDs();
            
            // Open the document and start reading
            byte[] readBuffer = new byte[chunkSize];
            int startIndex = 0;
            long moveBack = 0;

            using(Stream inStream = File.OpenRead(zipFileName)){
                int bytesRead = 0;
                long startPosition = 0;
                bool firstDoc = true;

                while ((bytesRead = inStream.Read(readBuffer, 0, readBuffer.Length)) != 0) {
                    if (!bStart)
                        magicBytes = magicBytesSearch;

                    startIndex = searchForBytePattern(readBuffer, magicBytes);
                    bStart = false;
                    
                    if (startIndex >= 0) {
                        // Move the start index up to avoid the null char
                        if (!firstDoc)
                            startIndex++;
                        //DEBUG TESTING - System.Diagnostics.Debug.Write(nFileCount.ToString() + " - "
                        //    + (startPosition + (long)startIndex).ToString() + " - ");

                        // Make sure we aren't running out of documents
                        string docFilename = "UNKNOWN_" + nFileCount.ToString() + ".unk";
                        if (nDocCount < docData.Count)
                            docFilename = docData[nDocCount].doc_file_name;
                        result = extractZipFile(zipFileName, startPosition + (long)startIndex,
                            outPath + docFilename);
                        //TODO - check return value

                        // Increment the document list count
                        nFileCount++;
                        // Update the progress bar
                        if (m_worker != null)
                            m_worker.ReportProgress(nFileCount);

                        if (nDocCount < docData.Count - 1)
                            nDocCount++;
                        // See if the new data ID is one we need to skip - the file isn't really on the CD
                        while (badDocIDs.ContainsKey(docData[nDocCount].doc_data_id))
                            nDocCount++;

                        firstDoc = false;
                    }
                    else {
                        // No match found - back up the length of the pattern & read some more
                        if (inStream.Length == inStream.Position) break;

                        moveBack = inStream.Position - (long) magicBytes.Length -1;
                        inStream.Position = moveBack;
                    }

                    startPosition = inStream.Position;
                }
            }

            return nFileCount;
        }

        private int searchForBytePattern(byte[] source, byte[] pattern)
        {
            int index = 0;
            bool isMatch = false;

            // Loop through the array searching for a match to the first magic byte
            for (index = 0; index < source.Length; index++)
            {
                if (pattern[0] == source[index] && index + pattern.Length <= source.Length)
                {
                    isMatch = true;

                    // Compare the remaining bytes
                    for (int j = 1; j < pattern.Length && isMatch == true; j++) {
                        if (pattern[j] != source[index + j])
                            isMatch = false;
                    }

                    // If we have a match we can exit
                    if (isMatch)
                        break;
                }
            }

            if (!isMatch)
                index = -1;

            return index;
        }

        public bool extractZipFile(string zipFileName, long startPosition, string extractFileName) {
            bool result = true;
            long bytesRead = 0;

                //GZipStream
                using (Stream fd = File.Create(extractFileName))
                using (Stream fs = File.OpenRead(zipFileName))
                using (Stream csStream = new Ionic.Zlib.GZipStream(fs, 
                        Ionic.Zlib.CompressionMode.Decompress)) {
                    byte[] buffer = new byte[1024];
                    int nRead;
                    try {
                        // Position the input stream to the document position
                        fs.Position = startPosition;

                        while ((nRead = csStream.Read(buffer, 0, buffer.Length)) > 0) {
                            bytesRead += nRead;
                            fd.Write(buffer, 0, nRead);
                        }
                    }
                    catch (Exception ex) {
                        System.Diagnostics.Debug.WriteLine("ERROR - extractZipFile: " + ex.Message);
                    }
                }

            return result;
        }

        private Dictionary<long, string> getBadDocIDs() {
            // Several of the doc_vault records for disks 2 & 3 don't have documents on the CDs
            // We want to skip these during processing to keep things in order
            Dictionary<long, string> badDocIDs = new Dictionary<long,string>();

            //TODO - move these to a Hub database table
            // Add the DB doc_data_ids that don't exist on the CDs
            badDocIDs = new Dictionary<long, string>();
            badDocIDs.Add(1077205, "Y");    // Disk 2
            badDocIDs.Add(1079693, "Y");    // Disk 2
            badDocIDs.Add(1081880, "Y");    // Disk 3
            badDocIDs.Add(1081881, "Y");    // Disk 3
            badDocIDs.Add(1083030, "Y");    // Disk 3
            badDocIDs.Add(1091009, "Y");    // Disk 3

            return badDocIDs;
        }
    }
}
