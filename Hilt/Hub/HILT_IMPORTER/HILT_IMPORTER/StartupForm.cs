﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HILT_IMPORTER {
    public partial class StartupForm : Form {
        public StartupForm() {
            InitializeComponent();
        }

        private void aulButton_Click(object sender, EventArgs e) {
            ((HiltImportMDIForm)this.ParentForm).showAULImportForm();
        }

        private void msslButton_Click(object sender, EventArgs e) {
            ((HiltImportMDIForm)this.ParentForm).showMSSLImportForm();
        }

        private void msdsButton_Click(object sender, EventArgs e) {
            ((HiltImportMDIForm)this.ParentForm).showMSDSImportForm();
        }
    }
}
