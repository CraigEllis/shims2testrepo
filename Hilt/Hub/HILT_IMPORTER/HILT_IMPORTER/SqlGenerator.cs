﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HILT_IMPORTER
{
    /* Method accepts a sql command as a string,
     * the file to export the sql command's results to,
     * and the StreamWriter of the file where the command 
     * is to be stored.
     */
    public class SqlGenerator
    {
        public void generate(String sql, String exportFile, StreamWriter writer)
        {
            writer.WriteLine(sql + " ># " + exportFile);
        }
    }
}
