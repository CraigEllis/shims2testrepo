﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILT_IMPORTER
{
    public interface ImportListener
    {
        void updateMessage(String str);

        void updateStatus(String str);

        void updateProgress(int progress);
    }
}
