﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HILT_IMPORTER
{
    public class IniGenerator
    {
        /* This class creates .ini files on the fly for our MSDS Export process.
         * It accepts some parameters and uses these parameters to create/update
         * the .ini file needed in the MSDS Export process.
         */
        public void generate(string user, string pass, string downloadInputFilename, string downloadOutputFilename, string downloadFormat, string securityAccess, string downloadType, StreamWriter writer)
        {
            writer.WriteLine("[Database]");
            writer.WriteLine("dbms=ODBC");
            writer.WriteLine("Database=CDROM");
            writer.WriteLine("CaseSensitivity=Upper");
            writer.WriteLine(" ");
            writer.WriteLine("[Log]");
            writer.WriteLine("Verbosity=ERROR");
            writer.WriteLine("");
            writer.WriteLine("[DOWNLOAD]");
            writer.WriteLine("UserID=" + user);
            writer.WriteLine("Password=" + pass);
            writer.WriteLine("PasswordEncrypted=N");
            writer.WriteLine("");
            writer.WriteLine("Download_Input_Filename= " + downloadInputFilename);
            writer.WriteLine("Download_Output_Filename= " + downloadOutputFilename);
            writer.WriteLine("Download_Format=" + downloadFormat);
            writer.WriteLine("Security_Access=" + securityAccess);
            writer.WriteLine("Download_Type=" + downloadType);
            writer.WriteLine("[VERSION]");
            writer.WriteLine("CreationDate=8/13/2009");
            writer.WriteLine("[APPSIZE]");
            writer.WriteLine("ApplicatonSize=2192751812");

        }
    }
}