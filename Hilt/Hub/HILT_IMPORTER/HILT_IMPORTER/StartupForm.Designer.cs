﻿namespace HILT_IMPORTER {
    partial class StartupForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartupForm));
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.importerLabel = new System.Windows.Forms.Label();
            this.introLabel = new System.Windows.Forms.Label();
            this.aulButton = new System.Windows.Forms.Button();
            this.msslButton = new System.Windows.Forms.Button();
            this.msdsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
            this.logoPictureBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.InitialImage")));
            this.logoPictureBox.Location = new System.Drawing.Point(24, 24);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(279, 89);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.logoPictureBox.TabIndex = 0;
            this.logoPictureBox.TabStop = false;
            // 
            // importerLabel
            // 
            this.importerLabel.AutoSize = true;
            this.importerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importerLabel.Location = new System.Drawing.Point(318, 44);
            this.importerLabel.Name = "importerLabel";
            this.importerLabel.Size = new System.Drawing.Size(211, 55);
            this.importerLabel.TabIndex = 1;
            this.importerLabel.Text = "Importer";
            // 
            // introLabel
            // 
            this.introLabel.AutoSize = true;
            this.introLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.introLabel.Location = new System.Drawing.Point(21, 139);
            this.introLabel.Name = "introLabel";
            this.introLabel.Size = new System.Drawing.Size(239, 20);
            this.introLabel.TabIndex = 2;
            this.introLabel.Text = "Select the type of data to import:";
            // 
            // aulButton
            // 
            this.aulButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aulButton.Location = new System.Drawing.Point(64, 180);
            this.aulButton.Name = "aulButton";
            this.aulButton.Size = new System.Drawing.Size(419, 37);
            this.aulButton.TabIndex = 3;
            this.aulButton.Text = "Hazardous Materials Control Lists (SHML, SMCL)";
            this.aulButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.aulButton.UseVisualStyleBackColor = true;
            this.aulButton.Click += new System.EventHandler(this.aulButton_Click);
            // 
            // msslButton
            // 
            this.msslButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msslButton.Location = new System.Drawing.Point(64, 233);
            this.msslButton.Name = "msslButton";
            this.msslButton.Size = new System.Drawing.Size(419, 37);
            this.msslButton.TabIndex = 4;
            this.msslButton.Text = "Inventory Lists (MSSL)";
            this.msslButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.msslButton.UseVisualStyleBackColor = true;
            this.msslButton.Click += new System.EventHandler(this.msslButton_Click);
            // 
            // msdsButton
            // 
            this.msdsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msdsButton.Location = new System.Drawing.Point(64, 290);
            this.msdsButton.Name = "msdsButton";
            this.msdsButton.Size = new System.Drawing.Size(419, 37);
            this.msdsButton.TabIndex = 5;
            this.msdsButton.Text = "HMIRS MSDS Data";
            this.msdsButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.msdsButton.UseVisualStyleBackColor = true;
            this.msdsButton.Click += new System.EventHandler(this.msdsButton_Click);
            // 
            // StartupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 384);
            this.Controls.Add(this.msdsButton);
            this.Controls.Add(this.msslButton);
            this.Controls.Add(this.aulButton);
            this.Controls.Add(this.introLabel);
            this.Controls.Add(this.importerLabel);
            this.Controls.Add(this.logoPictureBox);
            this.Name = "StartupForm";
            this.Text = "Start";
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.Label importerLabel;
        private System.Windows.Forms.Label introLabel;
        private System.Windows.Forms.Button aulButton;
        private System.Windows.Forms.Button msslButton;
        private System.Windows.Forms.Button msdsButton;
    }
}