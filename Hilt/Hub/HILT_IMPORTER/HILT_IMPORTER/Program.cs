﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HILT_IMPORTER
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try {
                Application.Run(new HiltImportMDIForm());
            }
            catch (Exception ex) {
                // Couldn't logon - or some other funky error
                MessageBox.Show("Could not start HILT Importer\n\n" + ex.Message,
                    "HILT Importer");
            }
        }
    }
}
