﻿Imports System.Text

<TestClass()>
Public Class UnitTest1

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    ' You can use the following additional attributes as you write your tests:
    '
    ' Use ClassInitialize to run code before running the first test in the class
    ' <ClassInitialize()> Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    ' End Sub
    '
    ' Use ClassCleanup to run code after all tests in a class have run
    ' <ClassCleanup()> Public Shared Sub MyClassCleanup()
    ' End Sub
    '
    ' Use TestInitialize to run code before running each test
    ' <TestInitialize()> Public Sub MyTestInitialize()
    ' End Sub
    '
    ' Use TestCleanup to run code after each test has run
    ' <TestCleanup()> Public Sub MyTestCleanup()
    ' End Sub
    '
#End Region

    <TestMethod()>
    Public Sub HMIRSBConnectTest()
        // Connect to the HMIRS database using ODBC
        System.Data.Odbc.OdbcConnection conn =
            new System.Data.Odbc.OdbcConnection ();
        // TODO: Modify the connection string and include any
        // additional required properties for your database.
        conn.ConnectionString = "FIL=MS Access;DSN=valid data source name";
        Try
        {
            conn.Open();
            // Process data here.
        }
        catch (Exception ex)
        {
            MessageBox.Show("Failed to connect to data source");
        }
        Finally
        {
            conn.Close();
        }




    End Sub

End Class
