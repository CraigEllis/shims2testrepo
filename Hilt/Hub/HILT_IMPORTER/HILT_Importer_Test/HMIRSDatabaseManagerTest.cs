﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Odbc;
using System.Windows.Forms;
using System.Diagnostics;
using HILT_IMPORTER;
using HILT_IMPORTER.DataObjects;

namespace HILT_Importer_Test {
    [TestClass]
    public class HMIRSDatabaseManagerTest {
        private string anywhereConnString = 
                "DRIVER=Adaptive Server Anywhere 8.0;"
                + "UID=dba;"
                + "PWD=sql;"
                + "Description=HMIRS CD DB v8.0;"
                + "Start=C:\\CD Install\\Disk1\\RTENG8.exe -c8m;"
                + "DatabaseFile=C:\\CD Install\\Disk1\\CDROM.CDB;"
                + "AutoStop=Yes;";

        [TestMethod]
        public void HMIRSConnectTest() {
            int count = 0;

            // Connect to the HMIRS DB using ODBC
            System.Data.Odbc.OdbcConnection conn =
                new System.Data.Odbc.OdbcConnection();
            conn.ConnectionString = anywhereConnString;
            try {
                conn.Open();

                // Process data here.
                String sql = "SELECT COUNT(*) FROM msdsonline_dbo.Company";
                OdbcCommand cmd = new OdbcCommand(sql, conn);
                count = (Int32)cmd.ExecuteScalar();
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                count = -1;
            }
            finally {
                conn.Close();
            }

            Assert.IsTrue(count > 0, "Count = " + count.ToString());
        }

        [TestMethod]
        public void getTableCountTest() {
            int count = 0;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                count = mgr.getTableCount("DOCUMENT_INDEX");
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                count = -1;
            }

            Assert.IsTrue(count > 0, "Count = " + count.ToString());
        }

        [TestMethod]
        public void getDocumentCountTest() {
            int count = 0;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                count = mgr.getDocumentCount();
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                count = -1;
            }

            Assert.IsTrue(count > 0, "Count = " + count.ToString());
        }

        [TestMethod]
        public void getNewRevisionsCountTest() {
            int count = 0;
            DateTime lastUpdate = new DateTime(2011, 1, 31);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                count = mgr.getNewRevisionCount(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                count = -1;
            }

            Assert.IsTrue(count > 0, "Count = " + count.ToString());
        }

        [TestMethod]
        public void getRevisionDocTypeCountsTest() {
            int[] counts = { };
            DateTime lastUpdate = new DateTime(2011, 1, 31);

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                counts = mgr.getRevisionDocTypeCounts(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                counts = new int[0];
            }

            Assert.IsTrue(counts.Length > 0);
            Assert.IsTrue(counts[0] > 0);
        }

        [TestMethod]
        public void getSupportDataRevisionCountsTest() {
            int[] counts = { };
            DateTime lastUpdate = new DateTime(2011, 1, 1);

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                counts = mgr.getSupportDataRevisionCounts(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                counts = new int[0];
            }

            Assert.IsTrue(counts.Length > 0);
            Assert.IsTrue(counts[0] > 0);
        }

        [TestMethod]
        public void getNewMSDSListTest() {
            List<string> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 31);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getNewMSDSList(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<string>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getDocIDsForSerialNumTest() {
            List<HMIRSDocumentRecord> list = null;
            string testSerialNum = "CZQKS"; // ZZNPK, ZZNWQ

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getDocIDsForSerialNum(testSerialNum);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<HMIRSDocumentRecord>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getUpdatedContractorInfoTest() {
            List<MSDSContractorInfo> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getUpdatedContractorInfo(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<MSDSContractorInfo>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getUpdatedAFJMRecordsTest() {
            List<MSDS_AFJM_PSN> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getUpdatedAFJMRecords(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<MSDS_AFJM_PSN>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getUpdatedDOTRecordsTest() {
            List<MSDS_DOT_PSN> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getUpdatedDOTRecords(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<MSDS_DOT_PSN>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getUpdatedIATARecordsTest() {
            List<MSDS_IATA_PSN> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getUpdatedIATARecords(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<MSDS_IATA_PSN>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getUpdatedIMORecordsTest() {
            List<MSDS_IMO_PSN> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getUpdatedIMORecords(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<MSDS_IMO_PSN>();
            }

            Assert.IsTrue(list.Count > 0, "Count = " + list.Count.ToString());
        }

        [TestMethod]
        public void getMSDSMasterRecordTest() {
            MSDSRecord item = null;
            string msdsSerNo = "ZZNWQ";
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                item = mgr.getMSDSMasterRecord(msdsSerNo, lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                item = new MSDSRecord();
            }

            Assert.AreEqual(item.MSDSSERNO, msdsSerNo);
        }

        [TestMethod]
        public void getMSDSContractorInfoTest() {
            MSDSRecord item = null;
            string msdsSerNo = "ZZNWQ";
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                item = mgr.getMSDSMasterRecord(msdsSerNo, lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                item = new MSDSRecord();
            }

            Assert.IsTrue(item.ContractorList.Count > 0);
        }

        [TestMethod]
        public void getMSDSDocTypesTest() {
            MSDSRecord item = null;
            string msdsSerNo = "ZZNWQ";
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                item = mgr.getMSDSMasterRecord(msdsSerNo, lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                item = new MSDSRecord();
            }

            Assert.IsTrue(item.DocumentTypes.product_sheet_filename.Length > 0);
        }

        [TestMethod]
        public void getMSDSLabelInfoRecordTest() {
            MSDSLabelInfo item = null;
            long index_id = 695180;
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                OdbcConnection conn = mgr.getConnection();

                // Process data here.
                item = mgr.getMSDSLabelInfoRecord(index_id, conn);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                item = new MSDSLabelInfo();
            }

            Assert.IsTrue(item.COMPANY_CAGE_RP.Length > 0);
        }

        [TestMethod]
        public void getMSDSRadiologicalTest() {
            MSDSRecord item = null;
            string msdsSerNo = "ZZPCZ";
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                item = mgr.getMSDSMasterRecord(msdsSerNo, lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                item = new MSDSRecord();
            }

            Assert.IsTrue(item.RadiologicalInfoList.Count > 0);
        }

        [TestMethod]
        public void getDocDataMappingTest() {
            List<HMIRSDocumentRecord> list = null;
            int diskNum = 2;
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getDocDataMapping(diskNum);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<HMIRSDocumentRecord>();
            }

            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void getNewDocumentListTest() {
            List<HMIRSDocumentRecord> list = null;
            DateTime lastUpdate = new DateTime(2011, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getNewDocumentList(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<HMIRSDocumentRecord>();
            }

            Assert.IsTrue(list.Count > 0);
        }


        [TestMethod]
        public void getMSDSDoc_IDMappingTest() {
            Dictionary<string, string> list = null;
            DateTime lastUpdate = new DateTime(2000, 1, 1);
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getMSDSDoc_IDMapping();
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new Dictionary<string, string>();
            }

            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void getCDNumsToExtractTest() {
            List<int> list = null;
            DateTime lastUpdate = new DateTime(2011, 8, 1); // should find disks 4 & 5
            //DateTime lastUpdate = DateTime.MinValue;

            try {
                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                // Process data here.
                list = mgr.getCDNumsToExtract(lastUpdate);
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to connect to data source\n" + ex.Message);
                list = new List<int>();
            }

            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void buildCompositeRecordTest() {
            string sLow = "L: ";
            string sHigh = "; H: ";

            double num = 0;
            string sLowVal = "test Low";
            if (double.TryParse(sLowVal, out num))
                sLowVal = String.Format("{0:N2}", num);
            string sHighVal = "test High";
            if (double.TryParse(sHighVal, out num)) 
                sHighVal = String.Format("{0:N2}", num);

            string testText = sLow + sLowVal + sHigh + sHighVal;
                        
            sLowVal = "1.333333";
            if (double.TryParse(sLowVal, out num)) 
                sLowVal = String.Format("{0:N2}", num);
            sHighVal = "66.6666";
            if (double.TryParse(sHighVal, out num))
                sHighVal = String.Format("{0:N2}", num);

            string testNumeric = sLow + sLowVal + sHigh + sHighVal;

            Assert.IsTrue(testText.Length == testNumeric.Length, 
                testText + " - " + testNumeric);
        }


    }
}
