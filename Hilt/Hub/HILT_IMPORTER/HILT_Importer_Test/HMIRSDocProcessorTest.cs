﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HILT_IMPORTER.ImportExport;
using HILT_IMPORTER;
using HILT_IMPORTER.DataObjects;

namespace HILT_Importer_Test {
    [TestClass]
    public class HMIRSDocProcessorTest {
        [TestMethod]
        public void countDiskDocumentsTest() {
            string testZipFile = "C:\\CD Install\\DiskXX\\CDROM-DiskXXTar1.dat";
            int diskNumber = 3;
            int result = 0;

            long totalTicks = Environment.TickCount;

            while (diskNumber <= 5) {
                string zipFile = testZipFile.Replace("XX", diskNumber.ToString());

                long ticks = Environment.TickCount;
                HMIRSDocProcessor processor = new HMIRSDocProcessor(null, null);

                result = processor.countDiskDocuments(zipFile);

                System.Diagnostics.Debug.WriteLine("countDiskDocumentsTest disk #: "
                    + diskNumber.ToString() + " Docs = " + result.ToString()
                    + "\tTiming: " + (((double)(Environment.TickCount - ticks)) / 1000).ToString());

                diskNumber++;
            }

            System.Diagnostics.Debug.WriteLine("countDiskDocumentsTest timing: "
                + (((double)(Environment.TickCount - totalTicks)) / 1000).ToString());

            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void unpackDocumentFileTest() {
            string testZipFile = "C:\\CD Install\\DiskXX\\CDROM-DiskXXTar1.dat";
            int diskNumber = 1;
            int nTotal = 0;

            long totalTicks = Environment.TickCount;

            while (diskNumber <= 5) {
                string zipFile = testZipFile.Replace("XX", diskNumber.ToString());

                long ticks = Environment.TickCount;

                HMIRSDatabaseManager mgr = new HMIRSDatabaseManager();

                List<HMIRSDocumentRecord> docData = mgr.getDocDataMapping(diskNumber);
                //System.Diagnostics.Debug.WriteLine("**** Estimated docs on disk: " + docData.Count.ToString());

                HMIRSDocProcessor processor = new HMIRSDocProcessor(null, null);

                int result = processor.unpackDocumentFiles(zipFile,
                    "C:\\test\\test\\", docData);
                nTotal += result;

                System.Diagnostics.Debug.WriteLine("\tDisk " + diskNumber.ToString() + ": "
                + nTotal.ToString() + "\tTiming: "
                + (((double)(Environment.TickCount - ticks)) / 1000).ToString());

                diskNumber++;
            }

            System.Diagnostics.Debug.WriteLine("unpackDocumentFileTest files extracted: "
            + nTotal.ToString() + " Timing: " 
            + (((double)(Environment.TickCount - totalTicks)) / 1000).ToString());

            Assert.IsTrue(nTotal > 0);
        }

        [TestMethod]
        public void extractZipFileTest()
        {
            string testZipFile = "C:\\CD Install\\Disk5\\CDROM-Disk5Tar1.dat";

            long ticks = Environment.TickCount;

            HMIRSDocProcessor processor = new HMIRSDocProcessor(null, null);

            bool result = processor.extractZipFile(testZipFile, 0, "C:\\temp\\HMIRS_1.pdf");

            Assert.IsTrue(result);

            result = processor.extractZipFile(testZipFile, 276153, "C:\\temp\\HMIRS_2.pdf");

            System.Diagnostics.Debug.WriteLine("extractZipFileTest timing: "
                + (((double)(Environment.TickCount - ticks)) / 1000).ToString());

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void extractZipHTMLFileTest()
        {
            string testZipFile = "C:\\CD Install\\Disk1\\CDROM-Disk1Tar1.dat";

            long ticks = Environment.TickCount;

            HMIRSDocProcessor processor = new HMIRSDocProcessor(null, null);

            bool result = processor.extractZipFile(testZipFile, 0, "C:\\temp\\HMIRS_1.html");

            Assert.IsTrue(result);

            result = processor.extractZipFile(testZipFile, 276153, "C:\\temp\\HMIRS_2.html");

            System.Diagnostics.Debug.WriteLine("extractZipFileTest timing: "
                + (((double)(Environment.TickCount - ticks)) / 1000).ToString());

            Assert.IsTrue(result);
        }
    }
}
