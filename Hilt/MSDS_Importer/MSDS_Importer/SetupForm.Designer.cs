﻿namespace MSDS_Importer
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkLocalMachine = new System.Windows.Forms.CheckBox();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkPassword = new System.Windows.Forms.CheckBox();
            this.txtPasswordBox = new System.Windows.Forms.TextBox();
            this.txtUsernameBox = new System.Windows.Forms.TextBox();
            this.txtPortBox = new System.Windows.Forms.TextBox();
            this.txtServerBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtSHIMSFilePath = new System.Windows.Forms.TextBox();
            this.shimsGroupBox = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnBrowseSHIMFiles = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2.SuspendLayout();
            this.shimsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkLocalMachine);
            this.groupBox2.Controls.Add(this.btnTestConnection);
            this.groupBox2.Controls.Add(this.txtDatabase);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.chkPassword);
            this.groupBox2.Controls.Add(this.txtPasswordBox);
            this.groupBox2.Controls.Add(this.txtUsernameBox);
            this.groupBox2.Controls.Add(this.txtPortBox);
            this.groupBox2.Controls.Add(this.txtServerBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 96);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 175);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "HILT HUB Database Settings";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // chkLocalMachine
            // 
            this.chkLocalMachine.AutoSize = true;
            this.chkLocalMachine.Location = new System.Drawing.Point(221, 50);
            this.chkLocalMachine.Name = "chkLocalMachine";
            this.chkLocalMachine.Size = new System.Drawing.Size(77, 17);
            this.chkLocalMachine.TabIndex = 4;
            this.chkLocalMachine.Text = "Local Host";
            this.chkLocalMachine.UseVisualStyleBackColor = true;
            this.chkLocalMachine.CheckedChanged += new System.EventHandler(this.chkLocalMachine_CheckedChanged);
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestConnection.Location = new System.Drawing.Point(383, 25);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(100, 23);
            this.btnTestConnection.TabIndex = 13;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(110, 74);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(251, 20);
            this.txtDatabase.TabIndex = 6;
            this.txtDatabase.Text = "hilt_hub";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Database:";
            // 
            // chkPassword
            // 
            this.chkPassword.AutoSize = true;
            this.chkPassword.Location = new System.Drawing.Point(110, 146);
            this.chkPassword.Name = "chkPassword";
            this.chkPassword.Size = new System.Drawing.Size(102, 17);
            this.chkPassword.TabIndex = 11;
            this.chkPassword.Text = "Show Password";
            this.chkPassword.UseVisualStyleBackColor = true;
            this.chkPassword.CheckedChanged += new System.EventHandler(this.chkPassword_CheckedChanged);
            // 
            // txtPasswordBox
            // 
            this.txtPasswordBox.Location = new System.Drawing.Point(110, 123);
            this.txtPasswordBox.Name = "txtPasswordBox";
            this.txtPasswordBox.Size = new System.Drawing.Size(251, 20);
            this.txtPasswordBox.TabIndex = 10;
            this.txtPasswordBox.Text = "ashore";
            this.txtPasswordBox.UseSystemPasswordChar = true;
            // 
            // txtUsernameBox
            // 
            this.txtUsernameBox.Location = new System.Drawing.Point(110, 100);
            this.txtUsernameBox.Name = "txtUsernameBox";
            this.txtUsernameBox.Size = new System.Drawing.Size(251, 20);
            this.txtUsernameBox.TabIndex = 8;
            this.txtUsernameBox.Text = "ashore";
            // 
            // txtPortBox
            // 
            this.txtPortBox.Location = new System.Drawing.Point(110, 48);
            this.txtPortBox.Name = "txtPortBox";
            this.txtPortBox.Size = new System.Drawing.Size(89, 20);
            this.txtPortBox.TabIndex = 3;
            this.txtPortBox.Text = "1434";
            // 
            // txtServerBox
            // 
            this.txtServerBox.Location = new System.Drawing.Point(110, 25);
            this.txtServerBox.Name = "txtServerBox";
            this.txtServerBox.Size = new System.Drawing.Size(251, 20);
            this.txtServerBox.TabIndex = 1;
            this.txtServerBox.Text = ".\\SQLEXPRESS2008";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Username:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Port:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Server Name:";
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.Location = new System.Drawing.Point(302, 282);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(87, 23);
            this.saveBtn.TabIndex = 3;
            this.saveBtn.Text = "&Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.Location = new System.Drawing.Point(395, 282);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(100, 23);
            this.cancelBtn.TabIndex = 4;
            this.cancelBtn.Text = "&Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtSHIMSFilePath
            // 
            this.txtSHIMSFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSHIMSFilePath.Location = new System.Drawing.Point(124, 22);
            this.txtSHIMSFilePath.Name = "txtSHIMSFilePath";
            this.txtSHIMSFilePath.Size = new System.Drawing.Size(278, 20);
            this.txtSHIMSFilePath.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtSHIMSFilePath, "The folder where the AUL (SMCL, SHML) files to import are located");
            // 
            // shimsGroupBox
            // 
            this.shimsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shimsGroupBox.Controls.Add(this.label12);
            this.shimsGroupBox.Controls.Add(this.txtSHIMSFilePath);
            this.shimsGroupBox.Controls.Add(this.btnBrowseSHIMFiles);
            this.shimsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.shimsGroupBox.Name = "shimsGroupBox";
            this.shimsGroupBox.Size = new System.Drawing.Size(494, 67);
            this.shimsGroupBox.TabIndex = 1;
            this.shimsGroupBox.TabStop = false;
            this.shimsGroupBox.Text = "SHIMS Settings";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "SHIMS Database File:";
            // 
            // btnBrowseSHIMFiles
            // 
            this.btnBrowseSHIMFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseSHIMFiles.Location = new System.Drawing.Point(408, 20);
            this.btnBrowseSHIMFiles.Name = "btnBrowseSHIMFiles";
            this.btnBrowseSHIMFiles.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseSHIMFiles.TabIndex = 2;
            this.btnBrowseSHIMFiles.Text = "Browse...";
            this.btnBrowseSHIMFiles.UseVisualStyleBackColor = true;
            this.btnBrowseSHIMFiles.Click += new System.EventHandler(this.btnBrowseSHIMSFiles_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 314);
            this.Controls.Add(this.shimsGroupBox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.Text = "HILT Import Setup";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.shimsGroupBox.ResumeLayout(false);
            this.shimsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPasswordBox;
        private System.Windows.Forms.TextBox txtUsernameBox;
        private System.Windows.Forms.TextBox txtPortBox;
        private System.Windows.Forms.TextBox txtServerBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkPassword;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox shimsGroupBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSHIMSFilePath;
        private System.Windows.Forms.Button btnBrowseSHIMFiles;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.CheckBox chkLocalMachine;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

