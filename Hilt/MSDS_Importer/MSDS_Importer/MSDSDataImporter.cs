﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;
using MSDS_Importer.DataObjects;
using System.IO;

namespace MSDS_Importer {
    class MSDSDataImporter {
        public enum ImportTarget {
            Hilt_v1,
            Hilt_v2,
            Shims_v1
        };

        public enum MSDSSheets {
            ValueAdded,
            Contracts,
            Ingredients,
            Radiological
        };

        // Column Definitions for list records
        const int CT_CONTRACT = 0;
        const int CT_PURCHASEORDER = 1;
        const int CT_CAGE = 2;
        const int CT_COMPANY_NAME = 3;
        const int CT_PO_BOX = 4;
        const int CT_ADDRESS = 5;
        const int CT_CITY = 6;
        const int CT_STATE = 7;
        const int CT_ZIP_CODE = 8;
        const int CT_COUNTRY = 9;
        const int CT_PHONE = 10;

        private String m_ErrorMsg = "";
        public string ErrorMsg {
            get {
                return m_ErrorMsg;
            }
            set {
                m_ErrorMsg = value;
            }
        }

        private List<String> m_msdsSerNos = null;
        private Dictionary<String, List<string>> m_msdsDocuments = null;

        #region Preview
        public static Dictionary<string, int> getMSDSStats(string msdsFolderName) {
            Dictionary<string, int> counts = new Dictionary<string, int>();
            Dictionary<string, int> msdsSerNo = new Dictionary<string, int>();

            // Initialize the dictionary
            counts.Add("MSDSSheets", 0);
            counts.Add("DocumentLinks", 0);

            // Open the MSDS List.xls spreadsheet and process the MSDS Serial # rows
            ExcelParser parser = new ExcelParser();
            ExcelFile fileData = parser.parseExcel(msdsFolderName + "\\MSDS List.xls", 1);

            foreach (string[] row in fileData.DataRows) {
                counts["DocumentLinks"]++;

                if (!msdsSerNo.ContainsKey(row[0])) {
                    // Add the serial no to the hash & increment the count
                    msdsSerNo.Add(row[0], 1);
                    counts["MSDSSheets"]++;
                }
            }

            // Get the file stats
            Dictionary<string, int> fileStats = getFolderStats(msdsFolderName);
            counts.Add("ValueAddedSheets", fileStats["ValueAddedSheets"]);
            counts.Add("Documents", fileStats["Documents"]);

            return counts;
        }

        private static Dictionary<string, int> getFolderStats(string msdsFolderName) {
            Dictionary<string, int> counts = new Dictionary<string, int>();

            // Get the number of MSDS spreadsheets
            DirectoryInfo folder = new DirectoryInfo(msdsFolderName + "\\ValueAdded");

            counts.Add("ValueAddedSheets", folder.GetFiles().Length);

            // Get the number of documents
            folder = new DirectoryInfo(msdsFolderName + "\\Documents");

            counts.Add("Documents", folder.GetFiles().Length);

            return counts;
        }
        #endregion

        public bool processImportLocal(string msdsFolderName, ImportTarget target,
                string fileNameOrConnString, BackgroundWorker worker) {
            bool result = true;

            // Get the list of MSDS Serial #s from the main sheet
            int count = getMSDSSerNoList(msdsFolderName, worker);

            // Make sure we read the file
            if (count == 0 || m_ErrorMsg.Length > 0) {
                // Write an error message via the background worker
                m_ErrorMsg = "ERROR - No MSDS Serial Numbers were found in "
                    + msdsFolderName + "\\MSDS List.xls"
                    + "\n\tCause: " + m_ErrorMsg;
                if (worker != null)
                    worker.ReportProgress(0, m_ErrorMsg);

                return false;
            }
            else {
            }

            // Get the value added data
            List<MSDSRecord> records = getValueAddedData(msdsFolderName, worker);

            // Loop through the list and load the individual MSDS data into the database
            foreach (string msdsSerNo in m_msdsSerNos) {

            }

            return result;
        }

        public bool processImportWeb(string msdsFolderName, ImportTarget target,
                string fileNameOrConnString) {
            bool result = true;

            return result;
        }

        private int getMSDSSerNoList(string msdsFolderName,
                BackgroundWorker worker) {
            m_msdsSerNos = new List<string>();
            m_msdsDocuments = new Dictionary<string, List<string>>();

            m_ErrorMsg = "";

            try {
                // Open the MSDS List.xls spreadsheet and process the MSDS Serial # rows
                ExcelParser parser = new ExcelParser();
                ExcelFile fileData = parser.parseExcel(msdsFolderName + "\\MSDS List.xls", 1);

                foreach (string[] row in fileData.DataRows) {
                    // get the document name
                    string docName = getDocumentName(row[2]);
                    
                    if (!m_msdsSerNos.Contains(row[0])) {
                        List<string> documents = new List<string>();

                        // Add the serial no to the hash
                        m_msdsSerNos.Add(row[0]);

                        if (docName != null)
                            documents.Add(docName);

                        m_msdsDocuments.Add(row[0], documents);
                    }
                    else {
                        // Add the document to the existing item
                        if (docName != null)
                            m_msdsDocuments[row[0]].Add(docName);
                    }
                }
            }
            catch (Exception ex) {
                m_ErrorMsg = ex.Message;
                m_msdsSerNos.Clear();
            }

            return m_msdsSerNos.Count;
        }

        private string getDocumentName(string linkName) {
            string result = null;
            Regex pattern = new Regex("LINK..(.+)?.,");

            Match m = pattern.Match(linkName);

            if (m != null && m.Groups.Count > 1 && m.Groups[1].Value.Length > 0)
                result = m.Groups[1].Value;

            return result;
        }

        private List<MSDSRecord> getValueAddedData(string msdsFolderName,
                BackgroundWorker worker) {
                    List<MSDSRecord> msdsRecords = new List<MSDSRecord>();
            m_ErrorMsg = "";

            try {
                // Open the MSDS record's spreadsheet and process the rows
                ExcelParser parser = new ExcelParser();

                foreach (string msdsSerNo in m_msdsSerNos) {
                    ExcelFile fileData = parser.parseExcel(msdsFolderName + "\\ValueAdded\\"
                        + msdsSerNo + ".xls", (int) MSDSSheets.ValueAdded);

                    MSDSRecord record = processMSDSDataToRecord(msdsSerNo, fileData);

                    // Process contracts off the second tab
                    processContracts(msdsFolderName, msdsSerNo, parser);

                    // TODO Process ingredients off the third tab

                    // TODO Process radiological off the fourth tab

                    // Add the record to the list
                    msdsRecords.Add(record);
                }
            }
            catch (Exception ex) {
                m_ErrorMsg = ex.Message;
                m_msdsSerNos.Clear();
            }

            return msdsRecords;
        }

        private MSDSRecord processMSDSDataToRecord(string msdsSerNo, ExcelFile fileData) {
            MSDSRecord record = new MSDSRecord();

            // Run through the records lloking for the section headers
            int nCount = 0;
            for (int row = 0; row < fileData.DataRows.Count; row++) {
                string[] rowData = fileData.DataRows[row];

                // Check for a section header
                if (rowData[0].ToUpper().Equals("HEADER DATA")) {
                    row = processHeaderData(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("ITEM DESCRIPTION")) {
                    row = processItemDescription(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("DISPOSAL")) {
                    row = processLabelData(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("LABEL")) {
                    row = processLabelData(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("PHYSICAL/CHEMICAL")) {
                    row = processPhysChem(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("TRANSPORTATION")) {
                    row = processTransportation(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("DOT PSN")) {
                    row = processDotPsn(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("IATA PSN")) {
                    row = processIataPsn(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("IMO PSN")) {
                    row = processImoPsn(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("AFJM PSN")) {
                    row = processAfjmPsn(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("INGREDIENTS")) {
                    row = processIngredients(row, fileData, record);
                    continue;
                }
                if (rowData[0].ToUpper().Equals("RADIOLOGICAL")) {
                    row = processRadiologicInfo(row, fileData, record);
                }

            }

            return record;
        }

        private int processHeaderData(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // temp vars for parsing
            DateTime dtTemp;
            bool bTemp;

            // Increment the row count to move off the header row
            row++;

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("MSDSSERNO")) {
                    record.MSDSSERNO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CAGE")) {
                    record.CAGE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MANUFACTURER")) {
                    record.MANUFACTURER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PARTNO")) {
                    record.PARTNO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("FSC")) {
                    record.FSC = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NIIN")) {
                    record.MSDSSERNO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("FILE NAME")) {
                    record.file_name = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MANUALLY ENTERED")) {
                    bool.TryParse(rowData[2], out bTemp);
                    record.manually_entered = bTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ARTICLE IND")) {
                    record.ARTICLE_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DESCRIPTION")) {
                    record.DESCRIPTION = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EMERGENCY TEL")) {
                    record.EMERGENCY_TEL = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("END COMP IND")) {
                    record.END_COMP_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("END ITEM IND")) {
                    record.END_ITEM_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("KIT IND")) {
                    record.KIT_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("KIT PART IND")) {
                    record.KIT_PART_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MANUFACTURER MSDS NO")) {
                    record.MANUFACTURER_MSDS_NO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MIXTURE IND")) {
                    record.MIXTURE_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRODUCT IDENTITY")) {
                    record.PRODUCT_IDENTITY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRODUCT LOAD DATE")) {
                    DateTime.TryParse(rowData[2], out dtTemp);
                    record.PRODUCT_LOAD_DATE = dtTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRODUCT RECORD STATUS")) {
                    record.PRODUCT_RECORD_STATUS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRODUCT REVISION NO")) {
                    record.PRODUCT_REVISION_NO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROPRIETARY IND")) {
                    record.PROPRIETARY_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PUBLISHED IND")) {
                    record.PUBLISHED_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PURCHASED PROD IND")) {
                    record.PURCHASED_PROD_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PURE IND")) {
                    record.PURE_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RADIOACTIVE IND")) {
                    record.RADIOACTIVE_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SERVICE AGENCY CODE")) {
                    record.SERVICE_AGENCY_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TRADE NAME")) {
                    record.TRADE_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TRADE SECRET IND")) {
                    record.TRADE_SECRET_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRODUCT IND")) {
                    record.PRODUCT_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRODUCT LANGUAGE")) {
                    record.PRODUCT_LANGUAGE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DATA SOURCE CD")) {
                    record.data_source_cd = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CREATED")) {
                    DateTime.TryParse(rowData[2], out dtTemp);
                    record.created = dtTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CHANGED")) {
                    DateTime.TryParse(rowData[2], out dtTemp);
                    record.changed = dtTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CREATED BY")) {
                    record.created_by = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CHANGED BY")) {
                    record.changed_by = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processDisposal(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.Disposal = new MSDSDisposal();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("DISPOSAL ADD INFO")) {
                    record.Disposal.DISPOSAL_ADD_INFO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA HAZ WASTE CODE")) {
                    record.Disposal.EPA_HAZ_WASTE_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA HAZ WASTE IND")) {
                    record.Disposal.EPA_HAZ_WASTE_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA HAZ WASTE NAME")) {
                    record.Disposal.EPA_HAZ_WASTE_NAME = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processItemDescription(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.ItemDescription = new MSDSItemDescription();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                // temp vars for parsing
                int nTemp;

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("ITEM MANAGER")) {
                    record.ItemDescription.ITEM_MANAGER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ITEM NAME")) {
                    record.ItemDescription.ITEM_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIFICATION NUMBER")) {
                    record.ItemDescription.SPECIFICATION_NUMBER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TYPE GRADE CLASS")) {
                    record.ItemDescription.TYPE_GRADE_CLASS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UNIT OF ISSUE")) {
                    record.ItemDescription.UNIT_OF_ISSUE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("QUANTITATIVE EXPRESSION")) {
                    record.ItemDescription.QUANTITATIVE_EXPRESSION = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UI CONTAINER QTY")) {
                    record.ItemDescription.UI_CONTAINER_QTY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TYPE OF CONTAINER")) {
                    record.ItemDescription.TYPE_OF_CONTAINER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("BATCH NUMBER")) {
                    record.ItemDescription.BATCH_NUMBER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LOT NUMBER")) {
                    record.ItemDescription.LOT_NUMBER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LOG FLIS NIIN VER")) {
                    record.ItemDescription.LOG_FLIS_NIIN_VER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LOG FSC")) {
                    int.TryParse(rowData[2], out nTemp);
                    record.ItemDescription.LOG_FSC = nTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NET UNIT WEIGHT")) {
                    record.ItemDescription.NET_UNIT_WEIGHT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SHELF LIFE CODE")) {
                    record.ItemDescription.SHELF_LIFE_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIAL EMP CODE")) {
                    record.ItemDescription.SPECIAL_EMP_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UN NA NUMBER")) {
                    record.ItemDescription.UN_NA_NUMBER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UPC GTIN")) {
                    record.ItemDescription.UPC_GTIN = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processLabelData(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.LabelInfo = new MSDSLabelInfo();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("COMPANY CAGE RP")) {
                    record.LabelInfo.COMPANY_CAGE_RP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("COMPANY NAME RP")) {
                    record.LabelInfo.COMPANY_NAME_RP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL EMERG PHONE")) {
                    record.LabelInfo.LABEL_EMERG_PHONE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL ITEM NAME")) {
                    record.LabelInfo.LABEL_ITEM_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL PROC YEAR")) {
                    record.LabelInfo.LABEL_PROC_YEAR = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL PROD IDENT")) {
                    record.LabelInfo.LABEL_PROD_IDENT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL PROD SERIALNO")) {
                    record.LabelInfo.LABEL_PROD_SERIALNO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL SIGNAL WORD CODE")) {
                    record.LabelInfo.LABEL_SIGNAL_WORD_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LABEL STOCK NO")) {
                    record.LabelInfo.LABEL_STOCK_NO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIFIC HAZARDS")) {
                    record.LabelInfo.SPECIFIC_HAZARDS = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processPhysChem(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.PhysChemical = new MSDSPhysChemical();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("VAPOR PRESS")) {
                    record.PhysChemical.VAPOR_PRESS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("VAPOR DENS")) {
                    record.PhysChemical.VAPOR_DENS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIFIC GRAV")) {
                    record.PhysChemical.SPECIFIC_GRAV = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("VOC POUNDS GALLON")) {
                    record.PhysChemical.VOC_POUNDS_GALLON = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("VOC GRAMS LITER")) {
                    record.PhysChemical.VOC_GRAMS_LITER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("VISCOSITY")) {
                    record.PhysChemical.VISCOSITY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EVAP RATE REF")) {
                    record.PhysChemical.EVAP_RATE_REF = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SOL IN WATER")) {
                    record.PhysChemical.SOL_IN_WATER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("APP ODOR")) {
                    record.PhysChemical.APP_ODOR = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PERCENT VOL VOLUME")) {
                    record.PhysChemical.PERCENT_VOL_VOLUME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("AUTOIGNITION TEMP")) {
                    record.PhysChemical.AUTOIGNITION_TEMP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CARCINOGEN IND")) {
                    record.PhysChemical.CARCINOGEN_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA ACUTE")) {
                    record.PhysChemical.EPA_ACUTE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA CHRONIC")) {
                    record.PhysChemical.EPA_CHRONIC = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA FIRE")) {
                    record.PhysChemical.EPA_FIRE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA PRESSURE")) {
                    record.PhysChemical.EPA_PRESSURE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA REACTIVITY")) {
                    record.PhysChemical.EPA_REACTIVITY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("FLASH PT TEMP")) {
                    record.PhysChemical.FLASH_PT_TEMP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NEUT AGENT")) {
                    record.PhysChemical.NEUT_AGENT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NFPA FLAMMABILITY")) {
                    record.PhysChemical.NFPA_FLAMMABILITY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NFPA HEALTH")) {
                    record.PhysChemical.NFPA_HEALTH = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NFPA REACTIVITY")) {
                    record.PhysChemical.NFPA_REACTIVITY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NFPA SPECIAL")) {
                    record.PhysChemical.NFPA_SPECIAL = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA CARCINOGENS")) {
                    record.PhysChemical.OSHA_CARCINOGENS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA COMB LIQUID")) {
                    record.PhysChemical.OSHA_COMB_LIQUID = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA COMP GAS")) {
                    record.PhysChemical.OSHA_COMP_GAS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA CORROSIVE")) {
                    record.PhysChemical.OSHA_CORROSIVE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA EXPLOSIVE")) {
                    record.PhysChemical.OSHA_EXPLOSIVE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA FLAMMABLE")) {
                    record.PhysChemical.OSHA_FLAMMABLE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA HIGH TOXIC")) {
                    record.PhysChemical.OSHA_HIGH_TOXIC = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA IRRITANT")) {
                    record.PhysChemical.OSHA_IRRITANT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA ORG PEROX")) {
                    record.PhysChemical.OSHA_ORG_PEROX = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA OTHERLONGTERM")) {
                    record.PhysChemical.OSHA_OTHERLONGTERM = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA OXIDIZER")) {
                    record.PhysChemical.OSHA_OXIDIZER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA PYRO")) {
                    record.PhysChemical.OSHA_PYRO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA SENSITIZER")) {
                    record.PhysChemical.OSHA_SENSITIZER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA TOXIC")) {
                    record.PhysChemical.OSHA_TOXIC = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA UNST REACT")) {
                    record.PhysChemical.OSHA_UNST_REACT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OTHER SHORT TERM")) {
                    record.PhysChemical.OTHER_SHORT_TERM = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PHYS STATE CODE")) {
                    record.PhysChemical.PHYS_STATE_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("VOL ORG COMP WT")) {
                    record.PhysChemical.VOL_ORG_COMP_WT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA WATER REACTIVE")) {
                    record.PhysChemical.OSHA_WATER_REACTIVE = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processTransportation(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // temp vars for parsing
            int nTemp;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.Transportation = new MSDSTransportation();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("AF MMAC CODE")) {
                    record.Transportation.AF_MMAC_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CERTIFICATE COE")) {
                    record.Transportation.CERTIFICATE_COE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("COMPETENT CAA")) {
                    record.Transportation.COMPETENT_CAA = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DOD ID CODE")) {
                    record.Transportation.DOD_ID_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DOT EXEMPTION NO")) {
                    record.Transportation.DOT_EXEMPTION_NO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DOT RQ IND")) {
                    record.Transportation.DOT_RQ_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EX NO")) {
                    record.Transportation.EX_NO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("FLASH PT TEMP")) {
                    int.TryParse(rowData[2], out nTemp);
                    record.Transportation.FLASH_PT_TEMP = nTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("HCC")) {
                    record.Transportation.HCC = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("HIGH EXPLOSIVE WT")) {
                    int.TryParse(rowData[2], out nTemp);
                    record.Transportation.HIGH_EXPLOSIVE_WT = nTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LTD QTY IND")) {
                    record.Transportation.LTD_QTY_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MAGNETIC IND")) {
                    record.Transportation.MAGNETIC_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MAGNETISM")) {
                    record.Transportation.MAGNETISM = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MARINE POLLUTANT IND")) {
                    record.Transportation.MARINE_POLLUTANT_IND = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NET EXP QTY DIST")) {
                    int.TryParse(rowData[2], out nTemp);
                    record.Transportation.NET_EXP_QTY_DIST = nTemp;
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NET EXP WEIGHT")) {
                    record.Transportation.NET_EXP_WEIGHT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NET PROPELLANT WT")) {
                    record.Transportation.NET_PROPELLANT_WT = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NOS TECHNICAL SHIPPING NAME")) {
                    record.Transportation.NOS_TECHNICAL_SHIPPING_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TRANSPORTATION ADDITIONAL DATA")) {
                    record.Transportation.TRANSPORTATION_ADDITIONAL_DATA = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processDotPsn(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.Dot_Psn = new MSDS_DOT_PSN();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("HAZARD CLASS DIV")) {
                    record.Dot_Psn.DOT_HAZARD_CLASS_DIV = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("HAZARD LABEL")) {
                    record.Dot_Psn.DOT_HAZARD_LABEL = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MAX CARGO")) {
                    record.Dot_Psn.DOT_MAX_CARGO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("MAX PASSENGER")) {
                    record.Dot_Psn.DOT_MAX_PASSENGER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK BULK")) {
                    record.Dot_Psn.DOT_PACK_BULK = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK EXCEPTIONS")) {
                    record.Dot_Psn.DOT_PACK_EXCEPTIONS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK NONBULK")) {
                    record.Dot_Psn.DOT_PACK_NONBULK = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP SHIP NAME")) {
                    record.Dot_Psn.DOT_PROP_SHIP_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP SHIP MODIFIER")) {
                    record.Dot_Psn.DOT_PROP_SHIP_MODIFIER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PSN CODE")) {
                    record.Dot_Psn.DOT_PSN_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIAL PROVISION")) {
                    record.Dot_Psn.DOT_SPECIAL_PROVISION = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SYMBOLS")) {
                    record.Dot_Psn.DOT_SYMBOLS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UN ID NUMBER")) {
                    record.Dot_Psn.DOT_UN_ID_NUMBER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("WATER OTHER REQ")) {
                    record.Dot_Psn.DOT_WATER_OTHER_REQ = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("WATER VESSEL STOW")) {
                    record.Dot_Psn.DOT_WATER_VESSEL_STOW = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK GROUP")) {
                    record.Dot_Psn.DOT_PACK_GROUP = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processIataPsn(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.Iata_Psn = new MSDS_IATA_PSN();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("CARGO_PACKING")) {
                    record.Iata_Psn.IATA_CARGO_PACKING = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("HAZARD_CLASS")) {
                    record.Iata_Psn.IATA_HAZARD_CLASS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("HAZARD_LABEL")) {
                    record.Iata_Psn.IATA_HAZARD_LABEL = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK_GROUP")) {
                    record.Iata_Psn.IATA_PACK_GROUP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PASS_AIR_PACK_LMT_INSTR")) {
                    record.Iata_Psn.IATA_PASS_AIR_PACK_LMT_INSTR = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PASS_AIR_PACK_LMT_PER_PKG")) {
                    record.Iata_Psn.IATA_PASS_AIR_PACK_LMT_PER_PKG = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PASS_AIR_PACK_NOTE")) {
                    record.Iata_Psn.IATA_PASS_AIR_PACK_NOTE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP_SHIP_NAME")) {
                    record.Iata_Psn.IATA_PROP_SHIP_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP_SHIP_MODIFIER")) {
                    record.Iata_Psn.IATA_PROP_SHIP_MODIFIER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CARGO_PACK_MAX_QTY")) {
                    record.Iata_Psn.IATA_CARGO_PACK_MAX_QTY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PSN_CODE")) {
                    record.Iata_Psn.IATA_PSN_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PASS_AIR_MAX_QTY")) {
                    record.Iata_Psn.IATA_PASS_AIR_MAX_QTY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIAL_PROV")) {
                    record.Iata_Psn.IATA_SPECIAL_PROV = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SUBSIDIARY_RISK")) {
                    record.Iata_Psn.IATA_SUBSIDIARY_RISK = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UN_ID_NUMBER")) {
                    record.Iata_Psn.IATA_UN_ID_NUMBER = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processImoPsn(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.Imo_Psn = new MSDS_IMO_PSN();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("EMS_NO")) {
                    record.Imo_Psn.IMO_EMS_NO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("HAZARD_CLASS")) {
                    record.Imo_Psn.IMO_HAZARD_CLASS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("IBC_INSTR")) {
                    record.Imo_Psn.IMO_IBC_INSTR = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("LIMITED_QTY")) {
                    record.Imo_Psn.IMO_LIMITED_QTY = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK_GROUP")) {
                    record.Imo_Psn.IMO_PACK_GROUP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK_INSTRUCTIONS")) {
                    record.Imo_Psn.IMO_PACK_INSTRUCTIONS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK_PROVISIONS")) {
                    record.Imo_Psn.IMO_PACK_PROVISIONS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP_SHIP_NAME")) {
                    record.Imo_Psn.IMO_PROP_SHIP_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP_SHIP_MODIFIER")) {
                    record.Imo_Psn.IMO_PROP_SHIP_MODIFIER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PSN_CODE")) {
                    record.Imo_Psn.IMO_PSN_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIAL_PROV")) {
                    record.Imo_Psn.IMO_SPECIAL_PROV = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("STOW_SEGR")) {
                    record.Imo_Psn.IMO_STOW_SEGR = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SUBSIDIARY_RISK")) {
                    record.Imo_Psn.IMO_SUBSIDIARY_RISK = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TANK_INSTR_IMO")) {
                    record.Imo_Psn.IMO_TANK_INSTR_IMO = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TANK_INSTR_PROV")) {
                    record.Imo_Psn.IMO_TANK_INSTR_PROV = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("TANK_INSTR_UN")) {
                    record.Imo_Psn.IMO_TANK_INSTR_UN = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UN_NUMBER")) {
                    record.Imo_Psn.IMO_UN_NUMBER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("IBC_PROVISIONS")) {
                    record.Imo_Psn.IMO_IBC_PROVISIONS = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private int processAfjmPsn(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Create the record object
            record.Afjm_Psn = new MSDS_AFJM_PSN();

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("HAZARD_CLASS")) {
                    record.Afjm_Psn.AFJM_HAZARD_CLASS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK_PARAGRAPH")) {
                    record.Afjm_Psn.AFJM_PACK_PARAGRAPH = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PACK_GROUP")) {
                    record.Afjm_Psn.AFJM_PACK_GROUP = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP_SHIP_NAME")) {
                    record.Afjm_Psn.AFJM_PROP_SHIP_NAME = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PROP_SHIP_MODIFIER")) {
                    record.Afjm_Psn.AFJM_PROP_SHIP_MODIFIER = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PSN_CODE")) {
                    record.Afjm_Psn.AFJM_PSN_CODE = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SPECIAL_PROV")) {
                    record.Afjm_Psn.AFJM_SPECIAL_PROV = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SUBSIDIARY_RISK")) {
                    record.Afjm_Psn.AFJM_SUBSIDIARY_RISK = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SYMBOLS")) {
                    record.Afjm_Psn.AFJM_SYMBOLS = rowData[2];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("UN_ID_NUMBER")) {
                    record.Afjm_Psn.AFJM_UN_ID_NUMBER = rowData[2];
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        #region MSDSRecord List items
        private List<MSDSContractorInfo> processContracts(string msdsFolderName, 
                string msdsSerNo, ExcelParser parser) {
            // Create the contracts list
            List<MSDSContractorInfo>  contractorList = new List<MSDSContractorInfo>();

            ExcelFile fileData = parser.parseExcel(msdsFolderName + "\\ValueAdded\\"
                + msdsSerNo + ".xls", (int)MSDSSheets.Contracts);

            // Go through the records until we hit a blank in the field name column
            foreach (string[] rowData in fileData.DataRows) {
                // If we have a blank CAGE then we're done
                if (rowData[CT_CAGE].Length == 0)
                    break;
                // Skip the header row
                else if (rowData[CT_CONTRACT].ToUpper().Equals("CONTRACT")
                        && rowData[CT_CAGE].ToUpper().Equals("CAGE"))
                    continue;
                // Real Data
                else 
                    contractorList.Add(populateContractListField(rowData));
            }

            // Back up one row in the main loop
            return contractorList;
        }

        private MSDSContractorInfo populateContractListField(string[] rowData) {
                MSDSContractorInfo record = new MSDSContractorInfo();
            record.CT_NUMBER = rowData[CT_CONTRACT];
            record.PURCHASE_ORDER_NO = rowData[CT_PURCHASEORDER];
            record.CT_CAGE = rowData[CT_CAGE];
            record.CT_COMPANY_NAME = rowData[CT_COMPANY_NAME];
            record.CT_PO_BOX = rowData[CT_PO_BOX];
            record.CT_ADDRESS_1 = rowData[CT_ADDRESS];
            record.CT_STATE = rowData[CT_STATE];
            record.CT_ZIP_CODE = rowData[CT_ZIP_CODE];
            record.CT_COUNTRY = rowData[CT_COUNTRY];
            record.CT_PHONE = rowData[CT_PHONE];

            return record;
        }

        // TODO redo using processContracts as go by
        private int processIngredients(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Get the number of ingredient records
            int count = fileData.DataRows[row].Length - 2;
            record.IngredientsList = new List<MSDSIngredient>(count);

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("CAS")) {
                    populateIngredientListField(record, rowData, "CAS");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("INGREDIENT_NAME")) {
                    populateIngredientListField(record, rowData, "INGREDIENT_NAME");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RTECS_NUM")) {
                    populateIngredientListField(record, rowData, "RTECS_NUM");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RTECS_CODE")) {
                    populateIngredientListField(record, rowData, "RTECS_CODE");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRCNT")) {
                    populateIngredientListField(record, rowData, "PRCNT");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA_PEL")) {
                    populateIngredientListField(record, rowData, "OSHA_PEL");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA_STEL")) {
                    populateIngredientListField(record, rowData, "OSHA_STEL");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ACGIH_TLV")) {
                    populateIngredientListField(record, rowData, "ACGIH_TLV");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ACGIH_STEL")) {
                    populateIngredientListField(record, rowData, "ACGIH_STEL");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA_REPORT_QTY")) {
                    populateIngredientListField(record, rowData, "EPA_REPORT_QTY");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DOT_REPORT_QTY")) {
                    populateIngredientListField(record, rowData, "DOT_REPORT_QTY");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRCNT_VOL_VALUE")) {
                    populateIngredientListField(record, rowData, "PRCNT_VOL_VALUE");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRCNT_VOL_WEIGHT")) {
                    populateIngredientListField(record, rowData, "PRCNT_VOL_WEIGHT");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CHEM_MFG_COMP_NAME")) {
                    populateIngredientListField(record, rowData, "CHEM_MFG_COMP_NAME");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ODS_IND")) {
                    populateIngredientListField(record, rowData, "ODS_IND");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OTHER_REC_LIMITS")) {
                    populateIngredientListField(record, rowData, "OTHER_REC_LIMITS");
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private void populateIngredientListField(MSDSRecord record, string[] rowData,
            string fieldName) {

            for (int i = 0; i < record.RadiologicalInfoList.Count; i++) {
                if (rowData[1].ToUpper().Equals("CAS")) {
                    record.IngredientsList[i].CAS = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("INGREDIENT_NAME")) {
                    record.IngredientsList[i].INGREDIENT_NAME = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RTECS_NUM")) {
                    record.IngredientsList[i].RTECS_NUM = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RTECS_CODE")) {
                    record.IngredientsList[i].RTECS_CODE = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRCNT")) {
                    record.IngredientsList[i].PRCNT = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA_PEL")) {
                    record.IngredientsList[i].OSHA_PEL = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OSHA_STEL")) {
                    record.IngredientsList[i].OSHA_STEL = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ACGIH_TLV")) {
                    record.IngredientsList[i].ACGIH_TLV = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ACGIH_STEL")) {
                    record.IngredientsList[i].ACGIH_STEL = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("EPA_REPORT_QTY")) {
                    record.IngredientsList[i].EPA_REPORT_QTY = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("DOT_REPORT_QTY")) {
                    record.IngredientsList[i].DOT_REPORT_QTY = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRCNT_VOL_VALUE")) {
                    record.IngredientsList[i].PRCNT_VOL_VALUE = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("PRCNT_VOL_WEIGHT")) {
                    record.IngredientsList[i].PRCNT_VOL_WEIGHT = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("CHEM_MFG_COMP_NAME")) {
                    record.IngredientsList[i].CHEM_MFG_COMP_NAME = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("ODS_IND")) {
                    record.IngredientsList[i].ODS_IND = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OTHER_REC_LIMITS")) {
                    record.IngredientsList[i].OTHER_REC_LIMITS = rowData[2 + i];
                    continue;
                }
            }
        }

        // TODO redo using processContracts as go by
        private int processRadiologicInfo(int row, ExcelFile fileData, MSDSRecord record) {
            int newRow = 0;

            // Increment the row count to move off the header row
            row++;

            // Get the number of radiological records
            int count = fileData.DataRows[row].Length - 2;
            record.RadiologicalInfoList = new List<MSDSRadiologicalInfo>(count);

            // Go through the records until we hit a blank in the field name column
            for (newRow = row; newRow < fileData.DataRows.Count; newRow++) {
                string[] rowData = fileData.DataRows[newRow];

                if (rowData[1].Length == 0)
                    break;
                if (rowData[1].ToUpper().Equals("RAD_CAS")) {
                    populateRadiologicalListField(record, rowData, "RAD_CAS");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_NAME")) {
                    populateRadiologicalListField(record, rowData, "RAD_NAME");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_SYMBOL")) {
                    populateRadiologicalListField(record, rowData, "RAD_SYMBOL");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NRC_LP_NUM")) {
                    populateRadiologicalListField(record, rowData, "NRC_LP_NUM");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OPERATOR")) {
                    populateRadiologicalListField(record, rowData, "OPERATOR");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_AMOUNT_MICRO")) {
                    populateRadiologicalListField(record, rowData, "RAD_AMOUNT_MICRO");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_FORM")) {
                    populateRadiologicalListField(record, rowData, "RAD_FORM");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("REP_NSN")) {
                    populateRadiologicalListField(record, rowData, "REP_NSN");
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SEALED")) {
                    populateRadiologicalListField(record, rowData, "SEALED");
                    continue;
                }
            }

            // Back up one row in the main loop
            return newRow - 1;
        }

        private void populateRadiologicalListField(MSDSRecord record, string[] rowData,
            string fieldName) {

            for (int i = 0; i < record.RadiologicalInfoList.Count; i++) {
                if (rowData[1].ToUpper().Equals("RAD_CAS")) {
                    record.RadiologicalInfoList[i].RAD_CAS = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_NAME")) {
                    record.RadiologicalInfoList[i].RAD_NAME = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_SYMBOL")) {
                    record.RadiologicalInfoList[i].RAD_SYMBOL = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("NRC_LP_NUM")) {
                    record.RadiologicalInfoList[i].NRC_LP_NUM = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("OPERATOR")) {
                    record.RadiologicalInfoList[i].OPERATOR = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_AMOUNT_MICRO")) {
                    record.RadiologicalInfoList[i].RAD_AMOUNT_MICRO = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("RAD_FORM")) {
                    record.RadiologicalInfoList[i].RAD_FORM = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("REP_NSN")) {
                    record.RadiologicalInfoList[i].REP_NSN = rowData[2 + i];
                    continue;
                }
                if (rowData[1].ToUpper().Equals("SEALED")) {
                    record.RadiologicalInfoList[i].SEALED = rowData[2 + i];
                    continue;
                }
            }
        }
        #endregion

        #region Utilities
        #endregion
    }
}
