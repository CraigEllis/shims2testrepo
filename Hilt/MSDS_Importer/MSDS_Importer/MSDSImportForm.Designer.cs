﻿namespace MSDS_Importer {
    partial class MSDSImportForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.directoryLabel = new System.Windows.Forms.Label();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msdsDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.msdsDirectoryButton = new System.Windows.Forms.Button();
            this.mainFolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.messageStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.countStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.dateStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.destinationGroupBox = new System.Windows.Forms.GroupBox();
            this.shims1RadioButton = new System.Windows.Forms.RadioButton();
            this.hilt2RadioButton = new System.Windows.Forms.RadioButton();
            this.hilt1RadioButton = new System.Windows.Forms.RadioButton();
            this.messageRTBox = new System.Windows.Forms.RichTextBox();
            this.previewButton = new System.Windows.Forms.Button();
            this.importButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.dateTimer = new System.Windows.Forms.Timer(this.components);
            this.mainWorker = new System.ComponentModel.BackgroundWorker();
            this.mainMenuStrip.SuspendLayout();
            this.mainStatusStrip.SuspendLayout();
            this.destinationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // directoryLabel
            // 
            this.directoryLabel.AutoSize = true;
            this.directoryLabel.Location = new System.Drawing.Point(12, 43);
            this.directoryLabel.Name = "directoryLabel";
            this.directoryLabel.Size = new System.Drawing.Size(117, 13);
            this.directoryLabel.TabIndex = 0;
            this.directoryLabel.Text = "MSDS Folder Location:";
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(792, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.openToolStripMenuItem.Text = "&Open";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(97, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.setupToolStripMenuItem.Text = "&Setup";
            this.setupToolStripMenuItem.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(115, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // msdsDirectoryTextBox
            // 
            this.msdsDirectoryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.msdsDirectoryTextBox.Location = new System.Drawing.Point(135, 40);
            this.msdsDirectoryTextBox.Name = "msdsDirectoryTextBox";
            this.msdsDirectoryTextBox.Size = new System.Drawing.Size(604, 20);
            this.msdsDirectoryTextBox.TabIndex = 2;
            this.msdsDirectoryTextBox.Text = "C:\\HubExports\\Test Hull";
            // 
            // msdsDirectoryButton
            // 
            this.msdsDirectoryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.msdsDirectoryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msdsDirectoryButton.Location = new System.Drawing.Point(745, 38);
            this.msdsDirectoryButton.Name = "msdsDirectoryButton";
            this.msdsDirectoryButton.Size = new System.Drawing.Size(35, 23);
            this.msdsDirectoryButton.TabIndex = 3;
            this.msdsDirectoryButton.Text = "...";
            this.msdsDirectoryButton.UseVisualStyleBackColor = true;
            this.msdsDirectoryButton.Click += new System.EventHandler(this.fileButton_Click);
            // 
            // mainStatusStrip
            // 
            this.mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.messageStatusLabel,
            this.progressBar,
            this.countStatusLabel,
            this.dateStatusLabel});
            this.mainStatusStrip.Location = new System.Drawing.Point(0, 550);
            this.mainStatusStrip.Name = "mainStatusStrip";
            this.mainStatusStrip.Size = new System.Drawing.Size(792, 22);
            this.mainStatusStrip.TabIndex = 4;
            this.mainStatusStrip.Text = "statusStrip1";
            // 
            // messageStatusLabel
            // 
            this.messageStatusLabel.Name = "messageStatusLabel";
            this.messageStatusLabel.Size = new System.Drawing.Size(320, 17);
            this.messageStatusLabel.Spring = true;
            this.messageStatusLabel.Text = "Ready";
            this.messageStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(300, 16);
            // 
            // countStatusLabel
            // 
            this.countStatusLabel.AutoSize = false;
            this.countStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.countStatusLabel.Name = "countStatusLabel";
            this.countStatusLabel.Size = new System.Drawing.Size(50, 17);
            // 
            // dateStatusLabel
            // 
            this.dateStatusLabel.AutoSize = false;
            this.dateStatusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.dateStatusLabel.Name = "dateStatusLabel";
            this.dateStatusLabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.dateStatusLabel.Size = new System.Drawing.Size(105, 17);
            this.dateStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // destinationGroupBox
            // 
            this.destinationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationGroupBox.Controls.Add(this.shims1RadioButton);
            this.destinationGroupBox.Controls.Add(this.hilt2RadioButton);
            this.destinationGroupBox.Controls.Add(this.hilt1RadioButton);
            this.destinationGroupBox.Location = new System.Drawing.Point(12, 70);
            this.destinationGroupBox.Name = "destinationGroupBox";
            this.destinationGroupBox.Size = new System.Drawing.Size(768, 55);
            this.destinationGroupBox.TabIndex = 5;
            this.destinationGroupBox.TabStop = false;
            this.destinationGroupBox.Text = "Destination:";
            // 
            // shims1RadioButton
            // 
            this.shims1RadioButton.AutoSize = true;
            this.shims1RadioButton.Enabled = false;
            this.shims1RadioButton.Location = new System.Drawing.Point(243, 20);
            this.shims1RadioButton.Name = "shims1RadioButton";
            this.shims1RadioButton.Size = new System.Drawing.Size(80, 17);
            this.shims1RadioButton.TabIndex = 2;
            this.shims1RadioButton.Text = "&SHIMS (v1)";
            this.shims1RadioButton.UseVisualStyleBackColor = true;
            // 
            // hilt2RadioButton
            // 
            this.hilt2RadioButton.AutoSize = true;
            this.hilt2RadioButton.Enabled = false;
            this.hilt2RadioButton.Location = new System.Drawing.Point(123, 20);
            this.hilt2RadioButton.Name = "hilt2RadioButton";
            this.hilt2RadioButton.Size = new System.Drawing.Size(70, 17);
            this.hilt2RadioButton.TabIndex = 1;
            this.hilt2RadioButton.Text = "HILT (v&2)";
            this.hilt2RadioButton.UseVisualStyleBackColor = true;
            // 
            // hilt1RadioButton
            // 
            this.hilt1RadioButton.AutoSize = true;
            this.hilt1RadioButton.Checked = true;
            this.hilt1RadioButton.Location = new System.Drawing.Point(16, 20);
            this.hilt1RadioButton.Name = "hilt1RadioButton";
            this.hilt1RadioButton.Size = new System.Drawing.Size(70, 17);
            this.hilt1RadioButton.TabIndex = 0;
            this.hilt1RadioButton.TabStop = true;
            this.hilt1RadioButton.Text = "HILT (v&1)";
            this.hilt1RadioButton.UseVisualStyleBackColor = true;
            // 
            // messageRTBox
            // 
            this.messageRTBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messageRTBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageRTBox.Location = new System.Drawing.Point(15, 131);
            this.messageRTBox.Name = "messageRTBox";
            this.messageRTBox.Size = new System.Drawing.Size(765, 376);
            this.messageRTBox.TabIndex = 6;
            this.messageRTBox.Text = "";
            // 
            // previewButton
            // 
            this.previewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.previewButton.Location = new System.Drawing.Point(15, 517);
            this.previewButton.Name = "previewButton";
            this.previewButton.Size = new System.Drawing.Size(75, 23);
            this.previewButton.TabIndex = 7;
            this.previewButton.Text = "P&review";
            this.previewButton.UseVisualStyleBackColor = true;
            this.previewButton.Click += new System.EventHandler(this.previewButton_Click);
            // 
            // importButton
            // 
            this.importButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.importButton.Location = new System.Drawing.Point(614, 517);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(75, 23);
            this.importButton.TabIndex = 8;
            this.importButton.Text = "&Import";
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(705, 517);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // dateTimer
            // 
            this.dateTimer.Enabled = true;
            this.dateTimer.Interval = 10000;
            this.dateTimer.Tick += new System.EventHandler(this.dateTimer_Tick);
            // 
            // mainWorker
            // 
            this.mainWorker.WorkerReportsProgress = true;
            this.mainWorker.WorkerSupportsCancellation = true;
            this.mainWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.mainWorker_DoWork);
            this.mainWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.mainWorker_ProgressChanged);
            this.mainWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.mainWorker_RunWorkerCompleted);
            // 
            // MSDSImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 572);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.importButton);
            this.Controls.Add(this.previewButton);
            this.Controls.Add(this.messageRTBox);
            this.Controls.Add(this.destinationGroupBox);
            this.Controls.Add(this.mainStatusStrip);
            this.Controls.Add(this.msdsDirectoryButton);
            this.Controls.Add(this.msdsDirectoryTextBox);
            this.Controls.Add(this.directoryLabel);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MSDSImportForm";
            this.Text = "MSDS Import";
            this.Load += new System.EventHandler(this.MSDSImportForm_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.mainStatusStrip.ResumeLayout(false);
            this.mainStatusStrip.PerformLayout();
            this.destinationGroupBox.ResumeLayout(false);
            this.destinationGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label directoryLabel;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.TextBox msdsDirectoryTextBox;
        private System.Windows.Forms.Button msdsDirectoryButton;
        private System.Windows.Forms.FolderBrowserDialog mainFolderBrowser;
        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.GroupBox destinationGroupBox;
        private System.Windows.Forms.ToolStripStatusLabel messageStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ToolStripStatusLabel countStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel dateStatusLabel;
        private System.Windows.Forms.RadioButton shims1RadioButton;
        private System.Windows.Forms.RadioButton hilt2RadioButton;
        private System.Windows.Forms.RadioButton hilt1RadioButton;
        private System.Windows.Forms.RichTextBox messageRTBox;
        private System.Windows.Forms.Button previewButton;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Timer dateTimer;
        private System.ComponentModel.BackgroundWorker mainWorker;
    }
}

