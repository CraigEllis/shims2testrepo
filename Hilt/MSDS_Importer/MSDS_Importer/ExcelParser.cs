﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;

namespace MSDS_Importer {
    public class ExcelParser {
        public enum FILE_TYPE {
            EXCEL_8,    //Old format 97-2003
            EXCEL_12,   // 2007+
            TEXT,
            INVALID,
            NO_FILENAME
        };

        private String m_ErrorMsg = null;
        public string ErrorMsg {
            get {
                return m_ErrorMsg;
            }
            set {
                m_ErrorMsg = value;
            }
        }

        //excel file connection
        private OleDbConnection con;
        private bool isExcel8 = true;

        public ExcelFile parseExcel(string filePath) {
            return parseExcel(filePath, 0);
            }
        public ExcelFile parseExcel(string filePath, int sheet) {
            ExcelFile fileData = new ExcelFile();
            List<string[]> resultList = new List<string[]>();

            fileData.FileName = filePath;

            // Get the file date
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
            fileData.FileDate = fileInfo.LastWriteTime;

            try {
                //attempt to connect to the excel file
                bool isConnected = connect(filePath);

                //unable to connect
                if (!isConnected) {
                    throw new Exception(m_ErrorMsg);
                }

                DataTable dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                String[] excelSheets = new String[dt.Rows.Count];
                int i = 0;

                // Add the sheet name to the string array.
                foreach (DataRow row in dt.Rows) {
                    excelSheets[i] = row["TABLE_NAME"].ToString();
                    i++;
                }

                // Create the select statement for the selected sheet number 
                string selectString = "SELECT * FROM [" + excelSheets[sheet].ToString() + "]";
                OleDbCommand cmd = new OleDbCommand(selectString, con);
            
                //bool to see if this is the first time we entered the loop
                bool isFirst = true;

                //reader
                OleDbDataReader rdr = cmd.ExecuteReader();
                DataTable schemaTable = rdr.GetSchemaTable();

                if (isExcel8) {
                    // Get the Excel 8 header row
                    string[] rowheaders = new string[schemaTable.Rows.Count];

                    int count = 0;

                    foreach (DataRow r in schemaTable.Rows) {
                        string columnName = r["columnname"].ToString();
                        rowheaders[count] = columnName;

                        count++;
                    }

                    //resultList.Add(rowheaders);
                    fileData.HeaderRecord = rowheaders;
                    isFirst = false;
                }

                while (rdr.Read()) {         
                    // Handle the data rows
                    string[] row = new string[schemaTable.Rows.Count];

                    for (int j = 0; j < row.Length; j++) {
                        row[j] = rdr[j].ToString();
                    }

                    // See if this is the header row
                    if (isFirst) {
                        fileData.HeaderRecord = row;
                        isFirst = false;
                    }
                    else {
                        resultList.Add(row);
                    }                    
                }

                con.Close();
            }
            catch (Exception ex) {
                con.Close();

                if (ex.Message.StartsWith("External table") && ex.Message.EndsWith("format.")) {
                    throw new Exception("File is not in the expected Excel 97-2003 format.");
                }
                else {
                    throw new Exception("Error reading the file. " + ex.Message);
                }
            }

            // Add the row data to the file object
            fileData.DataRows = resultList;

            return fileData;
        }

        // Connect to the excel file
        private bool connect(string filePath)
        {
            // pre-set the connection string for Excel 2003
            string connectionString = "Provider=Microsoft.Jet.OleDb.4.0; Data Source=" +
                filePath + "; Extended Properties=Excel 8.0;";

            // Check to see what type of file we have
            if (filePath.Equals(""))
            {
                m_ErrorMsg = "No file name provided, please select a file to upload.";
                return false;
            }
            else if (filePath.ToLower().Contains(".xlsx") || filePath.ToLower().Contains(".xlsm")) {
                // Connect to Excel 12 file - change the connection string 
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='"
                    + filePath + "';Extended Properties='Excel 12.0;HDR=No;IMEX=1;'";
                isExcel8 = false;
            }

            // Create the connection object
            con = new OleDbConnection(connectionString);
            con.Open();

            //check to the connection
            ConnectionState state = con.State;
            if (state == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                m_ErrorMsg = "Unable to connect to the Excel file. Please check the file and try again";
                return false;
            }
        }

    }

    public class ExcelFile {
        public ExcelFile() {
        }

        public String FileName { get; set; }
        public DateTime FileDate { get; set; }
        public ExcelParser.FILE_TYPE FileType { get; set; }
        public String[] HeaderRecord { get; set; }
        public List<String[]> DataRows { get; set; }
    }
}