﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MSDS_Importer
{
    public partial class SetupForm : Form
    {
        //Default Constructor
        public SetupForm()
        {
            InitializeComponent();

            // Load the properties
            loadProperties();
        }

        // App properties
        private void loadProperties() {
            this.txtSHIMSFilePath.Text = Properties.Settings.Default.SHIMSFileName;
            this.txtServerBox.Text = Properties.Settings.Default.ServerName;
            if (Properties.Settings.Default.ServerPort == 0) {
                chkLocalMachine.Checked = true;
            }
            else {
                this.txtPortBox.Text = Properties.Settings.Default.ServerPort.ToString();
            }
            this.txtDatabase.Text = Properties.Settings.Default.Database;
            this.txtUsernameBox.Text = Properties.Settings.Default.UserName;
            this.txtPasswordBox.Text = Properties.Settings.Default.UserPassword;
        }

        private Boolean saveProperties() {
            Boolean saved = true;

            try {
                Properties.Settings.Default.SHIMSFileName = this.txtSHIMSFilePath.Text;
                Properties.Settings.Default.ServerName = this.txtServerBox.Text;
                int nPort = 0;
                int.TryParse(this.txtPortBox.Text, out nPort);
                Properties.Settings.Default.ServerPort = nPort;
                Properties.Settings.Default.Database = this.txtDatabase.Text;
                Properties.Settings.Default.UserName = this.txtUsernameBox.Text;
                Properties.Settings.Default.UserPassword = this.txtPasswordBox.Text;

                // Build a new ConnectionString
                Properties.Settings.Default.ConnectionInfo = buildConnectionString();

                Properties.Settings.Default.Save();
            }
            catch (Exception ex) {
                MessageBox.Show("Error saving MSDS Importer setup parameters\n\n" + ex.Message,
                    "MSDS Importer");
                saved = false;
            }
            return saved;
        }

        //Browse Button actions
        private void btnBrowseSHIMSFiles_Click(object sender, EventArgs e) {
            this.openFileDialog1.FileName = txtSHIMSFilePath.Text;
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK) {
                this.txtSHIMSFilePath.Text = this.openFileDialog1.FileName;
            }
        }

        //Show Password box dialog.
        private void chkPassword_CheckedChanged(object sender, EventArgs e)
        {
            this.txtPasswordBox.UseSystemPasswordChar = !this.chkPassword.Checked;
        }

        //Cancel Button dialog
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Saves the properties, if all user specified data is valid.
        private void btnSave_Click(object sender, EventArgs e)
        {
            // validate input
            string validationMessage = "";

            // SHIMS
            //if (this.txtSHIMSFilePath.Text == "") {
            //    validationMessage += "\nSHIMS Settings\n\tPlease select a SHIMS Database\n";
            //}

            // Server
            string serverSettings = "\nHILT HUB Database Settings\n";
            if (this.txtPasswordBox.Text == "") {
                if (!validationMessage.Contains(serverSettings))
                    validationMessage += serverSettings;
                validationMessage += "\nPlease specify the HILT database password\n";
            }

            if (chkLocalMachine.Checked) {
                // Allow zero port for local machines
                txtPortBox.Text = "";
            }
            else {
                if (this.txtPortBox.Text == "") {
                    if (!validationMessage.Contains(serverSettings))
                        validationMessage += serverSettings;
                    validationMessage += "Please specify the HILT database port\n";
                }

                // Ensure the port is a positive int
                else {
                    int port = 1;
                    try {
                        port = int.Parse(this.txtPortBox.Text);

                        if (port <= 0) {
                            if (!validationMessage.Contains(serverSettings))
                                validationMessage += serverSettings;
                            validationMessage += "The port must be a positive integer greater than 0.\n";
                        }
                    }
                    catch (Exception) {
                        if (!validationMessage.Contains(serverSettings))
                            validationMessage += serverSettings;
                        validationMessage += "The port must be a positive integer greater than 0.\n";
                    }
                }
            }

            if (this.txtServerBox.Text == "")
            {
                if (!validationMessage.Contains(serverSettings))
                    validationMessage += serverSettings;
                validationMessage += "Please specify the HILT database server\n";
            }

            if (this.txtUsernameBox.Text == "")
            {
                if (!validationMessage.Contains(serverSettings))
                    validationMessage += serverSettings;
                validationMessage += "Please specify the HILT database user\n";
            }

            if (validationMessage != "")
            {
                MessageBox.Show(validationMessage, "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Save the properties
            Boolean saved = saveProperties();

            // Close the form
            this.Close();
        }

        private void btnTestConnection_Click(object sender, EventArgs e) {
            String connectMsg = null;

            // Build the connection string
            String connString = buildConnectionString();

            DatabaseManager mgr = new DatabaseManager(connString);

            connectMsg = mgr.testConnection(connString);

            MessageBox.Show(connString + "\n\n" + connectMsg, "Test Connection");
        }

        private String buildConnectionString() {
            // Build the connection string
            StringBuilder sb = new StringBuilder();
            sb.Append("Data Source=" + txtServerBox.Text);
            if (txtPortBox.Text.Length > 0)
                sb.Append("," + txtPortBox.Text);
            sb.Append(";Initial Catalog=" + txtDatabase.Text);
            sb.Append(";Persist Security Info=False;uid=" + txtUsernameBox.Text);
            sb.Append(";pwd=" + txtPasswordBox.Text);

            return sb.ToString();
        }

        private void chkLocalMachine_CheckedChanged(object sender, EventArgs e) {
            if (chkLocalMachine.Checked) {
                txtPortBox.Text = "";
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
