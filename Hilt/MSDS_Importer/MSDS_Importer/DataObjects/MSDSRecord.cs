using System;
using System.Collections.Generic;
using System.Web;

namespace MSDS_Importer.DataObjects {
    /// <summary>
    /// Container for all MSDS components of an MSDS record
    /// Basic msds info from the msds_master table is implemented as properties of
    /// the MSDSRecord. Data from MSDS sub tables are implemented as sub-objects of
    /// the MSDS Record
    /// </summary>
    public class MSDSRecord {
        #region Constructors
        // Default constructor
        public MSDSRecord() {
        }

        // Constructor using view models as input
        #endregion

        // MSDS MASTER TABLE
        #region MSDS TABLE
        // MSDS ID
        public long msds_id {
            get;
            set;
        }
        //PRODUCT SERIAL NUMBER
        public String MSDSSERNO {
            get;
            set;
        }
        //RESPONSIBLE PARTY CAGE.
        public String CAGE {
            get;
            set;
        }
        //RESPONSIBLE PARTY COMPANY NAME
        public String MANUFACTURER {
            get;
            set;
        }
        //PART NUMBER
        public String PARTNO {
            get;
            set;
        }
        //FSC
        public String FSC {
            get;
            set;
        }
        //NIIN
        public String NIIN {
            get;
            set;
        }
        //HCC ID
        public long hcc_id {
            get;
            set;
        }
        //HCC
        public String HCC {
            get;
            set;
        }
        // MSDS FILE NAME
        public String file_name {
            get;
            set;
        }
        //ARTICLE INDEX.
        public String ARTICLE_IND {
            get;
            set;
        }
        //DESCRIPTION
        public String DESCRIPTION {
            get;
            set;
        }
        //EMERGENCY TELEPHONE RESPONSIBLE PARTY
        public String EMERGENCY_TEL {
            get;
            set;
        }
        //END ITEM COMPONENT INDICATOR.
        public String END_COMP_IND {
            get;
            set;
        }
        //END ITEM INDICATOR.
        public String END_ITEM_IND {
            get;
            set;
        }
        //KIT INDICATOR.
        public String KIT_IND {
            get;
            set;
        }
        //KIT PART INDICATOR.
        public String KIT_PART_IND {
            get;
            set;
        }
        //MANUFACTURER MSDS NUMBER
        public String MANUFACTURER_MSDS_NO {
            get;
            set;
        }
        //MIXTURE INDICATOR.
        public String MIXTURE_IND {
            get;
            set;
        }
        //PRODUCT IDENTITY
        public String PRODUCT_IDENTITY {
            get;
            set;
        }
        //PRODUCT INDICATOR.
        public String PRODUCT_IND {
            get;
            set;
        }
        //PRODUCT LANGUAGE
        public String PRODUCT_LANGUAGE {
            get;
            set;
        }
        //PRODUCT LOAD DATE.
        public DateTime PRODUCT_LOAD_DATE {
            get;
            set;
        }
        //PRODUCT RECORD STATUS.
        public String PRODUCT_RECORD_STATUS {
            get;
            set;
        }
        //PRODUCT REVISION NUMBER.
        public String PRODUCT_REVISION_NO {
            get;
            set;
        }
        //PROPRIETARY INDICATOR
        public String PROPRIETARY_IND {
            get;
            set;
        }
        //PUBLISHED INDICATOR
        public String PUBLISHED_IND {
            get;
            set;
        }
        //PURCHASED PRODUCT INDICATOR
        public String PURCHASED_PROD_IND {
            get;
            set;
        }
        //PURE INDICATOR
        public String PURE_IND {
            get;
            set;
        }
        //RADIOACTIVE INDICATOR
        public String RADIOACTIVE_IND {
            get;
            set;
        }
        //SERVICE AGENCY
        public String SERVICE_AGENCY_CODE {
            get;
            set;
        }
        //TRADE NAME
        public String TRADE_NAME {
            get;
            set;
        }
        //TRADE SECRET INDICATOR
        public String TRADE_SECRET_IND {
            get;
            set;
        }
        // MANUALLY ENTERED
        public bool manually_entered {
            get;
            set;
        }
        // DATA SOURCE CD
        public String data_source_cd {
            get;
            set;
        }
        // CREATED DATETIME
        public DateTime created {
            get;
            set;
        }
        // CREATED BY
        public String created_by {
            get;
            set;
        }
        // CHANGED DATETIME
        public DateTime changed {
            get;
            set;
        }
        // CHANGED BY
        public String changed_by {
            get;
            set;
        }
        #endregion

        // Sub Table Objects
        #region SUB_TABLE OBJECTS
        //MSDS AFJM PSN
        public MSDS_AFJM_PSN Afjm_Psn {
            get;
            set;
        }
        //MSDS CONTRACTOR LIST
        public List<MSDSContractorInfo> ContractorList { 
            get; 
            set; 
        }
        //MSDS DISPOSAL
        public MSDSDisposal Disposal {
            get;
            set;
        }
        //MSDS DOC TYPES
        public MSDSDocumentTypes DocumentTypes { 
            get; 
            set; 
        }
        //MSDS DOT PSN
        public MSDS_DOT_PSN Dot_Psn { 
            get; 
            set; 
        }
        //MSDS IATA PSN
        public MSDS_IATA_PSN Iata_Psn { 
            get; 
            set; 
        }
        //MSDS IMO PSN
        public MSDS_IMO_PSN Imo_Psn { 
            get; 
            set; 
        } 
        // MSDS INGREDIENTS
        public List<MSDSIngredient> IngredientsList { 
            get; 
            set; 
        }
        //MSDS ITEM DESCRIPTION
        public MSDSItemDescription ItemDescription { 
            get; 
            set; 
        }
        //MSDS LABEL INFO
        public MSDSLabelInfo LabelInfo { 
            get; 
            set; 
        }
        //MSDS PHYS CHEMICAL TABLE
        public MSDSPhysChemical PhysChemical { 
            get; 
            set; 
        }
        //MSDS RADIOLOGICAL INFO
        public List<MSDSRadiologicalInfo> RadiologicalInfoList { 
            get; 
            set; 
        }
        //MSDS TRANSPORTATION
        public MSDSTransportation Transportation {
            get;
            set;
        }
        #endregion

        #region HMIRS Fields
        public long doc_index_id {
            get;
            set;
        }
        public long doc_data_id {
            get;
            set;
        }
        public DateTime last_update {
            get;
            set;
        }
        #endregion
    }

    //MSDS AFJM PSN
    public class MSDS_AFJM_PSN {
        //AFJM HAZARD CLASS/DIV
        public String AFJM_HAZARD_CLASS {
            get;
            set;
        }
        //AFJM PACKAGING PARAGRAPH
        public String AFJM_PACK_PARAGRAPH {
            get;
            set;
        }
        //AFJM PACKING GROUP
        public String AFJM_PACK_GROUP {
            get;
            set;
        }
        //AFJM PROPER SHIPPING NAME
        public String AFJM_PROP_SHIP_NAME {
            get;
            set;
        }
        //AFJM PROPER SHIPPING NAME MODIFIER
        public String AFJM_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //AFJM PSN CODE
        public String AFJM_PSN_CODE {
            get;
            set;
        }
        //AFJM SPECIAL PROVISIONS
        public String AFJM_SPECIAL_PROV {
            get;
            set;
        }
        //AFJM SUBSIDIARY RISK
        public String AFJM_SUBSIDIARY_RISK {
            get;
            set;
        }
        //AFJM SYMBOLS
        public String AFJM_SYMBOLS {
            get;
            set;
        }
        //AFJM UN ID NUMBER
        public String AFJM_UN_ID_NUMBER {
            get;
            set;
        }

        public DateTime TMSP_LAST_UPDT {
            get;
            set;
        }
    }

    //MSDS CONTRACTOR INFO
    public class MSDSContractorInfo {
        //CONTRACT NUMBER
        public string CT_NUMBER {
            get;
            set;
        }
        //CONTRACTOR CAGE
        public string CT_CAGE {
            get;
            set;
        }
        //CONTRACTOR CITY
        public string CT_CITY {
            get;
            set;
        }
        //CONTRACTOR COMPANY NAME
        public string CT_COMPANY_NAME {
            get;
            set;
        }
        //CONTRACTOR COUNTRY
        public string CT_COUNTRY {
            get;
            set;
        }
        //CONTRACTOR PO BOX
        public string CT_PO_BOX {
            get;
            set;
        }
        //CONTRACTOR STATE
        public string CT_STATE {
            get;
            set;
        }
        //CONTRACTOR TELE NUMBER
        public string CT_PHONE {
            get;
            set;
        }
        //PURCHASE ORDER NUMBER
        public string PURCHASE_ORDER_NO {
            get;
            set;
        }
        //Addess 1
        public string CT_ADDRESS_1 {
            get;
            set;
        }
        //Zip Code
        public string CT_ZIP_CODE {
            get;
            set;
        }

        public DateTime TMSP_LAST_UPDT {
            get;
            set;
        }
    }

    //MSDS DISPOSAL
    public class MSDSDisposal {
        //DISPOSAL ADDITIONAL INFO
        public String DISPOSAL_ADD_INFO {
            get;
            set;
        }
        //EPA HAZARDOUS WASTE CODE
        public String EPA_HAZ_WASTE_CODE {
            get;
            set;
        }
        //EPA HAZARDOUS WAST INDICATOR
        public String EPA_HAZ_WASTE_IND {
            get;
            set;
        }
        //EPA HAZARDOUS WASTE NAME
        public String EPA_HAZ_WASTE_NAME {
            get;
            set;
        }
    }

    //MSDS DOC TYPES
    public class MSDSDocumentTypes {
        //THESE ARE ALL SIMPLY FILE PATHS, ACTUAL
        //FILES WILL BE OBTAINED AND INSERTED AT HILT INSERTION TIME.
        #region MSDS DOCS
        //MSDS - SEE MSDS TABLE.
        //MSDS MANUFACTURER LABEL
        public String manufacturer_label_filename {
            get;
            set;
        }
        public long manufacturer_label_id {
            get;
            set;
        }
        //MSDS TRANSLATED
        public String msds_translated_filename {
            get;
            set;
        }
        public long msds_translated_id {
            get;
            set;
        }
        //NESHAP COMPLIANCE CERTIFICATE
        public String neshap_comp_filename {
            get;
            set;
        }
        public long neshap_comp_id {
            get;
            set;
        }
        //OTHER DOCS
        public String other_docs_filename {
            get;
            set;
        }
        public long other_docs_id {
            get;
            set;
        }
        //PRODUCT SHEET
        public String product_sheet_filename {
            get;
            set;
        }
        public long product_sheet_id {
            get;
            set;
        }
        //TRANSPORTATION CERT 
        public String transportation_cert_filename {
            get;
            set;
        }
        public long transportation_cert_id {
            get;
            set;
        }
        #endregion
    }

    //MSDS DOT PSN
    public class MSDS_DOT_PSN {
        //DOT HAZARD CLASS/DIV
        public String DOT_HAZARD_CLASS_DIV {
            get;
            set;
        }
        //DOT HAZARD LABEL
        public String DOT_HAZARD_LABEL {
            get;
            set;
        }
        //DOT MAX QUANTITY: CARGO AIRCRAFT ONLY
        public String DOT_MAX_CARGO {
            get;
            set;
        }
        //DOT MAX QUANTITY: PASSENGER AIRCRAFT/RAIL
        public String DOT_MAX_PASSENGER {
            get;
            set;
        }
        //DOT PACKAGING BULK
        public String DOT_PACK_BULK {
            get;
            set;
        }
        //DOT PACKAGING EXCEPTIONS
        public String DOT_PACK_EXCEPTIONS {
            get;
            set;
        }
        //DOT PACKAGING NON BULK
        public String DOT_PACK_NONBULK {
            get;
            set;
        }
        //DOT PACKING GROUP
        public String DOT_PACK_GROUP {
            get;
            set;
        }
        //DOT PROPER SHIPPING NAME
        public String DOT_PROP_SHIP_NAME {
            get;
            set;
        }
        //DOT PROPER SHIPPING NAME MODIFIER
        public String DOT_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //DOT PSN CODE
        public String DOT_PSN_CODE {
            get;
            set;
        }
        //DOT SPECIAL PROVISION
        public String DOT_SPECIAL_PROVISION {
            get;
            set;
        }
        //DOT SYMBOLS
        public String DOT_SYMBOLS {
            get;
            set;
        }
        //DOT UN ID NUMBER
        public String DOT_UN_ID_NUMBER {
            get;
            set;
        }
        //DOT WATER SHIPMENT OTHER REQS
        public String DOT_WATER_OTHER_REQ {
            get;
            set;
        }
        //DOT WATER SHIPMENT VESSEL STOWAGE
        public String DOT_WATER_VESSEL_STOW {
            get;
            set;
        }

        public DateTime TMSP_LAST_UPDT {
            get;
            set;
        }
    }

    //MSDS IATA PSN
    public class MSDS_IATA_PSN {
        //IATA CARGO PACKING: NOTE CARGO AIRCRAFT PACKING INSTRUCTIONS
        public String IATA_CARGO_PACKING {
            get;
            set;
        }
        //IATA HAZARD CLASS/DIV
        public String IATA_HAZARD_CLASS {
            get;
            set;
        }
        //IATA HAZARD LABEL
        public String IATA_HAZARD_LABEL {
            get;
            set;
        }
        //IATA PACKING GROUP
        public String IATA_PACK_GROUP {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: LMT QTY PKG INSTR
        public String IATA_PASS_AIR_PACK_LMT_INSTR {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: LMT QTY MAX QTY PER PKG
        public String IATA_PASS_AIR_PACK_LMT_PER_PKG {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING MAX QUANTITY
        public String IATA_PASS_AIR_MAX_QTY {
            get;
            set;
        }
        //IATA PASSENGER AIR PACKING: NOTE PASSENGER AIR PACKING INSTR
        public String IATA_PASS_AIR_PACK_NOTE {
            get;
            set;
        }
        //IATA PROPER SHIPPING NAME
        public String IATA_PROP_SHIP_NAME {
            get;
            set;
        }
        //IATA PROPER SHIPPING NAME MODIFIER
        public String IATA_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //IATA CARGO PACKING: MAX QUANTITY
        public String IATA_CARGO_PACK_MAX_QTY {
            get;
            set;
        }
        //IATA PSN CODE
        public String IATA_PSN_CODE {
            get;
            set;
        }
        //IATA SPECIAL PROVISIONS
        public String IATA_SPECIAL_PROV {
            get;
            set;
        }
        //IATA SUBSIDIARY RISK
        public String IATA_SUBSIDIARY_RISK {
            get;
            set;
        }
        //IATA UN ID NUMBER
        public String IATA_UN_ID_NUMBER {
            get;
            set;
        }

        public DateTime TMSP_LAST_UPDT {
            get;
            set;
        }
    }

    //MSDS IMO PSN
    public class MSDS_IMO_PSN {
        //IMO EMS NUMBER
        public String IMO_EMS_NO {
            get;
            set;
        }
        //IMO HAZARD CLASS/DIV
        public String IMO_HAZARD_CLASS {
            get;
            set;
        }
        //IMO IBC INSTRUCTIONS
        public String IMO_IBC_INSTR {
            get;
            set;
        }
        //IMO IBC PROVISIONS
        public String IMO_IBC_PROVISIONS {
            get;
            set;
        }
        //IMO LIMITED QUANTITY
        public String IMO_LIMITED_QTY {
            get;
            set;
        }
        //IMO PACKING GROUP
        public String IMO_PACK_GROUP {
            get;
            set;
        }
        //IMO PACKING INSTRUCTIONS
        public String IMO_PACK_INSTRUCTIONS {
            get;
            set;
        }
        //IMO PACKING PROVISIONS
        public String IMO_PACK_PROVISIONS {
            get;
            set;
        }
        //IMO PROPER SHIPPING NAME
        public String IMO_PROP_SHIP_NAME {
            get;
            set;
        }
        //IMO PROPER SHIPPING NAME MODIFIER
        public String IMO_PROP_SHIP_MODIFIER {
            get;
            set;
        }
        //IMO PSN CODE
        public String IMO_PSN_CODE {
            get;
            set;
        }
        //IMO SPECIAL PROVISIONS
        public String IMO_SPECIAL_PROV {
            get;
            set;
        }
        //IMO STOWAGE AND SEGREGATION
        public String IMO_STOW_SEGR {
            get;
            set;
        }
        //IMO SUBSIDIARY RISK LABEL
        public String IMO_SUBSIDIARY_RISK {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, IMO
        public String IMO_TANK_INSTR_IMO {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, PROVISIONS
        public String IMO_TANK_INSTR_PROV {
            get;
            set;
        }
        //IMO TANK INSTRUCTIONS, UN
        public String IMO_TANK_INSTR_UN {
            get;
            set;
        }
        //IMO UN NUMBER
        public String IMO_UN_NUMBER {
            get;
            set;
        }

        public DateTime TMSP_LAST_UPDT {
            get;
            set;
        }
    }

    //MSDS INGREDIENTS
    public class MSDSIngredient {
        //PERCENT TEXT value
        public string PRCNT {
            get;
            set;
        }
        //PERCENT VOLUME value
        public string PRCNT_VOL_VALUE {
            get;
            set;
        }
        //PERCENT WEIGHT value
        public string PRCNT_VOL_WEIGHT {
            get;
            set;
        }
        //ACGIH STEL
        public string ACGIH_STEL {
            get;
            set;
        }
        //ACGIH TLV
        public string ACGIH_TLV {
            get;
            set;
        }
        //CAS NUMBER
        public string CAS {
            get;
            set;
        }
        //CHEMICAL MANUFACTURER COMPANY NAME
        public string CHEM_MFG_COMP_NAME {
            get;
            set;
        }
        //COMPONENT INGREDIENT NAME
        public string INGREDIENT_NAME {
            get;
            set;
        }
        //DOT RQ
        public string DOT_REPORT_QTY {
            get;
            set;
        }
        //EPA RQ
        public string EPA_REPORT_QTY {
            get;
            set;
        }
        //ODS INDICATOR
        public string ODS_IND {
            get;
            set;
        }
        //OSHA PEL
        public string OSHA_PEL {
            get;
            set;
        }
        //OSHA STEL
        public string OSHA_STEL {
            get;
            set;
        }
        //OTHER RECORDED LIMITS
        public string OTHER_REC_LIMITS {
            get;
            set;
        }
        //RTECS NUMBER
        public string RTECS_NUM {
            get;
            set;
        }
        //RTECS CODE
        public string RTECS_CODE {
            get;
            set;
        }
    }

    //MSDS ITEM DESCRIPTION
    public class MSDSItemDescription {
        //BATCH NUMBER
        public String BATCH_NUMBER {
            get;
            set;
        }
        //LOT NUMBER
        public String LOT_NUMBER {
            get;
            set;
        }
        //ITEM MANAGER
        public String ITEM_MANAGER {
            get;
            set;
        }
        //ITEM NAME
        public String ITEM_NAME {
            get;
            set;
        }
        //LOGISTICS FLSI NIIN VERIFIED
        public String LOG_FLIS_NIIN_VER {
            get;
            set;
        }
        //LOGISTICS FSC
        public int LOG_FSC {
            get;
            set;
        }
        //NET UNIT WEIGHT
        public String NET_UNIT_WEIGHT {
            get;
            set;
        }
        //QUANTITATIVE EXPRESSION EXP
        public String QUANTITATIVE_EXPRESSION {
            get;
            set;
        }
        //SHELF LIFE CODE
        public String SHELF_LIFE_CODE {
            get;
            set;
        }
        //SPECIAL EMPHASIS CODE
        public String SPECIAL_EMP_CODE {
            get;
            set;
        }
        //SPECIFICATION NUMBER
        public String SPECIFICATION_NUMBER {
            get;
            set;
        }
        //SPECIFICATION TYPE/GRADE/CLASS
        public String TYPE_GRADE_CLASS {
            get;
            set;
        }
        //TYPE OF CONTAINER
        public String TYPE_OF_CONTAINER {
            get;
            set;
        }
        //UN/NA NUMBER
        public String UN_NA_NUMBER {
            get;
            set;
        }
        //UNIT OF ISSUE
        public String UNIT_OF_ISSUE {
            get;
            set;
        }
        //UNIT OF ISSUE CONTAINER QUANTITY
        public String UI_CONTAINER_QTY {
            get;
            set;
        }
        //UPC/GTIN
        public String UPC_GTIN {
            get;
            set;
        }
    }

    //MSDS LABEL INFO
    public class MSDSLabelInfo {
        //COMPANY CAGE(RESPONSIBLE PARTY)
        public String COMPANY_CAGE_RP {
            get;
            set;
        }
        //COMPANY NAME(RESPONSIBLE PARTY)
        public String COMPANY_NAME_RP {
            get;
            set;
        }
        //LABEL EMERGENCY TELE NUMBER
        public String LABEL_EMERG_PHONE {
            get;
            set;
        }
        //LABEL ITEM NAME
        public String LABEL_ITEM_NAME {
            get;
            set;
        }
        //LABEL PROCUREMENT YEAR
        public String LABEL_PROC_YEAR {
            get;
            set;
        }
        //LABEL PRODUCT IDENTITY
        public String LABEL_PROD_IDENT {
            get;
            set;
        }
        //LABEL PRODUCT SERIAL NUMBER
        public String LABEL_PROD_SERIALNO {
            get;
            set;
        }
        //LABEL SIGNAL WORD
        public String LABEL_SIGNAL_WORD_CODE {
            get;
            set;
        }
        //LABEL STOCK NUMBER
        public String LABEL_STOCK_NO {
            get;
            set;
        }
        //SPECIFIC HAZARDS
        public String SPECIFIC_HAZARDS {
            get;
            set;
        }
    }

    //MSDS PHYS CHEMICAL TABLE
    public class MSDSPhysChemical {
        //OSHA_WATER_REACTIVE
        public String OSHA_WATER_REACTIVE {
            get;
            set;
        }
        //PERCENT VOLATILES BY VOLUME
        public String PERCENT_VOL_VOLUME {
            get;
            set;
        }
        //APPEARANCE ODOR TEXT
        public String APP_ODOR {
            get;
            set;
        }
        //AUTOIGNITION TEMP. (C)
        public String AUTOIGNITION_TEMP {
            get;
            set;
        }
        //CARCINOGEN INDICATOR
        public String CARCINOGEN_IND {
            get;
            set;
        }
        //EPA ACUTE
        public String EPA_ACUTE {
            get;
            set;
        }
        //EPA CHRONIC
        public String EPA_CHRONIC {
            get;
            set;
        }
        //EPA FIRE
        public String EPA_FIRE {
            get;
            set;
        }
        //EPA PRESSURE
        public String EPA_PRESSURE {
            get;
            set;
        }
        //EPA REACTIVITY
        public String EPA_REACTIVITY {
            get;
            set;
        }
        //EVAPORATION RATE
        public String EVAP_RATE_REF {
            get;
            set;
        }
        //FLASH POint TEMP (C)
        public String FLASH_PT_TEMP {
            get;
            set;
        }
        //NEUTRALIZING AGENT TEXT
        public String NEUT_AGENT {
            get;
            set;
        }
        //NFPA FLAMMABILITY
        public String NFPA_FLAMMABILITY {
            get;
            set;
        }
        //NFPA HEALTH
        public String NFPA_HEALTH {
            get;
            set;
        }
        //NFPA REACTIVITY
        public String NFPA_REACTIVITY {
            get;
            set;
        }
        //NFPA SPECIAL
        public String NFPA_SPECIAL {
            get;
            set;
        }
        //OSHA CARCINOGENS
        public String OSHA_CARCINOGENS {
            get;
            set;
        }
        //OSHA COMBUSTION LIQUID
        public String OSHA_COMB_LIQUID {
            get;
            set;
        }
        //OSHA COMPRESSED GAS
        public String OSHA_COMP_GAS {
            get;
            set;
        }
        //OSHA CORROSIVE
        public String OSHA_CORROSIVE {
            get;
            set;
        }
        //OSHA EXPLOSIVE
        public String OSHA_EXPLOSIVE {
            get;
            set;
        }
        //OSHA FLAMMABLE
        public String OSHA_FLAMMABLE {
            get;
            set;
        }
        //OSHA HIGHLY TOXIC
        public String OSHA_HIGH_TOXIC {
            get;
            set;
        }
        //OSHA IRRITANT
        public String OSHA_IRRITANT {
            get;
            set;
        }
        //OSHA ORGANIC PEROXIDE
        public String OSHA_ORG_PEROX {
            get;
            set;
        }
        //OSHA OTHER/LONG TERM
        public String OSHA_OTHERLONGTERM {
            get;
            set;
        }
        //OSHA OXIDIZER
        public String OSHA_OXIDIZER {
            get;
            set;
        }
        //OSHA PYROPHORIC
        public String OSHA_PYRO {
            get;
            set;
        }
        //OSHA SENSITIZER
        public String OSHA_SENSITIZER {
            get;
            set;
        }
        //OSHA TOXIC
        public String OSHA_TOXIC {
            get;
            set;
        }
        //OSHA UNSTABLE REACTIVE
        public String OSHA_UNST_REACT {
            get;
            set;
        }

        //OTHER/SHORT TERM
        public String OTHER_SHORT_TERM {
            get;
            set;
        }
        //PH
        public String PH {
            get;
            set;
        }
        //PHYSICAL STATE CODE
        public String PHYS_STATE_CODE {
            get;
            set;
        }
        //SOLUBILITY IN WATER
        public String SOL_IN_WATER {
            get;
            set;
        }
        //SPECIFIC GRAVITY
        public String SPECIFIC_GRAV {
            get;
            set;
        }
        //VAPOR DENSITY
        public String VAPOR_DENS {
            get;
            set;
        }
        //VAPOR PRESSURE
        public String VAPOR_PRESS {
            get;
            set;
        }
        //VISCOSITY
        public String VISCOSITY {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (GM/L)
        public String VOC_GRAMS_LITER {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (LB/G)
        public String VOC_POUNDS_GALLON {
            get;
            set;
        }
        //VOLATILE ORGANIC COMPOUND (WT%)
        public String VOL_ORG_COMP_WT {
            get;
            set;
        }
    }

    //MSDS RADIOLOGICAL INFO
    public class MSDSRadiologicalInfo {
        //NRC LICENSE/PERMIT NUMBER
        public String NRC_LP_NUM {
            get;
            set;
        }
        //OPERATOR
        public String OPERATOR {
            get;
            set;
        }
        //RADIOACTIVE AMOUNT (MICROCURIES)
        public String RAD_AMOUNT_MICRO {
            get;
            set;
        }
        //RADIOACTIVE FORM
        public String RAD_FORM {
            get;
            set;
        }
        //RADIOISOTOPE CAS
        public String RAD_CAS {
            get;
            set;
        }
        //RADIOISOTOPE NAME
        public String RAD_NAME {
            get;
            set;
        }
        //RADIOISOTOPE SYMBOL
        public String RAD_SYMBOL {
            get;
            set;
        }
        //REPLACEMENT NSN
        public String REP_NSN {
            get;
            set;
        }
        //SEALED
        public String SEALED {
            get;
            set;
        }
    }

    //MSDS TRANSPORTATION
    public class MSDSTransportation {
        //AF MMAC CODE
        public String AF_MMAC_CODE {
            get;
            set;
        }
        //CERTIFICATE OF EQUIVALENCY
        public String CERTIFICATE_COE {
            get;
            set;
        }
        //COMPETENT AUTHORITY APPROVAL
        public String COMPETENT_CAA {
            get;
            set;
        }
        //DOD ID CODE
        public String DOD_ID_CODE {
            get;
            set;
        }
        //DOT EXEMPTION NUMBER
        public String DOT_EXEMPTION_NO {
            get;
            set;
        }
        //DOT RQ INDICATOR
        public String DOT_RQ_IND {
            get;
            set;
        }
        //EX NUMBER
        public String EX_NO {
            get;
            set;
        }
        //FLASH PT TEMP (C)
        public int FLASH_PT_TEMP {
            get;
            set;
        }
        //HCC
        public String HCC {
            get;
            set;
        }
        //HIGH EXPLOSIVE WEIGHT
        public int HIGH_EXPLOSIVE_WT {
            get;
            set;
        }
        //LTD QTY INDICATOR
        public String LTD_QTY_IND {
            get;
            set;
        }
        //MAGNETIC INDICATOR
        public String MAGNETIC_IND {
            get;
            set;
        }
        //MAGNETISM (MAGNETIC STRENGTH)
        public String MAGNETISM {
            get;
            set;
        }
        //MARINE POLLUTANT INDICATOR
        public String MARINE_POLLUTANT_IND {
            get;
            set;
        }
        //NET EXPLOSIVE QTY. DISTANCE WEIGHT
        public int NET_EXP_QTY_DIST {
            get;
            set;
        }
        //NET EXPLOSIVE WEIGHT (KG)
        public String NET_EXP_WEIGHT {
            get;
            set;
        }
        //NET PROPELLANT WEIGHT (KG)
        public String NET_PROPELLANT_WT {
            get;
            set;
        }
        //NOS TECHNICAL SHIPPING NAME
        public String NOS_TECHNICAL_SHIPPING_NAME {
            get;
            set;
        }
        //TRANSPORTATION ADDITIONAL DATA
        public String TRANSPORTATION_ADDITIONAL_DATA {
            get;
            set;
        }
        public string AFJM_PSN_CODE {
            get;
            set;
        }
        public string DOT_PSN_CODE {
            get;
            set;
        }
        public string IATA_PSN_CODE {
            get;
            set;
        }
        public string IMO_PSN_CODE {
            get;
            set;
        }
        public long afjm_psn_id {
            get;
            set;
        }
        public long dot_psn_id {
            get;
            set;
        }
        public long iata_psn_id {
            get;
            set;
        }
        public long imo_psn_id {
            get;
            set;
        }
    }
}
