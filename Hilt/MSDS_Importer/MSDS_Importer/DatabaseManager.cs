﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MSDS_Importer {
    class DatabaseManager {
        private string m_connString = "";

        #region Constructors
        public DatabaseManager() {
            m_connString = Properties.Settings.Default.ConnectionInfo;
        }

        public DatabaseManager(string connString) {
            m_connString = connString;
        }
        #endregion

        public String testConnection(String connString) {
            String connectMsg = "TEST COMPLETED SUCCESSFULLY";

            // Create a connection
            SqlConnection conn = new SqlConnection(connString);
            try {
                // Try to open it
                conn.Open();
            }
            catch (Exception ex) {
                connectMsg = ex.Message;
            }
            finally {
                conn.Close();
            }

            return connectMsg;
        }

        private static object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            }
            else {
                return DBNull.Value;
            }
        }
    }
}
