﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDS_Importer.DataObjects;

namespace MSDS_Importer.DatabaseManagers {
    /// <summary>
    /// Defines the base methods for the system-specific database managers.
    /// Provides common properties and methods.
    /// </summary>
    public abstract class BaseDatabaseManager {
        #region Abstract Methods
        /// <summary>
        /// Inserts a new MSDS record into the target database
        /// </summary>
        /// <param name="record">MSDSRecord object containing the new record data</param>
        /// <returns>ID of the new record in the database</returns>
        public abstract long insertMSDSMasterRecord(MSDSRecord record);

        /// <summary>
        /// updates the current MSDS record in the target database
        /// </summary>
        /// <param name="record">MSDSRecord object containing the updated record data</param>
        /// <returns>ID of the new record in the database</returns>
        public abstract long updateMSDSMasterRecord(MSDSRecord record);
        #endregion

        # region Implemented Properties
        /// <summary>
        /// Provides the current error message
        /// </summary>
        private String m_ErrorMsg = null;
        public string ErrorMsg {
            get {
                return m_ErrorMsg;
            }
            set {
                m_ErrorMsg = value;
            }
        }
        #endregion

        #region Implemented Methods
        protected static object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            }
            else {
                return DBNull.Value;
            }
        }
        #endregion
    }
}
