﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MSDS_Importer {
    public partial class MSDSImportForm : Form {
        const string DATE_TIME24_FORMAT = "{0:MM/dd/yyyy HHmm}";
        const string INT_FORMAT = "{0:#,##0}";
        const string LINE_60_STARS = 
                "************************************************************";
        protected int m_tabTwips = 360;

        private bool m_processing = false;
        private bool m_doPreview = false;

        long m_ProgressBarMaxValue = 100;

        public MSDSImportForm() {
            InitializeComponent();
        }

        private void MSDSImportForm_Load(object sender, EventArgs e) {
            dateStatusLabel.Text = string.Format(DATE_TIME24_FORMAT, DateTime.Now);
        }

        private void dateTimer_Tick(object sender, EventArgs e) {
            dateStatusLabel.Text = string.Format(DATE_TIME24_FORMAT, DateTime.Now);
        }

        #region Menu Events
        private void setupToolStripMenuItem_Click(object sender, EventArgs e) {
            // sow the setup form
            SetupForm form = new SetupForm();
            form.ShowDialog(this);
        }
        #endregion

        #region Button Events
        private void fileButton_Click(object sender, EventArgs e) {
            // CLear any exsting messages
            messageStatusLabel.Text = "Ready";

            // Display the directory dialog to select the directory containing the MSDS spreadsheets
            FolderBrowserDialog browseDialog = new FolderBrowserDialog();

            if (browseDialog.ShowDialog() == DialogResult.OK) {
                msdsDirectoryTextBox.Text = browseDialog.SelectedPath;
            }
        }

        private void importButton_Click(object sender, EventArgs e) {
            setCursors(Cursors.WaitCursor);
            resetMessageBox();

            DateTime dtStart = DateTime.Now;
            long nStart = Environment.TickCount;

            // Execute import as a background thread
            // Clear things out
            messageStatusLabel.Text = "Processing Data...";
            progressBar.Value = 0;
            messageRTBox.Text = "";
            cancelButton.Text = "&Cancel";

            m_processing = true;
            m_doPreview = false;
            mainWorker.RunWorkerAsync();

            while (mainWorker.IsBusy) {
                Application.DoEvents();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            if (m_processing) {
                // Make sure the user really wants to cancel
                DialogResult response = MessageBox.Show(this,
                    "This will cancel the MSDS import"
                    + "\nAre you sure you want to cancel the import?",
                    "MSDS Import Cancellation Pending", MessageBoxButtons.YesNo);

                if (response == DialogResult.Yes)
                    // Cancel the background task
                    mainWorker.CancelAsync();
            }
            else {
                // Clear the data and reset the status message 
                messageRTBox.Text = "";
                messageStatusLabel.Text = "Ready";
                progressBar.Value = 0;
                setCursors(Cursors.Default);
            }
        }

        private void previewButton_Click(object sender, EventArgs e) {
            setCursors(Cursors.WaitCursor);
            resetMessageBox();

            DateTime dtStart = DateTime.Now;
            long nStart = Environment.TickCount;

            // Execute import as a background thread
            // Clear things out
            messageStatusLabel.Text = "Previewing Data...";
            progressBar.Value = 0;
            messageRTBox.Text = "";
            cancelButton.Text = "&Cancel";

            m_processing = true;
            m_doPreview = true;
            mainWorker.RunWorkerAsync();

            while (mainWorker.IsBusy) {
                Application.DoEvents();
            }
        }
        #endregion

        #region Worker methods
        private void mainWorker_DoWork(object sender, DoWorkEventArgs e) {
            try {
                if (m_doPreview) {
                    // Generate the preview info
                    previewData();
                }
                else {
                    // Run the import
                        processImport();
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("mainWorker Error: " + ex.Message);
            }
        }

        private void mainWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Update the value of the ProgressBar and the count label to the BackgroundWorker progress.
            countStatusLabel.Text = String.Format(INT_FORMAT, e.ProgressPercentage);

            // change the count to a percentage for the progress bar
            double percent = (e.ProgressPercentage * 100.0) / m_ProgressBarMaxValue;
            progressBar.Value = (int) percent;

            if (e.UserState != null) {
                String msg = e.UserState.ToString();
                // See whether we write this to the message text box, or the status bar
                if (msg.StartsWith("SB_")) {
                    messageStatusLabel.Text = msg.Substring(3);
                }
                else {
                    messageRTBox.Text += msg + "\n";
                }
            }
        }

        private void mainWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // Clean things up
            m_processing = false;
            messageStatusLabel.Text = "Ready";
            progressBar.Value = 0;
            setCursors(Cursors.Default);
            cancelButton.Text = "&Clear";
        }
        #endregion

        private void processImport() {
            // Call the MSDS processImportLocal method to process the click event
            MSDSDataImporter importer = new MSDSDataImporter();
            if (shims1RadioButton.Checked)
                importer.processImportLocal(Properties.Settings.Default.SHIMSFileName,
                    MSDSDataImporter.ImportTarget.Shims_v1,
                    null,
                    mainWorker);
            else if (hilt1RadioButton.Checked)
                importer.processImportLocal(msdsDirectoryTextBox.Text,
                    MSDSDataImporter.ImportTarget.Hilt_v1,
                    Properties.Settings.Default.ConnectionInfo,
                    mainWorker);
            else if (hilt2RadioButton.Checked)
                importer.processImportLocal(msdsDirectoryTextBox.Text,
                    MSDSDataImporter.ImportTarget.Hilt_v2,
                    Properties.Settings.Default.ConnectionInfo,
                    mainWorker);
        }

        #region Preview
        private void previewData() {
            StringBuilder sb = new StringBuilder();
            // Get the MSDS.xls file - read the number of unique MSDS entries & number of doc files
            Dictionary<string, int> fileCounts = MSDSDataImporter.getMSDSStats(
                    msdsDirectoryTextBox.Text);

            sb.AppendLine(LINE_60_STARS);
            sb.AppendLine("MSDS Preview - " + msdsDirectoryTextBox.Text + " @ " 
                + String.Format(DATE_TIME24_FORMAT, DateTime.Now));
            sb.AppendLine("MSDS Files:");
            sb.Append("\tMSDS Records: ");
            sb.AppendLine(String.Format(INT_FORMAT, fileCounts["MSDSSheets"]));
            sb.Append("\tDocuments: ");
            sb.AppendLine(String.Format(INT_FORMAT, fileCounts["DocumentLinks"]));
            sb.AppendLine("\nFolder Contents:");
            sb.Append("\tValue Added Sheets: ");
            sb.AppendLine(String.Format(INT_FORMAT, fileCounts["ValueAddedSheets"]));
            sb.Append("\tDocument Files: ");
            sb.AppendLine(String.Format(INT_FORMAT, fileCounts["Documents"]));
            sb.AppendLine("\n" + LINE_60_STARS);

            mainWorker.ReportProgress(0, sb.ToString());
        }
        #endregion

        #region utilities
        // Clears the RTF message box and resets the default tab size
        protected void resetMessageBox() {
            messageRTBox.Clear();
            System.Diagnostics.Debug.WriteLine("Before: " + messageRTBox.Rtf);
            messageRTBox.Rtf = messageRTBox.Rtf.Replace("eflang1033",
                "eflang1033\\deftab" + m_tabTwips.ToString());
            System.Diagnostics.Debug.WriteLine("After : " + messageRTBox.Rtf);

            Application.DoEvents();
        }

        static protected internal string formatNumberThousands(int num) {
            return String.Format("{0:#,##0}", num);
        }

        private void setCursors(Cursor cursor) {
            Cursor = cursor;
            messageRTBox.Cursor = cursor;
        }
        #endregion
    }
}
