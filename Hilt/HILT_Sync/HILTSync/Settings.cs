﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILTSync
{
    public class Settings
    {
        private string wSUrl;

        private string handheldTargetDir;

        public string HandheldTargetDir
        {
            get { return handheldTargetDir; }
            set { handheldTargetDir = value; }
        }

        public string WebServiceUrl
        {
            get { return wSUrl; }
            set { wSUrl = value; }
        }
        private string localStorePath;

        public string LocalStorePath
        {
            get { return localStorePath; }
            set { localStorePath = value; }
        }
        
    }
}
