﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace HILTSync
{
    public partial class StatusRow : UserControl
    {       
        public Label NameLabel
        {
            get { return this.lblName; }
        }

        public ProgressBar ProgressBar
        {
            get { return this.progressBar; }
        }

        public Label StatusLabel
        {
            get { return this.lblStatus; }
        }

        public StatusRow()
        {
            InitializeComponent();
        }

        delegate void UpdateNameDelegate(string name);
        public void UpdateName(string name)
        {
            if (this.InvokeRequired)
            {
                Invoke(new UpdateNameDelegate(this.UpdateName), new object[] { name });
                return;
            }

            this.NameLabel.Text = name;
        }

        delegate void UpdateStatusDelegate(string status);
        public void UpdateStatus(string status)
        {
            if (this.InvokeRequired)
            {
                Invoke(new UpdateNameDelegate(this.UpdateStatus), new object[] { status });
                return;
            }

            this.StatusLabel.Text = status;
        }

        delegate void UpdateProgressDelegate(int percent);
        public void UpdateProgress(int percent)
        {
            if (this.InvokeRequired)
            {
                Invoke(new UpdateProgressDelegate(this.UpdateProgress), new object[] { percent });
                return;
            }

            this.progressBar.Value = percent;
        }

        delegate void UpdateEnabledDelegate(bool enabled);
        public void UpdateEnabled(bool enabled)
        {
            if (this.InvokeRequired)
            {
                Invoke(new UpdateEnabledDelegate(this.UpdateEnabled), new object[] { enabled });
                return;
            }

            this.Enabled = enabled;
        }

        delegate void UpdateProgressBarVisibilityDelegate(bool visible);
        public void UpdateProgressBarVisibility(bool visible)
        {
            if (this.InvokeRequired)
            {
                Invoke(new UpdateProgressBarVisibilityDelegate(this.UpdateProgressBarVisibility), new object[] { visible });
                return;
            }

            this.progressBar.Visible = visible;
        }
    }
}
