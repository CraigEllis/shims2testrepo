﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.Desktop.Communication;
using System.Collections;

using System.Security.Principal;
using System.Web.Services.Protocols;
using System.Net;
using System.Threading;
using System.Web.Services;

namespace HILTSync
{
    public partial class FrmMain : Form
    {
        private RAPI rapi;
        private DatabaseManager manager;

        // needed operations from web services:
        // * get a list of clipboards
        // * download any clipboard by ID
        // * upload any clipboard
        public FrmMain()
        {
            String personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            this.manager = new DatabaseManager(personalFolder + "\\HILT Hub\\hub.s3db");
            InitializeComponent();
            //this.PopulateSyncStatus();
        }

        private void DownloadFile(object o)
        {
            DownloadParameter dp = (DownloadParameter)o;

            FileTransferDownload ftd = new FileTransferDownload(manager.GetSettings());

            this.downloadProgressTable.Add(ftd, dp.StatusRow);
            ftd.Guid = dp.Guid;
           
            ftd.AutoSetChunkSize = true;

            Settings settings = manager.GetSettings();
            ftd.LocalSaveFolder = settings.LocalStorePath;

            ftd.IncludeHashVerification = false;
            ftd.ProgressChanged += new ProgressChangedEventHandler(ftd_ProgressChanged);
            ftd.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ftd_RunWorkerCompleted);
            ftd.RunWorkerSync(new DoWorkEventArgs(dp));
        }

        void ftd_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           
        }

        void ftd_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            FileTransferDownload ftd = (FileTransferDownload)sender;

            StatusRow r = (StatusRow)this.downloadProgressTable[ftd];
            r.UpdateProgress(e.ProgressPercentage);
        }

        private bool inProgress = false;

        delegate void UpdateStatusDelegate(string status);
        public void UpdateStatus(string status)
        {
            if (this.InvokeRequired)
            {
                Invoke(new UpdateStatusDelegate(this.UpdateStatus), new object[] { status });
                return;
            }

            this.lblStatus.Text = status;
            if (status.ToLower().Contains("progress"))
            {
                inProgress = true;
            }
            else if (status.Contains("Error"))
            {
               // MessageBox.Show(status, "Error");
                inProgress = false;
                this.backgroundWorker1.WorkerSupportsCancellation = true;
                this.backgroundWorker1.CancelAsync();
            }

            else
            {
                inProgress = false;
            }
            
        }

        private Hashtable uploadProgressTable = new Hashtable();
        private Hashtable downloadProgressTable = new Hashtable();

        private void UploadFile(object o)
        {
            UploadParameter up = (UploadParameter)o;
            string guid = up.Clipboard.Guid;
            string path = up.Clipboard.DTFilename;
            StatusRow row = up.StatusRow;
           
            FileTransferUpload ftu = new FileTransferUpload(manager.GetSettings());

            uploadProgressTable.Add(ftu, row);

            ftu.Guid = guid;

            ftu.AutoSetChunkSize = true;
      
            // set the remote file name and start the background worker
            ftu.LocalFilePath = path;
            //ftu.IncludeHashVerification = true;
            ftu.ProgressChanged += new ProgressChangedEventHandler(ftu_ProgressChanged);
            ftu.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ftu_RunWorkerCompleted);
            ftu.RunWorkerSync(new DoWorkEventArgs(up));
        }

        void ftu_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        { 
            string guid = (sender as FileTransferBase).Guid;
            if (e.Error != null)
            {
               // this.taskPanel1.EndOperation(guid, e.Error);
                return;
            }
            else if (e.Cancelled)
            {
               // this.taskPanel1.EndOperation(guid, new Exception("Cancelled"));
                return;
            }
            else
            {
             //   this.taskPanel1.EndOperation(guid, null);
            //    if (this.taskPanel1.List.Count == 0)	// play a ding on last item
           //         Ding();
            }
        }

        void ftu_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            FileTransferUpload ftu = (FileTransferUpload)sender;

            StatusRow r = (StatusRow)this.uploadProgressTable[ftu];
            r.UpdateProgress(e.ProgressPercentage);
   
            //         this.taskPanel1.ProgressChanged((sender as FileTransferBase).Guid, e.ProgressPercentage, e.UserState.ToString());
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            this.rapi = new RAPI();
            HHFileScanner scanner = new HHFileScanner(this.manager, this.pnlStatus);
            Settings settings = manager.GetSettings();

            string device_ID = scanner.GetDeviceID(settings, rapi);

            //check local clipboard store path
            bool found = this.manager.checkLocalClipboardStorePath();

            if (!found)
            {
                MessageBox.Show("The specified local Clipboard store path does not exist. Please specify a valid path and try again.");
                return;
            }

            if (backgroundWorker1.IsBusy)
            {
                MessageBox.Show("Synchronization is currently in progress..");
                return;
            }

            //check the connection to the web service
            //string url = this.manager.getWebserviceUrl();
            HILTTransfer.HILTTransfer c = new HILTTransfer.HILTTransfer();
//            Settings settings = manager.GetSettings();
            try
            {
                c.Url = settings.WebServiceUrl;
                c.Credentials = System.Net.CredentialCache.DefaultCredentials;
                string success = c.getName();
                c.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to connect to the Web Service. " + ex.Message + " Please check your settings and try again.","Error");
                return;
            }

            
          
//            this.rapi = new RAPI();                
            backgroundWorker1.RunWorkerAsync();
                                            

            /*
            MTOM.HILTTransfer.HILTTransfer c = new MTOM.HILTTransfer.HILTTransfer();
            c.Credentials = System.Net.CredentialCache.DefaultCredentials;
            string[] list = c.GetFilesList();
            */

            /*
            DatabaseManager mngr = new DatabaseManager(@"C:\hazmat-california\HAZMAT_HUB\HILTHub\HILTHub\hub.s3db");
            Settings settings = mngr.GetSettings();
            */
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!inProgress)
            {
                using (RAPI rapi = new RAPI())
                {
                    if (rapi.DevicePresent)
                    {
                        this.lblStatus.Text = "Connection Status: Connected";
                        this.lblStatus.ForeColor = Color.Green;
                        this.btnSync.Enabled = true;
                    }
                    else
                    {
                        this.lblStatus.Text = "Connection Status: Disconnected";
                        this.lblStatus.ForeColor = Color.Red;
                        this.btnSync.Enabled = false;
                    }
                }
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
            pnlStatus.ClearStatus();

            this.UpdateStatus("Synchronization in progress, please do not disconnect device...");

            StatusRow scanRow = pnlStatus.AddOperation("Scanning Device", "Looking for clipboards (this may take several minutes)...", false);
            
            HHFileScanner scanner = new HHFileScanner(this.manager, this.pnlStatus);

            List<Clipboard> clipboardList = null;
            try
            {
                clipboardList = scanner.GetClipboards(this.rapi, scanRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error copying files", MessageBoxButtons.OK, MessageBoxIcon.Error);
                pnlStatus.ClearStatus();
                this.inProgress = false;
                return;
            }
            scanRow.UpdateStatus("Complete");
            scanRow.UpdateEnabled(false);
   

            Hashtable upThreadTable = new Hashtable();
            List<Thread> upThreads = new List<Thread>();
            // upload 
            foreach (Clipboard cb in clipboardList)
            {
                UploadParameter up = new UploadParameter();
                up.Clipboard = cb;
                StatusRow upRow = pnlStatus.AddOperation(up.Clipboard.Name, "Uploading to HILT server...", true);
                up.StatusRow = upRow;

                // ThreadPool.QueueUserWorkItem(new WaitCallback(this.UploadFile), up);
                Thread t = new Thread(new ParameterizedThreadStart(this.UploadFile));
                upThreadTable.Add(t, upRow);
                t.Start(up);
                t.Join();
                upThreads.Add(t);
            }

            foreach (Thread t in upThreads)
            {
                t.Join();
                StatusRow row = (StatusRow)upThreadTable[t];
                row.UpdateStatus("Upload Complete");
                row.UpdateEnabled(false);
            }
            
            List<Thread> downThreads = new List<Thread>();

            // download
            int[] decantList;
            int[] auditList;
            int[] recurringAuditList;

            HILTTransfer.HILTTransfer c = new HILTTransfer.HILTTransfer();
            Settings settings = manager.GetSettings();           
            c.Url = settings.WebServiceUrl;                       
            c.Credentials = System.Net.CredentialCache.DefaultCredentials;
         
            List<Clipboard> downloadedClipboards = new List<Clipboard>();
            //// offload
            //{
            //    StatusRow rOffload = this.pnlStatus.AddOperation("Downloading Offload Clipboard", "Downloading...", true);


            //    DownloadParameter dp = new DownloadParameter();
            //    dp.Start = DateTime.Today;
            //    dp.End = DateTime.Today.AddDays(7);
            //    dp.StatusRow = rOffload;
            //    dp.DownloadedClipboards = downloadedClipboards;
            //    dp.Guid = System.Guid.NewGuid().ToString();
            //    dp.ID = 0;
            //    dp.Type = HILTTransfer.ClipboardType.Offload;

            //    //ThreadPool.QueueUserWorkItem(new WaitCallback(this.UploadFile), up);
            //    Thread t = new Thread(new ParameterizedThreadStart(this.DownloadFile));
            //    t.Start(dp);
            //    t.Join();
            //    rOffload.UpdateStatus("Complete");
            //    rOffload.UpdateEnabled(false);
            //}
            
            // INSURV
            {
                StatusRow rInsurv = this.pnlStatus.AddOperation("Downloading INSURV Clipboard", "Downloading...", true);

                
                DownloadParameter dp = new DownloadParameter();
                dp.Start = DateTime.Today;
                dp.End = DateTime.Today.AddDays(7);
                dp.StatusRow = rInsurv;
                dp.DownloadedClipboards = downloadedClipboards;
                dp.Guid = System.Guid.NewGuid().ToString();
                dp.ID = 0;
                dp.Type = HILTTransfer.ClipboardType.INSURV;

                //ThreadPool.QueueUserWorkItem(new WaitCallback(this.UploadFile), up);
                Thread t = new Thread(new ParameterizedThreadStart(this.DownloadFile));
                t.Start(dp);
                t.Join();
                rInsurv.UpdateStatus("Complete");
                rInsurv.UpdateEnabled(false);
            }
            
            //decanting
            int idx = 1;          
            decantList = c.getDecantingList(DateTime.Today.AddDays(-360), DateTime.Today);
            foreach (int i in decantList)
            {
                StatusRow rDecant = this.pnlStatus.AddOperation("Downloading Decanting Clipboard " + idx++, "Downloading...", true);

                string fileName = c.getDecantingFileNameById(i);
               

                DownloadParameter dp = new DownloadParameter();
                dp.End = DateTime.Today;
                dp.Start = DateTime.Today.AddDays(-7);
                dp.StatusRow = rDecant;
                dp.DownloadedClipboards = downloadedClipboards;
                dp.Guid = System.Guid.NewGuid().ToString();
                dp.ID = i;
                dp.Type = HILTTransfer.ClipboardType.Decanting;
                dp.FileName = fileName;

                //ThreadPool.QueueUserWorkItem(new WaitCallback(this.UploadFile), up);
                Thread t = new Thread(new ParameterizedThreadStart(this.DownloadFile));
                t.Start(dp);
                downThreads.Add(t);
                t.Join();
                rDecant.UpdateStatus("Complete");
                rDecant.UpdateEnabled(false);
               // break;
            }
            foreach (Thread t in downThreads)
            {
             
            }

            downThreads.Clear();

            

            idx = 1;
            auditList = c.getAuditList(DateTime.Today, DateTime.Today.AddDays(7));
            foreach (int i in auditList)
            {
                StatusRow rAudit = this.pnlStatus.AddOperation("Downloading Audit Clipboard " + idx++, "Downloading...", true);

                string fileName = c.getAuditNameById(i);
                

                DownloadParameter dp = new DownloadParameter();
                dp.Start = DateTime.Today;
                dp.End = DateTime.Today.AddDays(7);
                dp.StatusRow = rAudit;
                dp.DownloadedClipboards = downloadedClipboards;
                dp.Guid = System.Guid.NewGuid().ToString();
                dp.ID = i;
                dp.Type = HILTTransfer.ClipboardType.Audit;
                dp.FileName = fileName;

                //ThreadPool.QueueUserWorkItem(new WaitCallback(this.UploadFile), up);
                Thread t = new Thread(new ParameterizedThreadStart(this.DownloadFile));
                t.Start(dp);
                downThreads.Add(t);
                t.Join();
                rAudit.UpdateStatus("Complete");
                rAudit.UpdateEnabled(false);
              //  break;
            }
            foreach (Thread t in downThreads)
            {
            //    t.Join();
            }

            downThreads.Clear();

            idx = 1;
            recurringAuditList = c.getRecurringList(DateTime.Today, DateTime.Today.AddDays(7));
            foreach (int i in recurringAuditList)
            {
                StatusRow rAudit = this.pnlStatus.AddOperation("Downloading Recurring Clipboard " + idx++, "Downloading...", true);

                string fileName = c.getRecurringNameById(i);
               
                DownloadParameter dp = new DownloadParameter();
                dp.Start = DateTime.Today;
                dp.End = DateTime.Today.AddDays(7);
                dp.StatusRow = rAudit;
                dp.DownloadedClipboards = downloadedClipboards;
                dp.Guid = System.Guid.NewGuid().ToString();
                dp.ID = i;
                dp.Type = HILTTransfer.ClipboardType.RecurringAudit;
                dp.FileName = fileName;

                //ThreadPool.QueueUserWorkItem(new WaitCallback(this.UploadFile), up);
                Thread t = new Thread(new ParameterizedThreadStart(this.DownloadFile));
                t.Start(dp);
                downThreads.Add(t);
                t.Join();
                rAudit.UpdateStatus("Complete");
                rAudit.UpdateEnabled(false);
                // break;
            }


            int decantIdx = 1;
            int auditIdx = 1;

            
            // copy all data to the handheld
           
            if (rapi.DevicePresent)
            {
                rapi.Connect(true);

                foreach (Clipboard cb in downloadedClipboards)
                {
                    cb.HHFilename = settings.HandheldTargetDir + "\\";
                    
                    // create name
                    if (cb.Type == Clipboard.ClipboardType.Audit)
                    {
                        //cb.Name = "Audit Clipboard " + auditIdx++ + ".audit";
                        cb.Name = cb.Name + "_" + auditIdx++ + ".audit";
                        cb.HHFilename += cb.Name;
                    }
                    else if (cb.Type == Clipboard.ClipboardType.Decanting)
                    {
                        //cb.Name = "Decanting Clipboard " + decantIdx++ + ".decant";
                        cb.Name = cb.Name + "_" + decantIdx++ + ".decant";
                        cb.HHFilename += cb.Name;
                    }
                    else if (cb.Type == Clipboard.ClipboardType.INSURV)
                    {
                        cb.Name = "InsurvClipboard.insurv";
                        cb.HHFilename += cb.Name;
                    }
                    else if (cb.Type == Clipboard.ClipboardType.Offload)
                    {
                        cb.Name = "OffloadClipboard.offload";
                        cb.HHFilename += cb.Name;
                    }
                   // else if (cb.Type == Clipboard.ClipboardType.Receiving)
                   // {
                   //     cb.Name = "Insurv Clipboard.insurv";
                  //  }

                    rapi.CopyFileToDevice(cb.DTFilename, cb.HHFilename);
                }

                // update status
                this.UpdateStatus("Synchronization complete");

                pnlStatus.AddOperation("Synchronization Complete", "", false);

                rapi.Disconnect();
            }
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (inProgress)
            {
                DialogResult r = MessageBox.Show("Synchronization in progress. Are you sure you want to exit?", "Synchronization in Progress", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (r == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm sForm = new SettingsForm(this.manager);
            sForm.ShowDialog();
        }

    }
}
