﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenNETCF.Desktop.Communication;
using System.IO;
using System.Security.Cryptography;
using System.Xml;

namespace HILTSync
{
    public class HHFileScanner
    {
        private DatabaseManager manager;
        private StatusPanel pnlStatus;

        public HHFileScanner(DatabaseManager manager, StatusPanel pnlStatus)
        {
            this.manager = manager;
            this.pnlStatus = pnlStatus;
        }

        public string GetDeviceID(Settings settings, RAPI rapi)
        {
            string tempFilePath = System.IO.Path.GetTempFileName();
            string targetFilePath = settings.HandheldTargetDir + "\\xml\\hhConfig.xml";
            rapi.CopyFileFromDevice(tempFilePath, targetFilePath);

            // read XML file, get device ID
            XmlDocument doc = new XmlDocument();
            doc.Load(tempFilePath);

            XmlElement s = doc["mobile_settings"];
            XmlElement d = s["device_id"];
            if (d == null)
            {
                return null;
            }
            return d.InnerText;
        }

        private void Search(FileInformation fi, RAPI rapi, string dir, List<Clipboard> clipboardList, StatusRow scanRow)
        {
            //rapi.GetDeviceSystemInfo
            //scanRow.UpdateStatus("Scanning " + dir + "\\" + fi.FileName);
            Console.WriteLine("Scanning " + dir + "\\" + fi.FileName);
            try
            {
                FileList list = rapi.EnumFiles(dir + "\\" + fi.FileName + @"\*");
                if (list != null)
                {
                    foreach (FileInformation i in list)
                    {
                        if (i.FileAttributes == 16 || fi.FileAttributes == 272)
                        {
                            Search(i, rapi, dir + "\\" + fi.FileName, clipboardList, scanRow);
                        }
                        else
                        {
                            if (IsClipboard(i))
                            {
                                Clipboard cb = new Clipboard();
                                cb.HHFilename = dir + "\\" + fi.FileName + "\\" + i.FileName;
                                cb.Type = this.GetType(i);
                                cb.Name = i.FileName;
                                clipboardList.Add(cb);
                                pnlStatus.AddOperation("Found " + cb.Name + ";;", "test", false);
                            }
                            else
                            {
                               
                                Console.WriteLine("Scanning " + dir + "\\" + fi.FileName + "\\" + i.FileName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private Clipboard.ClipboardType GetType(FileInformation fi)
        {
            if (fi.FileName.ToLower().EndsWith("decant"))
            {
                return Clipboard.ClipboardType.Decanting;
            }
            else if (fi.FileName.ToLower().EndsWith("receive"))
            {
                return Clipboard.ClipboardType.Receiving;
            }
            else if (fi.FileName.ToLower().EndsWith("audit"))
            {
                return Clipboard.ClipboardType.Audit;
            }
            else if (fi.FileName.ToLower().EndsWith("offload"))
            {
                return Clipboard.ClipboardType.Offload;
            }
            // insurv
            else 
            {
                return Clipboard.ClipboardType.INSURV;
            }
        }

        private bool IsClipboard(FileInformation fi)
        {
            if (fi.FileName.ToLower().EndsWith("decant") ||
                fi.FileName.ToLower().EndsWith("receive") ||
                fi.FileName.ToLower().EndsWith("audit") ||
                fi.FileName.ToLower().EndsWith("offload") ||
                fi.FileName.ToLower().EndsWith("insurv"))
            {
                return true;
            }
            return false;
        }

        protected string GetMD5HashFromFile(string fileName)
        {
            FileStream file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void StoreLocally(DatabaseManager manager, List<Clipboard> clipboardList, Settings settings, RAPI rapi, string deviceID)
        {            
            // get settings for local store
            string localStorePath = settings.LocalStorePath;

            foreach (Clipboard cb in clipboardList)
            {
                //   copy to the local store
                cb.Guid = System.Guid.NewGuid().ToString();
                cb.DTFilename = localStorePath + "\\" + cb.Guid;

                try
                {
                    rapi.CopyFileFromDevice(cb.DTFilename, cb.HHFilename);
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to copy file " + cb.HHFilename + " from the device. Ensure the HILT program is not running and the file is not locked.");
                }
                // get the hash
                cb.MD5Hash = this.GetMD5HashFromFile(cb.DTFilename);

                // have we seen this hash in the past? If so, discard the file and move on
                if (manager.HashUnique(cb.MD5Hash))
                {
                    // insert into database
                    manager.RecordHistory(cb, deviceID);
                }

            }
            foreach(Clipboard cb in clipboardList)
            {
                // delete file from handheld
                try
                {
                    rapi.DeleteDeviceFile(cb.HHFilename);
                }
                catch (Exception ex)
                {
                 
                }
            }
        }

        public List<Clipboard> GetClipboards(RAPI rapi, StatusRow scanRow)
        {
            Settings settings = manager.GetSettings();

            List<Clipboard> clipboardList = new List<Clipboard>();

            {
                if (rapi.DevicePresent)
                {
                    rapi.Connect(true);

                    string deviceID = this.GetDeviceID(settings, rapi);
                    if (deviceID == null)
                    {
                        throw new Exception("Unable to retrieve unique device ID - ensure the HILT handheld application is installed and has been run at least once.");
                        
                    
                    }
                   
                    //FileList list = rapi.EnumFiles(@"\*");
                    FileList list = rapi.EnumFiles(settings.HandheldTargetDir + "\\*");
                    foreach (FileInformation fi in list)
                    {
                        string s = fi.FileName;
                       
                        if (fi.FileAttributes == 16 || fi.FileAttributes == 272)
                        {

                            Search(fi, rapi, settings.HandheldTargetDir, clipboardList, scanRow);
                        }
                        else
                        {
                            if (IsClipboard(fi))
                            {
                                Clipboard cb = new Clipboard();
                                cb.HHFilename =   settings.HandheldTargetDir + "\\" + fi.FileName;
                                cb.Type = this.GetType(fi);
                                cb.Name = fi.FileName;
                                clipboardList.Add(cb);
                             //   pnlStatus.AddOperation("Found " + cb.Name + ";;", "test", false);
                            }
                            else
                            {

                                Console.WriteLine("Scanning " + fi.FileName + "\\" + fi.FileName);
                            }
                        }
                    }
                   
                    this.StoreLocally(this.manager, clipboardList, settings, rapi, deviceID);

                    // rapi.CopyFileFromDevice(@"c:\temp\DECANTING16.decant", @"\Flash File Store\SSPB\DECANTING16.decant");


                    /* HILTHubWS.HILTTransferTestSoapClient c = new HILTHubWS.HILTTransferTestSoapClient();
                     FileStream fs = File.OpenRead(@"c:\temp\DECANTING16.decant");

                     byte[] bytes = new byte[fs.Length];
                     fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                     fs.Close();

                     c.PutFile(bytes);
                     */
                   // rapi.Disconnect();
                }
            }
            return clipboardList;
        }
    }
}
