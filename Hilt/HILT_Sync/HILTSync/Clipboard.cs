﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILTSync
{
    public class Clipboard
    {
        public enum ClipboardType
        {
            Audit,
            Decanting,
            INSURV,
            Receiving,
            Offload
        }

        private ClipboardType type;
        private string mD5Hash;
        private string guid;

        public string Guid
        {
            get { return guid; }
            set { guid = value; }
        }

        public string MD5Hash
        {
            get { return mD5Hash; }
            set { mD5Hash = value; }
        }

        public ClipboardType Type
        {
            get { return type; }
            set { type = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string hhFilename;

        public string HHFilename
        {
            get { return hhFilename; }
            set { hhFilename = value; }
        }
        private string dtFilename;

        public string DTFilename
        {
            get { return dtFilename; }
            set { dtFilename = value; }
        }

    }
}
