﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace HILTSync
{
    public class StatusPanel : FlowLayoutPanel
    {
        public StatusRow AddOperation(string name, string status, bool progressBar)
        {
            StatusRow row = new StatusRow();
            row.UpdateName(name);
            row.UpdateStatus(status);
            this.AddRow(row);

            if (!progressBar)
            {
                row.UpdateProgressBarVisibility(false);
            }

            return row;
        }

        public void ClearStatus()
        {
            this.ClearRows();
        }

        delegate void ClearRowsDelegate();
        private void ClearRows()
        {
            if (this.InvokeRequired)
            {
                Invoke(new ClearRowsDelegate(this.ClearRows));
                return;
            }

            this.Controls.Clear();
           
        }

        delegate void AddRowDelegate(StatusRow row);
        public void AddRow(StatusRow row)
        {
            if (this.InvokeRequired)
            {
                Invoke(new AddRowDelegate(this.AddRow), new object[] { row });
                return;
            }
           
            this.Controls.Add(row);
            this.Controls.SetChildIndex(row, 0);
            this.SetFlowBreak(row, true);
        }
    }
}
