﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILTSync
{
    public class DownloadParameter
    {
        private HILTTransfer.ClipboardType type;

        private StatusRow statusRow;

        private DateTime start;

        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }
        private DateTime end;

        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        public StatusRow StatusRow
        {
            get { return statusRow; }
            set { statusRow = value; }
        }

        public HILTTransfer.ClipboardType Type
        {
            get { return type; }
            set { type = value; }
        }
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string guid;

        public string Guid
        {
            get { return guid; }
            set { guid = value; }
        }

        private List<Clipboard> downloadedClipboards;

        public List<Clipboard> DownloadedClipboards
        {
            get { return downloadedClipboards; }
            set { downloadedClipboards = value; }
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

    }
}
