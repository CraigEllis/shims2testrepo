﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILTSync
{
    public class UploadParameter
    {
        StatusRow statusRow;

        public StatusRow StatusRow
        {
            get { return statusRow; }
            set { statusRow = value; }
        }
        Clipboard clipboard;

        public Clipboard Clipboard
        {
            get { return clipboard; }
            set { clipboard = value; }
        }
    }
}
