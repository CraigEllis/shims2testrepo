﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HILTSync
{
    public class Device
    {
        private string deviceID;

        private long? pkID;

        public long? PKID
        {
            get { return pkID; }
            set { pkID = value; }
        }

        public string DeviceID
        {
            get { return deviceID; }
            set { deviceID = value; }
        }
    }
}
