﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace HILTSync
{
    public partial class SettingsForm : Form
    {
        private DatabaseManager manager;

        public SettingsForm(DatabaseManager manager)
        {
            this.manager = manager;

            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            Settings s = manager.GetSettings();

            this.txtDeviceDir.Text = s.HandheldTargetDir;
            this.txtLocalStore.Text = s.LocalStorePath;
            this.txtWSUrl.Text = s.WebServiceUrl;

        }

        private void btnOK_Click(object sender, EventArgs e)
        {            
            Settings s = new Settings();
           
            s.HandheldTargetDir = this.txtDeviceDir.Text;
            // make sure the hh dir has a leading \
            if (!s.HandheldTargetDir.StartsWith("\\"))
            {
                s.HandheldTargetDir = "\\" + s.HandheldTargetDir;
            }
            
            s.LocalStorePath = this.txtLocalStore.Text;
            s.WebServiceUrl = this.txtWSUrl.Text;

            this.manager.StoreSettings(s);

            this.Close();
        }
    }
}
