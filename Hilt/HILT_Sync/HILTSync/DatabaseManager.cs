﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;

namespace HILTSync
{
    public class DatabaseManager
    {        
        private string connectionString = null;
        private SQLiteConnection conn = null;

        public DatabaseManager(string databaseFilePath)
        {
            Init(GetConnectionString(databaseFilePath));
        }

        ~DatabaseManager()
        {
            this.conn.Close();
        }

        private void Init(string connectionString)
        {
            this.connectionString = connectionString;
            this.conn = new SQLiteConnection(this.connectionString);
            this.conn.Open();
        }

        public Device StoreDevice(string deviceID)
        {
            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "insert into devices(device_id) values(@device_id)";
            c.Parameters.Add(new SQLiteParameter("@device_id", deviceID));
            c.ExecuteNonQuery();
            
            Device d = new Device();
            d.PKID = this.GetLastRowID();
            d.DeviceID = deviceID;
            return d;
        }

        public Device GetDevice(string deviceID)
        {
            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "select device_pk, device_id, device_name from devices where device_id = @device_id";
            c.Parameters.Add(new SQLiteParameter("@device_id", deviceID));
            SQLiteDataReader rdr = c.ExecuteReader();
            if(rdr.Read())
            {
                Device d = new Device();
                d.PKID = (long)rdr["device_pk"];
                d.DeviceID = (string)rdr["device_id"];
                rdr.Close();
                return d;
            }
            rdr.Close();

            return null;
        }

        public void RecordHistory(Clipboard cb, string deviceID)
        {
            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "insert into clipboards(clipboard_type, clipboard_name, md5_hash) values(@clipboard_type, @clipboard_name, @md5_hash);";
            c.Parameters.Add(new SQLiteParameter("@clipboard_type", cb.Type.ToString()));
            c.Parameters.Add(new SQLiteParameter("@clipboard_name", cb.Name));
            c.Parameters.Add(new SQLiteParameter("@md5_hash", cb.MD5Hash));
            c.ExecuteNonQuery();
            long? clipboardid = this.GetLastRowID();

            // does this device exist yet?
            Device d = this.GetDevice(deviceID);
            if (d == null)
            {
                d = this.StoreDevice(deviceID);
            }

            c = new SQLiteCommand(conn);
            c.CommandText = "insert into history(clipboard_id, device_id, action_time, action_type, local_store) values(@clipboard_id, @device_id, @action_time, @action_type, @local_store);";
            c.Parameters.Add(new SQLiteParameter("@clipboard_id", clipboardid));
            c.Parameters.Add(new SQLiteParameter("@device_id", d.PKID));
            c.Parameters.Add(new SQLiteParameter("@action_time", DateTime.Now));
            c.Parameters.Add(new SQLiteParameter("@action_type", "FROM_HH"));
            c.Parameters.Add(new SQLiteParameter("@local_store", cb.DTFilename));
            c.ExecuteNonQuery();
        }

        public bool HashUnique(string md5)
        {
            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "select count(*) from clipboards where md5_hash = @md5_hash";
            c.Parameters.Add(new SQLiteParameter("@md5_hash", md5));
            SQLiteDataReader rdr = c.ExecuteReader();
            rdr.Read();
            long count = (long)rdr[0];
            rdr.Close();

            if (count > 0)
            {
                return false;
            }
            return true;
        }

        private long? GetLastRowID()
        {
            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "SELECT last_insert_rowid();";
            SQLiteDataReader rdr = c.ExecuteReader();
            if (rdr.Read())
            {
                long id = (long)rdr[0];
                rdr.Close();
                return id;
            }
            rdr.Close();
            return null;
        }

        private string GetConnectionString(string databaseFilePath)
        {
            /// UNITTEST
            // does the database exist? 

            // if not, create it by copying the template over

            // return connection string

            // TODO: relative database
            return "Data Source=" + databaseFilePath + ";Version=3;";   
        }

        public void StoreSettings(Settings settings)
        {
            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "delete from settings";
            c.ExecuteNonQuery();

            c = new SQLiteCommand(conn);
            c.CommandText = "insert into settings(key, value) values(@key, @value)";

            c.Parameters.Add(new SQLiteParameter("@key", "WSURL"));
            c.Parameters.Add(new SQLiteParameter("@value", settings.WebServiceUrl));
            c.ExecuteNonQuery();

            c.Parameters.Add(new SQLiteParameter("@key", "local_store"));
            c.Parameters.Add(new SQLiteParameter("@value", settings.LocalStorePath));
            c.ExecuteNonQuery();

            c.Parameters.Add(new SQLiteParameter("@key", "hh_target_dir"));
            c.Parameters.Add(new SQLiteParameter("@value", settings.HandheldTargetDir));
            c.ExecuteNonQuery();
        }

        public string getWebserviceUrl()
        {

            string stmt = "select value from settings where key = @key";

            SQLiteCommand cmd = new SQLiteCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@key", "WSURL");
            SQLiteDataReader rdr = cmd.ExecuteReader();

            string path = "";

            while (rdr.Read())
            {

                path = rdr["value"].ToString();

                break;

            }

            cmd.Dispose();

            return path;

        }

        public bool checkLocalClipboardStorePath()
        {                                 

            string stmt = "select value from settings where key = @key";

            SQLiteCommand cmd = new SQLiteCommand(stmt, conn);
            cmd.Parameters.AddWithValue("@key", "local_store");
            SQLiteDataReader rdr = cmd.ExecuteReader();

            string path = "";

            while (rdr.Read())
            {

               path = rdr["value"].ToString();

               break;
              
            }

            cmd.Dispose();

            if (!Directory.Exists(path))
            {

                return false;

            }
            else
            {
                return true;
            }
          


        }

        public Settings GetSettings()
        {
            Settings s = new Settings();

            SQLiteCommand c = new SQLiteCommand(conn);
            c.CommandText = "select * from settings";
            SQLiteDataReader rdr = c.ExecuteReader();

            while (rdr.Read())
            {
                string key = (string)rdr["key"];
                string val = (string)rdr["value"];

                if (key.ToLower().Equals("wsurl"))
                {
                    s.WebServiceUrl = val;
                }
                else if (key.ToLower().Equals("local_store"))
                {
                    s.LocalStorePath = val;
                }
                else if (key.ToLower().Equals("hh_target_dir"))
                {
                    s.HandheldTargetDir = val;
                }
            }
            rdr.Close();

            return s;
        }
    }
}
