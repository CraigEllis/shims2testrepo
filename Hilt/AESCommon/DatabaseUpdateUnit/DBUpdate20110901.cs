﻿using System;
using System.Collections.Generic;
using System.Text;
// Required imports
using System.Data;
using System.Data.SqlClient;
using AESCommon.DatabaseUpdate;

namespace DatabaseUpdateUnit  {
    /// <summary>
    /// Adds two new columns to the db_properties table
    /// Used to test DatabaseUpdate package
    /// </summary>
    class DBUpdate20110901 : IDatabaseUpdate {
        public int AppDBVersion {
            get {
                return 20110901;
            }
        }

        public int update(IDbConnection connection) {
            SqlCommand command = ((SqlConnection) connection).CreateCommand();
            int result = 0;

            // Add two columns to the DB_Properties table
            command.CommandText = "ALTER TABLE DB_Properties " +
                    "ADD TempValue int DEFAULT 0," +
                    "TempName varchar(50) NULL";

            if (connection.State != ConnectionState.Open) {
                connection.Open();
                Console.WriteLine("\tconnection opened");
            }

            result = command.ExecuteNonQuery();

            // Add a row to the Data_Sources table
            command.CommandText = "INSERT INTO [dbo].[data_sources] " +
                "VALUES('RSUPPLY', 'RSupply')";

            result = command.ExecuteNonQuery();

            return result;
        }
 
    }
}
