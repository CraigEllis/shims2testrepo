﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace AESCommon.SafeFile {
    public static class SafeFile {
        private static string invalidFileChars = "[\\x22\\x3C\\x3E\\x7C" +
            "\\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0A\\x0B\\x0C\\x0D\\x0E\\x0F" +
            "\\x10\\x11\\x12\\x13\\x14\\x15\\x16\\x17\\x18\\x19\\x1A\\x1B\\x1C\\x1D\\x1E\\x1F" +
            "\\x3A\\x2A\\x3F\\x5C\\x2F]";
        private static string invalidPathChars = "[\\x22\\x3C\\x3E\\x7C" +
            "\\x01\\x02\\x03\\x04\\x05\\x06\\x07\\x08\\x09\\x0A\\x0B\\x0C\\x0D\\x0E\\x0F" +
            "\\x10\\x11\\x12\\x13\\x14\\x15\\x16\\x17\\x18\\x19\\x1A\\x1B\\x1C\\x1D\\x1E\\x1F]";

        /// <summary>
        /// Removes any invalid file name characters from the input string to avoid 
        /// attempts at path manipulation via the file name
        /// </summary>
        /// <param name="sIn">Input filename to make safe</param>
        /// <returns>String with all invalid file name characters removed</returns>
        public static String makeFileNameSafe(String sIn) {
            String sOut = null;
            RegexOptions options = new RegexOptions();
            options = RegexOptions.Singleline;

            try {
                // .. - attempt to move outside applicaton
                sOut = Regex.Replace(sIn, "^([.]{2}[\\/\\\\]{1,2})", "", options);
                Debug.WriteLine("sOut = " + sOut);
                // Regex to replace the invalid filename characters
                sOut = Regex.Replace(sOut, invalidFileChars, "", options);
            }
            catch (Exception ex) {
                Debug.WriteLine("\tError: " + ex.Message);
            }

            return sOut;
        }

        /// <summary>
        /// Removes any invalid path name characters from the input string to avoid 
        /// attempts at path manipulation via the path name
        /// </summary>
        /// <param name="sIn">Input pathname to make safe</param>
        /// <returns>String with all invalid path name characters removed</returns>
        public static String makePathSafe(String sIn) {
            String sOut = null;
            RegexOptions options = new RegexOptions();
            options = RegexOptions.Singleline;

            try {
                // Regex to replace the invalid pathname characters
                sOut = Regex.Replace(sIn, invalidPathChars, "", options);
            }
            catch (Exception ex) {
                Debug.WriteLine("\tError: " + ex.Message);
            }

            return sOut;
        }

        /// <summary>
        /// Checks the input filename against the list of invalid filename characters
        /// NOTE: the presence of any path information (:, \, /) in the filename will
        /// invalidate the file name
        /// </summary>
        /// <param name="fileName">File name to validate</param>
        /// <returns>true if the input contains only valid filename characters
        /// false if the input contains invalid characters</returns>
        public static Boolean isValidFileName(String fileName) {
            Boolean result = true;
            if (fileName.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) != -1)
                result = false;
            return result;
        }

        /// <summary>
        /// Checks the input pathname against the list of invalid path characters
        /// </summary>
        /// <param name="pathName">Path name to validate</param>
        /// <returns>true if the input contains only valid path characters
        /// false if the input contains invalid characters</returns>
        public static Boolean isValidPathName(String pathName) {
            Boolean result = true;
            if (pathName.IndexOfAny(System.IO.Path.GetInvalidPathChars()) != -1)
                result = false;
            return result;
        }
    }
}
