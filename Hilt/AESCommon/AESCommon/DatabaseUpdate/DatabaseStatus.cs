﻿using System;

namespace AESCommon.DatabaseUpdate {
    /// <summary>
    /// Data object class that containing the current database update status, and 
    /// DBVersion number.
    /// 
    /// Provides an enum for the database status
    /// </summary>
    public class DatabaseStatus {
        /// <summary>
        /// Enum of the database status
        /// Current - database version is current
        /// NeedsUpdate - database version is older that the current AppDBVersion 
        /// </summary>
        public enum DBStatus 
        {
            /// <summary>
            /// Database version is current
            /// </summary>
	        Current,
            /// <summary>
            /// Database version is older that the current AppDBVersion
            /// </summary>
	        NeedsUpdate
        };

        private Int32 version = 0;
        private DBStatus status = DBStatus.NeedsUpdate;

        /// <summary>
        /// Gets or Sets the current Database version
        /// </summary>
        public Int32 Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
            }
        }

        /// <summary>
        /// Gets or Sets the current database status
        /// </summary>
        public DBStatus Status {
            get {
                return this.status;
            }
            set {
                this.status = value;
            }
        }
    }
}
