﻿using System.Data;
//using System.Data.SqlClient;

namespace AESCommon.DatabaseUpdate
{
    /// <summary>
    /// Interface for database update classes - 
    /// the update function applies any changes to an applicaton's database that
    /// have been made since the last application update.
    /// 
    /// To use: create a new class in your project that implements this interface
    /// for each application release that requires a database update. 
    /// The recommended class name is:
    /// DatabaseUpdateYYYYMMDD 
    /// where YYYYMMDD is the datestamp when the changes were implemented
    /// 
    /// </summary>
    public interface IDatabaseUpdate
    {
        /// <summary>
        /// Application DB Version - the version of this updater.
        /// This is usually the date these changes were made to the database.
        /// e.g. DBVersion = 20110822
        /// </summary>
        int AppDBVersion {get;}

        /// <summary>
        /// Update method - this is where the code to update the database with 
        /// the release-specific changes go.
        /// 
        /// This method is usually called from within the application's 
        /// DatabaseUpdater class to apply the changes.
        /// </summary>
        /// <param name="connection">reference to the application database's 
        /// SQLConnection object</param>
        /// <returns></returns>
        int update(IDbConnection connection);
    }
}
