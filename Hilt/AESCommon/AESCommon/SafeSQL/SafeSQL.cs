﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace AESCommon.SafeSQL {
    public static class SafeSQL {
        public static String makeSafe(String sIn) {
            String sOut = null;
            RegexOptions options = new RegexOptions();
            options = RegexOptions.Singleline;

            try {
                // Regex to replace the evil SQL characters
                // ' - single quote replace with two single quotes
                sOut = Regex.Replace(sIn, "'", "''", options);
                // ; - semi-colon remove
                sOut = Regex.Replace(sOut, ";", "", options);
                // -- - SQL comment replace  with en dash
                sOut = Regex.Replace(sOut, "([-])\\1+", "-", options);
            }
            catch (Exception ex) {
                Debug.WriteLine("\tError: " + ex.Message);
            }

            return sOut;
        }

        public static Boolean validateSQL(String statement) {
            Boolean result = false;
            string newStmt = makeSafe(statement);

            if (newStmt.Equals(statement))
                result = true;

            return result;
        }
    }
}
