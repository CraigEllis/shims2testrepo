﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHIMS_Data_Migrator
{
    class Migrator
    {
        private SQLServerInstance instance;
        private string targetDb;
        private SqlConnection conn;
        private AccessReader reader;
        private AccessReader msdsaulReader;
        private BackgroundWorker bgWorker;

        public Migrator(SQLServerInstance instance, string targetDb, string mdbPath, string msdsaulPath, BackgroundWorker bgWorker)
        {
            this.instance = instance;
            this.targetDb = targetDb;
            this.conn = DBConn.GetServerConnection(instance);
            this.reader = new AccessReader(mdbPath);
            this.msdsaulReader = new AccessReader(msdsaulPath);
            this.bgWorker = bgWorker;
        }

        public void Migrate()
        {
            MigrateInventoryDetail();
            bgWorker.ReportProgress(10);
            MigrateWorkcenter();
            bgWorker.ReportProgress(30);
            MigrateLocation();
            bgWorker.ReportProgress(50);
            MigrateNiinCatalog();
            bgWorker.ReportProgress(70);
            MigrateInventoryOnHand();
            bgWorker.ReportProgress(90);
            MigrateShip();
            MigrateOffloadList();
            MigrateArchived();
            bgWorker.ReportProgress(100);
        }

        public void MigrateOffloadList()
        {
            var dataTable = reader.GetTableFromQuery("SELECT * FROM OFFLOAD", null, CommandType.Text);
            Dictionary<string, int> locationMap = getLocationMap();
            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the offload_list table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertOffloadList();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@fsc", (object)row[1] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@niin", (object)row[2] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@cage", (object)row[3] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@description", (object)row[4] ?? DBNull.Value);
                int? location_id = null;
                var thing = row[6].ToString();
                if (locationMap.ContainsKey(row[3].ToString()))
                    location_id = locationMap[thing];
                insertCmd.Parameters.AddWithValue("@location_id", (object)location_id ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@offload_qty", (object)row[7] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@offload_date", (object)row[9] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ui", (object)row[10] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@um", (object)row[11] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@offload_list_id", (object)row[0] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@add_data_4", (object)row[59] ?? DBNull.Value);

                insertCmd.ExecuteNonQuery();

                string insertDD1348 = "use " + targetDb + "; " + Scripts.InsertDD1348();
                insertCmd = new SqlCommand(insertDD1348, conn);
                insertCmd.Parameters.AddWithValue("@offload_list_id", (object)row[0] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@doc_ident", (object)row[12] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ri_from", (object)row[13] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@m_and_s", (object)row[14] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ser", (object)row[16] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@suppaddress", (object)row[17] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@sig", (object)row[18] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@fund", (object)row[19] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@distribution", (object)row[20] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@project", (object)row[21] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@pri", (object)row[22] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@reqd_del_date", (object)row[23] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@adv", (object)row[24] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ri", (object)row[25] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@op", (object)row[26] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@condition", (object)row[27] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@mgt", (object)row[28] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@unit_price", (object)row[29] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@total_price", (object)row[30] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ship_from", (object)row[31] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ship_to", (object)row[32] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@mark_for", (object)row[33] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@nmfc", (object)row[35] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@frt_rate", (object)row[36] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@cargo_type", (object)row[37] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ps", (object)row[38] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@qty_recd", (object)row[39] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@up", (object)row[40] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@unit_weight", (object)row[41] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@unit_cube", (object)row[42] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ufc", (object)row[43] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@sl", (object)row[44] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@item_name", (object)row[46] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ty_cont", (object)row[47] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@no_cont", (object)row[48] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@total_weight", (object)row[49] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@total_cube", (object)row[50] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@recd_by", (object)row[51] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@date_recd", (object)row[52] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@document_number", (object)row[53] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@memo_add_1", (object)row[58] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@memo_add_2", (object)row[59] ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }
        }

        public void MigrateArchived()
        {
            var dataTable = reader.GetTableFromQuery("SELECT * FROM [OFFLOAD ARCHIVE]", null, CommandType.Text);
            Dictionary<string, int> locationMap = getLocationMap();
            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the offload_list table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertOffloadArchived();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@fsc", (object)row[1] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@niin", (object)row[2] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@cage", (object)row[3] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@description", (object)row[4] ?? DBNull.Value);
                int? location_id = null;
                var thing = row[6].ToString();
                if (locationMap.ContainsKey(row[3].ToString()))
                    location_id = locationMap[thing];
                insertCmd.Parameters.AddWithValue("@location_id", (object)location_id ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@offload_qty", (object)row[7] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@offload_date", (object)row[9] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ui", (object)row[10] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@um", (object)row[11] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@offload_list_id", (object)row[0] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@add_data_4", (object)row[59] ?? DBNull.Value);

                insertCmd.ExecuteNonQuery();

                string insertDD1348 = "use " + targetDb + "; " + Scripts.InsertDD1348();
                insertCmd = new SqlCommand(insertDD1348, conn);
                insertCmd.Parameters.AddWithValue("@offload_list_id", (object)row[0] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@doc_ident", (object)row[12] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ri_from", (object)row[13] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@m_and_s", (object)row[14] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ser", (object)row[16] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@suppaddress", (object)row[17] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@sig", (object)row[18] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@fund", (object)row[19] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@distribution", (object)row[20] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@project", (object)row[21] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@pri", (object)row[22] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@reqd_del_date", (object)row[23] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@adv", (object)row[24] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ri", (object)row[25] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@op", (object)row[26] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@condition", (object)row[27] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@mgt", (object)row[28] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@unit_price", (object)row[29] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@total_price", (object)row[30] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ship_from", (object)row[31] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ship_to", (object)row[32] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@mark_for", (object)row[33] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@nmfc", (object)row[35] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@frt_rate", (object)row[36] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@cargo_type", (object)row[37] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ps", (object)row[38] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@qty_recd", (object)row[39] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@up", (object)row[40] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@unit_weight", (object)row[41] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@unit_cube", (object)row[42] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ufc", (object)row[43] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@sl", (object)row[44] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@item_name", (object)row[46] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@ty_cont", (object)row[47] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@no_cont", (object)row[48] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@total_weight", (object)row[49] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@total_cube", (object)row[50] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@recd_by", (object)row[51] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@date_recd", (object)row[52] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@document_number", (object)row[53] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@memo_add_1", (object)row[58] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@memo_add_2", (object)row[59] ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }
        }

        public void MigrateShip()
        {
            var dataTable = msdsaulReader.GetTableFromQuery("SELECT * FROM [OP09CDBF]", null, CommandType.Text);
            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the ship table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertShipSql();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@hull_type", (object)row[2] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@hull_number", (object)row[3] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@uic", (object)row[1] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@act_code", (object)row[4] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@sndl", (object)row[5] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@name", (object)row[7] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@city", (object)row[11] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@state", (object)row[12] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@base", (object)row[0] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@poc_title", (object)row[8] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@zip", (object)row[13] ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }
        }

        public void MigrateNiinCatalog()
        {
            var dataTable = msdsaulReader.GetTableFromQuery("SELECT * FROM NIINMSD", null, CommandType.Text);
            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the workcenter table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertMfgCatalogSql();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@niin", (object)row[1] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@cage", (object)row[2] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@msds_no", (object)row[3] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@manufacturer", (object)row[4] ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }
        }

        public void MigrateWorkcenter()
        {
            var dataTable = reader.GetTableFromQuery("SELECT DISTINCT [WORK CENTER] FROM [CUSTOMIZE LOCATION]", null, CommandType.Text);
            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the workcenter table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertWorkcenterSql();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@wid", (object)row[0] ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }
        }

        public void MigrateLocation()
        {
            var dataTable = reader.GetTableFromQuery("SELECT * FROM [CUSTOMIZE LOCATION]", null, CommandType.Text);

            Dictionary<string, int> workcenterMap = getWorkcenterMap();

            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the location table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertLocationSql();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@name", (object)row[1] ?? DBNull.Value);
                int? workcenter_id = null;
                var thing = row[2].ToString();
                if (workcenterMap.ContainsKey(row[2].ToString()))
                    workcenter_id = workcenterMap[thing];
                insertCmd.Parameters.AddWithValue("@workcenter_id", (object)workcenter_id ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }
        }

        public void MigrateInventoryOnHand()
        {
            var dataTable = reader.GetTableFromQuery("SELECT * FROM [INVENTORY LOCATION] WHERE LOCATION IS NOT NULL", null, CommandType.Text);
            var locationMap = getLocationMap();
            foreach (DataRow row in dataTable.Rows)
            {
                if ((object)row[1] != DBNull.Value && (object)row[2] != DBNull.Value)
                {
                    //insert into the inventory_onhand table
                    var items = row.ItemArray;
                    string insertStmt = "use " + targetDb + "; " + Scripts.InsertInventoryOnHandSql();
                    SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                    insertCmd.Parameters.AddWithValue("@fsc", (object)row[1] ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@niin", (object)row[2] ?? DBNull.Value);
                    int? location_id = null;
                    var thing = row[3].ToString();
                    if (locationMap.ContainsKey(row[3].ToString()))
                        location_id = locationMap[thing];
                    insertCmd.Parameters.AddWithValue("@location_id", (object)location_id ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@qty", (object)row[4] ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@inventoried_date", (object)row[5] ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@expiration_date", (object)row[7] ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@onboard_date", (object)row[8] ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@cage", (object)row[9] ?? DBNull.Value);
                    insertCmd.Parameters.AddWithValue("@manufacturer", (object)row[12].ToString() ?? DBNull.Value);
                    insertCmd.ExecuteNonQuery();
                }
            }
        }

        public void MigrateInventoryDetail()
        {
            var dataTable = reader.GetTableFromQuery("SELECT * FROM INVENTORY", null, CommandType.Text);

            Dictionary<string, int> smccMap = getSmccMap();
            Dictionary<string, int> slcMap = getSlcMap();
            Dictionary<string, int> slacMap = getSlacMap();
            Dictionary<string, int> uiMap = getUiMap();

            foreach (DataRow row in dataTable.Rows)
            {
                //insert into the inventory_detail table
                var items = row.ItemArray;
                string insertStmt = "use " + targetDb + "; " + Scripts.InsertInventorySql();
                SqlCommand insertCmd = new SqlCommand(insertStmt, conn);
                insertCmd.Parameters.AddWithValue("@inventory_detail_id", (object)row[0] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@fsc", (object)row[1] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@niin", (object)row[2] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@item_name", (object)row[3] ?? DBNull.Value);
                int? ui_id = null;
                var thing = row[4].ToString();
                if (uiMap.ContainsKey(row[4].ToString()))
                    ui_id = uiMap[thing];
                insertCmd.Parameters.AddWithValue("@ui_id", (object)ui_id ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@um", (object)row[5] ?? DBNull.Value);
                int? smcc_id = null;
                thing = row[6].ToString();
                if (smccMap.ContainsKey(row[6].ToString()))
                    smcc_id = smccMap[thing];
                insertCmd.Parameters.AddWithValue("@smcc_id", (object)smcc_id ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@allowance_quantity", (object)row[9] ?? DBNull.Value);
                insertCmd.Parameters.AddWithValue("@notes", (object)row[11] ?? DBNull.Value);
                int? slc_id = null;
                thing = row[12].ToString();
                if (slcMap.ContainsKey(row[12].ToString()))
                    slc_id = slcMap[thing];
                insertCmd.Parameters.AddWithValue("@slc_id", (object)slc_id ?? DBNull.Value);
                int? slac_id = null;
                thing = row[13].ToString();
                if (slacMap.ContainsKey(row[13].ToString()))
                    slac_id = slacMap[thing];
                insertCmd.Parameters.AddWithValue("@slac_id", (object)slac_id.ToString() ?? DBNull.Value);
                insertCmd.ExecuteNonQuery();
            }

        }

        //methods for mapping raw string data to IDs. In the Access DB there were no reference tables.
        public Dictionary<string, int> getUsageCategoryMap()
        {
            string selectStmt = "use " + targetDb + "; Select category, usage_category_id from usage_category";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getUiMap()
        {
            string selectStmt = "use " + targetDb + ";Select abbreviation, ui_id from ui";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSmccMap()
        {
            string selectStmt = "use " + targetDb + ";Select smcc, smcc_id from smcc";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlcMap()
        {
            string selectStmt = "use " + targetDb + ";Select slc, shelf_life_code_id from shelf_life_code";
            return getMap(selectStmt);
        }
        public Dictionary<string, int> getSlacMap()
        {
            string selectStmt = "use " + targetDb + ";Select slac, shelf_life_action_code_id from shelf_life_action_code";
            return getMap(selectStmt);
        }

        public Dictionary<string, int> getWorkcenterMap()
        {
            string selectStmt = "use " + targetDb + ";Select wid, workcenter_id from workcenter";
            return getMap(selectStmt);
        }

        public Dictionary<string, int> getLocationMap()
        {
            string selectStmt = "use " + targetDb + ";Select name, location_id from locations";
            return getMap(selectStmt);
        }

        public Dictionary<string, int> getMap(String selectStmt)
        {
            Dictionary<string, int> map = new Dictionary<string, int>();

            SqlCommand cmdRead = new SqlCommand(selectStmt, conn);

            SqlDataReader reader = cmdRead.ExecuteReader();
            while (reader.Read())
            {
                if (!map.ContainsKey("" + reader.GetValue(0)))
                    map.Add("" + reader.GetValue(0), Int32.Parse("" + reader.GetValue(1)));
            }

            reader.Dispose();
            reader.Close();

            return map;
        }
    }
}
