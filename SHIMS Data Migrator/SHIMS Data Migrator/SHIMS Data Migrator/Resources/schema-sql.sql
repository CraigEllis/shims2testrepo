CREATE TABLE [dbo].[inventory_detail](
	[inventory_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[ui_id] [int] NULL,
	[um] [nvarchar](25) NULL,
	[notes] [nvarchar](max) NULL,
	[slc_id] [bigint] NULL,
	[slac_id] [bigint] NULL,
	[hcc_id] [bigint] NULL,
	[allowance_quantity] [int] NULL,
	[item_name] [nvarchar](max) NULL,
	[spmig] [nvarchar](25) NULL,
	[specs] [nvarchar](25) NULL,
	[usage_category_id] [bigint] NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](10) NULL,
	[smcc_id] [bigint] NULL,
 CONSTRAINT [PK_inventory_detail] PRIMARY KEY CLUSTERED 
(
	[inventory_detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[inventory_detail]  WITH CHECK ADD  CONSTRAINT [FK_inventory_detail_hcc] FOREIGN KEY([hcc_id])
REFERENCES [dbo].[hcc] ([hcc_id])

ALTER TABLE [dbo].[inventory_detail] CHECK CONSTRAINT [FK_inventory_detail_hcc]

ALTER TABLE [dbo].[inventory_detail]  WITH CHECK ADD  CONSTRAINT [FK_inventory_detail_shelf_life_action_code] FOREIGN KEY([usage_category_id])
REFERENCES [dbo].[usage_category] ([usage_category_id])


ALTER TABLE [dbo].[inventory_detail] CHECK CONSTRAINT [FK_inventory_detail_shelf_life_action_code]


ALTER TABLE [dbo].[inventory_detail]  WITH CHECK ADD  CONSTRAINT [FK_inventory_detail_shelf_life_code] FOREIGN KEY([slc_id])
REFERENCES [dbo].[shelf_life_code] ([shelf_life_code_id])


ALTER TABLE [dbo].[inventory_detail] CHECK CONSTRAINT [FK_inventory_detail_shelf_life_code]


ALTER TABLE [dbo].[inventory_detail]  WITH CHECK ADD  CONSTRAINT [FK_inventory_detail_smcc] FOREIGN KEY([smcc_id])
REFERENCES [dbo].[smcc] ([smcc_id])


ALTER TABLE [dbo].[inventory_detail] CHECK CONSTRAINT [FK_inventory_detail_smcc]


ALTER TABLE [dbo].[inventory_detail]  WITH CHECK ADD  CONSTRAINT [FK_inventory_detail_ui] FOREIGN KEY([ui_id])
REFERENCES [dbo].[ui] ([ui_id])


ALTER TABLE [dbo].[inventory_detail] CHECK CONSTRAINT [FK_inventory_detail_ui]


CREATE TABLE [dbo].[mfg_catalog](
	[mfg_catalog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cage] [nvarchar](5) NULL,
	[manufacturer] [nvarchar](255) NULL,
	[msds_no] [nvarchar](20) NULL,
	[niin] [nvarchar](10) NULL,
 CONSTRAINT [PK_mfg_catalog] PRIMARY KEY CLUSTERED 
(
	[mfg_catalog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[locations](
	[location_id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[workcenter_id] [bigint] NULL,
 CONSTRAINT [PK_locations] PRIMARY KEY CLUSTERED 
(
	[location_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[inventory_onhand](
	[inventory_onhand_id] [int] IDENTITY(1,1) NOT NULL,
	[location_id] [bigint] NULL,
	[qty] [int] NULL,
	[inventoried_date] [datetime] NULL,
	[onboard_date] [datetime] NULL,
	[expiration_date] [datetime] NULL,
	[inventory_detail_id] [int] NULL,
	[mfg_catalog_id] [bigint] NULL,
 CONSTRAINT [PK_inventory_onhand] PRIMARY KEY CLUSTERED 
(
	[inventory_onhand_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[inventory_onhand]  WITH NOCHECK ADD  CONSTRAINT [FK_inventory_onhand_inventory_detail] FOREIGN KEY([inventory_detail_id])
REFERENCES [dbo].[inventory_detail] ([inventory_detail_id])


ALTER TABLE [dbo].[inventory_onhand] CHECK CONSTRAINT [FK_inventory_onhand_inventory_detail]


ALTER TABLE [dbo].[inventory_onhand]  WITH NOCHECK ADD  CONSTRAINT [FK_inventory_onhand_locations] FOREIGN KEY([location_id])
REFERENCES [dbo].[locations] ([location_id])


ALTER TABLE [dbo].[inventory_onhand] CHECK CONSTRAINT [FK_inventory_onhand_locations]



CREATE TABLE [dbo].[niin_catalog](
	[niin_catalog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](9) NULL,
	[ui] [int] NULL,
	[um] [nvarchar](50) NULL,
	[usage_category_id] [bigint] NULL,
	[description] [nvarchar](1024) NULL,
	[smcc_id] [bigint] NULL,
	[specs] [nvarchar](50) NULL,
	[shelf_life_code_id] [bigint] NULL,
	[shelf_life_action_code_id] [bigint] NULL,
	[remarks] [text] NULL,
	[storage_type_id] [bigint] NULL,
	[cog] [nvarchar](10) NULL,
	[spmig] [nvarchar](255) NULL,
	[nehc_rpt] [nvarchar](10) NULL,
	[catalog_group_id] [bigint] NULL,
	[catalog_serial_number] [nvarchar](255) NULL,
	[allowance_qty] [int] NULL,
	[created] [datetime] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](255) NULL,
	[msds_num] [nvarchar](255) NULL,
	[NMCPHC] [nvarchar](20) NULL,
	[part_name] [nvarchar](25) NULL,
	[ship_id] [bigint] NULL,
	[manually_entered] [bit] NULL,
 CONSTRAINT [PK_niin_catalog] PRIMARY KEY CLUSTERED 
(
	[niin_catalog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_smclniin] UNIQUE NONCLUSTERED 
(
	[niin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'federal supply class - 4 digit number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'niin_catalog', @level2type=N'COLUMN',@level2name=N'fsc'

CREATE TABLE [dbo].[workcenter](
	[workcenter_id] [bigint] IDENTITY(1,1) NOT NULL,
	[wid] [nvarchar](10) NULL,
 CONSTRAINT [PK_workcenter] PRIMARY KEY CLUSTERED 
(
	[workcenter_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[user_roles](
	[user_role_id] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[role] [nvarchar](50) NULL,
	[workcenter_id] [bigint] NULL,
 CONSTRAINT [PK_user_roles] PRIMARY KEY CLUSTERED 
(
	[user_role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[ships](
	[ship_id] [bigint] IDENTITY(1,1) NOT NULL,
	[current_ship] [bit] NOT NULL,
	[name] [nvarchar](50) NULL,
	[uic] [nvarchar](10) NULL,
	[hull_type] [nvarchar](10) NULL,
	[hull_number] [nvarchar](10) NULL,
	[address] [nvarchar](100) NULL,
	[POC_Name] [nvarchar](50) NULL,
	[POC_Telephone] [nvarchar](50) NULL,
	[POC_Email] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[zip] [nvarchar](10) NULL,
	[act_code] [nvarchar](25) NULL,
	[base] [nvarchar](50) NULL,
	[sndl] [nvarchar](25) NULL,
	[ship_logo_filename] [nvarchar](50) NULL,
	[ship_picture_filename] [nvarchar](50) NULL,
	[POC_Address] [nvarchar](100) NULL,
	[POC_Title] [nvarchar](50) NULL,
 CONSTRAINT [PK_ships] PRIMARY KEY CLUSTERED 
(
	[ship_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[sfr](
	[sfr_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sfr_type] [int] NULL,
	[date_submitted] [datetime] NULL,
	[username] [varchar](90) NULL,
	[completed_flag] [bit] NULL,
	[ship_id] [bigint] NULL,
	[manufacturer] [nvarchar](255) NULL,
	[cage] [nvarchar](5) NULL,
	[part_no] [nvarchar](255) NULL,
	[mfg_city] [nvarchar](255) NULL,
	[mfg_address] [nvarchar](255) NULL,
	[mfg_state] [nvarchar](20) NULL,
	[mfg_zip] [nvarchar](10) NULL,
	[system_equipment_material] [varchar](1024) NULL,
	[method_of_application] [varchar](1024) NULL,
	[proposed_usage] [varchar](1024) NULL,
	[negative_impact] [varchar](1024) NULL,
	[special_training] [varchar](1024) NULL,
	[precautions] [varchar](1024) NULL,
	[properties] [varchar](1024) NULL,
	[comments] [varchar](1024) NULL,
	[advantages] [varchar](1024) NULL,
	[inventory_detail_id] [int] NULL,
	[item_name] [nvarchar](100) NULL,
	[fsc] [int] NULL,
	[niin] [nvarchar](10) NULL,
	[specification_no] [nvarchar](10) NULL,
	[ui_id] [int] NULL,
	[mfg_poc] [nvarchar](255) NULL,
	[mfg_phone] [nvarchar](15) NULL,
	[msds_attached] [bit] NULL,
	[date_mailed] [datetime] NULL,
 CONSTRAINT [PK_sfr] PRIMARY KEY CLUSTERED 
(
	[sfr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[sfr]  WITH CHECK ADD  CONSTRAINT [FK_sfr_ui] FOREIGN KEY([ui_id])
REFERENCES [dbo].[ui] ([ui_id])

ALTER TABLE [dbo].[sfr] CHECK CONSTRAINT [FK_sfr_ui]

CREATE TABLE [dbo].[file_uploads](
	[file_upload_id] [bigint] IDENTITY(1,1) NOT NULL,
	[file_name] [nvarchar](255) NOT NULL,
	[file_date] [datetime] NULL,
	[upload_date] [datetime] NULL,
	[uploaded_by] [nvarchar](50) NULL,
	[file_type] [nvarchar](20) NOT NULL,
	[records_added] [int] NULL,
	[records_changed] [int] NULL,
	[records_deleted] [int] NULL
) ON [PRIMARY]



ALTER TABLE [dbo].[file_uploads] ADD  DEFAULT (0) FOR [records_added]


ALTER TABLE [dbo].[file_uploads] ADD  DEFAULT (0) FOR [records_changed]


ALTER TABLE [dbo].[file_uploads] ADD  DEFAULT (0) FOR [records_deleted]

CREATE TABLE [dbo].[offload_list](
	[offload_list_id] [bigint] IDENTITY(1,1) NOT NULL,
	[archived_id] [bigint] NULL,
	[shelf_life_expiration_date] [smalldatetime] NULL,
	[location_id] [bigint] NULL,
	[mfg_catalog_id] [bigint] NULL,
	[offload_qty] [int] NULL,
	[deleted] [bit] NULL,
	[offload_date] [datetime] NULL,
	[slc_id] [bigint] NULL,
	[slac_id] [bigint] NULL,
	[hcc_id] [bigint] NULL,
	[usage_category_id] [bigint] NULL,
	[smcc_id] [bigint] NULL,
	[fsc] [nvarchar](50) NULL,
	[niin] [nvarchar](50) NULL,
	[cage] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[ui] [nvarchar](50) NULL,
	[um] [nvarchar](50) NULL,
	[add_data_4] [nvarchar](max) NULL,
 CONSTRAINT [PK_offload_list] PRIMARY KEY CLUSTERED 
(
	[offload_list_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[offload_list]  WITH CHECK ADD  CONSTRAINT [fk_hcc] FOREIGN KEY([hcc_id])
REFERENCES [dbo].[hcc] ([hcc_id])


ALTER TABLE [dbo].[offload_list] CHECK CONSTRAINT [fk_hcc]


ALTER TABLE [dbo].[offload_list]  WITH NOCHECK ADD  CONSTRAINT [FK_offload_list_locations] FOREIGN KEY([location_id])
REFERENCES [dbo].[locations] ([location_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[offload_list] CHECK CONSTRAINT [FK_offload_list_locations]


ALTER TABLE [dbo].[offload_list]  WITH NOCHECK ADD  CONSTRAINT [FK_offload_list_mfg_catalog] FOREIGN KEY([mfg_catalog_id])
REFERENCES [dbo].[mfg_catalog] ([mfg_catalog_id])


ALTER TABLE [dbo].[offload_list] CHECK CONSTRAINT [FK_offload_list_mfg_catalog]



ALTER TABLE [dbo].[offload_list]  WITH CHECK ADD  CONSTRAINT [fk_slc] FOREIGN KEY([slc_id])
REFERENCES [dbo].[shelf_life_code] ([shelf_life_code_id])


ALTER TABLE [dbo].[offload_list] CHECK CONSTRAINT [fk_slc]


ALTER TABLE [dbo].[offload_list]  WITH CHECK ADD  CONSTRAINT [fk_smcc] FOREIGN KEY([smcc_id])
REFERENCES [dbo].[smcc] ([smcc_id])


ALTER TABLE [dbo].[offload_list] CHECK CONSTRAINT [fk_smcc]


ALTER TABLE [dbo].[offload_list]  WITH CHECK ADD  CONSTRAINT [fk_usage_at] FOREIGN KEY([usage_category_id])
REFERENCES [dbo].[usage_category] ([usage_category_id])


ALTER TABLE [dbo].[offload_list] CHECK CONSTRAINT [fk_usage_at]

CREATE TABLE [dbo].[archived](
	[archived_id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_archived] [smalldatetime] NULL,
	[offload_list_id] [bigint] NULL,
 CONSTRAINT [PK_archived] PRIMARY KEY CLUSTERED 
(
	[archived_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[archived]  WITH CHECK ADD FOREIGN KEY([offload_list_id])
REFERENCES [dbo].[offload_list] ([offload_list_id])

CREATE TABLE [dbo].[dd_1348](
	[dd_1348_id] [bigint] IDENTITY(1,1) NOT NULL,
	[offload_list_id] [bigint] NOT NULL,
	[archive_id] [bigint] NULL,
	[document_number] [nvarchar](50) NULL,
	[doc_ident] [nvarchar](50) NULL,
	[ri_from] [nvarchar](50) NULL,
	[m_and_s] [nvarchar](50) NULL,
	[ser] [nvarchar](50) NULL,
	[suppaddress] [nvarchar](50) NULL,
	[sig] [nvarchar](50) NULL,
	[fund] [nvarchar](50) NULL,
	[distribution] [nvarchar](50) NULL,
	[project] [nvarchar](50) NULL,
	[pri] [nvarchar](50) NULL,
	[reqd_del_date] [nvarchar](50) NULL,
	[adv] [nvarchar](50) NULL,
	[ri] [nvarchar](50) NULL,
	[op] [nvarchar](50) NULL,
	[mgt] [nvarchar](50) NULL,
	[condition] [nvarchar](50) NULL,
	[unit_price] [nvarchar](50) NULL,
	[total_price] [nvarchar](50) NULL,
	[ship_from] [nvarchar](50) NULL,
	[ship_to] [nvarchar](50) NULL,
	[mark_for] [nvarchar](50) NULL,
	[nmfc] [nvarchar](50) NULL,
	[frt_rate] [nvarchar](50) NULL,
	[cargo_type] [nvarchar](50) NULL,
	[ps] [nvarchar](50) NULL,
	[qty_recd] [nvarchar](50) NULL,
	[up] [nvarchar](50) NULL,
	[unit_weight] [nvarchar](50) NULL,
	[unit_cube] [nvarchar](50) NULL,
	[ufc] [nvarchar](50) NULL,
	[sl] [nvarchar](50) NULL,
	[item_name] [nvarchar](50) NULL,
	[ty_cont] [nvarchar](50) NULL,
	[no_cont] [nvarchar](50) NULL,
	[total_weight] [nvarchar](50) NULL,
	[total_cube] [nvarchar](50) NULL,
	[recd_by] [nvarchar](max) NULL,
	[date_recd] [nvarchar](50) NULL,
	[doc_num] [nvarchar](max) NULL,
	[memo_add_1] [nvarchar](max) NULL,
	[memo_add_2] [nvarchar](max) NULL,
 CONSTRAINT [PK_dd_1348] PRIMARY KEY CLUSTERED 
(
	[dd_1348_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[dd_1348]  WITH CHECK ADD  CONSTRAINT [FK_dd_1348_offload_list] FOREIGN KEY([offload_list_id])
REFERENCES [dbo].[offload_list] ([offload_list_id])

CREATE SEQUENCE [dbo].[Serial] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 

 CREATE TABLE [dbo].[msds](
	[msds_id] [bigint] IDENTITY(1,1) NOT NULL,
	[MSDSSERNO] [nvarchar](10) NULL,
	[CAGE] [nvarchar](5) NULL,
	[MANUFACTURER] [nvarchar](255) NULL,
	[PARTNO] [nvarchar](100) NULL,
	[FSC] [nvarchar](18) NULL,
	[NIIN] [nvarchar](9) NULL,
	[hcc_id] [bigint] NULL,
	[msds_file] [image] NULL,
	[file_name] [nvarchar](255) NULL,
	[manually_entered] [bit] NULL,
	[ARTICLE_IND] [nchar](10) NULL,
	[DESCRIPTION] [ntext] NULL,
	[EMERGENCY_TEL] [nvarchar](50) NULL,
	[END_COMP_IND] [nchar](10) NULL,
	[END_ITEM_IND] [nchar](10) NULL,
	[KIT_IND] [nchar](10) NULL,
	[KIT_PART_IND] [nchar](10) NULL,
	[MANUFACTURER_MSDS_NO] [nvarchar](50) NULL,
	[MIXTURE_IND] [nchar](10) NULL,
	[PRODUCT_IDENTITY] [nvarchar](255) NULL,
	[PRODUCT_LOAD_DATE] [smalldatetime] NULL,
	[PRODUCT_RECORD_STATUS] [nchar](10) NULL,
	[PRODUCT_REVISION_NO] [nvarchar](18) NULL,
	[PROPRIETARY_IND] [nchar](1) NULL,
	[PUBLISHED_IND] [nchar](1) NULL,
	[PURCHASED_PROD_IND] [nchar](1) NULL,
	[PURE_IND] [nchar](1) NULL,
	[RADIOACTIVE_IND] [nchar](1) NULL,
	[SERVICE_AGENCY] [nvarchar](10) NULL,
	[TRADE_NAME] [nvarchar](255) NULL,
	[TRADE_SECRET_IND] [nchar](1) NULL,
	[PRODUCT_IND] [nvarchar](1) NULL,
	[PRODUCT_LANGUAGE] [nvarchar](10) NULL,
	[created] [datetime] NULL,
	[service_agency_code] [nvarchar](50) NULL,
	[data_source_cd] [nvarchar](50) NULL,
	[created_by] [nvarchar](100) NULL,
 CONSTRAINT [PK_msds] PRIMARY KEY CLUSTERED 
(
	[msds_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[msds]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_hcc] FOREIGN KEY([hcc_id])
REFERENCES [dbo].[hcc] ([hcc_id])

ALTER TABLE [dbo].[msds] CHECK CONSTRAINT [FK_msds_hcc]

CREATE TABLE [dbo].[msds_disposal](
	[disp_info_id] [bigint] IDENTITY(1,1) NOT NULL,
	[DISPOSAL_ADD_INFO] [ntext] NULL,
	[EPA_HAZ_WASTE_CODE] [nvarchar](10) NULL,
	[EPA_HAZ_WASTE_IND] [nchar](1) NULL,
	[EPA_HAZ_WASTE_NAME] [nvarchar](100) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_disposal] PRIMARY KEY CLUSTERED 
(
	[disp_info_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[msds_disposal]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_disposal_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_disposal] CHECK CONSTRAINT [FK_msds_disposal_msds]

CREATE TABLE [dbo].[msds_document_types](
	[doc_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[MSDS_TRANSLATED] [image] NULL,
	[NESHAP_COMP_CERT] [image] NULL,
	[OTHER_DOCS] [image] NULL,
	[PRODUCT_SHEET] [image] NULL,
	[TRANSPORTATION_CERT] [image] NULL,
	[msds_translated_filename] [nvarchar](255) NULL,
	[neshap_comp_filename] [nvarchar](255) NULL,
	[other_docs_filename] [nvarchar](255) NULL,
	[product_sheet_filename] [nvarchar](255) NULL,
	[transportation_cert_filename] [nvarchar](255) NULL,
	[MANUFACTURER_LABEL] [image] NULL,
	[manufacturer_label_filename] [nvarchar](255) NULL,
 CONSTRAINT [PK_msds_document_types] PRIMARY KEY CLUSTERED 
(
	[doc_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[msds_document_types]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_document_types_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[msds_document_types] CHECK CONSTRAINT [FK_msds_document_types_msds]

CREATE TABLE [dbo].[msds_dot_psn](
	[dot_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[DOT_HAZARD_CLASS_DIV] [nvarchar](10) NULL,
	[DOT_HAZARD_LABEL] [nvarchar](100) NULL,
	[DOT_MAX_CARGO] [nvarchar](20) NULL,
	[DOT_MAX_PASSENGER] [nvarchar](20) NULL,
	[DOT_PACK_BULK] [nvarchar](20) NULL,
	[DOT_PACK_EXCEPTIONS] [nvarchar](30) NULL,
	[DOT_PACK_NONBULK] [nvarchar](20) NULL,
	[DOT_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[DOT_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[DOT_PSN_CODE] [nvarchar](10) NULL,
	[DOT_SPECIAL_PROVISION] [nvarchar](100) NULL,
	[DOT_SYMBOLS] [nvarchar](10) NULL,
	[DOT_UN_ID_NUMBER] [nvarchar](10) NULL,
	[DOT_WATER_OTHER_REQ] [nvarchar](30) NULL,
	[DOT_WATER_VESSEL_STOW] [nvarchar](20) NULL,
	[msds_id] [bigint] NULL,
	[DOT_PACK_GROUP] [nvarchar](10) NULL,
 CONSTRAINT [PK_msds_dot_psn] PRIMARY KEY CLUSTERED 
(
	[dot_psn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[msds_dot_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_dot_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[msds_dot_psn] CHECK CONSTRAINT [FK_msds_dot_psn_msds]

CREATE TABLE [dbo].[msds_iata_psn](
	[iata_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[IATA_CARGO_PACKING] [nvarchar](20) NULL,
	[IATA_HAZARD_CLASS] [nvarchar](10) NULL,
	[IATA_HAZARD_LABEL] [nvarchar](100) NULL,
	[IATA_PACK_GROUP] [nvarchar](10) NULL,
	[IATA_PASS_AIR_PACK_LMT_INSTR] [nvarchar](30) NULL,
	[IATA_PASS_AIR_PACK_LMT_PER_PKG] [nvarchar](20) NULL,
	[IATA_PASS_AIR_PACK_NOTE] [nvarchar](20) NULL,
	[IATA_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[IATA_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[IATA_CARGO_PACK_MAX_QTY] [nvarchar](20) NULL,
	[IATA_PSN_CODE] [nvarchar](20) NULL,
	[IATA_PASS_AIR_MAX_QTY] [nvarchar](20) NULL,
	[IATA_SPECIAL_PROV] [nvarchar](30) NULL,
	[IATA_SUBSIDIARY_RISK] [nvarchar](50) NULL,
	[IATA_UN_ID_NUMBER] [nvarchar](50) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_iata_psn] PRIMARY KEY CLUSTERED 
(
	[iata_psn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[msds_iata_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_iata_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[msds_iata_psn] CHECK CONSTRAINT [FK_msds_iata_psn_msds]

CREATE TABLE [dbo].[msds_imo_psn](
	[imo_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[IMO_EMS_NO] [nvarchar](30) NULL,
	[IMO_HAZARD_CLASS] [nvarchar](10) NULL,
	[IMO_IBC_INSTR] [nvarchar](10) NULL,
	[IMO_LIMITED_QTY] [nvarchar](10) NULL,
	[IMO_PACK_GROUP] [nvarchar](10) NULL,
	[IMO_PACK_INSTRUCTIONS] [nvarchar](10) NULL,
	[IMO_PACK_PROVISIONS] [nvarchar](30) NULL,
	[IMO_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[IMO_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[IMO_PSN_CODE] [nvarchar](10) NULL,
	[IMO_SPECIAL_PROV] [nvarchar](100) NULL,
	[IMO_STOW_SEGR] [nvarchar](15) NULL,
	[IMO_SUBSIDIARY_RISK] [nvarchar](30) NULL,
	[IMO_TANK_INSTR_IMO] [nvarchar](10) NULL,
	[IMO_TANK_INSTR_PROV] [nvarchar](30) NULL,
	[IMO_TANK_INSTR_UN] [nvarchar](10) NULL,
	[IMO_UN_NUMBER] [nvarchar](10) NULL,
	[msds_id] [bigint] NULL,
	[IMO_IBC_PROVISIONS] [nvarchar](30) NULL,
 CONSTRAINT [PK_msds_imo_psn] PRIMARY KEY CLUSTERED 
(
	[imo_psn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[msds_imo_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_imo_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_imo_psn] CHECK CONSTRAINT [FK_msds_imo_psn_msds]

CREATE TABLE [dbo].[msds_ingredients](
	[ingredients_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[CAS] [nvarchar](255) NULL,
	[RTECS_NUM] [nvarchar](255) NULL,
	[RTECS_CODE] [nvarchar](255) NULL,
	[INGREDIENT_NAME] [ntext] NULL,
	[PRCNT] [nvarchar](255) NULL,
	[OSHA_PEL] [nvarchar](255) NULL,
	[OSHA_STEL] [nvarchar](255) NULL,
	[ACGIH_TLV] [nvarchar](255) NULL,
	[ACGIH_STEL] [nvarchar](255) NULL,
	[EPA_REPORT_QTY] [nvarchar](255) NULL,
	[DOT_REPORT_QTY] [nvarchar](255) NULL,
	[PRCNT_VOL_VALUE] [nvarchar](255) NULL,
	[PRCNT_VOL_WEIGHT] [nvarchar](255) NULL,
	[CHEM_MFG_COMP_NAME] [nvarchar](255) NULL,
	[ODS_IND] [nvarchar](255) NULL,
	[OTHER_REC_LIMITS] [nvarchar](255) NULL,
 CONSTRAINT [PK_msds_ingredients] PRIMARY KEY CLUSTERED 
(
	[ingredients_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



ALTER TABLE [dbo].[msds_ingredients]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_ingredients_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_ingredients] CHECK CONSTRAINT [FK_msds_ingredients_msds]

CREATE TABLE [dbo].[msds_item_description](
	[item_description_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[ITEM_MANAGER] [nvarchar](10) NULL,
	[ITEM_NAME] [nvarchar](100) NULL,
	[SPECIFICATION_NUMBER] [nvarchar](20) NULL,
	[TYPE_GRADE_CLASS] [nvarchar](20) NULL,
	[UNIT_OF_ISSUE] [nvarchar](10) NULL,
	[QUANTITATIVE_EXPRESSION] [nvarchar](15) NULL,
	[UI_CONTAINER_QTY] [nvarchar](15) NULL,
	[TYPE_OF_CONTAINER] [nvarchar](15) NULL,
	[BATCH_NUMBER] [nvarchar](50) NULL,
	[LOT_NUMBER] [nvarchar](50) NULL,
	[LOG_FLIS_NIIN_VER] [nchar](1) NULL,
	[LOG_FSC] [numeric](18, 0) NULL,
	[NET_UNIT_WEIGHT] [nvarchar](15) NULL,
	[SHELF_LIFE_CODE] [nvarchar](10) NULL,
	[SPECIAL_EMP_CODE] [nvarchar](10) NULL,
	[UN_NA_NUMBER] [nvarchar](10) NULL,
	[UPC_GTIN] [nvarchar](15) NULL,
 CONSTRAINT [PK_msds_item_description] PRIMARY KEY CLUSTERED 
(
	[item_description_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[msds_item_description]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_item_description_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[msds_item_description] CHECK CONSTRAINT [FK_msds_item_description_msds]

CREATE TABLE [dbo].[msds_label_info](
	[label_info_id] [bigint] IDENTITY(1,1) NOT NULL,
	[COMPANY_CAGE_RP] [nvarchar](10) NULL,
	[COMPANY_NAME_RP] [nvarchar](255) NULL,
	[LABEL_EMERG_PHONE] [nvarchar](50) NULL,
	[LABEL_ITEM_NAME] [nvarchar](100) NULL,
	[LABEL_PROC_YEAR] [nvarchar](10) NULL,
	[LABEL_PROD_IDENT] [nvarchar](100) NULL,
	[LABEL_PROD_SERIALNO] [nvarchar](10) NULL,
	[LABEL_SIGNAL_WORD] [nvarchar](10) NULL,
	[LABEL_STOCK_NO] [nvarchar](15) NULL,
	[SPECIFIC_HAZARDS] [ntext] NULL,
	[msds_id] [bigint] NULL,
	[LABEL_SIGNAL_WORD_CODE] [nvarchar](50) NULL,
 CONSTRAINT [PK_msds_label_info] PRIMARY KEY CLUSTERED 
(
	[label_info_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[msds_label_info]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_label_info_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_label_info] CHECK CONSTRAINT [FK_msds_label_info_msds]

CREATE TABLE [dbo].[msds_phys_chemical](
	[phys_chemical_id] [bigint] IDENTITY(1,1) NOT NULL,
	[msds_id] [bigint] NULL,
	[VAPOR_PRESS] [nvarchar](30) NULL,
	[VAPOR_DENS] [nvarchar](10) NULL,
	[SPECIFIC_GRAV] [nvarchar](30) NULL,
	[VOC_POUNDS_GALLON] [nvarchar](50) NULL,
	[VOC_GRAMS_LITER] [nvarchar](50) NULL,
	[PH] [nvarchar](15) NULL,
	[VISCOSITY] [nvarchar](30) NULL,
	[EVAP_RATE_REF] [nvarchar](30) NULL,
	[SOL_IN_WATER] [nvarchar](30) NULL,
	[APP_ODOR] [nvarchar](255) NULL,
	[PERCENT_VOL_VOLUME] [nvarchar](15) NULL,
	[AUTOIGNITION_TEMP] [nvarchar](18) NULL,
	[CARCINOGEN_IND] [nchar](1) NULL,
	[EPA_ACUTE] [nchar](1) NULL,
	[EPA_CHRONIC] [nchar](1) NULL,
	[EPA_FIRE] [nchar](1) NULL,
	[EPA_PRESSURE] [nchar](1) NULL,
	[EPA_REACTIVITY] [nchar](1) NULL,
	[FLASH_PT_TEMP] [nvarchar](18) NULL,
	[NEUT_AGENT] [nvarchar](255) NULL,
	[NFPA_FLAMMABILITY] [nvarchar](10) NULL,
	[NFPA_HEALTH] [nvarchar](50) NULL,
	[NFPA_REACTIVITY] [nvarchar](50) NULL,
	[NFPA_SPECIAL] [nvarchar](50) NULL,
	[OSHA_CARCINOGENS] [nchar](1) NULL,
	[OSHA_COMB_LIQUID] [nchar](1) NULL,
	[OSHA_COMP_GAS] [nchar](1) NULL,
	[OSHA_CORROSIVE] [nchar](1) NULL,
	[OSHA_EXPLOSIVE] [nchar](1) NULL,
	[OSHA_FLAMMABLE] [nchar](1) NULL,
	[OSHA_HIGH_TOXIC] [nchar](1) NULL,
	[OSHA_IRRITANT] [nchar](1) NULL,
	[OSHA_ORG_PEROX] [nchar](1) NULL,
	[OSHA_OTHERLONGTERM] [nchar](1) NULL,
	[OSHA_OXIDIZER] [nchar](1) NULL,
	[OSHA_PYRO] [nchar](1) NULL,
	[OSHA_SENSITIZER] [nchar](1) NULL,
	[OSHA_TOXIC] [nchar](1) NULL,
	[OSHA_UNST_REACT] [nchar](1) NULL,
	[OTHER_SHORT_TERM] [nvarchar](20) NULL,
	[PHYS_STATE_CODE] [nchar](10) NULL,
	[VOL_ORG_COMP_WT] [nvarchar](18) NULL,
	[OSHA_WATER_REACTIVE] [nchar](1) NULL,
 CONSTRAINT [PK_msds_phys_chemical] PRIMARY KEY CLUSTERED 
(
	[phys_chemical_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[msds_phys_chemical]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_phys_chemical_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE

ALTER TABLE [dbo].[msds_phys_chemical] CHECK CONSTRAINT [FK_msds_phys_chemical_msds]

CREATE TABLE [dbo].[msds_radiological_info](
	[rad_info_id] [bigint] IDENTITY(1,1) NOT NULL,
	[NRC_LP_NUM] [nvarchar](30) NULL,
	[OPERATOR] [nvarchar](10) NULL,
	[RAD_AMOUNT_MICRO] [nvarchar](22) NULL,
	[RAD_FORM] [nvarchar](50) NULL,
	[RAD_CAS] [nvarchar](20) NULL,
	[RAD_NAME] [nvarchar](255) NULL,
	[RAD_SYMBOL] [nvarchar](10) NULL,
	[REP_NSN] [nvarchar](15) NULL,
	[SEALED] [nchar](1) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_radiological_info] PRIMARY KEY CLUSTERED 
(
	[rad_info_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[msds_radiological_info]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_radiological_info_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_radiological_info] CHECK CONSTRAINT [FK_msds_radiological_info_msds]

CREATE TABLE [dbo].[msds_afjm_psn](
	[afjm_psn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AFJM_HAZARD_CLASS] [nvarchar](10) NULL,
	[AFJM_PACK_PARAGRAPH] [nvarchar](30) NULL,
	[AFJM_PACK_GROUP] [nvarchar](10) NULL,
	[AFJM_PROP_SHIP_NAME] [nvarchar](255) NULL,
	[AFJM_PROP_SHIP_MODIFIER] [nvarchar](255) NULL,
	[AFJM_PSN_CODE] [nvarchar](10) NULL,
	[AFJM_SPECIAL_PROV] [nvarchar](100) NULL,
	[AFJM_SUBSIDIARY_RISK] [nvarchar](10) NULL,
	[AFJM_SYMBOLS] [nvarchar](10) NULL,
	[AFJM_UN_ID_NUMBER] [nvarchar](10) NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_afjm_psn] PRIMARY KEY CLUSTERED 
(
	[afjm_psn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[msds_afjm_psn]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_afjm_psn_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_afjm_psn] CHECK CONSTRAINT [FK_msds_afjm_psn_msds]

CREATE TABLE [dbo].[msds_contractor_info](
	[contractor_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CT_NUMBER] [ntext] NULL,
	[CT_CAGE] [ntext] NULL,
	[CT_CITY] [ntext] NULL,
	[CT_COMPANY_NAME] [ntext] NULL,
	[CT_COUNTRY] [nvarchar](255) NULL,
	[CT_PO_BOX] [nvarchar](255) NULL,
	[CT_PHONE] [ntext] NULL,
	[PURCHASE_ORDER_NO] [nvarchar](255) NULL,
	[msds_id] [bigint] NULL,
	[CT_STATE] [nvarchar](255) NULL,
 CONSTRAINT [PK_msds_contractor_info] PRIMARY KEY CLUSTERED 
(
	[contractor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[msds_contractor_info]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_contractor_info_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_contractor_info] CHECK CONSTRAINT [FK_msds_contractor_info_msds]

CREATE TABLE [dbo].[msds_transportation](
	[transportation_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AF_MMAC_CODE] [nvarchar](10) NULL,
	[CERTIFICATE_COE] [nvarchar](15) NULL,
	[COMPETENT_CAA] [nvarchar](15) NULL,
	[DOD_ID_CODE] [nvarchar](30) NULL,
	[DOT_EXEMPTION_NO] [nvarchar](15) NULL,
	[DOT_RQ_IND] [nchar](1) NULL,
	[EX_NO] [nvarchar](15) NULL,
	[FLASH_PT_TEMP] [numeric](18, 0) NULL,
	[HCC] [nvarchar](10) NULL,
	[HIGH_EXPLOSIVE_WT] [numeric](18, 0) NULL,
	[LTD_QTY_IND] [nchar](1) NULL,
	[MAGNETIC_IND] [nchar](1) NULL,
	[MAGNETISM] [nvarchar](50) NULL,
	[MARINE_POLLUTANT_IND] [nchar](1) NULL,
	[NET_EXP_QTY_DIST] [numeric](18, 0) NULL,
	[NET_EXP_WEIGHT] [nvarchar](30) NULL,
	[NET_PROPELLANT_WT] [nvarchar](30) NULL,
	[NOS_TECHNICAL_SHIPPING_NAME] [nvarchar](255) NULL,
	[TRANSPORTATION_ADDITIONAL_DATA] [ntext] NULL,
	[msds_id] [bigint] NULL,
 CONSTRAINT [PK_msds_transportation] PRIMARY KEY CLUSTERED 
(
	[transportation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[msds_transportation]  WITH NOCHECK ADD  CONSTRAINT [FK_msds_transportation_msds] FOREIGN KEY([msds_id])
REFERENCES [dbo].[msds] ([msds_id])
ON DELETE CASCADE


ALTER TABLE [dbo].[msds_transportation] CHECK CONSTRAINT [FK_msds_transportation_msds]

use msdb;
declare @command_var nvarchar(MAX);
set @command_var = N'use shims2; DECLARE @path nvarchar(200);
select @path = backup_location from config where config_id = 1;
DECLARE @date varchar(100);
set @date = CONVERT(VARCHAR(11),GETDATE(),112);
set @path = @path + @date + ''.bak'';
backup database SHIMS2 to disk = @path; update config set last_backup_date = CONVERT(VARCHAR(10),GETDATE(),10)';
DECLARE @jobId binary(16)

SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = N'SHIMS Database Backup')
IF (@jobId IS NOT NULL)
BEGIN
    EXEC msdb.dbo.sp_delete_job @jobId
END
EXEC dbo.sp_add_job
    @job_name = N'SHIMS Database Backup';
DECLARE @scheduleId binary(16)
SELECT  @scheduleId = schedule_id FROM msdb.dbo.sysschedules WHERE (name = N'Monthly')
IF (@scheduleId IS NOT NULL)
BEGIN
EXEC msdb.dbo.sp_delete_schedule @scheduleId
END
EXEC sp_add_jobstep
    @job_name = N'SHIMS Database Backup',
    @step_name = N'Backup',
    @subsystem = N'TSQL',
    @command = @command_var, 
    @retry_attempts = 5,
    @retry_interval = 5,
	@database_name = 'SHIMS2';

EXEC dbo.sp_add_schedule
    @schedule_name = N'Monthly',
    @freq_type = 16,
	@freq_interval = 1,
	@freq_recurrence_factor = 1

EXEC dbo.sp_attach_schedule
   @job_name = N'SHIMS Database Backup',
   @schedule_name = N'Monthly';

EXEC dbo.sp_add_jobserver
    @job_name = N'SHIMS Database Backup';

USE [msdb]

DECLARE @job2Id BINARY(16)
SELECT @job2Id = job_id FROM msdb.dbo.sysjobs WHERE (name = N'DeleteOldBackups')
IF (@job2Id IS NOT NULL)
BEGIN
    EXEC msdb.dbo.sp_delete_job @job2Id
END
EXEC  msdb.dbo.sp_add_job @job_name=N'DeleteOldBackups', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=2, 
		@notify_level_page=2, 
		@delete_level=0, 
		@description=N'Delete backups older than one year', 
		@category_name=N'Database Maintenance', 
	 @job_id = @job2Id OUTPUT
select @job2Id

EXEC msdb.dbo.sp_add_jobserver @job_name=N'DeleteOldBackups', @server_name = N'BRONN'

USE [msdb]

EXEC msdb.dbo.sp_add_jobstep @job_name=N'DeleteOldBackups', @step_name=N'Delete', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_fail_action=2, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC xp_cmdshell ''FORFILES /p c:\SHIMS2\Backups /s /m *.bak /d -365 /c "CMD /C del /Q /F @FILE"''', 
		@database_name=N'master', 
		@flags=0

USE [msdb]

EXEC msdb.dbo.sp_update_job @job_name=N'DeleteOldBackups', 
		@enabled=1, 
		@start_step_id=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=2, 
		@notify_level_page=2, 
		@delete_level=0, 
		@description=N'Delete backups older than one year', 
		@category_name=N'Database Maintenance', 
		@notify_email_operator_name=N'', 
		@notify_netsend_operator_name=N'', 
		@notify_page_operator_name=N''

USE [msdb]

DECLARE @schedule2id int
SELECT  @schedule2Id = schedule_id FROM msdb.dbo.sysschedules WHERE (name = N'Weekly')
IF (@schedule2Id IS NOT NULL)
BEGIN
EXEC msdb.dbo.sp_delete_schedule @schedule2Id
END
EXEC msdb.dbo.sp_add_jobschedule @job_name=N'DeleteOldBackups', @name=N'Weekly', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20150327, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, @schedule_id = @schedule2id OUTPUT
select @schedule2id

