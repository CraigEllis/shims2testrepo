CREATE TABLE [dbo].[hcc](
	[hcc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hcc] [nvarchar](5) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_hcc] PRIMARY KEY CLUSTERED 
(
	[hcc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[shelf_life_action_code](
	[shelf_life_action_code_id] [bigint] IDENTITY(1,1) NOT NULL,
	[slac] [nvarchar](2) NULL,
	[description] [text] NULL,
 CONSTRAINT [PK_shelf_life_action_code] PRIMARY KEY CLUSTERED 
(
	[shelf_life_action_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[shelf_life_code](
	[shelf_life_code_id] [bigint] IDENTITY(1,1) NOT NULL,
	[slc] [nvarchar](1) NULL,
	[time_in_months] [int] NULL,
	[description] [nvarchar](50) NULL,
	[type] [nvarchar](50) NULL,
 CONSTRAINT [PK_shelf_life_code] PRIMARY KEY CLUSTERED 
(
	[shelf_life_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[smcc](
	[smcc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[smcc] [nvarchar](5) NULL,
	[description] [nvarchar](255) NULL,
 CONSTRAINT [PK_smcc] PRIMARY KEY CLUSTERED 
(
	[smcc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[storage_type](
	[storage_type_id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](5) NULL,
	[description] [text] NULL,
 CONSTRAINT [PK_storage_type] PRIMARY KEY CLUSTERED 
(
	[storage_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[ui](
	[ui_id] [int] IDENTITY(1,1) NOT NULL,
	[abbreviation] [nvarchar](15) NULL,
	[description] [nvarchar](50) NULL,
 CONSTRAINT [PK_ui] PRIMARY KEY CLUSTERED 
(
	[ui_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[usage_category](
	[usage_category_id] [bigint] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](5) NULL,
	[description] [nvarchar](50) NULL,
 CONSTRAINT [PK_usage_category] PRIMARY KEY CLUSTERED 
(
	[usage_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET IDENTITY_INSERT [dbo].[hcc] ON 

INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (1, N'A1', N'Radioactive, Licensed')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (2, N'A2', N'Radioactive, License Exempt')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (3, N'A3', N'Radioactive, License Exempt, Authorized')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (4, N'B1', N'Alkali, Corrosive, Inorganic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (5, N'B2', N'Alkali, Corrosive, Organic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (6, N'B3', N'Alkali, Low Risk')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (7, N'C1', N'Acid, Corrosive, Inorganic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (8, N'C2', N'Acid, Corrosive, Organic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (9, N'C3', N'Acid, Low Risk')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (10, N'D1', N'Oxidizer')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (11, N'D2', N'Oxidizer and Poison')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (12, N'D3', N'Oxidizer and Corrosive - Acidic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (13, N'D4', N'Oxidizer and Corrosive - Alkali')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (14, N'E1', N'Explosives, Military')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (15, N'E2', N'Explosives, Low Risk')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (16, N'F1', N'Flammable Liquid, Packing Group I, OSHA IA')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (17, N'F2', N'Flammable Liquid, Packing Group II, OSHA IB')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (18, N'F3', N'Flammable Liquid, Packing Group III, OSHA IC')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (19, N'F4', N'Flammable Liquid, Packing Group III, OSHA II')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (20, N'F5', N'Flammable Liquid and Poison')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (21, N'F6', N'Flammable Liquid and Corrosive, Acidic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (22, N'F7', N'Flammable Liquid and Corrosive, Alkali')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (23, N'F8', N'Flammable Solid')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (24, N'G1', N'Gas, Poison (Nonflammable)')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (25, N'G2', N'Gas, Flammable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (26, N'G3', N'Gas Non-flammable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (27, N'G4', N'Gas Non-flammable, Oxidizer')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (28, N'G5', N'Gas Non-flammable, Corrosive')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (29, N'G6', N'Gas, Poison, Corrosive (Nonflammable)')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (30, N'G7', N'Gas, Poison, Oxidizer (Nonflammable)')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (31, N'G8', N'Gas, Poison, Flammable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (32, N'G9', N'Gas, Poison, Corrosive, Oxidizer (Nonflammable)')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (33, N'H1', N'Hazard Characteristics Not Yet Determined')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (34, N'K1', N'Infectious Substance')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (35, N'K2', N'Cytotoxic Drugs')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (36, N'M1', N'Magnetized Material')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (37, N'N1', N'Not regulated as Hazardous')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (38, N'P1', N'Peroxide, Organic, DOT Regulated')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (39, N'P2', N'Peroxide, Organic, Low Risk')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (40, N'R1', N'Reactive Chemical, Flammable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (41, N'R2', N'Water Reactive Chemical')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (42, N'T1', N'DOT Poison-Inhalation Hazard')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (43, N'T2', N'UN Poison, Packing Group I')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (44, N'T3', N'UN Poison, Packing Group II')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (45, N'T4', N'UN Poison, Packing Group III')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (46, N'T5', N'Pesticide, Low Risk')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (47, N'T6', N'Health Hazard')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (48, N'T7', N'Carcinogen')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (49, N'V1', N'UN Class 9 Miscellaneous Hazardous Materials')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (50, N'V2', N'Aerosol, Nonflammable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (51, N'V3', N'Aerosol, Flammable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (52, N'V4', N'DOT Combustible Liquid, OSHA IIIA')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (53, N'V5', N'High Flash Point Materials, OSHA IIIB')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (54, N'V6', N'Petroleum Products')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (55, N'V7', N'Environmental Hazard')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (56, N'X1', N'Multiple Hazards Under One National Stock Number (NSN)')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (57, N'Z1', N'Article Containing Asbestos')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (58, N'Z2', N'Article Containing Mercury')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (59, N'Z3', N'Article Containing Polychlorinated Biphenyl (PCB)')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (60, N'Z4', N'Article, Battery, Lead Acid, Nonspillable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (61, N'Z5', N'Article, Battery, Nickel Cadmium, Nonspillable')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (62, N'Z6', N'Article, Battery, Lithium')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (63, N'Z7', N'Article, Battery, Dry Cell')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (64, N'C4', N'Acid, Corrosive and Oxidizer, Inorganic')
INSERT [dbo].[hcc] ([hcc_id], [hcc], [description]) VALUES (65, N'C5', N'Acid, Corrosive and Oxidizer, Organic')
SET IDENTITY_INSERT [dbo].[hcc] OFF
SET IDENTITY_INSERT [dbo].[shelf_life_action_code] ON 

INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (1, N'00', N'Not deteriorative.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (2, N'C_', N'Incorporate all mandatory changes, etc. If found satisfactory, reclassify to the number of months indicated after which the item is considered unsuitable for restoration to issuable status. Shelf life codes will be used to identify the number of months for which the item is reclassified (i.e., 7C2 indicates an item having normal shelf life of 3 years may be extended for 6 months after incorporation of changes).')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (3, N'C0', N'Check/inspect/test in accordance with inventory manager''s instructions.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (4, N'C2', N'Incorporate all mandatory changes, perform minor adjustments required, clean and relubricate bearings, reassemble, test to post overhaul standards and correct any observed discrepancies. Items which pass tests shall be returned to stock as RFI. Exterior package marking of such items shall indicate the latest check and test date and the original date of manufacture. Items which fail test shall be placed in "F" condition.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (5, N'L_', N'  to be tested by the Laboratory/Activity:in increments after the initial time limit has expired. The letter or number following the "L" indicates increment in which laboratory tests are required. Example: 4Ll - After 12 months, and every three months thereafter, a sample should be submitted to the Laboratory/Activity for testing. If item fails test, take disposition action.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (11, N'RD', N'Replace all deteriorated and nonmetallic components subject to deterioration (disassemble and process to the level required to permit replacement of deteriorable items; test to post overhaul standards and return to stock as RFI item with fully restored storage time limitations). Exterior package marking of such items shall indicate the latest date of overhaul.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (12, N'RJ', N'This is assigned to fuel metering equipment which has been tested by other than MIL-F-7024A.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (13, N'RN', N'Provides for equipment that has been tested with fluids indicted by Specification MIL-F-7024A and has not subsequently been operated with other fluids. (Use for fuel metering equipment only.)')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (14, N'S9', N'Identification of Safety Items. A safety item designated by the Navy that is subject to a 5-year age limitation when used for proposes involving safety of personnel. Material in this category that is over 5 years old will not be used for repair or modification of personnel, drag, or special parachutes, or other uses directly involving personnel safety. Use advice code 2H unless material is being used for cargo parachutes or other uses not involving personnel safety.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (15, N'SA', N'Salvage.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (16, N'SB', N'Request cannibalization/salvage instructions from inventory manager.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (17, N'T_', N'Test: If the item passes, extend life by number of months indicated by the number following "T", after which process in accordance with Code RD.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (18, N'UU', N'Unsuitable for restoration to issuable status. At end of shelf life period material will be disposed of in accordance with existing instructions.')
INSERT [dbo].[shelf_life_action_code] ([shelf_life_action_code_id], [slac], [description]) VALUES (19, N'X_', N'Test: If passed, redate item to the number of months/years indicated by the Shelf Life Code. If item fails tests, dispose of it in accordance with existing instructions. All material that exceeds the age from date of manufacture to the age indicated by the character following the letter "X" will be disposed of in accordance with existing instructions.')
SET IDENTITY_INSERT [dbo].[shelf_life_action_code] OFF
SET IDENTITY_INSERT [dbo].[shelf_life_code] ON 

INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (1, N'0', NULL, N'non-deteriorative', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (2, N'1', 3, N'3 months ', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (3, N'2', 6, N'6 months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (4, N'3', 9, N'9 months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (5, N'4', 12, N'12 months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (11, N'5', 18, N'18 Months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (12, N'6', 24, N'24 Months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (13, N'7', 36, N'36 Months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (14, N'8', 48, N'48 Months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (15, N'9', 60, N'60 Months', N'TYPE II (EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (16, N'A', 1, N'1 Month', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (17, N'B', 2, N'2 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (18, N'C', 3, N'3 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (19, N'D', 4, N'4 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (20, N'E', 5, N'5 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (21, N'F', 6, N'6 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (22, N'G', 9, N'9 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (23, N'H', 12, N'12 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (24, N'J', 15, N'15 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (25, N'K', 18, N'18 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (26, N'L', 21, N'21 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (27, N'M', 24, N'24 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (28, N'N', 27, N'27 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (29, N'P', 30, N'30 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (30, N'Q', 36, N'36 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (31, N'R', 48, N'48 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (32, N'S', 60, N'60 Months', N'TYPE I (NON-EXTENDABLE)')
INSERT [dbo].[shelf_life_code] ([shelf_life_code_id], [slc], [time_in_months], [description], [type]) VALUES (34, N'X', 61, N'Greater than 60 Months', N'TYPE II (EXTENDABLE)')
SET IDENTITY_INSERT [dbo].[shelf_life_code] OFF
SET IDENTITY_INSERT [dbo].[smcc] ON 

INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (1, N'2', N' ESD/EM Sensitive Item')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (2, N'3', N'ESD Sensitive Item')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (3, N'4', N'EM Sensitive Item')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (4, N'5', N'Hazardous Material, N.O.S.')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (5, N'6', N'Explosive, Ordnance Item')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (6, N'7', N'Dangerous When Wet')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (7, N'8', N'Spontaneously Combustible')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (8, N'9', N'Non-Hazardous or Non-Sensitive')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (17, N'A', N'Medical Items')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (18, N'B', N'Flammable Compressed Gas')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (19, N'C', N'Corrosive Solids/Liquids (other than acid)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (20, N'D', N'Alcohol (Ethanol, Ethyl Alcohol, or Grain Alcohol)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (21, N'E', N'Precious Metals')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (22, N'F', N'Flammable Liquids (<141 degrees F.)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (23, N'G', N'Combustible Liquids (>141 degrees F to 200 degree')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (24, N'H', N'PCB')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (25, N'I', N'Mercury (Not Authorized for Submarine Use)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (26, N'J', N'Oxidizing Material')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (27, N'K', N'Organic Peroxides')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (28, N'L', N'Other Reg. Material (ORM) and UN Class 9 (Consumer Commodities - e.g., Perfume, Spray Paint, Charcoal, etc.)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (29, N'M', N'Magnetic Material')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (30, N'N', N'Asbestos')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (31, N'O', N'Mercury (Not Authorized for Shipboard Use)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (32, N'P', N'Poison (Including Methanol, Wood Alcohol, and Denatured Alcohol)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (33, N'Q', N'Explosive, Non-Ordnance')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (34, N'R', N'Radioactive Material')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (35, N'S', N'Oils/Petroleum Products N.O.S.')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (36, N'T', N'Toxic')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (37, N'U', N'Mercury (Authorized for General Use)')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (38, N'V', N'Acid')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (39, N'W', N'Non-Flamable Compressed Gas')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (40, N'X', N'Radioactive & Magnetic Material')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (41, N'Y', N'Non-Magnetic')
INSERT [dbo].[smcc] ([smcc_id], [smcc], [description]) VALUES (42, N'Z', N'Flammable Solids')
SET IDENTITY_INSERT [dbo].[smcc] OFF
SET IDENTITY_INSERT [dbo].[storage_type] ON 

INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (1, N'A', N' General Purpose, Unheated')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (2, N'B', N'General Purpose, Heated')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (3, N'C', N' General Purpose, Controlled Humidity (maximum 40 degrees RH ashore)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (4, N'D', N'Heavy Duty, Unheated (overhead crane area)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (5, N'E', N'Heavy Duty, Heated (overhead crane area)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (11, N'F', N'Heavy Duty, Controlled Humidity (overhead crane area)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (12, N'G', N'Flammable')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (13, N'H', N'Freeze (below 32 degrees F)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (14, N'I', N'Chill (between 32 degrees F and 50 degrees F)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (15, N'J', N'Shed')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (16, N'K', N'Open')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (17, N'L', N'Explosive Storage (non-ordnance items, such as explosive bolts and rivets)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (18, N'M', N'Acid Storage')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (19, N'N', N'Inert Compressed Gas Storage (NAVSEA Technical Manual, Chapter 9230, Section 23 (Stowage of Compressed Gases, General) and Section 24 (Stowage Precautions) provides stowage requirements and safety precautions for compressed gases)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (20, N'O', N'Special Storage (requires specific authority and stowage instructions)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (21, N'P', N'Separate Storage (Fir Producers, not elsewhere classified) (Keep away from acid, combustible, organic and readily oxidizable materials)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (22, N'Q', N'Warehouse/Flammable Storage (prohibited for shipboard storage)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (23, N'R', N'Warehouse/General Storage (no special requirements) (Prohibited for shipboard storage)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (24, N'S', N'Warehouse/Special Storage (requires specific authority and storage instructions) (Prohibited for shipboard storage)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (25, N'T', N'Warehouse/Separate Storage (fire producers) (Keep away from acid, combustible, organic and readily oxidazable materials) (Prohibited for shipboard storage)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (26, N'U', N'Flammable Compressed Gas (NAVSEA Technical Manual, Chapter 9230, Section 23 (Stowage of Compressed Gases, General) and Section 24 (Stowage Precautions) provides stowage requirements and safety precautions for compressed gases)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (27, N'V', N'Oxidizing Compressed Gas (NAVSEA Technical Manual, Chapter 9230, Section 23 (Stowage of Compressed Gases, General) and Section 24 (Stowage Precautions) provides stowage requirements and safety precautions for compressed gases)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (28, N'W', N'Poisonous Compressed Gas (NAVSEA Technical Manual, Chapter 9230, Section 23 (Stowage of Compressed Gases, General) and Section 24 (Stowage Precautions) provides stowage requirements and safety precautions for compressed gases)')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (29, N'X', N'Radioactive material. Store in a designated radioactive material area in accordance with Afloat Supply Procedures. NAVSUP Pub 485.')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (30, N'Y', N'Ship Critical Material (SCM), major ship equipment and/or components - store indoors and package/preserve appropriately')
INSERT [dbo].[storage_type] ([storage_type_id], [type], [description]) VALUES (31, N'Z', N'Ship Critical Material (SCM), major ship equipment and/or components - store outdoors under cover and package/preserve appropriately')
SET IDENTITY_INSERT [dbo].[storage_type] OFF
SET IDENTITY_INSERT [dbo].[ui] ON 

INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (1, N'AM', N'Ampoule')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (2, N'AY', N'Assembly')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (3, N'AT', N'Assortment')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (4, N'BG', N'Bag')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (5, N'BE', N'Bale')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (6, N'BA', N'Ball')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (7, N'BR', N'Bar')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (8, N'BL', N'Barrel')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (9, N'BF', N'Board Foot')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (10, N'BO', N'Bolt')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (11, N'BK', N'Book')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (12, N'BT', N'Bottle')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (13, N'BX', N'Box')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (14, N'BD', N'Bundle')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (15, N'CK', N'Cake')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (16, N'CN', N'Can')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (17, N'CB', N'Carboy')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (18, N'CT', N'Carton')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (19, N'CA', N'Cartridge')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (20, N'CS', N'Case')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (21, N'CL', N'Coil')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (22, N'CE', N'Cone')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (23, N'CO', N'Container')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (24, N'CF', N'Cubic Foot')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (25, N'CZ', N'Cubic Meter')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (26, N'CD', N'Cubic Yard')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (27, N'CY', N'Cylinder')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (28, N'DZ', N'Dozen')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (29, N'DR', N'Drum')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (30, N'EA', N'Each')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (31, N'FY', N'Fifty')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (32, N'FV', N'Five')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (33, N'FT', N'Foot')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (34, N'GL', N'Gallon')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (35, N'GR', N'Gross')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (36, N'GP', N'Group')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (37, N'HK', N'Hank')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (38, N'HD', N'Hundred')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (39, N'IN', N'Inch')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (40, N'JR', N'Jar')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (41, N'KT', N'Kit')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (42, N'LG', N'Length')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (43, N'LI', N'Liter')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (44, N'ME', N'Meal')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (45, N'MR', N'Meter')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (46, N'OZ', N'Ounce')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (47, N'OT', N'Outfit')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (48, N'PK', N'Pack')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (49, N'PG', N'Package')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (50, N'PZ', N'Packet')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (51, N'PD', N'Pad')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (52, N'PR', N'Pair')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (53, N'PT', N'Pint')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (54, N'PM', N'Plate')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (55, N'LB', N'Pound')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (56, N'QT', N'Quart')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (57, N'RA', N'Ration')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (58, N'RM', N'Ream')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (59, N'RL', N'Reel')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (60, N'RO', N'Roll')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (61, N'SE', N'Set')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (62, N'SH', N'Sheet')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (63, N'SO', N'Shot')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (64, N'SK', N'Skein')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (65, N'SD', N'Skid')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (66, N'SL', N'Spool')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (67, N'SF', N'Square Feet')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (68, N'SY', N'Square Yard')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (69, N'SX', N'Stick')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (70, N'SP', N'Strip')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (71, N'TE', N'Ten')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (72, N'TS', N'Thirty-Six')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (73, N'MX', N'Thousand')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (74, N'MC', N'Thousand Cubic Feet')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (75, N'TI', N'Tin')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (76, N'TN', N'Ton')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (77, N'TO', N'Troy Ounce')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (78, N'TU', N'Tube')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (79, N'TD', N'Twenty')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (80, N'TF', N'Twenty-Five')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (81, N'VI', N'Vial')
INSERT [dbo].[ui] ([ui_id], [abbreviation], [description]) VALUES (82, N'YD', N'Yard')
SET IDENTITY_INSERT [dbo].[ui] OFF
SET IDENTITY_INSERT [dbo].[usage_category] ON 

INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (1, N'X', N'Prohibited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (2, N'R', N'Restricted')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (3, N'L', N'Limited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (4, N'N', N'Permitted, no restrictions')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (5, N'U', N'Not yet evaluated')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (11, N'L/X', N'Limited/Prohibited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (12, N'L/R', N'Limited/Restricted')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (13, N'X/X', N'Prohibited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (14, N'L/L', N'Limited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (15, N'R/X', N'Restricted/Prohibited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (16, N'N/L', N'Permitted, no restrictions/Limited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (17, N'R/L', N'Restricted/Limited')
INSERT [dbo].[usage_category] ([usage_category_id], [category], [description]) VALUES (18, N'X/L', N'Prohibited/Limited')
SET IDENTITY_INSERT [dbo].[usage_category] OFF

CREATE TABLE [dbo].[TipOfTheDay](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TipText] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_TipOfTheDay] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET IDENTITY_INSERT [dbo].[TipOfTheDay] ON 


INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (1, N'Store organic acids in an acid locker separated by a partition or by at least three feet from all other materials.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (2, N'When transferring HM from one container to another, ensure that the new container has the appropriate precautionary labeling.  Select the ""Labels"" button in the Inventory screen to print labels.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (3, N'Ensure that all HM Restricted, Limited, or Prohibited by the SMCL is properly tagged at all times.  Select the "Labels" button in the Inventory screen to print labels.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (4, N'HM Prohibited by the SMCL is not allowed onboard at any time.  If a prohibited item is found, make arrangements for removal/disposal as soon as possible.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (5, N'HM Restricted by the SMCL is not allowed onboard while underway unless specifically authorized by the XO.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (6, N'All PPE listed by an MSDS must be accessible to anyone who uses that material and must be in good condition.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (7, N'Mark stowage compartments to identify type of HM stored and keep the compartment/materials clean and dry at all times.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (8, N'Do not transfer material to any container that was previously used for a different material without first checking the materials'' compatibility.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (9, N'Issue material on a first-in, first-out basis, considering shelf life.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (10, N'Stow HM only in a container which is compatible to the material (e.g., do not place corrosive materials in metal drums).')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (11, N'Seal and protect all containers against physical damage and secure for heavy seas.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (12, N'Upon completion of hazardous material use, return surplus material to its appropriate storage location.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (13, N'Flammable and combustible materials shall be stored separately from oxidizing materials.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (14, N'Never permit open flames or spark producing items in HM stowage areas.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (15, N'Operate only explosion-proof electrical equipment in a potentially explosive environment.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (16, N'Maintain explosion-proof electrical fixtures in proper condition in applicable HM stowage areas.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (17, N'Keep suitable fire extinguishing equipment and materials ready at all times for emergency use.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (18, N'Store all toxic materials in cool, dry, well ventilated locations separated from all sources of ignition, acids and acid mists/vapors, caustics, and oxidizers.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (19, N'Do not store oxidizers in an area adjacent to a torpedo room or small arms ammunition storage or heat source or where the maximum temperature exceeds 100 F under normal operating conditions.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (20, N'Only stow compressed gases in compartments and locations designated for cylinder storage.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (21, N'When testing for leaking gas cylinders, use soapy water or leak-detection compound conforming to MIL-L-25567.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (22, N'Aerosol spray cans are prohibited aboard submarines except as specifically allowed by appendix A of the Nuclear Powered Submarine Atmosphere Control Manual, NAVSEA S9510-AB-ATM-010.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (23, N'Do not mix batteries of different chemical types, sizes, or manufacturers.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (24, N'DO NOT enter or allow anyone to enter any fuel tank or storeroom that has not been declared safe for entry by the Gas-Free Engineer.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (25, N'Clean solvent-spilled areas using detergents and water.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (26, N'Keep HM containers closed and properly stowed when not in use.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (27, N'Use soap and water to remove paint from skin.  NEVER use solvents such as paint thinner or kerosene to clean paints from the skin.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (28, N'Store paint materials away from acids and strong oxidizing agents.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (29, N'Use and store lubricants/oils in areas free from sources of ignition such as sparks, open flames, and oxidizers.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (30, N'Always add an acid slowly to water.  NEVER ADD WATER TO ANY ACID.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (31, N'Never mix a HM with another substance unless specifically instructed by written procedures.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (32, N'Clear all unprotected personnel from the spill area.  (NOTE:  evacuation is only required for a large spill of HM which endangers personnel.)')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (33, N'Return all HMs to proper storage upon completion of the job.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (34, N'DO NOT use cleaning compounds on hot surfaces unless instructed by directions.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (35, N'Use only approved wrenches/tools for opening the valve of the compressed gas cylinder.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (36, N'Store and label used/excess HMs in approved drums or containers.')

INSERT [dbo].[TipOfTheDay] ([ID], [TipText]) VALUES (37, N'Hit F5 key to refresh SHIMS display screens to reflect changes made by other users who are currently logged into SHIMS.')

SET IDENTITY_INSERT [dbo].[TipOfTheDay] OFF

CREATE TABLE [dbo].[reference_library](
	[reference_id] [int] IDENTITY(1,1) NOT NULL,
	[doc_title] [nvarchar](max) NOT NULL,
	[filename] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_reference_library] PRIMARY KEY CLUSTERED 
(
	[reference_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


SET IDENTITY_INSERT [dbo].[reference_library] ON 


INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (1, N'5100.10E CHAPTER B3 "HAZARDOUS MATERIAL CONTROL AND MANAGEMENT"', N'b3_19e.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (2, N'5100.19E CHAPTER D15 "SUBMARINE HAZARDOUS MATERIAL CONTROL AND MANAGEMENT STANDARDS"', N'd15_19e.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (3, N'OPNAV M-5090.1 CHAPTER 35 - "ENVIRONMENTAL COMPLIANCE AFLOAT"', N'OPNAV_M-5090.1D.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (4, N'ACM CHAPTER 8 "SUBMARINE MATERIAL CONTROL"', N'ACM_Chapter_7_Rev_6.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (5, N'ACM TABLE A-1 PROHIBITED CHEMICALS/SUBSTANCES', N'ACM_Appendix_A_Rev_6.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (8, N'TABLE A-2 CHEMICALS/SUBSTANCES', N'ACM_Appendix_A_Rev_6.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (10, N'NSTM 670 VOL 1 - AFLOAT HAZMAT CONTROL AND MANAGEMENT GUIDELINES', N'NSTM_670_VOL_1_HMC&M_GUIDELINES.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (11, N'NSTM 670 VOL 1 APPENDIX E - SHIPBOARD HAZMAT COMPATIBILITY MATRIX', N'NSTM_670_Append_E1_Compat_Chart.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (12, N'NSTM 670 VOL 2 - AFLOAT HAZMAT CONTROL AND MANAGEMENT GUIDELINES - HMUG', N'NSTM_670_VOL_2_HMUG.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (13, N'NSTM 670 FIGURE 670-3-4 HM SIMPLIFIED COMPATIBILITY CHART', N'Simplified_Chart.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (14, N'CSLCSPINTST 4406.1 SUBMARINE SUPPLY PROCEDURES MANUAL', N'CSLCSPINST_4406-1A.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (15, N'HCC CODE DEFINITIONS', N'HCC_Code_Definition.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (16, N'SMCC CODE DEFINITIONS', N'SMCC_Code_Definition.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (17, N'DD 2477-3 LABELS', N'DD2477-3.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (18, N'DD 2522 LABELS', N'DD2522.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (19, N'SHIMS TRAINING BOOK', N'SHIMS_Training_Book.pdf')

INSERT [dbo].[reference_library] ([reference_id], [doc_title], [filename]) VALUES (20, N'SHIMS USER MANUAL', N'UsersGuide.pdf')

SET IDENTITY_INSERT [dbo].[reference_library] OFF

CREATE TABLE [dbo].[catalog_groups](
	[catalog_group_id] [bigint] IDENTITY(1,1) NOT NULL,
	[group_name] [nvarchar](255) NULL,
 CONSTRAINT [PK_catalog_groups] PRIMARY KEY CLUSTERED 
(
	[catalog_group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET IDENTITY_INSERT [dbo].[catalog_groups] ON 


INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (9, N'A-12. LUBRICANTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (10, N'A-21. MISCELLANEOUS ITEMS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (11, N'A-4. CLEANING AGENTS AND DETERGENTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (12, N'A-16. PESTICIDES AND INSECTICIDES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (13, N'A-19. PRESERVATIVES/ANTI-CORROSION AGENTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (14, N'A-7. DUPLICATING PRODUCTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (15, N'A-5. COATINGS AND SEALANTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (16, N'A-30. SPECWAR/ORDNANCE')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (17, N'A-29. ALKALIES/BASES/CAUSTICS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (18, N'A-15. PERSONAL HYGIENE ITEMS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (19, N'A-13. OFFICE SUPPLIES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (20, N'A-25. PACKAGING/PACKING MATERIALS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (21, N'A-27. COMPRESSED GASES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (22, N'A-10. DECK FINISHES AND WAXES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (23, N'A-26. BATTERIES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (24, N'A-6. DECK COVERINGS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (25, N'A-11. INSULATING MATERIALS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (26, N'A-23. WATER TREATMENT PRODUCTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (27, N'A-14. PAINTS AND VARNISHES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (28, N'A-22. SOLDERS, SOLDERING FLUXES AND CLEANERS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (29, N'A-20. SOLVENTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (30, N'A-24. PLASTIC/POLYMERIC MATERIALS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (31, N'A-18. POLISHES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (32, N'A-3. ADHESIVES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (33, N'A-17. PHOTOGRAPHIC SUPPLIES')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (34, N'A-28. ACIDS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (35, N'A-9. ELECTRICAL COMPONENTS')

INSERT [dbo].[catalog_groups] ([catalog_group_id], [group_name]) VALUES (36, N'A-8. DYE PENETRANTS')

SET IDENTITY_INSERT [dbo].[catalog_groups] OFF

CREATE TABLE [dbo].[config](
	[backup_frequency] [int] NULL,
	[last_backup_date] [datetime] NULL,
	[backup_location] [nvarchar](max) NULL,
	[config_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_config] PRIMARY KEY CLUSTERED 
(
	[config_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


SET IDENTITY_INSERT [dbo].[config] ON 

INSERT [dbo].[config] ([backup_frequency], [last_backup_date], [backup_location], [config_id]) VALUES (16, NULL, N'C:\SHIMS2\Backups\', 1)
SET IDENTITY_INSERT [dbo].[config] OFF
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [DF_config_backup_frequency]  DEFAULT ((16)) FOR [backup_frequency]
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [default_backup_location]  DEFAULT ('C:\SHIMS2\Backups\') FOR [backup_location]

CREATE TABLE [dbo].[hazard_warnings](
	[hazard_warning_id] [bigint] IDENTITY(1,1) NOT NULL,
	[hazard_id_1] [bigint] NULL,
	[hazard_id_2] [bigint] NULL,
	[warning_level] [nvarchar](50) NULL,
 CONSTRAINT [PK_hazard_warnings] PRIMARY KEY CLUSTERED 
(
	[hazard_warning_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET IDENTITY_INSERT [dbo].[hazard_warnings] ON 


INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (37, 1, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (38, 1, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (39, 1, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (40, 1, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (41, 1, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (42, 1, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (43, 1, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (44, 1, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (45, 1, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (46, 1, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (47, 1, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (48, 1, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (49, 1, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (51, 2, 2, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (52, 2, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (53, 2, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (54, 2, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (55, 2, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (56, 2, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (57, 2, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (58, 2, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (59, 2, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (60, 2, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (61, 2, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (62, 2, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (63, 2, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (66, 3, 4, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (67, 3, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (68, 3, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (69, 3, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (70, 3, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (71, 3, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (72, 3, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (73, 3, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (74, 3, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (75, 3, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (76, 3, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (77, 4, 4, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (79, 4, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (80, 4, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (81, 4, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (82, 4, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (83, 4, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (84, 4, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (85, 4, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (86, 4, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (87, 4, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (88, 4, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (89, 5, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (90, 5, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (91, 5, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (92, 5, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (93, 5, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (94, 5, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (95, 5, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (96, 5, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (97, 5, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (98, 6, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (99, 6, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (100, 6, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (101, 6, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (102, 6, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (103, 6, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (104, 6, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (105, 6, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (106, 7, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (107, 7, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (108, 7, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (109, 7, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (110, 7, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (111, 7, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (112, 7, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (113, 8, 8, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (114, 8, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (115, 8, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (116, 8, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (117, 8, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (118, 8, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (119, 8, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (120, 9, 9, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (121, 9, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (122, 9, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (123, 9, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (124, 9, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (125, 9, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (126, 10, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (127, 10, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (128, 10, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (129, 11, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (130, 11, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (131, 12, 12, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (132, 12, 13, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (133, 12, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (134, 13, 14, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (135, 2, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (136, 3, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (137, 4, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (138, 5, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (139, 6, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (140, 7, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (141, 8, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (142, 9, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (143, 10, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (144, 11, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (145, 12, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (146, 13, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (147, 14, 1, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (148, 2, 2, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (149, 3, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (150, 4, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (151, 5, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (152, 6, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (153, 7, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (154, 8, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (155, 9, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (156, 10, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (157, 11, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (158, 12, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (159, 13, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (160, 14, 2, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (161, 4, 3, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (162, 5, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (163, 6, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (164, 7, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (165, 8, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (166, 9, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (167, 10, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (168, 11, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (169, 12, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (170, 13, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (171, 14, 3, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (172, 4, 4, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (173, 5, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (174, 6, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (175, 7, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (176, 8, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (177, 9, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (178, 10, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (179, 11, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (180, 12, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (181, 13, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (182, 14, 4, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (183, 6, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (184, 7, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (185, 8, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (186, 9, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (187, 10, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (188, 11, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (189, 12, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (190, 13, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (191, 14, 5, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (192, 7, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (193, 8, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (194, 9, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (195, 10, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (196, 11, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (197, 12, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (198, 13, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (199, 14, 6, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (200, 8, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (201, 9, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (202, 10, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (203, 11, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (204, 12, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (205, 13, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (206, 14, 7, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (207, 8, 8, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (208, 9, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (209, 10, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (210, 11, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (211, 12, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (212, 13, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (213, 14, 8, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (214, 9, 9, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (215, 10, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (216, 11, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (217, 12, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (218, 13, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (219, 14, 9, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (220, 11, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (221, 12, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (222, 14, 10, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (223, 12, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (224, 14, 11, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (225, 12, 12, N'O')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (226, 13, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (227, 14, 12, N'X')

INSERT [dbo].[hazard_warnings] ([hazard_warning_id], [hazard_id_1], [hazard_id_2], [warning_level]) VALUES (228, 14, 13, N'X')

SET IDENTITY_INSERT [dbo].[hazard_warnings] OFF
