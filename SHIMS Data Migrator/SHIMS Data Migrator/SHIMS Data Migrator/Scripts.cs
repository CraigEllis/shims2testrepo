﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHIMS_Data_Migrator
{
    class Scripts
    {
        public static string CreateTargetDatabaseSql(string targetDb)
        {
            return " IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'" + targetDb + "') " +
                        " BEGIN " +
                        "ALTER DATABASE [" + targetDb + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE; " +
                        " DROP DATABASE [" + targetDb + "]; " +
                        " CREATE DATABASE [" + targetDb + "]; " +
                        " END " +
                        " ELSE " +
                        " BEGIN " +
                        " CREATE DATABASE [" + targetDb + "];" +
                        " END; " +
                        "use master; EXEC sp_configure 'show advanced options', 1;" +
                        "RECONFIGURE; EXEC sp_configure 'xp_cmdshell', 1; RECONFIGURE";
        }

        public static string CreateDefaultUserSql(string targetDb)
        {
            return @"if not exists (select loginname from master.dbo.syslogins where name='shims' and dbname='" + targetDb + "') begin CREATE LOGIN shims WITH PASSWORD = 'shims', DEFAULT_DATABASE=[" + targetDb + "], CHECK_POLICY= OFF; end ALTER SERVER ROLE [dbcreator] ADD MEMBER [shims]" +
                        " use [" + targetDb + "];" +
                        " if not exists (select name from sys.database_principals where name='shims') begin create user shims for login shims;  end" +
                        " EXEC sp_addrolemember N'db_datareader', N'shims'" +
                        " EXEC sp_addrolemember N'db_backupoperator', N'shims'" +
                        " EXEC sp_addrolemember N'db_ddladmin', N'shims'" +
                        " EXEC sp_addrolemember N'db_datawriter', N'shims'; " +
                        " use master; GRANT CREATE DATABASE to shims; " +
                        "use msdb;" +
                        "if not exists (select name from sys.database_principals where name='shims') begin " +
                        " CREATE USER [shims] end " +
                        " ALTER ROLE [SQLAgentUserRole] ADD MEMBER [shims]; GRANT SELECT to shims";
        }

        public static string InsertInventorySql()
        {
            return "set identity_insert inventory_detail ON; insert into inventory_detail (" +
                    " inventory_detail_id, " +
                    " fsc, " +
                    " niin, " +
                    " item_name, " +
                    " ui_id, " +
                    " um, " +
                    " smcc_id, " +
                    " allowance_quantity, " +
                    " notes, " +
                    " slac_id, " +
                    " slc_id) " +
                    " values" +
                    " (@inventory_detail_id, " +
                    " @fsc, " +
                    " @niin, " +
                    " @item_name, " +
                    " @ui_id, " +
                    " @um, " +
                    " @smcc_id, " +
                    " @allowance_quantity," +
                    " @notes, " +
                    " @slac_id, " +
                    " @slc_id ) set identity_insert inventory_detail OFF";
        }

        public static string InsertInventoryOnHandSql()
        {
            return @"insert into inventory_onhand (" +
                    " location_id, " +
                    " qty, " +
                    " inventoried_date, " +
                    " onboard_date, " +
                    " expiration_date, " +
                    " manufacturer, " +
                    " cage, " +
                    " inventory_detail_id, mfg_catalog_id) VALUES " +
                    " (@location_id, " +
                    " @qty, " +
                    " @inventoried_date, " +
                    " @onboard_date, " +
                    " @expiration_date, " +
                    " @manufacturer, " +
                    " @cage, " +
                    " (select inventory_detail_id from inventory_detail id where id.fsc = @fsc and id.niin = @niin),  (select top 1 mfg_catalog_id from mfg_catalog where mfg_catalog.niin = @niin and mfg_catalog.cage = @cage)); ";
        }

        public static string InsertOffloadList()
        {
            return @"set identity_insert offload_list ON;
                    insert into offload_list (offload_list_id, deleted, fsc, niin, cage, description, location_id, offload_qty, offload_date, ui, um, add_data_4) values (@offload_list_id, 0, @fsc, @niin, @cage, @description, @location_id, @offload_qty, @offload_date, @ui, @um, @add_data_4);
                    set identity_insert offload_list OFF;";
        }

        public static string InsertDD1348()
        {
            return @"insert into dd_1348
                        (offload_list_id, document_number, doc_ident, ri_from, m_and_s, ser, suppaddress, sig, fund, distribution, project, pri, reqd_del_date, adv, ri, op, mgt, condition, unit_price, total_price, ship_from, ship_to, mark_for, nmfc, frt_rate, cargo_type, ps, qty_recd, up, unit_weight, unit_cube, ufc, sl, item_name, ty_cont, no_cont, total_weight, total_cube, recd_by, date_recd, memo_add_1, memo_add_2)
                    VALUES (@offload_list_id, @document_number, @doc_ident, @ri_from, @m_and_s, @ser, @suppaddress, @sig, @fund, @distribution, @project, @pri, @reqd_del_date, @adv, @ri, @op, @mgt, @condition, @unit_price, @total_price, @ship_from, @ship_to, @mark_for, @nmfc, @frt_rate, @cargo_type, @ps, @qty_recd, @up, @unit_weight, @unit_cube, @ufc, @sl, @item_name, @ty_cont, @no_cont, @total_weight, @total_cube, @recd_by, @date_recd, @memo_add_1, @memo_add_2)";
        }

        public static string InsertOffloadArchived()
        {
            return @"insert into offload_list (deleted, fsc, niin, cage, description, location_id, offload_qty, offload_date, ui, um, add_data_4) values (0, @fsc, @niin, @cage, @description, @location_id, @offload_qty, @offload_date, @ui, @um, @add_data_4);
                    insert into archived (offload_list_id) (select scope_identity());
                    update offload_list set archived_id = (select scope_identity());";
        }

        public static string InsertWorkcenterSql()
        {
            return @"insert into workcenter (wid) values (@wid);";
        }

        public static string InsertLocationSql()
        {
            return @"insert into locations (name, workcenter_id) values (@name, @workcenter_id);";
        }

        public static string InsertMfgCatalogSql()
        {
            return @"insert into mfg_catalog (cage, manufacturer, msds_no, niin) values (@cage, @manufacturer, @msds_no, @niin)";
        }

        public static string InsertOffloadSql()
        {
            return @"insert into offload_list (fsc, niin, cage, location_id, offload_qty, offload_date, ui, um) values (@fsc, @niin, @cage, @location_id, @offload_qty, @offload_date, @ui, @um)";
        }

        public static string OffloadTriggersSql()
        {
            return @"
                execute ('CREATE TRIGGER [dbo].[dd_1348_pop] ON [dbo].[offload_list]
                   FOR INSERT 
                AS 
                DECLARE @Offloadid Bigint
                DECLARE @mfgCatalogId Bigint

                BEGIN
	                -- SET NOCOUNT ON added to prevent extra result sets from
	                -- interfering with SELECT statements.
	                SET NOCOUNT ON;

	                SELECT @Offloadid =(SELECT offload_list_id FROM INSERTED);
	                SELECT @MfgCatalogId = (SELECT mfg_catalog_id FROM INSERTED);
                    INSERT INTO dd_1348 (offload_list_id, unit_price, total_price)values(@Offloadid,''0'',''0'');

                END;')";
        }

        public static string InsertOffloadTriggers2()
        {
            return @"execute ('CREATE TRIGGER [dbo].[update_offload_list] ON [dbo].[archived]
                       AFTER INSERT
                    AS 
                    BEGIN
	                    -- SET NOCOUNT ON added to prevent extra result sets from
	                    -- interfering with SELECT statements.
	                    SET NOCOUNT ON;

	                    DECLARE @Offloadid Bigint
	                    DECLARE @Archivedid Bigint

	                    DECLARE archive_cursor CURSOR FOR
		                    SELECT archived_id, offload_list_id 
		                    FROM archived;

	                    OPEN archive_cursor

	                    FETCH NEXT FROM archive_cursor
	                    INTO @Archivedid, @Offloadid

	                    WHILE @@FETCH_STATUS = 0
	                    BEGIN

		                    UPDATE offload_list 
		                       SET archived_id = @Archivedid
		                     WHERE offload_list_id = @Offloadid

		                    FETCH NEXT FROM archive_cursor
		                    INTO @Archivedid, @Offloadid
	                    END
	                    CLOSE archive_cursor;
	                    DEALLOCATE archive_cursor;
                    END;')";
        }

        public static string InsertShipSql()
        {
            return @"insert into ships (hull_type, hull_number, uic, base, act_code, sndl, name, city, state, current_ship, zip, POC_Title) 
                        values (@hull_type, @hull_number, @uic, @base, @act_code, @sndl, @name, @city, @state, 0, @zip, @poc_title)";
        }
    }

}
