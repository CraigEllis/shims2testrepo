﻿namespace SHIMS_Data_Migrator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.sqlServerInstanceCombo = new System.Windows.Forms.ComboBox();
            this.winAuthRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.currUserLbl = new System.Windows.Forms.Label();
            this.currAuthLbl = new System.Windows.Forms.Label();
            this.currConLbl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl77 = new System.Windows.Forms.Label();
            this.authBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.passwordTxt = new System.Windows.Forms.TextBox();
            this.userNameTxt = new System.Windows.Forms.TextBox();
            this.sqlServerAuthRadio = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.mdbFilePath = new System.Windows.Forms.TextBox();
            this.findMdbFile = new System.Windows.Forms.Button();
            this.sqlDbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.migrateButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.migrationProgress = new System.Windows.Forms.ProgressBar();
            this.findMsdsaulFile = new System.Windows.Forms.Button();
            this.msdsAulBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "SQL Server Instance";
            // 
            // sqlServerInstanceCombo
            // 
            this.sqlServerInstanceCombo.FormattingEnabled = true;
            this.sqlServerInstanceCombo.Location = new System.Drawing.Point(135, 18);
            this.sqlServerInstanceCombo.Name = "sqlServerInstanceCombo";
            this.sqlServerInstanceCombo.Size = new System.Drawing.Size(245, 21);
            this.sqlServerInstanceCombo.TabIndex = 12;
            // 
            // winAuthRadio
            // 
            this.winAuthRadio.AutoSize = true;
            this.winAuthRadio.Checked = true;
            this.winAuthRadio.Location = new System.Drawing.Point(17, 19);
            this.winAuthRadio.Name = "winAuthRadio";
            this.winAuthRadio.Size = new System.Drawing.Size(140, 17);
            this.winAuthRadio.TabIndex = 0;
            this.winAuthRadio.TabStop = true;
            this.winAuthRadio.Text = "Windows Authentication";
            this.winAuthRadio.UseVisualStyleBackColor = true;
            this.winAuthRadio.CheckedChanged += new System.EventHandler(this.winAuthRadio_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.currUserLbl);
            this.groupBox1.Controls.Add(this.currAuthLbl);
            this.groupBox1.Controls.Add(this.currConLbl);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbl77);
            this.groupBox1.Controls.Add(this.authBtn);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.passwordTxt);
            this.groupBox1.Controls.Add(this.userNameTxt);
            this.groupBox1.Controls.Add(this.sqlServerAuthRadio);
            this.groupBox1.Controls.Add(this.winAuthRadio);
            this.groupBox1.Location = new System.Drawing.Point(27, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(352, 193);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Authentication";
            // 
            // currUserLbl
            // 
            this.currUserLbl.AutoSize = true;
            this.currUserLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currUserLbl.Location = new System.Drawing.Point(126, 161);
            this.currUserLbl.Name = "currUserLbl";
            this.currUserLbl.Size = new System.Drawing.Size(138, 13);
            this.currUserLbl.TabIndex = 20;
            this.currUserLbl.Text = "No Connection Established.";
            // 
            // currAuthLbl
            // 
            this.currAuthLbl.AutoSize = true;
            this.currAuthLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currAuthLbl.Location = new System.Drawing.Point(126, 139);
            this.currAuthLbl.Name = "currAuthLbl";
            this.currAuthLbl.Size = new System.Drawing.Size(138, 13);
            this.currAuthLbl.TabIndex = 19;
            this.currAuthLbl.Text = "No Connection Established.";
            // 
            // currConLbl
            // 
            this.currConLbl.AutoSize = true;
            this.currConLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currConLbl.Location = new System.Drawing.Point(126, 115);
            this.currConLbl.Name = "currConLbl";
            this.currConLbl.Size = new System.Drawing.Size(138, 13);
            this.currConLbl.TabIndex = 18;
            this.currConLbl.Text = "No Connection Established.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(57, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(49, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Connection";
            // 
            // lbl77
            // 
            this.lbl77.AutoSize = true;
            this.lbl77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl77.Location = new System.Drawing.Point(31, 139);
            this.lbl77.Name = "lbl77";
            this.lbl77.Size = new System.Drawing.Size(89, 13);
            this.lbl77.TabIndex = 15;
            this.lbl77.Text = "Authentication";
            // 
            // authBtn
            // 
            this.authBtn.Location = new System.Drawing.Point(252, 54);
            this.authBtn.Name = "authBtn";
            this.authBtn.Size = new System.Drawing.Size(82, 29);
            this.authBtn.TabIndex = 14;
            this.authBtn.Text = "Connect";
            this.authBtn.UseVisualStyleBackColor = true;
            this.authBtn.Click += new System.EventHandler(this.authBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "User name";
            // 
            // passwordTxt
            // 
            this.passwordTxt.Location = new System.Drawing.Point(78, 77);
            this.passwordTxt.Name = "passwordTxt";
            this.passwordTxt.Size = new System.Drawing.Size(157, 20);
            this.passwordTxt.TabIndex = 3;
            this.passwordTxt.UseSystemPasswordChar = true;
            // 
            // userNameTxt
            // 
            this.userNameTxt.Location = new System.Drawing.Point(78, 51);
            this.userNameTxt.Name = "userNameTxt";
            this.userNameTxt.Size = new System.Drawing.Size(157, 20);
            this.userNameTxt.TabIndex = 2;
            // 
            // sqlServerAuthRadio
            // 
            this.sqlServerAuthRadio.AutoSize = true;
            this.sqlServerAuthRadio.Location = new System.Drawing.Point(183, 19);
            this.sqlServerAuthRadio.Name = "sqlServerAuthRadio";
            this.sqlServerAuthRadio.Size = new System.Drawing.Size(151, 17);
            this.sqlServerAuthRadio.TabIndex = 1;
            this.sqlServerAuthRadio.Text = "SQL Server Authentication";
            this.sqlServerAuthRadio.UseVisualStyleBackColor = true;
            this.sqlServerAuthRadio.CheckedChanged += new System.EventHandler(this.sqlServerAuthRadio_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 270);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "SHIMS Data";
            // 
            // mdbFilePath
            // 
            this.mdbFilePath.Location = new System.Drawing.Point(127, 267);
            this.mdbFilePath.Name = "mdbFilePath";
            this.mdbFilePath.Size = new System.Drawing.Size(157, 20);
            this.mdbFilePath.TabIndex = 16;
            // 
            // findMdbFile
            // 
            this.findMdbFile.Location = new System.Drawing.Point(298, 262);
            this.findMdbFile.Name = "findMdbFile";
            this.findMdbFile.Size = new System.Drawing.Size(82, 29);
            this.findMdbFile.TabIndex = 17;
            this.findMdbFile.Text = "Browse";
            this.findMdbFile.UseVisualStyleBackColor = true;
            this.findMdbFile.Click += new System.EventHandler(this.findMdbFile_Click);
            // 
            // sqlDbName
            // 
            this.sqlDbName.Enabled = false;
            this.sqlDbName.Location = new System.Drawing.Point(127, 349);
            this.sqlDbName.Name = "sqlDbName";
            this.sqlDbName.Size = new System.Drawing.Size(161, 20);
            this.sqlDbName.TabIndex = 19;
            this.sqlDbName.Text = "SHIMS2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 352);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "SQL Database";
            // 
            // migrateButton
            // 
            this.migrateButton.Location = new System.Drawing.Point(102, 425);
            this.migrateButton.Name = "migrateButton";
            this.migrateButton.Size = new System.Drawing.Size(82, 29);
            this.migrateButton.TabIndex = 20;
            this.migrateButton.Text = "Migrate Data";
            this.migrateButton.UseVisualStyleBackColor = true;
            this.migrateButton.Click += new System.EventHandler(this.migrateButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(202, 425);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(82, 29);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // migrationProgress
            // 
            this.migrationProgress.Location = new System.Drawing.Point(44, 386);
            this.migrationProgress.Name = "migrationProgress";
            this.migrationProgress.Size = new System.Drawing.Size(315, 23);
            this.migrationProgress.Step = 1;
            this.migrationProgress.TabIndex = 22;
            // 
            // findMsdsaulFile
            // 
            this.findMsdsaulFile.Location = new System.Drawing.Point(298, 298);
            this.findMsdsaulFile.Name = "findMsdsaulFile";
            this.findMsdsaulFile.Size = new System.Drawing.Size(82, 29);
            this.findMsdsaulFile.TabIndex = 25;
            this.findMsdsaulFile.Text = "Browse";
            this.findMsdsaulFile.UseVisualStyleBackColor = true;
            this.findMsdsaulFile.Click += new System.EventHandler(this.findMsdsaulFile_Click);
            // 
            // msdsAulBox
            // 
            this.msdsAulBox.Location = new System.Drawing.Point(127, 303);
            this.msdsAulBox.Name = "msdsAulBox";
            this.msdsAulBox.Size = new System.Drawing.Size(157, 20);
            this.msdsAulBox.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 306);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "MSDSAUL";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 510);
            this.Controls.Add(this.findMsdsaulFile);
            this.Controls.Add(this.msdsAulBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.migrationProgress);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.migrateButton);
            this.Controls.Add(this.sqlDbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.findMdbFile);
            this.Controls.Add(this.mdbFilePath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sqlServerInstanceCombo);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "SHIMS Data Migrator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox sqlServerInstanceCombo;
        private System.Windows.Forms.RadioButton winAuthRadio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label currUserLbl;
        private System.Windows.Forms.Label currAuthLbl;
        private System.Windows.Forms.Label currConLbl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl77;
        private System.Windows.Forms.Button authBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox passwordTxt;
        private System.Windows.Forms.TextBox userNameTxt;
        private System.Windows.Forms.RadioButton sqlServerAuthRadio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox mdbFilePath;
        private System.Windows.Forms.Button findMdbFile;
        private System.Windows.Forms.TextBox sqlDbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button migrateButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ProgressBar migrationProgress;
        private System.Windows.Forms.Button findMsdsaulFile;
        private System.Windows.Forms.TextBox msdsAulBox;
        private System.Windows.Forms.Label label7;

    }
}

