﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHIMS_Data_Migrator
{
    public struct SQLServerInstance {
        public string dataSource; 
        public string userId;
        public string password;
        public bool isSQLServerAuthenticated;
        public string targetDB;
    };

    class DBConn
    {
        SqlConnection targetConn;
        SqlConnection defaultConn;
        SQLServerInstance instance;

        public SQLServerInstance Instance
        {

            get { return instance; }
        }

        public DBConn()
        {
        }


        public DBConn(SQLServerInstance instance, bool checkSchema = false)
        {
            this.instance = instance;
            defaultConn = CreateConnection("");
            CreateTargetDatabase(instance.targetDB);
        }

        public static SqlConnection GetServerConnection(SQLServerInstance instance)
        {
            //Create Connection
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder["Data Source"] = instance.dataSource;

            if (instance.isSQLServerAuthenticated)
            {
                builder["User ID"] = instance.userId;
                builder["Password"] = instance.password;
            }
            else builder.IntegratedSecurity = true;

            builder["Persist Security Info"] = true;
            builder["Connect Timeout"] = 15;
            try
            {
                SqlConnection sqlConn = new SqlConnection(builder.ConnectionString);
                sqlConn.Open();
                return sqlConn;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.StackTrace);
                return null;
            }
        }

        private SqlConnection CreateConnection(string catalog)
        {

            //Create Connection
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder["Data Source"] = instance.dataSource;

            if (instance.isSQLServerAuthenticated)
            {
                builder["User ID"] = instance.userId;
                builder["Password"] = instance.password;
            }
            if (catalog != "")
            {
                builder["Initial Catalog"] = catalog;
            }
            else builder.IntegratedSecurity = true;

            builder["Persist Security Info"] = true;
            builder["Connect Timeout"] = 0;
            try
            {
                SqlConnection sqlConn = new SqlConnection(builder.ConnectionString);
                sqlConn.Open();
                return sqlConn;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.StackTrace);
                return null;
            }
        }

        public SqlConnection GetTargetConnection()
        {
            if (targetConn != null)
            {
                if (targetConn.State == System.Data.ConnectionState.Closed)
                {
                    targetConn.Open();
                }
                
                return targetConn;
            }
            else
            {
                return null;
            }
        }

        public void CreateTargetDatabase(string targetDB)
        {
            try
            {
                Console.Out.WriteLine("Building target database...");

                using (SqlCommand insertCmd = new SqlCommand(Scripts.CreateTargetDatabaseSql(targetDB), defaultConn))
                 {
                     insertCmd.ExecuteNonQuery();
                 }

                using (SqlCommand insertCmd = new SqlCommand(Scripts.CreateDefaultUserSql(targetDB), defaultConn))
                {
                    insertCmd.ExecuteNonQuery();
                }

                defaultConn.Close();

                System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                builder["Data Source"] = instance.dataSource;

                builder["User ID"] = "shims";
                builder["Password"] = "shims";
                
                if (targetDB != "")
                {
                    builder["Initial Catalog"] = targetDB;
                }
                else builder.IntegratedSecurity = true;

                builder["Persist Security Info"] = true;
                builder["Connect Timeout"] = 0;

                defaultConn = new SqlConnection(builder.ConnectionString);
                defaultConn.Open();
               

                //create and populate reference tables
                using (SqlCommand insertCmd = new SqlCommand("use [" + targetDB + "];" + SHIMS_Data_Migrator.Properties.Resources.ref_tables_sql, defaultConn))
                {
                    insertCmd.ExecuteNonQuery();
                }
                //create the rest of the schema
                using (SqlCommand insertCmd = new SqlCommand("use [" + targetDB + "];" + SHIMS_Data_Migrator.Properties.Resources.schema_sql, defaultConn))
                {
                    insertCmd.ExecuteNonQuery();
                }

                //create some triggers
                using (SqlCommand insertCmd = new SqlCommand("use [" + targetDB + "];" + Scripts.OffloadTriggersSql(), defaultConn))
                {
                    insertCmd.ExecuteNonQuery();
                }

                using (SqlCommand insertCmd = new SqlCommand("use [" + targetDB + "];" + Scripts.InsertOffloadTriggers2(), defaultConn))
                {
                    insertCmd.ExecuteNonQuery();
                }

                Console.Out.WriteLine("Completed building target database.");

            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.StackTrace);
            }
        }
    }
}
