﻿using System;
using System.Collections.Generic;
using System.Data.Sql;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHIMS_Data_Migrator
{
    class SQLServerUtil
    {
        public struct SQLServerInstance
        {
            public string ServerName { get; set; }
            public string InstanceName { get; set; }
            public string Version;
            public string FullName
            {
                get
                {
                    return ServerName + "\\" + InstanceName;
                }
            }
        };

        public SQLServerUtil()
        {
        }

        public static List<SQLServerInstance> ListSQLServerInstances()
        {
            // Retrieve the enumerator instance and then the data.
            SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;

            System.Data.DataTable table = instance.GetDataSources();

            List<SQLServerInstance> listOfInstances = new List<SQLServerInstance>();
            foreach (System.Data.DataRow row in table.Rows)
            {
                SQLServerInstance sqlServerInstance = new SQLServerInstance();

                sqlServerInstance.ServerName = row["ServerName"].ToString();
                sqlServerInstance.InstanceName = row["InstanceName"].ToString();
                sqlServerInstance.Version = row["Version"].ToString();

                listOfInstances.Add(sqlServerInstance);
            }

            return listOfInstances;
        }
    }
}
