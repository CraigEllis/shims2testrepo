﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SHIMS_Data_Migrator
{
    public partial class Form1 : Form
    {
        private SQLServerInstance currentInstance;
        private List<string> listOfDatabases;
        private BackgroundWorker bgWorker;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //set up background worker
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            bgWorker.ProgressChanged += bgWorker_ProgressChanged;

            // bind sql server instances to combo
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = SQLServerUtil.ListSQLServerInstances();

            sqlServerInstanceCombo.DataSource = bindingSource.DataSource;

            sqlServerInstanceCombo.DisplayMember = "FullName";
            sqlServerInstanceCombo.ValueMember = "FullName";

            winAuthRadio_CheckedChanged(winAuthRadio, null);
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            migrationProgress.Value = e.ProgressPercentage;
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            migrateButton.Enabled = true;
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DBConn dbConn = new DBConn((SQLServerInstance)e.Argument);
            string targetDb = dbConn.Instance.targetDB;
            Migrator migrator = new Migrator(currentInstance, targetDb, mdbFilePath.Text, msdsAulBox.Text, bgWorker);
            migrator.Migrate();
    }


        private void winAuthRadio_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton btn = sender as RadioButton;
            if (btn.Checked)
            {
                userNameTxt.Text = WindowsIdentity.GetCurrent().Name;
                passwordTxt.Clear();
                passwordTxt.Enabled = false;
                userNameTxt.Enabled = false;
            }
        }

        private void sqlServerAuthRadio_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton btn = sender as RadioButton;
            if (btn.Checked)
            {
                userNameTxt.Clear();
                passwordTxt.Clear();
                passwordTxt.Enabled = true;
                userNameTxt.Enabled = true;
            }

        }

        private void authBtn_Click(object sender, EventArgs e)
        {
            currentInstance.dataSource = sqlServerInstanceCombo.Text;
            currentInstance.userId = userNameTxt.Text;

            if (sqlServerAuthRadio.Checked)
            {
                currentInstance.password = passwordTxt.Text;
                currentInstance.isSQLServerAuthenticated = true;
            }
            else currentInstance.isSQLServerAuthenticated = false;

            SqlConnection currentConnection = DBConn.GetServerConnection(currentInstance);

            if (currentConnection != null)
            {
                // get databases
                if (currentConnection.State != ConnectionState.Open)
                {
                    currentConnection.Open();
                }

                DataTable tblDatabases = currentConnection.GetSchema("Databases");
                currentConnection.Close();

                listOfDatabases = new List<string>();
                foreach (DataRow row in tblDatabases.Rows)
                {
                    listOfDatabases.Add(row["database_name"].ToString());
                }

                currConLbl.Text = currentInstance.dataSource;
                currAuthLbl.Text = (currentInstance.isSQLServerAuthenticated) ? "SQL Server Auth" : "Windows Auth";
                currUserLbl.Text = currentInstance.userId;

                MessageBox.Show(this, "Successful connection.");
            }
            else
            {
                listOfDatabases = null;
                currConLbl.Text = "No Connection Established.";
                currAuthLbl.Text = "No Connection Established.";
                currUserLbl.Text = "No Connection Established.";

                MessageBox.Show(this, "Connection refused.");
            }
        }

        private void findMdbFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Access database (.mdb)|*.mdb";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                mdbFilePath.Text = dlg.FileName;
            }

        }

        private void findMsdsaulFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Access database (.mdb)|*.mdb";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                msdsAulBox.Text = dlg.FileName;
            }
        }

        private void migrateButton_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure you want to migrate data? Doing this will delete and replace the current SHIMS2 database. All data previously entered into SHIMS2 will be lost.",
                "Confirm", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                SQLServerInstance sqlServer = new SQLServerInstance()
                {
                    dataSource = sqlServerInstanceCombo.Text,
                    userId = userNameTxt.Text,
                    password = passwordTxt.Text,
                    isSQLServerAuthenticated = sqlServerAuthRadio.Checked,
                    targetDB = sqlDbName.Text
                };
                migrateButton.Enabled = false;
                bgWorker.RunWorkerAsync(sqlServer);
            }
        }

    }
}
