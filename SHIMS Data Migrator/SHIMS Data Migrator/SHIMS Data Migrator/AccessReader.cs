﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHIMS_Data_Migrator
{
    class AccessReader
    {
        private readonly string _connectionString = "";

        public AccessReader(string table = "")
        {
            _connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + table;
        }

        public DataTable GetTableFromQuery(string query, Dictionary<string, object> parameters, CommandType commandType)
        {
            var dataTable = new DataTable();
            using (var conn = GetConnection())
            {
                using (var cmd = new OleDbCommand(query, conn))
                {
                    cmd.CommandType = commandType;
                    if (parameters != null)
                    {
                        foreach (var parameter in parameters)
                        {
                            cmd.Parameters.AddWithValue(parameter.Key, parameter.Value);
                        }
                    }
                    using (var adapter = new OleDbDataAdapter(cmd))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }
            return dataTable;
        }

        private OleDbConnection GetConnection()
        {
            var connection = new OleDbConnection(_connectionString);
            return connection;
        }

    }
}
