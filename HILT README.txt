

HILT README - Version 1.21.0

*******************************************
I. SETUP THE DATABASE AND THE "HILT" USER *
*******************************************

	1. Have Windows SQL Server 2008 installed

	2. Take the backup file "haz_782_SQL2k_02_02.bak" and restore it into a database of any name you choose.
	
	3. Make sure that the database is not set as the master or default database or else problems will occur.
	
	4. In SQL Server 2008, go to the Security tab, then right click the login tab. Select "New Login...".
	
	5. Use the following info to set up the new user:

		Login name: "hilt"

		Set SQL Server authentication

		Password: "hilt"

		Enforce password policy

		Enforce password expiration

		Default database: "Whatever you named the HILT Database"

	6. In the Server Roles tab of the Login Properties window, give 'hilt' all possible server roles.

	7. In the User Mapping tab of the Login Properites window, map 'hilt' to the HILT database. Close the login properties.

	8. Test the user by restarting the server. When the login screen appears, switch to SQL Server Authentication and login.

	9. If an error occurs, click the options button and change the database to connect to from <default> to master.

	10. Connect.

********************************************
II. SETUP THE ENVIRONMENT AND RUN THE CODE *
********************************************

	1. Pull the code from the repository and locate it on your computer.

	2. Make sure Visual Studio 2008 is installed

	3. Go to "hazmat_team/Hilt_Releases/Hazmat/releases/v1_0_21/Hazmat" and run "HAZMAT.sln"

	4. Once the project opens, traverse the solution explorer until you find "Web.config." Open it.

	5. Find the section called <connectionStrings> and alter the first connection string with the following data:

		DataSource = "Whatever is the server name on your computer"

		Initial Catalog ="Whatever is the name of your database"

	6. Build and run the project. It will say that there were build errors. Run anyway. 

	7. If the connection to the database was correct, Inventory.aspx should load. 

	8. Find the username at the top right of the page. It will be needed for section III.

************************************
III. SET YOUR COMPUTER AS AN ADMIN *
************************************

	1. Go back to SQL Server 2008 and open a new sql query.

	2. Run the following commands:

		SET IDENTITY_INSERT {DatabaseName}.dbo.user_roles ON

		INSERT INTO {DatabaseName}.dbo.user_roles (user_role_id,username,role) VALUES ('{Number}','{Username from Section II}', 'ADMIN')

		SET IDENTITY_INSERT {DatabaseName}.dbo.user_roles OFF


	3. Restart the "HAZMAT.sln" project, then build and run again.

	4. You should now have complete access to all functions of HILT
