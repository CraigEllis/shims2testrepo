﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CIC_Security_Processor;
using System.Data.SqlTypes;

namespace CIC_Security_Processor_Test {
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class DatabaseManagerTest {
        private const int USER_COLUMNS = 35;
        private static SecurityUser m_testUser = null;
        private static SecurityOrg m_testOrg = null;

        public DatabaseManagerTest() {
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        [ClassCleanup()]
        public static void Cleanup() {

            //  remove the test user and organization
            DatabaseManager mgr = new DatabaseManager();

            //if (m_testUser != null)
            //    mgr.deleteSecurityUserRecord(m_testUser[ColumnMapping.USER_BADGE_NUMBER]);
            //if (m_testOrg != null)
            //    mgr.deleteSecurityOrgRecord(m_testUser[ColumnMapping.ORG_CAGE]);
        }
        #endregion

        [TestMethod]
        public void processUserInsertTest() {
            // Create a new test user
            getTestUser();

            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            long userID = mgr.processUserRecord(m_testUser, "N", DateTime.Now);

            Assert.IsTrue(userID > 0);
        }

        [TestMethod]
        public void processUserUpdateTest() {
            // Get the test user
            getTestUser();

            // Change the action type
            m_testUser.Fax = "47";
            m_testUser.Location = "Disconnected";
            m_testUser.NATO = "Y";

            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            long userID = mgr.processUserRecord(m_testUser, "U",  DateTime.Now);

            Assert.IsTrue(userID > 0);
        }

        [TestMethod]
        public void processOrgInsertTest() {
            // Create a new test org
            getTestOrg();

            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            int userID = mgr.insertSecurityOrgRecord(m_testOrg, DateTime.Now);

            Assert.IsTrue(userID > 0);
        }

        [TestMethod]
        public void getSecurityUserRecordTest() {
            string badgeNumber = "1234ABCD";

            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            SecurityUser user = mgr.getSecurityUserRecord(badgeNumber);

            Assert.AreEqual(badgeNumber, user.Badge_Number);
        }

        [TestMethod]
        public void getUserCheckedOutItemsTest() {
            string firstName = "Paul";
            string lastName = "Masse";

            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            List<ClassifiedItem> items = mgr.getUserCheckedOutItems(firstName, lastName);

            Assert.IsTrue(items.Count > 0);
        }

        [TestMethod]
        public void getUserOwnedItemsTest() {
            string firstName = "George";
            string lastName = "Maris";

            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            List<ClassifiedItem> items = mgr.getUserOwnedItems(firstName, lastName);

            Assert.IsTrue(items.Count > 0);
        }

        //[TestMethod]
        //public void processOrgUpdateTest() {
        //    // Get the test Org
        //    getTestOrg();

        //    // Change some of the data
        //    m_testOrg[ColumnMapping.ORG_CITY] = "Seattle";

        //    // Create the database manager
        //    DatabaseManager mgr = new DatabaseManager();

        //    long userID = mgr.processSecurityOrgRecord(m_testOrg, DateTime.Now);

        //    Assert.IsTrue(userID > 0);
        //}

        [TestMethod]
        public void getSecurityOrgRecordTest() {
            // Get the test Org
            getTestOrg();

            string orgCage = m_testOrg.Org_CAGE;
            // Create the database manager
            DatabaseManager mgr = new DatabaseManager();

            SecurityOrg org = mgr.getSecurityOrgRecord(orgCage);

            Assert.AreEqual(orgCage, org.Org_CAGE);
        }

        private SecurityUser getTestUser() {
            if (m_testUser == null) {
                m_testUser = new SecurityUser();

                // Populate the user with test data 
                m_testUser.First_Name = "Test";
                m_testUser.Last_Name = "CIC_User";
                m_testUser.Middle_Initial = "E";
                m_testUser.Person_Type = "Test User";
                m_testUser.Phone = "425-555-1212";
                m_testUser.Fax = null;
                m_testUser.Email = "CIC_User@google.com";
                m_testUser.Dept_Code = "1514";
                m_testUser.Badge_Number = "1234ABCD";
                m_testUser.Security_Level = "S";
                m_testUser.Supervisor_Last_Name = "Da";
                m_testUser.Supervisor_First_Name = "Boss";
                m_testUser.Middle_Initial = null;
                m_testUser.Supervisor_Phone = "425-555-0000";
                m_testUser.Supervisor_Email = "DaBoss@google.com";
                m_testUser.Location = "678";
                m_testUser.Org_Type = "N";
                m_testUser.Org_CAGE = null;
                m_testUser.Org_Name = null;
                m_testUser.Org_Address_1 = null;
                m_testUser.Org_Address_2 = null;
                m_testUser.Org_City = null;
                m_testUser.Org_State = null;
                m_testUser.Org_Zip = null;
                m_testUser.NATO = "1";
                m_testUser.CNWDI = "0";
                m_testUser.Courier_Card_Number = null;
                m_testUser.CAC_EID = "TEST CAC";
            }

            return m_testUser;
        }

        private SecurityOrg getTestOrg() {
            if (m_testOrg == null) {
                m_testOrg = new SecurityOrg();

                // Populate the user with test data 
                m_testOrg.Org_Type = "N";
                m_testOrg.Org_CAGE = "TEST_CAGE";
                m_testOrg.Org_Acronym = "TST";
                m_testOrg.Org_Name = "Test Org";
                m_testOrg.Org_Address_1 = "1234 1st ST";
                m_testOrg.Org_Address_2 = null;
                m_testOrg.Org_City = "Kirkland";
                m_testOrg.Org_State = "WA";
                m_testOrg.Org_Zip = "98052";
            }

            return m_testOrg;
        }
    }
}
