﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CIC_Security_Processor;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.IO;

namespace CIC_Security_Processor_Test {
    [TestClass]
    public class FileProcessorTest {
        string baseTesFilePath = "C:\\test\\CIC_Security\\test_data\\";

        /// <summary>
        /// Tests the check file type methods using a file with a bad header
        /// Test File: TestBadHeader.txt
        /// Expected Result: false
        /// </summary>
        [TestMethod]
        public void ImportFileBadTest() {
            FileProcessor proc = new FileProcessor();

            bool result = proc.processFile(baseTesFilePath + "TestBadHeader.txt");

            Assert.IsFalse(result);
        }

        /// <summary>
        /// Tests the processFile method using a file with 48 user records
        /// Test File: TestUserList_50.txt        /// 
        /// Expected Result: True - Database should show 50 records either new 'N' or updated 'U'
        /// </summary>
        [TestMethod]
        public void ImportFile50Test() {
            FileProcessor proc = new FileProcessor();

            bool result = proc.processFile(baseTesFilePath + "TestUserList_50.txt");

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Tests the processFile method and the email generation using a file with 46 user records
        /// Test File: TestUserList_46.txt        /// 
        /// Expected Result: True - Database should show 47 records either new 'N' or updated 'U'
        /// status and 4 record with disabled 'D' status. Emails should be generated for 4 users.
        /// </summary>
        [TestMethod]
        public void ImportFile46Test() {
            FileProcessor proc = new FileProcessor();

            bool result = proc.processFile(baseTesFilePath + "TestUserList_46.txt");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void archiveFileNameTest() {
            string fileName = 
                "C:\\AES_SpecialProjects\\CIC\\CIC_Security_Processor\\CIC_Security_Processor_Test\\bin\\Debug\\CICEmailTemplate.htm";

            string archiveFileName = Path.GetFileNameWithoutExtension(fileName)
                + String.Format("{0:_yyyyMMdd_HHmm}", DateTime.Now)
                + Path.GetExtension(fileName);

                // Archive the file to the processed folder
                archiveFileName = Path.GetFullPath(fileName) + "\\processed\\" + archiveFileName;

            System.Diagnostics.Debug.WriteLine(archiveFileName);
        }

//        [TestMethod]
//        public void EmailTest() {
//            MailMessage mail = new MailMessage();
//            SmtpClient server = new SmtpClient();

//            MailSettingsSectionGroup mailSettings =
//                Configuration.EmailSettings;

//            mail.From = new MailAddress(mailSettings.Smtp.From);
//            mail.To.Add(Configuration.EmailNotification);
//            mail.Subject = "Test Mail";
//            mail.Body = "This is for testing SMTP mail from GMAIL";
//            server.Host = mailSettings.Smtp.Network.Host;
//            server.Port = mailSettings.Smtp.Network.Port;
//            server.Credentials = new System.Net.NetworkCredential(
//                mailSettings.Smtp.Network.UserName,
//                mailSettings.Smtp.Network.Password);
////            SmtpServer.EnableSsl = true;

//            server.Send(mail);
//            Assert.IsTrue(true);
//        }
    }
}
