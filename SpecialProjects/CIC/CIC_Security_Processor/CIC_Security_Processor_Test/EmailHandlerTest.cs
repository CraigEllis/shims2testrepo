﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CIC_Security_Processor;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;

namespace CIC_Security_Processor_Test {
    [TestClass]
    public class EmailHandlerTest {
        SecurityUser m_User = null;
        List<ClassifiedItem> m_CheckedOutItems = new List<ClassifiedItem>();
        List<ClassifiedItem> m_OwnedItems = new List<ClassifiedItem>();

        MailMessage m_Msg = null;

        [TestMethod]
        public void generateUserCICEmaiNoItemsTest() {
            EmailHandler handlr = new EmailHandler();

            createTestUser();

            // First test - no items checked out or owned
            MailMessage msg = handlr.generateUserCICEmail(m_User, m_CheckedOutItems,
                m_OwnedItems);

            System.Diagnostics.Debug.WriteLine("**********\n" + msg.Body
                + "\n**********");
            System.Diagnostics.Debug.WriteLine(msg.To.ToString()
                + "\n**********");

            Assert.IsTrue(msg.Body.Length > 0);

            m_Msg = msg;
        }

        [TestMethod]
        public void generateUserCICEmaiWithItemsTest() {
            EmailHandler handlr = new EmailHandler();

            createTestUser();

            createCheckedOutItems(4);
            createOwnedItems(2);

            // First test - no items checked out or owned
            MailMessage msg = handlr.generateUserCICEmail(m_User, m_CheckedOutItems,
                m_OwnedItems);

            System.Diagnostics.Debug.WriteLine("**********\n" + msg.Body
                + "\n**********");
            System.Diagnostics.Debug.WriteLine(msg.To.ToString()
                + "\n**********");

            Assert.IsTrue(msg.Body.Length > 0);
        }


        [TestMethod]
        public void sendEmailMessageTest() {
            EmailHandler handlr = new EmailHandler();

            if (m_Msg == null) {
                createTestUser();

                createCheckedOutItems(4);
                createOwnedItems(2);

                // First test - no items checked out or owned
                m_Msg = handlr.generateUserCICEmail(m_User, m_CheckedOutItems,
                    m_OwnedItems);

                System.Diagnostics.Debug.WriteLine("**********\n" + m_Msg.Body
                    + "\n**********");
                System.Diagnostics.Debug.WriteLine(m_Msg.To.ToString()
                    + "\n**********");
            }

            bool sent = handlr.sendEmailMessage(m_Msg);

            Assert.IsTrue(sent);
        }

        [TestMethod]
        public void generateErrorFileNotificationEmailTest() {
            EmailHandler handlr = new EmailHandler();

            List<string> details = new List<string>();
            details.Add("Test Error Message");

            MailMessage msg = handlr.generateFileNotificationEmail("Test File Name", false,
                "Archived Test File Name", details);

            bool sent = handlr.sendEmailMessage(msg);

            Assert.IsTrue(sent);
        }

        [TestMethod]
        public void generateSuccessFileNotificationEmailTest() {
            EmailHandler handlr = new EmailHandler();

            List<string> details = new List<string>();
            details.Add("100");
            details.Add("72");
            details.Add("21");
            details.Add("7");

            MailMessage msg = handlr.generateFileNotificationEmail("Test File Name", true,
                "Archived Test File Name", details);

            bool sent = handlr.sendEmailMessage(msg);

            Assert.IsTrue(sent);
        }

        [TestMethod]
        public void EmailConfigurationTest() {
            MailSettingsSectionGroup mailSettings =
                CIC_Security_Processor.CIC_Configuration.EmailSettings;

            System.Diagnostics.Debug.WriteLine(mailSettings.Smtp.From);
        }

        [TestMethod]
        public void EmailTest() {
            MailSettingsSectionGroup mailSettings =
                CIC_Configuration.EmailSettings;

            MailMessage mail = new MailMessage(
                mailSettings.Smtp.From, CIC_Configuration.EmailNotification);
            SmtpClient server = new SmtpClient();

            mail.Subject = "Test Mail";
            mail.Body = "This is for testing SMTP mail from GMAIL";
            server.Host = mailSettings.Smtp.Network.Host;
            server.Port = mailSettings.Smtp.Network.Port;
            server.Credentials = new System.Net.NetworkCredential(
                mailSettings.Smtp.Network.UserName,
                mailSettings.Smtp.Network.Password);
            server.EnableSsl = true;
            server.DeliveryMethod = SmtpDeliveryMethod.Network;
            server.UseDefaultCredentials = false;
            server.Credentials = new System.Net.NetworkCredential(
                mailSettings.Smtp.Network.UserName,
                mailSettings.Smtp.Network.Password);

            server.Send(mail);
            Assert.IsTrue(true);
        }

        private void createTestUser() {
            if (m_User == null) {
                m_User = new SecurityUser();

                m_User.Badge_Number = "1234ABCD";
                m_User.Dept_Code = "1101";
                m_User.Email = "testuser@testing.com";
                m_User.First_Name = "Tommy";
                m_User.Last_Name = "Tester";
                m_User.Location = "678/2/101";
                m_User.Middle_Initial = "T";
                m_User.Phone = "410-555-1111";
                m_User.Supervisor_Email = "testsupv@testing.com";
                m_User.Supervisor_First_Name = "Da";
                m_User.Supervisor_Last_Name = "Boss";
                m_User.Supervisor_Phone = "410-555-2222";
            }
        }

        private void createCheckedOutItems(int num) {
            m_CheckedOutItems.Clear();

            for (int i = 1; i <= num; i++) {
                ClassifiedItem item = new ClassifiedItem();

                item.Barcode = "ID_" + string.Format("{0:000}", i);
                item.CheckoutDate = new DateTime(2012, 5, i);
                item.Classification = "C";
                item.INUM = int.Parse(string.Format("{0:00000}", i));
                item.ItemOwner = "Bob_Tester";
                item.Location = "678/2/101";
                item.MediaType = "Compact Disc";
                item.Title = "Test Item_" + i.ToString() + "____________________Test Only";

                m_CheckedOutItems.Add(item);
            }
        }

        private void createOwnedItems(int num) {
            m_OwnedItems.Clear();

            for (int i = 1; i <= num; i++) {
                ClassifiedItem item = new ClassifiedItem();

                item.Barcode = "ID_" + string.Format("{0:000}", i * 10);
                item.CheckoutDate = new DateTime(2012, 6, i);
                item.Classification = "C";
                item.INUM = int.Parse(string.Format("{0:00000}", i * 10));
                item.ItemOwner = "Tester, Tommy";
                item.Location = "678/1/11";
                item.MediaType = "Manuscript";
                item.Title = "Owned Item_" + i.ToString() + "____________________Test Only";

                m_OwnedItems.Add(item);
            }
        }
    }
}
