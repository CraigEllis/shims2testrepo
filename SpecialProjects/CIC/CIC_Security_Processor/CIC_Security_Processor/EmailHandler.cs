﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.IO;

namespace CIC_Security_Processor {
    public class EmailHandler {
        public MailMessage generateUserCICEmail(SecurityUser user, 
                List<ClassifiedItem> checkedOutItems,
                List<ClassifiedItem> ownedItems) {
            string reportDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);
            string userName = buildUserName(user);

            string checkedOutList = buildCheckedOutList(checkedOutItems);
            string ownedList = buildOwnedList(ownedItems);

            string htmlBody = File.ReadAllText(
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly(​).Location)
                + "\\CICEmailTemplate.htm");

            // Do the replacements
            ListDictionary replace = new ListDictionary();
            htmlBody = htmlBody.Replace("||User_Name||", userName);
            htmlBody = htmlBody.Replace("||Badge_Number||", user.Badge_Number);
            htmlBody = htmlBody.Replace("||Report_Date||", reportDate);
            htmlBody = htmlBody.Replace("||Dept_Code||", user.Dept_Code);
            htmlBody = htmlBody.Replace("||Location||", user.Location);
            htmlBody = htmlBody.Replace("||Supv_Name||", buildSupervisorName(user));
            htmlBody = htmlBody.Replace("||User_Email||", user.Email);
            htmlBody = htmlBody.Replace("||Supv_Email||", user.Supervisor_Email);
            htmlBody = htmlBody.Replace("||User_Phone||", user.Phone);
            htmlBody = htmlBody.Replace("||Supv_Phone||", user.Supervisor_Phone);
            htmlBody = htmlBody.Replace("||Checked_Out_List||", checkedOutList);
            htmlBody = htmlBody.Replace("||Owned_List||", ownedList);

            AlternateView view = AlternateView.CreateAlternateViewFromString(htmlBody,
                new System.Net.Mime.ContentType("text/html"));

            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = true;
            msg.Subject = "CIC Inventory Report for: " + userName + " - " + reportDate;
            // Add the addressees
            string[] addressees = CIC_Configuration.EmailNotification.Split(';');
            foreach (string addr in addressees) {
                msg.To.Add(new MailAddress(addr));
            }
            msg.AlternateViews.Add(view);
            msg.Body = htmlBody;


            return msg;
        }

        public MailMessage generateFileNotificationEmail(string fileName, bool processed, 
                string archiveFileName, List<string> details) {
            string reportDate = String.Format("{0:yyyy-MM-dd HHmm}", DateTime.Now);

            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = true;
            string htmlBody = null;

            string execPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (processed) {
                htmlBody = File.ReadAllText(execPath + "\\FileEmailTemplateSuccess.htm");

                // Do the replacements
                htmlBody = htmlBody.Replace("||Total_Records||", details[0]);
                htmlBody = htmlBody.Replace("||Updated_Users||", details[1]);
                htmlBody = htmlBody.Replace("||New Users||", details[2]);
                htmlBody = htmlBody.Replace("||Disabled_Users||", details[3]);

                msg.Subject = "CIC Security Processor - Upload File Processed - " + reportDate;
            }
            else {
                htmlBody = File.ReadAllText(execPath + "\\FileEmailTemplateError.htm");

                // Do the replacements
                htmlBody = htmlBody.Replace("||Error_Msg||", details[0]);

                msg.Subject = "CIC Security Processor - Errors Processing Upload File - " + reportDate;
            }

            // Replace the common fields
            htmlBody = htmlBody.Replace("||File_Name||", fileName);
            htmlBody = htmlBody.Replace("||Processed_Date||", reportDate);
            htmlBody = htmlBody.Replace("||Archive_FileName||", archiveFileName);

            AlternateView view = AlternateView.CreateAlternateViewFromString(htmlBody,
                new System.Net.Mime.ContentType("text/html"));

            // Add the addressees
            string[] addressees = CIC_Configuration.SystemNotification.Split(';');
            foreach (string addr in addressees) {
                msg.To.Add(new MailAddress(addr));
            }
            msg.AlternateViews.Add(view);
            msg.Body = htmlBody;

            return msg;
        }

        public bool sendEmailMessage(MailMessage message) {
            bool result = true;

            MailSettingsSectionGroup mailSettings =
                CIC_Configuration.EmailSettings;

            SmtpClient server = new SmtpClient();

            server.Host = mailSettings.Smtp.Network.Host;
            server.Port = mailSettings.Smtp.Network.Port;
            server.Credentials = new System.Net.NetworkCredential(
                mailSettings.Smtp.Network.UserName,
                mailSettings.Smtp.Network.Password);
            server.EnableSsl = true;
            server.DeliveryMethod = SmtpDeliveryMethod.Network;
            server.UseDefaultCredentials = false;
            server.Credentials = new System.Net.NetworkCredential(
                mailSettings.Smtp.Network.UserName,
                mailSettings.Smtp.Network.Password);

            message.From = new MailAddress(mailSettings.Smtp.From);

            server.Send(message);

            // Write the email subject to the console
            Console.WriteLine(message.Subject);

            return result;
        }

        private string buildUserName(SecurityUser user) {
            StringBuilder sb = new StringBuilder();
            sb.Append(user.Last_Name);
            sb.Append(", ");
            sb.Append(user.First_Name);
            if (user.Middle_Initial != null && user.Middle_Initial.Length > 0)
                sb.Append(" " + user.Middle_Initial);

            return sb.ToString();
        }

        private string buildSupervisorName(SecurityUser user) {
            StringBuilder sb = new StringBuilder();
            sb.Append(user.Supervisor_Last_Name);
            sb.Append(", ");
            sb.Append(user.Supervisor_First_Name);
            if (user.Supervisor_Middle_Initial != null && user.Supervisor_Middle_Initial.Length > 0)
                sb.Append(" " + user.Supervisor_Middle_Initial);

            return sb.ToString();
        }

        private string buildCheckedOutList(List<ClassifiedItem> items) {
            StringBuilder sb = new StringBuilder();

            if (items.Count == 0)
                sb.Append("***** No Items checked out *****");
            else {
                // Table header
                sb.Append(buildCheckedOutTableHeader());

                // Checked out items
                int row = 1;
                foreach(ClassifiedItem item in items) {
                    sb.Append(buildCheckedOutItemRow(item, row));
                    row++;
                }

                // Close table
                sb.AppendLine("</table>");
            }

            return sb.ToString();
        }

        private string buildOwnedList(List<ClassifiedItem> items) {
            StringBuilder sb = new StringBuilder();

            if (items.Count == 0)
                sb.Append("***** No Owned Items *****");
            else {
                // Table header
                sb.Append(buildOwnedTableHeader());

                // Checked out items
                int row = 1;
                foreach (ClassifiedItem item in items) {
                    sb.Append(buildOwnedItemRow(item, row));
                    row++;
                }

                // Close table
                sb.AppendLine("</table>");
            }

            return sb.ToString();
        }

        private string buildCheckedOutTableHeader() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<table width=800px, border='1'>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<th width=50px align=right, class='style1'></th>");
            sb.AppendLine("<th width = 100px class='style1'>Barcode</th>");
            sb.AppendLine("<th width = 470pxclass='style1'>Item/Title</th>");
            sb.AppendLine("<th width = 100px class='style1'>Type</th>");
            sb.AppendLine("<th width = 80px class='style1'>Checked Out</th>");
            sb.AppendLine("</tr>");

            return sb.ToString();
        }

        private string buildCheckedOutItemRow(ClassifiedItem item, int row) {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<tr>");
            sb.AppendLine("<td width=50px align=right, class='style1'>" + row.ToString() + "</td>");
            sb.AppendLine("<td width = 100px class='style1'>" + item.INUM + "</td>");
            sb.AppendLine("<td width = 470pxclass='style1'>" + item.Title + "</td>");
            sb.AppendLine("<td width = 100px class='style1'>" + item.MediaType + "</td>");
            sb.AppendLine("<td width = 80px class='style1'>"
                + String.Format("{0:MM/dd/yyyy}", item.CheckoutDate) + "</td>");
            sb.AppendLine("</tr>");

            return sb.ToString();
        }

        private string buildOwnedTableHeader() {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<table width=720px, border='1'>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<th width=50px align='right', class='style1'></th>");
            sb.AppendLine("<th width=100px class='style1'>Barcode</th>");
            sb.AppendLine("<th width=470pxclass='style1'>Item/Title</th>");
            sb.AppendLine("<th width=100px class='style1'>Type</th>");
            sb.AppendLine("</tr>");

            return sb.ToString();
        }

        private string buildOwnedItemRow(ClassifiedItem item, int row) {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<tr>");
            sb.AppendLine("<td width=50px align='right', class='style1'>" + row.ToString() + "</td>");
            sb.AppendLine("<td width=100px class='style1'>" + item.INUM + "</td>");
            sb.AppendLine("<td width=470pxclass='style1'>" + item.Title + "</td>");
            sb.AppendLine("<td width = 100px class='style1'>" + item.MediaType + "</td>");
            sb.AppendLine("</tr>");

            return sb.ToString();
        }
    }
}
