﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CIC_Security_Processor {
    public class SecurityUser {
        #region Constructors
        public SecurityUser() {
        }

        public SecurityUser(string[] data) {
            populateUserFields(data);
        }
        #endregion

        private string m_First_Name = null;
        public string First_Name {
            get { return m_First_Name; }
            set { m_First_Name = value; }
        }

        private string m_Last_Name = null;
        public string Last_Name {
            get { return m_Last_Name; }
            set { m_Last_Name = value; }
        }

        private string m_Middle_Initial = null;
        public string Middle_Initial {
            get { return m_Middle_Initial; }
            set { m_Middle_Initial = value; }
        }

        private string m_Person_Type = null;
        public string Person_Type {
            get { return m_Person_Type; }
            set { m_Person_Type = value; }
        }

        private string m_Phone = null;
        public string Phone {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        private string m_Fax = null;
        public string Fax {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        private string m_Email = null;
        public string Email {
            get { return m_Email; }
            set { m_Email = value; }
        }

        private string m_Dept_Code = null;
        public string Dept_Code {
            get { return m_Dept_Code; }
            set { m_Dept_Code = value; }
        }

        private string m_Badge_Number = null;
        public string Badge_Number {
            get { return m_Badge_Number; }
            set { m_Badge_Number = value; }
        }

        private string m_Security_Level = null;
        public string Security_Level {
            get { return m_Security_Level; }
            set { m_Security_Level = value; }
        }

        private string m_Supervisor_Last_Name = null;
        public string Supervisor_Last_Name {
            get { return m_Supervisor_Last_Name; }
            set { m_Supervisor_Last_Name = value; }
        }

        private string m_Supervisor_First_Name = null;
        public string Supervisor_First_Name {
            get { return m_Supervisor_First_Name; }
            set { m_Supervisor_First_Name = value; }
        }

        private string m_Supervisor_Middle_Initial = null;
        public string Supervisor_Middle_Initial {
            get { return m_Supervisor_Middle_Initial; }
            set { m_Supervisor_Middle_Initial = value; }
        }

        private string m_Supervisor_Phone = null;
        public string Supervisor_Phone {
            get { return m_Supervisor_Phone; }
            set { m_Supervisor_Phone = value; }
        }

        private string m_Supervisor_Email = null;
        public string Supervisor_Email {
            get { return m_Supervisor_Email; }
            set { m_Supervisor_Email = value; }
        }

        private string m_Location = null;
        public string Location {
            get { return m_Location; }
            set { m_Location = value; }
        }

        private string m_Org_Type = null;

        public string Org_Type {
            get { return m_Org_Type; }
            set { m_Org_Type = value; }
        }

        private string m_Org_CAGE = null;
        public string Org_CAGE {
            get { return m_Org_CAGE; }
            set { m_Org_CAGE = value; }
        }

        private string m_Org_Acronym = null;
        public string Org_Acronym {
            get { return m_Org_Acronym; }
            set { m_Org_Acronym = value; }
        }

        private string m_Org_Name = null;
        public string Org_Name {
            get { return m_Org_Name; }
            set { m_Org_Name = value; }
        }

        private string m_Org_Address_1 = null;
        public string Org_Address_1 {
            get { return m_Org_Address_1; }
            set { m_Org_Address_1 = value; }
        }

        private string m_Org_Address_2 = null;
        public string Org_Address_2 {
            get { return m_Org_Address_2; }
            set { m_Org_Address_2 = value; }
        }

        private string m_Org_City = null;
        public string Org_City {
            get { return m_Org_City; }
            set { m_Org_City = value; }
        }

        private string m_Org_State = null;
        public string Org_State {
            get { return m_Org_State; }
            set { m_Org_State = value; }
        }

        private string m_Org_Zip = null;
        public string Org_Zip {
            get { return m_Org_Zip; }
            set { m_Org_Zip = value; }
        }

        private string m_NATO = null;
        public string NATO {
            get { return m_NATO; }
            set { m_NATO = value; }
        }

        private string m_CNWDI = null;
        public string CNWDI {
            get { return m_CNWDI; }
            set { m_CNWDI = value; }
        }

        private string m_Courier_Card_Number = null;
        public string Courier_Card_Number {
            get { return m_Courier_Card_Number; }
            set { m_Courier_Card_Number = value; }
        }

        private DateTime m_Courier_Card_Expiration;
        public DateTime Courier_Card_Expiration {
            get { return m_Courier_Card_Expiration; }
            set { m_Courier_Card_Expiration = value; }
        }

        private string m_CAC_EID = null;
        public string CAC_EID {
            get { return m_CAC_EID; }
            set { m_CAC_EID = value; }
        }

        private string m_Status_Code = null;
        public string Status_Code {
            get { return m_Status_Code; }
            set { m_Status_Code = value; }
        }

        private DateTime m_Changed_Date;
        public DateTime Changed_Date {
            get { return m_Changed_Date; }
            set { m_Changed_Date = value; }
        }

        public string[] ToArray() {
            string[] user = new string[ColumnMapping.USER_COLUMNS];

            // Populate the user with test data 
            user[ColumnMapping.USER_FIRST_NAME] = m_First_Name;
            user[ColumnMapping.USER_LAST_NAME] = m_Last_Name;
            user[ColumnMapping.USER_MIDDLE_INITIAL] = m_Middle_Initial;
            user[ColumnMapping.USER_PERSON_TYPE] = m_Person_Type;
            user[ColumnMapping.USER_PHONE] = m_Phone;
            user[ColumnMapping.USER_FAX] = m_Fax;
            user[ColumnMapping.USER_EMAIL] = m_Email;
            user[ColumnMapping.USER_DEPT_CODE] = m_Dept_Code;
            user[ColumnMapping.USER_BADGE_NUMBER] = m_Badge_Number;
            user[ColumnMapping.USER_SECURITY_LEVEL] = m_Security_Level;
            user[ColumnMapping.USER_SUPERVISOR_LAST_NAME] = m_Supervisor_Last_Name;
            user[ColumnMapping.USER_SUPERVISOR_FIRST_NAME] = m_Supervisor_First_Name;
            user[ColumnMapping.USER_SUPERVISOR_MIDDLE_INITIAL] = m_Supervisor_Middle_Initial;
            user[ColumnMapping.USER_SUPERVISOR_PHONE] = m_Supervisor_Phone;
            user[ColumnMapping.USER_SUPERVISOR_EMAIL] = m_Supervisor_Email;
            user[ColumnMapping.USER_LOCATION] = m_Location;
            user[ColumnMapping.USER_ORG_TYPE] = m_Org_Type;
            user[ColumnMapping.USER_ORG_CAGE] = m_Org_CAGE;
//            user[ColumnMapping.USER_ORG_ACRONYM] = m_Org_Acronym;
            user[ColumnMapping.USER_ORG_NAME] = m_Org_Name;
            user[ColumnMapping.USER_ORG_ADDRESS_1] = m_Org_Address_1;
            user[ColumnMapping.USER_ORG_ADDRESS_2] = m_Org_Address_2;
            user[ColumnMapping.USER_ORG_CITY] = m_Org_City;
            user[ColumnMapping.USER_ORG_STATE] = m_Org_State;
            user[ColumnMapping.USER_ORG_ZIP] = m_Org_Zip;
            user[ColumnMapping.USER_NATO] = m_NATO;
            user[ColumnMapping.USER_CNWDI] = m_CNWDI;
            user[ColumnMapping.USER_COURIER_CARD_NUMBER] = m_Courier_Card_Number;
            user[ColumnMapping.USER_COURIER_CARD_EXPIRATION] = m_Courier_Card_Expiration.ToShortDateString();
            user[ColumnMapping.USER_CAC_EID] = m_CAC_EID;

            return user;
        }

        private void populateUserFields(string[] data) {
            // Populate the fields
            m_Last_Name = data[ColumnMapping.USER_LAST_NAME];
            m_First_Name = data[ColumnMapping.USER_FIRST_NAME];
            m_Middle_Initial = data[ColumnMapping.USER_MIDDLE_INITIAL];
            m_Person_Type = data[ColumnMapping.USER_PERSON_TYPE];
            m_Phone = data[ColumnMapping.USER_PHONE];
            m_Fax = data[ColumnMapping.USER_FAX];
            m_Email = data[ColumnMapping.USER_EMAIL];
            m_Dept_Code = data[ColumnMapping.USER_DEPT_CODE];
            m_Badge_Number = data[ColumnMapping.USER_BADGE_NUMBER];
            m_Security_Level = data[ColumnMapping.USER_SECURITY_LEVEL];
            m_Supervisor_Last_Name = data[ColumnMapping.USER_SUPERVISOR_LAST_NAME];
            m_Supervisor_First_Name = data[ColumnMapping.USER_SUPERVISOR_FIRST_NAME];
            m_Supervisor_Middle_Initial = data[ColumnMapping.USER_SUPERVISOR_MIDDLE_INITIAL];
            m_Supervisor_Phone = data[ColumnMapping.USER_SUPERVISOR_PHONE];
            m_Supervisor_Email = data[ColumnMapping.USER_SUPERVISOR_EMAIL];
            m_Location = data[ColumnMapping.USER_LOCATION];
            m_Org_Type = data[ColumnMapping.USER_ORG_TYPE];
            m_Org_CAGE = data[ColumnMapping.USER_ORG_CAGE];
            m_Org_Name = data[ColumnMapping.USER_ORG_NAME];
            m_Org_Address_1 = data[ColumnMapping.USER_ORG_ADDRESS_1];
            m_Org_Address_2 = data[ColumnMapping.USER_ORG_ADDRESS_2];
            m_Org_City = data[ColumnMapping.USER_ORG_CITY];
            m_Org_State = data[ColumnMapping.USER_ORG_STATE];
            m_Org_Zip = data[ColumnMapping.USER_ORG_ZIP];
            m_NATO = data[ColumnMapping.USER_NATO];
            m_CNWDI = data[ColumnMapping.USER_CNWDI];
            m_Courier_Card_Number = data[ColumnMapping.USER_COURIER_CARD_NUMBER];
            DateTime tempDate;
            DateTime.TryParse(data[ColumnMapping.USER_COURIER_CARD_EXPIRATION], out tempDate);
            m_Courier_Card_Expiration = tempDate;
            m_CAC_EID = data[ColumnMapping.USER_CAC_EID];

            // Note - the generated fields (org_arconym, statusCode Changed Date are left blank 
        }
    }
}
