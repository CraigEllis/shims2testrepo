﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace CIC_Security_Processor {
    public class DatabaseManager {
        private static string m_LBITSconnString = "";
        private static SqlConnection m_LBITSconn = null;
        private static string m_LSRconnString = "";
        private static SqlConnection m_LSRconn = null;

        #region Constructors
        public DatabaseManager() {
            m_LBITSconnString = CIC_Configuration.LBITSConnectionString;
            getLBITSConnection();
            m_LSRconnString = CIC_Configuration.LSRConnectionString;
            getLSRConnection();
        }

        public DatabaseManager(string LBITSconnectionString, string LSRconnectionString) {
            m_LBITSconnString = LBITSconnectionString;
            getLBITSConnection();
            m_LSRconnString = LSRconnectionString;
            getLSRConnection();
        }
        #endregion

        protected internal SqlConnection getLBITSConnection() {
            if (m_LBITSconnString == null)
                m_LBITSconnString = CIC_Configuration.LBITSConnectionString;
            if (m_LBITSconn == null)
                m_LBITSconn = new SqlConnection(m_LBITSconnString);

            return m_LBITSconn;
        }

        protected internal SqlConnection getLSRConnection() {
            if (m_LSRconnString == null)
                m_LSRconnString = CIC_Configuration.LSRConnectionString;
            if (m_LSRconn == null)
                m_LSRconn = new SqlConnection(m_LSRconnString);

            return m_LSRconn;
        }

        #region Users
        public long processUserRecord(SecurityUser user, string statusCode, DateTime processDate) {
            long userID = 0;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                using (SqlCommand cmd = new SqlCommand("", m_LBITSconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    // Check the user status - for now rely on the status in the feed being correct
                    if (statusCode.ToUpper().Equals(CIC_Configuration.ACTION_NEW)) {
                        // New user - insert into table
                        cmd.CommandText = "procAddSecurityUser";
                    }
                    else {
                        // Update
                        cmd.CommandText = "procUpdateSecurityUser";
                    }

                    // Populate the parameters
                    cmd.Parameters.Add(new SqlParameter("@Last_Name", user.Last_Name));
                    cmd.Parameters.Add(new SqlParameter("@First_Name", user.First_Name));
                    cmd.Parameters.Add(new SqlParameter("@Middle_Initial", dbNull(user.Middle_Initial)));
                    cmd.Parameters.Add(new SqlParameter("@Person_Type", dbNull(user.Person_Type)));
                    cmd.Parameters.Add(new SqlParameter("@Phone", dbNull(user.Phone)));
                    cmd.Parameters.Add(new SqlParameter("@Fax", dbNull(user.Fax)));
                    cmd.Parameters.Add(new SqlParameter("@Email", dbNull(user.Email)));
                    cmd.Parameters.Add(new SqlParameter("@Dept_Code", dbNull(user.Dept_Code)));
                    cmd.Parameters.Add(new SqlParameter("@Badge_Number", dbNull(user.Badge_Number)));
                    cmd.Parameters.Add(new SqlParameter("@Security_Level", dbNull(user.Security_Level)));
                    cmd.Parameters.Add(new SqlParameter("@Supervisor_Last_Name", dbNull(user.Supervisor_Last_Name)));
                    cmd.Parameters.Add(new SqlParameter("@Supervisor_First_Name", dbNull(user.Supervisor_First_Name)));
                    cmd.Parameters.Add(new SqlParameter("@Supervisor_Middle_Initial", dbNull(user.Supervisor_Middle_Initial)));
                    cmd.Parameters.Add(new SqlParameter("@Supervisor_Phone", dbNull(user.Supervisor_Phone)));
                    cmd.Parameters.Add(new SqlParameter("@Supervisor_Email", dbNull(user.Supervisor_Email)));
                    cmd.Parameters.Add(new SqlParameter("@Location", dbNull(user.Location)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Type", dbNull(user.Org_Type)));
                    cmd.Parameters.Add(new SqlParameter("@Org_CAGE", dbNull(user.Org_CAGE)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Acronym", dbNull(user.Org_Acronym)));
                    cmd.Parameters.Add(new SqlParameter("@NATO", dbNull(user.NATO)));
                    cmd.Parameters.Add(new SqlParameter("@CNWDI", dbNull(user.CNWDI)));
                    cmd.Parameters.Add(new SqlParameter("@Courier_Card_Number", dbNull(user.Courier_Card_Number)));
                    if (user.Courier_Card_Expiration > DateTime.MinValue)
                        cmd.Parameters.Add(new SqlParameter("@Courier_Card_Expiration", user.Courier_Card_Expiration));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Courier_Card_Expiration", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@CAC_EID", dbNull(user.CAC_EID)));
                    cmd.Parameters.Add(new SqlParameter("@Status_Code", statusCode));
                    cmd.Parameters.Add(new SqlParameter("@Changed_Date", processDate));

                    SqlParameter retval = cmd.Parameters.Add("@User_Security_ID", SqlDbType.BigInt);
                    retval.Direction = ParameterDirection.Output;

                    // Execute the proc
                    cmd.ExecuteNonQuery();

                    if (cmd.Parameters["@User_Security_ID"].Value != null)
                        long.TryParse(cmd.Parameters["@User_Security_ID"].Value.ToString(), out userID);
                }
            }
            return userID;
        }

        public int disableUsersByChangedDate(DateTime lastProcessDate) {
            int result = 0;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                // Change the status code to D (disabled) for any active users who didn't bet processed.
                string stmt = "UPDATE User_Security SET Status_Code = 'D' "
                    + "WHERE Status_Code != 'D' AND Changed_Date < @lastProcessDate";

                using (SqlCommand cmd = new SqlCommand(stmt, m_LBITSconn)) {

                    // Populate the parameters
                    cmd.Parameters.Add(new SqlParameter("@lastProcessDate", lastProcessDate));
                    
                    // Execute the command and get the number of rows affected
                    int.TryParse(cmd.ExecuteNonQuery().ToString(), out result);
                }
            }

            return result;
        }

        public List<string> getNewDisabledUsers(DateTime lastProcessDate) {
            List<string> badges = new List<string>();

            // Get the badge numbers of the users that were disabled

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                // Change the status code to D (disabled) for any active users who didn't bet processed.
                string stmt = "SELECT Badge_Number FROM User_Security "
                    + "WHERE Status_Code != 'D' AND Changed_Date < @lastProcessDate";

                using (SqlCommand cmd = new SqlCommand(stmt, m_LBITSconn)) {

                    // Populate the parameters
                    cmd.Parameters.Add(new SqlParameter("@lastProcessDate", lastProcessDate));

                    // Execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            // Process the items
                            string badge = rdr["badge_number"].ToString();

                            badges.Add(badge);
                        }
                    }
                }
            }

            // Disable any users
            if (badges.Count > 0)
                disableUsersByChangedDate(lastProcessDate);

            return badges;
        }

        public bool deleteSecurityUserRecord(string badgeNumber) {
            bool result = false;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                string stmt = "DELETE FROM User_Security WHERE Badge_Number = @badgeNumber";

                using (SqlCommand cmd = new SqlCommand(stmt, m_LBITSconn)) {

                    cmd.Parameters.Add(new SqlParameter("@badgeNumber", badgeNumber));

                    // execute the command
                    int id = cmd.ExecuteNonQuery();

                    if (id > 0)
                        result = true;
                }
            }

            return result;
        }

        public SecurityUser getSecurityUserRecord(string badgeNumber) {
            SecurityUser user = null;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                using (SqlCommand cmd = new SqlCommand("procSecurityUserBadgeSel", m_LBITSconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Badge_Number", badgeNumber));

                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        if (rdr.Read()) {
                            //Process the items
                            user = new SecurityUser();

                            user.First_Name = rdr["First_Name"].ToString();
                            user.Last_Name = rdr["Last_Name"].ToString();
                            user.Middle_Initial = rdr["Middle_Initial"].ToString();
                            user.Person_Type = rdr["Person_Type"].ToString();
                            user.Phone = rdr["Phone"].ToString();
                            user.Fax = rdr["Fax"].ToString();
                            user.Email = rdr["Email"].ToString();
                            user.Dept_Code = rdr["Dept_Code"].ToString();
                            user.Badge_Number = rdr["Badge_Number"].ToString();
                            user.Security_Level = rdr["Security_Level"].ToString();
                            user.Supervisor_Last_Name = rdr["Supervisor_Last_Name"].ToString();
                            user.Supervisor_First_Name = rdr["Supervisor_First_Name"].ToString();
                            user.Supervisor_Middle_Initial = rdr["Supervisor_Middle_Initial"].ToString();
                            user.Supervisor_Phone = rdr["Supervisor_Phone"].ToString();
                            user.Supervisor_Email = rdr["Supervisor_Email"].ToString();
                            user.Location = rdr["Location"].ToString();
                            user.Org_Type = rdr["ORG_Type"].ToString();
                            user.Org_CAGE = rdr["ORG_CAGE"].ToString();
                            user.Org_Acronym = rdr["ORG_Acronym"].ToString();
                            user.NATO = rdr["NATO"].ToString();
                            user.CNWDI = rdr["CNWDI"].ToString();
                            user.Courier_Card_Number = rdr["Courier_Card_Number"].ToString();
                            DateTime tempDate;
                            DateTime.TryParse(rdr["Courier_Card_Expiration"].ToString(), out tempDate);
                            user.Courier_Card_Expiration = tempDate;
                            user.CAC_EID = rdr["CAC_EID"].ToString();
                            user.Status_Code = rdr["Status_Code"].ToString();
                            DateTime.TryParse(rdr["Changed_Date"].ToString(), out tempDate);
                            user.Changed_Date = tempDate;
                        }
                    }
                }
            }

            return user;
        }
        #endregion

        #region Orgs
        public int insertSecurityOrgRecord(SecurityOrg org, DateTime processDate) {
            int orgID = 0;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                using (SqlCommand cmd = new SqlCommand("procAddSecurityOrg", m_LBITSconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    // Populate the parameters
                    cmd.Parameters.Add(new SqlParameter("@Org_CAGE", org.Org_CAGE));
                    cmd.Parameters.Add(new SqlParameter("@Org_Type", dbNull(org.Org_Type)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Acronym", dbNull(org.Org_Acronym)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Name", dbNull(org.Org_Name)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Address_1", dbNull(org.Org_Address_1)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Address_2", dbNull(org.Org_Address_2)));
                    cmd.Parameters.Add(new SqlParameter("@Org_City", dbNull(org.Org_City)));
                    cmd.Parameters.Add(new SqlParameter("@Org_State", dbNull(org.Org_State)));
                    cmd.Parameters.Add(new SqlParameter("@Org_Zip", dbNull(org.Org_Zip)));
                    cmd.Parameters.Add(new SqlParameter("@Changed_Date", processDate));

                    SqlParameter retval = cmd.Parameters.Add("@Org_Security_ID", SqlDbType.BigInt);
                    retval.Direction = ParameterDirection.Output;

                    // Execute the proc
                    cmd.ExecuteNonQuery();

                    if (cmd.Parameters["@Org_Security_ID"].Value != null)
                        int.TryParse(cmd.Parameters["@Org_Security_ID"].Value.ToString(), out orgID);
                }
            }
            return orgID;
        }

        public bool deleteSecurityOrgRecord(string org_CAGE) {
            bool result = false;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                string stmt = "DELETE FROM Org_Security WHERE Org_CAGE = @org_CAGE";

                using (SqlCommand cmd = new SqlCommand(stmt, m_LBITSconn)) {

                    cmd.Parameters.Add(new SqlParameter("@org_CAGE", org_CAGE));

                    // execute the command
                    int id = cmd.ExecuteNonQuery();

                    if (id > 0)
                        result = true;
                }
            }

            return result;
        }

        public SecurityOrg getSecurityOrgRecord(string org_CAGE) {
            SecurityOrg org = null;

            // Connection
            using (m_LBITSconn) {
                if (m_LBITSconn.ConnectionString == null || m_LBITSconn.ConnectionString.Length == 0)
                    m_LBITSconn.ConnectionString = CIC_Configuration.LBITSConnectionString;
                if (m_LBITSconn.State != System.Data.ConnectionState.Open)
                    m_LBITSconn.Open();

                using (SqlCommand cmd = new SqlCommand("procSecurityOrgCAGESel", m_LBITSconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@org_CAGE", org_CAGE));

                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        if (rdr.Read()) {
                            //Process the items
                            org = new SecurityOrg();

                            org.Org_Type = rdr["Org_Type"].ToString();
                            org.Org_CAGE = rdr["Org_CAGE"].ToString();
                            org.Org_Acronym = rdr["Org_Acronym"].ToString();
                            org.Org_Name = rdr["Org_Name"].ToString();
                            org.Org_Address_1 = rdr["Org_Address_1"].ToString();
                            org.Org_Address_2 = rdr["Org_Address_2"].ToString();
                            org.Org_City = rdr["Org_City"].ToString();
                            org.Org_State = rdr["Org_State"].ToString();
                            org.Org_Zip = rdr["Org_Zip"].ToString();
                            org.Org_FSO_Name = rdr["Org_FSO_Name"].ToString();
                            org.Org_FSO_Phone = rdr["Org_FSO_Phone"].ToString();
                            org.Org_FSO_Email = rdr["Org_FSO_Email"].ToString();
                        }
                    }
                }
            }

            return org;
        }

        #endregion

        #region Items
        public List<ClassifiedItem> getUserCheckedOutItems(string badgeNumber) {
            List<ClassifiedItem> items = new List<ClassifiedItem>();

            // Connection
            using (m_LSRconn) {
                if (m_LSRconn.ConnectionString == null || m_LSRconn.ConnectionString.Length == 0)
                    m_LSRconn.ConnectionString = CIC_Configuration.LSRConnectionString;
                if (m_LSRconn.State != System.Data.ConnectionState.Open)
                    m_LSRconn.Open();

                using (SqlCommand cmd = new SqlCommand("procListCheckedOutByBadge", m_LSRconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@badgeNumber", badgeNumber));

                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            //Process the items
                            ClassifiedItem item = new ClassifiedItem();

                            long tempNum = 0;
                            long.TryParse(rdr["INUM"].ToString(), out tempNum);
                            item.INUM = tempNum;
                            item.Customer = rdr["Customer"].ToString();
                            item.Title = rdr["Title"].ToString();
                            item.Classification = rdr["Class"].ToString();
                            item.SerialNumber = rdr["serial_no"].ToString();
                            item.MediaType = rdr["MediaType"].ToString();
                            DateTime tempDate;
                            DateTime.TryParse(rdr["CheckOutDate"].ToString(), out tempDate);
                            item.CheckoutDate = tempDate;
                            item.Location = rdr["LocationTo"].ToString();
                            item.CustomerBadegNumber = rdr["BadgeNumber"].ToString();
                            item.ItemOwner = rdr["ItemOwner"].ToString();

                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }

        public List<ClassifiedItem> getUserCheckedOutItems(string firstName, string lastName) {
            List<ClassifiedItem> items = new List<ClassifiedItem>();

            // Connection
            using (m_LSRconn) {
                if (m_LSRconn.ConnectionString == null || m_LSRconn.ConnectionString.Length == 0)
                    m_LSRconn.ConnectionString = CIC_Configuration.LSRConnectionString;
                if (m_LSRconn.State != System.Data.ConnectionState.Open)
                    m_LSRconn.Open();

                using (SqlCommand cmd = new SqlCommand("procListCheckedOutSecurityByUser", m_LSRconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@FirstName", firstName));
                    cmd.Parameters.Add(new SqlParameter("@LastName", lastName));

                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            //Process the items
                            ClassifiedItem item = new ClassifiedItem();

                            long tempNum = 0;
                            long.TryParse(rdr["INUM"].ToString(), out tempNum);
                            item.INUM = tempNum;
                            item.Customer = rdr["Customer"].ToString();
                            item.Title = rdr["Title"].ToString();
                            item.Classification = rdr["Class"].ToString();
                            item.SerialNumber = rdr["serial_no"].ToString();
                            item.MediaType = rdr["MediaType"].ToString();
                            DateTime tempDate;
                            DateTime.TryParse(rdr["CheckOutDate"].ToString(), out tempDate);
                            item.CheckoutDate = tempDate;
                            item.Location = rdr["LocationTo"].ToString();
                            item.CustomerBadegNumber = rdr["BadgeNumber"].ToString();
                            item.ItemOwner = rdr["ItemOwner"].ToString();

                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }

        public List<ClassifiedItem> getUserOwnedItems(string badgeNumber) {
            List<ClassifiedItem> items = new List<ClassifiedItem>();

            // Connection
            using (m_LSRconn) {
                if (m_LSRconn.ConnectionString == null || m_LSRconn.ConnectionString.Length == 0)
                    m_LSRconn.ConnectionString = CIC_Configuration.LSRConnectionString;
                if (m_LSRconn.State != System.Data.ConnectionState.Open)
                    m_LSRconn.Open();

                using (SqlCommand cmd = new SqlCommand("procListOwnerByBadge", m_LSRconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@badgeNumber", badgeNumber));

                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            //Process the items
                            ClassifiedItem item = new ClassifiedItem();

                            long tempNum = 0;
                            long.TryParse(rdr["INUM"].ToString(), out tempNum);
                            item.INUM = tempNum;
                            item.Customer = rdr["Customer"].ToString();
                            item.Title = rdr["Title"].ToString();
                            item.Classification = rdr["Class"].ToString();
                            item.SerialNumber = rdr["serial_no"].ToString();
                            item.MediaType = rdr["MediaType"].ToString();
                            DateTime tempDate;
                            DateTime.TryParse(rdr["CheckOutDate"].ToString(), out tempDate);
                            item.CheckoutDate = tempDate;
                            item.Location = rdr["LocationTo"].ToString();
                            item.CustomerBadegNumber = rdr["BadgeNumber"].ToString();
                            item.ItemOwner = rdr["ItemOwner"].ToString();

                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }

        public List<ClassifiedItem> getUserOwnedItems(string firstName, string lastName) {
            List<ClassifiedItem> items = new List<ClassifiedItem>();

            // Connection
            using (m_LSRconn) {
                if (m_LSRconn.ConnectionString == null || m_LSRconn.ConnectionString.Length == 0)
                    m_LSRconn.ConnectionString = CIC_Configuration.LSRConnectionString;
                if (m_LSRconn.State != System.Data.ConnectionState.Open)
                    m_LSRconn.Open();

                using (SqlCommand cmd = new SqlCommand("procListByOwnerName", m_LSRconn)) {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@FirstName", firstName));
                    cmd.Parameters.Add(new SqlParameter("@LastName", lastName));

                    // execute the command
                    using (SqlDataReader rdr = cmd.ExecuteReader()) {
                        while (rdr.Read()) {
                            //Process the items
                            ClassifiedItem item = new ClassifiedItem();

                            long tempNum = 0;
                            long.TryParse(rdr["INUM"].ToString(), out tempNum);
                            item.INUM = tempNum;
                            item.Customer = rdr["Customer"].ToString();
                            item.Title = rdr["Title"].ToString();
                            item.Classification = rdr["Class"].ToString();
                            item.SerialNumber = rdr["serial_no"].ToString();
                            item.MediaType = rdr["MediaType"].ToString();
                            DateTime tempDate;
                            DateTime.TryParse(rdr["CheckOutDate"].ToString(), out tempDate);
                            item.CheckoutDate = tempDate;
                            item.Location = rdr["LocationTo"].ToString();
                            item.CustomerBadegNumber = rdr["BadgeNumber"].ToString();
                            item.ItemOwner = rdr["ItemOwner"].ToString();

                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }
        #endregion

        private object dbNull(object o) {
            if (o != null && !o.Equals("")) {
                return o;
            }
            return DBNull.Value;
        }
    }
}
