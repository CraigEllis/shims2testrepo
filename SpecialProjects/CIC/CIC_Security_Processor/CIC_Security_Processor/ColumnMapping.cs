﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CIC_Security_Processor {
    public class ColumnMapping {
        public static Dictionary<string, int> m_UserColMap = new Dictionary<string, int>();

        // Security User
        public const int USER_COLUMNS = 29;

        public const int USER_LAST_NAME = 0;
        public const int USER_FIRST_NAME = 1;
        public const int USER_MIDDLE_INITIAL = 2;
        public const int USER_PERSON_TYPE = 3;
        public const int USER_PHONE = 4;
        public const int USER_FAX = 5;
        public const int USER_EMAIL = 6;
        public const int USER_DEPT_CODE =  7;
        public const int USER_BADGE_NUMBER =  8;
        public const int USER_SECURITY_LEVEL =  9;
        public const int USER_SUPERVISOR_LAST_NAME = 10;
        public const int USER_SUPERVISOR_FIRST_NAME = 11;
        public const int USER_SUPERVISOR_MIDDLE_INITIAL = 12;
        public const int USER_SUPERVISOR_PHONE = 13;
        public const int USER_SUPERVISOR_EMAIL = 14;
        public const int USER_LOCATION = 15;
        public const int USER_ORG_TYPE = 16;
        public const int USER_ORG_CAGE = 17;
        public const int USER_ORG_NAME =  18;
        public const int USER_ORG_ADDRESS_1 =  19;
        public const int USER_ORG_ADDRESS_2 =  20;
        public const int USER_ORG_CITY =  21;
        public const int USER_ORG_STATE =  22;
        public const int USER_ORG_ZIP =  23;
        public const int USER_NATO =  24;
        public const int USER_CNWDI =  25;
        public const int USER_COURIER_CARD_NUMBER =  26;
        public const int USER_COURIER_CARD_EXPIRATION =  27;
        public const int USER_CAC_EID = 28;

        // Security Org
        public const int ORG_COLUMNS = 9;

        public const int ORG_CAGE = 0;
        public const int ORG_TYPE = 1;
        public const int ORG_ACRONYM = 2;
        public const int ORG_NAME = 3;
        public const int ORG_ADDRESS_1 = 4;
        public const int ORG_ADDRESS_2 = 5;
        public const int ORG_CITY = 6;
        public const int ORG_STATE = 7;
        public const int ORG_ZIP = 8;

    }
}
