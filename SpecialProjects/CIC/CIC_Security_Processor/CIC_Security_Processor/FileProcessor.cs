﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Common.Logging;
using System.Net.Mail;

namespace CIC_Security_Processor {
    public class FileProcessor {
        private const int TOTAL_COUNT = 0;
        private const int UPDATED_COUNT = 1;
        private const int NEW_COUNT = 2;
        private const int DISABLED_COUNT = 3;

        private static ILog logger = LogManager.GetCurrentClassLogger();
        private string HeaderRowStart = "last_name\tfirst_name";
        private List<string> m_processMessages = new List<string>();
        private int[] counts = new int[4]; 

        public bool processFile(string fileName) {
            bool result = true;

            // Clear the processMessages list
            m_processMessages.Clear();

            // Determine which file type we are dealing with
            if (isFileTypeUpdate(fileName)) {
                // Process the updates
                result = processUpdates(fileName);
            }
            else if (isFileTypeUsers(fileName)) {
                // Process the users
                result = processUsers(fileName);
            }
            else {
                // Log an error message that the file isn't in the correct format
                logger.Error(fileName + " - Invalid file format.");

                m_processMessages.Add("Invalid file format. ");

                result = false;
            }

            // Archive the file
            string archiveFileName = archiveFile(fileName, result);
            
            // Generate the system email
            EmailHandler handlr = new EmailHandler();

            MailMessage msg = handlr.generateFileNotificationEmail(fileName, result,
                archiveFileName, m_processMessages);

            handlr.sendEmailMessage(msg);

            return result;
        }

        private bool processUpdates(string fileName) {
            bool result = true;
            DateTime processDate = DateTime.Now;

            // Open the file and process the user records
            using (StreamReader reader = new StreamReader(fileName)) {
                bool foundData = false;
                string row;

                while ((row = reader.ReadLine()) != null) {
                    // Look for the start of data
                    if (row.ToUpper().StartsWith("//BOD"))
                        foundData = true;
                    else if (row.ToUpper().StartsWith("//EOD")) {
                        foundData = false;
                        break;
                    }
                    else if (foundData && result) {
                        // Split the data into the fields
                        string[] data = row.Split('\t');

                        // Check to make sure we have the correct number of fields
                        if (data.Length == ColumnMapping.USER_COLUMNS) {
                            long userID = processUserRecord(data, processDate);
                        }
                        else
                            result = false;
                    }
                }
            }

            return result;
        }

        private bool processUsers(string fileName) {
            bool result = true;
            Dictionary<string, long> users = new Dictionary<string, long>();
            DateTime processDate = DateTime.Now;

            // Open the file and process the user records
            using (StreamReader reader = new StreamReader(fileName)) {
                bool foundData = false;
                string row;

                while ((row = reader.ReadLine()) != null) {
                    // Look for the start of data
                    if (row.ToUpper().StartsWith("//BOD"))
                        foundData = true;
                    else if (row.ToUpper().StartsWith("//EOD")) {
                        foundData = false;
                        break;
                    }
                    else if (foundData && result) {
                        // Make sure we skip the header row
                        if (!row.ToLower().StartsWith(HeaderRowStart)) {
                            // Split the data into the fields
                            string[] data = row.Split('\t');

                            // Check to make sure we have the correct number of fields
                            if (data.Length == ColumnMapping.USER_COLUMNS) {
                                // Add the user to the dictionary
                                users.Add(data[ColumnMapping.USER_BADGE_NUMBER], 0);
                                counts[TOTAL_COUNT]++;

                                long userID = processUserRecord(data, processDate);
                                if (userID > 0)
                                    users[data[ColumnMapping.USER_BADGE_NUMBER]] = userID;
                            }
                            else
                                result = false;
                        }
                    }
                }

                if (result) {// Now go through and disable the current users who aren't in the table
                    disableMissingUsers(processDate);
                }

                // Populate the processing messages
                m_processMessages.Add(counts[TOTAL_COUNT].ToString());
                m_processMessages.Add(counts[UPDATED_COUNT].ToString());
                m_processMessages.Add(counts[NEW_COUNT].ToString());
                m_processMessages.Add(counts[DISABLED_COUNT].ToString());
            }

            return result;
        }

        // Process the user record into the database
        private long processUserRecord(string[] data, DateTime processDate) {
            long userID = 0;

            // Create the SecurityUser object - this allows us to update the Org Acronym field
            SecurityUser user = new SecurityUser(data);

            // Call the database manager to process the file into the User_Security table
            DatabaseManager mgr = new DatabaseManager();

            // See if this is not a NUWC user and we have an org CAGE
            if (!user.Org_Type.ToUpper().Equals("N")
                    && (user.Org_CAGE.Length > 0 
                    || user.Org_Name.Length > 0)) {
                // Process the organization
                SecurityOrg orgData = processOrgRecord(data, processDate);
            }

            // Set the status code
            string statusCode = "";
            SecurityUser userTemp = mgr.getSecurityUserRecord(user.Badge_Number);
            if (userTemp == null) {
                statusCode = "N";
                counts[NEW_COUNT]++;
            }
            else {
                statusCode = "U";
                counts[UPDATED_COUNT]++;
            }

            userID = mgr.processUserRecord(user, statusCode, processDate);

            return userID;
        }

        private bool disableMissingUsers(DateTime lastProcessDate) {
            bool result = true;
            DatabaseManager mgr = new DatabaseManager();

            // If someone is dropped we need to do a closeout check on them.
            // Get a list of newly disabled user badge numbers
            List<string> disabledUsers = mgr.getNewDisabledUsers(lastProcessDate);

            counts[DISABLED_COUNT] = disabledUsers.Count;

            if (disabledUsers.Count > 0) 
                // Generate the document lists for the newly disabled users
                result = generateDocumentLists(disabledUsers);

            return result;
        }

        private bool generateDocumentLists(List<string> badges) {
            bool result = true;
            DatabaseManager mgr = new DatabaseManager();

            foreach (string badge in badges) {
                // Get the user info
                SecurityUser user = mgr.getSecurityUserRecord(badge);

                // See if we have any checked out documents for this user
                List<ClassifiedItem> checkedOutDocs = mgr.getUserCheckedOutItems(badge);

                // See if we have any owned documents for this user
                List<ClassifiedItem> ownedDocs = mgr.getUserOwnedItems(badge);

                // Create and send the email
                EmailHandler handlr = new EmailHandler();
                MailMessage msg = handlr.generateUserCICEmail(user, checkedOutDocs,
                    ownedDocs);

                bool sent = handlr.sendEmailMessage(msg);
            }

            return result;
        }

        protected internal SecurityOrg processOrgRecord(string[] userData, DateTime processDate) {
            SecurityOrg orgRaw = new SecurityOrg(); 

            // Create the orgRaw array from the user Data
            orgRaw.Org_CAGE = userData[ColumnMapping.USER_ORG_CAGE];
            orgRaw.Org_Type = userData[ColumnMapping.USER_ORG_TYPE];
            orgRaw.Org_Name = userData[ColumnMapping.USER_ORG_NAME];
            orgRaw.Org_Address_1 = userData[ColumnMapping.USER_ORG_ADDRESS_1];
            orgRaw.Org_Address_2 = userData[ColumnMapping.USER_ORG_ADDRESS_2];
            orgRaw.Org_City = userData[ColumnMapping.USER_ORG_CITY];
            orgRaw.Org_State = userData[ColumnMapping.USER_ORG_STATE];
            orgRaw.Org_Zip = userData[ColumnMapping.USER_ORG_ZIP];

            // See if we have to build a CAGE from the Name
            if (orgRaw.Org_CAGE.Length == 0) {
                string tempCAGE = orgRaw.Org_Name;
                if (tempCAGE.Length > 7)
                    tempCAGE = tempCAGE.Substring(0, 6) + "_X";

                orgRaw.Org_CAGE = tempCAGE;
            }

            // Do the same if we don't have an acronym
            if (orgRaw.Org_Acronym == null || orgRaw.Org_Acronym.Length == 0) {
                orgRaw.Org_Acronym = orgRaw.Org_CAGE;
            }

            // Get the database manager
            DatabaseManager mgr = new DatabaseManager();
            SecurityOrg orgData = mgr.getSecurityOrgRecord(orgRaw.Org_CAGE);

            // See if we already have this org in the database
            if (orgData == null)
                mgr.insertSecurityOrgRecord(orgRaw, processDate);
            else
                orgData = orgRaw;

            return orgData;
        }

        private string archiveFile(string fileName, bool processed) {
            string archiveFileName = Path.GetFileNameWithoutExtension(fileName)
                + String.Format("{0:_yyyyMMdd_HHmm}", DateTime.Now)
                + Path.GetExtension(fileName);

            if (processed) {
                // Archive the file to the processed folder
                archiveFileName = CIC_Configuration.AppDataFolder + "\\processed\\" + archiveFileName;
            }
            else {
                // Archive the file to the errors folder
                archiveFileName = CIC_Configuration.AppDataFolder + "\\errors\\" + archiveFileName;
            }

            // Move and rename the upload file
            File.Move(fileName, archiveFileName);

            return archiveFileName;
        }

        private bool isFileTypeUpdate(string fileName) {
            bool result = false;

            // Open the file and read the first record
            using (StreamReader reader = new StreamReader(fileName)) {
                string line = reader.ReadLine();
                if (line.ToUpper().StartsWith("UPDATE"))
                    result = true;
            }

            return result;
        }

        private bool isFileTypeUsers(string fileName) {
            bool result = false;

            // Open the file and read the first record
            using (StreamReader reader = new StreamReader(fileName)) {
                string line = reader.ReadLine();
                if (line.ToUpper().StartsWith("USERS"))
                    result = true;
            }

            return result;
        }
    }
}
