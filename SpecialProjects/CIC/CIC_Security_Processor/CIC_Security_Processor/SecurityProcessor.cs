﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Logging;
using System.Threading;
using System.IO;

namespace CIC_Security_Processor {
    class SecurityProcessor {
        private static ILog logger = LogManager.GetCurrentClassLogger();

        private static Timer m_Timer = null;
        private static TimerCallback m_CallBack = null;
        private bool m_Processing = false;

        private string m_FileFolder = null;
        private int m_CheckInterval = 120000; // time in milliseconds

        // flag to keep the loop going - if you want to allow user cancellation, make it a property
        private bool m_KeepGoing = true;

        public void start() {
            // catch any start up errors and log them
            try {
                // Initialize things
                initialize();

                // Write startup message to log file
                logger.Info("Security_Processor started. File Folder = "
                    + m_FileFolder + " Check Interval = " + m_CheckInterval + " (minutes)");

                // Create the Timer and start it
                m_CallBack = new TimerCallback(CheckFiles);
                m_Timer = new Timer(m_CallBack, null, 0, m_CheckInterval);
            }
            catch (Exception ex) {
                // Write startup error message to log file
                logger.Error("Security_Processor start up error: " + ex.Message);
                
                // Clear the keep going flag
                m_KeepGoing = false;
            }

            // Put this puppy into an infinite loop
            while (m_KeepGoing) {
            }
        }

        private void initialize() {
            // Initialize the properties
            // Security File Folder
            m_FileFolder = CIC_Configuration.SecurityFileFolder;

            // Timer interval - make sure we don't overflow things
            if (CIC_Configuration.FileCheckInterval * 60000 > int.MaxValue)
                m_CheckInterval = int.MaxValue;
            else
                m_CheckInterval = CIC_Configuration.FileCheckInterval * 60000;
        }

        private void CheckFiles(Object stateInfo) {
            // If we are still processing files, skip until the next event
            FileProcessor parser = new FileProcessor();

            if (!m_Processing) {
                try {
                    // Set the processing flag
                    m_Processing = true;

                    // Get the new files
                    string[] newFiles = Directory.GetFiles(m_FileFolder);

                    // See if we have any new files to process
                    if (newFiles.Length > 0) {
                        logger.Debug(newFiles.Length.ToString() + " files to process.");

                        // Process the files
                        foreach (string fileName in newFiles) {
                            parser.processFile(fileName);
                        }
                    }
                    else {
                        // Nothing to process - log a message
                        logger.Debug(" No files to process.");
                    }
                }
                catch (Exception ex) {
                    logger.Error("Error in CheckFiles: " + ex.Message);
                }

                // Clear the processing flag
                m_Processing = false;
            }
        }

    }
}
