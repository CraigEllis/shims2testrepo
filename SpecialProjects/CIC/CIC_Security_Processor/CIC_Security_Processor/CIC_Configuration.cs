﻿using System.Configuration;
using System.Net.Configuration;

namespace CIC_Security_Processor {
    /// <summary>
    /// Gets the application settings from the app.config file
    /// </summary>
    public static class CIC_Configuration {
        public const string ACTION_NEW = "N";
        public const string ACTION_UPDATE = "U";
        public const string ACTION_DISABLE = "D";
        //public const string ACTION_OTHER = "O";
        //public const string ACTION_REMOVE = "R";
        //public const string ACTION_TRANSFER = "T";

        /// <summary>
        /// Returns the database Connection String pointing to the LBITS User_Security table
        /// </summary>
        public static string LBITSConnectionString {
            get {
                return ConfigurationManager.ConnectionStrings["LBITS_ConnectionString"].ConnectionString;
            }
        }

        /// <summary>
        /// Returns the database Connection String pointing to the LSR User_Security table
        /// </summary>
        public static string LSRConnectionString {
            get {
                return ConfigurationManager.ConnectionStrings["LSR_ConnectionString"].ConnectionString;
            }
        }

        /// <summary>
        /// Returns the path to the folder where the application looks for new 
        /// security files to process
        /// </summary>
        public static string SecurityFileFolder {
            get {
                return ConfigurationManager.AppSettings["SecurityFileFolder"];
            }
        }

        /// <summary>
        /// Returns the path to the folder where the application looks for new 
        /// security files to process
        /// </summary>
        public static string AppDataFolder {
            get {
                return ConfigurationManager.AppSettings["AppDataFolder"];
            }
        }

        /// <summary>
        /// Returns a string containing the email addresses to send user update 
        /// information to
        /// </summary>
        public static string EmailNotification {
            get {
                return ConfigurationManager.AppSettings["EmailNotification"];
            }
        }

        /// <summary>
        /// Returns a string containing the email addresses to send file upload and other  
        /// system information to
        /// </summary>
        public static string SystemNotification {
            get {
                return ConfigurationManager.AppSettings["SystemNotification"];
            }
        }

        /// <summary>
        /// Returns the time interval in minutes to check for new intervals
        /// </summary>
        public static int FileCheckInterval {
            get {
                // Default = 2 minutes
                int interval = 2;

                int.TryParse(ConfigurationManager.AppSettings["FileCheckInterval"], 
                    out interval);

                return interval;
            }
        }

        public static MailSettingsSectionGroup EmailSettings {
            get {
                Configuration configFile = ConfigurationManager.OpenExeConfiguration(
                    System.Configuration.ConfigurationUserLevel.None);

                return (MailSettingsSectionGroup)configFile.GetSectionGroup("system.net/mailSettings");
            }
        }
    }
}
