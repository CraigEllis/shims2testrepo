﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CIC_Security_Processor {
    public class ClassifiedItem {
        private long m_INUM = 0;
        public long INUM {
            get { return m_INUM; }
            set { m_INUM = value; }
        }

        private string m_Barcode = null;
        public string Barcode {
            get { return m_Barcode; }
            set { m_Barcode = value; }
        }

        private string m_SerialNumber = null;
        public string SerialNumber {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        private string m_Customer = null;
        public string Customer {
            get { return m_Customer; }
            set { m_Customer = value; }
        }

        private string m_CustomerBadegNumber = null;
        public string CustomerBadegNumber {
            get { return m_CustomerBadegNumber; }
            set { m_CustomerBadegNumber = value; }
        }

        private string m_ItemOwner = null;
        public string ItemOwner {
            get { return m_ItemOwner; }
            set { m_ItemOwner = value; }
        }

        private string m_OwnerBadegNumber = null;
        public string OwnerBadegNumber {
            get { return m_OwnerBadegNumber; }
            set { m_OwnerBadegNumber = value; }
        }

        private string m_Title = null;
        public string Title {
            get { return m_Title; }
            set { m_Title = value; }
        }

        private string m_MediaType = null;
        public string MediaType {
            get { return m_MediaType; }
            set { m_MediaType = value; }
        }

        private string m_Classification = null;
        public string Classification {
            get { return m_Classification; }
            set { m_Classification = value; }
        }

        private string m_Location = null;
        public string Location {
            get { return m_Location; }
            set { m_Location = value; }
        }

        private DateTime m_CheckoutDate;
        public DateTime CheckoutDate {
            get { return m_CheckoutDate; }
            set { m_CheckoutDate = value; }
        }
    }
}
