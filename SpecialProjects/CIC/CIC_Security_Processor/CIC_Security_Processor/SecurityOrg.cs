﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CIC_Security_Processor {
    public class SecurityOrg {
        private string m_Org_Type = null;

        public string Org_Type {
            get { return m_Org_Type; }
            set { m_Org_Type = value; }
        }

        private string m_Org_CAGE = null;
        public string Org_CAGE {
            get { return m_Org_CAGE; }
            set { m_Org_CAGE = value; }
        }

        private string m_Org_Acronym = null;
        public string Org_Acronym {
            get { return m_Org_Acronym; }
            set { m_Org_Acronym = value; }
        }

        private string m_Org_Name = null;
        public string Org_Name {
            get { return m_Org_Name; }
            set { m_Org_Name = value; }
        }

        private string m_Org_Address_1 = null;
        public string Org_Address_1 {
            get { return m_Org_Address_1; }
            set { m_Org_Address_1 = value; }
        }

        private string m_Org_Address_2 = null;
        public string Org_Address_2 {
            get { return m_Org_Address_2; }
            set { m_Org_Address_2 = value; }
        }

        private string m_Org_City = null;
        public string Org_City {
            get { return m_Org_City; }
            set { m_Org_City = value; }
        }

        private string m_Org_State = null;
        public string Org_State {
            get { return m_Org_State; }
            set { m_Org_State = value; }
        }

        private string m_Org_Zip = null;
        public string Org_Zip {
            get { return m_Org_Zip; }
            set { m_Org_Zip = value; }
        }

        private string m_Org_FSO_Name = null;
        public string Org_FSO_Name {
            get { return m_Org_FSO_Name; }
            set { m_Org_FSO_Name = value; }
        }

        private string m_Org_FSO_Email = null;
        public string Org_FSO_Email {
            get { return m_Org_FSO_Email; }
            set { m_Org_FSO_Email = value; }
        }

        private string m_Org_FSO_Phone = null;
        public string Org_FSO_Phone {
            get { return m_Org_FSO_Phone; }
            set { m_Org_FSO_Phone = value; }
        }

        public string[] ToArray() {
            string[] org = new string[ColumnMapping.ORG_COLUMNS];

            // Populate the org with test data 
            org[ColumnMapping.ORG_CAGE] = m_Org_CAGE;
            org[ColumnMapping.ORG_TYPE] = m_Org_Type;
            org[ColumnMapping.ORG_ACRONYM] = m_Org_Acronym;
            org[ColumnMapping.ORG_NAME] = m_Org_Name;
            org[ColumnMapping.ORG_ADDRESS_1] = m_Org_Address_1;
            org[ColumnMapping.ORG_ADDRESS_2] = m_Org_Address_2;
            org[ColumnMapping.ORG_CITY] = m_Org_City;
            org[ColumnMapping.ORG_STATE] = m_Org_State;
            org[ColumnMapping.ORG_ZIP] = m_Org_Zip;

            return org;
        }
    }
}
