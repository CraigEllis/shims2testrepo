﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Logging;
using System.IO;

namespace CIC_Security_Processor {
    class Program {
        static void Main(string[] args) {
            // Check input folder existence
            checkFolderExists(CIC_Configuration.SecurityFileFolder);
            checkFolderExists(CIC_Configuration.AppDataFolder);
            // Check sub folders existence
            checkFolderExists(CIC_Configuration.AppDataFolder + "\\processed");
            checkFolderExists(CIC_Configuration.AppDataFolder + "\\errors");

	        // Start processor listening
            Console.WriteLine("CIC_Security_Processor started - " 
                + String.Format("{0:yyyy-MM-dd HHmm}", DateTime.Now));

            SecurityProcessor proc = new SecurityProcessor();
            proc.start();

            Console.WriteLine("CIC_Security_Processor closed - "
                + String.Format("{0:yyyy-MM-dd HHmm}", DateTime.Now));
        }        

        private static void checkFolderExists(string folderName) {
            if (!Directory.Exists(folderName)) {
                // Create the folder
                Directory.CreateDirectory(folderName);
            }
        }
    }
}
