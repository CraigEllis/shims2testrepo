USE [LSRv00];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

/****** Object: View [dbo].[vListCheckoutsSecurity]   Script Date: 4/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vListCheckoutsSecurity]'))
DROP VIEW [dbo].[vListCheckoutsSecurity]
GO

CREATE VIEW [dbo].[vListCheckoutsSecurity]
AS
SELECT     
  tabA.inum,  
  tabC.ID AS MediaHistID, 
  tabC.Name AS CustomerName, 
  tabA.item_title AS item_title, 
  tabA.security AS Classification, 
  tabA.serial_no AS serial_no, 
  tabA.Media AS MediaType, 
  tabA.idate AS DateReceived, 
  tabA.LastInventoryDate AS LastInventoryDate, 
  tabC.CheckOutDate AS CheckOutDate, 
  tabC.[Due Date] AS ReturnDueDate, 
  tabC.Status AS MediaHistStatus, 
  tabA.Status AS LibraryStatus, 
  tabC.LocationTo AS LocationTo,
  tabC.NUWCNumber AS BadgeNumber,
  tabA.Owner_Originator AS ItemOwner
FROM dbo.Person_Item tabC 
INNER JOIN dbo.Item tabA ON tabA.inum = tabC.inum
INNER JOIN
  (SELECT MAX(ID) AS LatestMediaHistID, INUM
   FROM dbo.Person_item
   GROUP BY INUM) tabB 
  ON tabC.ID = tabB.LatestMediaHistID 
WHERE  (CHARINDEX(''Loan'', taba.Status) > 0) AND (CHARINDEX(''Transmittal'', taba.Status) = 0)
GO

/****** Object: Procedure [dbo].[procListCheckedOutByUser]   Script Date: 4/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procListCheckedOutSecurityByUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procListCheckedOutSecurityByUser]
GO

CREATE PROCEDURE [dbo].[procListCheckedOutSecurityByUser]
(
@FirstName nvarchar(50) = null,
@LastName nvarchar(50) = null
)
AS
BEGIN
DECLARE @LookupName nvarchar(100)
SET @LookupName = @LastName + ', ' + @FirstName
SELECT INUM,
       CustomerName As Customer,
       item_title As Title,
       Classification As Class,
       serial_no,
       MediaType,
       DateReceived,
       LastInventoryDate,
       CheckOutDate,
       ReturnDueDate,
       LocationTo,
       BadgeNumber,
	   ItemOwner
       FROM vListCheckoutsSecurity
WHERE (CHARINDEX(@LookupName,CustomerName) > 0)
ORDER BY INUM DESC;
END
GO

/****** Object: Procedure [dbo].[procListCheckedOutByBadge]   Script Date: 4/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procListCheckedOutByBadge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procListCheckedOutByBadge]
GO

CREATE PROCEDURE [dbo].[procListCheckedOutByBadge]
(
	@BadgeNumber nvarchar(50) = null
)
AS
BEGIN
DECLARE @LookupName nvarchar(100)
SET @LookupName = (SELECT LastName + ', ' + FirstName FROM Customers WHERE NuwcNumber = @BadgeNumber);
SELECT INUM,
       CustomerName As Customer,
       item_title As Title,
       Classification As Class,
       serial_no,
       MediaType,
       DateReceived,
       LastInventoryDate,
       CheckOutDate,
       ReturnDueDate,
       LocationTo,
       BadgeNumber,
       ItemOwner
FROM vListCheckoutsSecurity
END
GO

/****** Object: View [dbo].[vListOwner]   Script Date: 4/23/2012 ******/
USE [LSRv00];
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vListOwner]'))
DROP VIEW [dbo].[vListOwner]
GO

CREATE VIEW [dbo].[vListOwner]
AS
SELECT
  tabA.inum,  
  tabB.LatestMediaHistID AS MediaHistID, 
  tabC.Name AS CustomerName, 
  tabA.item_title AS item_title, 
  tabA.security AS Classification, 
  tabA.serial_no AS serial_no, 
  tabA.Media AS MediaType, 
  tabA.idate AS DateReceived, 
  tabA.LastInventoryDate AS LastInventoryDate, 
  tabC.CheckOutDate AS CheckOutDate, 
  tabC.[Due Date] AS ReturnDueDate, 
  tabC.Status AS MediaHistStatus, 
  tabA.Status AS LibraryStatus, 
  tabC.LocationTo AS LocationTo,
  tabC.NUWCNumber AS BadgeNumber,
  tabA.Owner_Originator AS ItemOwner
FROM dbo.Item tabA
left outer JOIN
  (SELECT MAX(ID) AS LatestMediaHistID, INUM
   FROM dbo.Person_item
   GROUP BY INUM) tabB 
  ON tabA.inum = tabB.INUM
LEFT OUTER JOIN Person_item tabC on tabC.ID = tabB.LatestMediaHistID 
GO

/****** Object: Procedure [dbo].[[procListByOwnerName]]   Script Date: 4/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procListByOwnerName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procListByOwnerName]
GO

CREATE PROCEDURE [dbo].[procListByOwnerName]
(
@FirstName nvarchar(50) = null,
@LastName nvarchar(50) = null
)
AS
BEGIN
DECLARE @LookupName nvarchar(100)
SET @LookupName = @LastName + ', ' + @FirstName
SELECT
  inum,  
  COALESCE(CustomerName, '''') AS Customer, 
  item_title AS Title, 
  Classification AS Class, 
  serial_no, 
  MediaType, 
  DateReceived, 
  LastInventoryDate, 
  CheckOutDate, 
  ReturnDueDate, 
  MediaHistStatus, 
  LibraryStatus,
  LocationTo,
  BadgeNumber,
  ItemOwner
FROM vListOwner
WHERE (CHARINDEX(@LookupName,ItemOwner) > 0)
ORDER BY INUM DESC;
END
GO


/****** Object: Procedure [dbo].[[procListOwnerByBadge]]   Script Date: 4/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procListOwnerByBadge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procListOwnerByBadge]
GO

CREATE PROCEDURE [dbo].[procListOwnerByBadge]
(
	@BadgeNumber nvarchar(50) = null
)
AS
BEGIN
DECLARE @LookupName nvarchar(100)
SET @LookupName = (SELECT LastName + '', '' + FirstName FROM Customers WHERE NuwcNumber = @BadgeNumber);
SELECT
  inum,  
  COALESCE(CustomerName, '''') AS Customer, 
  item_title AS Title, 
  Classification AS Class, 
  serial_no, 
  MediaType, 
  DateReceived, 
  LastInventoryDate, 
  CheckOutDate, 
  ReturnDueDate, 
  MediaHistStatus, 
  LibraryStatus,
  LocationTo,
  BadgeNumber,
  ItemOwner
FROM vListOwner
WHERE (CHARINDEX(@LookupName,ItemOwner) > 0)
ORDER BY INUM DESC;
END
GO

