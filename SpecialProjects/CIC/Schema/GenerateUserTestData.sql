use LSRv00;
-- QUERY to build test data for User Security import
select distinct cu.lastname as last_name, cu.firstname as first_name, coalesce(cu.mi, coalesce(pp.mi, '')) as middle_initial, 
coalesce(cu.Organization, '') as person_type,
coalesce(cu.phone, coalesce(pp.voice,'')) as phone, coalesce(pp.fax, '') as fax, 
coalesce(cu.emailaddress, coalesce(pp.email, '')) as email, 
left(coalesce(pp.code, coalesce(cu.code, '')), 2) as dept_code, cu.nuwcnumber as badge_number, 
'S' as security_level,  
'' as supv_last_name, '' as supv_first_name, '' as supv_middle_initial, 
'' as supv_phone, '' as supv_email, '' as location,  
coalesce(pp.otype, '') as org_type, '' as org_cage, 
coalesce(og.orgnam, '') as org_name, 
coalesce(og.orgaddr, '') as org_address_1, '' as org_address_2, coalesce(og.orgcty, '') as org_city, 
coalesce(og.orgstate, '') as org_state, coalesce(og.orgzip, '') as org_zip,
'N' as NATO, '0' AS SAP,
'' as courier_card_number, '' as courier_card_expiration, 
coalesce((pp.X509IssuerCN + ' ' + pp.X509SubjectCN), '') as cac_eid
from customers cu
left join lbits001.dbo.tblPeople pp on rtrim(pp.LNam) = cu.LastName and rtrim(pp.FNam) = cu.FirstName
left join lbits001.dbo.tblOrg og on og.OrgID = pp.OrgID and pp.UserID not like '%test%'
where cu.firstname is not null and cu.LastName is not null
order by cu.LastName, cu.FirstName;

-- QUERY to build test data for Unit Tests (50 users)
select distinct cu.lastname as last_name, cu.firstname as first_name, coalesce(cu.mi, coalesce(pp.mi, '')) as middle_initial, 
coalesce(cu.Organization, '') as person_type,
coalesce(cu.phone, coalesce(pp.voice,'')) as phone, coalesce(pp.fax, '') as fax, 
coalesce(cu.emailaddress, coalesce(pp.email, '')) as email, 
left(coalesce(pp.code, coalesce(cu.code, '')), 2) as dept_code, cu.nuwcnumber as badge_number, 
'S' as security_level,  
'' as supv_last_name, '' as supv_first_name, '' as supv_middle_initial, 
'' as supv_phone, '' as supv_email, '' as location,  
coalesce(pp.otype, '') as org_type, '' as org_cage, 
coalesce(og.orgnam, '') as org_name, 
coalesce(og.orgaddr, '') as org_address_1, '' as org_address_2, coalesce(og.orgcty, '') as org_city, 
coalesce(og.orgstate, '') as org_state, coalesce(og.orgzip, '') as org_zip,
'N' as NATO, '0' AS SAP,
'' as courier_card_number, '' as courier_card_expiration, 
coalesce((pp.X509IssuerCN + ' ' + pp.X509SubjectCN), '') as cac_eid
from customers cu
left join lbits001.dbo.tblPeople pp on rtrim(pp.LNam) = cu.LastName and rtrim(pp.FNam) = cu.FirstName
left join lbits001.dbo.tblOrg og on og.OrgID = pp.OrgID and pp.UserID not like '%test%'
where cu.NUWCNumber in (
'16530','16532','19394','17841','16926','19537','19572','14927','19589','19652',
'17398','16606','18197','17354','19721','17601','25261','17113','19742','230',
'15493','16600','19471','22132','19369','12121','18503','17942','23673','14906',
'16491','23199','19388','16523','20320','17000','17474','18496','16636','14692',
'18726','19365','16286','23243','14962','19458','26032','25103','16705','16677',
'19505','19699','24241','19398','19632','19845','16595'

)
order by cu.LastName, cu.FirstName;

