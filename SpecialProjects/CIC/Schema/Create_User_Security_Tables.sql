use [LBITS001]
GO

/****** Object:  Table [dbo].[User_Security]    Script Date: 08/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Security]') AND type in (N'U'))
DROP TABLE [dbo].[User_Security]
GO

CREATE TABLE [dbo].[User_Security] 
(
	[User_Security_ID]	[INT] IDENTITY,
	[Last_Name]			[NVARCHAR](30) NOT NULL,
	[First_Name]		[NVARCHAR](25) NOT NULL,
	[Middle_Initial]	[NVARCHAR](3) NULL,
	[Person_Type]	[NVARCHAR](20) NULL,
	[Phone]				[NVARCHAR](35)  NULL,
	[Fax]				[NVARCHAR](15)  NULL,
	[Email]				[NVARCHAR](129)  NULL,
	[Dept_Code]			[NVARCHAR](10) NULL,
	[Badge_Number]		[NVARCHAR](50) NOT NULL,
	[Security_Level]	[NVARCHAR](50) NOT NULL, -- U, IC, C, IS, S, IT, T
	[Supervisor_Last_Name]	[NVARCHAR](30) NULL,
	[Supervisor_First_Name]	[NVARCHAR](25) NULL,
	[Supervisor_Middle_Initial]	[NVARCHAR](3) NULL,
	[Supervisor_Phone]	[NVARCHAR](35) NULL,
	[Supervisor_Email]	[NVARCHAR](129) NULL,
	[Location]			[NVARCHAR](20) NULL,
	[Org_Type]			[NVARCHAR](1) DEFAULT 'N', -- 'N'= NUWC Employee, 'C' = Contractor, 'A' = Agency (government)
	[Org_CAGE]			[NVARCHAR](10) NULL, -- Organization CAGE/Contract # (Blank unless Org_Type is 'C' or 'A')
	[Org_Acronym]		[NVARCHAR](15) NULL, -- Organization Acronym (Blank unless Org_Type is 'C' or 'A')
	[NATO]				[NVARCHAR](12) NULL,
	[CNWDI]				[NVARCHAR](12) NULL,
	[Courier_Card_Number]	[NVARCHAR](10) NULL,
	[Courier_Card_Expiration]	[DateTime] NULL,
	[CAC_EID]			[NVARCHAR](60) NOT NULL,
	[Status_Code]		[NVARCHAR](1) NOT NULL, -- 'A' = Active, 'P' = Pending Action, 'D' = Disabled
	[Changed_Date]		[DateTime] NOT NULL,	
	[X509SubjectCN]		[NVARCHAR](255) NULL, -- Subject section from CAC card
	[X509IssuerCN]		[NVARCHAR](255) NULL, -- Issuer section from CAC card
	[Courier_Card]		[NVARCHAR](1) NULL,
	[Courier_Card_Issued]	[DateTime] NULL,
	[Courier_Card_Classification]	[NVARCHAR](2) NULL, -- U, IC, C, IS, S, IT, T
	[Courier_Card_Geo_Limit]	[NVARCHAR](1) NULL, -- 
	[Special_Access]	[NVARCHAR](1)  NULL -- 0 = no access
)
GO

CREATE INDEX INDX_User_Security_Badge_Number ON [User_Security]([Badge_Number])
GO


/****** Object:  Table [dbo].[User_Status_Code]    Script Date: 08/16/2012 12:04:35 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_Status_Code]') AND type in (N'U'))
--DROP TABLE [dbo].[User_Status_Code]
--GO
--
--CREATE TABLE [dbo].[User_Status_Code] 
--(
--	[Status_Code]			[NVARCHAR](1)  PRIMARY KEY, -- 'A' = Active, 'P' = Pending Action, 'D' = Disabled
--	[Description]			[NVARCHAR](100) NULL, 
--	[Active]		[NVARCHAR](1) NULL
--)
--GO


/****** Object:  Table [dbo].[Org_Security]    Script Date: 08/23/2012 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Org_Security]') AND type in (N'U'))
DROP TABLE [dbo].[Org_Security]
GO

CREATE TABLE [dbo].[Org_Security] 
(
	[Org_Security_ID]	[INT] IDENTITY,
	[Org_CAGE]			[NVARCHAR](10) NOT NULL, -- Organization CAGE/Contract 
	[Org_Type]			[NVARCHAR](1) NOT NULL, 
	[Org_Acronym]		[NVARCHAR](15) NULL, 
	[Org_Name]			[NVARCHAR](78) NULL, 
	[Org_Address_1]		[NVARCHAR](50) NULL, 
	[Org_Address_2]		[NVARCHAR](50) NULL, 
	[Org_City]			[NVARCHAR](25) NULL, 
	[Org_State]			[NVARCHAR](3) NULL, 
	[Org_Zip]	[NVARCHAR](12) NULL, 
	[Org_FSO_Name]		[NVARCHAR](85) NULL, 
	[Org_FSO_Email]		[NVARCHAR](129) NULL, 
	[Org_FSO_Phone]		[NVARCHAR](35) NULL, 
	[Changed_Date]		[DateTime] NULL	
)

CREATE INDEX INDX_Org_Security_CAGE ON [Org_Security]([Org_CAGE])

GO
