USE [LBITS001]
GO

/****** Object:  Trigger [tr_insert_user_security]    Script Date: 09/20/2012 13:14:06 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_user_security]'))
DROP TRIGGER [dbo].[tr_insert_user_security]
GO

USE [LBITS001]
GO

/****** Object:  Trigger [dbo].[tr_insert_user_security]    Script Date: 09/20/2012 13:14:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_user_security]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[tr_insert_user_security]
ON [dbo].[User_Security] AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @orgcage VARCHAR(10);
	DECLARE @orgid INT;
	
	SELECT @orgcage = (SELECT Org_CAGE FROM INSERTED);
	
	IF (@orgcage IS NULL)
		SET @orgid = null;
	ELSE
		SELECT @orgid = OrgID FROM tblOrg WHERE OrgNam IN 
			(SELECT Org_Name FROM Org_Security WHERE Org_CAGE = @orgcage);

	INSERT INTO tblPeople (
		UserID,
		FNam,
		MI,
		LNam,
		Voice,
		Fax,
		Email,
		OType,
		OrgID,
		Code,
		X509SubjectCN,
		X509IssuerCN,
		NATO,
		SAP,
		Security_Level,
		NUWCNumber,
		Organization		
	) 
	SELECT   
		LEFT(sec.First_Name  + ''.'' + sec.Last_Name, 83) AS UserID,
		sec.First_Name AS FNam,
		sec.Middle_Initial AS MI,
		LEFT(sec.Last_Name, 25) AS FNam,
		LEFT(sec.Phone, 15) AS Voice,
		sec.Fax AS Fax,
		sec.Email AS Email,
		sec.Org_Type AS OType,
		@orgid AS OrgID, 
		LEFT(sec.Dept_Code, 8) AS Code,
		sec.X509SubjectCN AS X509SubjectCN,
		sec.X509IssuerCN AS X509IssuerCN,
		(CASE sec.NATO
			WHEN ''Y'' then -1
			ELSE 0
		END) AS NATO,
		sec.CNWDI AS SAP,
		sec.Security_Level,
		sec.Badge_Number AS NUWCNumber,
		sec.Person_Type AS Organization
	FROM User_Security sec
	JOIN INSERTED ins ON ins.Badge_Number = sec.Badge_Number;
END
' 
GO

USE [LBITS001]
GO

/****** Object:  Trigger [tr_update_user_security]    Script Date: 09/20/2012 13:14:36 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_user_security]'))
DROP TRIGGER [dbo].[tr_update_user_security]
GO

USE [LBITS001]
GO

/****** Object:  Trigger [dbo].[tr_update_user_security]    Script Date: 09/20/2012 13:14:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_user_security]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[tr_update_user_security]
ON [dbo].[User_Security] AFTER UPDATE
AS
BEGIN
	DECLARE @status VARCHAR(1)
	DECLARE @fnam VARCHAR(25)
	DECLARE @lnam VARCHAR(30)
	DECLARE @mi VARCHAR(3)
	DECLARE @nato VARCHAR(12)
	DECLARE @natonum INT
	
	SELECT @status = (SELECT Status_Code FROM inserted);
	SELECT @fnam = (SELECT First_Name FROM inserted);
	SELECT @lnam = (SELECT Last_Name FROM inserted);
	SELECT @mi = (SELECT Middle_Initial FROM inserted);
	SELECT @nato = (SELECT NATO FROM inserted);
	
	SET @natonum = 0
	IF (@nato = ''Y'')
		SET @natonum = -1
	
	IF @status = ''D''   
			DELETE FROM tblPeople WHERE FNam = @fnam AND LNam = @lnam 
				AND MI = @mi;
	ELSE
		BEGIN
			SET NOCOUNT ON;

			UPDATE tp 
				SET tp.UserID = sec.First_Name  + ''.'' + sec.Last_Name,
				tp.FNam = sec.First_Name,
				tp.MI = sec.Middle_Initial,
				tp.LNam = sec.Last_Name,
				tp.Voice = sec.Phone,
				tp.Fax = sec.Fax,
				tp.Email = sec.Email,
				tp.OType = sec.Org_Type,
				tp.OrgID = (SELECT  TOP 1 OrgID FROM tblOrg WHERE OrgNam IN 
					(SELECT Org_Name FROM Org_Security WHERE Org_CAGE = sec.Org_CAGE)),
				tp.Code = sec.Dept_Code,
				tp.X509SubjectCN = sec.X509SubjectCN,
				tp.X509IssuerCN = sec.X509IssuerCN,	
				tp.NATO = @natonum,
				tp.SAP = CAST(sec.CNWDI AS INT),
				tp.Security_Level = sec.Security_Level,
				tp.NUWCNumber = sec.Badge_Number,
				tp.Organization = sec.Person_Type
			FROM tblPeople tp
			JOIN User_Security sec ON sec.First_Name = tp.FNam AND sec.Last_Name = tp.LNam AND sec.Middle_Initial = tp.MI
			JOIN INSERTED ins ON ins.Badge_Number = sec.Badge_Number;
		END
END
' 
GO

USE [LBITS001]
GO

/****** Object:  Trigger [tr_insert_org_security]    Script Date: 09/20/2012 13:15:34 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_org_security]'))
DROP TRIGGER [dbo].[tr_insert_org_security]
GO

USE [LBITS001]
GO

/****** Object:  Trigger [dbo].[tr_insert_org_security]    Script Date: 09/20/2012 13:15:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_insert_org_security]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[tr_insert_org_security]
ON [dbo].[Org_Security] AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tblOrg (
		OrgType,
		OrgAcro,
		OrgNam,
		OrgAddr,
		OrgCty,
		OrgState,
		OrgZip
	) 
	SELECT   
		Org_Type,
		Org_Acronym,
		Org_Name,
		Org_Address_1,
		Org_City,
		Org_State,
		Org_Zip
	FROM INSERTED;
END
' 
GO

USE [LBITS001]
GO

/****** Object:  Trigger [tr_update_org_security]    Script Date: 09/20/2012 13:15:58 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_org_security]'))
DROP TRIGGER [dbo].[tr_update_org_security]
GO

USE [LBITS001]
GO

/****** Object:  Trigger [dbo].[tr_update_org_security]    Script Date: 09/20/2012 13:15:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[tr_update_org_security]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[tr_update_org_security]
ON [dbo].[Org_Security] FOR UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblOrg 
		SET tblOrg.OrgType = sec.Org_Type,
		tblOrg.OrgAcro = sec.Org_Acronym,
		tblOrg.OrgNam = sec.Org_Name,
		tblOrg.OrgAddr = sec.Org_Address_1,
		tblOrg.OrgCty = sec.Org_City,
		tblOrg.OrgState = sec.Org_State,
		tblOrg.OrgZip = sec.Org_Zip
	FROM dbo.tblOrg
	JOIN dbo.Org_Security sec ON sec.Org_Name = tblOrg.OrgNam
	JOIN INSERTED ins ON ins.Org_CAGE = sec.Org_CAGE
END
' 
GO

