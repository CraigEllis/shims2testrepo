USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procAddSecurityUser]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procAddSecurityUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procAddSecurityUser]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procAddSecurityUser    Script Date: 08/23/2012 ******/
CREATE PROCEDURE [dbo].[procAddSecurityUser] (
	@Last_Name NVARCHAR(30),
	@First_Name NVARCHAR(25),
	@Middle_Initial NVARCHAR(3) = NULL,
	@Person_Type NVARCHAR(20) = NULL,
	@Phone NVARCHAR(35)  = NULL,
	@Fax NVARCHAR(15)  = NULL,
	@Email NVARCHAR(129)  = NULL,
	@Dept_Code NVARCHAR(10) = NULL,
	@Badge_Number NVARCHAR(50),
	@Security_Level NVARCHAR(50), 
	@Supervisor_Last_Name NVARCHAR(30) = NULL,
	@Supervisor_First_Name NVARCHAR(25) = NULL,
	@Supervisor_Middle_Initial NVARCHAR(3) = NULL,
	@Supervisor_Phone NVARCHAR(35) = NULL,
	@Supervisor_Email NVARCHAR(129) = NULL,
	@Location NVARCHAR(20) = NULL,
	@Org_Type NVARCHAR(1), 
	@Org_CAGE NVARCHAR(10) = NULL, 
	@Org_Acronym NVARCHAR(15) = NULL, 
	@NATO NVARCHAR(12) = NULL,
	@CNWDI NVARCHAR(12) = NULL,
	@Courier_Card_Number NVARCHAR(10) = NULL,
	@Courier_Card_Expiration DateTime = NULL,
	@CAC_EID NVARCHAR(60) = NULL,
	@X509SubjectCN NVARCHAR(255) = NULL, 
	@X509IssuerCN NVARCHAR(255) = NULL, 
	@Courier_Card NVARCHAR(1) = NULL,
	@Courier_Card_Issued DateTime = NULL,
	@Courier_Card_Classification NVARCHAR(2) = NULL, 
	@Courier_Card_Geo_Limit NVARCHAR(1) = NULL,  
	@Special_Access NVARCHAR(1) = NULL, 
	@Status_Code NVARCHAR(1), 
	@Changed_Date DateTime,
	@User_Security_ID INT OUTPUT
)
AS
INSERT INTO User_Security(
	Last_Name, 	First_Name, Middle_Initial, Person_Type, 
	Phone, Fax, Email,
	Dept_Code, Badge_Number, Security_Level,
	Supervisor_Last_Name, Supervisor_First_Name, Supervisor_Middle_Initial,
	Supervisor_Phone, Supervisor_Email,
	Location,
	Org_Type, Org_CAGE, Org_Acronym,
	NATO, CNWDI,
	Courier_Card_Number, Courier_Card_Expiration, CAC_EID,
	Status_Code, Changed_Date,
	X509SubjectCN, X509IssuerCN,
	Courier_Card, Courier_Card_Issued, 
	Courier_Card_Classification, Courier_Card_Geo_Limit, 
	Special_Access
)
VALUES(
	@Last_Name, @First_Name, @Middle_Initial, @Person_Type,
	@Phone, @Fax, @Email,
	@Dept_Code, @Badge_Number, @Security_Level,
	@Supervisor_Last_Name, @Supervisor_First_Name, @Supervisor_Middle_Initial,
	@Supervisor_Phone, @Supervisor_Email,
	@Location,
	@Org_Type, @Org_CAGE, @Org_Acronym,
	@NATO, @CNWDI,
	@Courier_Card_Number, @Courier_Card_Expiration, @CAC_EID,
	@Status_Code, @Changed_Date,
	@X509SubjectCN, @X509IssuerCN,
	@Courier_Card, @Courier_Card_Issued,
	@Courier_Card_Classification, @Courier_Card_Geo_Limit, 
	@Special_Access
)

SELECT @User_Security_ID = @@IDENTITY
' 
END
GO


USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procUpdateSecurityUser]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procUpdateSecurityUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procUpdateSecurityUser]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procUpdateSecurityUser    Script Date: 8/23/2012 ******/

CREATE PROCEDURE [dbo].[procUpdateSecurityUser] (
	@Last_Name NVARCHAR(30),
	@First_Name NVARCHAR(25),
	@Middle_Initial NVARCHAR(3) = NULL,
	@Person_Type NVARCHAR(20) = NULL,
	@Phone NVARCHAR(35)  = NULL,
	@Fax NVARCHAR(15)  = NULL,
	@Email NVARCHAR(129)  = NULL,
	@Dept_Code NVARCHAR(10) =  NULL,
	@Badge_Number NVARCHAR(50),
	@Security_Level NVARCHAR(50), 
	@Supervisor_Last_Name NVARCHAR(30) = NULL,
	@Supervisor_First_Name NVARCHAR(25) = NULL,
	@Supervisor_Middle_Initial NVARCHAR(3) = NULL,
	@Supervisor_Phone NVARCHAR(35) = NULL,
	@Supervisor_Email NVARCHAR(129) = NULL,
	@Location NVARCHAR(20) = NULL,
	@Org_Type NVARCHAR(1), 
	@Org_CAGE NVARCHAR(10) = NULL, 
	@Org_Acronym NVARCHAR(15) = NULL, 
	@NATO NVARCHAR(12) = NULL,
	@CNWDI NVARCHAR(12) = NULL,
	@Courier_Card_Number NVARCHAR(10) = NULL,
	@Courier_Card_Expiration DateTime = NULL,
	@CAC_EID NVARCHAR(60) = NULL,
	@X509SubjectCN NVARCHAR(255) = NULL, 
	@X509IssuerCN NVARCHAR(255) = NULL, 
	@Courier_Card NVARCHAR(1) = NULL,
	@Courier_Card_Issued DateTime = NULL,
	@Courier_Card_Classification NVARCHAR(2) = NULL, 
	@Courier_Card_Geo_Limit NVARCHAR(1) = NULL,  
	@Special_Access NVARCHAR(1) = NULL, 
	@Status_Code NVARCHAR(1), 
	@Changed_Date DateTime,
	@User_Security_ID INT OUTPUT
)
AS
UPDATE User_Security
SET 
	First_Name = @First_Name,
	Last_Name = @Last_Name,
	Middle_Initial = @Middle_Initial,
	Person_Type = @Person_Type,
	Phone = @Phone,
	Fax = @Fax,
	Email = @Email,
	Dept_Code = @Dept_Code,
	Security_Level = @Security_Level,
	Supervisor_Last_Name = @Supervisor_Last_Name,
	Supervisor_First_Name = @Supervisor_First_Name,
	Supervisor_Middle_Initial = @Supervisor_Middle_Initial,
	Supervisor_Phone = @Supervisor_Phone,
	Supervisor_Email = @Supervisor_Email,
	Location = @Location,
	Org_Type = @Org_Type,
	Org_CAGE = @Org_CAGE,
	Org_Acronym = @Org_Acronym,
	NATO = @NATO,
	CNWDI = @CNWDI,
	Courier_Card_Number = @Courier_Card_Number,
	Courier_Card_Expiration = @Courier_Card_Expiration,
	CAC_EID = @CAC_EID,
	X509SubjectCN = @X509SubjectCN,
	X509IssuerCN = @X509IssuerCN,
	Courier_Card = @Courier_Card,
	Courier_Card_Issued = @Courier_Card_Issued,
	Courier_Card_Classification = @Courier_Card_Classification,
	Courier_Card_Geo_Limit = @Courier_Card_Geo_Limit,
	Special_Access = @Special_Access,
	Status_Code = @Status_Code,
	Changed_Date = @Changed_Date
WHERE Badge_Number = @Badge_Number


SET @User_Security_ID = (SELECT User_Security_ID FROM User_Security
WHERE Badge_Number = @Badge_Number)
' 
END
GO


USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procSecurityUserIDSel]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procSecurityUserIDSel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procSecurityUserIDSel]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procSecurityUserIDSel    Script Date: 8/23/2012 ******/
CREATE PROCEDURE [dbo].[procSecurityUserIDSel] (
	@User_Security_ID INT
)
AS
	SET NOCOUNT ON;
	SELECT * 
	FROM User_Security 
	WHERE (User_Security_ID = @User_Security_ID);
' 
END
GO

USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procSecurityUserBadgeSel]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procSecurityUserBadgeSel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procSecurityUserBadgeSel]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procSecurityUserBadgeSel    Script Date: 8/23/2012 ******/
CREATE PROCEDURE [dbo].[procSecurityUserBadgeSel] (
	@Badge_Number  NVARCHAR(50)
)
AS
	SET NOCOUNT ON;
	SELECT * 
	FROM User_Security 
	WHERE (Badge_Number = @Badge_Number)
' 
END
GO


/****** Object:  StoredProcedure [dbo].[procAddSecurityOrg]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procAddSecurityOrg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procAddSecurityOrg]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procAddSecurityOrg    Script Date: 08/23/2012 ******/
CREATE PROCEDURE [dbo].[procAddSecurityOrg] (
	@Org_CAGE NVARCHAR(10),
	@Org_Type NVARCHAR(1) = NULL,
	@Org_Acronym NVARCHAR(15) = NULL,
	@Org_Name NVARCHAR(78) = NULL,
	@Org_Address_1 NVARCHAR(50) = NULL,
	@Org_Address_2 NVARCHAR(50) = NULL,
	@Org_City NVARCHAR(25) = NULL,
	@Org_State NVARCHAR(3) = NULL,
	@Org_Zip NVARCHAR(12) = NULL,
	@Org_FSO_Name NVARCHAR(85) = NULL,
	@Org_FSO_Email NVARCHAR(129) = NULL,
	@Org_FSO_Phone NVARCHAR(35) = NULL,
	@Changed_Date datetime = NULL,
	@Org_Security_ID INT OUTPUT
)
AS
INSERT INTO Org_Security(
	Org_CAGE, Org_Type, Org_Acronym, Org_Name, 
	Org_Address_1, Org_Address_2,
	Org_City, Org_State, Org_Zip,
	Org_FSO_Name, Org_FSO_Email, Org_FSO_Phone,
	Changed_Date
)
VALUES(
@Org_CAGE, @Org_Type, @Org_Acronym, @Org_Name,
@Org_Address_1, @Org_Address_2,
@Org_City, @Org_State, @Org_Zip, 
@Org_FSO_Name, @Org_FSO_Email, @Org_FSO_Phone,
@Changed_Date
)

SELECT @Org_Security_ID = @@IDENTITY
' 
END
GO


USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procUpdateSecurityOrg]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procUpdateSecurityOrg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procUpdateSecurityOrg]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procUpdateSecurityOrg    Script Date: 8/23/2012 ******/
CREATE PROCEDURE [dbo].[procUpdateSecurityOrg] (
	@Org_Type NVARCHAR(1) = NULL,
	@Org_CAGE NVARCHAR(10) = NULL,
	@Org_Acronym NVARCHAR(15) = NULL,
	@Org_Name NVARCHAR(78) = NULL ,
	@Org_Address_1 NVARCHAR(50) = NULL,
	@Org_Address_2 NVARCHAR(50) = NULL,
	@Org_City NVARCHAR(25) = NULL ,
	@Org_State NVARCHAR(3) = NULL ,
	@Org_Zip NVARCHAR(12) = NULL,
	@Org_FSO_Name NVARCHAR(85) = NULL,
	@Org_FSO_Email NVARCHAR(129) = NULL,
	@Org_FSO_Phone NVARCHAR(35) = NULL,
	@Changed_Date datetime = NULL,
	@Org_Security_ID INT OUTPUT
)
AS
UPDATE Org_Security
SET
	Org_Type = @Org_Type, 
	Org_Acronym = @Org_Acronym, 
	Org_Name = @Org_Name, 
	Org_Address_1 = @Org_Address_1, 
	Org_Address_2 = @Org_Address_2, 
	Org_City = @Org_City, 
	Org_State = @Org_State, 
	Org_Zip = @Org_Zip,
	Org_FSO_Name = @Org_FSO_Name,
	Org_FSO_Email = @Org_FSO_Email,
	Org_FSO_Phone = @Org_FSO_Phone,
	Changed_Date = @Changed_Date
WHERE Org_CAGE = @Org_CAGE

SET @Org_Security_ID = (SELECT Org_Security_ID FROM Org_Security
WHERE Org_CAGE = @Org_CAGE)
' 
END
GO


USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procSecurityOrgIDSel]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procSecurityOrgIDSel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procSecurityOrgIDSel]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procSecurityOrgIDSel    Script Date: 8/23/2012 ******/
CREATE PROCEDURE [dbo].[procSecurityOrgIDSel] (
	@Org_Security_ID INT
)
AS
	SET NOCOUNT ON;
	SELECT * 
	FROM Org_Security 
	WHERE (Org_Security_ID = @Org_Security_ID)
' 
END
GO


USE [LBITS001]
GO

/****** Object:  StoredProcedure [dbo].[procSecurityOrgCAGESel]    Script Date: 08/23/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[procSecurityOrgCAGESel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[procSecurityOrgCAGESel]
GO

BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.procSecurityOrgCAGESel    Script Date: 8/23/2012 ******/
CREATE PROCEDURE [dbo].[procSecurityOrgCAGESel] (
	@Org_CAGE  NVARCHAR(10)
)
AS
	SET NOCOUNT ON;
	SELECT * 
	FROM Org_Security 
	WHERE (Org_CAGE = @Org_CAGE)
' 
END
GO

