use LBITS001;
BULK INSERT tblOrg
FROM 'C:\AES_SpecialProjects\CIC\Test Data\tblOrg.txt'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n',
KEEPNULLS,
FIRSTROW = 2
)

BULK INSERT tblPeople
FROM 'C:\AES_SpecialProjects\CIC\Test Data\tblPeople.txt'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n',
KEEPNULLS,
FIRSTROW = 2
)

use LSRv00;
BULK INSERT Customers
FROM 'C:\AES_SpecialProjects\CIC\Test Data\Customers.txt'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n',
KEEPNULLS,
FIRSTROW = 2
)
select * from customers;
-- Needed to load LSRv00 data from sqlcmd w/flat files
--C:\AES_SpecialProjects\CIC\Test Data>sqlcmd -S snyds-aes\SQLEXPRESS_2008 -i Item_Insert.txt -o Item_Insert.log
--C:\AES_SpecialProjects\CIC\Test Data>sqlcmd -S snyds-aes\SQLEXPRESS_2008 -i PeopleItem_Insert.txt -o People_Item_Insert.log
