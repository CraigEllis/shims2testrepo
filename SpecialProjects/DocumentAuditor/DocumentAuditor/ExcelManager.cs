﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace DocumentAuditor {
    class ExcelManager {
        String errorMsg = null;
        string connectionString = null;

        //excel file connection
        private OleDbConnection con;
        private bool isExcel8 = true;

        public ExcelManager() {
        }

        public ExcelManager(string fileName) {
            buildConnectionString(fileName);
        }

        //connect to excel file
        private void buildConnectionString(string filePath) {

            // Check to see what type of file we have
            if (filePath.ToLower().Contains(".xlsx") || filePath.ToLower().Contains(".xlsm")) {
                // Connect to Excel 12 files 
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='"
                    + filePath + "';Extended Properties='Excel 12.0;HDR=Yes;'";
                isExcel8 = false;
            }
            else if (filePath.ToLower().Contains(".xls")) {
                //  Excel 2003
                connectionString = "Provider=Microsoft.Jet.OleDb.4.0; Data Source=" +
                    filePath + ";Extended Properties='Excel 8.0;HDR=Yes'";
            }

            con = new OleDbConnection(connectionString);
        }

        //connect to excel file
        public bool isConnected() {
            bool result = false;
            try {
                if (con.State != ConnectionState.Open) ;
                con.Open();

                //check to the connection
                ConnectionState state = con.State;
                if (state == ConnectionState.Open) {
                    result = true;
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return result;
        }

        public bool documentExists(string value) {
            bool result = false;

            if (con.State != ConnectionState.Open)
            con.Open();

            // See if the record exists in the current table
            string sql = "SELECT COUNT(*) FROM [Documents$]"
                + "WHERE [Document] = '" + value + "'";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            if (count > 0)
                result = true;

            return result;
        }

        public bool updateDocumentRecord(ScanRecord record) {
            bool result = false;
            String sql = null;

            if (con.State != ConnectionState.Open)
                con.Open();

            // See if the record has already been scanned
            sql = "SELECT COUNT(*) FROM [Documents$] "
                + "WHERE [Document] = '" + record.value + "' AND [DocCount] = '1'";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            int count = Convert.ToInt32(cmd.ExecuteScalar());

            if (count == 0) {
                // First scan - update the document record count
                updateDocumentCount(record);
            }
            else {
                // Update the Documents record to show we have a duplicate
                record.duplicate = "D";
                markDocumentAsDuplicate(record);

                // Add the scan as a duplicate scan
                addDuplicateRecord(record);
            }

            con.Close();

            return result;
        }

        public bool updateDocumentCount(ScanRecord record) {
            bool result = false;
            String sql = null;

            if (con.State != ConnectionState.Open)
                con.Open();

            // See if the record has already been scanned
            sql = "UPDATE [Documents$] "
                + "SET [DocCount] = " + record.docCount + ", "
                + "[DocScanned] = '" + record.scanned.ToString() + "' "
                + "WHERE [Document] = '" + record.value + "'";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            int count = cmd.ExecuteNonQuery();

            if (count != 0) {
                // First scan - update the document record
                result = true;
            }

            return result;
        }

        public bool markDocumentAsDuplicate(ScanRecord record) {
            bool result = false;
            String sql = null;

            if (con.State != ConnectionState.Open)
                con.Open();

            // See if the record has already been scanned
            sql = "UPDATE [Documents$] "
                + "SET [Duplicates] = '" + record.duplicate + "'"
                + "WHERE [Document] = '" + record.value + "'";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            int count = cmd.ExecuteNonQuery();

            if (count != 0) {
                // First scan - update the document record
                result = true;
            }

            return result;
        }

        public bool addDuplicateRecord(ScanRecord record) {
            bool result = false;
            String sql = null;

            if (con.State != ConnectionState.Open)
                con.Open();

            // See if the record has already been scanned
            sql = "INSERT INTO [DupDocuments$] "
                + "([DupDocument], [DupCount], [DupScanned]) "
                + " VALUES ('" + record.value + "',"
                + record.docCount + ",'"
                + record.scanned.ToString() + "' " + ")";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            int count = cmd.ExecuteNonQuery();

            if (count != 0) {
                // First scan - update the document record
                result = true;
            }

            return result;
        }

        public bool addNewRecord(ScanRecord record) {
            bool result = false;
            String sql = null;

            if (con.State != ConnectionState.Open)
                con.Open();

            // Update the record as New
            record.newCount = 1;
            record.docCount = 0;

            // See if the record has already been scanned
            sql = "INSERT INTO [NewDocuments$] "
                + "([NewDocument], [NewCount], [NewScanned]) "
                + " VALUES ('" + record.value + "',"
                + record.newCount + ",'"
                + record.scanned.ToString() + "' " + ")";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            int count = cmd.ExecuteNonQuery();

            if (count != 0) {
                // First scan - update the document record
                result = true;
            }

            con.Close();

            return result;
        }

        public int getDocumentCount() {
            int count = 0;

            if (con.State != ConnectionState.Open)
                con.Open();

            // See if the record exists in the current table
            string sql = "SELECT COUNT(*) FROM [Documents$]";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            count = Convert.ToInt32(cmd.ExecuteScalar());

            return count;
        }

        public int getScannedCount() {
            int count = 0;

            if (con.State != ConnectionState.Open)
                con.Open();

            // See if the record exists in the current table
            string sql = "SELECT COUNT(*) FROM [Documents$] "
                + "WHERE [DocCount] = '1'";

            OleDbCommand cmd = new OleDbCommand(sql, con);

            count = Convert.ToInt32(cmd.ExecuteScalar());

            return count;
        }
    }
}
