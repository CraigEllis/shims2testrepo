﻿namespace DocumentAuditor {
    partial class DocumentAuditorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileOpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSaveAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileSeparator1MenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.fileExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsSetupMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.msgStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.docsStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.docCountStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.scanStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.scannedCountStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.databaseLabel = new System.Windows.Forms.Label();
            this.scanLabel = new System.Windows.Forms.Label();
            this.databaseTextBox = new System.Windows.Forms.TextBox();
            this.scanTextBox = new System.Windows.Forms.TextBox();
            this.scanDataGrid = new System.Windows.Forms.DataGridView();
            this.docnumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanFoundColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newDocColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DuplicateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanDateTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scanCaptureTextBox = new System.Windows.Forms.TextBox();
            this.remainingStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.remainingScanStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuMain.SuspendLayout();
            this.statusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scanDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.resetMenuItem,
            this.toolsMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(792, 24);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileOpenMenuItem,
            this.fileSaveMenuItem,
            this.fileSaveAsMenuItem,
            this.fileSeparator1MenuItem1,
            this.fileExitMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileMenuItem.Text = "&File";
            // 
            // fileOpenMenuItem
            // 
            this.fileOpenMenuItem.Name = "fileOpenMenuItem";
            this.fileOpenMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fileOpenMenuItem.Text = "&Open";
            this.fileOpenMenuItem.Click += new System.EventHandler(this.fileOpenMenuItem_Click);
            // 
            // fileSaveMenuItem
            // 
            this.fileSaveMenuItem.Name = "fileSaveMenuItem";
            this.fileSaveMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fileSaveMenuItem.Text = "&Save";
            this.fileSaveMenuItem.Visible = false;
            // 
            // fileSaveAsMenuItem
            // 
            this.fileSaveAsMenuItem.Name = "fileSaveAsMenuItem";
            this.fileSaveAsMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fileSaveAsMenuItem.Text = "&Save As";
            this.fileSaveAsMenuItem.Visible = false;
            // 
            // fileSeparator1MenuItem1
            // 
            this.fileSeparator1MenuItem1.Name = "fileSeparator1MenuItem1";
            this.fileSeparator1MenuItem1.Size = new System.Drawing.Size(149, 6);
            // 
            // fileExitMenuItem
            // 
            this.fileExitMenuItem.Name = "fileExitMenuItem";
            this.fileExitMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fileExitMenuItem.Text = "E&xit";
            this.fileExitMenuItem.Click += new System.EventHandler(this.fileExitMenuItem_Click);
            // 
            // resetMenuItem
            // 
            this.resetMenuItem.Name = "resetMenuItem";
            this.resetMenuItem.Size = new System.Drawing.Size(47, 20);
            this.resetMenuItem.Text = "&Reset";
            this.resetMenuItem.Click += new System.EventHandler(this.resetMenuItem_Click);
            // 
            // toolsMenuItem
            // 
            this.toolsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsSetupMenuItem});
            this.toolsMenuItem.Name = "toolsMenuItem";
            this.toolsMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolsMenuItem.Text = "&Tools";
            this.toolsMenuItem.Visible = false;
            // 
            // toolsSetupMenuItem
            // 
            this.toolsSetupMenuItem.Name = "toolsSetupMenuItem";
            this.toolsSetupMenuItem.Size = new System.Drawing.Size(102, 22);
            this.toolsSetupMenuItem.Text = "&Setup";
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.msgStatusLabel,
            this.docsStatusLabel,
            this.docCountStatusLabel,
            this.scanStatusLabel,
            this.scannedCountStatusLabel,
            this.remainingStatusLabel,
            this.remainingScanStatusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 551);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(792, 22);
            this.statusBar.TabIndex = 1;
            this.statusBar.Text = "statusStrip1";
            // 
            // msgStatusLabel
            // 
            this.msgStatusLabel.Name = "msgStatusLabel";
            this.msgStatusLabel.Size = new System.Drawing.Size(451, 17);
            this.msgStatusLabel.Spring = true;
            this.msgStatusLabel.Text = "Ready";
            this.msgStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // docsStatusLabel
            // 
            this.docsStatusLabel.Name = "docsStatusLabel";
            this.docsStatusLabel.Size = new System.Drawing.Size(64, 17);
            this.docsStatusLabel.Text = "Documents:";
            this.docsStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // docCountStatusLabel
            // 
            this.docCountStatusLabel.AutoSize = false;
            this.docCountStatusLabel.Name = "docCountStatusLabel";
            this.docCountStatusLabel.Size = new System.Drawing.Size(50, 17);
            this.docCountStatusLabel.Text = "0";
            this.docCountStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // scanStatusLabel
            // 
            this.scanStatusLabel.Name = "scanStatusLabel";
            this.scanStatusLabel.Size = new System.Drawing.Size(52, 17);
            this.scanStatusLabel.Text = "Scanned:";
            this.scanStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // scannedCountStatusLabel
            // 
            this.scannedCountStatusLabel.AutoSize = false;
            this.scannedCountStatusLabel.Name = "scannedCountStatusLabel";
            this.scannedCountStatusLabel.Size = new System.Drawing.Size(50, 17);
            this.scannedCountStatusLabel.Text = "0";
            this.scannedCountStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // databaseLabel
            // 
            this.databaseLabel.AutoSize = true;
            this.databaseLabel.Location = new System.Drawing.Point(12, 39);
            this.databaseLabel.Name = "databaseLabel";
            this.databaseLabel.Size = new System.Drawing.Size(56, 13);
            this.databaseLabel.TabIndex = 2;
            this.databaseLabel.Text = "Database:";
            // 
            // scanLabel
            // 
            this.scanLabel.AutoSize = true;
            this.scanLabel.Location = new System.Drawing.Point(439, 39);
            this.scanLabel.Name = "scanLabel";
            this.scanLabel.Size = new System.Drawing.Size(35, 13);
            this.scanLabel.TabIndex = 4;
            this.scanLabel.Text = "Scan:";
            // 
            // databaseTextBox
            // 
            this.databaseTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseTextBox.Location = new System.Drawing.Point(74, 34);
            this.databaseTextBox.Name = "databaseTextBox";
            this.databaseTextBox.ReadOnly = true;
            this.databaseTextBox.Size = new System.Drawing.Size(359, 22);
            this.databaseTextBox.TabIndex = 5;
            // 
            // scanTextBox
            // 
            this.scanTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scanTextBox.Location = new System.Drawing.Point(491, 34);
            this.scanTextBox.Name = "scanTextBox";
            this.scanTextBox.ReadOnly = true;
            this.scanTextBox.Size = new System.Drawing.Size(289, 22);
            this.scanTextBox.TabIndex = 6;
            // 
            // scanDataGrid
            // 
            this.scanDataGrid.AllowUserToAddRows = false;
            this.scanDataGrid.AllowUserToDeleteRows = false;
            this.scanDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.scanDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.scanDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scanDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.docnumColumn,
            this.scanFoundColumn,
            this.newDocColumn,
            this.DuplicateColumn,
            this.scanDateTimeColumn});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.scanDataGrid.DefaultCellStyle = dataGridViewCellStyle14;
            this.scanDataGrid.Location = new System.Drawing.Point(15, 79);
            this.scanDataGrid.Name = "scanDataGrid";
            this.scanDataGrid.ReadOnly = true;
            this.scanDataGrid.Size = new System.Drawing.Size(765, 455);
            this.scanDataGrid.TabIndex = 7;
            // 
            // docnumColumn
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.docnumColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.docnumColumn.HeaderText = "Document #";
            this.docnumColumn.Name = "docnumColumn";
            this.docnumColumn.ReadOnly = true;
            this.docnumColumn.Width = 300;
            // 
            // scanFoundColumn
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.scanFoundColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.scanFoundColumn.HeaderText = "Found";
            this.scanFoundColumn.Name = "scanFoundColumn";
            this.scanFoundColumn.ReadOnly = true;
            this.scanFoundColumn.Width = 70;
            // 
            // newDocColumn
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.newDocColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this.newDocColumn.HeaderText = "New";
            this.newDocColumn.Name = "newDocColumn";
            this.newDocColumn.ReadOnly = true;
            this.newDocColumn.Width = 70;
            // 
            // DuplicateColumn
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DuplicateColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this.DuplicateColumn.HeaderText = "Duplicate";
            this.DuplicateColumn.Name = "DuplicateColumn";
            this.DuplicateColumn.ReadOnly = true;
            this.DuplicateColumn.Width = 70;
            // 
            // scanDateTimeColumn
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.scanDateTimeColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this.scanDateTimeColumn.HeaderText = "Scan Date";
            this.scanDateTimeColumn.Name = "scanDateTimeColumn";
            this.scanDateTimeColumn.ReadOnly = true;
            this.scanDateTimeColumn.Width = 190;
            // 
            // scanCaptureTextBox
            // 
            this.scanCaptureTextBox.Location = new System.Drawing.Point(506, 34);
            this.scanCaptureTextBox.Name = "scanCaptureTextBox";
            this.scanCaptureTextBox.Size = new System.Drawing.Size(144, 20);
            this.scanCaptureTextBox.TabIndex = 8;
            this.scanCaptureTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.scanCaptureTextBox_KeyDown);
            // 
            // remainingStatusLabel
            // 
            this.remainingStatusLabel.Name = "remainingStatusLabel";
            this.remainingStatusLabel.Size = new System.Drawing.Size(60, 17);
            this.remainingStatusLabel.Text = "Remaining:";
            // 
            // remainingScanStatusLabel
            // 
            this.remainingScanStatusLabel.AutoSize = false;
            this.remainingScanStatusLabel.Name = "remainingScanStatusLabel";
            this.remainingScanStatusLabel.Size = new System.Drawing.Size(50, 17);
            this.remainingScanStatusLabel.Text = "0";
            this.remainingScanStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DocumentValidatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.scanDataGrid);
            this.Controls.Add(this.scanTextBox);
            this.Controls.Add(this.databaseTextBox);
            this.Controls.Add(this.scanLabel);
            this.Controls.Add(this.databaseLabel);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.menuMain);
            this.Controls.Add(this.scanCaptureTextBox);
            this.MainMenuStrip = this.menuMain;
            this.Name = "DocumentValidatorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Doc Aud";
            this.Activated += new System.EventHandler(this.DocumentValidatorForm_Activated);
            this.Load += new System.EventHandler(this.DocumentValidatorForm_Load);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scanDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileOpenMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileSaveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileSaveAsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileExitMenuItem;
        private System.Windows.Forms.ToolStripSeparator fileSeparator1MenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsSetupMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel msgStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel docsStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel docCountStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel scanStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel scannedCountStatusLabel;
        private System.Windows.Forms.Label databaseLabel;
        private System.Windows.Forms.Label scanLabel;
        private System.Windows.Forms.TextBox databaseTextBox;
        private System.Windows.Forms.TextBox scanTextBox;
        private System.Windows.Forms.DataGridView scanDataGrid;
        private System.Windows.Forms.TextBox scanCaptureTextBox;
        private System.Windows.Forms.ToolStripMenuItem resetMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn docnumColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanFoundColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn newDocColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DuplicateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scanDateTimeColumn;
        private System.Windows.Forms.ToolStripStatusLabel remainingStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel remainingScanStatusLabel;
    }
}

