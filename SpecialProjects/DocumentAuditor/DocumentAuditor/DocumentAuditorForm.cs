﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DocumentAuditor {
    public partial class DocumentAuditorForm : Form {
        public enum Beeps {
            Good,   // Initial scan - Beep
            Exists, // Already scanned - Beep twice
            New,    // New item not in list - Question
            Error,  // Error somewhere - Error
        };

        private ExcelManager manager = null;

        int docCount = 0;
        int scanned = 0;
        int remaining = 0;

        public DocumentAuditorForm() {
            InitializeComponent();
        }

        private void DocumentValidatorForm_Load(object sender, EventArgs e) {
        }

        private void DocumentValidatorForm_Activated(object sender, EventArgs e) {
            scanCaptureTextBox.Focus();
        }

        private void resetMenuItem_Click(object sender, EventArgs e) {
            // Set the focus back to the scan capture text box
            scanCaptureTextBox.Focus();
        }

        private void fileOpenMenuItem_Click(object sender, EventArgs e) {
            // Show the open file dialog to get the database name
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel files (*.xls, *.xlsx)|*.xls;*.xlsx|All Files (*.*)|*.*";

            DialogResult result = dialog.ShowDialog(this);

            if (result == DialogResult.OK) {
                // Stuff the file name into the text box
                databaseTextBox.Text = dialog.SafeFileName;

                // Create the excel manager
                openDatabase(dialog.FileName);


            }

            resetMenuItem_Click(sender, e);
        }

        private void scanCaptureTextBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                // process the scan
                processScan(scanCaptureTextBox.Text);

                e.Handled = true;
            }
        }

        private void fileExitMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }
        
        private Beeps processScan(String value) {
            Beeps beep = Beeps.Error;

            if (databaseTextBox.Text.Length == 0) {
                msgStatusLabel.Text = "No File Selected";
                playWAV("./sounds/555klaxon-2.wav", 1);

                // Clear the scan
                scanCaptureTextBox.Text = "";

                MessageBox.Show(this, "Please select a Document spreadsheet to audit",
                    "No File Selected");

                msgStatusLabel.Text = "Ready";

                return beep;
            }

            // Update the scan text box
            scanTextBox.Text = scanCaptureTextBox.Text;

            // Update the database
            beep = processToDatabase(scanCaptureTextBox.Text);

            // Update the form
            switch (beep) {
                case Beeps.Good:
                    // Play the good beep
                    playWAV("./sounds/Speech Sleep.wav", 1);
                    msgStatusLabel.Text = "Document processed";

                    // Increment the scanned count
                    scanned++;
                    remaining--;
                    updateScanCounts();
                    break;
                case Beeps.Exists:
                    // Play the good beep twice - no longer it's an alarm
                    playWAV("./sounds/555klaxon-2.wav", 2);
                    msgStatusLabel.Text = "Duplicate document found";
                    break;
                case Beeps.New:
                    // Play the new beep 
                    playWAV("./sounds/555klaxon-2.wav", 1);
                    msgStatusLabel.Text = "New document found";
                    break;
                case Beeps.Error:
                    // Play the error beep
                    playWAV("./sounds/555klaxon-2.wav", 2);
                    msgStatusLabel.Text = "Scan error";
                    break;
            }

            // Clear the scan capture
            scanCaptureTextBox.Clear();

            return beep;
        }

        private Beeps processToDatabase(String value) {
            Beeps beep = Beeps.Error;

            // Create a scan record
            ScanRecord record = new ScanRecord();
            record.value = value;
            record.docCount = 1;
            record.duplicate = "";
            record.scanned = DateTime.Now;

            // Check for an existing document
            if (manager.documentExists(record.value)) {
                // Update the document record
                manager.updateDocumentRecord(record);
                addGridRecord(record);

                // Set the beeps
                beep = Beeps.Good;
                if (record.duplicate.Length > 0) 
                    beep = Beeps.Exists;
            }
            else {
                // Add the scan as a new item
                manager.addNewRecord(record);
                addGridRecord(record);

                // Set the beeps
                beep = Beeps.New;
            }

            scanDataGrid.CurrentCell = 
                this.scanDataGrid[0, this.scanDataGrid.Rows.Count - 1];

            return beep;
        }

        private void openDatabase(String fileName) {
            manager = new ExcelManager(fileName);

            // Test to make sure we can connect
            if (!manager.isConnected()) {
                playWAV("./sounds/555klaxon-2.wav", 2);
                msgStatusLabel.Text = "Cannot connect to Excel file: " + fileName;

                MessageBox.Show(this, "Error connecting to Excel file.",
                    "Excel error");

                msgStatusLabel.Text = "Ready";
            }
            else {
                // Display the number of documents and the number remaining
                docCount = manager.getDocumentCount();
                scanned = manager.getScannedCount();
                remaining = docCount - scanned;
                docCountStatusLabel.Text = String.Format("{0:n0}", docCount);
                updateScanCounts();
            }
        }

        private void addGridRecord(ScanRecord record) {
            int n = scanDataGrid.Rows.Add();

            scanDataGrid.Rows[n].Cells[0].Value = record.value;
            scanDataGrid.Rows[n].Cells[1].Value = record.docCount;
            scanDataGrid.Rows[n].Cells[2].Value = record.newCount;
            scanDataGrid.Rows[n].Cells[3].Value = record.duplicate;
            scanDataGrid.Rows[n].Cells[4].Value = record.scanned.ToString();
        }

        public static void playWAV(String wavFile, int repeat = 0) {
            //Declare player as a new SoundPlayer using the wav file
            System.Media.SoundPlayer player = new System.Media.SoundPlayer(wavFile);

            for (int i = 0; i < repeat; i++) {
                //Play the sound
                player.Play();
                System.Threading.Thread.Sleep(500);
                //System.Media.SystemSound sound = System.Media.SystemSounds.Beep;
                //sound.Play();
            }
        }

        private void updateScanCounts() {
            scannedCountStatusLabel.Text = String.Format("{0:n0}", scanned);
            remainingScanStatusLabel.Text = String.Format("{0:n0}", remaining);
        }
    }
}
