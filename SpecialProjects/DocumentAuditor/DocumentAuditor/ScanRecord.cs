﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentAuditor {
    class ScanRecord {
        public string value { get; set; }

        public int docCount { get; set; }

        public int newCount { get; set; }

        public string duplicate { get; set; }

        public DateTime scanned { get; set; }
    }
}
