﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace SecureConfigFile {
    class Program {
        static bool process = true;

        static void Main(string[] args) {
            Console.WriteLine("SecureConfigFile - encrypt config file connection strings");

            while (process) {
	            Console.Write("\nEnter config file name (blank to exit): ");
	            string fileName = Console.ReadLine();

                if (fileName.Length > 0) {
                    ToggleConfigEncryption(fileName);
                }
                else
                    process = false;
	        }
        }

        static void ToggleConfigEncryption(string fileName) {
            try {
                // Open the configuration file and retrieve the connectionStrings section
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = fileName;
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(
                    fileMap, ConfigurationUserLevel.None);

                ConnectionStringsSection section =
                    config.GetSection("connectionStrings")
                    as ConnectionStringsSection;

                if (section.SectionInformation.IsProtected) {
                    Console.WriteLine("File is already encrypted");
                }
                else {
                    // Backup the unencrypted file - obfuscating the sensitive data
                    if (BackupFile(fileName)) {
                        // Encrypt the section.
                        section.SectionInformation.ProtectSection(
                            "DataProtectionConfigurationProvider");

                        // Save the current configuration.
                        config.Save();

                        Console.WriteLine("Protected={0}",
                            section.SectionInformation.IsProtected);
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine("ERROR - " + ex.Message);
            }

            //// See if they want to do another file
            //Console.Write("\nProcess another config file (Y/N): ");
            //string response = Console.ReadLine();

            //if (response.ToUpper().Equals("N"))
            //    process = false;
        }

        static bool BackupFile(string fileName) {
            bool result = false;
            try {
                // Open the file and replace the sensitive data with Xs
                StreamReader streamReader = new StreamReader(fileName);
                string fileText = streamReader.ReadToEnd();
                streamReader.Close();

                // Obfuscate the sensitive data
                string safeText = fileText;
                string pattern = "";

                // Data Source
                pattern = "(.+?Data Source=)(.+?)([;\"].+)";
                safeText = Regex.Replace(safeText, pattern, m => m.Groups[1] 
                    + "XXXXX" + m.Groups[3]);

                // Initial Database
                pattern = "(.+?Initial Catalog=)(.+?)([;\"].+)";
                safeText = Regex.Replace(safeText, pattern, m => m.Groups[1] 
                    + "XXXXX" + m.Groups[3]);

                // User
                pattern = "(.+?User ID=)(.+?)([;\"].+)";
                safeText = Regex.Replace(safeText, pattern, m => m.Groups[1] 
                    + "XXXXX" + m.Groups[3]);

                // Password
                pattern = "(.+?Password=)(.+?)([;\"].+)";
                safeText = Regex.Replace(safeText, pattern, m => m.Groups[1] 
                    + "XXXXX" + m.Groups[3]);

                // Save the safe file (fileName.bak)
                StreamWriter streamWriter = new StreamWriter(fileName + ".bak");
                streamWriter.Write(safeText);
                streamWriter.Flush();
                streamWriter.Close();

                Console.WriteLine("Config file backed up to: " + fileName + ".bak");

                result = true;
            }
            catch (Exception ex) {
                Console.WriteLine("ERROR - Could not backup config file - "
                    + ex.Message);
            }

            return result;
        }
    }
}
